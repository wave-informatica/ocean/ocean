/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.audit;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.waveinformatica.ocean.audit.entities.SentMailEntity;
import com.waveinformatica.ocean.core.annotations.OceanPersistenceContext;

public class AuditDao {
	
	@Inject
	@OceanPersistenceContext
	private EntityManager em;
	
	public List<SentMailEntity> getSentMailsByRecipient(String recipient) {
		
		List<SentMailEntity> result = em.createQuery("select x from SentMailEntity x where x.to = :recipient or x.from = :recipient or x.to like '%<' || :recipient || '>' or x.from like '%<' || :recipient || '>' order by x.date", SentMailEntity.class)
				.setParameter("recipient", recipient)
				.getResultList();
		
		return result;
		
	}

}
