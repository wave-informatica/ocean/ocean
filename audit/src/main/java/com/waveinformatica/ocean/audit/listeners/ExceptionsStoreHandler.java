/*
 * Copyright 2016, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.audit.listeners;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.audit.entities.ExceptionEntity;
import com.waveinformatica.ocean.audit.entities.ExceptionTraceEntity;
import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.annotations.OceanPersistenceContext;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.OperationErrorEvent;
import com.waveinformatica.ocean.core.controllers.results.HttpErrorResult;
import com.waveinformatica.ocean.core.exceptions.OperationNotFoundException;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.ParametersBuilder;

/**
 *
 * @author Ivano
 */
@CoreEventListener(eventNames = CoreEventName.OPERATION_ERROR)
public class ExceptionsStoreHandler implements IEventListener<OperationErrorEvent> {

	@Inject
	private ObjectFactory factory;
	
	@Inject
	private Configuration config;
	
	@Inject
	private CoreController controller;
	
	@Inject
	@OceanPersistenceContext
	private EntityManager em;
	
	@Override
	public void performAction(OperationErrorEvent evt) {
		
		try {
			
			em.getTransaction().begin();
			
			Throwable root = evt.getError();
			while (root.getCause() != null && root.getCause() != root) {
				root = root.getCause();
			}
			
			ExceptionEntity exc = new ExceptionEntity();
			exc.setErrorDate(new Date());
			exc.setMessage(evt.getError().getMessage());
			exc.setException(evt.getError().getClass().getName());
			exc.setRootException(root.getClass().getName());
			
			em.persist(exc);
			em.getTransaction().commit();
			
			em.getTransaction().begin();
			
			Long idExc = exc.getId();
			
			ExceptionTraceEntity trace = em.createQuery("select x from ExceptionTraceEntity x where x.id = :id", ExceptionTraceEntity.class)
				  .setParameter("id", idExc)
				  .getSingleResult();
			
			trace.setTrace(serializeException(evt.getError()));
			
			em.merge(trace);
			
			em.getTransaction().commit();
			
		} finally {
			LoggerUtils.getLogger(ExceptionsStoreHandler.class).log(Level.SEVERE, "Unable to store exception data for the following error:", evt.getError());
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}
		
		if (evt.isCritical()) {

			Properties props = config.getLoggingProperties();
			String recipient = props.getProperty("ocean.errors.feedback.recipient");

			if (StringUtils.isNotBlank(recipient)) {
				HttpErrorResult res = factory.newInstance(HttpErrorResult.class);
				res.setThrowable(evt.getError());
				ParametersBuilder builder = new ParametersBuilder();
				builder.add("exception", res.getFormattedException());
				if (Context.get() != null && Context.get().getRequest() != null) {
					builder.add("method", Context.get().getRequest().getMethod());
				}
				builder.add("parameters", res.getRequestInput());
				try {
					controller.executeOperation("/exceptions/sendError", builder.build(), Context.get().getRequest());
				} catch (OperationNotFoundException e) {
					LoggerUtils.getLogger(ExceptionsStoreHandler.class).log(Level.SEVERE, null, e);
				}
			}
			
		}
		
	}
	
	private String serializeException(Throwable t) {
		
		StringWriter writer = new StringWriter();
		t.printStackTrace(new PrintWriter(writer));
		return writer.toString();
		
	}
	
}
