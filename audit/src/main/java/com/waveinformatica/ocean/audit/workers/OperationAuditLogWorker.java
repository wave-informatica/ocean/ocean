/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.audit.workers;

import com.google.gson.Gson;
import com.waveinformatica.ocean.audit.dto.AuditOperation;
import com.waveinformatica.ocean.audit.entities.AuditActionType;
import com.waveinformatica.ocean.audit.entities.AuditEntity;
import com.waveinformatica.ocean.core.annotations.OceanPersistenceContext;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.tasks.OceanRunnable;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;

import java.util.logging.Level;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author Ivano
 */
public class OperationAuditLogWorker extends OceanRunnable {
    
    @Inject
    private ObjectFactory factory;
    
    private AuditOperation operation;
    
    public void loadAuditOperation(AuditOperation operation) {
	this.operation = operation;
    }
    
    @Override
    public void execute() {
	
	Support support = factory.newInstance(Support.class);
	
	Gson gson = JsonUtils.getBuilder().create();
	
	AuditEntity ae = new AuditEntity();
	ae.setActionDescription(operation.getOperation());
	ae.setLogTime(operation.getTime());
	ae.setType(AuditActionType.OPERATION);
	ae.setUserId(operation.getUserId());
	ae.setUserName(operation.getUserName());
	ae.setJsonData(gson.toJson(operation.getParams()));
	
	support.getEm().getTransaction().begin();
	
	try {
	    
	    support.getEm().persist(ae);
	    
	    support.getEm().getTransaction().commit();
	    
	} catch (Exception e) {
	    if (support.getEm().getTransaction().isActive()) {
		support.getEm().getTransaction().rollback();
	    }
	    LoggerUtils.getLogger(OperationAuditLogWorker.class).log(Level.SEVERE, "Error saving audit log for operation \"" + operation.getOperation() + "\" from user \"" + operation.getUserName() + "\"", e);
	}
	
    }
    
    public static class Support {
	
	@Inject
	@OceanPersistenceContext
	private EntityManager em;

	public EntityManager getEm() {
	    return em;
	}
	
    }
    
}
