/*
* Copyright 2016 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.audit.entities;

import com.waveinformatica.ocean.core.annotations.Filterable;
import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.core.filtering.DayFilter;
import com.waveinformatica.ocean.core.filtering.StringFilter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Ivano
 */
@Entity
@Table(name = "audit_mails")
public class SentMailEntity implements Serializable {
	
	@Id
	@GeneratedValue(generator = "seq_audit_mail", strategy = GenerationType.AUTO)
	@SequenceGenerator(name = "seq_audit_mail", sequenceName = "seq_audit_mail")
	@Column
	@TableResult.TableResultColumn(hidden = true, key = true)
	private Long id;
	
	@Column(name = "sent_date")
	@Temporal(TemporalType.TIMESTAMP)
	@Filterable(filter = DayFilter.class)
	private Date date;
	
	@Column(name = "from_address", length = 1000)
	@Filterable(filter = StringFilter.class)
	private String from;
	
	@Column(name = "to_address", length = 1000)
	@Filterable(filter = StringFilter.class)
	private String to;
	
	@Column(length = 1000)
	@TableResult.TableResultColumn(hidden = true)
	private String cc;
	
	@Column(length = 1000)
	@TableResult.TableResultColumn(hidden = true)
	private String bcc;
	
	@Column(length = 1000)
	@Filterable(filter = StringFilter.class)
	private String subject;
	
	@Column(name = "message_id")
	@TableResult.TableResultColumn(hidden = true)
	private String messageId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	
}
