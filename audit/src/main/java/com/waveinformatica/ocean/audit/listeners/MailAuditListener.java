/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.audit.listeners;

import com.waveinformatica.ocean.audit.entities.SentMailBodyEntity;
import com.waveinformatica.ocean.audit.entities.SentMailEntity;
import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.annotations.OceanPersistenceContext;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.Event;
import com.waveinformatica.ocean.core.controllers.events.MailSentEvent;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.logging.Level;
import javax.inject.Inject;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
@CoreEventListener(eventNames = CoreEventName.MAIL_SENT)
public class MailAuditListener implements IEventListener {

	@Inject
	@OceanPersistenceContext
	private EntityManager em;
	
	@Override
	public void performAction(Event event) {
		
		MailSentEvent evt = (MailSentEvent) event;
		
		try {
			
			em.getTransaction().begin();
			
			SentMailEntity mailSummary = new SentMailEntity();
			
			MimeMessage msg = (MimeMessage) evt.getMessage();
			
			mailSummary.setDate(new Date());
			mailSummary.setSubject(msg.getSubject());
			mailSummary.setFrom(addressesToString(msg.getFrom()));
			mailSummary.setTo(addressesToString(msg.getRecipients(Message.RecipientType.TO)));
			mailSummary.setCc(addressesToString(msg.getRecipients(Message.RecipientType.CC)));
			mailSummary.setBcc(addressesToString(msg.getRecipients(Message.RecipientType.BCC)));
			mailSummary.setMessageId(msg.getMessageID());
			
			em.persist(mailSummary);
			
			SentMailBodyEntity body = em.createQuery("select x from SentMailBodyEntity x where x.id = :id", SentMailBodyEntity.class)
				  .setParameter("id", mailSummary.getId())
				  .getSingleResult();
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			msg.writeTo(baos);
			body.setMessage(new String(baos.toByteArray(), Charset.forName("US-ASCII")));
			
			em.merge(body);
			
			em.getTransaction().commit();
			
		} catch (Exception e) {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			LoggerUtils.getLogger(MailAuditListener.class).log(Level.SEVERE, "Error storing mail log", e);
		}
		
	}
	
	private String addressesToString(Address[] addresses) {
		if (addresses == null || addresses.length == 0) {
			return null;
		}
		StringBuilder builder = new StringBuilder();
		for (Address a : addresses) {
			InternetAddress ia = (InternetAddress) a;
			if (builder.length() > 0) {
				builder.append("; ");
			}
			if (StringUtils.isNotBlank(ia.getPersonal())) {
				builder.append(ia.getPersonal());
				builder.append(" <");
			}
			builder.append(ia.getAddress());
			if (StringUtils.isNotBlank(ia.getPersonal())) {
				builder.append('>');
			}
		}
		return builder.toString();
	}
	
}
