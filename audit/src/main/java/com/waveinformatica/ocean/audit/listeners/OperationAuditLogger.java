/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.audit.listeners;

import com.waveinformatica.ocean.audit.AuditService;
import com.waveinformatica.ocean.audit.dto.AuditOperation;
import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.events.BeforeOperationEvent;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.Event;
import com.waveinformatica.ocean.core.security.UserSession;
import java.util.Date;
import java.util.Map;
import javax.inject.Inject;

/**
 *
 * @author Ivano
 */
@CoreEventListener(eventNames = CoreEventName.BEFORE_OPERATION)
public class OperationAuditLogger implements IEventListener {

    @Inject
    private UserSession session;
    
    @Inject
    private CoreController controller;
    
    @Override
    public void performAction(Event event) {
	
	AuditService service = controller.getService(AuditService.class);
	
	BeforeOperationEvent evt = (BeforeOperationEvent) event;
	
	OperationInfo op = evt.getOperation();
	Map<String, String[]> args = evt.getArgs();
	
	if (!service.isOperationAudited(op.getFullName())) {
	    return;
	}
	
	AuditOperation auditOp = new AuditOperation();
	auditOp.setOperation(op.getFullName());
	auditOp.setParams(args);
	auditOp.setTime(new Date());
	auditOp.setUserId(session.isLogged() ? session.getPrincipal().getId() : null);
	auditOp.setUserName(session.getUserName());
	
	service.logAuditOperation(auditOp);
    }
    
}
