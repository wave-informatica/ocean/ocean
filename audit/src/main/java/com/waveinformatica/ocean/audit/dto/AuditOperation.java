/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.audit.dto;

import java.util.Date;
import java.util.Map;

/**
 *
 * @author Ivano
 */
public class AuditOperation {
    
    private Long userId;
    private String userName;
    private String operation;
    private Map<String, String[]> params;
    private Date time;

    public Long getUserId() {
	return userId;
    }

    public void setUserId(Long userId) {
	this.userId = userId;
    }

    public String getUserName() {
	return userName;
    }

    public void setUserName(String userName) {
	this.userName = userName;
    }

    public String getOperation() {
	return operation;
    }

    public void setOperation(String operation) {
	this.operation = operation;
    }

    public Map<String, String[]> getParams() {
	return params;
    }

    public void setParams(Map<String, String[]> params) {
	this.params = params;
    }

    public Date getTime() {
	return time;
    }

    public void setTime(Date time) {
	this.time = time;
    }
    
}
