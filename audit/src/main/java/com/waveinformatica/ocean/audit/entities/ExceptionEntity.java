/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.audit.entities;

import com.waveinformatica.ocean.core.annotations.Filterable;
import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.core.filtering.DayFilter;
import com.waveinformatica.ocean.core.filtering.StringFilter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Ivano
 */
@Entity
@Table(name = "audit_exceptions")
public class ExceptionEntity implements Serializable {
	
	@Id
	@GeneratedValue(generator = "seq_audit_exceptions", strategy = GenerationType.AUTO)
	@SequenceGenerator(name = "seq_audit_exceptions", sequenceName = "seq_audit_exceptions")
	@Column
	@TableResult.TableResultColumn(hidden = true, key = true)
	private Long id;
	
	@Column(name = "error_date")
	@Temporal(TemporalType.TIMESTAMP)
	@Filterable(filter = DayFilter.class)
	private Date errorDate;
	
	@Column(name = "exception_name")
	@Filterable(filter = StringFilter.class)
	private String exception;
	
	@Column(name = "root_exception_name")
	@Filterable(filter = StringFilter.class)
	private String rootException;
	
	@Column
	@Filterable(filter = StringFilter.class)
	private String message;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getErrorDate() {
		return errorDate;
	}

	public void setErrorDate(Date errorDate) {
		this.errorDate = errorDate;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getRootException() {
		return rootException;
	}

	public void setRootException(String rootException) {
		this.rootException = rootException;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
