/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.audit;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.waveinformatica.ocean.audit.dto.ConfigElement;
import com.waveinformatica.ocean.audit.entities.AuditEntity;
import com.waveinformatica.ocean.audit.entities.ExceptionEntity;
import com.waveinformatica.ocean.audit.entities.ExceptionTraceEntity;
import com.waveinformatica.ocean.audit.entities.SentMailBodyEntity;
import com.waveinformatica.ocean.audit.entities.SentMailEntity;
import com.waveinformatica.ocean.core.annotations.OceanPersistenceContext;
import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.annotations.RootOperation;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.MessageResult;
import com.waveinformatica.ocean.core.controllers.results.StackResult;
import com.waveinformatica.ocean.core.controllers.results.StreamResult;
import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.core.controllers.results.input.Range;
import com.waveinformatica.ocean.core.controllers.scopes.AdminScopeProvider.AdminRootScope;
import com.waveinformatica.ocean.core.controllers.scopes.OperationScope;
import com.waveinformatica.ocean.core.filtering.FilterBuilder;
import com.waveinformatica.ocean.core.filtering.QueryFilter;
import com.waveinformatica.ocean.core.filtering.drivers.JpqlBuilderDriver;
import com.waveinformatica.ocean.core.filtering.drivers.JpqlQueryFiller;
import com.waveinformatica.ocean.core.filtering.drivers.ValuesManager;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.PersistedProperties;

@OperationProvider(namespace = "admin/audit", inScope = AdminRootScope.class)
public class Operations {
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private CoreController controller;
	
	@Inject
	@OceanPersistenceContext
	private EntityManager em;
	
	@Operation("")
	@RootOperation(section = "audit", iconUrl = "cog", title = "manage")
	public StackResult manage(OperationScope opScope) {
		
		opScope.addBreadcrumbLink("admin/audit/", "manage");
		
		StackResult result = factory.newInstance(StackResult.class);
		
		//result.addOperation("/admin/audit/events");
		result.addOperation("/admin/audit/operations");
		
		return result;
		
	}
	
	@Operation("events")
	public TableResult events() {
		
		TableResult result = factory.newInstance(TableResult.class);
		
		return result;
		
	}
	
	@Operation("operations")
	public TableResult operations() {
		
		AuditService service = controller.getService(AuditService.class);
		PersistedProperties props = service.getConfig();
		Gson gson = JsonUtils.getBuilder().create();
		Map<String, ConfigElement> config = gson.fromJson(props.getProperty("auditOperations", "{}"), new TypeToken<Map<String, ConfigElement>>() {}.getType());
		
		TableResult result = factory.newInstance(TableResult.class);
		
		List<ConfigElement> elements = new ArrayList<ConfigElement>();
		
		for (String n : controller.getInspector().getOperationNames()) {
			ConfigElement el = config.get(n);
			if (el == null) {
				el = new ConfigElement();
				el.setType("operation");
				el.setName(n);
				el.setAudited(false);
			}
			elements.add(el);
		}
		
		result.setTableData(elements, ConfigElement.class);
		result.setCheckable(true);
		
		result.setRowsPerPage(10);
		
		result.addRowOperation("admin/audit/toggleOpAudit", "toggleAudit");
		result.addTableOperation("admin/audit/toggleOpAudit", "toggleAudit");
		
		return result;
		
	}
	
	@Operation("toggleOpAudit")
	public MessageResult toggleOpAudit(@Param("name") String name, @Param("fw:tab_sel") List<String> names) {
		
		if (names == null) {
			names = new ArrayList<String>(1);
			names.add(name);
		}
		
		AuditService service = controller.getService(AuditService.class);
		PersistedProperties props = service.getConfig();
		Gson gson = JsonUtils.getBuilder().create();
		Map<String, ConfigElement> config = gson.fromJson(props.getProperty("auditOperations", "{}"), new TypeToken<Map<String, ConfigElement>>() {}.getType());
		
		for (String n : names) {
			ConfigElement el = config.get(n);
			if (el == null) {
				el = new ConfigElement();
				el.setType("operation");
				el.setName(n);
				el.setAudited(true);
				config.put(n, el);
			}
			else {
				config.remove(n);
			}
		}
		
		props.setProperty("auditOperations", gson.toJson(config));
		
		try {
			props.store("");
		} catch (IOException ex) {
			LoggerUtils.getLogger(Operations.class).log(Level.SEVERE, "Error storing audit configuration", ex);
		}
		
		service.updateAuditOperations(config);
		
		MessageResult result = factory.newInstance(MessageResult.class);
		result.setMessage("Operation audit successfully toggled");
		result.setType(MessageResult.MessageType.CONFIRM);
		result.setConfirmAction("admin/audit/");
		return result;
	}
	
	@Operation("logs")
	@RootOperation(section = "audit", iconUrl = "list", title = "logs")
	public TableResult showLogs(HttpServletRequest request, OperationScope opScope) {
		
		TableResult result = factory.newInstance(TableResult.class);
		
		opScope.addBreadcrumbLink("admin/audit/logs", "logs");
		
		final ValuesManager values = new ValuesManager();
		JpqlBuilderDriver driver = new JpqlBuilderDriver(values, "x");
		FilterBuilder fb = factory.newInstance(FilterBuilder.class);
		fb.init(AuditEntity.class);
		final QueryFilter<String> qf = fb.getFilter(result, driver, values);
		
		result.setTableData(AuditEntity.class, new TableResult.PaginationCount() {
			@Override
			public Long getCount() {
				return new JpqlQueryFiller<Long>(values)
						.fillQueryParams(em.createQuery("select count(x) from AuditEntity x" + JpqlBuilderDriver.getCondition(qf, " where "), Long.class))
						.getSingleResult();
			}
		}, new TableResult.PaginationData<AuditEntity>() {
			@Override
			public Collection<AuditEntity> getData(Range<Long> bound) {
				return new JpqlQueryFiller<AuditEntity>(values).fillQueryParams(em.createQuery("select x from AuditEntity x" + JpqlBuilderDriver.getCondition(qf, " where ") + " order by x.logTime desc", AuditEntity.class))
						.getResultList();
			}
		}, request);
		
		result.addRowOperation("admin/audit/logDetails", "details");
		result.addTableOperation("admin/audit/logs?fw:type=EXCEL", "excel");
		
		return result;
		
	}
	
	@Operation("logDetails")
	public TableResult showLogDetails(@Param("id") Long id) {
		
		TableResult result = factory.newInstance(TableResult.class);
		result.setParentOperation("admin/audit/logs");
		
		AuditEntity entity = em.find(AuditEntity.class, id);
		
		Gson gson = JsonUtils.getBuilder().create();
		Map<String, String[]> details = gson.fromJson(entity.getJsonData(), new TypeToken<Map<String, String[]>>(){}.getType());
		
		Map<String, String> parsed = new HashMap<String, String>();
		
		for (String k : details.keySet()) {
			String[] sa = details.get(k);
			if (sa.length == 1) {
				parsed.put(k, sa[0]);
			}
			else {
				StringBuilder builder = new StringBuilder();
				for (String s : sa) {
					builder.append(", ");
					builder.append(s);
				}
				parsed.put(k, "[ " + builder.toString().substring(2) + " ]");
			}
		}
		
		result.setTableData(parsed);
		result.setRowsPerPage(null);
		
		return result;
		
	}
	
	@Operation("mailLogs")
	@RootOperation(section = "audit", iconUrl = "list", title = "mailLogs")
	public TableResult mailLogs(HttpServletRequest request, OperationScope opScope) {
		
		TableResult result = factory.newInstance(TableResult.class);
		
		opScope.addBreadcrumbLink("admin/audit/mailLogs", "mailLogs");
		
		final ValuesManager values = new ValuesManager();
		JpqlBuilderDriver driver = new JpqlBuilderDriver(values, "x");
		FilterBuilder fb = factory.newInstance(FilterBuilder.class);
		fb.init(SentMailEntity.class);
		final QueryFilter<String> qf = fb.getFilter(result, driver, values);
		
		result.setTableData(SentMailEntity.class, new TableResult.PaginationCount() {
			@Override
			public Long getCount() {
				return new JpqlQueryFiller<Long>(values).fillQueryParams(em.createQuery("select count(x) from SentMailEntity x" + JpqlBuilderDriver.getCondition(qf, " where "), Long.class))
						.getSingleResult();
			}
		}, new TableResult.PaginationData<SentMailEntity>() {
			@Override
			public Collection<SentMailEntity> getData(Range<Long> bound) {
				return new JpqlQueryFiller<SentMailEntity>(values).fillQueryParams(em.createQuery("select x from SentMailEntity x" + JpqlBuilderDriver.getCondition(qf, " where ") + " order by x.date desc", SentMailEntity.class))
						.setFirstResult(bound.getFrom().intValue())
						.setMaxResults(bound.getTo().intValue() - bound.getFrom().intValue() + 1)
						.getResultList();
			}
		}, request);
		
		result.addTableOperation("admin/audit/mailLogs?fw:type=EXCEL", "excel");
		result.addRowOperation("admin/audit/downloadMail", "download");
		
		return result;
		
	}
	
	@Operation("downloadMail")
	public StreamResult downloadMail(@Param("id") Long id) {
		
		StreamResult result = factory.newInstance(StreamResult.class);
		
		try {
			
			em.getTransaction().begin();
			
			SentMailEntity mail = em.find(SentMailEntity.class, id);
			SentMailBodyEntity body = em.find(SentMailBodyEntity.class, id);

			result.setName(mail.getSubject() + ".eml");
			result.setAttachment(true);
			result.setContentLength((long) body.getMessage().length());
			result.setContentType("message/rfc822");
			result.setStream(new ByteArrayInputStream(body.getMessage().getBytes(Charset.forName("US-ASCII"))));
			
			em.getTransaction().commit();
			
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}
		
		return result;
		
	}
	
	@Operation("exceptions/")
	@RootOperation(section = "audit", iconUrl = "bug", title = "exceptions")
	public TableResult exceptionsLog(HttpServletRequest request, OperationScope opScope) {
		
		TableResult result = factory.newInstance(TableResult.class);
		
		opScope.addBreadcrumbLink("admin/audit/exceptions/", "exceptions");
		
		final ValuesManager values = new ValuesManager();
		JpqlBuilderDriver driver = new JpqlBuilderDriver(values, "x");
		FilterBuilder fb = factory.newInstance(FilterBuilder.class);
		fb.init(ExceptionEntity.class);
		final QueryFilter<String> qf = fb.getFilter(result, driver, values);
		
		result.setTableData(ExceptionEntity.class, new TableResult.PaginationCount() {
			@Override
			public Long getCount() {
				TypedQuery<Long> query = em.createQuery("select count(x) from ExceptionEntity x" + JpqlBuilderDriver.getCondition(qf, " where "), Long.class);
				new JpqlQueryFiller<Long>(values).fillQueryParams(query);
				return query.getSingleResult();
			}
		}, new TableResult.PaginationData<ExceptionEntity>() {
			@Override
			public Collection<ExceptionEntity> getData(Range<Long> bound) {
				TypedQuery<ExceptionEntity> query = em.createQuery("select x from ExceptionEntity x" + JpqlBuilderDriver.getCondition(qf, " where ") + " order by x.errorDate desc", ExceptionEntity.class)
						.setFirstResult(bound.getFrom().intValue())
						.setMaxResults(bound.getTo().intValue() - bound.getFrom().intValue() + 1);
				new JpqlQueryFiller<ExceptionEntity>(values).fillQueryParams(query);
				return query.getResultList();
			}
		}, request);
		
		result.addRowOperation("admin/audit/exceptions/read", "stacktrace");
		
		return result;
		
	}
	
	@Operation("exceptions/read")
	public StreamResult exceptionStackTrace(@Param("id") Long id) {
		
		StreamResult result = factory.newInstance(StreamResult.class);
		
		try {
			
			em.getTransaction().begin();
			
			ExceptionEntity exc = em.find(ExceptionEntity.class, id);
			ExceptionTraceEntity trace = em.find(ExceptionTraceEntity.class, id);

			result.setAttachment(true);
			result.setContentType("text/plain; charset=utf-8");
			result.setName(exc.getException() + ".txt");
			result.setContentLength((long) trace.getTrace().length());
			result.setStream(new ByteArrayInputStream(trace.getTrace().getBytes(Charset.forName("UTF-8"))));

			em.getTransaction().commit();
			
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}
		
		return result;
		
	}
	
}
