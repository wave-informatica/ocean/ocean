/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.audit;

import java.util.Map;

import javax.inject.Inject;

import com.google.gson.reflect.TypeToken;
import com.waveinformatica.ocean.audit.dto.AuditOperation;
import com.waveinformatica.ocean.audit.dto.ConfigElement;
import com.waveinformatica.ocean.audit.workers.OperationAuditLogWorker;
import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.Service;
import com.waveinformatica.ocean.core.controllers.IService;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.ServiceStatus;
import com.waveinformatica.ocean.core.tasks.OceanExecutorService;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.PersistedProperties;

/**
 *
 * @author Ivano
 */
@Service("AuditService")
public class AuditService implements IService {

	@Inject
	private OceanExecutorService executor;

	@Inject
	private Configuration configuration;

	@Inject
	private ObjectFactory factory;

	private Map<String, ConfigElement> auditOperations;

	public PersistedProperties getConfig() {

		return configuration.getCustomProperties("audit");

	}

	public void updateAuditOperations(Map<String, ConfigElement> auditOps) {
		this.auditOperations = auditOps;
	}

	public boolean isOperationAudited(String name) {
		ConfigElement ce = auditOperations.get(name);
		return ce != null && ce.isAudited();
	}

	public void logAuditOperation(AuditOperation op) {

		if (!getServiceStatus().isRunning()) {
			return;
		}

		OperationAuditLogWorker worker = factory.newInstance(OperationAuditLogWorker.class);
		worker.loadAuditOperation(op);
		executor.submit(worker);

	}

	@Override
	public boolean start() {

		auditOperations = JsonUtils.getBuilder().create().fromJson(getConfig().getProperty("auditOperations", "{}"), new TypeToken<Map<String, ConfigElement>>() {}.getType());

		return true;
	}

	@Override
	public boolean stop() {
		auditOperations = null;

		return true;

	}

	@Override
	public ServiceStatus getServiceStatus() {
		ServiceStatus status = new ServiceStatus();
		status.setRunning(executor != null && !executor.isShutdown() && !executor.isTerminated());
		return status;
	}

}
