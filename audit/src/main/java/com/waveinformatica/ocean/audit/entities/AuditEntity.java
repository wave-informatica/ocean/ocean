/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.audit.entities;

import com.waveinformatica.ocean.core.annotations.Filterable;
import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.core.filtering.DayFilter;
import com.waveinformatica.ocean.core.filtering.NumericFilter;
import com.waveinformatica.ocean.core.filtering.StringFilter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Ivano
 */
@Entity
@Table(name = "audit_log")
public class AuditEntity implements Serializable {
    
    @Id
    @GeneratedValue(generator = "seq_audit_log", strategy = GenerationType.AUTO)
    @SequenceGenerator(name = "seq_audit_log", sequenceName = "seq_audit_log", allocationSize = 1)
    @Column
    @TableResult.TableResultColumn(hidden = true, key = true)
    private Long id;
    
    @Column(name = "log_time")
    @Temporal(TemporalType.TIMESTAMP)
    @Filterable(filter = DayFilter.class)
    private Date logTime;
    
    @Column
    @Enumerated(EnumType.STRING)
    private AuditActionType type;
    
    @Column(name = "description")
    @Filterable(filter = StringFilter.class)
    private String actionDescription;
    
    @Column(name = "json_data", length = 40000)
    @TableResult.TableResultColumn(ignore = true)
    private String jsonData;
    
    @Column(name = "user_id")
    @Filterable(filter = NumericFilter.class)
    private Long userId;
    
    @Column(name = "user_name")
    @Filterable(filter = StringFilter.class)
    private String userName;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Date getLogTime() {
	return logTime;
    }

    public void setLogTime(Date logTime) {
	this.logTime = logTime;
    }

    public AuditActionType getType() {
	return type;
    }

    public void setType(AuditActionType type) {
	this.type = type;
    }

    public String getActionDescription() {
	return actionDescription;
    }

    public void setActionDescription(String actionDescription) {
	this.actionDescription = actionDescription;
    }

    public String getJsonData() {
	return jsonData;
    }

    public void setJsonData(String jsonData) {
	this.jsonData = jsonData;
    }

    public Long getUserId() {
	return userId;
    }

    public void setUserId(Long userId) {
	this.userId = userId;
    }

    public String getUserName() {
	return userName;
    }

    public void setUserName(String userName) {
	this.userName = userName;
    }
    
}
