var MessagingManager = null;

(function () {
    
    var unsentMessages = [];
    var registeredResources = [];
    
    var wsUri = location.href;
    var base = $('base').attr('href');
    
    wsUri = 'ws' + wsUri.substring(4, wsUri.indexOf(base, wsUri.indexOf('//') + 2) + base.length);
    
    var webSocket = null;

    var MessagingManagerCls = function (ws) {
        
        var me = this;
        
        this.wsUri = ws.url;
        
        this._initConnection(ws);
        
        this.listeners = {};
        this.statusListeners = [];

    };
    
    MessagingManagerCls.prototype._initConnection = function (ws) {
        
        var me = this;
        
        this.ws = ws || new WebSocket(this.wsUri);
        
        this.ws.onopen = function() { MessagingManagerCls.prototype.messaging_wsOnOpen.apply(me, arguments); };
        this.ws.onclose = function() { MessagingManagerCls.prototype.messaging_wsOnClose.apply(me, arguments); };
        this.ws.onmessage = function() { MessagingManagerCls.prototype.messaging_wsOnMessage.apply(me, arguments); };
        this.ws.onerror = function() { MessagingManagerCls.prototype.messaging_wsOnError.apply(me, arguments); };
        
    };

    MessagingManagerCls.prototype.messaging_wsOnOpen = function(evt) {
        
        while (unsentMessages.length > 0) {
            var msg = unsentMessages.splice(0, 1);
            this.send(msg[0].msg, msg[0].params);
        }
        
        var len = registeredResources.length;
        for (var i = 0; i < len; i++) {
            var reg = registeredResources[i];
            this.register(reg.type, reg.callback, reg.scope);
        }
        
        this._fireStatusChange();
    };

    MessagingManagerCls.prototype.messaging_wsOnClose = function(evt) {
        this._fireStatusChange();
        this._initConnection();
    };

    MessagingManagerCls.prototype.messaging_wsOnMessage = function(evt) {
        var msg = JSON.parse(evt.data);
        var listeners = this.listeners[msg.resource.managerId];
        if (listeners) {
            for (var i = 0; i < listeners.callbacks.length; i++) {
                listeners.callbacks[i].callback.call(listeners.callbacks[i].scope || window, msg);
            }
        }
        listeners = this.listeners[msg.resource.managerId + ':' + msg.resource.resourceId];
        if (listeners) {
            for (var i = 0; i < listeners.callbacks.length; i++) {
                listeners.callbacks[i].callback.call(listeners.callbacks[i].scope || window, msg);
            }
        }
    };

    MessagingManagerCls.prototype.messaging_wsOnError = function(evt) {
        if (console && typeof console.log === 'function') {
            console.log('Errore sul websocket');
            console.log(evt);
        }
        this._fireStatusChange();
    };
    
    MessagingManagerCls.prototype._fireStatusChange = function () {
        for (var i = 0; i < this.statusListeners.length; i++) {
            var cb = this.statusListeners[i];
            try {
                cb.callback.call(cb.scope, this.ws.readyState);
            } catch (e) {
                
            }
        }
    };
    
    MessagingManagerCls.prototype.addListener = function (messageType, callback, scope) {
        var listener = this.listeners[messageType];
        if (typeof listener === "undefined") {
            listener = {
                messageType: messageType,
                callbacks: []
            };
            this.listeners[messageType] = listener;
        }
        listener.callbacks.push({
            callback: callback,
            scope: scope
        });
    };
    
    MessagingManagerCls.prototype.addStatusListener = function (callback, scope) {
        var opts = {
            callback: callback,
            scope: scope || window
        };
        this.statusListeners.push(opts);
        opts.callback.call(opts.scope, this.ws.readyState);
    };
    
    MessagingManagerCls.prototype.send = function (msg, params) {
        
        if (this.ws.readyState != this.ws.OPEN) {
            unsentMessages.push({
                msg: msg,
                params: params
            });
            return;
        }
        
        if (typeof msg === "string") {
            var strParams = '';
            if (typeof params === "object") {
                strParams = JSON.stringify(params);
                this.ws.send(msg + ':' + strParams);
            }
            else {
                this.ws.send(msg);
            }
        }
        else {
            var str = JSON.stringify(msg);
            this.ws.send(str);
        }
    };
    
    MessagingManagerCls.prototype.register = function (messageType, callback, scope) {
        var alreadyRegistered = false;
        for (var i = 0; i < registeredResources.length; i++) {
            var reg = registeredResources[i];
            if (reg.type === messageType && reg.callback == callback && reg.scope == scope) {
                alreadyRegistered = true;
                break;
            }
        }
        if (!alreadyRegistered) {
            registeredResources.push({
                type: messageType,
                callback: callback,
                scope: scope
            });
        }
        this.addListener(messageType, callback, scope);
        this.send('/ocean-msg/register', {
            res: [ messageType ]
        });
    };
    
    MessagingManagerCls.prototype.unregister = function (messageType, callback, scope) {
        this.send('/ocean-msg/unregister', {
            res: [ messageType ]
        });
        delete this.listeners[messageType];
        for (var i = 0; i < registeredResources.length; i++) {
            var reg = registeredResources[i];
            if (reg.type === messageType && (!callback || (reg.callback == callback && reg.scope == scope))) {
                registeredResources.splice(i, 1);
            }
        }
    };
    
    /*var pos = 5;
    while (true) {
        pos = wsUri.indexOf('/', pos + 1);
        if (pos < 0) {
            break;
        }*/
        try {
            var tmpWsUri = wsUri + 'ws/ocean';
            webSocket = new WebSocket(tmpWsUri);
            //break;
        } catch (e) {
            webSocket = null;
            //continue;
        }
    //}
    
    if (webSocket !== null) {
        
        MessagingManager = new MessagingManagerCls(webSocket);
        
    }
    
})();