/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.messaging.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Ivano
 */
public class OceanMessage implements Serializable {
	
	private Long id;
	private Resource resource;
	private UserDef senderUser;
	private List<UserDef> receiverUsers;
	private Object payload;
	private String payloadModule;
	private String payloadType;
	private Date sentTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public UserDef getSenderUser() {
		return senderUser;
	}

	public void setSenderUser(UserDef senderUser) {
		this.senderUser = senderUser;
	}

	public List<UserDef> getReceiverUsers() {
		return receiverUsers;
	}

	public void setReceiverUsers(List<UserDef> receiverUsers) {
		this.receiverUsers = receiverUsers;
	}

	public Object getPayload() {
		return payload;
	}

	public void setPayload(Object payload) {
		this.payload = payload;
	}

	public String getPayloadModule() {
		return payloadModule;
	}

	public void setPayloadModule(String payloadModule) {
		this.payloadModule = payloadModule;
	}

	public String getPayloadType() {
		return payloadType;
	}

	public void setPayloadType(String payloadType) {
		this.payloadType = payloadType;
	}

	public Date getSentTime() {
		return sentTime;
	}

	public void setSentTime(Date sentTime) {
		this.sentTime = sentTime;
	}
	
}
