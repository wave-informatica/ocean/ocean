/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.messaging.events;

import com.waveinformatica.ocean.core.controllers.events.Event;
import com.waveinformatica.ocean.core.features.websocket.WebSocketSession;
import com.waveinformatica.ocean.messaging.dto.Resource;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
public class ResourceRegistrationEvent extends Event {
	
	public static final String NAME = "SYNC_RESOURCE_REGISTRATION";
	
	private final boolean registered;
	private final Resource resource;
	private final WebSocketSession session;
	
	public ResourceRegistrationEvent(Object source, String managerId, boolean registered, Resource resource, WebSocketSession session) {
		super(source, NAME, StringUtils.isNotBlank(managerId) ? new String[]{ managerId } : new String[]{ });
		this.registered = registered;
		this.resource = resource;
		this.session = session;
	}

	public boolean isRegistered() {
		return registered;
	}

	public Resource getResource() {
		return resource;
	}

	public WebSocketSession getSession() {
		return session;
	}
	
}
