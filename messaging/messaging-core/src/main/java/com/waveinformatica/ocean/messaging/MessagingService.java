/*
 * Copyright 2016, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.messaging;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.codec.binary.Base64;

import com.google.gson.Gson;
import com.waveinformatica.ocean.core.annotations.Preferred;
import com.waveinformatica.ocean.core.annotations.Service;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.IService;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.ServiceStatus;
import com.waveinformatica.ocean.core.exceptions.OperationNotAuthorizedException;
import com.waveinformatica.ocean.core.features.websocket.WebSocketSession;
import com.waveinformatica.ocean.core.modules.ComponentsBin;
import com.waveinformatica.ocean.core.modules.ModuleClassLoader;
import com.waveinformatica.ocean.core.security.OceanUser;
import com.waveinformatica.ocean.core.security.SecurityManager;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.messaging.annotations.ResourceManager;
import com.waveinformatica.ocean.messaging.dto.OceanMessage;
import com.waveinformatica.ocean.messaging.dto.Resource;
import com.waveinformatica.ocean.messaging.dto.UserDef;
import com.waveinformatica.ocean.messaging.events.ResourceRegistrationEvent;
import com.waveinformatica.ocean.messaging.events.UserSessionEvent;

/**
 *
 * @author Ivano
 */
@Service("MessagingService")
public class MessagingService implements IService, ComponentsBin.ComponentLoaded, ComponentsBin.ComponentUnloaded {
	
	private static final SecureRandom random = new SecureRandom();
	
	private boolean started = false;
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private CoreController controller;
	
	@Inject @Preferred
	@ResourceManager("")
	private ComponentsBin components;
	
	private final Map<String, Class<? extends IResourceManager>> resourceManagers = new HashMap<String, Class<? extends IResourceManager>>();
	private final Map<Long, List<WebSocketSession>> userSessions = new HashMap<Long, List<WebSocketSession>>();
	private final Map<String, List<WebSocketSession>> registeredSessions = new HashMap<String, List<WebSocketSession>>();
	private final Map<String, IMessagingChannelManager> channelManagers = new HashMap<String, IMessagingChannelManager>();
	
	@PostConstruct
	public void init() {
		components.register(this);
	}
	
	public synchronized void send(String type, Object payload) {
		
		if (payload == null) {
			throw new NullPointerException("Payload is null");
		}
		
		Support support = factory.newInstance(Support.class);
		
		// Message construction
		OceanMessage msg = new OceanMessage();
		if (support.getSecurityManager().getPrincipal() != null) {
			msg.setSenderUser(new UserDef(support.getSecurityManager().getPrincipal()));
		}
		msg.setSentTime(new Date());
		msg.setResource(new Resource(type));
		
		Class<? extends IResourceManager> manager = resourceManagers.get(msg.getResource().getManagerId());
		if (manager == null) {
			Set<Class<?>> set = components.getComponents(ResourceManager.class);
			for (Class<?> c : set) {
				ResourceManager rm = c.getAnnotation(ResourceManager.class);
				if (rm.value().equals(msg.getResource().getManagerId())) {
					manager = (Class<? extends IResourceManager>) c;
					resourceManagers.put(msg.getResource().getManagerId(), manager);
					break;
				}
			}
		}
		
		List<WebSocketSession> sessions = new ArrayList<WebSocketSession>();

		List<UserDef> destination = new ArrayList<UserDef>();
		
		if (manager != null) {
			IResourceManager managerInst = factory.newInstance(manager);
			destination.addAll(managerInst.getDestinationUsers(msg.getResource()));
		}

		// Find websockets to send message
		if (!destination.isEmpty()) {
			Long[] userIds = new Long[destination.size()];
			int i = 0;
			for (UserDef usr : destination) {
				userIds[i++] = usr.getUserId();
			}
			sessions.addAll(getUserSessions(userIds));
		}

		List<WebSocketSession> regSessions = registeredSessions.get(type);
		if (regSessions != null) {
			sessions.addAll(regSessions);
		}
		
		if (sessions.isEmpty()) {
			return;
		}
		
		// Build payload
		msg.setPayloadType(payload.getClass().getName());
		msg.setPayload(payload);
		ClassLoader cl = payload.getClass().getClassLoader();
		if (cl instanceof ModuleClassLoader) {
			msg.setPayloadModule(((ModuleClassLoader) cl).getModule().getName());
		}
		
		// Build JSON message
		Gson gson = JsonUtils.getBuilder().create();
		String jsonMsg = gson.toJson(msg);
		
		List<String> partials = new ArrayList<>(jsonMsg.length() / 125 + 1);
		for (int i = 0; i < jsonMsg.length(); i += 125) {
			partials.add(jsonMsg.substring(i, Math.min(i + 125, jsonMsg.length())));
		}
		
		// Send message to users
		for (int i = 0; i < partials.size(); i++) {
			for (WebSocketSession wss : sessions) {
				if (wss.getSession().isOpen()) {
					try {
						wss.getSession().getBasicRemote().sendText(partials.get(i), i == partials.size() - 1);
						//wss.getSession().getBasicRemote().sendText(jsonMsg);
						wss.setLastActivity(new Date());
					} catch (IllegalStateException e) {
						LoggerUtils.getLogger(MessagingService.class).log(Level.WARNING, null, e);
					} catch (IOException e) {
						LoggerUtils.getLogger(MessagingService.class).log(Level.WARNING, null, e);
					}
				}
			}
		}
		
	}
	
	public synchronized void userConnected(OceanUser user, WebSocketSession session) {
		if (user == null) {
			return;
		}
		List<WebSocketSession> lista = userSessions.get(user.getId());
		if (lista == null) {
			lista = new LinkedList<WebSocketSession>();
			userSessions.put(user.getId(), lista);
		}
		lista.add(session);
		
		UserSessionEvent event = new UserSessionEvent(this, session, true);
		controller.dispatchEvent(event);
	}
	
	public synchronized void userDisconnected(OceanUser user, WebSocketSession session) {
		if (user != null) {
			List<WebSocketSession> lista = userSessions.get(user.getId());
			if (lista == null) {
				return;
			}
			Iterator<WebSocketSession> iter = lista.iterator();
			while (iter.hasNext()) {
				WebSocketSession wss = iter.next();
				if (wss.getId().equals(session.getId())) {
					iter.remove();
					break;
				}
			}
			if (lista.isEmpty()) {
				userSessions.remove(user.getId());
			}
		}
		// Cleaning registeredResources
		Iterator<Entry<String, List<WebSocketSession>>> iter2 = registeredSessions.entrySet().iterator();
		while (iter2.hasNext()) {
			Entry<String, List<WebSocketSession>> lista2 = iter2.next();
			Iterator<WebSocketSession> iter = lista2.getValue().iterator();
			while (iter.hasNext()) {
				WebSocketSession wss = iter.next();
				if (wss.getId().equals(session.getId())) {
					
					Resource resource = new Resource(lista2.getKey());
					ResourceRegistrationEvent event = new ResourceRegistrationEvent(this, resource.getManagerId(), false, resource, wss);
					controller.dispatchEvent(event);
					
					iter.remove();
					
				}
			}
			if (lista2.getValue().isEmpty()) {
				iter2.remove();
			}
		}
		
		UserSessionEvent event = new UserSessionEvent(this, session, false);
		controller.dispatchEvent(event);
	}
	
	public synchronized List<WebSocketSession> getUserSessions(Long[] userIds) {
		
		List<WebSocketSession> sessions = new ArrayList<WebSocketSession>();
		
		for (Long id : userIds) {
			List<WebSocketSession> tmp = userSessions.get(id);
			if (tmp != null) {
				sessions.addAll(tmp);
			}
		}
		
		return sessions;
		
	}
	
	public synchronized Map<Long, Date> getConnectedUserIds() {
		
		Map<Long, Date> ids =  new HashMap<>();
		for (Long l : userSessions.keySet()) {
			Date lastActivity = null;
			List<WebSocketSession> sessions = userSessions.get(l);
			Iterator<WebSocketSession> wssIter = sessions.iterator();
			while (wssIter.hasNext()) {
				WebSocketSession wss = wssIter.next();
				if (!wss.getSession().isOpen()) {
					wssIter.remove();
				}
				else if (lastActivity == null || wss.getLastActivity().after(lastActivity)) {
					lastActivity = wss.getLastActivity();
				}
			}
			if (!sessions.isEmpty()) {
				ids.put(l, lastActivity);
			}
		}
		
		return ids;
		
	}
	
	public synchronized void register(Resource resource, WebSocketSession ws) {
		
		Class<? extends IResourceManager> manager = resourceManagers.get(resource.getManagerId());
		if (manager == null) {
			Set<Class<?>> set = components.getComponents(ResourceManager.class);
			for (Class<?> c : set) {
				ResourceManager rm = c.getAnnotation(ResourceManager.class);
				if (rm.value().equals(resource.getManagerId())) {
					manager = (Class<? extends IResourceManager>) c;
					resourceManagers.put(resource.getManagerId(), manager);
					break;
				}
			}
		}
		if (manager != null) {
			IResourceManager man = factory.newInstance(manager);
			Boolean canRegister = man.canRegister(resource);
			if (canRegister != null && !canRegister) {
				throw new OperationNotAuthorizedException();
			}
		}
		
		List<WebSocketSession> registered = registeredSessions.get(resource.toString());
		if (registered == null) {
			registered = new ArrayList<WebSocketSession>();
			registeredSessions.put(resource.toString(), registered);
		}
		else {
			for (WebSocketSession wss : registered) {
				if (wss.getId().equals(ws.getId())) {

					ResourceRegistrationEvent event = new ResourceRegistrationEvent(this, resource.getManagerId(), true, resource, ws);
					controller.dispatchEvent(event);
					
					return;
					
				}
			}
		}
		registered.add(ws);
		
		ResourceRegistrationEvent event = new ResourceRegistrationEvent(this, resource.getManagerId(), true, resource, ws);
		controller.dispatchEvent(event);
	}
	
	public synchronized void unregister(Resource resource, WebSocketSession ws) {
		List<WebSocketSession> registered = registeredSessions.get(resource.toString());
		if (registered != null) {
			Iterator<WebSocketSession> iter = registered.iterator();
			while (iter.hasNext()) {
				WebSocketSession wss = iter.next();
				if (wss.getId().equals(ws.getId())) {
					iter.remove();
					break;
				}
			}
		}
		
		ResourceRegistrationEvent event = new ResourceRegistrationEvent(this, resource.getManagerId(), false, resource, ws);
		controller.dispatchEvent(event);
	}
	
	public synchronized String createChannel(IMessagingChannelManager channelManager) {
		
		if (channelManager == null) {
			throw new NullPointerException("An IMessagingChannelManager instance is required to create a messaging channel");
		}
		
		byte[] bytes = new byte[30];
		random.nextBytes(bytes);
		
		String name = Base64.encodeBase64String(bytes);
		
		channelManagers.put(name, channelManager);
		
		return name;
		
	}
	
	public synchronized void unregisterChannel(IMessagingChannelManager channelManager) {
		Iterator<IMessagingChannelManager> iter = channelManagers.values().iterator();
		while (iter.hasNext()) {
			if (channelManager == iter.next()) {
				iter.remove();
				break;
			}
		}
	}
	
	public synchronized IMessagingChannelManager getChannelManager(String name) {
		
		return channelManagers.get(name);
		
	}
	
	@Override
	public boolean start() {
		
		started = true;
		
		return started;
		
	}

	@Override
	public boolean stop() {
		
		started = false;
		
		return !started;
		
	}

	@Override
	public ServiceStatus getServiceStatus() {
		ServiceStatus status = new ServiceStatus();
		status.setRunning(started);
		return status;
	}

	@Override
	public void componentLoaded(Class<? extends Annotation> annotation, Class<?> componentClass) {
		if (annotation == ResourceManager.class) {
			Class<? extends IResourceManager> c = (Class<? extends IResourceManager>) componentClass;
			ResourceManager ann = c.getAnnotation(ResourceManager.class);
			resourceManagers.put(ann.value(), c);
		}
	}

	@Override
	public void componentUnloaded(Class<? extends Annotation> annotation, Class<?> componentClass) {
		if (annotation == ResourceManager.class) {
			Class<? extends IResourceManager> c = (Class<? extends IResourceManager>) componentClass;
			ResourceManager ann = c.getAnnotation(ResourceManager.class);
			resourceManagers.remove(ann.value());
		}
	}
	
	public static class Support {
		
		@Inject
		private com.waveinformatica.ocean.core.security.SecurityManager securityManager;

		public SecurityManager getSecurityManager() {
			return securityManager;
		}
		
	}
	
}
