/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.messaging;

import com.waveinformatica.ocean.core.features.websocket.WebSocketChannelManager;
import com.waveinformatica.ocean.core.features.websocket.WebSocketSession;

/**
 *
 * @author Ivano
 */
public interface IMessagingChannelManager extends WebSocketChannelManager {
	
	public void attachedSession(WebSocketSession session);
	
	public void detachedSession(WebSocketSession session);
	
}
