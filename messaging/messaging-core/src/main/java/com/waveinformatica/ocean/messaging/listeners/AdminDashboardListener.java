/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.messaging.listeners;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.DashboardEvent;
import com.waveinformatica.ocean.core.controllers.results.DashboardResult.Counter;
import com.waveinformatica.ocean.messaging.MessagingService;

@CoreEventListener(eventNames = CoreEventName.DASHBOARD, flags = "/admin/")
public class AdminDashboardListener implements IEventListener<DashboardEvent> {
	
	@Inject
	private CoreController controller;
	
	@Override
	public void performAction(DashboardEvent event) {
		
		MessagingService service = controller.getService(MessagingService.class);
		
		// Online users
		Counter onlineUsers = event.getDashboard().addCounter("users", "onlineUsers");
		onlineUsers.setValue(String.valueOf(service.getConnectedUserIds().size()));
		
	}

}
