/*******************************************************************************
 * Copyright 2015, 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.messaging;

import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.exceptions.OperationNotAuthorizedException;
import com.waveinformatica.ocean.core.features.websocket.WebSocketSession;
import com.waveinformatica.ocean.messaging.dto.Resource;
import javax.inject.Inject;

@OperationProvider(namespace = "ocean-msg")
public class Operations {
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private CoreController controller;
	
	@Operation("register")
	public void register(@Param("res") String resource, WebSocketSession session) {
		
		if (session == null) {
			throw new OperationNotAuthorizedException();
		}
		
		MessagingService service = controller.getService(MessagingService.class);
		
		service.register(new Resource(resource), session);
		
	}
	
	@Operation("unregister")
	public void unregister(@Param("res") String resource, WebSocketSession session) {
		
		if (session == null) {
			throw new OperationNotAuthorizedException();
		}
		
		MessagingService service = controller.getService(MessagingService.class);
		
		service.unregister(new Resource(resource), session);
		
	}
	
	@Operation("attachChannel")
	public void attachChannel(@Param("channel") String channelName, WebSocketSession session) {
		
		if (session.getChannelManager() != null) {
			
			session.setChannelManager(null);
			
			if (session.getChannelManager() instanceof IMessagingChannelManager) {
				
				IMessagingChannelManager manager = (IMessagingChannelManager) session.getChannelManager();

				manager.detachedSession(session);
				
			}
			
		}
		
		MessagingService service = controller.getService(MessagingService.class);
		
		IMessagingChannelManager manager = service.getChannelManager(channelName);
		
		session.setChannelManager(manager);
		
		manager.attachedSession(session);
		
	}
	
	@Operation("detachChannel")
	public void detachChannel(WebSocketSession session) {
		
		if (session.getChannelManager() == null) {
			return;
		}
		
		session.setChannelManager(null);
		
		if (session.getChannelManager() instanceof IMessagingChannelManager) {
			
			IMessagingChannelManager manager = (IMessagingChannelManager) session.getChannelManager();

			manager.detachedSession(session);
			
		}
		
	}
	
}