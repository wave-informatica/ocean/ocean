/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.messaging.listeners;

import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.events.BeforeResultEvent;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.Event;
import com.waveinformatica.ocean.core.util.Results;

/**
 *
 * @author Ivano
 */
@CoreEventListener(
	  eventNames = CoreEventName.BEFORE_RESULT,
	  flags = {
		  "com.waveinformatica.ocean.core.controllers.results.BaseResult"
	  }
)
public class WebPageFiller implements IEventListener {

	@Override
	public void performAction(Event event) {
		
		BeforeResultEvent evt = (BeforeResultEvent) event;
		
		Results.addScriptSource(evt.getResult(), "ris/js/messaging-core.js");
		
	}
	
}
