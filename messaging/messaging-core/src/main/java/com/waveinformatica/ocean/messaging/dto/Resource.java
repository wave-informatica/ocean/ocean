/*
 * Copyright 2016, 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.messaging.dto;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
public class Resource {
	
	private final String managerId;
	private final String resourceId;

	public Resource(String resourceName) {
		
		int pos = resourceName.indexOf(':');
		this.managerId = (pos >= 0 ? resourceName.substring(0, pos) : resourceName);
		this.resourceId = (pos >= 0 ? resourceName.substring(pos + 1) : null);
		
	}

	public String getManagerId() {
		return managerId;
	}

	public String getResourceId() {
		return resourceId;
	}

	@Override
	public String toString() {
		if (resourceId != null) {
			return managerId + ":" + resourceId;
		}
		else {
			return managerId;
		}
	}
	
}
