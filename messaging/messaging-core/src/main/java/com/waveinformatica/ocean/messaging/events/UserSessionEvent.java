/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.messaging.events;

import com.waveinformatica.ocean.core.controllers.events.Event;
import com.waveinformatica.ocean.core.features.websocket.WebSocketSession;

/**
 *
 * @author Ivano
 */
public class UserSessionEvent extends Event {

	public static final String NAME = "SYNC_USER_SESSION_EVENT";
	
	private final WebSocketSession session;
	private final boolean connected;
	
	public UserSessionEvent(Object source, WebSocketSession session, boolean connected) {
		super(source, NAME);
		this.session = session;
		this.connected = connected;
	}

	public WebSocketSession getSession() {
		return session;
	}

	public boolean isConnected() {
		return connected;
	}
	
}
