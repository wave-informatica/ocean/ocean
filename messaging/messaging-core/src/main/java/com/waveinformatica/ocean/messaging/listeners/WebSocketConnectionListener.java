/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.messaging.listeners;

import com.waveinformatica.ocean.core.annotations.EventListener;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.events.Event;
import com.waveinformatica.ocean.core.features.websocket.WebSocketChannelManager;
import com.waveinformatica.ocean.core.features.websocket.WebSocketClosedEvent;
import com.waveinformatica.ocean.core.features.websocket.WebSocketConnectedEvent;
import com.waveinformatica.ocean.core.security.UserSession;
import com.waveinformatica.ocean.messaging.IMessagingChannelManager;
import com.waveinformatica.ocean.messaging.MessagingService;
import javax.inject.Inject;

/**
 *
 * @author Ivano
 */
@EventListener(eventNames = "WEBSOCKET_CONNECTED")
public class WebSocketConnectionListener implements IEventListener {

	@Inject
	private CoreController controller;
	
	@Inject
	private UserSession session;
	
	@Override
	public void performAction(Event event) {
		
		if (event instanceof WebSocketConnectedEvent) {
			userConnected((WebSocketConnectedEvent) event);
		}
		else if (event instanceof WebSocketClosedEvent) {
			userDisconnected((WebSocketClosedEvent) event);
		}
		
	}
	
	public void userConnected(WebSocketConnectedEvent event) {
		
		MessagingService service = controller.getService(MessagingService.class);
		
		event.getSession().addCloseListener(this);
		
		service.userConnected(session.getPrincipal(), event.getSession());
		
	}
	
	public void userDisconnected(WebSocketClosedEvent event) {
		
		MessagingService service = controller.getService(MessagingService.class);
		
		service.userDisconnected(session.getPrincipal(), event.getClosedSession());
		
		WebSocketChannelManager mngr = event.getClosedSession().getChannelManager();
		if (mngr != null && mngr instanceof IMessagingChannelManager) {
			event.getClosedSession().setChannelManager(null);
			((IMessagingChannelManager) mngr).detachedSession(event.getClosedSession());
		}
		
	}
	
}
