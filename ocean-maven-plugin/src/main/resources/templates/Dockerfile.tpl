FROM <#if sourceRepository?has_content>${sourceRepository}/</#if>wave/ocean-server:${version}

<#if libsDir?has_content>
COPY ${libsDir}/* /opt/ocean/lib/
</#if>
COPY oceandeps/*.jar /opt/ocean/Ocean/modules/

<#if configDir?has_content>
COPY ${configDir}/*.properties /opt/ocean/Ocean/
</#if>
COPY ${moduleJar} /opt/ocean/Ocean/modules/

