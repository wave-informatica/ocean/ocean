/*
* Copyright 2017, 2018 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.maven.plugin;

import com.waveinformatica.ocean.core.i18n.Translate;
import com.waveinformatica.ocean.core.i18n.Translation;
import com.waveinformatica.ocean.maven.plugin.dto.ArgumentModel;
import com.waveinformatica.ocean.maven.plugin.dto.I18NModel;
import com.waveinformatica.ocean.maven.plugin.dto.PropertyModel;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.plugin.logging.Log;
import org.scannotation.AnnotationDB;

/**
 *
 * @author ivano
 */
public class I18NGenerator {
	
	private final File classesDirectory;
	private final File outputDirectory;
	private final Log log;
	private final AnnotationDB annotationDb;
	private final ClassLoader classLoader;
	private final List<File> generatedClasses = new ArrayList<File>();
	
	protected I18NGenerator(Log log, File classesDirectory, File outputDirectory, List<String> compilePath) {
		this.log = log;
		this.classesDirectory = classesDirectory;
		this.outputDirectory = outputDirectory;
		this.annotationDb = new AnnotationDB();
		
		ClassLoader cl = null;
		try {
			cl = new URLClassLoader(new URL[]{classesDirectory.toURI().toURL()}, I18NGenerator.class.getClassLoader());
		} catch (MalformedURLException ex) {
			Logger.getLogger(I18NGenerator.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		classLoader = cl;
		
	}
	
	protected List<File> processI18N() throws IOException {
		
		// Inspect classes from JAR file for components
		scanFile(classesDirectory);
		Set<String> translations = annotationDb
			  .getAnnotationIndex()
			  .get(Translate.class.getName());
		
		if (translations != null) {
                    log.info("Found " + translations.size() + " translation classes");
			for (String cn : translations) {
				try {
					Class<?> cl = classLoader.loadClass(cn);
					generatePropertiesFromClass(cl);
				} catch (ClassNotFoundException ex) {
					Logger.getLogger(I18NGenerator.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		
		return generatedClasses;
	}
	
	protected void generatePropertiesFromClass(Class<?> cls) throws IOException {
		
		Map<String, Properties> map = new HashMap<String, Properties>();
		
		String prefix = cls.getName();
		
		Translate tr = cls.getAnnotation(Translate.class);
		if (tr != null && StringUtils.isNotBlank(tr.prefix())) {
			prefix = tr.prefix();
		}
		
		I18NModel model = new I18NModel();
		model.setProperties(new ArrayList<PropertyModel>());
		model.setClsPackage(cls.getPackage().getName());
		model.setInterfaceName(cls.getSimpleName());
		model.setImplementation(cls.isInterface());
		
		for (Field f : cls.getDeclaredFields()) {
			Translate mtr = f.getAnnotation(Translate.class);
			if (mtr != null) {
				populate(map, f, mtr, prefix, model);
			}
		}
		
		for (Method m : cls.getDeclaredMethods()) {
			Translate mtr = m.getAnnotation(Translate.class);
			if (mtr != null) {
				populate(map, m, mtr, prefix, model);
			}
		}
		
		Set<String> keys = new TreeSet<String>();
		for (String mapK : map.keySet()) {
			keys.addAll(map.get(mapK).stringPropertyNames());
		}
		for (String mapK : map.keySet()) {
			Properties props = map.get(mapK);
			for (String k : keys) {
				if (!props.containsKey(k)) {
					props.setProperty(k, "");
				}
			}
		}
		
		for (String k : map.keySet()) {
			Properties props = new Properties();
			File source = new File(classesDirectory, "i18n/" + cls.getSimpleName() + (k.equals("en") ? "" : "_" + k) + ".properties");
			log.info("Merging file " + source.getName());
			if (source.exists()) {
				FileInputStream fis = new FileInputStream(source);
				try {
					props.load(fis);
					Properties tmp = map.get(k);
					for (String tmpK : tmp.stringPropertyNames()) {
						if (!props.containsKey(tmpK)) {
							props.setProperty(tmpK, tmp.getProperty(tmpK));
						}
					}
				} finally {
					fis.close();
				}
			} else {
				props = map.get(k);
			}
			File dest = new File(outputDirectory, source.getName());
			FileOutputStream fos = new FileOutputStream(dest);
			try {
				props.store(fos, "");
			} finally {
				fos.close();
			}
			dest = new File(classesDirectory, "i18n/" + source.getName());
			fos = new FileOutputStream(dest);
			try {
				props.store(fos, "");
			} finally {
				fos.close();
			}
		}
		
		generatedClasses.add(produceClass(model));
		
	}
	
	protected void populate(Map<String, Properties> map, Member m, Translate tr, String prefix, I18NModel model) {
		
		if (StringUtils.isBlank(prefix)) {
			prefix = "";
		} else {
			prefix += ".";
		}
		
		if (StringUtils.isNotBlank(tr.prefix())) {
			prefix = tr.prefix() + ".";
		}
		
		for (Translation t : tr.value()) {
			
			String lang = t.lang();
			if (StringUtils.isBlank(lang)) {
				lang = "en";
			}
			
			Properties props = map.get(lang);
			if (props == null) {
				props = new Properties();
				map.put(lang, props);
			}
			
			props.setProperty(prefix + m.getName(), t.value());
			
			boolean found = false;
			for (PropertyModel pm : model.getProperties()) {
				if (pm.getName().equals(m.getName())) {
					found = true;
					break;
				}
			}
			if (!found) {
				PropertyModel pm = new PropertyModel();
				pm.setPrefix(prefix);
				pm.setName(m.getName());
				pm.setArguments(new ArrayList<ArgumentModel>(m instanceof Method ? ((Method) m).getParameterTypes().length : 0));
				if (m instanceof Method) {
					int parIndex = 0;
					for (Class<?> p : ((Method)m).getParameterTypes()) {
						ArgumentModel arg = new ArgumentModel();
						arg.setType(p);
						arg.setName("arg" + (parIndex++));
						pm.getArguments().add(arg);
					}
				}
				model.getProperties().add(pm);
			}
		}
		
	}
	
	private void scanFile(File base) throws IOException {
		
		if (base.isDirectory()) {
			for (File f : base.listFiles()) {
				scanFile(f);
			}
		} else if (base.isFile() && base.getName().endsWith(".class")) {
			FileInputStream fis = new FileInputStream(base);
			try {
				annotationDb.scanClass(fis);
                        } catch (IOException e) {
                            log.warn("Unable to parse class " + base.getAbsolutePath(), e);
			} finally {
				fis.close();
			}
		}
		
	}
	
	private File produceClass(I18NModel model) throws IOException {
		
		// Producing class
		StringWriter outputWriterBuffer = new StringWriter();
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);
		cfg.setTemplateLoader(new ClassTemplateLoader(I18NGenerator.class.getClassLoader(), ""));
		Template tpl = cfg.getTemplate("/templates/I18NInterfaceImpl.jtpl");
		try {
			tpl.process(model, outputWriterBuffer);
		} catch (TemplateException ex) {
			throw new IOException(ex);
		}
		
		File generatedSource = new File(outputDirectory.getParentFile(), model.getClsPackage().replace('.', '/') + "/" + model.getInterfaceName() + "Impl.java");
		if (!generatedSource.getParentFile().exists()) {
			generatedSource.getParentFile().mkdirs();
		}
		FileOutputStream fos = new FileOutputStream(generatedSource);
		fos.write(outputWriterBuffer.toString().getBytes(Charset.forName("UTF-8")));
		fos.close();
		
		return generatedSource;
		
	}
	
}
