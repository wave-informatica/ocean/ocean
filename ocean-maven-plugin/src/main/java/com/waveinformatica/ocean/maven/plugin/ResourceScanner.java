/*
 * Copyright 2017, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.maven.plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ivano
 */
public class ResourceScanner {
	
	private final Map<String, Set<String>> resources = new HashMap<String, Set<String>>();

	public void scan(String group, String prefix, File directory, boolean clearName) {
		scan(group, prefix, directory, directory, clearName);
	}
	
	public void scan(String group, String prefix, File root, File directory, boolean clearName) {
		
		if (!directory.exists() || StringUtils.isBlank(directory.getName()) || directory.getName().startsWith(".")) {
			return;
		}
		
		if (StringUtils.isBlank(prefix)) {
			prefix = "";
		}
		
		if (directory.isFile()) {
			String resName = directory.getAbsolutePath().substring(root.getAbsolutePath().length() + prefix.length());
			if (clearName) {
				resName = clearName(resName);
			}
			Set<String> resSet = resources.get(group);
			if (resSet == null) {
				resSet = new TreeSet<String>();
				resources.put(group, resSet);
			}
			resSet.add(resName);
		}
		else {
			for (File d : directory.listFiles()) {
				if (d.isDirectory()) {
					scan(group, prefix, root, d, clearName);
				}
				else {
					String resName = d.getAbsolutePath().substring(root.getParentFile().getAbsolutePath().length() + prefix.length()).replace("\\", "/");
					if (StringUtils.isBlank(resName) || resName.startsWith(".")) {
						continue;
					}
					if (clearName) {
						resName = clearName(resName);
					}
					Set<String> resSet = resources.get(group);
					if (resSet == null) {
						resSet = new TreeSet<String>();
						resources.put(group, resSet);
					}
					resSet.add(resName);
				}
			}
		}
		
	}
	
	public void scanWithHandler(String group, File directory, ScannerHandler handler) {
		if (handler == null) {
			return;
		}
		
		List<String> resources = new ArrayList<String>();
		
		if (directory.isDirectory()) {
			for (File f : directory.listFiles()) {
				scanWithHandler(group, f, handler);
			}
		}
		else {
			resources = handler.getResources(this, directory);
		}
		
		if (resources != null && !resources.isEmpty()) {
			for (String s : resources) {
				Set<String> resSet = this.resources.get(group);
				if (resSet == null) {
					resSet = new TreeSet<String>();
					this.resources.put(group, resSet);
				}
				resSet.add(s);
			}
		}
	}
	
	private String clearName(String resName) {
            resName = resName.replace("\\", "/");
		int ind = resName.lastIndexOf("/");
		int lastDot = resName.lastIndexOf('.');
		int firstDot = resName.indexOf('.', resName.length() - lastDot);
		return resName.substring(0, firstDot) +
			  resName.substring(lastDot);
	}
	
	public Map<String, Set<String>> getResources() {
		return resources;
	}
	
	public Set<String> getResources(String group) {
		return resources.get(group);
	}
	
	public interface ScannerHandler {
		
		public List<String> getResources(ResourceScanner scanner, File file);
		
	}
	
}
