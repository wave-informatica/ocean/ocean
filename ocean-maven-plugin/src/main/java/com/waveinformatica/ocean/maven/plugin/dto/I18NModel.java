/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.maven.plugin.dto;

import java.util.List;

/**
 *
 * @author ivano
 */
public class I18NModel {
	
	private String clsPackage;
	private String interfaceName;
	private boolean implementation;
	private List<PropertyModel> properties;

	public String getClsPackage() {
		return clsPackage;
	}

	public void setClsPackage(String clsPackage) {
		this.clsPackage = clsPackage;
	}

	public String getInterfaceName() {
		return interfaceName;
	}

	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	public boolean isImplementation() {
		return implementation;
	}

	public void setImplementation(boolean implementation) {
		this.implementation = implementation;
	}

	public List<PropertyModel> getProperties() {
		return properties;
	}

	public void setProperties(List<PropertyModel> properties) {
		this.properties = properties;
	}
	
}
