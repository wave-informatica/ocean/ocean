package com.waveinformatica.ocean.maven.plugin;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import org.apache.commons.io.FileUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactResolutionRequest;
import org.apache.maven.artifact.resolver.ArtifactResolutionResult;
import org.apache.maven.artifact.resolver.filter.ArtifactFilter;
import org.apache.maven.model.Build;
import org.apache.maven.model.Model;
import org.apache.maven.model.Plugin;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.apache.maven.repository.RepositorySystem;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.repository.RemoteRepository;

@Mojo(name = "ocean-module", defaultPhase = LifecyclePhase.VALIDATE, requiresProject = true, threadSafe = true,
requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME, executionStrategy = "always")
public class OceanModuleBuilder extends AbstractMojo {

	@Component
	protected RepositorySystem repoSystem;
	
	@Parameter(defaultValue = "${localRepository}")
	protected ArtifactRepository local;
	
	@Parameter(defaultValue = "${project.remoteArtifactRepositories}")
    protected List<ArtifactRepository> remoteRepos;

	/**
	 * The current repository/network configuration of Maven.
	 */
	@Parameter(defaultValue = "${repositorySystemSession}")
	private RepositorySystemSession repoSession;

	/**
	 * The project's remote repositories to use for the resolution of project dependencies.
	 */
	@Parameter(defaultValue = "${project.remoteProjectRepositories}")
	private List<RemoteRepository> projectRepos;

	/**
	 * The associated maven project
	 */
	@Parameter(defaultValue = "${project}", required = true, readonly = true)
	MavenProject project;

	private final Map<String, Artifact> foundArtifacts = new HashMap<String, Artifact>();
	private final List<Artifact> oceanModules = new ArrayList<Artifact>();

	public void execute() throws MojoExecutionException, MojoFailureException {
		
		inspectArtifactDependencies(project.getArtifact());
		
		for (Artifact a : foundArtifacts.values()) {
			try {
				inspectArtifact(a);
			} catch (IOException e) {
				throw new MojoExecutionException("Unexpected error", e);
			} catch (XmlPullParserException e) {
				throw new MojoExecutionException("Unexpected error", e);
			}
		}
		
		if (!oceanModules.isEmpty()) {
			
			File moduleDeps = new File(project.getBasedir(), "target" + File.separator + "oceandeps");
			if (!moduleDeps.isDirectory()) {
				moduleDeps.mkdirs();
			}
			
			StringBuilder builder = new StringBuilder();
			for (Artifact a : oceanModules) {

				getLog().info("Found runtime dependency " + artifactToName(a));
				
				builder.append(',');
				builder.append(a.getGroupId());
				builder.append('.');
				builder.append(a.getArtifactId());
				
				File depDest = new File(moduleDeps, a.getGroupId() + "." + a.getArtifactId() + ".jar");
				try {
					FileUtils.copyFile(a.getFile(), depDest);
				} catch (IOException e) {
					throw new MojoFailureException("Unable to move artifact " + a.getId() + " to " + depDest.getAbsolutePath(), e);
				}
			}
			project.getProperties().setProperty("ocean.module.runtime.dependencies", builder.substring(1));
			getLog().info("ocean.module.runtime.dependencies = " + builder.substring(1));
		}
		
	}

	private String artifactToName(Artifact artifact) {
		return artifact.getGroupId() + ":" + artifact.getArtifactId();
	}

	private void inspectArtifact(Artifact artifact) throws IOException, XmlPullParserException {
		
		if (artifact.getGroupId().equals("com.waveinformatica.ocean") && artifact.getArtifactId().equals("core")) {
			project.getProperties().setProperty("ocean.module.core.version", artifact.getVersion());
		}
		
		if (!"compile".equals(artifact.getScope()) && !"runtime".equals(artifact.getScope())) {
			return;
		}
		
		MavenXpp3Reader reader = new MavenXpp3Reader();

		JarFile file = new JarFile(artifact.getFile());
		try {
			
			ZipEntry pom = file.getEntry("META-INF/maven/" + artifact.getGroupId() + "/" + artifact.getArtifactId() + "/pom.xml");
			if (pom != null) {
				InputStream stream = file.getInputStream(pom);
				try {
					Model model = reader.read(stream);
					MavenProject project = new MavenProject(model);
					Build build = project.getBuild();
					if (build != null) {
						if (build.getPlugins() != null) {
							boolean found = false;
							for (Plugin p : build.getPlugins()) {
								if (p.getGroupId().equals("com.waveinformatica.ocean") &&
										p.getArtifactId().equals("ocean-maven-plugin")) {
									oceanModules.add(artifact);
									found = true;
									break;
								}
							}
						}
					}
				} finally {
					stream.close();
				}
			}

		} finally {
			file.close();
		}
	}
	
	private void inspectArtifactDependencies(Artifact artifact) {
		
		IncludeAllFilter filter = new IncludeAllFilter();
		
		ArtifactResolutionRequest req = new ArtifactResolutionRequest();
		req.setArtifact(artifact);
		req.setResolveTransitively(true);
		req.setRemoteRepositories(remoteRepos);
		req.setLocalRepository(local);
		req.setCollectionFilter(filter);
		req.setResolutionFilter(filter);
		ArtifactResolutionResult result = repoSystem.resolve(req);
		for (Artifact a : result.getArtifacts()) {
			if (!foundArtifacts.containsKey(artifactToName(a))) {
				foundArtifacts.put(artifactToName(a), a);
			}
		}
		
	}
	
	public static class IncludeAllFilter implements ArtifactFilter {

		public boolean include(Artifact artifact) {
			return true;
		}
		
	}

}
