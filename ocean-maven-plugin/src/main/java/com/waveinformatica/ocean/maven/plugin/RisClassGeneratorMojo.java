/*
 * Copyright 2017, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.maven.plugin;

import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.jboss.forge.roaster.ParserException;
import org.jboss.forge.roaster.Roaster;
import org.jboss.forge.roaster.model.Annotation;
import org.jboss.forge.roaster.model.JavaType;
import org.jboss.forge.roaster.model.source.JavaClassSource;
import org.jboss.forge.roaster.model.source.MethodSource;

/**
 *
 * @author ivano
 */
@Mojo(name = "ocean-r-generator", defaultPhase = LifecyclePhase.GENERATE_SOURCES, requiresProject = true, threadSafe = true,
	  requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME, executionStrategy = "always")
public class RisClassGeneratorMojo extends AbstractMojo {

	/**
	 * The output directory of generated classes
	 */
	@Parameter(property = "ocean.outputDirectory", defaultValue = "${project.build.directory}/generated-sources/ocean")
	File outputDirectory;
	
	@Parameter(property = "ocean.resourcesDirectory", defaultValue = "${project.basedir}/src/main/java")
	File javaDirectory;
	
	@Parameter(property = "ocean.resourcesDirectory", defaultValue = "${project.basedir}/src/main/resources")
	File resourcesDirectory;

	/**
	 * The main project's java package
	 */
	@Parameter(property = "ocean.projectPackage")
	String projectPackage;

	/**
	 * The associated maven project
	 */
	@Parameter(defaultValue = "${project}", required = true, readonly = true)
	MavenProject project;

	public void execute() throws MojoExecutionException, MojoFailureException {
		
		if (resourcesDirectory == null || !resourcesDirectory.exists()) {
			getLog().info("Resources directory not found. Skipping...");
			return;
		}
		
		initProps();
		
		// Scanning static resources to produce class R
		
		getLog().info("Scanning static resources...");
		
		ResourceScanner scanner = new ResourceScanner();
		
		scanner.scan("templates", null, new File(resourcesDirectory, "templates"), true);
		
		File themesFile = new File(resourcesDirectory, "themes");
		if (themesFile.exists()) {
			for (File t : themesFile.listFiles()) {

				scanner.scan("templates", null, new File(t, "templates"), true);
				scanner.scan("ris", "/", new File(t, "ris"), false);

				for (File tf : t.listFiles()) {
					if (Arrays.asList("ris", "templates").contains(tf.getName())) {
						continue;
					}
					scanner.scan("other", null, tf, false);
				}

			}
		}
		
		scanner.scan("ris", "/", new File(resourcesDirectory, "ris"), false);
		
		for (File t : resourcesDirectory.listFiles()) {
			if (Arrays.asList("ris", "templates", "i18n", "themes").contains(t.getName())) {
				continue;
			}
			scanner.scan("other", null, t, false);
		}
		
		// Scanning operations
		
		getLog().info("Scanning java files...");
		
		scanner.scanWithHandler("operations", javaDirectory, new ResourceScanner.ScannerHandler() {
			public List<String> getResources(ResourceScanner scanner, File file) {
				List<String> result = new ArrayList<String>();
				
				try {
					JavaClassSource res = null;
					
					try {
						res = Roaster.parse(JavaClassSource.class, file);
					} catch (ParserException e) {
						return null;
					}
					
					Annotation ann = res.getAnnotation(OperationProvider.class);
					if (ann == null) {
						return null;
					}

					String namespace = ann.getStringValue("namespace");
					if (namespace == null) {
						namespace = "";
					}
					
					List<MethodSource<JavaClassSource>> methods = res.getMethods();
					for (MethodSource<JavaClassSource> m : methods) {
						if (!m.isPublic() || m.isStatic()) {
							continue;
						}
						Annotation mann = m.getAnnotation(Operation.class);
						if (mann == null) {
							continue;
						}
						String fullName = ":/" + namespace + "/" + mann.getStringValue();
						fullName = fullName.replaceAll("\\/+", "/");
						if(StringUtils.isNotBlank(fullName) && !":/".equals(fullName))
                                                    result.add(fullName);
					}
					
				} catch (FileNotFoundException ex) {
					getLog().error(ex);
				}
				return result;
			}
		});
		
		// Producing class R
		
		Model model = new Model(scanner.getResources(), projectPackage);
		try {
			produceFile(model, projectPackage);
		} catch (IOException ex) {
			throw new MojoExecutionException("Unexpected error occurred producing R class", ex);
		}
		
		project.addCompileSourceRoot(outputDirectory.getAbsolutePath());
		
	}

	private void initProps() {

		if (StringUtils.isBlank(projectPackage)) {
			projectPackage = project.getGroupId() + "." + project.getArtifactId().replaceAll("[^a-zA-Z0-9]+", " ").trim().replace(' ', '.');
		}
		
		getLog().info("Using package name " + projectPackage);
		
	}
	
	private void produceFile(Model model, String clsPackage) throws IOException {
		
		// Producing class
		StringWriter outputWriterBuffer = new StringWriter();
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);
		cfg.setTemplateLoader(new ClassTemplateLoader(RisClassGeneratorMojo.class.getClassLoader(), ""));
		Template tpl = cfg.getTemplate("/templates/R.jtpl");
		try {
			tpl.process(model, outputWriterBuffer);
		} catch (TemplateException ex) {
			throw new IOException(ex);
		}
		
		File generatedSource = new File(outputDirectory, clsPackage.replace('.', '/') + "/R.java");
		if (!generatedSource.getParentFile().exists()) {
			generatedSource.getParentFile().mkdirs();
		}
		FileOutputStream fos = new FileOutputStream(generatedSource);
		fos.write(outputWriterBuffer.toString().getBytes(Charset.forName("UTF-8")));
		fos.close();
		
	}
	
	public static class Model {
		
		private final Map<String, Set<String>> groups;
		private final String clsPackage;
		private final Map<String, Set<String>> usedNames = new HashMap<String, Set<String>>();

		public Model(Map<String, Set<String>> groups, String clsPackage) {
			this.groups = groups;
			this.clsPackage = clsPackage;
		}

		public Map<String, Set<String>> getGroups() {
			return groups;
		}

		public String getClsPackage() {
			return clsPackage;
		}
		
		public String buildPropertyName(String group, String res) {
			int offset = 1;
			int ind = res.indexOf("/");
			if (ind == 0) {
				offset = 0;
				ind = res.indexOf("/", 1);
			}
			String nameBase = res.substring(ind + offset)
				  .replaceAll("[^a-zA-Z0-9_]+", " ")
				  .trim()
				  .replace(" ", "_");
			String name = nameBase;
			int index = 1;
			if (usedNames.get(group) == null) {
				usedNames.put(group, new HashSet<String>());
			}
			while (usedNames.get(group).contains(name)) {
				name = nameBase + '_' + (index++);
			};
			usedNames.get(group).add(name);
			return name;
		}
		
		public String buildPropertyValue(String res) {
                    res = res.replace("\\", "/");
			return res.startsWith(":/") ? res.substring(2) : res;
		}
		
	}
	
}
