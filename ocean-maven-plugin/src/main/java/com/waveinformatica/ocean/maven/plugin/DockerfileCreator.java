package com.waveinformatica.ocean.maven.plugin;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.DefaultArtifact;
import org.apache.maven.artifact.handler.ArtifactHandler;
import org.apache.maven.artifact.handler.DefaultArtifactHandler;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactResolutionRequest;
import org.apache.maven.artifact.resolver.ArtifactResolutionResult;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.repository.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.repository.RemoteRepository;
import org.yaml.snakeyaml.Yaml;

import com.waveinformatica.ocean.maven.plugin.dto.PropertiesGenerator;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Mojo(name = "ocean-docker-create", defaultPhase = LifecyclePhase.PACKAGE, requiresProject = true, threadSafe = true, executionStrategy = "always")
public class DockerfileCreator extends AbstractMojo {

	@Component
	protected RepositorySystem repoSystem;
	
	@Parameter(defaultValue = "${localRepository}")
	protected ArtifactRepository local;
	
	@Parameter(defaultValue = "${project.remoteArtifactRepositories}")
    protected List<ArtifactRepository> remoteRepos;

	/**
	 * The current repository/network configuration of Maven.
	 */
	@Parameter(defaultValue = "${repositorySystemSession}")
	private RepositorySystemSession repoSession;

	/**
	 * The project's remote repositories to use for the resolution of project dependencies.
	 */
	@Parameter(defaultValue = "${project.remoteProjectRepositories}")
	private List<RemoteRepository> projectRepos;
	
	@Parameter(defaultValue = "")
	protected String sourceRepository;
	
	@Parameter(defaultValue = "${ocean.module.core.version}")
	protected String version;
	
	@Parameter(defaultValue = "src/main/docker/docker.yml")
	protected String yamlFile;
	
	@Parameter(defaultValue = "${project.build.directory}")
	protected File outputDirectory;

	/**
	 * The classes directory
	 */
	@Parameter(property = "ocean.configDirectory", defaultValue = "${project.build.directory}/oceanconfig")
	protected File configDirectory;

	/**
	 * The classes directory
	 */
	@Parameter(property = "ocean.libsDirectory", defaultValue = "${project.build.directory}/oceanlibs")
	protected File libsDirectory;
	
	/**
	 * The associated maven project
	 */
	@Parameter(defaultValue = "${project}", required = true, readonly = true)
	MavenProject project;
	
	public void execute() throws MojoExecutionException, MojoFailureException {
		
		if (StringUtils.isNotBlank(sourceRepository)) {
			project.getProperties().setProperty("ocean.docker.sourceRepository", sourceRepository + "/");
		}
		
		File targetDir = new File(project.getBasedir(), "target");
		
		DockerfileModel model = new DockerfileModel();
		model.setSourceRepository(sourceRepository);
		model.setVersion(version);
		model.setModuleJar(project.getGroupId() + "." + project.getArtifactId() + ".jar");
		
		// Loading configuration
		File actualYamlFile = new File(yamlFile);
		try {
			Map<String, Object> configuration = null;
			Yaml yaml = new Yaml();
			FileReader fileReader = new FileReader(actualYamlFile);
			try {
				configuration = yaml.load(fileReader);
			} finally {
				fileReader.close();
			}
			
			if (!configDirectory.isDirectory()) {
				configDirectory.mkdirs();
			}
			
			Map<String, Object> config = (Map<String, Object>) configuration.get("config");
			
			PropertiesGenerator generator = new PropertiesGenerator(config);
			generator.build();
			getLog().info("Writing configuration properties to " + configDirectory.getAbsolutePath());
			if (configDirectory.getAbsolutePath().startsWith(outputDirectory.getAbsolutePath())) {
				model.setConfigDir(configDirectory.getAbsolutePath().substring(outputDirectory.getAbsolutePath().length()));
			}
			while (model.getConfigDir().startsWith(File.separator)) {
				model.setConfigDir(model.getConfigDir().substring(1));
			}
			generator.writeAll(configDirectory);
			
			List<String> libs = (List<String>) config.get("libs");
			if (libs != null && !libs.isEmpty()) {
				if (!libsDirectory.exists()) {
					libsDirectory.mkdirs();
				}
				if (libsDirectory.getAbsolutePath().startsWith(outputDirectory.getAbsolutePath())) {
					model.setLibsDir(libsDirectory.getAbsolutePath().substring(outputDirectory.getAbsolutePath().length()));
				}
				else {
					model.setLibsDir(libsDirectory.getPath());
				}
				while (model.getLibsDir().startsWith(File.separator)) {
					model.setLibsDir(model.getLibsDir().substring(1));
				}
				Set<String> libNames = new HashSet<String>();
				for (String l : libs) {
					copyLibs(libNames, l);
				}
			}
			
		} catch (IOException e) {
			throw new MojoFailureException("Unable to read defined YAML file " + actualYamlFile.getAbsolutePath(), e);
		}
		
		// Producing Dockerfile
		FileWriter writer = null;
		try {
			
			writer = new FileWriter(new File(targetDir, "Dockerfile"));
			Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);
			cfg.setTemplateLoader(new ClassTemplateLoader(I18NGenerator.class.getClassLoader(), ""));
			Template tpl = cfg.getTemplate("/templates/Dockerfile.tpl");
			tpl.process(model, writer);
			
		} catch (IOException e) {
			throw new MojoFailureException("Unexpected error", e);
		} catch (TemplateException e) {
			throw new MojoFailureException("Unexpected error", e);
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					// ingore...
				}
			}
		}
		
	}
	
	private void copyLibs(Set<String> libNames, String lib) {
		
		String[] parts = lib.split(":");
		
		Artifact artifact = new DefaultArtifact(parts[0], parts[1], parts[2], "compile", "jar", null, new DefaultArtifactHandler("jar"));
		ArtifactResolutionRequest req = new ArtifactResolutionRequest();
		req.setArtifact(artifact);
		req.setResolveTransitively(true);
		req.setRemoteRepositories(remoteRepos);
		req.setLocalRepository(local);
		ArtifactResolutionResult result = repoSystem.resolve(req);
		for (Artifact a : result.getArtifacts()) {
			String aName = a.getGroupId() + ":" + a.getArtifactId();
			if (!libNames.contains(aName)) {
				try {
					FileUtils.copyFile(a.getFile(), new File(libsDirectory, a.getFile().getName()));
					libNames.add(aName);
				} catch (IOException e) {
					getLog().error("Error copying " + a.getFile() + " to " + libsDirectory.getAbsolutePath(), e);
				}
			}
		}
	}
	
	public static class DockerfileModel {
		
		private String sourceRepository;
		private String version;
		private String moduleJar;
		private String libsDir;
		private String configDir;

		public String getSourceRepository() {
			return sourceRepository;
		}

		public void setSourceRepository(String sourceRepository) {
			this.sourceRepository = sourceRepository;
		}

		public String getVersion() {
			return version;
		}

		public void setVersion(String version) {
			this.version = version;
		}

		public String getModuleJar() {
			return moduleJar;
		}

		public void setModuleJar(String moduleJar) {
			this.moduleJar = moduleJar;
		}

		public String getLibsDir() {
			return libsDir;
		}

		public void setLibsDir(String libsDir) {
			this.libsDir = libsDir;
		}

		public String getConfigDir() {
			return configDir;
		}

		public void setConfigDir(String configDir) {
			this.configDir = configDir;
		}
		
	}

}
