package com.waveinformatica.ocean.maven.plugin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import org.apache.commons.lang.StringUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
/*
 * Copyright 2001-2018 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

@Mojo(name = "ocean-generator", defaultPhase = LifecyclePhase.PROCESS_CLASSES, requiresProject = true, threadSafe = true,
	  requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME, executionStrategy = "always")
public class OceanGeneratorMojo extends AbstractMojo {

	/**
	 * The output directory of generated classes
	 */
	@Parameter(property = "ocean.outputDirectory", defaultValue = "${project.build.directory}/generated-sources/ocean")
	File outputDirectory;

	/**
	 * The working directory
	 */
	@Parameter(property = "ocean.resourcesDirectory", defaultValue = "${project.basedir}/src/main/resources")
	File resourcesDirectory;

	/**
	 * The working directory
	 */
	@Parameter(property = "ocean.sourcesDirectory", defaultValue = "${project.basedir}/src/main/java")
	File sourcesDirectory;

	/**
	 * The classes directory
	 */
	@Parameter(property = "ocean.classesDirectory", defaultValue = "${project.build.outputDirectory}")
	File classesDirectory;

	/**
	 * The main project's java package
	 */
	@Parameter(property = "ocean.projectPackage")
	String projectPackage;

	/**
	 * The associated maven project
	 */
	@Parameter(defaultValue = "${project}", required = true, readonly = true)
	MavenProject project;
	
	@Parameter(defaultValue = "${project.compileClasspathElements}", readonly = true, required = true)
	private List<String> compilePath;
	
	public void execute() throws MojoExecutionException {

		Properties oceanProps = new Properties();

		if (!resourcesDirectory.exists()) {
			getLog().warn("{resourcesDirectory} doesn't exist: " + resourcesDirectory.getAbsolutePath());
			return;
		}

		File f = new File(outputDirectory, "i18n");
		//File f = outputDirectory;

		if (!f.exists()) {
			f.mkdirs();
		}

		initProps(oceanProps);

		getLog().info("Using projectPackage: " + oceanProps.getProperty("ocean.package"));

		try {
			
			List<File> generatedClasses = new ArrayList<File>();

			// I18N processing
			generatedClasses.addAll(
				  new I18NGenerator(getLog(), classesDirectory, f, compilePath)
				  .processI18N()
			);
			
			if (!generatedClasses.isEmpty()) {
				getLog().info("Compiling " + generatedClasses.size() + " generated class files");
				compileGeneratedClasses(generatedClasses);
			}

		} catch (IOException ex) {
                    getLog().error(ex);
			throw new MojoExecutionException("Unable to read I18N files", ex);
		}

		// Finally writing module descriptor
		try {

			FileOutputStream fos = new FileOutputStream(new File(classesDirectory, "ocean.module"));

			try {
				oceanProps.store(fos, "");
			} finally {
				fos.close();
			}

		} catch (IOException ex) {
			ex.printStackTrace(System.err);
			throw new MojoExecutionException("Unable to write module properties file \"ocean.module\"", ex);
		}

	}

	private void initProps(Properties props) {

		if (StringUtils.isBlank(projectPackage)) {
			projectPackage = project.getGroupId() + "." + project.getArtifactId().replaceAll("[^a-zA-Z0-9]+", " ").trim().replace(' ', '.');
		}

		if (StringUtils.isBlank(props.getProperty("ocean.package"))) {
			props.setProperty("ocean.package", projectPackage);
		}
	}
	
	public void compileGeneratedClasses(List<File> sources) throws IOException {
		
		Object compilerVersion = project.getProperties().getProperty("maven.compiler.source");
		if (compilerVersion == null) {
			compilerVersion = "1.5";
		}
		
		StringBuilder classPath = new StringBuilder(classesDirectory.getAbsolutePath() + "/");
		for (String cp : compilePath) {
			classPath.append(File.pathSeparatorChar);
			classPath.append(cp);
		}
		
		List<String> options = new ArrayList<String>(Arrays.asList(
			  "-source", compilerVersion.toString(),
			  "-target", compilerVersion.toString(),
			  "-cp", classPath.substring(1),
			  "-d", classesDirectory.getAbsolutePath() + "/",
			  "-sourcepath", outputDirectory.getParent() + "/"
		));
		for (File f : sources) {
			options.add(f.getAbsolutePath());
		}
		
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		int result = compiler.run(null, null, null, options.toArray(new String[options.size()]));
		if (result != 0) {
			throw new IOException("error compiling");
		}
		
	}

}
