<#include "/templates/includes/header.tpl">

<#if data??>
<pre>${data}</pre>
</#if>

<form action="dev/testObjectConverter" method="post">
    <input type="hidden" name="test.nome" value="test">
    <input type="hidden" name="test.interi" value="1">
    <input type="hidden" name="test.interi" value="2">
    <input type="hidden" name="test.annidato.nome" value="oggetto annidato">
    <input type="hidden" name="test.annidato.interi" value="3">
    <input type="hidden" name="test.annidato.interi" value="4">
    <input type="hidden" name="test.altro.nome" value="altro oggetto annidato con nome cambiato">
    <input type="hidden" name="test.arrayDiOggetti$0.nome" value="oggetto in array 1">
    <input type="hidden" name="test.arrayDiOggetti$0.interi" value="5">
    <input type="hidden" name="test.arrayDiOggetti$0.interi" value="6">
    <input type="hidden" name="test.arrayDiOggetti$0.arrayDiOggetti$0.nome" value="altro oggetto in array 1">
    <input type="hidden" name="test.arrayDiOggetti$1.nome" value="oggetto in array 2">
    <input type="hidden" name="test.provaRange.from" value="da">
    <input type="hidden" name="test.provaRange.to" value="a">
    <input type="submit" value="Testa">
</form>

<#include "/templates/includes/footer.tpl">