/*
 * Copyright 2015, 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.devtools.results;

import com.waveinformatica.ocean.core.controllers.results.WebPageResult;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Ivano
 */
public class WizardResult extends WebPageResult {
    
    private final List<WizardStep> steps = new ArrayList<WizardStep>();
    private String currentStep;
    private Date now = new Date();

    public WizardResult() {
        setTemplate("/templates/wizard.tpl");
    }
    
    public void add(String operation, String label) {
        steps.add(new WizardStep(operation, label));
    }

    public List<WizardStep> getSteps() {
        return steps;
    }

    public String getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(String currentStep) {
        this.currentStep = currentStep;
    }
    
    public WizardStep getCurrent() {
        
        Iterator<WizardStep> siter = steps.iterator();
        while (siter.hasNext()) {
            WizardStep step = siter.next();
            if (step.getOperation().equals(currentStep)) {
                return step;
            }
        }
        return null;
        
    }
    
    public String getStepClass(int index) {
        
        int currIndex = -1;
        
        Iterator<WizardStep> siter = steps.iterator();
        while (siter.hasNext()) {
            WizardStep step = siter.next();
            currIndex++;
            if (step.getOperation().equals(currentStep)) {
                break;
            }
        }
        
        if (currIndex > index) {
            return "complete";
        }
        else if (currIndex == index) {
            return "active";
        }
        else {
            return "disabled";
        }
    }
    
    public String getNext() {
        
        Iterator<WizardStep> siter = steps.iterator();
        while (siter.hasNext()) {
            WizardStep step = siter.next();
            if (step.getOperation().equals(currentStep)) {
                if (!siter.hasNext()) {
                    return null;
                }
                else {
                    return siter.next().getOperation();
                }
            }
        }
        
        return null;
        
    }
    
    public String getPrevious() {
        
        String lastOperation = null;
        
        Iterator<WizardStep> siter = steps.iterator();
        while (siter.hasNext()) {
            WizardStep step = siter.next();
            if (step.getOperation().equals(currentStep)) {
                return lastOperation;
            }
            else {
                lastOperation = step.getOperation();
            }
        }
        
        return null;
        
    }

	public Date getNow() {
		return now;
	}

	public void setNow(Date now) {
		this.now = now;
	}
    
    public static class WizardStep {
        private final String operation;
        private final String label;

        public WizardStep(String operation, String label) {
            this.operation = operation;
            this.label = label;
        }

        public String getOperation() {
            return operation;
        }

        public String getLabel() {
            return label;
        }
        
    }
    
}
