/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.devtools;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.annotations.RootOperation;
import com.waveinformatica.ocean.core.controllers.InstanceListener;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.controllers.results.StackResult;
import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.core.controllers.results.input.LongTextFieldHandler;
import com.waveinformatica.ocean.core.modules.ModulesManager;

@OperationProvider(namespace = "jpa")
public class JpaTester {
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private ModulesManager manager;
	
	@Operation("")
	@RootOperation(iconUrl = "database", title = "JPA tester")
	public StackResult main() {
		
		StackResult result = factory.newInstance(StackResult.class);
		
		result.addOperation("jpa/input");
		result.addOperation("jpa/results");
		
		return result;
		
	}
	
	@Operation("input")
	public InputResult queryInput(
			@Param("conn") @InputResult.Field(required = true) String conn,
			@Param("query") @InputResult.Field(handler = LongTextFieldHandler.class, required = true) String query
	) {
		
		InputResult result = factory.newInstance(InputResult.class);
		
		result.setAction("jpa/");
		result.setRefAction("jpa/results");
		
		result.setValue("conn", conn);
		result.setValue("query", query);
		
		return result;
		
	}
	
	@Operation("results")
	public TableResult results(
			@Param("conn") @InputResult.Field(required = true) String conn,
			@Param("query") @InputResult.Field(handler = LongTextFieldHandler.class, required = true) String query
	) {
		
		if (StringUtils.isBlank(conn) || StringUtils.isBlank(query)) {
			return null;
		}
		
		TableResult result = factory.newInstance(TableResult.class);
		
		int pos = conn.indexOf(':');
		String module = conn.substring(0, pos);
		conn = conn.substring(pos + 1);
		
		EntityManagerFactory emFactory = manager.getModule(module).getModuleEntityManagerFactory(conn);
		
		EntityManager em = emFactory.createEntityManager();
		
		InstanceListener.get().getEntityManagers().add(em);
		
		try {
			
			em.getTransaction().begin();
			
			List<?> results = em.createQuery(query).getResultList();
			
			if (results.isEmpty()) {
				return null;
			}
			else {
				Class t = results.get(0).getClass();
				result.setTableData(results, t);
			}
			
			em.getTransaction().commit();
			
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}
		
		return result;
		
	}
	
}
