/*******************************************************************************
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.ocean.devtools.dto;

import java.io.File;

/**
 *
 * @author Ivano
 */
public class ModuleData implements Comparable<ModuleData> {
    
    private String path;
    private String groupId;
    private String artifactId;
    private String version;
    
    public ModuleData() {
        
    }
    
    public ModuleData(String repoPath, String path) {
        this.path = path.substring(repoPath.length());
        File file = new File(path);
        this.version = file.getName();
        this.artifactId = file.getParentFile().getName();
        String gid = file.getParentFile().getParentFile().getAbsolutePath();
        gid = gid.substring(repoPath.length());
        this.groupId = gid.replace(File.separatorChar, '.');
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public int compareTo(ModuleData o) {
        int res = groupId.compareTo(o.groupId);
        if (res == 0) {
            res = artifactId.compareTo(o.artifactId);
        }
        if (res == 0) {
            res = version.compareTo(o.version);
        }
        return res;
    }
    
}
