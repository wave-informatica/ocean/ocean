/*
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.devtools;

import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.util.OceanSession;
import com.waveinformatica.ocean.devtools.results.WizardResult;
import javax.inject.Inject;

/**
 *
 * @author Ivano
 */
@OperationProvider(namespace = "dev/compliance")
public class ComplianceTestWizard {
    
    @Inject
    private OceanSession session;
    
    @Inject
    private ObjectFactory factory;
    
    @Operation("main")
    public WizardResult main() {
        
        WizardResult result = build();
        
        result.setCurrentStep("dev/compliance/main");
        
        return result;
        
    }
    
    @Operation("sessionBegin")
    public WizardResult sessionBegin() {
        
        session.put("session_test_value", "test");
        
        WizardResult result = build();
        
        result.setCurrentStep("dev/compliance/sessionBegin");
        
        return result;
        
    }
    
    @Operation("sessionEnd")
    public WizardResult sessionEnd() {
        
        WizardResult result = build();
        
        result.setCurrentStep("dev/compliance/sessionEnd");
        
        result.setData(session.get("session_test_value"));
        
        return result;
        
    }
    
    private WizardResult build() {
        
        WizardResult result = factory.newInstance(WizardResult.class);
        
        result.add("dev/compliance/main", "Inizio");
        result.add("dev/compliance/sessionBegin", "Session Set");
        result.add("dev/compliance/sessionEnd", "Session Verification");
        
        return result;
        
    }
    
}
