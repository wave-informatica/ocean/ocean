/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.ocean.devtools;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.StandardWatchEventKinds;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.mail.Message;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;
import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.annotations.RootOperation;
import com.waveinformatica.ocean.core.annotations.SkipAuthorization;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.controllers.results.MenuResult;
import com.waveinformatica.ocean.core.controllers.results.MessageResult;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.core.controllers.results.WebPageResult;
import com.waveinformatica.ocean.core.controllers.results.input.LongTextFieldHandler;
import com.waveinformatica.ocean.core.controllers.results.input.Range;
import com.waveinformatica.ocean.core.converters.ObjectConverter;
import com.waveinformatica.ocean.core.exceptions.ModuleException;
import com.waveinformatica.ocean.core.mail.MailBuilder;
import com.waveinformatica.ocean.core.modules.ModulesManager;
import com.waveinformatica.ocean.core.operations.Administration;
import com.waveinformatica.ocean.core.util.FsUtils;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.devtools.dto.ModuleData;
import com.waveinformatica.ocean.filesystem.listener.ListenerService;
import com.waveinformatica.ocean.filesystem.listener.dto.FileEventInfo;

/**
 * @author Ivano
 * @author Marco Merli
 */
@OperationProvider(namespace = "/dev")
public class DevOperations {

	private static final Set<String> onDeployFiles = new ConcurrentSkipListSet<String>();

	@Inject
	private ObjectFactory factory;

	@Inject
	private CoreController coreController;

	@Inject
	private ModulesManager modulesManager;

	@Inject
	private FsUtils fsUtils;

	private final String baseRepo;
	private final String basePath;

	public DevOperations() {
		StringBuilder basePathBuilder = new StringBuilder();
		basePathBuilder.append(System.getProperty("user.home"));
		basePathBuilder.append(File.separatorChar);
		basePathBuilder.append(".m2");
		basePathBuilder.append(File.separatorChar);
		basePathBuilder.append("repository");
		basePathBuilder.append(File.separatorChar);

		baseRepo = basePathBuilder.toString();

		basePathBuilder.append("com");
		basePathBuilder.append(File.separatorChar);
		basePathBuilder.append("waveinformatica");
		basePathBuilder.append(File.separatorChar);
		basePathBuilder.append("ocean");

		basePath = basePathBuilder.toString();
	}

	@Operation("main")
	@RootOperation(title = "Dev Tools", iconUrl = "connectdevelop")
	public MenuResult main() {

		MenuResult mr = factory.newInstance(MenuResult.class);
		mr.setParentOperation("");

		MenuResult.MenuSection section = mr.addSection("DevTools");

		section.add("cubes", "Moduli", "dev/modules");
		section.add("anchor", "Web Sockets", "dev/websockets");
		section.add("file-o", "Diag. Richiesta", "dev/request");
		section.add("file-code-o", "Compliance Tests", "dev/compliance/main");
		section.add("envelope-o", "Send mail test", "dev/testMail");

		return mr;

	}

	@Operation("modules")
	public TableResult modules() {

		TableResult res = factory.newInstance(TableResult.class);
		res.setParentOperation("dev/main");

		ListenerService service = (ListenerService) coreController.getService("FileSystemListener");

		Set<String> paths = service.getWatchedPaths();

		TreeSet<ModuleData> finalPaths = new TreeSet<ModuleData>(){};

		for (String p : paths) {
			if (p.startsWith(baseRepo)) {
				finalPaths.add(new ModuleData(baseRepo, p));
			}
		}

		res.setTableData(finalPaths);

		res.getColumn("artifactId").setKey(true);
		res.getColumn("path").setKey(true);
		res.rebuildKeys();

		res.addTableOperation("dev/addModule", "Add new module");
		res.addRowOperation("dev/remModule", "Remove");

		return res;
	}

	@Operation("addModule")
	public InputResult addModule() {

		InputResult res = factory.newInstance(InputResult.class);

		res.setParentOperation("dev/modules");
		res.setAction("dev/addModuleConfirm");

		return res;

	}

	@Operation("addModuleConfirm")
	public MessageResult addMavenModuleConfirm (@Param("groupId") String groupId,
		@Param("artifactId") String artifactId, @Param("version") String version) {

		StringBuilder builder = new StringBuilder();
		builder.append(baseRepo);
		builder.append(groupId.replace('.', File.separatorChar));
		builder.append(File.separatorChar);
		builder.append(artifactId);
		builder.append(File.separatorChar);
		builder.append(version);
		builder.append(File.separatorChar);
		builder.append(artifactId);
		builder.append('-');
		builder.append(version);
		builder.append(".jar");

		File jarFile = new File(builder.toString());

		MessageResult res = factory.newInstance(MessageResult.class);
		res.setParentOperation("dev/modules");

		if (!jarFile.isFile()) {
			res.setMessage("The requested maven module does not exist. Should you do a maven install yet?");
			res.setCancelAction("dev/modules");
			res.setType(MessageResult.MessageType.CRITICAL);
		}
		else {
			try {
				ListenerService fsListener = (ListenerService) coreController.getService("FileSystemListener");
				fsListener.addPath(artifactId, jarFile.getParentFile(), "/dev/modified", "/dev/modified", null);
				res.setMessage("Maven module successfully added. The module will be automatically deployed next time it will be installed.");
				res.setConfirmAction("dev/modules");
				res.setType(MessageResult.MessageType.INFO);
			} catch (Exception e) {
				res.setMessage("An unexpected error occurred while adding the maven module.");
				res.setException(e);
				res.setCancelAction("dev/modules");
				res.setType(MessageResult.MessageType.CRITICAL);
			}
		}

		return res;

	}

	@Operation("remModule")
	public MessageResult removeModule(@Param("path") String path, @Param("artifactId") String artifactId) {

		MessageResult res = factory.newInstance(MessageResult.class);
		res.setParentOperation("dev/modules");

		try {
			ListenerService fsListener = (ListenerService) coreController.getService("FileSystemListener");
			fsListener.removePath(artifactId, new File(baseRepo + path));
			res.setMessage("Maven module successfully deleted.");
			res.setConfirmAction("dev/modules");
			res.setType(MessageResult.MessageType.INFO);
		} catch (Exception e) {
			res.setMessage("An unexpected error occurred while removing the maven module.");
			res.setException(e);
			res.setCancelAction("dev/modules");
			res.setType(MessageResult.MessageType.CRITICAL);
		}

		return res;

	}

	@Operation("modified")
	@SkipAuthorization
	public void deployModule(@Param("file") String file, FileEventInfo info) {

		if (file == null || !file.endsWith(".jar")) {
			return;
		}

		if (info.getEvent().kind() != StandardWatchEventKinds.ENTRY_MODIFY) {
			return;
		}

		synchronized (file) {
			if (onDeployFiles.contains(file)) {
				return;
			}
			else {
				onDeployFiles.add(file);
			}
		}

		StringBuilder name = new StringBuilder();
		File oldFile = new File(file);
		try {

			JarFile jar = new JarFile(oldFile);
			Manifest manifest = jar.getManifest();
			name.append(manifest.getMainAttributes().getValue("Maven-GroupId"));
			name.append('.');
			name.append(manifest.getMainAttributes().getValue("Maven-ArtifactId"));
			name.append(".jar");
			jar.close();

			FileInputStream fis = new FileInputStream(oldFile);
			final File moduleFile = fsUtils.renameTempFile(fis, name.toString());
			fis.close();
			
			coreController.runLockedAsync(new Runnable() {
				@Override
				public void run() {
					try {
						modulesManager.loadModule(moduleFile);
					} catch (ModuleException e) {
						LoggerUtils.getLogger(DevOperations.class).log(Level.SEVERE, "Error loading module " + moduleFile.getName().substring(0, moduleFile.getName().lastIndexOf('.')));
					} catch (IOException e) {
						LoggerUtils.getLogger(DevOperations.class).log(Level.SEVERE, "Error loading module " + moduleFile.getName().substring(0, moduleFile.getName().lastIndexOf('.')));
					}
				}
			}, true);
			
		}
		catch (IOException ex) {
			Logger.getLogger(Administration.class.getName()).log(Level.SEVERE, null, ex);
		}
		finally {
			onDeployFiles.remove(file);
		}

	}

	@Operation("websockets")
	public WebPageResult testWebSockets() {

		WebPageResult res = factory.newInstance(WebPageResult.class);

		res.setParentOperation("dev/main");
		res.setTemplate("/templates/tests/websockets.tpl");

		return res;

	}

	@Operation("request")
	public WebPageResult diagRequest(HttpServletRequest request) {

		WebPageResult result = factory.newInstance(WebPageResult.class);

		result.setParentOperation("dev/main");
		result.setTemplate("/templates/tests/stringList.tpl");

		List<String> headers = new ArrayList<String>();

		Enumeration<String> names = request.getHeaderNames();
		while (names.hasMoreElements()) {
			String name = names.nextElement();
			Enumeration<String> values = request.getHeaders(name);
			while (values.hasMoreElements()) {
				headers.add(name + ": " + values.nextElement());
			}
		}

		result.setData(headers);

		return result;

	}

	@Operation("testObjectConverter")
	public WebPageResult testObjectConverter(@Param(value = "test", converter = ObjectConverter.class) ObjectConverterTest test) {

		WebPageResult result = factory.newInstance(WebPageResult.class);

		result.setParentOperation("dev/main");
		result.setTemplate("/templates/tests/objectConverter.tpl");

		Gson gson = JsonUtils.getBuilder().setPrettyPrinting().create();

		result.setData(gson.toJson(test));

		return result;

	}

	@Operation("testMail")
	public InputResult testMail(
		@Param("from") String from,
		@Param("to") String to,
		@Param("cc") String cc,
		@Param("subject") String subject,
		@Param("htmlText") @InputResult.Field(handler = LongTextFieldHandler.class) String htmlText,
		HttpServletRequest request
		) {

		InputResult result = factory.newInstance(InputResult.class);
		result.setParentOperation("dev/");

		result.setAction("dev/testMail");

		if ("POST".equalsIgnoreCase(request.getMethod())) {

			result.setData(request.getParameterMap());

			MessageResult msg = factory.newInstance(MessageResult.class);

			MailBuilder builder = factory.newInstance(MailBuilder.class);

			try {
				builder.setFrom(from);
				builder.addRecipient(Message.RecipientType.TO, to);
				if (StringUtils.isNotBlank(cc)) {
					builder.addRecipient(Message.RecipientType.CC, cc);
				}
				builder.setSubject(subject);

				builder.loadViewFromString(MimeTypes.HTML, htmlText);

				builder.send();

				msg.setType(MessageResult.MessageType.CONFIRM);
				msg.setMessage("Mail successfully sent");

			} catch (Exception ex) {
				msg.setType(MessageResult.MessageType.CRITICAL);
				msg.setMessage("Unexpected error building or sending message");
				msg.setException(ex);
			}

			result.setMessage(msg);

		}

		return result;

	}

	public static class TableResultTest {

		@TableResult.TableResultColumn(ignore = true)
		private String ignored;
		private String nome;
		private Integer count;

		public String getIgnored() {
			return ignored;
		}

		public void setIgnored(String ignored) {
			this.ignored = ignored;
		}

		public String getNome() {
			return nome;
		}

		public void setNome(String nome) {
			this.nome = nome;
		}

		public Integer getCount() {
			return count;
		}

		public void setCount(Integer count) {
			this.count = count;
		}

	}

	public static class ObjectConverterTest {

		private String nome;
		private List<Integer> interi;
		private ObjectConverterTest annidato;
		private ObjectConverterTest annidatoChanged;
		private List<ObjectConverterTest> arrayDiOggetti;
		private Range<String> provaRange;

		public String getNome() {
			return nome;
		}

		public void setNome(String nome) {
			this.nome = nome;
		}

		public List<Integer> getInteri() {
			return interi;
		}

		public void setInteri(List<Integer> interi) {
			this.interi = interi;
		}

		public ObjectConverterTest getAnnidato() {
			return annidato;
		}

		public void setAnnidato(@Param(value = "", converter = ObjectConverter.class) ObjectConverterTest annidato) {
			this.annidato = annidato;
		}

		public ObjectConverterTest getAnnidatoChanged() {
			return annidatoChanged;
		}

		public void setAnnidatoChanged(@Param(value = "altro", converter = ObjectConverter.class)ObjectConverterTest annidatoChanged) {
			this.annidatoChanged = annidatoChanged;
		}

		public List<ObjectConverterTest> getArrayDiOggetti() {
			return arrayDiOggetti;
		}

		public void setArrayDiOggetti(List<ObjectConverterTest> arrayDiOggetti) {
			this.arrayDiOggetti = arrayDiOggetti;
		}

		public Range<String> getProvaRange() {
			return provaRange;
		}

		public void setProvaRange(Range<String> provaRange) {
			this.provaRange = provaRange;
		}

	}

}
