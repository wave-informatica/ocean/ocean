/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.features.websocket;

import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.cdi.CDIHelper;
import com.waveinformatica.ocean.core.cdi.CDISession;
import com.waveinformatica.ocean.core.cdi.GlobalSessionScopeContext;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.InstanceListener;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.OperationContext;
import com.waveinformatica.ocean.core.controllers.events.BeforeResultEvent;
import com.waveinformatica.ocean.core.controllers.events.SecurityConfigurationEvent;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.security.SecurityManager;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.DecoratorHelper;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;

@ServerEndpoint(value = "/ws/ocean", configurator = SessionAwareConfigurator.class)
public class RootEndpoint {
	
	private static final Map<String, WebSocketSession> sessions = new ConcurrentHashMap<String, WebSocketSession>();
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private CoreController controller;
	
	@OnOpen
	public void open(Session session, EndpointConfig config) {
		
		if (controller == null) {
			controller = Application.getInstance().getCoreController();
			factory = Application.getInstance().getObjectFactory();
		}
		
		HttpSession httpSession = (HttpSession) config.getUserProperties().get("HttpSession");
		CDISession cdiSession = GlobalSessionScopeContext.getCDISession(httpSession);
		
		try {
			
			//CDIHelper.startGlobalSessionContextOnly(httpSession);
			CDIHelper.startCdiContexts(cdiSession, httpSession);
			
			WebSocketSession wsSession = new WebSocketSession(session, cdiSession);

			sessions.put(session.getId(), wsSession);

			WebSocketConnectedEvent evt = new WebSocketConnectedEvent(this, wsSession);
			controller.dispatchEvent(evt);
			
		} finally {
			Context.clear();
			CDIHelper.stopCdiContexts();
			InstanceListener.get().clear();
		}
	}
	
	@OnClose
	public void close(Session session, CloseReason reason) {
		
		LoggerUtils.getCoreLogger().log(Level.INFO, "WebSocket closed: " + reason.getReasonPhrase());
		
		if (controller == null) {
			controller = Application.getInstance().getCoreController();
			factory = Application.getInstance().getObjectFactory();
		}
		
		try {
			
			CDIHelper.startCdiContexts(sessions.get(session.getId()).getCdiSession(), sessions.get(session.getId()).getCdiSession());

			WebSocketClosedEvent evt = new WebSocketClosedEvent(this, sessions.get(session.getId()));

			for (IEventListener l : evt.getClosedSession().getCloseListeners()) {
				try {
					l.performAction(evt);
				} catch (Exception e) {
					LoggerUtils.getLogger(l.getClass()).log(Level.SEVERE, "Error signaling WebSocket close to listener \"" + l.getClass().getName() + "\"", e);
				}
			}

			sessions.remove(session.getId());
			
		} finally {
			Context.clear();
			CDIHelper.stopCdiContexts();
			InstanceListener.get().clear();
		}
	}
	
	@OnError
	public void onError(Throwable t) {
		if (t instanceof IOException || t instanceof EOFException) {
			return;
		}
		LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unexpected error on websocket", t);
	}
	
	@OnMessage
	public void handleMessage(String message, Session session) {
		
		if (controller == null) {
			controller = Application.getInstance().getCoreController();
			factory = Application.getInstance().getObjectFactory();
		}
		
		Logger logger = LoggerUtils.getCoreLogger();
		
		WebSocketSession wss = sessions.get(session.getId());
		wss.setLastActivity(new Date());
		
		CDIHelper.startCdiContexts(wss.getCdiSession(), wss.getCdiSession());
		Context.init();
		Context.setCoreController(controller);
		
		SecurityManager securityManager = factory.getBean(SecurityManager.class);
		SecurityConfigurationEvent secConfigEvent = new SecurityConfigurationEvent(this, securityManager);
		controller.dispatchEvent(secConfigEvent);
		securityManager.setAuthorizeGuests(!secConfigEvent.isAuthenticationNeeded());
		
		String operation;
		String parameters;
		
		int colonPos = message.indexOf(':');
		if (colonPos != -1) {
			operation = message.substring(0, colonPos);
			parameters = message.substring(colonPos + 1);
		} else {
			operation = message;
			parameters = null;
		}
		
		Map<String, String[]> paramsMap;
		
		if (parameters != null) {
			Gson gson = JsonUtils.getBuilder().create();
			paramsMap = gson.fromJson(parameters, new TypeToken<Map<String, String[]>>() {
			}.getType());
		} else {
			paramsMap = new HashMap<String, String[]>();
		}
		
		try {
			
			OperationContext opCtx = new OperationContext();
			
			Object result = controller.executeOperation(operation, paramsMap, opCtx, session, sessions.get(session.getId()));
			
			if (result != null) {
				
				if (result instanceof BaseResult) {
					BeforeResultEvent evt = new BeforeResultEvent(this, opCtx.getOperation(), (BaseResult) result, null);
					controller.dispatchEvent(evt);
				}
				
				DecoratorHelper helper = factory.newInstance(DecoratorHelper.class);
				
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				helper.decorateResult(result, baos, opCtx, MimeTypes.JSON);
				
				try {
					
					session.getAsyncRemote().sendText(new String(baos.toByteArray(), "UTF-8"));
					
				} catch (Exception ex) {
					logger.log(Level.SEVERE, null, ex);
				}
				
			}
			
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error executing operation \"" + operation + "\" through WebSocket bridge", e);
		} finally {
			Context.setCoreController(null);
			Context.clear();
			CDIHelper.stopCdiContexts();
			InstanceListener.get().clear();
		}
	}
	
	@OnMessage
	public void onMessage(ByteBuffer byteBufer, boolean last, Session session) {
		WebSocketSession wss = sessions.get(session.getId());
		wss.setLastActivity(new Date());
		
		if (wss.getChannelManager() != null) {

			if (controller == null) {
				controller = Application.getInstance().getCoreController();
				factory = Application.getInstance().getObjectFactory();
			}
			
			try {
				
				CDIHelper.startCdiContexts(wss.getCdiSession(), wss.getCdiSession());
				Context.init();
				Context.setCoreController(controller);
				
				if (wss.getBinaryDataBuffer() == null && !last) {
					wss.setBinaryDataBuffer(new ByteArrayOutputStream());
				}
				if (!last) {
					try {
						wss.getBinaryDataBuffer().write(byteBufer.array());
					} catch (IOException ex) {
						LoggerUtils.getLogger(RootEndpoint.class).log(Level.SEVERE, null, ex);
					}
				}
				else {
					if (wss.getBinaryDataBuffer() != null) {
						try {
							wss.getBinaryDataBuffer().write(byteBufer.array());
						} catch (IOException ex) {
							LoggerUtils.getLogger(RootEndpoint.class).log(Level.SEVERE, null, ex);
						}
						wss.getChannelManager().manage(ByteBuffer.wrap(wss.getBinaryDataBuffer().toByteArray()), wss);
						wss.setBinaryDataBuffer(null);
					}
					else {
						wss.getChannelManager().manage(byteBufer, wss);
					}
				}
				
			} finally {
				Context.setCoreController(null);
				Context.clear();
				CDIHelper.stopCdiContexts();
				InstanceListener.get().clear();
			}
		}
	}
	
}
