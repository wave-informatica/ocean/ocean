/*******************************************************************************
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.features.websocket;

import com.waveinformatica.ocean.core.controllers.events.Event;

public class WebSocketClosedEvent extends Event {
	
	private final WebSocketSession closedSession;
	
	public WebSocketClosedEvent(Object source, WebSocketSession closedSession) {
		super(source, WebSocketClosedEvent.class.getSimpleName());
		this.closedSession = closedSession;
	}

	public WebSocketSession getClosedSession() {
		return closedSession;
	}

}
