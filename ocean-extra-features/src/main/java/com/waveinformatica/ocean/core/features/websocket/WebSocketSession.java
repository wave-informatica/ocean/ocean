/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.features.websocket;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.websocket.Session;

import com.waveinformatica.ocean.core.cdi.CDISession;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import java.io.ByteArrayOutputStream;

public class WebSocketSession {
	
	private final Session session;
	private final CDISession cdiSession;
	private final List<IEventListener> closeListeners = new LinkedList<IEventListener>();
	private WebSocketChannelManager channelManager;
	private ByteArrayOutputStream binaryDataBuffer;
	private Date lastActivity;
	
	public WebSocketSession(Session session, CDISession cdiSession) {
		
		super();
		this.session = session;
		this.cdiSession = cdiSession == null ? new CDISession() : cdiSession;
		this.lastActivity = new Date();
		
	}

	public Session getSession() {
		return session;
	}

	protected CDISession getCdiSession() {
		return cdiSession;
	}
	
	public void addCloseListener(IEventListener el) {
		closeListeners.add(el);
	}
	
	protected List<IEventListener> getCloseListeners() {
		return closeListeners;
	}

	public String getId() {
		return session.getId();
	}
	
	public void forceClose() throws IOException {
		closeListeners.clear();
		session.close();
	}

	public WebSocketChannelManager getChannelManager() {
		return channelManager;
	}

	public void setChannelManager(WebSocketChannelManager channelManager) {
		this.channelManager = channelManager;
	}

	public ByteArrayOutputStream getBinaryDataBuffer() {
		return binaryDataBuffer;
	}

	public void setBinaryDataBuffer(ByteArrayOutputStream binaryDataBuffer) {
		this.binaryDataBuffer = binaryDataBuffer;
	}

	public Date getLastActivity() {
		return lastActivity;
	}

	public void setLastActivity(Date lastActivity) {
		this.lastActivity = lastActivity;
	}
	
}
