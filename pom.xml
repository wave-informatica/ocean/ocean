<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2015, 2018 Wave Informatica S.r.l.. Licensed under the Apache 
	License, Version 2.0 (the "License"); you may not use this file except in 
	compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 
	Unless required by applicable law or agreed to in writing, software distributed 
	under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES 
	OR CONDITIONS OF ANY KIND, either express or implied. See the License for 
	the specific language governing permissions and limitations under the License. -->

<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.waveinformatica</groupId>
	<artifactId>ocean</artifactId>
	<version>2.5.0-SNAPSHOT</version>
	<packaging>pom</packaging>

	<name>${project.artifactId}-${project.version}</name>

	<description>Ocean Framework is a web app Java framework for the everyday work of any web developer.</description>

	<organization>
		<name>Wave Informatica S.r.l.</name>
		<url>http://www.waveinformatica.com</url>
	</organization>

	<licenses>
		<license>
			<name>Apache License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
			<comments>A business-friendly OSS license</comments>
		</license>
	</licenses>

	<developers>
		<developer>
			<name>Ivano Culmine</name>
			<email>ivano.culmine@waveinformatica.com</email>
			<organization>Wave Informatica S.r.l.</organization>
			<roles>
				<role>architect</role>
				<role>developer</role>
			</roles>
			<timezone>Europe/Rome</timezone>
		</developer>
	</developers>

	<contributors>
		<contributor>
			<name>Marco Merli</name>
			<email>marco.merli@waveinformatica.com</email>
			<organization>Wave Informatica S.r.l.</organization>
			<roles>
				<role>developer</role>
			</roles>
		</contributor>
		<contributor>
			<name>Giuseppe Avola</name>
			<email>giuseppe.avola@waveinformatica.com</email>
			<organization>Wave Informatica S.r.l.</organization>
			<roles>
				<role>developer</role>
			</roles>
		</contributor>
		<contributor>
			<name>Chiara Leanza</name>
			<email>chiara.leanza@waveinformatica.com</email>
			<organization>Wave Informatica S.r.l.</organization>
			<roles>
				<role>designer</role>
			</roles>
		</contributor>
		<contributor>
			<name>Enrico La Malfa</name>
			<email>enrico.lamalfa@waveinformatica.com</email>
			<organization>Wave Informatica S.r.l.</organization>
			<roles>
				<role>developer</role>
			</roles>
		</contributor>
	</contributors>

	<scm>
		<connection>scm:git:https://gitlab.waveinformatica.com/ocean/ocean.git</connection>
		<developerConnection>scm:git:https://gitlab.waveinformatica.com/ocean/ocean.git</developerConnection>
		<tag>HEAD</tag>
		<url>https://gitlab.waveinformatica.com/ocean/ocean</url>
	</scm>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<poi.version>3.17</poi.version>
		<poi.schemas.version>1.3</poi.schemas.version>
	</properties>

	<modules>
		<module>core</module>
		<module>classicWebApp</module>
		<module>filesystem-listener</module>
		<module>devtools</module>
		<module>scheduler</module>
		<module>batch</module>
		<module>ocean-extra-features</module>
		<module>persistence-modules/hibernate-4.3</module>
		<module>web-extras</module>
		<module>audit</module>
		<module>archetypes/ocean-module-archetype</module>
		<module>archetypes/ocean-war-archetype</module>
		<module>messaging/messaging-core</module>
		<module>startup</module>
		<module>ocean-maven-plugin</module>
		<module>ocean-doclet</module>
		<module>caches/infinispan</module>
		<module>pages</module>
		<module>ocean-docker-files</module>
		<module>ocean-server</module>
		<module>manual-maker</module>
		<module>gpalgorithms</module>
		<module>deploy/deployer-base</module>
		<module>caches/redis</module>
		<module>console</module>
	</modules>

	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-site-plugin</artifactId>
					<version>3.7.1</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-deploy-plugin</artifactId>
					<version>2.8.2</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-javadoc-plugin</artifactId>
					<version>2.10.4</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-release-plugin</artifactId>
					<version>2.5.3</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-source-plugin</artifactId>
					<version>3.2.0</version>
				</plugin>
			</plugins>
		</pluginManagement>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-release-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<dependencies>
					<dependency>
						<groupId>com.waveinformatica.ocean</groupId>
						<artifactId>ocean-doclet</artifactId>
						<version>${project.version}</version>
					</dependency>
				</dependencies>
				<configuration>
					<doclet>com.waveinformatica.ocean.doclet.DocletMain</doclet>
					<docletArtifact>
						<groupId>com.waveinformatica.ocean</groupId>
						<artifactId>ocean-doclet</artifactId>
						<version>${project.version}</version>
					</docletArtifact>
					<useStandardDocletOptions>false</useStandardDocletOptions>
					<aggregate>true</aggregate>
					<additionalParam>-root ${project.basedir}</additionalParam>
				</configuration>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-deploy-plugin</artifactId>
			</plugin>
		</plugins>
	</build>

	<distributionManagement>
		<snapshotRepository>
			<id>waverepo</id>
			<url>https://cloud.waveinformatica.com/nexus/repository/maven-snapshots/</url>
		</snapshotRepository>
		<repository>
			<id>waverepo</id>
			<url>https://cloud.waveinformatica.com/nexus/repository/maven-oss/</url>
		</repository>
	</distributionManagement>

	<reporting>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-site-plugin</artifactId>
				<version>3.7.1</version>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-project-info-reports-plugin</artifactId>
				<version>3.0.0</version>
				<reportSets>
					<reportSet>
						<reports><!-- select reports -->
							<report>index</report>
							<report>licenses</report>
							<report>dependencies</report>
							<report>dependency-convergence</report>
							<report>dependency-info</report>
							<report>dependency-management</report>
							<report>help</report>
							<report>modules</report>
							<report>plugin-management</report>
							<report>plugins</report>
							<report>team</report>
							<report>scm</report>
							<report>summary</report>
							<report>ci-management</report>
							<report>issue-management</report>
						</reports>
					</reportSet>
				</reportSets>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>2.10.4</version>
				<configuration>
					<doclet>com.waveinformatica.ocean.doclet.DocletMain</doclet>
					<additionalDependencies>
						<additionalDependency>
							<groupId>com.waveinformatica.ocean</groupId>
							<artifactId>ocean-doclet</artifactId>
							<version>${project.version}</version>
						</additionalDependency>
					</additionalDependencies>
					<docletArtifact>
						<groupId>com.waveinformatica.ocean</groupId>
						<artifactId>ocean-doclet</artifactId>
						<version>${project.version}</version>
					</docletArtifact>
					<useStandardDocletOptions>false</useStandardDocletOptions>
					<aggregate>true</aggregate>
				</configuration>
				<reportSets>
					<reportSet>
						<reports>
							<report>aggregate</report>
						</reports>
					</reportSet>
				</reportSets>
			</plugin>
			<!-- plugin> <groupId>org.owasp</groupId> <artifactId>dependency-check-maven</artifactId> 
				<version>4.0.2</version> <reportSets> <reportSet> <reports> <report>aggregate</report> 
				</reports> </reportSet> </reportSets> </plugin -->
		</plugins>
	</reporting>
</project>
