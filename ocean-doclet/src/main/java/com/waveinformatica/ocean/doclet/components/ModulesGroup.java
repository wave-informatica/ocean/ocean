/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.doclet.components;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.plexus.util.StringUtils;

import com.waveinformatica.ocean.doclet.ModuleDoc;

public class ModulesGroup {
	
	private final String pathName;
	private final List<ModuleDoc> modules = new ArrayList<>();
	
	public ModulesGroup(String pathName) {
		while (pathName.startsWith("/")) {
			pathName = pathName.substring(1);
		}
		this.pathName = pathName;
	}

	public String getPathName() {
		return pathName;
	}
	
	public String getTitle() {
		return StringUtils.capitaliseAllWords(pathName.replaceAll("[^a-zA-Z0-9]", " ").trim());
	}

	public List<ModuleDoc> getModules() {
		return modules;
	}

}
