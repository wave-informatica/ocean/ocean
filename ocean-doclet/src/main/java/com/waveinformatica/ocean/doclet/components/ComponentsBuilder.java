/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.doclet.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.waveinformatica.ocean.doclet.DocletMain;
import com.waveinformatica.ocean.doclet.ModuleDoc;
import com.waveinformatica.ocean.doclet.ProcessContext;

public class ComponentsBuilder {
	
	private final ProcessContext context;
	private final ModuleDoc rootModule;
	private final List<ModulesGroup> groups = new ArrayList<>();
	
	public ComponentsBuilder(ProcessContext context, ModuleDoc rootModule) {
		
		this.context = context;
		this.rootModule = rootModule;
		
		Map<String, ModulesGroup> grMap = new HashMap<>();
		
		for (ModuleDoc md : rootModule.getSubModules()) {
			
			String pathName = md.getPathName();
			int ind = pathName.lastIndexOf('/');
			
			String gName = pathName.substring(0, ind + 1);
			ModulesGroup group = grMap.get(gName);
			if (group == null) {
				group = new ModulesGroup(gName);
				grMap.put(gName, group);
				groups.add(group);
			}
			
			group.getModules().add(md);
			
		}
		
	}

	public ModuleDoc getRootModule() {
		return rootModule;
	}

	public List<ModulesGroup> getGroups() {
		return groups;
	}
	
	public String getTitle() {
		return "Components";
	}
	
	public String getContextPath() {
		return DocletMain.getBackRelativePath(rootModule.getOutputPath(), context.getDirectory());
	}
	
}
