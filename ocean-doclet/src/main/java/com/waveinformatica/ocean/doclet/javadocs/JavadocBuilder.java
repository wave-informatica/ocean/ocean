/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.doclet.javadocs;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.codehaus.plexus.util.StringUtils;

import com.sun.javadoc.AnnotationDesc;
import com.sun.javadoc.AnnotationDesc.ElementValuePair;
import com.sun.javadoc.AnnotationTypeDoc;
import com.sun.javadoc.AnnotationTypeElementDoc;
import com.sun.javadoc.AnnotationValue;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.ConstructorDoc;
import com.sun.javadoc.Doc;
import com.sun.javadoc.ExecutableMemberDoc;
import com.sun.javadoc.FieldDoc;
import com.sun.javadoc.MethodDoc;
import com.sun.javadoc.PackageDoc;
import com.sun.javadoc.ParamTag;
import com.sun.javadoc.Parameter;
import com.sun.javadoc.ParameterizedType;
import com.sun.javadoc.SeeTag;
import com.sun.javadoc.Tag;
import com.sun.javadoc.ThrowsTag;
import com.sun.javadoc.Type;
import com.sun.javadoc.TypeVariable;
import com.waveinformatica.ocean.doclet.DocletMain;
import com.waveinformatica.ocean.doclet.ModuleDoc;
import com.waveinformatica.ocean.doclet.ProcessContext;
import com.waveinformatica.ocean.doclet.TemplatesManager;
import com.waveinformatica.ocean.doclet.cache.CacheableModel;

import freemarker.template.Template;
import freemarker.template.TemplateException;

public class JavadocBuilder {
	
	protected static final Pattern linkTagPattern = Pattern.compile("\\{@link (([a-zA-Z0-9\\._]+?\\.)?([a-zA-Z$_]+))(#(([a-zA-Z0-9_]+)\\([^\\)]*\\)))?( .+)?\\}");
	
	private final ProcessContext context;
	private final TemplatesManager templates;
	private final ModuleDoc module;
	private final File output;
	
	public JavadocBuilder(ProcessContext context, TemplatesManager templates, ModuleDoc module, File output) {
		this.context = context;
		this.templates = templates;
		this.module = module;
		this.output = output;
	}
	
	public void initCache() {
		
		for (ClassDoc clsDoc : module.getClasses()) {
			
			if (!output.exists()) {
				output.mkdirs();
			}
			
			String contextPath = DocletMain.getBackRelativePath(output, context.getDirectory());
			
			ModuleModel model = context.getModel(ModuleModel.class, context, module, contextPath);
			context.getModel(ClassModel.class, model, contextPath, clsDoc);
			
		}
		
	}
	
	public void build() {
		
		ModuleModel model = context.getModel(ModuleModel.class, context, module, DocletMain.getBackRelativePath(output, context.getDirectory()));
		
		// Index
		Template tpl = templates.getTemplate("/templates/apidocs/index.tpl");
		
		try (FileWriter writer = new FileWriter(new File(output, "index.html"))) {
			
			tpl.process(model, writer);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TemplateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Packages
		tpl = templates.getTemplate("/templates/apidocs/package.tpl");
		for (PackageModel pkg : model.getPackages()) {
			try (FileWriter writer = new FileWriter(new File(output, pkg.getName() + ".html"))) {
				
				tpl.process(pkg, writer);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TemplateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		for (ClassDoc clsDoc : module.getClasses()) {
			buildClass(context.getModel(ClassModel.class, model, model.getContextPath(), clsDoc));
		}
		
	}
	
	public void buildClass(ClassModel model) {
		
		if (!model.isLinked()) {
			model.link(context);
		}
		
		// Index
		Template tpl = templates.getTemplate("/templates/apidocs/class.tpl");
		
		try (FileWriter writer = new FileWriter(new File(output, model.getFullName() + ".html"))) {
			
			tpl.process(model, writer);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TemplateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static abstract class CommentedModel {
		
		private String comment;
		private String summaryComment;
		private List<Tag> blockTags;
		
		protected final void initComments(ProcessContext context, Doc doc, String contextPath) {
			
			comment = processTags(context, doc, doc.inlineTags(), contextPath);
			summaryComment = processTags(context, doc, doc.firstSentenceTags(), contextPath);
			
			blockTags = new ArrayList<>();
			for (Tag t : doc.tags()) {
				if (t.holder() == doc) {
					blockTags.add(t);
				}
			}
		}
		
		public void initComments(ProcessContext context, Doc doc, Tag[] inlineTags, String contextPath) {
			comment = processTags(context, doc, inlineTags, contextPath);
		}
		
		public String getComment() {
			return comment;
		}
		
		public String getSummaryComment() {
			return summaryComment;
		}
		
		public List<Tag> getBlockTags() {
			return blockTags;
		}

		private String processTags(ProcessContext context, Doc doc, Tag[] tags, String contextPath) {
			StringBuilder builder = new StringBuilder();
			for (Tag t : tags) {
				if ("Text".equals(t.name())) {
					builder.append(t.text());
				}
				else if ("@link".equals(t.name())) {
					SeeTag st = (SeeTag) t;
					ClassDoc linkClass = st.referencedClass();
					if (linkClass != null) {
						String method = st.referencedMember() != null ? "#" + st.referencedMemberName() : "";
						ClassModel cm = context.getModel(ClassModel.class, null, null, linkClass);
						String label = st.label().trim();
						if (StringUtils.isBlank(label)) {
							label = cm.getName() + method;
						}
						builder.append("<a href=\"" + contextPath + cm.getUrl() + method + "\">" + label + "</a>");
					}
					else {
						builder.append("[link]" + t.toString());
					}
				}
				else if ("@literal".equals(t.name())) {
					builder.append(t.text());
				}
				else {
					builder.append(t.toString());
				}
			}
			return builder.toString();
		}
		
	}
	
	public static class ModuleModel implements CacheableModel {
		
		private final ModuleDoc module;
		private final String contextPath;
		private final List<PackageModel> packages = new ArrayList<>();
		private final String basePath;

		public ModuleModel(ProcessContext context, ModuleDoc module, String contextPath) {
			super();
			this.module = module;
			this.contextPath = contextPath;
			
			String basePath = module.getOutputPath().getAbsolutePath().substring(context.getDirectory().getAbsolutePath().length());
			while (basePath.startsWith("/")) {
				basePath = basePath.substring(1);
			}
			this.basePath = basePath;
			
			for (PackageDoc pkg : module.getPackages()) {
				PackageModel pkgModel = context.getModel(PackageModel.class, this, pkg, contextPath);
				packages.add(pkgModel);
			}
		}

		public ModuleDoc getModule() {
			return module;
		}

		public String getContextPath() {
			return contextPath;
		}
		
		public String getTitle() {
			return module.getName() + " API Docs";
		}

		public List<PackageModel> getPackages() {
			return packages;
		}

		@Override
		public String getKey() {
			return "module[" + module.getPathName() + "]";
		}

		@Override
		public void link(ProcessContext context) {
			
		}

		public String getBasePath() {
			return basePath;
		}

		@Override
		public String getUrl() {
			return getBasePath() + "index.html";
		}

		@Override
		public boolean isLinked() {
			return true;
		}
		
	}
	
	public static class PackageModel extends CommentedModel implements CacheableModel {
		
		private final ModuleModel module;
		private final PackageDoc pkg;
		private final String contextPath;
		private final List<ClassModel> enums = new ArrayList<>();
		private final List<ClassModel> annotations = new ArrayList<>();
		private final List<ClassModel> interfaces = new ArrayList<>();
		private final List<ClassModel> classes = new ArrayList<>();
		private final List<ClassModel> exceptions = new ArrayList<>();
		private final List<ClassModel> errors = new ArrayList<>();
		private boolean linked = false;

		public PackageModel(ModuleModel module, PackageDoc pkg, String contextPath) {
			super();
			this.module = module;
			this.pkg = pkg;
			this.contextPath = contextPath;
		}

		public PackageDoc getPkg() {
			return pkg;
		}
		
		public String getName() {
			return pkg.name();
		}
		
		public String getTitle() {
			return pkg.name();
		}

		public String getContextPath() {
			return contextPath;
		}

		public List<ClassModel> getClasses() {
			return classes;
		}

		public List<ClassModel> getEnums() {
			return enums;
		}

		public List<ClassModel> getAnnotations() {
			return annotations;
		}

		public List<ClassModel> getInterfaces() {
			return interfaces;
		}

		public List<ClassModel> getExceptions() {
			return exceptions;
		}

		public List<ClassModel> getErrors() {
			return errors;
		}

		@Override
		public String getKey() {
			return module.getKey() + "/package[" + pkg.name() + "]";
		}

		@Override
		public void link(ProcessContext context) {
			
			for (ClassDoc cd : pkg.ordinaryClasses()) {
				if (cd.containingClass() == null) {
					classes.add(context.getModel(ClassModel.class, module, contextPath, cd));
				}
			}
			
			for (ClassDoc cd : pkg.enums()) {
				if (cd.containingClass() == null) {
					enums.add(context.getModel(ClassModel.class, module, contextPath, cd));
				}
			}
			
			for (AnnotationTypeDoc cd : pkg.annotationTypes()) {
				if (cd.containingClass() == null) {
					ClassModel model = context.getModel(ClassModel.class, module, contextPath, cd);
					if (!model.isLinked()) {
						model.link(context);
					}
					annotations.add(model);
				}
			}
			
			for (ClassDoc cd : pkg.interfaces()) {
				if (cd.containingClass() == null) {
					interfaces.add(context.getModel(ClassModel.class, module, contextPath, cd));
				}
			}
			
			for (ClassDoc cd : pkg.exceptions()) {
				if (cd.containingClass() == null) {
					exceptions.add(context.getModel(ClassModel.class, module, contextPath, cd));
				}
			}
			
			for (ClassDoc cd : pkg.errors()) {
				if (cd.containingClass() == null) {
					errors.add(context.getModel(ClassModel.class, module, contextPath, cd));
				}
			}
			
			initComments(context, pkg, contextPath);
			
			if (getUrl() != null) {
				context.getLunr().index(getUrl(), getName(), getComment(), "package " + getName());
			}
			
			linked = true;
			
		}

		@Override
		public String getUrl() {
			return module.getBasePath() + "/javadocs/" + getName() + ".html";
		}

		@Override
		public boolean isLinked() {
			return linked;
		}
		
	}
	
	public static class ClassModel extends CommentedModel implements CacheableModel {
		
		private final ModuleModel module;
		private final String contextPath;
		private final ClassDoc clsDoc;
		private final String type;
		private final List<TypeRef> hierarchy = new LinkedList<>();
		private final List<TypeVariableModel> arguments = new ArrayList<>();
		private final List<AnnotatedModel> classAnnotations = new ArrayList<>();
		private final List<TypeRef> implInterfaces = new ArrayList<>();
		private final List<ClassModel> subClasses = new ArrayList<>();
		private ClassModel enclosingClass;
		private final List<ClassModel> enums = new ArrayList<>();
		private final List<ClassModel> annotations = new ArrayList<>();
		private final List<ClassModel> interfaces = new ArrayList<>();
		private final List<ClassModel> classes = new ArrayList<>();
		private final List<ClassModel> exceptions = new ArrayList<>();
		private final List<ClassModel> errors = new ArrayList<>();
		private final List<FieldModel> enumConstants = new ArrayList<>();
		private final List<FieldModel> annotationElements = new ArrayList<>();
		private final List<FieldModel> protectedFields = new ArrayList<>();
		private final List<FieldModel> publicFields = new ArrayList<>();
		private final List<MethodModel> constructors = new ArrayList<>();
		private final List<MethodModel> protectedMethods = new ArrayList<>();
		private final List<MethodModel> publicMethods = new ArrayList<>();
		private final List<String> groupTags = new ArrayList<>();
		private final List<UmlDiagram> umlDiagrams = new ArrayList<>();
		private boolean linked = false;
		
		public ClassModel(ModuleModel module, String contextPath, ClassDoc clsDoc) {
			super();
			this.module = module;
			this.contextPath = contextPath;
			this.clsDoc = clsDoc.asParameterizedType() != null ? (ClassDoc) clsDoc.asParameterizedType() : clsDoc;
			
			if (clsDoc.isEnum()) {
				type = "Enum";
			}
			else if (clsDoc.isInterface()) {
				type = "Interface";
			}
			else if (clsDoc.isException()) {
				type = "Exception";
			}
			else if (clsDoc.isError()) {
				type = "Error";
			}
			else if (clsDoc.isAnnotationType()) {
				type = "Annotation";
			}
			else if (clsDoc.isClass()) {
				type = "Class";
			}
			else {
				type = null;
			}
			
			for (Tag t : clsDoc.tags("@group")) {
				groupTags.add(t.text());
			}
			
		}
		
		public ModuleModel getModule() {
			return module;
		}
		
		public String getTitle() {
			return type + " " + clsDoc.name();
		}
		
		public String getContextPath() {
			return contextPath;
		}
		
		public ClassDoc getClsDoc() {
			return clsDoc;
		}
		
		public String getFullName() {
			return clsDoc.containingPackage().name() + "." + clsDoc.name();
		}
		
		public String getName() {
			return clsDoc.simpleTypeName();
		}

		public String getType() {
			return type;
		}

		public List<TypeRef> getHierarchy() {
			return hierarchy;
		}
		
		public String getDeclaration() {
			if (clsDoc.isEnum()) {
				return "enum";
			}
			else if (clsDoc.isInterface()) {
				return "interface";
			}
			else if (clsDoc.isException()) {
				return "class";
			}
			else if (clsDoc.isError()) {
				return "class";
			}
			else if (clsDoc.isAnnotationType()) {
				return "@interface";
			}
			else if (clsDoc.isClass()) {
				return "class";
			}
			return null;
		}

		public List<TypeRef> getImplInterfaces() {
			return implInterfaces;
		}

		public List<ClassModel> getSubClasses() {
			return subClasses;
		}

		public ClassModel getEnclosingClass() {
			return enclosingClass;
		}

		public List<ClassModel> getEnums() {
			return enums;
		}

		public List<ClassModel> getAnnotations() {
			return annotations;
		}

		public List<ClassModel> getInterfaces() {
			return interfaces;
		}

		public List<ClassModel> getClasses() {
			return classes;
		}

		public List<ClassModel> getExceptions() {
			return exceptions;
		}

		public List<ClassModel> getErrors() {
			return errors;
		}

		public List<FieldModel> getEnumConstants() {
			return enumConstants;
		}

		public List<FieldModel> getProtectedFields() {
			return protectedFields;
		}

		public List<FieldModel> getPublicFields() {
			return publicFields;
		}

		public List<MethodModel> getProtectedMethods() {
			return protectedMethods;
		}

		public List<MethodModel> getPublicMethods() {
			return publicMethods;
		}

		public List<AnnotatedModel> getClassAnnotations() {
			return classAnnotations;
		}

		public List<TypeVariableModel> getArguments() {
			return arguments;
		}

		public List<MethodModel> getConstructors() {
			return constructors;
		}

		public List<FieldModel> getAnnotationElements() {
			return annotationElements;
		}

		@Override
		public String getKey() {
			return "class[" + getFullName() + "]";
		}
		
		@Override
		public void link(ProcessContext context) {
			
			Type superCls = clsDoc.superclassType();
			if (superCls != null) {
				ClassModel superClsMod = context.getModel(ClassModel.class, null, contextPath, superCls.asClassDoc());
				if (superClsMod != null) {
					superClsMod.getSubClasses().add(this);
				}
			}
			while (superCls != null) {
				TypeRef ref = new TypeRef(context, superCls);
				hierarchy.add(0, ref);
				superCls = superCls.asClassDoc().superclassType();
			}
			
			enclosingClass = clsDoc.containingClass() != null ? context.getModel(ClassModel.class, module, contextPath, clsDoc.containingClass()) : null;
			
			for (Type cd : clsDoc.interfaceTypes()) {
				TypeRef typeRef = new TypeRef(context, cd);
				implInterfaces.add(typeRef);
				typeRef.getCls().getSubClasses().add(this);
			}
			
			for (TypeVariable tp : clsDoc.typeParameters()) {
				TypeVariableModel tvm = new TypeVariableModel(context, tp);
				arguments.add(tvm);
			}
			
			for (AnnotationDesc ann : clsDoc.annotations()) {
				AnnotatedModel am = new AnnotatedModel(ann);
				am.link(context);
				classAnnotations.add(am);
			}
			
			for (FieldDoc ec : clsDoc.enumConstants()) {
				FieldModel ecm = new FieldModel(context, ec, contextPath);
				enumConstants.add(ecm);
			}
			
			if (clsDoc instanceof AnnotationTypeDoc) {
				for (AnnotationTypeElementDoc am : ((AnnotationTypeDoc) clsDoc).elements()) {
					annotationElements.add(new FieldModel(context, am, contextPath));
				}
			}
			
			for (FieldDoc fd : clsDoc.fields()) {
				FieldModel fdm = new FieldModel(context, fd, contextPath);
				if (fd.isProtected()) {
					protectedFields.add(fdm);
				}
				else if (fd.isPublic()) {
					publicFields.add(fdm);
				}
			}
			
			for (ConstructorDoc cd : clsDoc.constructors()) {
				MethodModel cdm = new MethodModel(context, cd, contextPath);
				constructors.add(cdm);
			}
			
			for (MethodDoc md : clsDoc.methods()) {
				MethodModel mdm = new MethodModel(context, md, contextPath);
				if (md.isProtected()) {
					protectedMethods.add(mdm);
				}
				else if (md.isPublic()) {
					publicMethods.add(mdm);
				}
			}
			
			for (ClassDoc cd : clsDoc.innerClasses()) {
				if (cd.containingClass() == clsDoc) {
					if (cd.isClass()) {
						classes.add(context.getModel(ClassModel.class, module, contextPath, cd));
					}
					else if (cd.isEnum()) {
						enums.add(context.getModel(ClassModel.class, module, contextPath, cd));
					}
					else if (cd.isInterface()) {
						interfaces.add(context.getModel(ClassModel.class, module, contextPath, cd));
					}
					else if (cd.isAnnotationType()) {
						ClassModel model = context.getModel(ClassModel.class, module, contextPath, (AnnotationTypeDoc) cd);
						if (!model.isLinked()) {
							model.link(context);
						}
						annotations.add(model);
					}
					else if (cd.isException()) {
						exceptions.add(context.getModel(ClassModel.class, module, contextPath, cd));
					}
					else if (cd.isError()) {
						errors.add(context.getModel(ClassModel.class, module, contextPath, cd));
					}
				}
			}
			
			initComments(context, clsDoc, contextPath);
			
			if (getUrl() != null) {
				context.getLunr().index(getUrl(), getName(), getComment(), "class " + getFullName());
			}
			
			linked = true;
			
		}
		
		public void addUmlDiagram(String url, String title, String tag) {
			umlDiagrams.add(new UmlDiagram(url, title, tag));
		}

		public List<UmlDiagram> getUmlDiagrams() {
			return umlDiagrams;
		}

		public List<String> getGroupTags() {
			return groupTags;
		}

		@Override
		public String getUrl() {
			if (module == null) {
				return null;
			}
			return module.getBasePath() + "/javadocs/" + getFullName() + ".html";
		}

		@Override
		public boolean isLinked() {
			return linked;
		}
		
	}
	
	public static class AnnotatedModel {
		
		private ClassModel annotationModel;
		private final AnnotationDesc annotation;
		private final Map<String, AnnotationValueWrapper> values = new HashMap<>();
		private TypeRef typeRef;

		public AnnotatedModel(AnnotationDesc annotation) {
			super();
			this.annotation = annotation;
		}

		public AnnotationDesc getAnnotation() {
			return annotation;
		}

		public ClassModel getAnnotationModel() {
			return annotationModel;
		}
		
		public void link(ProcessContext context) {
			annotationModel = context.getModel(ClassModel.class, null, null, annotation.annotationType());
//			annotation.elementValues()[0].value().
			for (ElementValuePair v : annotation.elementValues()) {
				values.put(v.element().name(), new AnnotationValueWrapper(context, v.value()));
			}
			typeRef = new TypeRef(context, annotation.annotationType());
		}

		public Map<String, AnnotationValueWrapper> getValues() {
			return values;
		}

		public TypeRef getTypeRef() {
			return typeRef;
		}
		
	}
	
	public static class AnnotationValueWrapper {
		
		private final AnnotationValue value;
		private final ProcessContext context;

		public AnnotationValueWrapper(ProcessContext context, AnnotationValue value) {
			super();
			this.context = context;
			this.value = value;
		}

		public Object getValue() {
			return value.value();
		}
		
		public boolean isArray() {
			return value.value().getClass().isArray();
		}
		
		public boolean isClassLiteral() {
			return value.value() instanceof Type;
		}
		
		public boolean isSimple() {
			return value.value() instanceof Number || value.value() instanceof Boolean || value.value().getClass().isPrimitive();
		}
		
		public boolean isString() {
			return value.value() instanceof String;
		}
		
		public boolean isFieldDoc() {
			return value.value() instanceof FieldDoc;
		}
		
		public boolean isAnnotation() {
			return value.value() instanceof AnnotationDesc;
		}
		
		public TypeRef toTypeRef(Type t) {
			return new TypeRef(context, t);
		}
		
		public List<AnnotationValueWrapper> getValues() {
			List<AnnotationValueWrapper> values = new ArrayList<AnnotationValueWrapper>(((AnnotationValue[]) value.value()).length);
			for (AnnotationValue av : (AnnotationValue[]) value.value()) {
				values.add(new AnnotationValueWrapper(context, av));
			}
			return values;
		}
		
		public AnnotatedModel toModel(AnnotationDesc annotation) {
			AnnotatedModel model = new AnnotatedModel(annotation);
			model.link(context);
			return model;
		}
		
	}
	
	public static class TypeVariableModel {
		
		private final ProcessContext context;
		private final TypeVariable type;

		public TypeVariableModel(ProcessContext context, TypeVariable type) {
			super();
			this.context = context;
			this.type = type;
		}

		public TypeVariable getType() {
			return type;
		}

		public String getName() {
			return type.simpleTypeName();
		}
		
		public List<TypeRef> getBounds() {
			List<TypeRef> typeRefs = new ArrayList<>();
			for (Type b : type.bounds()) {
				typeRefs.add(new TypeRef(context, b));
			}
			return typeRefs;
		}
		
	}
	
	public static class TypeRef {
		
		private final Type type;
		private final String primitiveName;
		private final ClassModel cls;
		private final List<TypeRef> arguments = new ArrayList<>();

		public TypeRef(ProcessContext context, Type type) {
			super();
			this.type = type;
			if (type.isPrimitive()) {
				this.primitiveName = type.simpleTypeName();
				this.cls = null;
			}
			else {
				ClassModel cls = context.getModel(ClassModel.class, null, null, type.asClassDoc());
				ParameterizedType pType = type.asParameterizedType();
				if (pType != null) {
					for (Type t : pType.typeArguments()) {
						arguments.add(new TypeRef(context, t));
					}
				}
				this.cls = cls;
				primitiveName = null;
			}
		}

		public ClassModel getCls() {
			return cls;
		}

		public String getPrimitiveName() {
			return primitiveName;
		}

		public List<TypeRef> getArguments() {
			return arguments;
		}
		
		public String getSimpleName() {
			return StringUtils.isNotBlank(primitiveName) ? primitiveName : cls.getClsDoc().simpleTypeName();
		}
		
		public String getDimension() {
			return type.dimension();
		}
		
	}
	
	public static class FieldModel extends CommentedModel {
		
		private final TypeRef typeRef;
		private final FieldDoc fieldDoc;
		private final AnnotationTypeElementDoc annotationElementDoc;
		private final List<AnnotatedModel> annotations = new ArrayList<>();
		private final AnnotationValueWrapper defaultValue;
		
		public FieldModel(ProcessContext context, FieldDoc fieldDoc, String contextPath) {
			super();
			this.typeRef = new TypeRef(context, fieldDoc.type());
			this.fieldDoc = fieldDoc;
			for (AnnotationDesc ann : fieldDoc.annotations()) {
				AnnotatedModel annotation = new AnnotatedModel(ann);
				annotation.link(context);
				annotations.add(annotation);
			}
			annotationElementDoc = null;
			defaultValue = null;
			initComments(context, fieldDoc, contextPath);
		}
		
		public FieldModel(ProcessContext context, AnnotationTypeElementDoc fieldDoc, String contextPath) {
			super();
			this.typeRef = new TypeRef(context, fieldDoc.returnType());
			this.fieldDoc = null;
			this.annotationElementDoc = fieldDoc;
			for (AnnotationDesc ann : fieldDoc.annotations()) {
				AnnotatedModel annotation = new AnnotatedModel(ann);
				annotation.link(context);
				annotations.add(annotation);
			}
			defaultValue = annotationElementDoc.defaultValue() != null ? new AnnotationValueWrapper(context, annotationElementDoc.defaultValue()) : null;
			initComments(context, fieldDoc, contextPath);
		}

		public TypeRef getTypeRef() {
			return typeRef;
		}
		
		public FieldDoc getFieldDoc() {
			return fieldDoc;
		}
		
		public AnnotationTypeElementDoc getAnnotationTypeElementDoc() {
			return annotationElementDoc;
		}
		
		public String getDeclaration() {
			return fieldDoc != null ? fieldDoc.modifiers() : annotationElementDoc.modifiers();
		}

		public String getName() {
			return fieldDoc != null ? fieldDoc.name() : annotationElementDoc.name();
		}
		
		public AnnotationValueWrapper getDefaultValue() {
			return defaultValue;
		}

		public List<AnnotatedModel> getAnnotations() {
			return annotations;
		}
		
	}
	
	public static class ParameterModel extends CommentedModel {
		
		private final Parameter parameter;
		private final TypeRef type;
		private final List<AnnotatedModel> annotations = new ArrayList<>();

		public ParameterModel(ProcessContext context, ExecutableMemberDoc method, Parameter parameter) {
			super();
			this.parameter = parameter;
			type = new TypeRef(context, parameter.type());
			for (AnnotationDesc ann : parameter.annotations()) {
				AnnotatedModel annotation = new AnnotatedModel(ann);
				annotation.link(context);
				annotations.add(annotation);
			}
			for (ParamTag pt : method.paramTags()) {
				if (pt.parameterName().equals(parameter.name())) {
					initComments(context, method, pt.inlineTags(), "");
					break;
				}
			}
		}

		public Parameter getParameter() {
			return parameter;
		}
		
		public TypeRef getType() {
			return type;
		}

		public List<AnnotatedModel> getAnnotations() {
			return annotations;
		}
		
		public String getName() {
			return parameter.name();
		}
		
	}
	
	public static class ThrownExceptionModel extends CommentedModel {
		
		private final TypeRef exceptionType;

		public ThrownExceptionModel(ProcessContext context, MethodModel method, Type exception) {
			super();
			this.exceptionType = new TypeRef(context, exception);
			for (ThrowsTag tt : method.method.throwsTags()) {
				if (tt.exceptionType() == exception) {
					initComments(context, method.method, tt.inlineTags(), "");
					break;
				}
			}
		}

		public TypeRef getExceptionType() {
			return exceptionType;
		}
		
	}
	
	public static class MethodModel extends CommentedModel {
		
		private final ExecutableMemberDoc method;
		private final List<TypeVariableModel> arguments = new ArrayList<>();
		private final TypeRef returnType;
		private final List<AnnotatedModel> annotations = new ArrayList<>();
		private final List<ParameterModel> parameters = new ArrayList<>();
		private final List<ThrownExceptionModel> thrownExceptions = new ArrayList<>();
		
		public MethodModel(ProcessContext context, ExecutableMemberDoc method, String contextPath) {
			this.method = method;
			if (method instanceof MethodDoc) {
				this.returnType = new TypeRef(context, ((MethodDoc) method).returnType());
			}
			else {
				this.returnType = null;
			}
			for (TypeVariable arg : method.typeParameters()) {
				arguments.add(new TypeVariableModel(context, arg));
			}
			for (AnnotationDesc ann : method.annotations()) {
				AnnotatedModel annotation = new AnnotatedModel(ann);
				annotation.link(context);
				annotations.add(annotation);
			}
			for (Parameter param : method.parameters()) {
				parameters.add(new ParameterModel(context, method, param));
			}
			for (Type tet : method.thrownExceptionTypes()) {
				thrownExceptions.add(new ThrownExceptionModel(context, this, tet));
			}
			
			initComments(context, method, contextPath);
			
			ClassModel cm = context.getModel(ClassModel.class, null, null, method.containingClass());
			
			if (cm != null && cm.getUrl() != null) {
				StringBuilder paramsBuilder = new StringBuilder();
				for (ParameterModel pm : parameters) {
					paramsBuilder.append(',');
					paramsBuilder.append(pm.getType().getSimpleName());
				}
				String params = paramsBuilder.length() > 0 ? paramsBuilder.substring(1) : "";
				context.getLunr().index(cm.getUrl() + "#" + getName() + "(" + params + ")", getName() + " (" + params + ")", getComment(), "class " + cm.getFullName());
			}
		}
		
		public String getDeclaration() {
			return method.modifiers();
		}

		public String getName() {
			return method.name();
		}

		public List<TypeVariableModel> getArguments() {
			return arguments;
		}

		public TypeRef getReturnType() {
			return returnType;
		}

		public List<AnnotatedModel> getAnnotations() {
			return annotations;
		}

		public List<ParameterModel> getParameters() {
			return parameters;
		}

		public List<ThrownExceptionModel> getThrownExceptions() {
			return thrownExceptions;
		}

		public ExecutableMemberDoc getMethod() {
			return method;
		}
		
	}
	
	public static class UmlDiagram {
		
		private final String url;
		private final String title;
		private final String tag;
		
		public UmlDiagram(String url, String title, String tag) {
			super();
			this.url = url;
			this.title = title;
			this.tag = tag;
		}

		public String getUrl() {
			return url;
		}

		public String getTitle() {
			return title;
		}

		public String getTag() {
			return tag;
		}
		
	}

}
