/*
 * Copyright 2015, 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.doclet;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateNotFoundException;
import java.io.IOException;
import java.util.Locale;

/**
 *
 * @author Ivano
 */
public class TemplatesConfiguration extends Configuration {

	@Override
	public Template getTemplate(String name, Locale locale, Object customLookupCondition, String encoding, boolean parseAsFTL, boolean ignoreMissing)
		  throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException {

		Template result = null;

		if (!name.startsWith("/")) {
			name = "/" + name;
		}

		result = super.getTemplate(name, locale, customLookupCondition, encoding, parseAsFTL, ignoreMissing);

		return result;
	}

}
