/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.doclet.uml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.plexus.util.StringUtils;

import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.FieldDoc;
import com.sun.javadoc.MethodDoc;
import com.sun.javadoc.Type;
import com.waveinformatica.ocean.doclet.DocletMain;
import com.waveinformatica.ocean.doclet.ModuleDoc;
import com.waveinformatica.ocean.doclet.ProcessContext;
import com.waveinformatica.ocean.doclet.javadocs.JavadocBuilder.ClassModel;
import com.waveinformatica.ocean.doclet.javadocs.JavadocBuilder.ModuleModel;
import com.waveinformatica.ocean.doclet.javadocs.JavadocBuilder.TypeRef;

import net.sourceforge.plantuml.FileFormat;
import net.sourceforge.plantuml.FileFormatOption;
import net.sourceforge.plantuml.SourceStringReader;

public class UmlGenerator {
	
	private final ProcessContext context;
	private final ModuleDoc module;
	private final File output;
	private final List<TypeGroup> typeGroups = new ArrayList<>();

	public UmlGenerator(ProcessContext context, ModuleDoc module, File output) {
		this.context = context;
		this.module = module;
		this.output = output;
	}
	
	public void generate() {

		ModuleModel model = context.getModel(ModuleModel.class, context, module, DocletMain.getBackRelativePath(output, context.getDirectory()));
		
		// Types hierarchy diagrams
		
		Map<String, TypeGroup> parsedClasses = new HashMap<>();
		
		for (ClassDoc clsDoc : module.getClasses()) {
			ClassModel cm = context.getModel(ClassModel.class, null, null, clsDoc);
			if (cm != null) {
				buildTypeGroup(cm, parsedClasses);
			}
		}
		
		IdentityHashMap<TypeGroup, TypeGroup> groups = new IdentityHashMap<>();
		for (TypeGroup tg : parsedClasses.values()) {
			groups.put(tg, tg);
		}
		
		int index = 0;
		for (TypeGroup tg : groups.keySet()) {
			generateTypeHierarchy(tg, index);
			++index;
		}
		
		// References UML diagram
		
		Map<String, List<TypeGroup>> nParsedClasses = new HashMap<>();
		
		IdentityHashMap<Type, Type> traversedClasses = new IdentityHashMap<>();
		
		for (ClassDoc clsDoc : module.getClasses()) {
			ClassModel cm = context.getModel(ClassModel.class, null, null, clsDoc);
			if (cm != null) {
				buildReferenceGroup(cm, nParsedClasses, traversedClasses);
			}
		}
		
		groups.clear();
		for (List<TypeGroup> ltg : nParsedClasses.values()) {
			for (TypeGroup tg : ltg) {
				groups.put(tg, tg);
			}
		}
		
		index = 0;
		for (TypeGroup tg : groups.keySet()) {
			generateTypeReferences(tg, index);
			++index;
		}
		
	}
	
	private void buildTypeGroup(ClassModel cm, Map<String, TypeGroup> classes) {
		
		if (classes.containsKey(cm.getFullName())) {
			return;
		}
		
		List<TypeRef> hierarchy = new ArrayList<>(cm.getHierarchy());
		Collections.reverse(hierarchy);
		
		IdentityHashMap<TypeGroup, TypeGroup> groups = new IdentityHashMap<>();
		
		for (TypeRef c : cm.getImplInterfaces()) {
			if (c.getCls().getModule() != null && c.getCls().getModule().getModule() == module) {
				buildTypeGroup(c.getCls(), classes);
				groups.put(classes.get(c.getCls().getFullName()), classes.get(c.getCls().getFullName()));
			}
		}
		for (TypeRef tr : hierarchy) {
			if (tr.getCls().getModule() != null && tr.getCls().getModule().getModule() == module) {
				buildTypeGroup(tr.getCls(), classes);
				groups.put(classes.get(tr.getCls().getFullName()), classes.get(tr.getCls().getFullName()));
			}
		}
		
		TypeGroup ntg = new TypeGroup();
		for (TypeGroup tg : groups.keySet()) {
			for (ClassModel tgcm : tg.getClasses()) {
				boolean found = false;
				for (ClassModel ntgcm : ntg.getClasses()) {
					if (ntgcm.getFullName().equals(tgcm.getFullName())) {
						found = true;
						break;
					}
				}
				if (!found) {
					ntg.getClasses().add(tgcm);
				}
			}
		}
		
		ntg.getClasses().add(cm);
		
		for (ClassModel ntgcm : ntg.getClasses()) {
			classes.put(ntgcm.getFullName(), ntg);
		}
		
	}
	
	private void generateTypeHierarchy(TypeGroup group, int index) {
		
		Set<String> definedTypes = new HashSet<>();
		
		List<String> packageNames = new ArrayList<>();
		Map<String, Integer> packageCounter = new HashMap<>();
		
		StringWriter stringWriter = new StringWriter();
		stringWriter.write("@startuml\n");
		stringWriter.write("\n");
		for (ClassModel cm : group.getClasses()) {
			defineType(cm, false, stringWriter, definedTypes, packageNames);
		}
		
		stringWriter.write("\n");
		for (ClassModel cm : group.getClasses()) {
			for (TypeRef c : cm.getImplInterfaces()) {
				if (c.getCls().getClsDoc().qualifiedName().equals("java.lang.annotation.Annotation")) {
					continue;
				}
				defineType(c.getCls(), false, stringWriter, definedTypes, packageNames);
				stringWriter.write(c.getCls().getClsDoc().qualifiedName());
				Integer cnt = packageCounter.get(cm.getClsDoc().containingPackage().name());
				if (cnt == null) {
					cnt = 0;
				}
				packageCounter.put(cm.getClsDoc().containingPackage().name(), ++cnt);
				if (!c.getCls().getClsDoc().containingPackage().name().equals(cm.getClsDoc().containingPackage().name())) {
					stringWriter.write(
							StringUtils.rightPad(" <|..", 5 + cnt / 6 + 2 * packageNames.indexOf(cm.getClsDoc().containingPackage().name()), ".") + " "
					);
				}
				else {
					stringWriter.write(StringUtils.rightPad(" <|..", 5 + cnt / 6, ".") + " ");
				}
				stringWriter.write(cm.getClsDoc().qualifiedName());
				stringWriter.write("\n");
			}
			if (!cm.getHierarchy().isEmpty() && !Arrays.asList("java.lang.Object", "java.lang.Enum").contains(((LinkedList<TypeRef>) cm.getHierarchy()).getLast().getCls().getClsDoc().qualifiedName())) {
				defineType(((LinkedList<TypeRef>) cm.getHierarchy()).getLast().getCls(), false, stringWriter, definedTypes, packageNames);
				stringWriter.write(((LinkedList<TypeRef>) cm.getHierarchy()).getLast().getCls().getClsDoc().qualifiedName());
				Integer cnt = packageCounter.get(cm.getClsDoc().containingPackage().name());
				if (cnt == null) {
					cnt = 0;
				}
				packageCounter.put(cm.getClsDoc().containingPackage().name(), ++cnt);
				if (!((LinkedList<TypeRef>) cm.getHierarchy()).getLast().getCls().getClsDoc().containingPackage().name().equals(cm.getClsDoc().containingPackage().name())) {
					stringWriter.write(StringUtils.rightPad(" <|--", 5 + cnt / 6 + 2 * packageNames.indexOf(cm.getClsDoc().containingPackage().name()), "-") + " ");
				}
				else {
					stringWriter.write(StringUtils.rightPad(" <|--", 5 + cnt / 6, "-") + " ");
				}
				stringWriter.write(cm.getClsDoc().qualifiedName());
				stringWriter.write("\n");
			}
		}
		stringWriter.write("\n");
		stringWriter.write("@enduml\n");
		
		File file = new File(output, "hierarchy-" + index + ".uml");
		
		try (FileWriter writer = new FileWriter(file)) {
			
			writer.write(stringWriter.toString());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		File svg = new File(file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf('.')) + ".svg");
		
		try (FileOutputStream fos = new FileOutputStream(svg)) {
			
			SourceStringReader reader = new SourceStringReader(stringWriter.toString());
			reader.generateImage(fos, new FileFormatOption(FileFormat.SVG));
			
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for (ClassModel cm : group.getClasses()) {
			cm.addUmlDiagram(cm.getContextPath() + svg.getAbsolutePath().substring(context.getDirectory().getAbsolutePath().length() + 1), "Type Hierarchy", null);
		}
		
	}
	
	private void buildReferenceGroup(ClassModel cm, Map<String, List<TypeGroup>> classes, IdentityHashMap<Type, Type> traversedClasses) {
		
		if (classes.containsKey(cm.getFullName()) || traversedClasses.containsKey(cm.getClsDoc())) {
			return;
		}
		
		traversedClasses.put(cm.getClsDoc(), cm.getClsDoc());
		
		List<TypeRef> hierarchy = new ArrayList<>(cm.getHierarchy());
		Collections.reverse(hierarchy);
		
		IdentityHashMap<TypeGroup, TypeGroup> groups = new IdentityHashMap<>();

		for (TypeRef tr : hierarchy) {
			if (tr.getCls().getModule() != null && tr.getCls().getModule().getModule() == module) {
				buildReferenceGroup(tr.getCls(), classes, traversedClasses);
				if (classes.get(tr.getCls().getFullName()) != null) {
					List<TypeGroup> tgl = classes.get(tr.getCls().getFullName());
					for (TypeGroup tg : tgl) {
						groups.put(tg, tg);
					}
				}
			}
		}
		for (FieldDoc f : cm.getClsDoc().fields(false)) {
			if (isCollection(f.type()) != null) {
				TypeRef tr = new TypeRef(context, isCollection(f.type()));
				if (tr.getCls() != null && tr.getCls().getModule() != null && tr.getCls().getModule().getModule() == module) {
					buildReferenceGroup(tr.getCls(), classes, traversedClasses);
					if (classes.get(tr.getCls().getFullName()) != null) {
						List<TypeGroup> tgl = classes.get(tr.getCls().getFullName());
						for (TypeGroup tg : tgl) {
							groups.put(tg, tg);
						}
					}
				}
			}
			else {
				TypeRef tr = new TypeRef(context, f.type());
				if (tr.getCls() != null && tr.getCls().getModule() != null && tr.getCls().getModule().getModule() == module) {
					buildReferenceGroup(tr.getCls(), classes, traversedClasses);
					if (classes.get(tr.getCls().getFullName()) != null) {
						List<TypeGroup> tgl = classes.get(tr.getCls().getFullName());
						for (TypeGroup tg : tgl) {
							groups.put(tg, tg);
						}
					}
				}
			}
		}
		
		TypeGroup ntg = new TypeGroup();
		for (TypeGroup tg : groups.keySet()) {
			if (tg.getTag() != null) {
				continue;
			}
			for (ClassModel tgcm : tg.getClasses()) {
				boolean found = false;
				for (ClassModel ntgcm : ntg.getClasses()) {
					if (ntgcm.getFullName().equals(tgcm.getFullName())) {
						found = true;
						break;
					}
				}
				if (!found) {
					ntg.getClasses().add(tgcm);
				}
			}
		}
		
		ntg.getClasses().add(cm);
		
		for (ClassModel ntgcm : ntg.getClasses()) {
			List<TypeGroup> ltg = classes.get(ntgcm.getFullName());
			if (ltg == null) {
				ltg = new ArrayList<>();
				classes.put(ntgcm.getFullName(), ltg);
			}
			Iterator<TypeGroup> iter = ltg.iterator();
			while (iter.hasNext()) {
				TypeGroup tg = iter.next();
				if (tg.getTag() == null) {
					iter.remove();
				}
			}
			ltg.add(ntg);
		}
		
		for (String t : cm.getGroupTags()) {
			TypeGroup gntg = new TypeGroup();
			gntg.setTag(t);
			for (TypeGroup gtg : groups.keySet()) {
				if (t.equals(gtg.getTag())) {
					for (ClassModel gtgcm : gtg.getClasses()) {
						boolean found = false;
						for (ClassModel gntgcm : gntg.getClasses()) {
							if (gntgcm.getFullName().equals(gtgcm.getFullName())) {
								found = true;
								break;
							}
						}
						if (!found) {
							gntg.getClasses().add(gtgcm);
						}
					}
				}
			}
			
			gntg.getClasses().add(cm);
			
			for (ClassModel ntgcm : gntg.getClasses()) {
				List<TypeGroup> ltg = classes.get(ntgcm.getFullName());
				if (ltg == null) {
					ltg = new ArrayList<>();
					classes.put(ntgcm.getFullName(), ltg);
				}
				Iterator<TypeGroup> iter = ltg.iterator();
				while (iter.hasNext()) {
					TypeGroup tg = iter.next();
					if (t.equals(tg.getTag())) {
						iter.remove();
					}
				}
				ltg.add(gntg);
			}
		}
		
	}
	
	private void defineType(ClassModel cm, boolean full, StringWriter stringWriter, Set<String> definedTypes, List<String> packageNames) {
		
		if (definedTypes.contains(cm.getClsDoc().qualifiedName())) {
			return;
		}
		
		if (packageNames != null && !packageNames.contains(cm.getClsDoc().containingPackage().name())) {
			packageNames.add(cm.getClsDoc().containingPackage().name());
		}
		
		if (cm.getClsDoc().isAnnotationType()) {
			stringWriter.write("annotation ");
			full = false;
		}
		else if (cm.getClsDoc().isInterface()) {
			stringWriter.write("interface ");
		}
		else if (cm.getClsDoc().isEnum()) {
			stringWriter.write("enum ");
		}
		else if (cm.getClsDoc().isAbstract()) {
			stringWriter.write("abstract ");
		}
		else {
			stringWriter.write("class ");
		}
		stringWriter.write(cm.getClsDoc().qualifiedName());
		if (StringUtils.isNotBlank(cm.getUrl())) {
			stringWriter.write(" [[");
			stringWriter.write(DocletMain.getBackRelativePath(output, context.getDirectory()));
			stringWriter.write(cm.getUrl());
			stringWriter.write("]]");
		}
		if (full) {
			stringWriter.write(" {\n");
			for (FieldDoc f : cm.getClsDoc().fields(false)) {
				stringWriter.write("\t");
				if (f.isPublic()) {
					stringWriter.write("+");
				}
				else if (f.isProtected()) {
					stringWriter.write("#");
				}
				else if (f.isPrivate()) {
					stringWriter.write("-");
				}
				else {
					stringWriter.write("~");
				}
				stringWriter.write(f.name());
				stringWriter.write("\n");
			}
			for (MethodDoc m : cm.getClsDoc().methods()) {
				stringWriter.write("\t");
				if (m.isPublic()) {
					stringWriter.write("+");
				}
				else if (m.isProtected()) {
					stringWriter.write("#");
				}
				else {
					stringWriter.write("~");
				}
				stringWriter.write(m.name());
				stringWriter.write("()\n");
			}
			stringWriter.write("}\n");
		}
		stringWriter.write("\n");
		
		definedTypes.add(cm.getClsDoc().qualifiedName());
	}
	
	private void generateTypeReferences(TypeGroup group, int index) {
		
		Set<String> definedTypes = new HashSet<>();
		
		StringWriter stringWriter = new StringWriter();
		stringWriter.write("@startuml\n");
		stringWriter.write("\n");
		for (ClassModel cm : group.getClasses()) {
			defineType(cm, true, stringWriter, definedTypes, null);
		}
		
		for (ClassModel cm : group.getClasses()) {
			for (FieldDoc f : cm.getClsDoc().fields(false)) {
				TypeRef tr = new TypeRef(context, f.type());
				if (tr.getCls() == null || f.type().isPrimitive() || (isCollection(f.type()) == null && f.type().qualifiedTypeName().startsWith("java."))) {
					continue;
				}
				stringWriter.write(cm.getFullName());
				if (isCollection(f.type()) != null || f.type().dimension().startsWith("[]")) {
					stringWriter.write(" \"1\" *-- \"n\" ");
					if (isCollection(f.type()) != null) {
						stringWriter.write(isCollection(f.type()).qualifiedTypeName());
					}
					else if (f.type().dimension().startsWith("[]")) {
						stringWriter.write(f.type().qualifiedTypeName());
					}
					else {
						stringWriter.write("java.lang.Object");
					}
				}
				else {
					stringWriter.write(" -> ");
					stringWriter.write(f.type().qualifiedTypeName());
				}
				stringWriter.write(" : " + f.name() + " >\n");
			}
		}
		
		stringWriter.write("\n");
		stringWriter.write("@enduml\n");
		
		File file = new File(output, "references-" + index + ".uml");
		
		try (FileWriter writer = new FileWriter(file)) {
			
			writer.write(stringWriter.toString());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		File svg = new File(file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf('.')) + ".svg");
		
		try (FileOutputStream fos = new FileOutputStream(svg)) {
			
			SourceStringReader reader = new SourceStringReader(stringWriter.toString());
			reader.generateImage(fos, new FileFormatOption(FileFormat.SVG));
			
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for (ClassModel cm : group.getClasses()) {
			cm.addUmlDiagram(cm.getContextPath() + svg.getAbsolutePath().substring(context.getDirectory().getAbsolutePath().length() + 1), "Class Relations", group.getTag());
		}
		
	}
	
	private Type isCollection(Type type) {
		if (type.asClassDoc() != null) {
			for (Type i : type.asClassDoc().interfaceTypes()) {
				if (i.qualifiedTypeName().equals(Collection.class.getName()) && type.asParameterizedType().typeArguments().length > 0) {
					return type.asParameterizedType() != null ? type.asParameterizedType().typeArguments()[0] : i;
				}
			}
			Type st = type.asClassDoc().superclassType();
			if (st != null) {
				return isCollection(st);
			}
		}
		return null;
	}
	
	public static class TypeGroup {
		
		private String tag;
		private final List<ClassModel> classes = new ArrayList<>();

		public String getTag() {
			return tag;
		}

		public void setTag(String tag) {
			this.tag = tag;
		}

		public List<ClassModel> getClasses() {
			return classes;
		}
		
	}

}
