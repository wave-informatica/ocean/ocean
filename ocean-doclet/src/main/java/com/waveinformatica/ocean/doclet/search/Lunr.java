/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.doclet.search;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * This class is intended to wrap Lunr with a {@link ScriptEngine} to be able to pre-build
 * the documents index to be used at runtime, by the search feature.
 * 
 * @author ivano
 * @see <a href="https://lunrjs.com/">lunrjs.com</a> for details on Lunr
 *
 */
public class Lunr {
	
	private List<IndexElement> index = new ArrayList<>();
	
	public void index(String url, String title, String content, String location) {
		index.add(new IndexElement(url, title, content, location));
	}
	
	public List<IndexElement> getIndex() {
		return index;
	}
	
	public void generateIndex(File output) {
		
		ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
		
		InputStream is = this.getClass().getResourceAsStream("/base-contents/assets/plugins/lunr/lunr.min.js");
		InputStream supportIs = this.getClass().getResourceAsStream("/support/java-lunr.js");
		try {
			
			engine.eval(new InputStreamReader(is, Charset.forName("UTF-8")));
			
			engine.eval(new InputStreamReader(supportIs, Charset.forName("UTF-8")));
			
			Invocable invocable = (Invocable) engine;
			
			Gson gson = new GsonBuilder().create();
			
			Object result = invocable.invokeFunction("loadDocuments", gson.toJson(index));
			
			Map<String, IndexElement> elementsMap = new HashMap<>();
			for (IndexElement el : index) {
				elementsMap.put(el.getUrl(), el);
			}
			
			try (FileOutputStream fos = new FileOutputStream(output)) {
				
				fos.write("var globalIndex=lunr.Index.load(".getBytes("UTF-8"));
				fos.write(result.toString().getBytes("UTF-8"));
				fos.write(");var fullData=".getBytes("UTF-8"));
				fos.write(gson.toJson(elementsMap).getBytes("UTF-8"));
				fos.write(";".getBytes("UTF-8"));
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (ScriptException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static class IndexElement {
		
		private String url;
		private String title;
		private String content;
		private String location;
		
		public IndexElement(String url, String title, String content, String location) {
			super();
			this.url = url;
			this.title = title;
			this.content = content;
			this.location = location;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String text) {
			this.title = text;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		public String getLocation() {
			return location;
		}

		public void setLocation(String location) {
			this.location = location;
		}
		
	}

}
