/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.doclet.parsers;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import com.vladsch.flexmark.ast.Heading;
import com.vladsch.flexmark.ext.gfm.strikethrough.StrikethroughExtension;
import com.vladsch.flexmark.ext.tables.TablesExtension;
import com.vladsch.flexmark.ext.toc.TocBlock;
import com.vladsch.flexmark.ext.toc.TocExtension;
import com.vladsch.flexmark.html.AttributeProvider;
import com.vladsch.flexmark.html.AttributeProviderFactory;
import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.html.HtmlRenderer.Builder;
import com.vladsch.flexmark.html.HtmlRenderer.HtmlRendererExtension;
import com.vladsch.flexmark.html.IndependentAttributeProviderFactory;
import com.vladsch.flexmark.html.renderer.AttributablePart;
import com.vladsch.flexmark.html.renderer.LinkResolverContext;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.ast.NodeVisitor;
import com.vladsch.flexmark.util.ast.VisitHandler;
import com.vladsch.flexmark.util.ast.Visitor;
import com.vladsch.flexmark.util.data.MutableDataHolder;
import com.vladsch.flexmark.util.data.MutableDataSet;
import com.vladsch.flexmark.util.html.Attributes;
import com.waveinformatica.ocean.doclet.ProcessContext;
import com.waveinformatica.ocean.doclet.TemplatesManager;

import freemarker.template.Template;
import freemarker.template.TemplateException;

public class MarkdownParser {

	public MarkdownParser() {
		
	}
	
	public void parse(File file, File output, String contextPath, TemplatesManager templates, ProcessContext ctx) throws IOException {
		
		MutableDataSet options = new MutableDataSet();
		
		options.set(Parser.EXTENSIONS, Arrays.asList(
				TablesExtension.create(),
				StrikethroughExtension.create(),
				TocExtension.create(),
				TocNavExtension.create()
		))
		.set(TablesExtension.CLASS_NAME, "table table-bordered table-striped table-condensed");
		
		Parser parser = Parser.builder(options).build();
		HtmlRenderer renderer = HtmlRenderer.builder(options).build();
		
		try (FileReader reader = new FileReader(file)) {
			
			Node document = parser.parseReader(reader);
			
			FindAndRemoveVisitor<Heading> titleVisitor = new FindAndRemoveVisitor<>();
			FindAndRemoveVisitor<TocBlock> tocVisitor = new FindAndRemoveVisitor<>();
			
			NodeVisitor visitor = new NodeVisitor(
					new VisitHandler<>(Heading.class, titleVisitor),
					new VisitHandler<>(TocBlock.class, tocVisitor)
			);
			
			visitor.visit(document);
			
			WikiTemplateModel model = new WikiTemplateModel();
			model.setContextPath(contextPath);
			if (titleVisitor.getElement() != null) {
				model.setTitle(titleVisitor.getElement().getText().toString());
			}
			if (tocVisitor.getElement() != null) {
				model.setTocContent(renderer.render(tocVisitor.getElement()));
			}
			titleVisitor.remove();
			tocVisitor.remove();
			model.setContent(renderer.render(document));
			
			String url = "";
			File tmp = output.getParentFile();
			for (String s : contextPath.split("/")) {
				if (url.length() >= 0) {
					url = "/" + url;
				}
				url = tmp.getName() + url;
				tmp = tmp.getParentFile();
			}
			if (url.startsWith("/")) {
				url = url.substring(1);
			}
			ctx.getLunr().index(url + output.getName(), titleVisitor.getElement().getText().toString(), model.getContent(), "manual " + output.getName());
			
			Template tpl = templates.getTemplate("/templates/wiki.tpl");
			
			try (FileWriter writer = new FileWriter(output)) {
				
				tpl.process(model, writer);
				
			} catch (TemplateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
	
	public static class FindAndRemoveVisitor<T extends Node> implements Visitor<T> {
		
		private T element = null;
		
		@Override
		public void visit(T node) {
			if (element == null) {
				element = node;
			}
		}
		
		public void remove() {
			if (element != null) {
				element.unlink();
			}
		}

		public T getElement() {
			return element;
		}
		
	}
	
	public static class TocNavExtension implements HtmlRendererExtension {

		@Override
		public void rendererOptions(MutableDataHolder options) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void extend(Builder rendererBuilder, String rendererType) {
			rendererBuilder.attributeProviderFactory(TocNavAttributeProvider.Factory());
		}
		
		static TocNavExtension create() {
			return new TocNavExtension();
		}
		
	}
	
	public static class TocNavAttributeProvider implements AttributeProvider {

		@Override
		public void setAttributes(Node node, AttributablePart part, Attributes attributes) {
			if (node instanceof TocBlock) {
				if (node.getAncestorOfType(TocBlock.class) == null) {
					attributes.addValue("class", "nav doc-menu");
					//attributes.addValue("data-spy", "affix");
					attributes.addValue("id", "doc-menu");
				}
			}
		}

        static AttributeProviderFactory Factory() {
            return new IndependentAttributeProviderFactory() {
				@Override
				public AttributeProvider apply(LinkResolverContext arg0) {
					return new TocNavAttributeProvider();
				}
            };
        }
		
	}
	
}
