/*
 * Copyright 2017, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.doclet;

import java.io.File;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.waveinformatica.ocean.doclet.cache.CacheableModel;
import com.waveinformatica.ocean.doclet.search.Lunr;

/**
 *
 * @author ivano
 */
public class ProcessContext {
	
	private File sourceRoot;
	private File directory;
	private final Set<File> sourcePaths = new HashSet<>();
	private final Set<URL> externalLinks = new HashSet<>();
	private final Map<String, ModuleDoc> modules = new HashMap<>();
	private ModuleDoc rootModule;
	private final Lunr lunr = new Lunr();
	
	private String projectName;
	private String projectVersion;
	private String projectDescription;
	private String organization;
	private String organizationUrl;
	
	private final Map<String, CacheableModel> modelsCache = new HashMap<>();
	
	public <T extends CacheableModel> T getModel(Class<T> cls, Object...params) {
		
		Class[] types = new Class[params.length];
		for (int i = 0; i < params.length; i++) {
			Object o = params[i];
			if (o != null) {
				types[i] = o.getClass();
			}
			else {
				types[i] = null;
			}
		}
		
		Constructor constr = null;
		ext:
		for (Constructor c : cls.getConstructors()) {
			Class[] paramTypes = c.getParameterTypes();
			if (paramTypes.length != types.length) {
				continue;
			}
			for (int i = 0; i < paramTypes.length; i++) {
				if (params[i] != null && !paramTypes[i].isAssignableFrom(types[i])) {
					continue ext;
				}
			}
			constr = c;
			break;
		}
		
		if (constr != null) {
			try {
				T res = (T) constr.newInstance(params);
				String k = res.getKey();
				T cached = (T) modelsCache.get(k);
				if (cached != null) {
					return cached;
				}
				else {
					modelsCache.put(k, res);
					return res;
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		
		throw new RuntimeException("Unable to find a suitable constructor");
		
	}
	
	public Collection<CacheableModel> getCacheableModels() {
		return modelsCache.values();
	}

	public File getSourceRoot() {
		return sourceRoot;
	}

	public void setSourceRoot(File sourceRoot) {
		this.sourceRoot = sourceRoot;
	}

	public File getDirectory() {
		return directory;
	}

	public void setDirectory(File directory) {
		this.directory = directory;
	}

	public Set<File> getSourcePaths() {
		return sourcePaths;
	}

	public Set<URL> getExternalLinks() {
		return externalLinks;
	}

	public Map<String, ModuleDoc> getModules() {
		return modules;
	}

	public ModuleDoc getRootModule() {
		return rootModule;
	}

	public void setRootModule(ModuleDoc rootModule) {
		this.rootModule = rootModule;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectVersion() {
		return projectVersion;
	}

	public void setProjectVersion(String projectVersion) {
		this.projectVersion = projectVersion;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getOrganizationUrl() {
		return organizationUrl;
	}

	public void setOrganizationUrl(String organizationUrl) {
		this.organizationUrl = organizationUrl;
	}
	
	public Lunr getLunr() {
		return lunr;
	}
}
