/*
* Copyright 2015, 2017 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */
package com.waveinformatica.ocean.doclet;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

/**
 *
 * @author Ivano
 */
public class TemplateLoader implements freemarker.cache.TemplateLoader {

	private static long start = System.currentTimeMillis();
	
	@Override
	public Object findTemplateSource(String string) throws IOException {
		
		URL url = TemplateLoader.class.getClassLoader().getResource(string);
		
		return url;

	}

	@Override
	public long getLastModified(Object o) {
		return start;
	}

	@Override
	public Reader getReader(Object o, String string) throws IOException {
		return new InputStreamReader(((URL) o).openStream(), Charset.forName("UTF-8"));
	}

	@Override
	public void closeTemplateSource(Object o) throws IOException {
		
	}

}
