/*
 * Copyright 2017, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.doclet.discover.maven;

import com.waveinformatica.ocean.doclet.ModuleDoc;
import com.waveinformatica.ocean.doclet.ProcessContext;
import com.waveinformatica.ocean.doclet.discover.InfoDiscover;

import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.maven.model.Build;
import org.apache.maven.model.CiManagement;
import org.apache.maven.model.Contributor;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.DependencyManagement;
import org.apache.maven.model.Developer;
import org.apache.maven.model.DistributionManagement;
import org.apache.maven.model.InputLocation;
import org.apache.maven.model.IssueManagement;
import org.apache.maven.model.License;
import org.apache.maven.model.MailingList;
import org.apache.maven.model.Model;
import org.apache.maven.model.Organization;
import org.apache.maven.model.Parent;
import org.apache.maven.model.Prerequisites;
import org.apache.maven.model.Profile;
import org.apache.maven.model.Reporting;
import org.apache.maven.model.Repository;
import org.apache.maven.model.Scm;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.StringUtils;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

/**
 *
 * @author ivano
 */
public class MavenInfoDiscover implements InfoDiscover {

	@Override
	public ModuleDoc discover(ProcessContext context) {
		
		try {
			
			if (context.getSourceRoot() == null) {
				File f = context.getSourcePaths().iterator().next();
				File lastPom = null;
				do {
					File pom = new File(f, "pom.xml");
					if (pom.exists()) {
						lastPom = pom;
					}
					f = f.getParentFile();
				} while (f != null);
				
				if (lastPom == null) {
					return null;
				}
				else {
					context.setSourceRoot(lastPom.getParentFile());
				}
			}
			
			File rootPom = new File(context.getSourceRoot(), "pom.xml");
			
			MavenXpp3Reader reader = new MavenXpp3Reader();
			Model model = reader.read(new FileReader(rootPom));
			
			MavenProcessModel mpm = new MavenProcessModel();
			mpm.setProject(model);
			
			context.setProjectName(process(mpm, model.getName()));
			context.setProjectVersion(process(mpm, model.getVersion()));
			context.setProjectDescription(process(mpm, model.getDescription()));
			if (model.getOrganization() != null) {
				context.setOrganization(process(mpm, model.getOrganization().getName()));
				context.setOrganizationUrl(process(mpm, model.getOrganization().getUrl()));
			}
			
			ModuleDoc rootModule = processPom(reader, rootPom, null, context, null);
			
			return rootModule;
			
		} catch (Exception ex) {
			Logger.getLogger(MavenInfoDiscover.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		return null;
		
	}
	
	private ModuleDoc processPom(MavenXpp3Reader reader, File pom, ModuleDoc parent, ProcessContext context, MavenProjectReader parentReader) {
		
		ModuleDoc current = new ModuleDoc();
		
		try (FileReader pomReader = new FileReader(pom)) {
			
			Model model = reader.read(pomReader);
			
			MavenProcessModel mpm = new MavenProcessModel();
			mpm.setProject(new MavenProjectReader(this, model, parentReader));
			
			current.setParent(parent);
			
			current.setName(process(mpm, model.getName()));
			if (parent != null && pom.getParent().startsWith(parent.getBasePath().getAbsolutePath())) {
				current.setPathName(pom.getParent().substring(parent.getBasePath().getAbsolutePath().length()));
			}
			else {
				current.setPathName("");
			}
			current.setBasePath(pom.getParentFile());
			
			if ("pom".equals(model.getPackaging())) {
				current.setIconClass("fa fa-folder");
				current.setType("POM");
			}
			else if ("war".equals(model.getPackaging())) {
				current.setIconClass("fa fa-globe");
				current.setType("WAR");
			}
			else if ("jar".equals(model.getPackaging()) || StringUtils.isBlank(model.getPackaging())) {
				current.setIconClass("fa fa-cogs");
				current.setType("JAR");
			}
			else if ("maven-archetype".equals(model.getPackaging())) {
				current.setIconClass("fa fa-bolt");
				current.setType("Maven Archetype");
			}
			else if ("maven-plugin".equals(model.getPackaging())) {
				current.setIconClass("fa fa-cubes");
				current.setType("Maven Plugin");
			}
			current.setDescription(mpm.getProject().getDescription());
			
			fillSources(pom.getParentFile(), current);
			
			if (model.getPackaging().equals("pom")) {
				for (String m : model.getModules()) {
					File modPom = new File(current.getBasePath(), m + "/pom.xml");
					ModuleDoc subModule = processPom(reader, modPom, current, context, (MavenProjectReader) mpm.getProject());
					if (subModule != null) {
						current.getSubModules().add(subModule);
					}
				}
			}
			
			return current;
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	private void fillSources(File base, ModuleDoc mod) {
		
		File src = new File(base, "src");
		if (!src.isDirectory()) {
			return;
		}
		
		for (File d : src.listFiles()) {
			if (d.getName().equalsIgnoreCase("main")) {
				for (File sd : d.listFiles()) {
					if (sd.isDirectory()) {
						mod.getDirectories().add(sd);
					}
				}
			}
			else if (d.getName().equalsIgnoreCase("site")) {
				mod.getDirectories().add(d);
			}
		}
		
	}
	
	private String process(MavenProcessModel mpm, String value) {
		
		if (value == null) {
			return "";
		}
		
		try {
			
			Template tpl = new Template("", new StringReader(value));
			
			StringWriter writer = new StringWriter();
			
			tpl.process(mpm, writer);
			
			return writer.toString();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return value;
		} catch (TemplateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return value;
		}
		
	}
	
	public static class MavenProcessModel {
		
		private Model project;

		public Model getProject() {
			return project;
		}

		public void setProject(Model project) {
			this.project = project;
		}
		
	}
	
	public static class MavenProjectReader extends Model {
		
		private final MavenInfoDiscover discover;
		private final Model project;
		private final MavenProjectReader parent;
		private final MavenProcessModel process;

		public MavenProjectReader(MavenInfoDiscover discover, Model project) {
			super();
			this.discover = discover;
			this.project = project;
			this.parent = null;
			this.process = new MavenProcessModel();
			this.process.setProject(this);
		}

		public MavenProjectReader(MavenInfoDiscover discover, Model project, MavenProjectReader parent) {
			super();
			this.discover = discover;
			this.project = project;
			this.parent = parent;
			this.process = new MavenProcessModel();
			this.process.setProject(this);
		}

		public List<Dependency> getDependencies() {
			List<Dependency> values = project.getDependencies();
			if (parent != null && (values == null || values.isEmpty())) {
				return parent.getDependencies();
			}
			return values;
		}

		public DependencyManagement getDependencyManagement() {
			DependencyManagement value = project.getDependencyManagement();
			if (value == null && parent != null) {
				return parent.getDependencyManagement();
			}
			return value;
		}

		public DistributionManagement getDistributionManagement() {
			DistributionManagement value = project.getDistributionManagement();
			if (value == null && parent != null) {
				return parent.getDistributionManagement();
			}
			return value;
		}

		public InputLocation getLocation(Object key) {
			InputLocation value = project.getLocation(key);
			if (value == null && parent != null) {
				return parent.getLocation(key);
			}
			return value;
		}

		public List<String> getModules() {
			return project.getModules();
		}

		public List<Repository> getPluginRepositories() {
			return project.getPluginRepositories();
		}

		public Properties getProperties() {
			Properties value = new Properties();
			if (parent != null && parent.getProperties() != null) {
				value.putAll(parent.getProperties());
			}
			if (project.getProperties() != null) {
				value.putAll(project.getProperties());
			}
			return value;
		}

		public Reporting getReporting() {
			return project.getReporting();
		}

		public String getArtifactId() {
			return project.getArtifactId();
		}

		public Object getReports() {
			return project.getReports();
		}

		public Build getBuild() {
			return project.getBuild();
		}

		public List<Repository> getRepositories() {
			return project.getRepositories();
		}

		public CiManagement getCiManagement() {
			return project.getCiManagement();
		}

		public List<Contributor> getContributors() {
			return project.getContributors();
		}

		public String getDescription() {
			String value = project.getDescription();
			if (StringUtils.isBlank(value) && parent != null) {
				return parent.getDescription();
			}
			return value;
		}

		public List<Developer> getDevelopers() {
			return project.getDevelopers();
		}

		public String getGroupId() {
			String groupId = project.getGroupId();
			if (StringUtils.isBlank(groupId) && parent != null) {
				return parent.getGroupId();
			}
			return groupId;
		}

		public String getInceptionYear() {
			return project.getInceptionYear();
		}

		public IssueManagement getIssueManagement() {
			return project.getIssueManagement();
		}

		public List<License> getLicenses() {
			return project.getLicenses();
		}

		public List<MailingList> getMailingLists() {
			return project.getMailingLists();
		}

		public String getModelEncoding() {
			return project.getModelEncoding();
		}

		public String getModelVersion() {
			return project.getModelVersion();
		}

		public String getName() {
			return discover.process(process, project.getName());
		}

		public Organization getOrganization() {
			return project.getOrganization();
		}

		public String getPackaging() {
			return project.getPackaging();
		}

		public Parent getParent() {
			return project.getParent();
		}

		public Prerequisites getPrerequisites() {
			return project.getPrerequisites();
		}

		public List<Profile> getProfiles() {
			return project.getProfiles();
		}

		public Scm getScm() {
			return project.getScm();
		}

		public String getUrl() {
			return project.getUrl();
		}

		public String getVersion() {
			String value = project.getVersion();
			if (StringUtils.isBlank(value) && parent != null) {
				return parent.getVersion();
			}
			return value;
		}

		public File getPomFile() {
			return project.getPomFile();
		}

		public File getProjectDirectory() {
			return project.getProjectDirectory();
		}

		public String getId() {
			return project.getId();
		}
		
	}
	
}
