/*
* Copyright 2015, 2018 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */
package com.waveinformatica.ocean.doclet;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.waveinformatica.ocean.doclet.templates.TrimDirective;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.util.Locale;

/**
 *
 * @author Ivano
 */
public class TemplatesManager {

	private static final String DEFAULT_ENCODING = "UTF-8";

	private TemplatesConfiguration config;

	public TemplatesManager() {

		config = new TemplatesConfiguration();

		config.setDefaultEncoding(DEFAULT_ENCODING);
		config.setURLEscapingCharset(DEFAULT_ENCODING);
		config.setLocalizedLookup(false);

		config.setTemplateLoader(new TemplateLoader());
		
		config.addAutoImport("utils", "/macros/utils.tpl");
		
		config.setSharedVariable("trim", new TrimDirective());

	}

	public Template getTemplate(String name) {
		try {

			Template tpl = config.getTemplate(name, Locale.getDefault());
			if (tpl != null) {
				try {
					tpl.setSetting("locale", getFreemarkerLocale(Locale.getDefault()));
				} catch (TemplateException ex) {
					Logger.getLogger(TemplatesManager.class.getName()).log(Level.WARNING, "Unable to set locale for template: " + getFreemarkerLocale(Locale.getDefault()), ex);
				}
			}
			return tpl;

		} catch (IOException ex) {
			Logger.getLogger(TemplatesManager.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}
	}

	public Configuration getConfig() {
		return config;
	}

	private String getFreemarkerLocale(Locale locale) {
		StringBuilder builder = new StringBuilder();
		builder.append(locale.getLanguage());
		if (locale.getCountry().length() > 0) {
			builder.append('_');
			builder.append(locale.getCountry());
		}
		return builder.toString();
	}
}
