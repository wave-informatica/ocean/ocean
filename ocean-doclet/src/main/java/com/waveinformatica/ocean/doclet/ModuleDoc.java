/*
 * Copyright 2017, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.doclet;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.PackageDoc;

/**
 *
 * @author ivano
 */
public class ModuleDoc {
	
	private ModuleDoc parent;
	private String name;
	private String pathName;
	private final List<ModuleDoc> subModules = new ArrayList<>();
	private PackageDoc rootPackage;
	private final List<PackageDoc> packages = new ArrayList<>();
	private final List<ClassDoc> classes = new ArrayList<>();
	private File basePath;
	private final Set<File> directories = new HashSet<File>();
	private File outputPath;
	private String iconClass;
	private String type;
	private String description;

	public ModuleDoc getParent() {
		return parent;
	}

	public void setParent(ModuleDoc parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPathName() {
		return pathName;
	}

	public void setPathName(String pathName) {
		this.pathName = pathName;
	}

	public List<ModuleDoc> getSubModules() {
		return subModules;
	}

	public PackageDoc getRootPackage() {
		return rootPackage;
	}

	public void setRootPackage(PackageDoc rootPackage) {
		this.rootPackage = rootPackage;
	}

	public List<PackageDoc> getPackages() {
		return packages;
	}

	public List<ClassDoc> getClasses() {
		return classes;
	}

	public File getBasePath() {
		return basePath;
	}

	public void setBasePath(File basePath) {
		this.basePath = basePath;
	}

	public Set<File> getDirectories() {
		return directories;
	}

	public File getOutputPath() {
		return outputPath;
	}

	public void setOutputPath(File outputPath) {
		this.outputPath = outputPath;
	}

	public String getIconClass() {
		return iconClass;
	}

	public void setIconClass(String iconClass) {
		this.iconClass = iconClass;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		if (parent != null) {
			return parent.toString() + "/" + name;
		}
		else {
			return name;
		}
	}
	
}
