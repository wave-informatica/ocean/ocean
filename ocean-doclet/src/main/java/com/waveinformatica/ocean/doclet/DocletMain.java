/*
* Copyright 2017, 2018 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.doclet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.LanguageVersion;
import com.sun.javadoc.PackageDoc;
import com.sun.javadoc.RootDoc;
import com.waveinformatica.ocean.doclet.cache.CacheableModel;
import com.waveinformatica.ocean.doclet.components.ComponentsBuilder;
import com.waveinformatica.ocean.doclet.discover.maven.MavenInfoDiscover;
import com.waveinformatica.ocean.doclet.javadocs.JavadocBuilder;
import com.waveinformatica.ocean.doclet.parsers.MarkdownParser;
import com.waveinformatica.ocean.doclet.uml.UmlGenerator;

import freemarker.core.Environment;
import freemarker.ext.beans.BeansWrapper;
import freemarker.ext.beans.StringModel;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 *
 * @author ivano
 */
public class DocletMain {
	
	private static final ProcessContext context = new ProcessContext();
	private static final TemplatesManager templates = new TemplatesManager();
	
	public static LanguageVersion languageVersion() {
	    return LanguageVersion.JAVA_1_5;
	}
	
	public static boolean start(RootDoc root) {
		
		readOptions(root.options());
		
		discoverInfo();
		
		initModule(root, context.getRootModule());
		
		linkModules();
		
		prepareStructure();
		
		buildModule(context.getRootModule());
		
		writePackageList();
		
		context.getLunr().generateIndex(new File(context.getDirectory(), "assets/js/lunr-index.js"));
		
		return true;
		
	}
	
	private static void readOptions(String[][] options) {
		for (int i = 0; i < options.length; i++) {
			String[] opt = options[i];
			if (opt[0].equals("-d")) {
				context.setDirectory(new File(opt[1]));
			}
			else if (opt[0].equals("-link")) {
				try {
					System.out.println("-link: " + opt[1]);
					context.getExternalLinks().add(new URL(opt[1]));
				} catch (MalformedURLException ex) {
					
				}
			}
			else if (opt[0].equals("-sourcepath")) {
				String[] paths = opt[1].split(File.pathSeparator);
				for (String s : paths) {
					context.getSourcePaths().addAll(processSourcePath(s));
				}
			}
			else if (opt[0].equals("-root")) {
				context.setSourceRoot(new File(opt[1]));
			}
		}
	}
	
	private static Set<File> processSourcePath(String path) {
		
		Set<File> dirs = new HashSet<>();
		
		File f = new File(path);
		if (f.isDirectory()) {
			dirs.add(f);
		}
		if (f.exists() && f.getParentFile() != null && f.getParentFile().exists()) {
			f = new File(f.getParentFile(), "wiki");
			if (f.isDirectory()) {
				dirs.add(f);
			}
		}
		
		return dirs;
		
	}
	
	private static void discoverInfo() {
		
		if (context.getDirectory() == null) {
			context.setDirectory(new File("."));
		}
		if (context.getSourceRoot() == null && context.getDirectory().getAbsoluteFile().getParentFile().getName().equals("target")) {
			context.setSourceRoot(context.getDirectory().getParentFile().getParentFile());
		}
		
		MavenInfoDiscover mvn = new MavenInfoDiscover();
		ModuleDoc rootMod = mvn.discover(context);
		rootMod.setOutputPath(context.getDirectory());
		
		if (rootMod != null) {
			context.setRootModule(rootMod);
		}
		
	}
	
	private static void initModule(RootDoc root, ModuleDoc mod) {
		
		for (ClassDoc cd : root.classes()) {
			for (File src : mod.getDirectories()) {
				if (src != null && cd.position() != null && cd.position().file() != null && cd.position().file().getAbsolutePath().startsWith(src.getAbsolutePath())) {
					
					mod.getClasses().add(cd);
					
					boolean packageAdded = false;
					for (PackageDoc pd : mod.getPackages()) {
						if (pd.name().equals(cd.containingPackage().name())) {
							packageAdded = true;
							break;
						}
					}
					if (!packageAdded) {
						mod.getPackages().add(cd.containingPackage());
					}
				}
			}
		}
		
		File output = context.getDirectory();
		if (mod.getParent() != null) {
			output = new File(mod.getParent().getOutputPath(), "components");
			output = new File(output, mod.getPathName());
		}
		if (!output.exists()) {
			output.mkdirs();
		}
		mod.setOutputPath(output);
		
		JavadocBuilder builder = new JavadocBuilder(context, templates, mod, new File(output, "javadocs"));
		builder.initCache();
		
		for (ModuleDoc md : mod.getSubModules()) {
			context.getModules().put(mod.getName(), mod);
			initModule(root, md);
		}
		
	}
	
	private static void linkModules() {
		for (CacheableModel cm : new ArrayList<>(context.getCacheableModels())) {
			if (!cm.isLinked()) {
				cm.link(context);
			}
		}
	}
	
	private static void prepareStructure() {
		
		try {
			
			Enumeration<URL> resources = DocletMain.class.getClassLoader().getResources("base-contents/");
			while (resources.hasMoreElements()) {
				
				URL url = resources.nextElement();
				
				String resPath = url.getFile().substring("file:/".length());
				if (!resPath.startsWith("/")) {
					resPath = "/" + resPath;
				}
				String inPath = resPath.substring(resPath.indexOf('!') + 1);
				resPath = resPath.substring(0, resPath.indexOf('!'));
				List<String> files = findResourcesInJar(resPath, inPath);
				
				try (JarFile jar = new JarFile(new File(resPath))) {
					for (String f : files) {
						File newFile = new File(context.getDirectory(), f);
						if (f.endsWith("/")) {
							newFile.mkdirs();
						}
						else {
							try (FileOutputStream fow = new FileOutputStream(newFile)) {
								JarEntry entry = (JarEntry) jar.getEntry("base-contents/" + f);
								InputStream is = jar.getInputStream(entry);
								byte[] buff = new byte[4096];
								int read;
								while ((read = is.read(buff)) > 0) {
									fow.write(buff, 0, read);
								}
								is.close();
							}
						}
					}
				}
				
			}
			
		} catch (IOException ex) {
			Logger.getLogger(DocletMain.class.getName()).log(Level.SEVERE, null, ex);
		}
		
	}
	
	private static void buildModule(ModuleDoc module) {
		
		buildIndex(module);
		
		buildComponents(module);
		
		buildApiDocs(module);
		
		buildWiki(module);
		
		for (ModuleDoc md : module.getSubModules()) {
			buildModule(md);
		}
		
	}
	
	private static void buildIndex(ModuleDoc module) {
		
		try (FileOutputStream fos = new FileOutputStream(new File(module.getOutputPath(), "index.html"))) {
			
			try (OutputStreamWriter writer = new OutputStreamWriter(fos)) {

				Template tpl = templates.getTemplate("/templates/index.tpl");
				
				Environment env = tpl.createProcessingEnvironment(module, writer);
				
				env.setGlobalVariable("contextPath", new StringModel(getBackRelativePath(module.getOutputPath(), context.getDirectory()), (BeansWrapper) env.getObjectWrapper()));
				
				env.process();

			}
			
		} catch (IOException ex) {
			Logger.getLogger(DocletMain.class.getName()).log(Level.SEVERE, null, ex);
		} catch (TemplateException ex) {
			Logger.getLogger(DocletMain.class.getName()).log(Level.SEVERE, null, ex);
		}
		
	}
	
	private static List<String> findResourcesInJar(String jarFile, String path) {

		path = path.substring(1);

		List<String> resources = new ArrayList<String>();

		try {

			if (jarFile.indexOf('%') != -1) {
				jarFile = URLDecoder.decode(jarFile, "UTF-8");
			}

			JarFile jar = new JarFile(jarFile);

			Enumeration<JarEntry> entries = jar.entries();
			while (entries.hasMoreElements()) {
				JarEntry entry = entries.nextElement();
				if (entry.getName().startsWith(path)) {
					String resName = entry.getName().substring(path.length());
					if (resName.length() > 0) {
						resources.add(resName);
					}
				}
			}

			jar.close();

		} catch (IOException ex) {
			Logger.getLogger(DocletMain.class.getName()).log(Level.SEVERE, null, ex);
		}

		return resources;
	}
	
	private static void buildWiki(ModuleDoc module) {
		
		for (File f : module.getDirectories()) {
			if (f.getAbsolutePath().endsWith("/wiki")) {
				buildWikiFile(module, f, "");
			}
		}
		
	}
	
	private static void buildWikiFile(ModuleDoc module, File f, String suffix) {
		
		if (f.isDirectory()) {
			
			for (File c : f.listFiles()) {
				
				if (c.isDirectory()) {
					buildWikiFile(module, c, suffix + "/" + c.getName());
				}
				else if (c.getName().endsWith(".md")) {
					buildWikiFile(module, c, suffix);
				}
				
			}
			
		}
		else {
			
			if (!suffix.startsWith("/")) {
				suffix = "/" + suffix;
			}
			
			File output = new File(module.getOutputPath(), "wiki" + suffix);
			if (!output.exists()) {
				output.mkdirs();
			}
			output = new File(output, f.getName().substring(0, f.getName().lastIndexOf('.')) + ".html");
			
			MarkdownParser mdp = new MarkdownParser();
			try {
				mdp.parse(f, output, getBackRelativePath(output, context.getDirectory()), templates, context);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
	
	public static void buildComponents(ModuleDoc module) {
		
		ComponentsBuilder builder = new ComponentsBuilder(context, module);
		
		try (FileOutputStream fos = new FileOutputStream(new File(module.getOutputPath(), "components.html"))) {
			
			try (OutputStreamWriter writer = new OutputStreamWriter(fos)) {

				Template tpl = templates.getTemplate("/templates/components.tpl");
				
				tpl.process(builder, writer);

			}
			
		} catch (IOException ex) {
			Logger.getLogger(DocletMain.class.getName()).log(Level.SEVERE, null, ex);
		} catch (TemplateException ex) {
			Logger.getLogger(DocletMain.class.getName()).log(Level.SEVERE, null, ex);
		}
		
	}
	
	public static void buildApiDocs(ModuleDoc module) {
		
		File umlOutput = new File(module.getOutputPath(), "uml");
		if (!umlOutput.exists()) {
			umlOutput.mkdirs();
		}
		
		UmlGenerator umlGenerator = new UmlGenerator(context, module, umlOutput);
		umlGenerator.generate();
		
		File output = new File(module.getOutputPath(), "javadocs");
		if (!output.exists()) {
			output.mkdirs();
		}
		
		JavadocBuilder builder = new JavadocBuilder(context, templates, module, output);
		builder.build();
		
	}
	
	private static void writePackageList() {
		for (String key : context.getModules().keySet()) {
			writeModulePackageList(context.getModules().get(key));
		}
	}
	
	private static void writeModulePackageList(ModuleDoc md) {
		
		File f = new File(md.getOutputPath(), "javadocs");
		if (!f.exists()) {
			f.mkdirs();
		}
		
		try (FileWriter writer = new FileWriter(new File(f, "package-list"))) {
			for (PackageDoc pd : md.getPackages()) {
				writer.write(pd.name() + "\r\n");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (ModuleDoc smd : md.getSubModules()) {
			writeModulePackageList(smd);
		}
	}
	
	public static String getBackRelativePath(File from, File to) {
		if (!from.getAbsolutePath().startsWith(to.getAbsolutePath())) {
			return "";
		}
		StringBuilder contextPath = new StringBuilder();
		File tmp = from;
		if (tmp.isFile() || !tmp.exists()) {
			tmp = tmp.getParentFile();
		}
		while (tmp != null && tmp.getAbsolutePath() != null && !tmp.getAbsolutePath().equals(to.getAbsolutePath())) {
			tmp = tmp.getParentFile();
			contextPath.append("../");
		}
		return contextPath.toString();
	}
	
}
