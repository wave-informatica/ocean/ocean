<#include "/templates/includes/header.tpl">

<div class="doc-content">
	
	<div class="content-inner">
		
		${content!}
		
	</div>
	
</div>

<div class="doc-sidebar hidden-xs">
	
	<nav id="doc-nav">
		
		${tocContent!"Nessun contenuto"}
		
	</nav>
	
</div>

<#include "/templates/includes/footer.tpl">