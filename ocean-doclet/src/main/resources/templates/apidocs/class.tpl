<#include "/templates/includes/header.tpl">

<div class="doc-content">
	
	<div class="content-inner">
		
        <section id="hierarchy" class="doc-section">
        	<h2 class="section-title">Hierarchy</h2>
            <div class="section-block">
            	
            	<#list hierarchy as cls>
            	<ul class="list">
            		<li><@utils.typeRef object=cls full=true />
            	</#list>
            			<ul class="list">
            				<li><strong>${fullName}</strong><#if arguments?has_content><@utils.typeVar arguments=arguments /></#if></li>
            			</ul>
            	<#list hierarchy as cls>
            		</li>
            	</ul>
            	</#list>
            	
            	<dl>
            		<#if implInterfaces?has_content>
            		<dt>All Implemented Interfaces:</dt>
            		<dd>
            			<#list implInterfaces as iface><@utils.typeRef object=iface /><#sep>, </#sep></#list>
            		</dd>
            		</#if>
            		<#if subClasses?has_content>
            		<dt>Direct Known Subclasses:</dt>
            		<dd>
            			<#list subClasses as iface><@utils.link object=iface><span title="${iface.fullName}">${iface.name}</span></@utils.link><#sep>, </#sep></#list>
            		</dd>
            		</#if>
            		<#if enclosingClass??>
            		<dt>Enclosing Class:</dt>
            		<dd>
            			<@utils.link object=enclosingClass><span title="${enclosingClass.fullName}">${enclosingClass.name}</span></@utils.link>
            		</dd>
            		</#if>
            	</dl>
            	
            	<#if umlDiagrams?has_content>
            	<div class="section-block" id="uml-diagrams">
            		<h3>UML diagrams</h3>
            		<div class="row">
            			<#list umlDiagrams as file>
            			<div class="col-md-4">
            				<h6>${file.title}<#if file.tag?has_content> &middot; <a href="#${file.tag}">#${file.tag}</a></#if></h6>
		            		<div class="screenshot-holder">
		                        <a href="${file.url}" data-title="${file.title}" data-toggle="lightbox"><img class="img-responsive" src="${file.url}" alt="${file.title}" /></a>
		                        <a class="mask" href="${file.url}" data-title="${file.title}" data-toggle="lightbox"><i class="icon fa fa-search-plus"></i></a>
		                    </div>
		                </div> 
		                <#if file?index % 3 == 2 && !file?is_last>
		                </div><div class="row">
		                </#if>
            			</#list>
            		</div>
            	</div>
            	</#if>
            	
            </div>
        </section>
		
        <section id="declaration" class="doc-section">
        	<h2 class="section-title">Description</h2>
            <div class="section-block">
            	
            	<pre class="language-java"><code class="language-java"><#list classAnnotations as ann><@utils.annotation object=ann />
<@trim>			</@trim></#list>${clsDoc.modifiers()} ${declaration} ${clsDoc.name()}</code></pre>
            	
            	<p>${comment!}</p>
            	
            	<#if blockTags?has_content>
            	<dl>
            	<#list blockTags as bt>
            		<dt>${bt.name()}</dt>
            		<dd>${bt.text()}</dd>
            	</#list>
            	</dl>
            	</#if>
            	
            </div>
        </section>
        
        <#if enumConstants?has_content>
        <section id="enum-constants" class="doc-section">
        	<h2 class="section-title">Enum Constants</h2>
            <div id="fields-protected" class="section-block">
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Constant</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list enumConstants as const>
                		<tr>
                			<th>
                				<a href="#enum-constants-${const?index}">${const.name}</a>
                			</th>
                			<td>
                				${const.summaryComment}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
            </div>
        </section>
        </#if>
        
        <#if protectedFields?has_content || publicFields?has_content>
        <section id="fields" class="doc-section">
        	<h2 class="section-title">Fields Summary</h2>
        	<#if protectedFields?has_content>
            <div id="fields-protected" class="section-block">
            	<h3 class="block-title">Protected Fields</h3>
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Field</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list protectedFields as field>
                		<tr>
                			<th>
                				${field.declaration}
                				<@utils.typeRef object=field.typeRef full=false />
                				<a href="#fields-protected-${field?index}" class="scrollto">${field.name}</a>
                			</th>
                			<td>
                				${field.summaryComment}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
            </div>
            </#if>
            <#if publicFields?has_content>
            <div id="fields-public" class="section-block">
            	<h3 class="block-title">Public Fields</h3>
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Field</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list publicFields as field>
                		<tr>
                			<th>
                				${field.declaration}
                				<@utils.typeRef object=field.typeRef full=false />
                				<a href="#fields-public-${field?index}" class="scrollto">${field.name}</a>
                			</th>
                			<td>
                				${field.summaryComment}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
            </div>
            </#if>
        </section>
        </#if>
        
        <#if constructors?has_content>
        <section id="constructors" class="doc-section">
        	<h2 class="section-title">Constructors Summary</h2>
            <div class="section-block">
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Constructor</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list constructors as constructor>
                		<tr>
                			<th>
                				${constructor.declaration}
                				<a class="scrollto" href="#constructors-${constructor?index}(<#list constructor.parameters as param>${param.type.simpleName}<#sep>,</#sep></#list>)">${constructor.name}</a>
                				(
                				<#list constructor.parameters as param>
                					<@utils.typeRef object=param.type full=false /><#sep>, </#sep>
                				</#list>
                				)
                			</th>
                			<td>
                				${constructor.summaryComment}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
            </div>
        </section>
        </#if>
        
        <#if annotationElements?has_content>
        <section id="annotation-elements" class="doc-section">
        	<h2 class="section-title">Annotation Elements</h2>
            <div id="fields-protected" class="section-block">
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Element</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list annotationElements as const>
                		<tr>
                			<th>
                				<@utils.typeRef object=const.typeRef full=false />
                				<a class="scrollto" href="#annotation-elements-${const?index}">${const.name}</a>
                			</th>
                			<td>
                				${const.summaryComment}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
            </div>
        </section>
        </#if>
        
        <#if protectedMethods?has_content || publicMethods?has_content>
        <section id="methods" class="doc-section">
        	<h2 class="section-title">Methods Summary</h2>
        	<#if protectedMethods?has_content>
            <div id="methods-protected" class="section-block">
            	<h3 class="block-title">Protected Methods</h3>
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Method</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list protectedMethods as method>
                		<tr>
                			<th>
                				${method.declaration}
                				<@utils.typeRef object=method.returnType full=false />
                				<a class="scrollto" href="#methods-protected-${method?index}(<#list method.parameters as param>${param.type.simpleName}<#sep>,</#sep></#list>)">${method.name}</a>
                				(
                				<#list method.parameters as param>
                					<@utils.typeRef object=param.type full=false /><#sep>, </#sep>
                				</#list>
                				)
                			</th>
                			<td>
                				${method.summaryComment}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
            </div>
            </#if>
            <#if publicMethods?has_content>
            <div id="methods-public" class="section-block">
            	<h3 class="block-title">Public Methods</h3>
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Method</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list publicMethods as method>
                		<tr>
                			<th>
                				${method.declaration}
                				<@utils.typeRef object=method.returnType full=false />
                				<a class="scrollto" href="#methods-public-${method?index}(<#list method.parameters as param>${param.type.simpleName}<#sep>,</#sep></#list>)">${method.name}</a>
                				(
                				<#list method.parameters as param>
                					<@utils.typeRef object=param.type full=false /><#sep>, </#sep>
                				</#list>
                				)
                			</th>
                			<td>
                				${method.summaryComment}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
            </div>
            </#if>
        </section>
        </#if>
        
        <#if enums?has_content>
        <section id="enums" class="doc-section">
        	<h2 class="section-title">Enums</h2>
            <div class="section-block">
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Enum</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list enums as cls>
                		<tr>
                			<th>
                				<a href="${cls.fullName}.html">${cls.name}</a>
                			</th>
                			<td>
                				${cls.summaryComment}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
            </div>
        </section>
        </#if>
        
        <#if annotations?has_content>
        <section id="annotations" class="doc-section">
        	<h2 class="section-title">Annotations</h2>
            <div class="section-block">
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Annotation</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list annotations as cls>
                		<tr>
                			<th>
                				<a href="${cls.fullName}.html">${cls.name}</a>
                			</th>
                			<td>
                				${cls.summaryComment}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
            </div>
        </section>
        </#if>
        
        <#if interfaces?has_content>
        <section id="interfaces" class="doc-section">
        	<h2 class="section-title">Interfaces</h2>
            <div class="section-block">
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Interface</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list interfaces as cls>
                		<tr>
                			<th>
                				<a href="${cls.fullName}.html">${cls.name}</a>
                			</th>
                			<td>
                				${cls.summaryComment}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
            </div>
        </section>
        </#if>
        
        <#if classes?has_content>
        <section id="classes" class="doc-section">
        	<h2 class="section-title">Classes</h2>
            <div class="section-block">
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Class</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list classes as cls>
                		<tr>
                			<th>
                				<a href="${cls.fullName}.html">${cls.name}</a>
                			</th>
                			<td>
                				${cls.summaryComment}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
            </div>
        </section>
        </#if>
        
        <#if exceptions?has_content>
        <section id="exceptions" class="doc-section">
        	<h2 class="section-title">Exceptions</h2>
            <div class="section-block">
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Exception</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list exceptions as cls>
                		<tr>
                			<th>
                				<a href="${cls.fullName}.html">${cls.name}</a>
                			</th>
                			<td>
                				${cls.summaryComment}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
            </div>
        </section>
        </#if>
        
        <#if errors?has_content>
        <section id="errors" class="doc-section">
        	<h2 class="section-title">Errors</h2>
            <div class="section-block">
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Error</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list errors as cls>
                		<tr>
                			<th>
                				<a href="${cls.fullName}.html">${cls.name}</a>
                			</th>
                			<td>
                				${cls.summaryComment}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
            </div>
        </section>
        </#if>
        
        <#if enumConstants?has_content>
        <section id="enum-constants-details" class="doc-section">
        	<h2 class="section-title">Enum Constants Detail</h2>
            <div class="section-block">
            	<#list enumConstants as const>
            	<#if const?index != 0><hr></#if>
            	<h3 class="block-title" id="enum-constants-${const?index}">${const.name}</h3>
            	<pre class="language-java"><code class="language-java"><#list const.annotations as ann><@utils.annotation object=ann />
<@trim>			</@trim></#list>${const.declaration} <@utils.typeRef object=const.typeRef full=false /> ${const.name}</code></pre>
            	<p>${const.comment}</p>
            	
            	<#if blockTags?has_content>
            	<dl>
            	<#list blockTags as bt>
            		<dt>${bt.name()}</dt>
            		<dd>${bt.text()}</dd>
            	</#list>
            	</dl>
            	</#if>
            	</#list>
            </div>
        </section>
        </#if>
        
        <#if protectedFields?has_content>
        <section id="fields-protected-details" class="doc-section">
        	<h2 class="section-title">Protected Fields Detail</h2>
            <div class="section-block">
            	<#list protectedFields as field>
            	<#if field?index != 0><hr></#if>
            	<h3 class="block-title" id="fields-protected-${field?index}">${field.name}</h3>
            	<pre class="language-java"><code class="language-java"><#list field.annotations as ann><@utils.annotation object=ann />
<@trim>			</@trim></#list>${field.declaration} <@utils.typeRef object=field.typeRef full=false /> ${field.name}</code></pre>
            	<p>${field.comment}</p>
            	
            	<#if blockTags?has_content>
            	<dl>
            	<#list blockTags as bt>
            		<dt>${bt.name()}</dt>
            		<dd>${bt.text()}</dd>
            	</#list>
            	</dl>
            	</#if>
            	</#list>
            </div>
        </section>
        </#if>
        
        <#if publicFields?has_content>
        <section id="fields-public-details" class="doc-section">
        	<h2 class="section-title">Public Fields Detail</h2>
            <div class="section-block">
            	<#list publicFields as field>
            	<#if field?index != 0><hr></#if>
            	<h3 class="block-title" id="fields-public-${field?index}">${field.name}</h3>
            	<pre class="language-java"><code class="language-java"><#list field.annotations as ann><@utils.annotation object=ann />
<@trim>			</@trim></#list>${field.declaration} <@utils.typeRef object=field.typeRef full=false /> ${field.name}</code></pre>
            	<p>${field.comment}</p>
            	
            	<#if blockTags?has_content>
            	<dl>
            	<#list blockTags as bt>
            		<dt>${bt.name()}</dt>
            		<dd>${bt.text()}</dd>
            	</#list>
            	</dl>
            	</#if>
            	</#list>
            </div>
        </section>
        </#if>
        
        <#if constructors?has_content>
        <section id="constructors-details" class="doc-section">
        	<h2 class="section-title">Constructors Detail</h2>
            <div class="section-block">
            	<#list constructors as constructor>
            	<#if constructor?index != 0><hr></#if>
            	<h3 class="block-title" id="constructors-${constructor?index}">${constructor.name}</h3>
            	<pre class="language-java"><code class="language-java"><#list constructor.annotations as ann><@utils.annotation object=ann />
<@trim>			</@trim></#list>${constructor.declaration} ${constructor.name} (
    			<#list constructor.parameters as param>
	    			<#list param.annotations as ann><@utils.annotation object=ann /> </#list><@utils.typeRef object=param.type full=false /> ${param.name}<#sep>,
	    			</#sep>
    			</#list>

<@trim>         )<#if constructor.thrownExceptions?has_content> throws <#list constructor.thrownExceptions as te><@utils.typeRef object=te.exceptionType full=false /><#sep>, </#sep></#list></#if></@trim></code></pre>
            	<p>${constructor.comment}</p>
            	
            	<#if blockTags?has_content>
            	<dl>
            	<#list blockTags as bt>
            		<dt>${bt.name()}</dt>
            		<dd>${bt.text()}</dd>
            	</#list>
            	</dl>
            	</#if>
            	
            	<#if constructor.parameters?has_content>
            	<h4>Parameters</h4>
            	<dl>
            	<#list constructor.parameters as param>
            		<dt>${param.parameter.name()}</dt>
            		<dd>${param.comment!}</dd>
            	</#list>
            	</dl>
            	</#if>
            	
            	<#if constructor.thrownExceptions?has_content>
            	<h4>Throws</h4>
            	<dl>
            	<#list constructor.thrownExceptions as exception>
            		<dt><@utils.typeRef object=exception.exceptionType full=false /></dt>
            		<dd>${exception.comment!}</dd>
            	</#list>
            	</dl>
            	</#if>
            	
            	</#list>
            </div>
        </section>
        </#if>
        
        <#if annotationElements?has_content>
        <section id="annotation-elements-details" class="doc-section">
        	<h2 class="section-title">Annotation Elements Detail</h2>
            <div class="section-block">
            	<#list annotationElements as el>
            	<#if el?index != 0><hr></#if>
            	<h3 class="block-title" id="annotation-elements-${el?index}">${el.name}</h3>
            	<pre class="language-java"><code class="language-java"><#list el.annotations as ann><@utils.annotation object=ann />
<@trim>			</@trim></#list>${el.declaration} <@utils.typeRef object=el.typeRef full=false /> ${el.name} ()<#if el.defaultValue?has_content> default <@utils.annotationValue value=el.defaultValue /></#if></code></pre>
            	<p>${el.comment}</p>
            	
            	<#if blockTags?has_content>
            	<dl>
            	<#list blockTags as bt>
            		<dt>${bt.name()}</dt>
            		<dd>${bt.text()}</dd>
            	</#list>
            	</dl>
            	</#if>
            	</#list>
            </div>
        </section>
        </#if>
        
        <#if protectedMethods?has_content>
        <section id="methods-protected-details" class="doc-section">
        	<h2 class="section-title">Protected Methods Detail</h2>
            <div class="section-block">
            	<#list protectedMethods as el>
            	<#if el?index != 0><hr></#if>
            	<h3 class="block-title" id="methods-protected-${el?index}">${el.name}</h3>
            	<pre class="language-java"><code class="language-java"><#list el.annotations as ann><@utils.annotation object=ann />
<@trim>			</@trim></#list>${el.declaration} <#if el.arguments?has_content><@utils.typeVar arguments=el.arguments /> </#if><@utils.typeRef object=el.returnType full=false /> ${el.name} (
            		<#list el.parameters as param>
            			<#list param.annotations as ann><@utils.annotation object=ann /> </#list><@utils.typeRef object=param.type full=false /> ${param.name}<#sep>,
            			</#sep>
            		</#list>

<@trim>            	)<#if el.thrownExceptions?has_content> throws <#list el.thrownExceptions as te><@utils.typeRef object=te.exceptionType full=false /><#sep>, </#sep></#list></#if></@trim></code></pre>
            	<p>${el.comment}</p>
            	
            	<#if blockTags?has_content>
            	<dl>
            	<#list blockTags as bt>
            		<dt>${bt.name()}</dt>
            		<dd>${bt.text()}</dd>
            	</#list>
            	</dl>
            	</#if>
            	
            	<#if el.parameters?has_content>
            	<h4>Parameters</h4>
            	<dl>
            	<#list el.parameters as param>
            		<dt>${param.parameter.name()}</dt>
            		<dd>${param.comment!}</dd>
            	</#list>
            	</dl>
            	</#if>
            	
            	<#if el.thrownExceptions?has_content>
            	<h4>Throws</h4>
            	<dl>
            	<#list el.thrownExceptions as exception>
            		<dt><@utils.typeRef object=exception.exceptionType full=false /></dt>
            		<dd>${exception.comment!}</dd>
            	</#list>
            	</dl>
            	</#if>
            	
            	</#list>
            </div>
        </section>
        </#if>
        
        <#if publicMethods?has_content>
        <section id="methods-public-details" class="doc-section">
        	<h2 class="section-title">Public Methods Detail</h2>
            <div class="section-block">
            	<#list publicMethods as el>
            	<#if el?index != 0><hr></#if>
            	<h3 class="block-title" id="methods-public-${el?index}">${el.name}</h3>
            	<pre class="language-java"><code class="language-java"><#list el.annotations as ann><@utils.annotation object=ann />
<@trim>			</@trim></#list>${el.declaration} <#if el.arguments?has_content><@utils.typeVar arguments=el.arguments /> </#if><@utils.typeRef object=el.returnType full=false /> ${el.name} (
            		<#list el.parameters as param>
            			<#list param.annotations as ann><@utils.annotation object=ann /> </#list><@utils.typeRef object=param.type full=false /> ${param.name}<#sep>,
            			</#sep>
            		</#list>

<@trim>            	)<#if el.thrownExceptions?has_content> throws <#list el.thrownExceptions as te><@utils.typeRef object=te.exceptionType full=false /><#sep>, </#sep></#list></#if></@trim></code></pre>
            	<p>${el.comment}</p>
            	
            	<#if blockTags?has_content>
            	<dl>
            	<#list blockTags as bt>
            		<dt>${bt.name()}</dt>
            		<dd>${bt.text()}</dd>
            	</#list>
            	</dl>
            	</#if>
            	
            	<#if el.parameters?has_content>
            	<h4>Parameters</h4>
            	<dl>
            	<#list el.parameters as param>
            		<dt>${param.parameter.name()}</dt>
            		<dd>${param.comment!}</dd>
            	</#list>
            	</dl>
            	</#if>
            	
            	<#if el.thrownExceptions?has_content>
            	<h4>Throws</h4>
            	<dl>
            	<#list el.thrownExceptions as exception>
            		<dt><@utils.typeRef object=exception.exceptionType full=false /></dt>
            		<dd>${exception.comment!}</dd>
            	</#list>
            	</dl>
            	</#if>
            	
            	</#list>
            </div>
        </section>
        </#if>
		
	</div>
	
</div>

<div class="doc-sidebar hidden-xs">
	
	<nav id="doc-nav">
		
		<ul id="doc-menu" class="nav doc-menu hidden-xs affix-top" data-spy="affix">
			<li>
				<a class="scrollto" href="#hierarchy">Hierarchy</a>
				<ul class="nav doc-sub-menu">
					<li><a href="#uml-diagrams" class="scrollto">UML diagrams</a></li>
				</ul>
			</li>
			<li><a class="scrollto" href="#declaration">Description</a></li>
			<#if enumConstants?has_content><li><a href="#enum-constants" class="scrollto">Enum Constants</a></li></#if>
			<#if protectedFields?has_content || publicFields?has_content>
			<li>
				<a class="scrollto" href="#fields">Fields</a>
				<ul class="nav doc-sub-menu">
					<#if protectedFields?has_content><li><a href="#fields-protected" class="scrollto">Protected Fields</a></li></#if>
					<#if publicFields?has_content><li><a href="#fields-public" class="scrollto">Public Fields</a></li></#if>
				</ul>
			</li>
			</#if>
			<#if constructors?has_content><li><a href="#constructors" class="scrollto">Constructors</a></li></#if>
			<#if annotationElements?has_content><li><a class="scrollto" href="#annotation-elements">Annotation Elements</a></li></#if>
			<#if protectedMethods?has_content || publicMethods?has_content>
			<li>
				<a class="scrollto" href="#methods">Methods</a>
				<ul class="nav doc-sub-menu">
					<#if protectedMethods?has_content><li><a href="#methods-protected" class="scrollto">Protected Methods</a></li></#if>
					<#if publicMethods?has_content><li><a href="#methods-public" class="scrollto">Public Methods</a></li></#if>
				</ul>
			</li>
			</#if>
			<#if enums?has_content><li><a class="scrollto" href="#enums">Enums</a></li></#if>
            <#if annotations?has_content><li><a class="scrollto" href="#annotations">Annotations</a></li></#if>
            <#if interfaces?has_content><li><a class="scrollto" href="#interfaces">Interfaces</a></li></#if>
            <#if classes?has_content><li><a class="scrollto" href="#classes">Classes</a></li></#if>
            <#if exceptions?has_content><li><a class="scrollto" href="#exceptions">Exceptions</a></li></#if>
            <#if errors?has_content><li><a class="scrollto" href="#errors">Errors</a></li></#if>
            
			<#if enumConstants?has_content><li><a href="#enum-constants-details" class="scrollto">Enum Constants Detail</a></li></#if>
			<#if protectedFields?has_content || publicFields?has_content>
			<li>
				<a class="scrollto" href="#fields-details">Fields Detail</a>
				<ul class="nav doc-sub-menu">
					<#if protectedFields?has_content><li><a href="#fields-protected-details" class="scrollto">Protected Fields Detail</a></li></#if>
					<#if publicFields?has_content><li><a href="#fields-public-details" class="scrollto">Public Fields Detail</a></li></#if>
				</ul>
			</li>
			</#if>
			<#if constructors?has_content><li><a href="#constructors-details" class="scrollto">Constructors Detail</a></li></#if>
			<#if annotationElements?has_content><li><a class="scrollto" href="#annotation-elements-details">Annotation Elements Detail</a></li></#if>
			<#if protectedMethods?has_content || publicMethods?has_content>
			<li>
				<a class="scrollto" href="#methods-details">Methods Detail</a>
				<ul class="nav doc-sub-menu">
					<#if protectedMethods?has_content><li><a href="#methods-protected-details" class="scrollto">Protected Methods Detail</a></li></#if>
					<#if publicMethods?has_content><li><a href="#methods-public-details" class="scrollto">Public Methods Detail</a></li></#if>
				</ul>
			</li>
			</#if>
        </ul>
		
	</nav>
	
</div>

<#include "/templates/includes/footer.tpl">