<#include "/templates/includes/header.tpl">

<div class="doc-content">
	
	<div class="content-inner">
		
        <section id="packages" class="doc-section">
            <h2 class="section-title">Packages</h2>
            <div class="section-block">
                <p>Welcome! The screenshots used in this page are taken from <a href="https://wrapbootstrap.com/theme/appkit-admin-theme-angularjs-WB051SCJ1?ref=3wm" target="_blank">AppKit</a>. AppKit is a Bootstrap Angular admin theme.</p>
                <ul class="list list-inline">
                    <li><a class="btn btn-cta btn-pink" href="https://wrapbootstrap.com/theme/appkit-admin-theme-angularjs-WB051SCJ1?ref=3wm" target="_blank"><i class="fa fa-external-link"></i> AppKit Details</a></li>
                    <li><a class="btn btn-cta btn-primary" href="https://wrapbootstrap.com/theme/appkit-admin-theme-angularjs-WB051SCJ1?ref=3wm" target="_blank"><i class="fa fa-eye"></i> AppKit Demo</a></li>
                </ul>
                
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Package</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list packages as pkg>
                		<tr>
                			<th>
                				<a href="${pkg.name}.html">${pkg.name}</a>
                			</th>
                			<td>
                				${pkg.comment}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
                
            </div><!--//section-block-->
        </section>
		
	</div>
	
</div>

<div class="doc-sidebar hidden-xs">
	
	<nav id="doc-nav">
		
		<div id="doc-menu" class="nav doc-menu hidden-xs affix-top" data-spy="affix">
			
			<section class="doc-section">
				<h2 class="section-title">All classes</h2>
				<div class="" style="max-height: 350px; overflow: scroll;">
					<ul class="nav doc-menu hidden-xs">
						<#list module.classes as cls>
			            <li class="">
			                <a title="${cls.containingPackage().name()}.${cls.name()}" href="${cls.containingPackage().name()}.${cls.name()}.html">${cls.name()}</a>
			            </li>
			            </#list>
			        </ul>
			    </div>
			</section>
		    
		</div>
		
	</nav>
	
</div>

<#include "/templates/includes/footer.tpl">