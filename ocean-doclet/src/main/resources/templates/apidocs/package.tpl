<#include "/templates/includes/header.tpl">

<div class="doc-content">
	
	<div class="content-inner">
		
        <section id="packages" class="doc-section">
            <h2 class="section-title">Package ${pkg.name()}</h2>
            <div class="section-block">
                <p>${comment}</p>
            </div>
        </section>
        
        <#if enums?has_content>
        <section id="enums" class="doc-section">
        	<h2 class="section-title">Enums</h2>
            <div class="section-block">
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Enum</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list enums as cls>
                		<tr>
                			<th>
                				<a href="${cls.fullName}.html">${cls.name}</a>
                			</th>
                			<td>
                				${cls.summaryComment!"&nbsp;"}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
            </div>
        </section>
        </#if>
        
        <#if annotations?has_content>
        <section id="annotations" class="doc-section">
        	<h2 class="section-title">Annotations</h2>
            <div class="section-block">
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Annotation</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list annotations as cls>
                		<tr>
                			<th>
                				<a href="${cls.fullName}.html">${cls.name}</a>
                			</th>
                			<td>
                				${cls.summaryComment!"&nbsp;"}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
            </div>
        </section>
        </#if>
        
        <#if interfaces?has_content>
        <section id="interfaces" class="doc-section">
        	<h2 class="section-title">Interfaces</h2>
            <div class="section-block">
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Interface</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list interfaces as cls>
                		<tr>
                			<th>
                				<a href="${cls.fullName}.html">${cls.name}</a>
                			</th>
                			<td>
                				${cls.summaryComment!"&nbsp;"}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
            </div>
        </section>
        </#if>
        
        <#if classes?has_content>
        <section id="classes" class="doc-section">
        	<h2 class="section-title">Classes</h2>
            <div class="section-block">
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Class</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list classes as cls>
                		<tr>
                			<th>
                				<a href="${cls.fullName}.html">${cls.name}</a>
                			</th>
                			<td>
                				${cls.summaryComment!"&nbsp;"}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
            </div>
        </section>
        </#if>
        
        <#if exceptions?has_content>
        <section id="exceptions" class="doc-section">
        	<h2 class="section-title">Exceptions</h2>
            <div class="section-block">
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Exception</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list exceptions as cls>
                		<tr>
                			<th>
                				<a href="${cls.fullName}.html">${cls.name}</a>
                			</th>
                			<td>
                				${cls.summaryComment!"&nbsp;"}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
            </div>
        </section>
        </#if>
        
        <#if errors?has_content>
        <section id="errors" class="doc-section">
        	<h2 class="section-title">Errors</h2>
            <div class="section-block">
                <table class="table table-bordered table-striped table-condensed">
                	<thead>
                		<tr>
                			<th>Error</th>
                			<th>Description</th>
                		</tr>
                	</thead>
                	<tbody>
                		<#list errors as cls>
                		<tr>
                			<th>
                				<a href="${cls.fullName}.html">${cls.name}</a>
                			</th>
                			<td>
                				${cls.summaryComment!"&nbsp;"}
                			</td>
                		</tr>
                		</#list>
                	</tbody>
                </table>
            </div>
        </section>
        </#if>
                
	</div>
	
</div>

<div class="doc-sidebar hidden-xs">
	
	<nav id="doc-nav">
		
		<ul id="doc-menu" class="nav doc-menu hidden-xs affix-top" data-spy="affix">
			<#if enums?has_content><li><a class="scrollto" href="#enums">Enums</a></li></#if>
            <#if annotations?has_content><li><a class="scrollto" href="#annotations">Annotations</a></li></#if>
            <#if interfaces?has_content><li><a class="scrollto" href="#interfaces">Interfaces</a></li></#if>
            <#if classes?has_content><li><a class="scrollto" href="#classes">Classes</a></li></#if>
            <#if exceptions?has_content><li><a class="scrollto" href="#exceptions">Exceptions</a></li></#if>
            <#if errors?has_content><li><a class="scrollto" href="#errors">Errors</a></li></#if>
        </ul>
		
	</nav>
	
</div>

<#include "/templates/includes/footer.tpl">