
    
    <footer class="footer text-center">
        <div class="container">
            <!--/* This template is released under the Creative Commons Attribution 3.0 License. Please keep the attribution link below when using for your own project. Thank you for your support. :) If you'd like to use the template without the attribution, you can check out other license options via our website: themes.3rdwavemedia.com */-->
            <small class="copyright">Designed with <i class="fa fa-heart"></i> by <a href="http://themes.3rdwavemedia.com/" target="_blank">Xiaoying Riley</a> for developers</small>
            
        </div><!--//container-->
    </footer><!--//footer-->
    
     
    <!-- Main Javascript -->          
    <script type="text/javascript" src="${contextPath!}assets/plugins/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="${contextPath!}assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${contextPath!}assets/plugins/prism/prism.js"></script>
    <script type="text/javascript" src="${contextPath!}assets/plugins/jquery-scrollTo/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="${contextPath!}assets/plugins/lightbox/dist/ekko-lightbox.min.js"></script>
    <script type="text/javascript" src="${contextPath!}assets/plugins/jquery-match-height/jquery.matchHeight-min.js"></script>
    <script type="text/javascript" src="${contextPath!}assets/plugins/lunr/lunr.min.js"></script>
    <script type="text/javascript" src="${contextPath!}assets/plugins/selectize/js/standalone/selectize.min.js"></script>
    <script type="text/javascript" src="${contextPath!}assets/js/main.js"></script>
    <script type="text/javascript" src="${contextPath!}assets/js/lunr-index.js"></script>
    
</body>
</html> 
