<#include "/templates/includes/header.tpl">

<div class="doc-content">
	
	<div class="content-inner">
		
		<#list groups as group>
        <section id="components-${group?index}" class="doc-section">
            <h2 class="section-title"><#if group.title?has_content>${group.title}<#else>Main components</#if></h2>
            <div class="section-block">
                <p>Welcome! The screenshots used in this page are taken from <a href="https://wrapbootstrap.com/theme/appkit-admin-theme-angularjs-WB051SCJ1?ref=3wm" target="_blank">AppKit</a>. AppKit is a Bootstrap Angular admin theme.</p>
                <ul class="list list-inline">
                    <li><a class="btn btn-cta btn-pink" href="https://wrapbootstrap.com/theme/appkit-admin-theme-angularjs-WB051SCJ1?ref=3wm" target="_blank"><i class="fa fa-external-link"></i> AppKit Details</a></li>
                    <li><a class="btn btn-cta btn-primary" href="https://wrapbootstrap.com/theme/appkit-admin-theme-angularjs-WB051SCJ1?ref=3wm" target="_blank"><i class="fa fa-eye"></i> AppKit Demo</a></li>
                </ul>
                
            </div><!--//section-block-->
            <div class="section-block">
                <div class="row">
                	<#list group.modules as module>
                    <div class="col-md-6 col-sm-12 col-sm-12">
                        <h4><#if module.iconClass?has_content><i class="${module.iconClass}"></i> </#if>${module.name}</h4>
                        <#if module.description?has_content>
                        <p>${module.description}</p>
                        </#if>
                        <ul class="list list-inline">
                        	<#if module.iconClass?has_content && module.type?has_content>
                        	<li><i class="${module.iconClass}"></i> ${module.type!}</li>
                        	</#if>
                        </ul>
                        <ul class="list list-inline">
		                    <li><a class="btn btn-cta btn-primary" href="components/${module.pathName?substring(1)}/index.html"><span aria-hidden="true" class="icon_documents_alt icon"></span> Documentation</a></li>
		                </ul>
                    </div>
                    <#if module?index % 2 == 1>
                    <div class="clearfix"></div>
                    </#if>
                    </#list>
                </div><!--//row-->
                <div class="callout-block callout-info">
                    <div class="icon-holder">
                        <i class="fa fa-bullhorn"></i>
                    </div><!--//icon-holder-->
                    <div class="content">
                        <h4 class="callout-title">Lightbox Example</h4>
                        <p>Click the screenshot images to trigger the image modal.</p>
                    </div><!--//content-->
                </div><!--//callout-->
            </div><!--//section-block-->
            
        </section><!--//doc-section-->
        </#list>
                            
	</div>
	
</div>

<div class="doc-sidebar hidden-xs">
	
	<nav id="doc-nav">
		
		<ul id="doc-menu" class="nav doc-menu hidden-xs affix-top" data-spy="affix">
			<#list groups as group>
            <li class="">
                <a class="scrollto" href="#components-${group?index}"><#if group.title?has_content>${group.title}<#else>Main components</#if></a>
                <!--ul class="nav doc-sub-menu">
                	<#list group.modules as module>
                    <li class=""><a class="scrollto" href="#projects">${module.name}</a></li>
                    </#list>
                </ul--><!--//nav-->
            </li>
            </#list>
        </ul>
		
	</nav>
	
</div>

<#include "/templates/includes/footer.tpl">