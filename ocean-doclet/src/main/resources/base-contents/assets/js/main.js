$(document).ready(function() {
	
	/* ===== Affix Sidebar ===== */
	/* Ref: http://getbootstrap.com/javascript/#affix-examples */

    	
	$('#doc-menu').affix({
        offset: {
            top: ($('#header').outerHeight(true) + $('#doc-header').outerHeight(true)) + 45,
            bottom: ($('#footer').outerHeight(true) + $('#promo-block').outerHeight(true)) + 75
        }
    });
    
    /* Hack related to: https://github.com/twbs/bootstrap/issues/10236 */
    $(window).on('load resize', function() {
        $(window).trigger('scroll'); 
    });

    /* Activate scrollspy menu */
    $('body').scrollspy({target: '#doc-nav', offset: 100});
    
    /* Smooth scrolling */
	$('a.scrollto').on('click', function(e){
        //store hash
        var target = this.hash;    
        e.preventDefault();
		$('body').scrollTo(target, 800, {offset: 0, 'axis':'y'});
		
	});
	
    
    /* ======= jQuery Responsive equal heights plugin ======= */
    /* Ref: https://github.com/liabru/jquery-match-height */
    
     $('#cards-wrapper .item-inner').matchHeight();
     $('#showcase .card').matchHeight();
     
    /* Bootstrap lightbox */
    /* Ref: http://ashleydw.github.io/lightbox/ */

    $(document).delegate('*[data-toggle="lightbox"]', 'click', function(e) {
        e.preventDefault();
        $(this).ekkoLightbox();
    });    
    
    /* ====== Search field ==== */
    
    /*Selectize.prototype.search = function (query) { 
	  return {
	    query: query,
	    tokens: [], // disable highlight
	    items: $.map(this.options, function (item, key) {
	      return {id: key}
	    })
	  }
	};*/
    
    $('#search-field').selectize({
    	persist: false,
    	valueField: 'url',
    	labelField: 'title',
    	searchField: [],
    	preload: false,
    	highlight: false,
    	score: function () {
    		return function() { return 1; };
    	},
    	load: function (query, callback) {
    		var results = globalIndex.search(query);
    		var options = [];
    		for (var i = 0; i < results.length; i++) {
    			var data = fullData[results[i].ref];
    			var opt = {
    				url: data.url,
    				title: data.title,
    				location: data.location,
    				content: highlight(i, data, results)
    			};
    			options.push(opt);
    		}
    		this.clearOptions();
    		callback(options);
    	},
    	onChange: function (value) {
    		location.href = $('meta[name="context-path"]').attr('content') + value;
    	},
    	render: {
    		option: function (item, escape) {
    			return '<div>' +
					'<span class="title"><strong>' + escape(item.title) + '</strong></span><br>' +
					'<span class="description">' + escape(item.content) + '</span><br>' +
					'<span class="location">' + escape(item.location) + '</span>' +
				'</div>';
    		}
    	}
    });
    
    function highlight(idx, data, results) {
    	var highlighted = '';
    	for (var i in results[idx].matchData.metadata) {
    		for (var f in results[idx].matchData.metadata[i]) {
				var lastPos = 0;
				var lastEnd = 0;
				for (var j = 0; j < results[idx].matchData.metadata[i][f].position.length; j++) {
					lastPos = Math.max(lastEnd, results[idx].matchData.metadata[i][f].position[j][0] - 20);
					if (lastPos > 0 && lastEnd === 0) {
						highlighted += '... ';
					}
					lastEnd = Math.min(data[f].length, lastPos + results[idx].matchData.metadata[i][f].position[j][1] + 20);
					highlighted += data[f].substr(lastPos, lastEnd) + ' ... ';
				}
			}
    	}
    	return highlighted;
    }
    
});
