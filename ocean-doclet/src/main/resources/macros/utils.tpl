<#macro link object><#if (object.url)?has_content><a href="${contextPath!}${object.url?html}"><#nested></a><#else><#nested></#if></#macro>

<#macro typeRef object full=false><#if object.cls??><@link object=object.cls><span title="${object.cls.fullName}"><#if full>${object.cls.fullName}<#else>${object.cls.name}</#if></span></@link><#if object.arguments?has_content>&lt;<#list object.arguments as arg><@typeRef object=arg /><#sep>, </#sep></#list>&gt;</#if><#else>${object.primitiveName}</#if>${object.dimension!}</#macro>

<#macro annotation object><@trim>
	@<@typeRef object=object.typeRef /><#if object.annotation.elementValues()?has_content>(<#list object.annotation.elementValues() as el>${el.element().name()} = <@annotationValue value=object.values[el.element().name()] /><#sep>, </#sep></#list>)</#if>
</@trim></#macro>

<#macro annotationValue value><@trim>
	<#if value.array>
	{<#list value.values as v><@annotationValue value=v /><#sep>, </#sep></#list>}
	<#elseif value.simple>
	${value.value?c}
	<#elseif value.annotation>
	<@annotation object=value.toModel(value.value) />
	<#elseif value.classLiteral>
	<@typeRef object=value.toTypeRef(value.value) full=false />.class
	<#elseif value.fieldDoc>
	<@typeRef object=value.toTypeRef(value.value.type()) full=false />.${value.value.name()}
	<#elseif value.string>
	"${value.value?html}"
	</#if>
</@trim></#macro>

<#macro typeVar arguments><@trim>
&lt;<#list arguments as object>${object.name}<#if object.bounds?has_content> extends <#list object.bounds as bound><@typeRef object=bound full=false /><#sep>, </#sep></#list></#if><#sep>, </#sep></#list>&gt;
</@trim></#macro>