function loadDocuments(json) {
	var idx = lunr(function () {
		this.ref('url');
		this.field('title');
		this.field('content');
		this.field('location');
		this.metadataWhitelist = ['position'];
		
		var documents = JSON.parse(json);
		documents.forEach(function (doc) {
			this.add(doc);
		}, this);
	});
	
	return JSON.stringify(idx);
}