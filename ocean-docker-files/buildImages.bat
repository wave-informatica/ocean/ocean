rem @echo off

rem #*******************************************************************************
rem # Copyright 2015, 2018 Wave Informatica S.r.l..
rem #
rem # Licensed under the Apache License, Version 2.0 (the "License");
rem # you may not use this file except in compliance with the License.
rem # You may obtain a copy of the License at
rem #
rem #      http://www.apache.org/licenses/LICENSE-2.0
rem #  
rem # Unless required by applicable law or agreed to in writing, software
rem # distributed under the License is distributed on an "AS IS" BASIS,
rem # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
rem # See the License for the specific language governing permissions and
rem # limitations under the License.
rem #*******************************************************************************

set OCEAN_APP_SERVER=%1%

rem function join_by { local IFS="$1"; shift; echo "$*"; }

goto start

:noprofile
	rem AVAILABLE=$(join_by '|' $(ls src/main/dockerfiles/))
	echo "Please provide desired application server name:"
	echo "    buildImages.bat tomcat | wildfly-10"
	exit /B 0

:notfound
	echo "Unable to find profile %OCEAN_APP_SERVER%"
	goto noprofile
	exit /B 0

:start
if "%OCEAN_APP_SERVER%" == "" goto noprofile

if not exist src\main\dockerfiles\%OCEAN_APP_SERVER%\Dockerfile goto notfound

rem cd ..

rem Ocean full

rem mvn clean install

rem cd startup
rem mvn -P%OCEAN_APP_SERVER%,hibernate-search,websocket,poi-full-ooxml clean install
rem cd ../ocean-docker-files

rem mvn clean package

xcopy /E ..\startup\target\startup-*.war target\ROOT.war

xcopy /E src\main\dockerfiles\%OCEAN_APP_SERVER%\* target\

for /f "delims=" %%x in (target/maven-archiver/pom.properties) do (set "%%x")

rem type src\main\dockerfiles\%OCEAN_APP_SERVER%\Dockerfile | bash -c envsubst > target/Dockerfile

rem docker build --no-cache -t wave/ocean:${OCEAN_APP_SERVER} -f target/Dockerfile --network host target

rem docker tag wave/ocean:${OCEAN_APP_SERVER} 192.168.22.1:8083/wave/ocean:${OCEAN_APP_SERVER}
rem docker tag wave/ocean:${OCEAN_APP_SERVER} 192.168.22.1:8082/wave/ocean:${OCEAN_APP_SERVER}
