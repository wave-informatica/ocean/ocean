#!/bin/bash
#*******************************************************************************
# Copyright 2015, 2018 Wave Informatica S.r.l..
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#  
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#*******************************************************************************
      
      JBOSS_HOME=/opt/jboss/wildfly
      JBOSS_CLI=$JBOSS_HOME/bin/jboss-cli.sh
      JBOSS_MODE=${1:-"standalone"}
      JBOSS_CONFIG=${2:-"$JBOSS_MODE.xml"}
      
      function wait_for_server() {
        until `$JBOSS_CLI -c "ls /deployment" &> /dev/null`; do
          sleep 1
        done
      }
      
      echo "=> Starting WildFly server"
      $JBOSS_HOME/bin/$JBOSS_MODE.sh -c $JBOSS_CONFIG > /dev/null &
      
      echo "=> Waiting for the server to boot"
      wait_for_server
      
      echo "=> Executing the commands"
      $JBOSS_CLI -c --file=`dirname "$0"`/commands.cli
      
      echo "=> Shutting down WildFly"
      if [ "$JBOSS_MODE" = "standalone" ]; then
        $JBOSS_CLI -c ":shutdown"
      else
        $JBOSS_CLI -c "/host=*:shutdown"
      fi
