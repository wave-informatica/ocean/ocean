#!/bin/bash
#*******************************************************************************
# Copyright 2015, 2018 Wave Informatica S.r.l..
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#  
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#*******************************************************************************

OCEAN_APP_SERVER=$1

function join_by { local IFS="$1"; shift; echo "$*"; }

function noprofile {
	AVAILABLE=$(join_by '|' $(ls src/main/dockerfiles/))
	echo "Please provide desired application server name:"
	echo "    ./buildImages.sh" $AVAILABLE
}

if [ "$OCEAN_APP_SERVER"x == "x" ]; then
	noprofile
	exit 1
fi

if [ ! -f src/main/dockerfiles/${OCEAN_APP_SERVER}/Dockerfile ]; then
	echo "Unable to find profile ${OCEAN_APP_SERVER}"
	noprofile
	exit 1
fi

cd ..

# Ocean full

mvn clean install

cd startup
mvn -P${OCEAN_APP_SERVER},hibernate-search,websocket,poi-full-ooxml clean install
cd ../ocean-docker-files

mvn clean package

cp ../startup/target/startup-*.war target/ROOT.war

cp -R src/main/dockerfiles/${OCEAN_APP_SERVER}/* target/

source target/maven-archiver/pom.properties
export version=${version}

cat src/main/dockerfiles/${OCEAN_APP_SERVER}/Dockerfile | envsubst > target/Dockerfile

docker build --no-cache -t wave/ocean:${OCEAN_APP_SERVER} -f target/Dockerfile --network host target

docker tag wave/ocean:${OCEAN_APP_SERVER} 192.168.22.1:8083/wave/ocean:${OCEAN_APP_SERVER}
docker tag wave/ocean:${OCEAN_APP_SERVER} 192.168.22.1:8082/wave/ocean:${OCEAN_APP_SERVER}
