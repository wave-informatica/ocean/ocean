/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages.operations;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.ScopeProvider;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.InputResult.Persistence;
import com.waveinformatica.ocean.core.controllers.scopes.AbstractScope;
import com.waveinformatica.ocean.core.controllers.scopes.IScopeProvider;
import com.waveinformatica.ocean.core.controllers.scopes.ScopeChain;
import com.waveinformatica.ocean.core.converters.ITypeConverter;
import com.waveinformatica.ocean.core.exceptions.OperationNotAuthorizedException;
import com.waveinformatica.ocean.core.modules.ClassInfoCache;
import com.waveinformatica.ocean.core.modules.ClassInfoCache.Factory;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.Results;
import com.waveinformatica.ocean.pages.annotations.LifecycleListener;
import com.waveinformatica.ocean.pages.annotations.Page;
import com.waveinformatica.ocean.pages.annotations.Property;
import com.waveinformatica.ocean.pages.annotations.ViewComponent;
import com.waveinformatica.ocean.pages.annotations.ViewData;
import com.waveinformatica.ocean.pages.annotations.ViewEventListener;
import com.waveinformatica.ocean.pages.annotations.ViewState;
import com.waveinformatica.ocean.pages.annotations.ViewValue;
import com.waveinformatica.ocean.pages.components.UIComponent;
import com.waveinformatica.ocean.pages.components.UIPage;
import com.waveinformatica.ocean.pages.validators.PageValidator;

@ScopeProvider(inScope = AbstractScope.class)
public class PageScopeProvider implements IScopeProvider {

	@Inject
	private CoreController controller;
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private ClassInfoCache cache;
	
	@Override
	public String produce(ScopeChain chain, String path) {
		
		PagesService service = controller.getService(PagesService.class);
		
		Class<? extends UIPage> cls = service.getPageClass(path);
		
		if (cls == null) {
			return null;
		}
		
		UIPage page = factory.newInstance(cls);
		page.setPersistence(cls.getAnnotation(Page.class).persistence());
		
		try {
			page.onPreCheckAuthorization();
		} catch (OperationNotAuthorizedException e) {
			LoggerUtils.getLogger(PageScopeProvider.class).log(Level.SEVERE, "Pre authorization failed for page " + path, e);
			return null;
		}
        
        Results.addScriptSource(page, "ris/js/rivets.min.js");
        Results.addScriptSource(page, "ris/js/ocean-pages.js");
		
		PageScope scope = factory.newInstance(PageScope.class);
		scope.setPageUrl(path.startsWith("/") ? path.substring(1) : path);
		if (Context.get().getRequest() != null && StringUtils.isNotBlank(Context.get().getRequest().getQueryString())) {
			scope.setPageUrl(scope.getPageUrl() + "?" + Context.get().getRequest().getQueryString());
		}
		scope.setPage(page);
		fill(page, "", page);
		
		chain.add(scope);
		
		return "/processPage";
	}
	
	private void fill(final Object page, String path, UIPage mainPage) {
		
		ObjectDescriptor descriptor = (ObjectDescriptor) cache.get(page.getClass(), page.getClass().getName() + "::" + ObjectDescriptor.class.getName(), new Factory() {
			@Override
			public Object create() {
				
				ObjectDescriptor descriptor = new ObjectDescriptor();
				
				Set<String> fieldNames = new HashSet<>();
				
				Class cls = page.getClass();
				while (cls != null) {
					
					Page annotation = (Page) cls.getAnnotation(Page.class);
					if (annotation != null) {
						for (Class<? extends PageValidator> c : annotation.validate()) {
							descriptor.getValidators().add(c);
						}
					}
					
					Field[] fields = cls.getDeclaredFields();
					for (Field f : fields) {
						if (fieldNames.contains(f.getName())) {
							continue;
						}
						ViewComponent vc = f.getAnnotation(ViewComponent.class);
						if (vc != null) {
							f.setAccessible(true);
							ComponentDescriptor cd = new ComponentDescriptor(f, vc);
							for (Property prop : vc.properties()) {
								try {
									if (!cd.getComponentProperties().containsKey(prop.name())) {
										Field pf = findField(f.getType(), prop.name());
										if (pf == null) {
											LoggerUtils.getLogger(f.getType()).log(Level.SEVERE, "Property " + prop.name() + " not found in class " + f.getType().getName());
										}
										else {
											pf.setAccessible(true);
											cd.getComponentProperties().put(pf.getName(), pf);
										}
									}
								} catch (SecurityException e) {
									LoggerUtils.getLogger(page.getClass()).log(Level.SEVERE, null, e);
								}
							}
							descriptor.getComponents().add(cd);
							fieldNames.add(f.getName());
						}
						ViewState vs = f.getAnnotation(ViewState.class);
						if (vs != null) {
							descriptor.getViewStateFields().add(f);
							if (vs.validateTrue()) {
								descriptor.getValidateFields().add(f);
							}
						}
						ViewData vd = f.getAnnotation(ViewData.class);
						if (vd != null) {
							descriptor.getDataFields().add(f);
							if (vd.validateTrue()) {
								descriptor.getValidateFields().add(f);
							}
						}
                        ViewValue vv = f.getAnnotation(ViewValue.class);
                        if (vv != null) {
                            descriptor.setValueField(f);
                        }
					}
					
					Method[] methods = cls.getMethods();
					for (Method m : methods) {
						descriptor.addMethod(m);
						ViewEventListener vel = m.getAnnotation(ViewEventListener.class);
						if (vel != null) {
							List<Method> listeners = descriptor.getLocalEventListeners().get(vel.value());
							if (listeners == null) {
								listeners = new ArrayList<>();
								descriptor.getLocalEventListeners().put(vel.value(), listeners);
							}
							listeners.add(m);
						}
						LifecycleListener lcl = m.getAnnotation(LifecycleListener.class);
						if (lcl != null) {
							List<Method> ms = descriptor.getLifecycleListeners().get(lcl.value());
							if (ms == null) {
								ms = new ArrayList<>();
								descriptor.getLifecycleListeners().put(lcl.value(), ms);
							}
							ms.add(m);
						}
					}
					
					cls = cls.getSuperclass();
				}
				
				return descriptor;
			}
		});
		
		if (page instanceof UIComponent) {
			((UIComponent) page).setObjectDescriptor(descriptor);
		}
		else if (page instanceof UIPage) {
			((UIPage) page).setDescriptor(descriptor);
		}
		
		for (ComponentDescriptor cd : descriptor.getComponents()) {
			
			String tmpPath = path;
			if (StringUtils.isNotBlank(tmpPath)) {
				tmpPath += ".";
			}
			
			UIComponent cmp = (UIComponent) factory.newInstance(cd.getField().getType());
			
			cmp.setId(tmpPath + cd.getField().getName());
			cmp.setComponentDescriptor(cd);
			
			fill(cmp, cmp.getId(), mainPage);
			
			try {
				
				cd.getField().set(page, cmp);
				
				for (Property prop : cd.getViewComponent().properties()) {
					Field pf = cd.getComponentProperties().get(prop.name());
					if (pf != null) {
						if (pf.getType() == String.class && prop.value().length == 1) {
							pf.set(cmp, prop.value()[0]);
						}
						else {
							Class<? extends ITypeConverter> conv = (Class<? extends ITypeConverter>) controller.findConverter(pf.getType().getName());
							if (conv != null) {
								ITypeConverter converter = factory.newInstance(conv);
								Map<String, String[]> params = new HashMap<>();
								params.put("value", prop.value());
								Object v = converter.fromParametersMap("value", pf.getType(), pf.getType().getGenericSuperclass(), params, new Object[0]);
								pf.set(cmp, v);
							}
						}
					}
				}
				
				for (Property el : cd.getViewComponent().eventListeners()) {
					for (String v : el.value()) {
						cmp.addEventListener(el.name(), page, descriptor.getMethod(v));
					}
				}
				
				cmp.onInitialized(mainPage);
				
			} catch (IllegalArgumentException e) {
				LoggerUtils.getLogger(page.getClass()).log(Level.SEVERE, null, e);
			} catch (IllegalAccessException e) {
				LoggerUtils.getLogger(page.getClass()).log(Level.SEVERE, null, e);
			}
			
			
		}
		
	}
	
	private Field findField(Class cls, String name) {
		while (cls != null) {
			try {
				return cls.getDeclaredField(name);
			} catch (NoSuchFieldException e) {
				cls = cls.getSuperclass();
			} catch (SecurityException e) {
				LoggerUtils.getLogger(cls).log(Level.SEVERE, "Unable to search for field " + name, e);
			}
		}
		return null;
	}

}
