/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages.dto;

import java.lang.reflect.Method;

public class EventListenerInvocation {
	
	private String eventName;
	private Object scope;
	private Method method;
	private boolean locked = false;
	
	public EventListenerInvocation copy(boolean locked) {
		if (this.locked && !locked) {
			throw new IllegalStateException("Locked event listeners can only be cloned as locked ones");
		}
		EventListenerInvocation invocation = new EventListenerInvocation();
		invocation.eventName = eventName;
		invocation.scope = scope;
		invocation.method = method;
		invocation.locked = locked || this.locked;
		return invocation;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		if (locked) {
			throw new IllegalStateException("Unmodifiable event listener");
		}
		this.eventName = eventName;
	}

	public Object getScope() {
		return scope;
	}

	public void setScope(Object scope) {
		if (locked) {
			throw new IllegalStateException("Unmodifiable event listener");
		}
		this.scope = scope;
	}

	public Method getMethod() {
		return method;
	}

	public void setMethod(Method method) {
		if (locked) {
			throw new IllegalStateException("Unmodifiable event listener");
		}
		this.method = method;
	}
	
}
