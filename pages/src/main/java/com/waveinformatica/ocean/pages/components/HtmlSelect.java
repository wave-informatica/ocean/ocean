/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages.components;

import java.util.ArrayList;
import java.util.List;

import com.waveinformatica.ocean.pages.annotations.ViewState;
import com.waveinformatica.ocean.pages.annotations.ViewValue;

public class HtmlSelect extends AbstractFormComponent {
	
	@ViewState(attribute = false)
	private List<Option> selectOptions = new ArrayList<HtmlSelect.Option>();
	
	@ViewState
    @ViewValue
	private String value = "";
	
	public void addOption(String value, String text) {
		Option opt = new Option();
		opt.setValue(value);
		opt.setText(text);
		selectOptions.add(opt);
	}
	
    public void Clear(){
    	value = "";
        selectOptions.clear();
    }
	
	public List<Option> getSelectOptions() {
		return selectOptions;
	}

	public void setSelectOptions(List<Option> options) {
		this.selectOptions = options;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
            if(null != value)
		this.value = value;
	}

	@Override
	public String getTemplate() {
		return "/templates/components/select.tpl";
	}
	
	@Override
	public void onInitialized(UIPage page) {
		super.onInitialized(page);
	}
	
	public static class Option {
		
		private String value;
		private String text;

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

                public Option() {
                }

                public Option(String value, String text) {
                    this.value = value;
                    this.text = text;
                }
		
	}

}
