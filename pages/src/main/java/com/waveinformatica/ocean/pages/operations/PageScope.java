/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages.operations;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.controllers.scopes.AbstractScope;
import com.waveinformatica.ocean.core.controllers.scopes.TemplateScope;
import com.waveinformatica.ocean.core.security.SecurityManager;
import com.waveinformatica.ocean.pages.components.UIPage;

public class PageScope extends AbstractScope implements TemplateScope {
	
	private String pageUrl;
	private UIPage page;
	
	@Inject
	private SecurityManager sm;

	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

	public UIPage getPage() {
		return page;
	}

	public void setPage(UIPage page) {
		this.page = page;
	}

	@Override
	public String getHeaderTemplate() {
		return "/templates/form/header.tpl";
	}

	@Override
	public String getFooterTemplate() {
		return "/templates/form/footer.tpl";
	}
	
	@Override
	public boolean isAuthorized() {
		
		PageOperationInfo opInfo = new PageOperationInfo(page, page.getDescriptor().getMethod("onInitialized"));
		
		return super.isAuthorized() && sm.authorize(opInfo, getChain());
		
	}
	
}
