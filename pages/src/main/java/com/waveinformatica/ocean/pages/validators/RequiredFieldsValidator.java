/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages.validators;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.logging.Level;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.util.I18N;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.pages.components.AbstractFormComponent;
import com.waveinformatica.ocean.pages.components.UIComponent;
import com.waveinformatica.ocean.pages.components.UIPage;
import com.waveinformatica.ocean.pages.operations.ComponentDescriptor;

public class RequiredFieldsValidator implements PageValidator {

	@Inject
	private I18N i18n;
	
	@Override
	public boolean validate(UIPage page) {
		
		boolean valid = true;
		
		for (ComponentDescriptor cd : page.getDescriptor().getComponents()) {
			try {
				cd.getField().setAccessible(true);
				UIComponent cmp = (UIComponent) cd.getField().get(page);
				if (cmp instanceof AbstractFormComponent && ((AbstractFormComponent) cmp).isRequired()) {
					Field vf = cmp.getObjectDescriptor().getValueField();
					if (vf != null) {
						Object v = vf.get(cmp);
						if (v == null || (v instanceof String && StringUtils.isBlank((String) v)) ||
								(v instanceof Collection && ((Collection) v).isEmpty())) {
							((AbstractFormComponent) cmp).getErrors().add(i18n.translate("validation.required"));
							valid = false;
						}
					}
				}
			} catch (IllegalArgumentException e) {
				LoggerUtils.getLogger(cd.getField().getDeclaringClass()).log(Level.SEVERE, null, e);
			} catch (IllegalAccessException e) {
				LoggerUtils.getLogger(cd.getField().getDeclaringClass()).log(Level.SEVERE, null, e);
			}
		}
		
		return valid;
	}

}
