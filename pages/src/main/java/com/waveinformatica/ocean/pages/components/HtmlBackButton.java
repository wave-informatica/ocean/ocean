package com.waveinformatica.ocean.pages.components;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.util.OceanConversation;
import com.waveinformatica.ocean.core.util.UrlBuilder;
import com.waveinformatica.ocean.pages.annotations.LifecycleListener;
import com.waveinformatica.ocean.pages.annotations.LifecycleListener.Phase;
import com.waveinformatica.ocean.pages.dto.NavigationStackManager;

public class HtmlBackButton extends HtmlLink {
	
	@Inject
	private OceanConversation conversation;
	
	@Override
	public void onInitialized(UIPage page) {
		super.onInitialized(page);
	}
	
	@LifecycleListener(Phase.PRE_RENDER)
	public void onPreRender() {
		
		NavigationStackManager manager = NavigationStackManager.get(conversation, false);
		if (manager == null) {
			return;
		}
		
		UrlBuilder url = manager.getBack();
		if (url != null) {
			
			url.getParameters().replace("cid", conversation.getId());
			
			setHref(url.getUrl());
			
		}
		else {
			setHref("");
		}
	}
	
}
