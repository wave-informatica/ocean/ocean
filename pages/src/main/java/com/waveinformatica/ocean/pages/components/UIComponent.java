/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages.components;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.ExcludeSerialization;
import com.waveinformatica.ocean.core.templates.TemplatesManager;
import com.waveinformatica.ocean.pages.annotations.LifecycleListener;
import com.waveinformatica.ocean.pages.annotations.LifecycleListener.Phase;
import com.waveinformatica.ocean.pages.annotations.Property;
import com.waveinformatica.ocean.pages.annotations.ViewData;
import com.waveinformatica.ocean.pages.annotations.ViewState;
import com.waveinformatica.ocean.pages.dto.EventListenerInvocation;
import com.waveinformatica.ocean.pages.operations.ComponentDescriptor;
import com.waveinformatica.ocean.pages.operations.ObjectDescriptor;

import freemarker.core.Environment;
import freemarker.ext.beans.BeanModel;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public abstract class UIComponent implements TemplateDirectiveModel {
    
	@ExcludeSerialization
    @Inject
    private TemplatesManager templatesManager;
	
	@ExcludeSerialization
	private ComponentDescriptor componentDescriptor;
	
	@ExcludeSerialization
	private ObjectDescriptor objectDescriptor;
	
	private String id;
	
	@ExcludeSerialization
	private final Map<String, List<EventListenerInvocation>> eventListeners = new HashMap<>();
	
	public abstract String getTemplate();
	
	public List<EventListenerInvocation> getEventListeners(String name) {
		List<EventListenerInvocation> result = new ArrayList<>();
		List<EventListenerInvocation> listeners = eventListeners.get(name);
		if (listeners == null) {
			return Collections.EMPTY_LIST;
		}
		else {
			for (EventListenerInvocation eli : listeners) {
				result.add(eli.copy(true));
			}
			return Collections.unmodifiableList(result);
		}
	}
	
	public void addEventListener(String name, Object scope, Method m) {
		List<EventListenerInvocation> listeners = eventListeners.get(name);
		if (listeners == null) {
			listeners = new LinkedList<>();
			eventListeners.put(name, listeners);
		}
		
		EventListenerInvocation eli = new EventListenerInvocation();
		eli.setEventName(name);
		eli.setMethod(m);
		eli.setScope(scope);
		
		listeners.add(eli);
	}
	
	public void onInitialized(UIPage page) {
		
	}
	
	@LifecycleListener(Phase.VIEW_STATE_APPLIED)
	public void onViewStateApplied() {
		
	}
	
	/**
	 * Returns the client controller module. This method should be implemented if the component requires a
	 * client controller module, else the method can safely return null.
	 * 
	 * @return the client controller module name
	 */
	protected String getClientController() {
		return null;
	}
	
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body)
			throws TemplateException, IOException {
		
		Template tpl = templatesManager.getTemplate(getTemplate());
		
		env.getOut().write("<uicomponent scope=\"components." + StringEscapeUtils.escapeHtml(id.toString()) + "\" controller=\"" + StringUtils.defaultIfBlank(getClientController(), "") + "\">");

		Environment lEnv = tpl.createProcessingEnvironment(new BeanModel(this, (BeansWrapper) env.getConfiguration().getObjectWrapper()), env.getOut());
		
		lEnv.process();
		
		env.getOut().write("</uicomponent>");
		
	}
	
	public ComponentDescriptor getComponentDescriptor() {
		return componentDescriptor;
	}

	public void setComponentDescriptor(ComponentDescriptor descriptor) {
		this.componentDescriptor = descriptor;
	}

	public ObjectDescriptor getObjectDescriptor() {
		return objectDescriptor;
	}

	public void setObjectDescriptor(ObjectDescriptor objectDescriptor) {
		this.objectDescriptor = objectDescriptor;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
        
        public boolean autoWireAttribute(Field f) {
            return true;
        }
	
	public final String buildAttributes() {
		
		StringBuilder builder = new StringBuilder();
		
		for (Field f : objectDescriptor.getViewStateFields()) {
			if (f.getAnnotation(ViewState.class).attribute() && autoWireAttribute(f)) {
				builder.append(" rv-");
				builder.append(f.getName());
				builder.append("=\"");
				builder.append("viewState.");
				builder.append(f.getName());
				builder.append("\"");
			}
		}
		
		for (Field f : objectDescriptor.getDataFields()) {
			if (f.getAnnotation(ViewData.class).attribute() && autoWireAttribute(f)) {
				builder.append(" rv-");
				builder.append(f.getName());
				builder.append("=\"");
				builder.append("viewData.");
				builder.append(f.getName());
				builder.append("\"");
			}
		}
		
		Set<String> addedServerEvents = new HashSet<>();
		
		for (Property p : componentDescriptor.getViewComponent().eventListeners()) {
			if (addedServerEvents.contains(p.name())) {
				continue;
			}
			builder.append(" rv-serveron-");
			builder.append(p.name());
			builder.append("=\"id\"");
		}
		
		for (String le : objectDescriptor.getLocalEventListeners().keySet()) {
			if (addedServerEvents.contains(le)) {
				continue;
			}
			builder.append(" rv-serveron-");
			builder.append(le);
			builder.append("=\"id\"");
		}
		
		if (!objectDescriptor.getValidateFields().isEmpty()) {
			StringBuilder validate = new StringBuilder();
			for (Field f : objectDescriptor.getValidateFields()) {
				if (validate.length() > 0) {
					validate.append(" | or ");
				}
				if (objectDescriptor.getViewStateFields().contains(f)) {
					validate.append("viewState.");
				}
				else if (objectDescriptor.getDataFields().contains(f)) {
					validate.append("viewData.");
				}
				validate.append(f.getName());
			}
			builder.append(" rv-validate=\"");
			builder.append(validate.toString());
			builder.append("\"");
		}
		
		return builder.toString();
		
	}
	
}
