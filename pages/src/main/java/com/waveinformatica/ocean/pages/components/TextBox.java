/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages.components;

import com.waveinformatica.ocean.pages.annotations.ViewData;
import com.waveinformatica.ocean.pages.annotations.ViewState;
import com.waveinformatica.ocean.pages.annotations.ViewValue;

public class TextBox extends AbstractFormComponent {

	@ViewData
	private String placeholder;
        
    @ViewData
	private int rows = 1;
        
    @ViewData
    private boolean isTextarea;
    
    @ViewData
    private String type = "text";
    
    @ViewData
    private String step = "any";

	@ViewState
    @ViewValue
	private String value;
	
	@Override
	public String getTemplate() {
		return "/templates/components/textbox.tpl";
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStep() {
		return step;
	}

	public void setStep(String step) {
		this.step = step;
	}

	public String getPlaceholder() {
		return placeholder;
	}

	public void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
	}

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
        isTextarea = rows > 1;
    }

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
        if(null != value)
        	this.value = value;
	}
	
	public void onInitialized(UIPage page) {
        super.onInitialized(page);
        isTextarea = rows > 1;
	}

}
