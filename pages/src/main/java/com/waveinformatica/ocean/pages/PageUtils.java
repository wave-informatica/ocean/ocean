/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.dto.OperationContext;
import com.waveinformatica.ocean.core.converters.ITypeConverter;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.LazyRequestParameterMap;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.pages.annotations.LifecycleListener;
import com.waveinformatica.ocean.pages.components.UIComponent;
import com.waveinformatica.ocean.pages.components.UIPage;

public class PageUtils {
	
	public static final String EVT_COMPONENT_ID = "componentId";
	
	public static void executeLifecycleListener(UIPage page, LifecycleListener.Phase phase, OperationContext ctx) {
		List<Method> methods = page.getDescriptor().getLifecycleListeners().get(phase);
		if (methods != null) {
			for (Method m : methods) {
				executeLifecycleListener(page, m, ctx, null);
			}
		}
	}
	
	public static void executeLifecycleListener(UIComponent component, LifecycleListener.Phase phase, OperationContext ctx) {
		List<Method> methods = component.getObjectDescriptor().getLifecycleListeners().get(phase);
		if (methods != null) {
			for (Method m : methods) {
				executeLifecycleListener(component, m, ctx, null);
			}
		}
	}
    
    public static void executeLifecycleListener(Object o, Method m, OperationContext ctx, JsonObject paramsMap) {
    	
    	Class[] types = m.getParameterTypes();
    	Type[] genericTypes = m.getGenericParameterTypes();
    	Object[] params = new Object[types.length];
    	
		HttpServletRequest req = Context.get().getRequest();
		LazyRequestParameterMap reqParamsMap = new LazyRequestParameterMap(req);
    	
    	Gson gson = null;
    	if (paramsMap != null) {
    		gson = JsonUtils.getBuilder().create();
    	}
    	
    	for (int i = 0; i < params.length; i++) {
    		boolean found = false;
    		String pName = null;
    		for (Annotation a : m.getParameterAnnotations()[i]) {
    			if (a.annotationType() == Param.class) {
    				pName = ((Param) a).value();
    				break;
    			}
    		}
    		if (paramsMap != null) {
    			if (StringUtils.isNotBlank(pName)) {
    				JsonElement pVal = paramsMap.get(pName);
    				params[i] = pVal != null ? gson.fromJson(pVal, types[i]) : null;
    				found = true;
    			}
    		}
    		if (!found && StringUtils.isNotBlank(pName)) {
    			CoreController controller = Application.getInstance().getCoreController();
    			Class<? extends ITypeConverter> converterCls = controller.findConverter(types[i].getName());
    			if (converterCls != null) {
    				ITypeConverter converter = Application.getInstance().getObjectFactory().newInstance(converterCls);
    				params[i] = converter.fromParametersMap(pName, types[i], genericTypes[i], reqParamsMap, ctx.getExtraObjects().toArray(new Object[ctx.getExtraObjects().size()]));
    				found = params[i] != null;
    			}
    		}
    		if (!found && ctx != null && StringUtils.isBlank(pName)) {
	    		for (Object eo : ctx.getExtraObjects()) {
					if (types[i].isAssignableFrom(eo.getClass())) {
						params[i] = eo;
						found = true;
						break;
					}
				}
    		}
			if (!found && StringUtils.isBlank(pName)) {
				Object eo = Application.getInstance().getObjectFactory().getBean(m, i);
				if (eo != null) {
					params[i] = eo;
					found = true;
				}
			}
    		if (!found) {
    			params[i] = null;
    		}
    	}
    	
    	try {
    		
			m.invoke(o, params);
			
		} catch (IllegalAccessException e) {
			LoggerUtils.getLogger(o.getClass()).log(Level.SEVERE, null, e);
		} catch (IllegalArgumentException e) {
			LoggerUtils.getLogger(o.getClass()).log(Level.SEVERE, null, e);
		} catch (InvocationTargetException e) {
			LoggerUtils.getLogger(o.getClass()).log(Level.SEVERE, null, e);
		} catch (Exception e) {
			LoggerUtils.getLogger(o != null ? o.getClass() : m.getDeclaringClass()).log(Level.SEVERE, "Unexpected error executing " + m.getName() + " on a " + (o != null ? o.getClass().getName() : "null") + " instance");
		}
    	
    }

}
