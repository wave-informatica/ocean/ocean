/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.waveinformatica.ocean.core.annotations.OceanComponent;
import com.waveinformatica.ocean.core.controllers.results.InputResult.PersistenceType;
import com.waveinformatica.ocean.pages.components.UIPage;
import com.waveinformatica.ocean.pages.validators.PageValidator;

@Retention(RUNTIME)
@Target({ TYPE, FIELD })
@OceanComponent(mustExtend = UIPage.class)
public @interface Page {
	
	String value();
	
	Class<? extends PageValidator>[] validate() default {};
	
	PersistenceType persistence() default PersistenceType.NONE;
	
}
