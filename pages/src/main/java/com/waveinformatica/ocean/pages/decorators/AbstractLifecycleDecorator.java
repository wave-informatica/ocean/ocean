package com.waveinformatica.ocean.pages.decorators;

import java.util.List;
import java.util.logging.Level;

import com.waveinformatica.ocean.core.controllers.decorators.AbstractTemplateDecorator;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.pages.PageUtils;
import com.waveinformatica.ocean.pages.annotations.LifecycleListener.Phase;
import com.waveinformatica.ocean.pages.components.UIComponent;
import com.waveinformatica.ocean.pages.components.UIPage;
import com.waveinformatica.ocean.pages.operations.ComponentDescriptor;

public abstract class AbstractLifecycleDecorator<T> extends AbstractTemplateDecorator<T> {
	
	protected void invokePreRenderLifecycle(Object o) {
		
		if (o == null) {
			return;
		}
		
		List<ComponentDescriptor> children = null;
		
		if (o instanceof UIPage) {
			UIPage page = (UIPage) o;
			PageUtils.executeLifecycleListener(page, Phase.PRE_RENDER, getOperationContext());
			children = page.getDescriptor().getComponents();
		}
		else if (o instanceof UIComponent) {
			UIComponent cmp = (UIComponent) o;
			PageUtils.executeLifecycleListener(cmp, Phase.PRE_RENDER, getOperationContext());
			children = cmp.getObjectDescriptor().getComponents();
		}
		
		if (children != null && !children.isEmpty()) {
			for (ComponentDescriptor cd : children) {
				cd.getField().setAccessible(true);
				try {
					Object child = cd.getField().get(o);
					if (child != null) {
						invokePreRenderLifecycle(child);
					}
				} catch (IllegalArgumentException e) {
					LoggerUtils.getLogger(this.getClass()).log(Level.SEVERE, "Unexpected error invoking PRE_RENDER lifecycle on component " + cd.getField().getName() + " from " + o.getClass().getName(), e);
				} catch (IllegalAccessException e) {
					LoggerUtils.getLogger(this.getClass()).log(Level.SEVERE, "Unexpected error invoking PRE_RENDER lifecycle on component " + cd.getField().getName() + " from " + o.getClass().getName(), e);
				}
			}
		}
		
	}
	
}
