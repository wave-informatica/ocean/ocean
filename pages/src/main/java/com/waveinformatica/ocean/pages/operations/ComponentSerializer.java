/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages.operations;

import java.io.Writer;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.logging.Level;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonSyntaxException;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.OperationContext;
import com.waveinformatica.ocean.core.converters.ITypeConverter;
import com.waveinformatica.ocean.core.modules.ClassInfoCache;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.pages.PageUtils;
import com.waveinformatica.ocean.pages.annotations.LifecycleListener.Phase;
import com.waveinformatica.ocean.pages.components.UIComponent;
import com.waveinformatica.ocean.pages.components.UIPage;

public class ComponentSerializer {
	
	@Inject
	private ClassInfoCache cache;
        
        @Inject
        private CoreController controller;
        
        @Inject
        private ObjectFactory factory;
	
	public void serialize(Object obj, Writer writer) {
		
		GsonBuilder builder = JsonUtils.getBuilder();
		builder.registerTypeHierarchyAdapter(UIPage.class, new ObjectSerializer<UIPage>(cache));
		builder.registerTypeHierarchyAdapter(UIComponent.class, new ObjectSerializer<UIComponent>(cache));
		
		Gson gson = builder.create();
		gson.toJson(obj, writer);
	}
	
	public void apply(Object obj, ObjectDescriptor descriptor, JsonObject jo, OperationContext opCtx, boolean applyViewData) {
		
		Gson gson = JsonUtils.getBuilder().create();
		
		JsonObject viewState = jo.getAsJsonObject("viewState");
		
		for (Field f : descriptor.getViewStateFields()) {
			if (viewState.has(f.getName())) {
				try {
					
					f.setAccessible(true);
					f.set(obj, gson.fromJson(viewState.get(f.getName()), f.getGenericType()));
					
				} catch (JsonSyntaxException e) {
					LoggerUtils.getLogger(obj.getClass()).log(Level.SEVERE, null, e);
				} catch (IllegalArgumentException e) {
					LoggerUtils.getLogger(obj.getClass()).log(Level.SEVERE, null, e);
				} catch (IllegalAccessException e) {
					LoggerUtils.getLogger(obj.getClass()).log(Level.SEVERE, null, e);
				}
			}
		}
		
		if (applyViewData) {
			JsonObject viewData = jo.getAsJsonObject("viewData");
			if (viewData != null) {
				for (Field f : descriptor.getDataFields()) {
					if (viewData.has(f.getName())) {
						try {
							
							f.setAccessible(true);
							f.set(obj, gson.fromJson(viewData.get(f.getName()), f.getGenericType()));
							
						} catch (JsonSyntaxException e) {
							LoggerUtils.getLogger(obj.getClass()).log(Level.SEVERE, null, e);
						} catch (IllegalArgumentException e) {
							LoggerUtils.getLogger(obj.getClass()).log(Level.SEVERE, null, e);
						} catch (IllegalAccessException e) {
							LoggerUtils.getLogger(obj.getClass()).log(Level.SEVERE, null, e);
						}
					}
				}
			}
		}
		
		if (jo.has("components")) {
			JsonObject components = jo.getAsJsonObject("components");
			for (ComponentDescriptor cd : descriptor.getComponents()) {
				if (components.has(cd.getField().getName())) {
					try {
						UIComponent cmp = (UIComponent) cd.getField().get(obj);
						apply(cmp, cmp.getObjectDescriptor(), components.getAsJsonObject(cd.getField().getName()), opCtx, applyViewData);
					} catch (IllegalArgumentException e) {
						LoggerUtils.getLogger(obj.getClass()).log(Level.SEVERE, null, e);
					} catch (IllegalAccessException e) {
						LoggerUtils.getLogger(obj.getClass()).log(Level.SEVERE, null, e);
					}
				}
			}
		}
        
        if (obj instanceof UIPage) {
        	PageUtils.executeLifecycleListener(((UIPage) obj), Phase.VIEW_STATE_APPLIED, opCtx);
        }
        else if (obj instanceof UIComponent) {
            PageUtils.executeLifecycleListener(((UIComponent) obj), Phase.VIEW_STATE_APPLIED, opCtx);
        }
		
	}
	
	public void apply(Object obj, String prefix, ObjectDescriptor descriptor, Map<String, String[]> params, OperationContext opCtx) {
		
        if (descriptor.getValueField() != null && params.containsKey(prefix)) {
            
            try {
                if (descriptor.getValueField().getType() == String.class) {
                    descriptor.getValueField().setAccessible(true);
                    descriptor.getValueField().set(obj, params.get(prefix).length == 1 ? params.get(prefix)[0] : null);
                }
                else {
                    Class<ITypeConverter> converterClass = controller.findConverter(descriptor.getValueField().getType().getName());
                    if (converterClass != null) {
                        ITypeConverter converter = factory.newInstance(converterClass);

                        descriptor.getValueField().setAccessible(true);
                        descriptor.getValueField().set(obj, converter.fromParametersMap(prefix, descriptor.getValueField().getType(), descriptor.getValueField().getGenericType(), params, new Object[0]));
                    }
                }
            } catch (IllegalAccessException e) {
                LoggerUtils.getLogger(ComponentSerializer.class).log(Level.SEVERE, "Unable to set " + prefix, e);
            }
            
        }

        if (StringUtils.isNotBlank(prefix)) {
            prefix += ".";
        }
        for (ComponentDescriptor cd : descriptor.getComponents()) {
            try {
                    UIComponent cmp = (UIComponent) cd.getField().get(obj);
                    apply(cmp, prefix + cd.getField().getName(), cmp.getObjectDescriptor(), params, opCtx);
            } catch (IllegalArgumentException e) {
                    LoggerUtils.getLogger(obj.getClass()).log(Level.SEVERE, null, e);
            } catch (IllegalAccessException e) {
                    LoggerUtils.getLogger(obj.getClass()).log(Level.SEVERE, null, e);
            }
        }
        
        if (obj instanceof UIPage) {
        	PageUtils.executeLifecycleListener(((UIPage) obj), Phase.VIEW_STATE_APPLIED, opCtx);
        }
        else if (obj instanceof UIComponent) {
            PageUtils.executeLifecycleListener(((UIComponent) obj), Phase.VIEW_STATE_APPLIED, opCtx);
        }
		
	}
	
	public static class ObjectSerializer<T> implements JsonSerializer<T> {
		
		private final ClassInfoCache cache;
		
		public ObjectSerializer(ClassInfoCache cache) {
			super();
			this.cache = cache;
		}

		@Override
		public JsonElement serialize(T obj, Type typeOfObj, JsonSerializationContext ctx) {
			
			ObjectDescriptor descriptor = (ObjectDescriptor) cache.get(obj.getClass(), obj.getClass().getName() + "::" + ObjectDescriptor.class.getName(), null);
			
			JsonObject jo = new JsonObject();
			
			if (obj instanceof UIComponent) {
				jo.addProperty("id", ((UIComponent) obj).getId());
			}
			
			JsonObject viewState = new JsonObject();
			for (Field f : descriptor.getViewStateFields()) {
				try {
					f.setAccessible(true);
					Object v = f.get(obj);
					if (v != null) {
						viewState.add(f.getName(), ctx.serialize(v));
					}
				} catch (IllegalArgumentException e) {
					LoggerUtils.getLogger(ObjectSerializer.class).log(Level.SEVERE, null, e);
				} catch (IllegalAccessException e) {
					LoggerUtils.getLogger(ObjectSerializer.class).log(Level.SEVERE, null, e);
				}
			}
			jo.add("viewState", viewState);
			
			JsonObject viewData = new JsonObject();
			for (Field f : descriptor.getDataFields()) {
				try {
					f.setAccessible(true);
					Object v = f.get(obj);
					if (v != null) {
						viewData.add(f.getName(), ctx.serialize(f.get(obj)));
					}
				} catch (IllegalArgumentException e) {
					LoggerUtils.getLogger(ObjectSerializer.class).log(Level.SEVERE, null, e);
				} catch (IllegalAccessException e) {
					LoggerUtils.getLogger(ObjectSerializer.class).log(Level.SEVERE, null, e);
				}
			}
			jo.add("viewData", viewData);
			
			JsonObject components = new JsonObject();
			for (ComponentDescriptor f : descriptor.getComponents()) {
				try {
					f.getField().setAccessible(true);
					Object v = f.getField().get(obj);
					if (v != null) {
						components.add(f.getField().getName(), ctx.serialize(f.getField().get(obj)));
					}
				} catch (IllegalArgumentException e) {
					LoggerUtils.getLogger(ObjectSerializer.class).log(Level.SEVERE, null, e);
				} catch (IllegalAccessException e) {
					LoggerUtils.getLogger(ObjectSerializer.class).log(Level.SEVERE, null, e);
				}
			}
			jo.add("components", components);
			
			return jo;
		}
		
	}
	
}



















