/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages.operations;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.waveinformatica.ocean.core.annotations.Preferred;
import com.waveinformatica.ocean.core.annotations.Service;
import com.waveinformatica.ocean.core.controllers.IService;
import com.waveinformatica.ocean.core.controllers.dto.ServiceStatus;
import com.waveinformatica.ocean.core.modules.ComponentsBin;
import com.waveinformatica.ocean.pages.annotations.Page;
import com.waveinformatica.ocean.pages.components.UIPage;

@Service("PagesService")
public class PagesService implements IService, ComponentsBin.ComponentLoaded, ComponentsBin.ComponentUnloaded {
	
	@Inject @Preferred
	@Page(value = "")
	private ComponentsBin components;
	
	private Map<String, List<Class<? extends UIPage>>> pages = new HashMap<>();
	
	@PostConstruct
	public void init() {
		components.register(this);
	}
	
	public Class<? extends UIPage> getPageClass(String page) {
		List<Class<? extends UIPage>> cls = pages.get(page);
		if (cls != null && !cls.isEmpty()) {
			return cls.get(0);
		}
		return null;
	}
	
	@Override
	public boolean start() {
		return true;
	}

	@Override
	public boolean stop() {
		return true;
	}

	@Override
	public ServiceStatus getServiceStatus() {
		ServiceStatus status = new ServiceStatus();
		status.setRunning(true);
		return status;
	}

	@Override
	public void componentUnloaded(Class<? extends Annotation> annotation, Class<?> componentClass) {
		Page view = componentClass.getAnnotation(Page.class);
		List<Class<? extends UIPage>> list = pages.get(view.value());
		if (list != null) {
			list.remove(componentClass);
			if (list.isEmpty()) {
				pages.remove(view.value());
			}
		}
	}

	@Override
	public void componentLoaded(Class<? extends Annotation> annotation, Class<?> componentClass) {
		Page view = componentClass.getAnnotation(Page.class);
		List<Class<? extends UIPage>> list = pages.get(view.value());
		if (list == null) {
			list = new ArrayList<>();
			pages.put(view.value(), list);
		}
		list.add(0, (Class<? extends UIPage>) componentClass);
	}

}
