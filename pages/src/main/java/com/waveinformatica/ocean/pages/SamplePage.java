/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages;

import java.text.SimpleDateFormat;
import java.util.UUID;

import com.waveinformatica.ocean.core.controllers.results.InputResult.PersistenceType;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.pages.annotations.Page;
import com.waveinformatica.ocean.pages.annotations.Property;
import com.waveinformatica.ocean.pages.annotations.ViewComponent;
import com.waveinformatica.ocean.pages.components.DatePicker;
import com.waveinformatica.ocean.pages.components.HtmlButton;
import com.waveinformatica.ocean.pages.components.HtmlSelect;
import com.waveinformatica.ocean.pages.components.ImagesGallery;
import com.waveinformatica.ocean.pages.components.ImagesGallery.UploadHandler;
import com.waveinformatica.ocean.pages.components.ImagesGallery.UploadedFile;
import com.waveinformatica.ocean.pages.components.TextBox;
import com.waveinformatica.ocean.pages.components.UIPage;
import com.waveinformatica.ocean.pages.operations.util.ChunkUploadOperations.FileUploadTransaction;

@Page(value = "/PaginaDiProva.html", persistence = PersistenceType.CONVERSATION)
public class SamplePage extends UIPage {

	@ViewComponent(
			properties = {
					@Property(name = "placeholder", value = "Scrivi il valore del componente 1"),
					@Property(name = "value", value = "Text1 value"),
					@Property(name = "required", value = "true"),
					@Property(name = "cssClass", value = "form-control")
			},
			eventListeners = {
					@Property(name = "change", value = "text1_OnChange")
			}
	)
	private TextBox text1;
	
	@ViewComponent(
			properties = {
					@Property(name = "label", value = "Select :)"),
					@Property(name = "cssClass", value = "form-control")
			},
			eventListeners = {
					@Property(name = "change", value = "select1_OnChange")
			}
	)
	private HtmlSelect select1;
	
	@ViewComponent(
			properties = {
					@Property(name = "label", value = "Testo nuova opz."),
					@Property(name = "cssClass", value = "form-control")
			}
	)
	private TextBox optName;
	
	@ViewComponent(
			properties = {
					@Property(name = "label", value = "Valore nuova opz."),
					@Property(name = "cssClass", value = "form-control")
			}
	)
	private TextBox optValue;
	
	@ViewComponent(
			properties = {
					@Property(name = "cssClass", value = "btn btn-primary"),
					@Property(name = "html", value = "Aggiungi")
			},
			eventListeners = {
					@Property(name = "click", value = "button_OnClick")
			}
	)
	private HtmlButton button;
	
	@ViewComponent(
			properties = {
					@Property(name = "label", value = "Data"),
					@Property(name = "cssClass", value = "form-control")
			},
			eventListeners = {
					@Property(name = "change", value = "date_OnChange")
			}
	)
	private DatePicker date;
	
	@ViewComponent(
			properties = {
					@Property(name = "cssClass", value = "btn btn-danger"),
					@Property(name = "html", value = "Redirect")
			},
			eventListeners = {
					@Property(name = "click", value = "buttonRedirect_OnClick")
			}
	)
	private HtmlButton buttonRedirect;
	
	@ViewComponent
	private ImagesGallery gallery;
	
	@ViewComponent
	private ImagesGallery gallery2;
	
	@ViewComponent(
			properties = {
					@Property(name = "cssClass", value = "btn btn-warning"),
					@Property(name = "html", value = "Save images")
			},
			eventListeners = {
					@Property(name = "click", value = "buttonGallery_OnClick")
			}
	)
	private HtmlButton buttonGallery;
	
	@Override
	public String getTemplate() {
		return "/templates/paginaProva.tpl";
	}
	
	public void text1_OnChange() {
		select1.setValue(text1.getValue());
		text1.setTitle(text1.getValue());
		text1.setLabel(text1.getTitle());
		text1.setValue(text1.getValue() + " modificato");
	}
	
	public void select1_OnChange() {
		text1.setValue(select1.getValue());
	}
	
	public void button_OnClick() {
		select1.addOption(optValue.getValue(), optName.getValue());
		optValue.setValue("");
		optName.setValue("");
	}
	
	public void date_OnChange() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		text1.setValue(date.getDateValue() != null ? sdf.format(date.getDateValue()) : null);
	}
	
	public void buttonRedirect_OnClick() {
		sendRedirect(Context.get().getRequest().getContextPath() + "/");
	}
	
	public void buttonGallery_OnClick() {
		gallery.handleUploadedFiles(new UploadHandler() {
			@Override
			public String handle(FileUploadTransaction transaction) {
				UploadedFile f = new UploadedFile();
				f.setId(UUID.randomUUID().toString());
				f.setUrl("https://cc-media-foxit.fichub.com/image/floptv/018eecba-c58d-4733-9416-a28b9d8e28a0/immagini-avatar-whatsapp-03-maxw-736.jpg");
				gallery2.getExistingFiles().add(f);
				return f.getId();
			}
		});
	}
	
	public TextBox getText1() {
		return text1;
	}

	public HtmlSelect getSelect1() {
		return select1;
	}

	public TextBox getOptName() {
		return optName;
	}

	public TextBox getOptValue() {
		return optValue;
	}

	public HtmlButton getButton() {
		return button;
	}

	public DatePicker getDate() {
		return date;
	}

	public HtmlButton getButtonRedirect() {
		return buttonRedirect;
	}

	public ImagesGallery getGallery() {
		return gallery;
	}

	public ImagesGallery getGallery2() {
		return gallery2;
	}

	public HtmlButton getButtonGallery() {
		return buttonGallery;
	}

}
