/*
 * Copyright 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.pages.components;

import com.waveinformatica.ocean.core.util.Results;
import com.waveinformatica.ocean.pages.annotations.ViewData;
import com.waveinformatica.ocean.pages.annotations.ViewState;
import com.waveinformatica.ocean.pages.annotations.ViewValue;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vito
 */
public class Selectize extends AbstractFormComponent {
	
    @ViewState(attribute = false)
    private List<Option> selectOptions = new ArrayList<Option>();
    
    @ViewState(attribute = false)
    @ViewValue
    private List<String> values = new ArrayList<>();
    
    @ViewData(attribute = false)
    private boolean multiple = false;
    
    @ViewData
    private boolean create = false;
    
    @ViewData
    private boolean persist = false;
    
    @ViewData
    private boolean hideSelected = false;
    
    @ViewData
    private String placeholder;

    public void addOption(String value, String text) {
        Option opt = new Option();
        opt.setValue(value);
        opt.setText(text);
        selectOptions.add(opt);
    }

    public void Clear(){
        selectOptions.clear();
        values.clear();
    }

    public List<Option> getSelectOptions() {
        return selectOptions;
    }

    public void setSelectOptions(List<Option> options) {
        this.selectOptions = options;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }
    
    public void addValue(String value) {
        this.values.add(value);
    }
    
    public void removeValue(String value) {
        this.values.remove(value);
    }

    public boolean isMultiple() {
        return multiple;
    }

    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }

    public boolean isCreate() {
        return create;
    }

    public void setCreate(boolean create) {
        this.create = create;
    }

    public boolean isPersist() {
        return persist;
    }

    public void setPersist(boolean persist) {
        this.persist = persist;
    }

    public boolean isHideSelected() {
        return hideSelected;
    }

    public void setHideSelected(boolean hideSelected) {
        this.hideSelected = hideSelected;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    @Override
    public String getTemplate() {
        return "/templates/components/selectize.tpl";
    }

    @Override
    public void onInitialized(UIPage page) {
        super.onInitialized(page);
        
        Results.addCssSource(page, "ris/libs/selectize/css/selectize.bootstrap3.css");
        Results.addScriptSource(page, "ris/libs/selectize/js/standalone/selectize.js");
        Results.addScriptSource(page, "ris/components/selectize/selectize.js");
        Results.addCssSource(page, "ris/components/selectize/selectize.css");
        
    }

    @Override
    protected String getClientController() {
        return "pages.selectize";
    }

    @Override
    public boolean autoWireAttribute(Field f) {
        return !f.getName().equals("show");
    }
    
    public static class Option {

        private String value;
        private String text;

        public String getValue() {
                return value;
        }

        public void setValue(String value) {
                this.value = value;
        }

        public String getText() {
                return text;
        }

        public void setText(String text) {
                this.text = text;
        }

        public Option() {
        }

        public Option(String value, String text) {
            this.value = value;
            this.text = text;
        }

    }
    
}
