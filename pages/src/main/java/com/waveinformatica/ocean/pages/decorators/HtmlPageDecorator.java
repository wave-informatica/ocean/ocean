/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages.decorators;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.xhtmlrenderer.extend.ReplacedElementFactory;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.swing.Java2DRenderer;
import org.xhtmlrenderer.util.FSImageWriter;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfReader;
import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.ResultDecorator;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.decorators.WebPageDecorator;
import com.waveinformatica.ocean.core.controllers.events.PdfPostRendererEvent;
import com.waveinformatica.ocean.core.controllers.events.PdfPreRendererEvent;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.html.ChainedReplacedElementFactory;
import com.waveinformatica.ocean.core.html.Measure;
import com.waveinformatica.ocean.core.html.PDFUtils;
import com.waveinformatica.ocean.core.html.XHTMLUtils;
import com.waveinformatica.ocean.core.internal.OceanUserAgentCallback;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.pages.components.UIPage;

import freemarker.core.Environment;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@ResultDecorator(classes = UIPage.class, mimeTypes = { MimeTypes.HTML, MimeTypes.PDF, MimeTypes.PNG, MimeTypes.JPEG })
public class HtmlPageDecorator extends AbstractLifecycleDecorator<UIPage> {

	@Inject
	private CoreController coreController;

	@Inject
	private Application application;
	
	@Inject
	private Configuration configuration;

	private String pageBaseUrl;

	@Override
	public void decorate(UIPage result, OutputStream out, MimeTypes type) {
		
		invokePreRenderLifecycle(result);

		Template tpl = loadTemplate(result.getTemplate());
		Writer outWriter = new OutputStreamWriter(out);

		if (type != MimeTypes.HTML) {
			outWriter = new StringWriter();
		}
		
		try {
			
			Environment env = tpl.createProcessingEnvironment(result, outWriter);
			
			if (getExtraObjects() != null) {
				for (String k : getExtraObjects().keySet()) {
					env.setGlobalVariable("_" + k, env.getObjectWrapper().wrap(getExtraObjects().get(k)));
				}
			}
			
			env.process();
			
		}
		catch (TemplateException ex) {
			Logger.getLogger(WebPageDecorator.class.getName()).log(Level.SEVERE, null, ex);
		}
		catch (IOException ex) {
			Logger.getLogger(WebPageDecorator.class.getName()).log(Level.SEVERE, null, ex);
		}

		if (type != MimeTypes.HTML) {
			try {
				if (pageBaseUrl == null) {
					if (Context.get() != null && Context.get().getRequest() != null) {
						setPageBaseUrl(Context.get().getRequest(), "localhost");
					}
					else {
						setPageBaseUrl(application.getContext());
					}
				}
				if (((BaseResult) result).getName() == null) {
					((BaseResult) result).setName("export");
				}
				if (type == MimeTypes.PDF) {
					convertToPdf((StringWriter) outWriter, out, pageBaseUrl);
				}
				else if (type == MimeTypes.PNG || type == MimeTypes.JPEG) {
					convertToImage((StringWriter) outWriter, out, pageBaseUrl, type);
				}
			}
			catch (IOException ex) {
				Logger.getLogger(WebPageDecorator.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	@Override
	public void decorate(UIPage result, HttpServletRequest request, HttpServletResponse response, MimeTypes type)
	{
		
		invokePreRenderLifecycle(result);

		response.setStatus(result.getStatusCode());
		if (type == MimeTypes.HTML) {
			response.setCharacterEncoding("UTF-8");
		}

		Template tpl = loadTemplate(result.getTemplate());

		Writer outWriter = null;
		if (type == MimeTypes.HTML) {
			try {
				outWriter = response.getWriter();
			}
			catch (IOException ex) {
				Logger.getLogger(WebPageDecorator.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		else {
			outWriter = new StringWriter();
		}
		
		try {
			
			Environment env = tpl.createProcessingEnvironment(result, outWriter);
			
			if (getExtraObjects() != null) {
				for (String k : getExtraObjects().keySet()) {
					env.setGlobalVariable("_" + k, env.getObjectWrapper().wrap(getExtraObjects().get(k)));
				}
			}
			
			env.process();
			
		}
		catch (TemplateException ex) {
			Logger.getLogger(WebPageDecorator.class.getName()).log(Level.SEVERE, null, ex);
		}
		catch (IOException ex) {
			Logger.getLogger(WebPageDecorator.class.getName()).log(Level.SEVERE, null, ex);
		}

		if (type == MimeTypes.PDF || type == MimeTypes.JPEG || type == MimeTypes.PNG) {
			try {
				if (pageBaseUrl == null) {
					setPageBaseUrl(request, "localhost");
				}
				if (result.getName() == null) {
					result.setName("export");
				}
				if (!request.getParameterMap().containsKey("debug")) {
					response.setHeader("Content-disposition", (result.isAttachment() ? "attachment" : "inline") + "; filename=\"" + result.getName() + "." + type.getExtensions()[0] + "\"");
					if (type == MimeTypes.PDF) {
						convertToPdf((StringWriter) outWriter, response.getOutputStream(), pageBaseUrl);
					}
					else if (type == MimeTypes.JPEG || type == MimeTypes.PNG) {
						convertToImage((StringWriter) outWriter, response.getOutputStream(), pageBaseUrl, type);
					}
				}
				else {
					response.getWriter().write(outWriter.toString());
				}
			}
			catch (IOException ex) {
				Logger.getLogger(WebPageDecorator.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	public String getPageBaseUrl()
	{
		return pageBaseUrl;
	}

	public void setPageBaseUrl(String pageBaseUrl)
	{
		this.pageBaseUrl = pageBaseUrl;
	}

	public void setPageBaseUrl(HttpServletRequest request, String host)
	{
		StringBuilder baseUrl = new StringBuilder();
		baseUrl.append(request.getScheme());
		baseUrl.append("://");
		baseUrl.append(StringUtils.isNotBlank(host) ? host : request.getServerName());
		if (request.getServerPort() != 80 && request.getServerPort() > 0) {
			baseUrl.append(':');
			baseUrl.append(request.getServerPort());
		}
		baseUrl.append(request.getContextPath());
		baseUrl.append('/');
		pageBaseUrl = baseUrl.toString();
	}

	public void setPageBaseUrl(ServletContext context)
	{
		StringBuilder baseUrl = new StringBuilder();
		String site = configuration.getSiteProperty("site.url", "http://localhost:8080");
		if (site.endsWith("/")) {
			site = site.substring(0, site.length() - 1);
		}
		baseUrl.append(site);
		baseUrl.append(context.getContextPath());
		baseUrl.append('/');
		pageBaseUrl = baseUrl.toString();
	}

	private void convertToPdf(StringWriter writer, OutputStream output, String baseUrl)
		throws IOException
	{
		System.setProperty("java.awt.headless", "true");
		
		// Cleaning HTML input
		Document xmlDoc = XHTMLUtils.toDocument(writer.toString());
		PDFUtils.prepareDocument(xmlDoc);
		
		ITextRenderer renderer = new ITextRenderer();
		
		// Setting the UserAgent for retrieving resources
		renderer.getSharedContext().setUserAgentCallback(new OceanUserAgentCallback(renderer));
		
		ReplacedElementFactory factory = renderer.getSharedContext().getReplacedElementFactory();
		if (factory == null || !(factory instanceof ChainedReplacedElementFactory)) {
			ChainedReplacedElementFactory newFactory = new ChainedReplacedElementFactory();
			if (factory != null) {
				newFactory.addFactory(factory);
			}
			factory = newFactory;
		}
		renderer.getSharedContext().setReplacedElementFactory(factory);

		// Firing event to populate renderer with extra features
		String operation = Context.get().getRequest().getPathInfo();
		PdfPreRendererEvent preEvent = new PdfPreRendererEvent(
			this, operation, xmlDoc, renderer, renderer.getSharedContext(), null);
		coreController.dispatchEvent(preEvent);
		// ... end event

		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		renderer.setDocument(xmlDoc, baseUrl);
		renderer.layout();
		try {
			renderer.createPDF(bytes);
		}
		catch (DocumentException ex) {
			Logger.getLogger(WebPageDecorator.class.getName()).log(Level.SEVERE, null, ex);
		}

		PdfReader reader = new PdfReader(bytes.toByteArray());
		PdfPostRendererEvent postEvent = new PdfPostRendererEvent(
			this, operation, reader, output, null, renderer);
		coreController.dispatchEvent(postEvent);

		if (! postEvent.hasWriter())
			postEvent.getStamper();

		try {
			if (postEvent.hasWriter()) {
				postEvent.getWriter(null).close();
			}
			else if (postEvent.hasStamper()) {
				postEvent.getStamper().close();
			}
		}
		catch (com.itextpdf.text.DocumentException ex) {
			Logger.getLogger(WebPageDecorator.class.getName()).log(Level.SEVERE, null, ex);
		}
		finally {
			reader.close();
		}
	}

	private void convertToImage(StringWriter writer, OutputStream output, String baseUrl, MimeTypes type)
		throws IOException
	{
		System.setProperty("java.awt.headless", "true");
		
		int width = 1024;
		int height = -1;
		
		// Cleaning HTML input
		Document xmlDoc = XHTMLUtils.toDocument(writer.toString());
		PDFUtils.PrepareResult prepRes = PDFUtils.prepareDocument(xmlDoc);
		
		if (prepRes.getWidth() != null) {
			width = prepRes.getWidth().convert(Measure.Unit.PX).getValue().intValue();
		}
		if (prepRes.getHeight()!= null) {
			height = prepRes.getHeight().convert(Measure.Unit.PX).getValue().intValue();
		}
		
		try {
			HttpServletRequest request = Context.get().getRequest();
			if (request != null) {
				String sizeStr = request.getParameter("fw:size");
				if (StringUtils.isNotBlank(sizeStr)) {
					String[] parts = sizeStr.split("-");
					if (parts.length == 1) {
						Measure m = new Measure(parts[0]);
						width = height = m.convert(Measure.Unit.PX).getValue().intValue();
					}
					else if (parts.length == 2) {
						Measure mw = new Measure(parts[0]);
						width = mw.convert(Measure.Unit.PX).getValue().intValue();
						Measure mh = new Measure(parts[1]);
						height = mh.convert(Measure.Unit.PX).getValue().intValue();
					}
				}
			}
		} catch (Exception e) {
			LoggerUtils.getLogger(WebPageDecorator.class).log(Level.WARNING, "Error parsing requested image size", e);
			width = 1024;
			height = 768;
		}
		
		Java2DRenderer renderer = new Java2DRenderer(xmlDoc, width, height);
		
		ReplacedElementFactory factory = renderer.getSharedContext().getReplacedElementFactory();
		if (factory == null || !(factory instanceof ChainedReplacedElementFactory)) {
			ChainedReplacedElementFactory newFactory = new ChainedReplacedElementFactory();
			if (factory != null) {
				newFactory.addFactory(factory);
			}
			factory = newFactory;
		}
		renderer.getSharedContext().setReplacedElementFactory(factory);
		
		// Adjusting fonts anti-aliasing
		renderer.getSharedContext().getTextRenderer().setSmoothingThreshold(0);
		
		// Setting the UserAgent for retrieving resources
		renderer.getSharedContext().setUserAgentCallback(new OceanUserAgentCallback(renderer));

		// Firing event to populate renderer with extra features
		String operation = Context.get().getRequest().getPathInfo();
		PdfPreRendererEvent preEvent = new PdfPreRendererEvent(
			this, operation, xmlDoc, null, renderer.getSharedContext(), null);
		coreController.dispatchEvent(preEvent);
		// ... end event
		
		BufferedImage img = renderer.getImage();
		
		if (type == MimeTypes.JPEG) {
			FSImageWriter imageWriter = FSImageWriter.newJpegWriter(0.8f);
			imageWriter.write(img, output);
		}
		else if (type == MimeTypes.PNG) {
			FSImageWriter imageWriter = new FSImageWriter();
			imageWriter.write(img, output);
		}
		
	}
	
}
