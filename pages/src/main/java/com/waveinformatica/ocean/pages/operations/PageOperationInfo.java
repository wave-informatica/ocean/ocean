/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages.operations;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.pages.annotations.Page;
import com.waveinformatica.ocean.pages.components.UIPage;

public class PageOperationInfo extends OperationInfo {
	
	private final UIPage page;
	private final String fullName;
	private final Set<Annotation> extraAnnotations = new HashSet<Annotation>();
	
	public PageOperationInfo(UIPage page, Method method) {
		
		super(page.getClass(), method, new ArrayList<>(), false, true);

		for (Annotation a : page.getClass().getAnnotations()) {
			extraAnnotations.add(a);
		}
		
		this.page = page;
		this.fullName = page.getClass().getAnnotation(Page.class).value();
		
	}
	
	@Override
	public <T extends Annotation> T getExtraAnnotation(Class<T> annotation) {
		for (Annotation a : extraAnnotations) {
			if (a.annotationType() == annotation) {
				return (T) a;
			}
		}
		return null;
	}
	
	@Override
	public String getFullName() {
		return fullName;
	}

	public UIPage getPage() {
		return page;
	}
	
}
