package com.waveinformatica.ocean.pages.dto;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.Stack;

import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.util.OceanConversation;
import com.waveinformatica.ocean.core.util.UrlBuilder;

public class NavigationStackManager implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static NavigationStackManager get(OceanConversation conversation, boolean install) {
		NavigationStackManager manager = (NavigationStackManager) conversation.get(NavigationStackManager.class.getName());
		if (manager == null && install) {
			manager = new NavigationStackManager();
			conversation.put(NavigationStackManager.class.getName(), manager);
		}
		return manager;
	}
	
	private Stack<UrlBuilder> stack = new Stack<>();
	
	public void navigated(OperationInfo opInfo) {
		UrlBuilder urlBuilder = new UrlBuilder(opInfo.getFullName().substring(1));
		urlBuilder.getParameters().remove("cid");
		boolean remove = false;
		Iterator<UrlBuilder> iter = stack.iterator();
		while (iter.hasNext()) {
			UrlBuilder prev = iter.next();
			if (remove) {
				iter.remove();
			}
			else if (urlBuilder.getUrl(false).equals(prev.getUrl(false))) {
				remove = true;
			}
		}
		if (!remove) {
			stack.push(urlBuilder);
		}
	}
	
	public void navigated(String url, Map<String, String[]> parameters) {
		UrlBuilder urlBuilder = new UrlBuilder(url);
		urlBuilder.load(parameters);
		urlBuilder.getParameters().remove("cid");
		boolean remove = false;
		Iterator<UrlBuilder> iter = stack.iterator();
		while (iter.hasNext()) {
			UrlBuilder prev = iter.next();
			if (remove) {
				iter.remove();
			}
			else if (urlBuilder.getUrl(false).equals(prev.getUrl(false))) {
				remove = true;
			}
		}
		if (!remove) {
			stack.push(urlBuilder);
		}
	}
	
	public void navigated(String url) {
		UrlBuilder urlBuilder = new UrlBuilder(url);
		urlBuilder.getParameters().remove("cid");
		boolean remove = false;
		Iterator<UrlBuilder> iter = stack.iterator();
		while (iter.hasNext()) {
			UrlBuilder prev = iter.next();
			if (remove) {
				iter.remove();
			}
			else if (urlBuilder.getUrl(false).equals(prev.getUrl(false))) {
				remove = true;
			}
		}
		if (!remove) {
			stack.push(urlBuilder);
		}
	}
	
	public UrlBuilder getBack() {
		if (stack.isEmpty()) {
			return null;
		}
		else {
			if (stack.size() > 1) {
				return stack.get(stack.size() - 2);
			}
			else {
				return null;
			}
		}
	}
	
}
