/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages.operations;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.waveinformatica.ocean.pages.annotations.LifecycleListener;
import com.waveinformatica.ocean.pages.validators.PageValidator;

public class ObjectDescriptor {
	
	private final List<ComponentDescriptor> components = new ArrayList<>();
	private final Map<String, Method> methods = new HashMap<>();
	private final Map<String, List<Method>> localEventListeners = new HashMap<>();
	private final Set<Field> viewStateFields = new HashSet<>();
	private final Set<Field> dataFields = new HashSet<>();
    private Field valueField;
    private final List<Class<? extends PageValidator>> validators = new ArrayList<>();
    private final List<Field> validateFields = new ArrayList<>();
    private final Map<LifecycleListener.Phase, List<Method>> lifecycleListeners = new HashMap<>();

	public List<ComponentDescriptor> getComponents() {
		return components;
	}
	
	void addMethod(Method method) {
		
		if (!methods.containsKey(method.getName())) {
			methods.put(method.getName(), method);
		}
		
	}
	
	public Method getMethod(String name) {
		return methods.get(name);
	}

	public Set<Field> getViewStateFields() {
		return viewStateFields;
	}

	public Set<Field> getDataFields() {
		return dataFields;
	}

    public Field getValueField() {
        return valueField;
    }

    public void setValueField(Field valueField) {
        this.valueField = valueField;
    }

	public Map<String, List<Method>> getLocalEventListeners() {
		return localEventListeners;
	}

	public List<Class<? extends PageValidator>> getValidators() {
		return validators;
	}

	public List<Field> getValidateFields() {
		return validateFields;
	}

	public Map<LifecycleListener.Phase, List<Method>> getLifecycleListeners() {
		return lifecycleListeners;
	}
	
}
