/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages.components;

import java.io.StringWriter;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.waveinformatica.ocean.core.annotations.ExcludeSerialization;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.InputResult.PersistenceType;
import com.waveinformatica.ocean.core.exceptions.OperationNotAuthorizedException;
import com.waveinformatica.ocean.core.util.OceanConversation;
import com.waveinformatica.ocean.core.util.UrlBuilder;
import com.waveinformatica.ocean.pages.annotations.Page;
import com.waveinformatica.ocean.pages.annotations.ViewData;
import com.waveinformatica.ocean.pages.annotations.ViewEventListener;
import com.waveinformatica.ocean.pages.operations.ComponentDescriptor;
import com.waveinformatica.ocean.pages.operations.ComponentSerializer;
import com.waveinformatica.ocean.pages.operations.ObjectDescriptor;

public abstract class UIPage extends BaseResult {

    @ExcludeSerialization
    @Inject
    private ObjectFactory factory;
    
    @ExcludeSerialization
    @Inject
    protected OceanConversation conversation;
    
    @ExcludeSerialization
    private PersistenceType persistence;

    @ExcludeSerialization
    private ObjectDescriptor descriptor;

    @ExcludeSerialization
    private int statusCode = 200;

    @ViewData
    private String redirect;

    @ViewData
    private String submit;

    @ViewData
    private String cssClass = "form-horizontal";
    
    @ViewData
    private String cid;

    private boolean valid;

    public abstract String getTemplate();
    
    @PostConstruct
    public void postConstruct() {
    	if (persistence == null) {
    		persistence = this.getClass().getAnnotation(Page.class).persistence();
    	}
    	if (!conversation.isActive() && persistence == PersistenceType.CONVERSATION) {
    		conversation.forceActivation();
    	}
    	cid = conversation.getId();
    }

    public void onInitialized() {

    }

    public void onPreCheckAuthorization() throws OperationNotAuthorizedException {

    }

    public void onCheckAuthorization() throws OperationNotAuthorizedException {

    }

    @ViewEventListener("validate")
    public boolean revalidated() {
        return true;
    }

    public void sendRedirect(String url) {
    	if (conversation.isActive()) {
    		UrlBuilder urlBuilder = new UrlBuilder(url);
    		urlBuilder.getParameters().replace("cid", conversation.getId());
    		url = urlBuilder.getUrl();
    	}
        redirect = url;
    }

    public void sendSubmit(String url) {
    	if (conversation.isActive()) {
    		UrlBuilder urlBuilder = new UrlBuilder(url);
    		urlBuilder.getParameters().replace("cid", conversation.getId());
    		url = urlBuilder.getUrl();
    	}
        submit = url;
    }

    public String getCssClass() {
        return cssClass;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public ObjectDescriptor getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(ObjectDescriptor descriptor) {
        this.descriptor = descriptor;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getJsonData() {
        StringWriter writer = new StringWriter();
        factory.newInstance(ComponentSerializer.class).serialize(this, writer);
        return writer.toString();
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

	public PersistenceType getPersistence() {
		return persistence;
	}

	public void setPersistence(PersistenceType persistence) {
		this.persistence = persistence;
	}

	protected UIComponent geUitByName(String componentName) {
        return getUiByName(componentName, true);
    }
    protected UIComponent geUitByNameIgnoreCase(String componentName) {
        return getUiByName(componentName, false);
    }
    private UIComponent getUiByName(String componentName, boolean bCaseSensitive) {
        try {
            List<ComponentDescriptor> lCD = this.getDescriptor().getComponents();
            for (ComponentDescriptor cd : lCD) {
                String name = cd.getField().getName();
                boolean bCompareOk = bCaseSensitive ? name.compareTo(componentName) == 0 : name.compareToIgnoreCase(componentName) == 0;
                if(bCompareOk)
                    return (UIComponent) cd.getField().get(this);
            }
        } catch(Exception ex) {
            throw new RuntimeException(ex);
        }
        return null;
    }
}
