/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages.components;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.util.OceanSession;
import com.waveinformatica.ocean.core.util.Results;
import com.waveinformatica.ocean.pages.annotations.ViewData;
import com.waveinformatica.ocean.pages.annotations.ViewEventListener;
import com.waveinformatica.ocean.pages.annotations.ViewState;
import com.waveinformatica.ocean.pages.operations.util.ChunkUploadOperations.FileUploadTransaction;

public class ImagesGallery extends AbstractHtmlComponent {
	
	@Inject
	private OceanSession session;
	
	@ViewData
	private boolean selectable = false;
	
	@ViewData
	private boolean checkable = false;
	
	@ViewData
	private boolean descriptable = false;
	
	@ViewData
	private String accept = ".jpg";
	
	@ViewState
	private String selected;
	
	@ViewState
	private List<UploadedFile> existingFiles = new ArrayList<>();
	
	@ViewState
	private List<UploadedFile> uploadedFiles = new ArrayList<>();
	
	@ViewData
	private boolean readOnly;
	
	public boolean isSelectable() {
		return selectable;
	}

	public void setSelectable(boolean selectable) {
		this.selectable = selectable;
	}

	public boolean isCheckable() {
		return checkable;
	}

	public void setCheckable(boolean checkable) {
		this.checkable = checkable;
	}

	public boolean isDescriptable() {
		return descriptable;
	}

	public void setDescriptable(boolean descriptable) {
		this.descriptable = descriptable;
	}
	
	/**
	 * Gets the accepted file filter (comma separated list of extensions (.ext1, .ext2)
	 * 
	 * @return the accepted file filter
	 */
	public String getAccept() {
		return accept;
	}

	/**
	 * Sets the accepted file filter (comma separated list of extensions (.ext1, .ext2)
	 * 
	 * @param accept the accepted file filter
	 */
	public void setAccept(String accept) {
		this.accept = accept;
	}

	public String getSelected() {
		return selected;
	}

	public void setSelected(String selected) {
		this.selected = selected;
	}

	public List<UploadedFile> getExistingFiles() {
		return existingFiles;
	}

	public void setExistingFiles(List<UploadedFile> existingFiles) {
		this.existingFiles = existingFiles;
	}

	public List<UploadedFile> getUploadedFiles() {
		return uploadedFiles;
	}

	public void setUploadedFiles(List<UploadedFile> uploadedFiles) {
		this.uploadedFiles = uploadedFiles;
	}
	
	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	/**
	 * Handles new uploaded files. For each new uploaded file, it invokes the specified handler and,
	 * if the handler returns a not empty ID, the uploaded file is moved to the existing files list
	 * setting the returned ID. If the handler returns null or an empty string, the file is kept in
	 * the new uploaded files.
	 * 
	 * @param handler the handler to handle the uploaded file
	 */
	public void handleUploadedFiles (UploadHandler handler) {
		Iterator<UploadedFile> iter = uploadedFiles.iterator();
		while (iter.hasNext()) {
			UploadedFile uf = iter.next();
			FileUploadTransaction transaction = (FileUploadTransaction) session.get(uf.getTransaction());
			if (transaction != null) {
				String id = handler.handle(transaction);
				if (StringUtils.isNotBlank(id)) {
					uf.setId(id);
					uf.setTransaction(null);
					existingFiles.add(uf);
					iter.remove();
					if (transaction.getTempFile().exists()) {
						transaction.getTempFile().delete();
					}
					session.remove(transaction.getTransaction());
				}
			}
		}
	}
	
	@ViewEventListener("itemDeleted")
	public void itemDeleted(@Param("fileId") String id) {
		Iterator<UploadedFile> iter = existingFiles.iterator();
		while (iter.hasNext()) {
			UploadedFile uf = iter.next();
			if (id != null && id.equals(uf.getId())) {
				iter.remove();
				break;
			}
		}
	}

	@Override
	public String getTemplate() {
		return "/templates/components/chunkUpload-gallery.tpl";
	}
	
	@Override
	public void onInitialized(UIPage page) {
		// Upload module
		Results.addScriptSource(page, "ris/components/modules/upload.js");
		
		// Gallery resources
		Results.addCssSource(page, "ris/components/images-gallery/chunkedUpload.css");
		Results.addCssSource(page, "ris/components/images-gallery/circle.css");
		
		// Luminous
		Results.addScriptSource(page, "ris/components/images-gallery/Luminous.min.js");
		Results.addCssSource(page, "ris/components/images-gallery/luminous-basic.min.css");
		
		// Gallery controller
		Results.addScriptSource(page, "ris/components/images-gallery/images-gallery.js");
		
	}
	
	@Override
	protected String getClientController() {
		return "pages.imagesGallery";
	}
	
	public static interface UploadHandler {
		public String handle(FileUploadTransaction transaction);
	}
	
	public static class UploadedFile {
		
		private String id;
		private String transaction;
		private String url;
		private String bigUrl;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getTransaction() {
			return transaction;
		}

		public void setTransaction(String transaction) {
			this.transaction = transaction;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public String getBigUrl() {
			return bigUrl;
		}

		public void setBigUrl(String bigUrl) {
			this.bigUrl = bigUrl;
		}
		
	}
}
