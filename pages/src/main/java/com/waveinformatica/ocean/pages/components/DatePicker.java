/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages.components;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.input.DateFieldHandler;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.pages.annotations.ViewData;
import com.waveinformatica.ocean.pages.annotations.ViewState;
import com.waveinformatica.ocean.pages.annotations.ViewValue;

public class DatePicker extends AbstractFormComponent {
	
	@ViewState
        @ViewValue
	private String value = "";
	
	@ViewState
	private String format = "dd/MM/yyyy";
	
	@ViewData
	private String clientFormat;
	
	private Date date;
	private SimpleDateFormat sdf;
	private DateFieldHandler handler;
	
	@Inject
	private ObjectFactory factory;
	
	@Override
	public void onInitialized(UIPage page) {
		super.onInitialized(page);
		
		handler = factory.newInstance(DateFieldHandler.class);
		
		DateFieldHandler.loadResources(page);
	}
	
	@Override
	public void onViewStateApplied() {
		
		super.onViewStateApplied();
		
		sdf = new SimpleDateFormat(format);
		if (StringUtils.isNotBlank(value)) {
			try {
				date = sdf.parse(value);
			} catch (ParseException e) {
				LoggerUtils.getLogger(DatePicker.class).log(Level.SEVERE, "Unable to parse date " + value, e);
			}
		}
		
		clientFormat = handler.getClientFormat(format);
		
	}
	
	@Override
	public String getTemplate() {
		return "/templates/components/datePicker.tpl";
	}
	
	public Date getDateValue() {
		return date;
	}
	
	public void setDateValue(Date date) {
            if(null != date){
                if (sdf == null) {
                    sdf = new SimpleDateFormat(format);
                }
                this.date = date;
		value = sdf.format(date);
            }
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
		if (StringUtils.isNotBlank(value)) {
			try {
				date = sdf.parse(value);
			} catch (ParseException e) {
				LoggerUtils.getLogger(DatePicker.class).log(Level.SEVERE, "Unable to parse date " + value, e);
			}
		}
		else {
			date = null;
		}
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
		this.sdf = new SimpleDateFormat(format);
		clientFormat = handler.getClientFormat(format);
	}

	public String getClientFormat() {
		return clientFormat;
	}
	
}
