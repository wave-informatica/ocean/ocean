/*
 * Copyright 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.pages.components;

import com.waveinformatica.ocean.pages.annotations.ViewData;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vito
 */
public class MessageStack extends UIComponent {
    
    @ViewData
    private List<Message> messages = new ArrayList<>();
    
    public void add(String message) {
        messages.add(new Message(message));
    }
    
    public void add(Level level, String message) {
        messages.add(new Message(level, message));
    }
    
    @Override
    public String getTemplate() {
        return "/templates/components/messageStack.tpl";
    }
    
    public static enum Level {
        INFO,
        WARNING,
        CRITICAL,
        SUCCESS
    }
    
    public static class Message {
        
        private boolean info = false;
        private boolean warning = false;
        private boolean success = false;
        private boolean critical = false;
        private String message;
        
        public Message(Level level, String message) {
            switch (level) {
                case INFO:
                    info = true;
                    break;
                case SUCCESS:
                    success = true;
                    break;
                case WARNING:
                    warning = true;
                    break;
                case CRITICAL:
                    critical = true;
                    break;
            }
            this.message = message;
        }
        
        public Message(String message) {
            this.info = true;
            this.message = message;
        }

        public boolean isInfo() {
            return info;
        }

        public boolean isWarning() {
            return warning;
        }

        public boolean isSuccess() {
            return success;
        }

        public boolean isCritical() {
            return critical;
        }

        public String getMessage() {
            return message;
        }
        
    }
    
}
