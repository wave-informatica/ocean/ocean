package com.waveinformatica.ocean.pages.components;

import com.waveinformatica.ocean.pages.annotations.ViewData;

public class HtmlLink extends AbstractHtmlComponent {
	
	@ViewData
	private String href;
	
	@ViewData
	private String target;
	
	@ViewData
	private String html;
	
	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	@Override
	public String getTemplate() {
		return "/templates/components/link.tpl";
	}

}
