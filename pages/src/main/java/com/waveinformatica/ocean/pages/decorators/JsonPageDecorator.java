/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages.decorators;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.logging.Level;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.waveinformatica.ocean.core.annotations.ResultDecorator;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.decorators.IDecorator;
import com.waveinformatica.ocean.core.controllers.results.InputResult.PersistenceType;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.OceanConversation;
import com.waveinformatica.ocean.core.util.OceanSession;
import com.waveinformatica.ocean.core.util.RequestCache;
import com.waveinformatica.ocean.pages.components.UIComponent;
import com.waveinformatica.ocean.pages.components.UIPage;
import com.waveinformatica.ocean.pages.operations.ComponentSerializer;

@ResultDecorator(classes = { UIPage.class, UIComponent.class }, mimeTypes = MimeTypes.JSON)
public class JsonPageDecorator implements IDecorator {
    
    @Inject
    private RequestCache requestCache;
    
    @Inject
    private OceanSession session;
    
    @Inject
    private OceanConversation conversation;
	
	@Inject
	private ObjectFactory factory;
	
	@Override
	public void decorate(Object result, OutputStream out, MimeTypes type) {
		decorate(result, new OutputStreamWriter(out, Charset.forName("UTF-8")));
	}

	@Override
	public void decorate(Object result, HttpServletRequest request, HttpServletResponse response, MimeTypes type) {
		
		response.setContentType("application/json;charset=UTF-8");
		
		try {
			decorate(result, response.getWriter());
		} catch (IOException e) {
			LoggerUtils.getLogger(result.getClass()).log(Level.SEVERE, null, e);
		}
	}
	
	protected void decorate(Object result, Writer writer) {
		
		StringWriter strWriter = null;
		
		if (result instanceof UIPage) {
			UIPage page = (UIPage) result;
			if (page.getPersistence() != null && page.getPersistence() != PersistenceType.NONE) {
				strWriter = new StringWriter();
			}
		}
		
		ComponentSerializer serializer = factory.newInstance(ComponentSerializer.class);
		
		serializer.serialize(result, strWriter != null ? strWriter : writer);
		
		if (result instanceof UIPage) {
			UIPage page = (UIPage) result;
			if (page.getPersistence() != null) {
				switch (page.getPersistence()) {
				case REQUEST:
					requestCache.put(page.getClass().getName() + "[pages.viewState]", strWriter.toString());
					break;
				case SESSION:
					session.put(page.getClass().getName() + "[pages.viewState]", strWriter.toString());
					break;
				case CONVERSATION:
					conversation.put(page.getClass().getName() + "[pages.viewState]", strWriter.toString());
					break;
				default:
					break;
				}
			}
		}
		
		if (strWriter != null) {
			try {
				writer.write(strWriter.toString());
			} catch (IOException e) {
				LoggerUtils.getLogger(JsonPageDecorator.class).log(Level.SEVERE, "Unable to write view state", e);
			}
		}
		
	}

}
