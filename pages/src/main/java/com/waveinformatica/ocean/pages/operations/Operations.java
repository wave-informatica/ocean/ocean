/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.pages.operations;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.logging.Level;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.jboss.weld.exceptions.IllegalStateException;

import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.OperationContext;
import com.waveinformatica.ocean.core.exceptions.OperationNotFoundException;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.OceanConversation;
import com.waveinformatica.ocean.core.util.OceanSession;
import com.waveinformatica.ocean.core.util.RequestCache;
import com.waveinformatica.ocean.pages.PageUtils;
import com.waveinformatica.ocean.pages.annotations.LifecycleListener.Phase;
import com.waveinformatica.ocean.pages.annotations.Property;
import com.waveinformatica.ocean.pages.components.UIComponent;
import com.waveinformatica.ocean.pages.components.UIPage;
import com.waveinformatica.ocean.pages.validators.PageValidator;

@OperationProvider(inScope = PageScope.class)
public class Operations {
    
    @Inject
    private ObjectFactory factory;
    
    @Inject
    private RequestCache requestCache;
    
    @Inject
    private OceanSession session;
    
    @Inject
    private OceanConversation conversation;
    
    @Operation("processPage")
    public UIPage processPage(
    		PageScope scope,
    		HttpServletRequest request,
    		OperationContext opCtx
    ) throws OperationNotFoundException, JsonIOException, JsonSyntaxException, IOException {
        
        UIPage page = scope.getPage();
        page.onInitialized();
        
        PageUtils.executeLifecycleListener(page, Phase.POST_INITIALIZED, opCtx);
        
        if (request != null && "POST".equalsIgnoreCase(request.getMethod()) && !"application/json".equals(request.getContentType())) {

            ComponentSerializer serializer = factory.newInstance(ComponentSerializer.class);
            serializer.apply(page, "", page.getDescriptor(), request.getParameterMap(), opCtx);
            
            page.onCheckAuthorization();
    		
    		boolean valid = true;
    		
    		for (Class<? extends PageValidator> c : page.getDescriptor().getValidators()) {
    			PageValidator validator = factory.newInstance(c);
    			if (!validator.validate(page)) {
    				valid = false;
    			}
    		}
    		
    		page.setValid(valid);
            
        }
        else if (request != null && "POST".equalsIgnoreCase(request.getMethod()) && "application/json".equals(request.getContentType())) {
        	
        	JsonParser parser = new JsonParser();
        	JsonObject reqObject = parser.parse(request.getReader()).getAsJsonObject();
        	JsonObject o = reqObject.getAsJsonObject("model");
        	String id = reqObject.has("id") ? reqObject.get("id").getAsString() : null;
        	String event = reqObject.has("event") ? reqObject.get("event").getAsString() : null;
        	
        	ComponentSerializer serializer = factory.newInstance(ComponentSerializer.class);
        	serializer.apply(page, page.getDescriptor(), o, opCtx, false);
            
            page.onCheckAuthorization();
        	
        	if (StringUtils.isNotBlank(event)) {
        		
        		boolean valid = true;
        		
        		for (Class<? extends PageValidator> c : page.getDescriptor().getValidators()) {
        			PageValidator validator = factory.newInstance(c);
        			if (!validator.validate(page)) {
        				valid = false;
        			}
        		}
        		
        		page.setValid(valid);
        		if (valid) {
        			page.setValid(page.revalidated());
        		}
        		
        		String[] path = id.split("\\.");
        		
        		Object parent = page;
        		Object tmpParent = parent;
        		ObjectDescriptor descriptor = page.getDescriptor();
        		ObjectDescriptor pathDescriptor = page.getDescriptor();
        		ComponentDescriptor componentDescriptor = null;
        		UIComponent cmp = null;
        		
        		if (StringUtils.isNotBlank(id)) {
	        		for (String p : path) {
	        			if (tmpParent != null) {
	        				parent = tmpParent;
	        			}
	        			boolean found = false;
	        			for (ComponentDescriptor cd : descriptor.getComponents()) {
	        				if (cd.getField().getName().equals(p)) {
	        					componentDescriptor = cd;
	        					try {
									cmp = (UIComponent) componentDescriptor.getField().get(tmpParent);
									pathDescriptor = cmp.getObjectDescriptor();
									descriptor = cmp.getObjectDescriptor();
									tmpParent = cmp;
									found = true;
								} catch (IllegalArgumentException e) {
									LoggerUtils.getLogger(Operations.class).log(Level.SEVERE, null, e);
								} catch (IllegalAccessException e) {
									LoggerUtils.getLogger(Operations.class).log(Level.SEVERE, null, e);
								}
	        				}
	        			}
	        			if (!found) {
	        				throw new OperationNotFoundException("Unable to find component " + id);
	        			}
	        		}
        		}
        		
        		if (parent instanceof UIPage) {
        			descriptor = ((UIPage) parent).getDescriptor();
        		}
        		else if (parent instanceof UIComponent) {
        			descriptor = ((UIComponent) parent).getObjectDescriptor();
        		}
        		else {
        			throw new IllegalStateException("Invalid page structure");
        		}
        		
        		boolean eventFound = false;
        		List<Method> localEventListeners = descriptor.getLocalEventListeners().get(event);
        		if (localEventListeners != null && !localEventListeners.isEmpty()) {
        			eventFound = true;
        			for (Method listener : localEventListeners) {
    					JsonObject namedParams = null;
						if (reqObject.has("eventArgs")) {
        					namedParams = reqObject.getAsJsonObject("eventArgs");
						}
						if (namedParams == null) {
							namedParams = new JsonObject();
						}
						if (!namedParams.has(PageUtils.EVT_COMPONENT_ID)) {
							namedParams.addProperty(PageUtils.EVT_COMPONENT_ID, id);
						}
    					if (cmp == null && StringUtils.isBlank(id)) {
    						PageUtils.executeLifecycleListener(page, listener, opCtx, namedParams);
    					} else {
    						PageUtils.executeLifecycleListener(cmp, listener, opCtx, namedParams);
    					}
        			}
        		}
        		if (pathDescriptor != null && cmp!=null) {
            		localEventListeners = pathDescriptor.getLocalEventListeners().get(event);
            		if (localEventListeners != null && !localEventListeners.isEmpty()) {
            			eventFound = true;
            			for (Method listener : localEventListeners) {
        					JsonObject namedParams = null;
    						if (reqObject.has("eventArgs")) {
            					namedParams = reqObject.getAsJsonObject("eventArgs");
    						}
    						if (namedParams == null) {
    							namedParams = new JsonObject();
    						}
    						if (!namedParams.has(PageUtils.EVT_COMPONENT_ID)) {
    							namedParams.addProperty(PageUtils.EVT_COMPONENT_ID, id);
    						}
        					PageUtils.executeLifecycleListener(cmp, listener, opCtx, namedParams);
            			}
            		}
        		}
        		if (componentDescriptor != null) {
	        		for (Property p : componentDescriptor.getViewComponent().eventListeners()) {
	        			if (p.name().equals(event)) {
	        				eventFound = true;
	        				for (String mName : p.value()) {
	        					Method listener = descriptor.getMethod(mName);
	        					JsonObject namedParams = null;
	    						if (reqObject.has("eventArgs")) {
	            					namedParams = reqObject.getAsJsonObject("eventArgs");
	    						}
	    						if (namedParams == null) {
	    							namedParams = new JsonObject();
	    						}
	    						if (!namedParams.has(PageUtils.EVT_COMPONENT_ID)) {
	    							namedParams.addProperty(PageUtils.EVT_COMPONENT_ID, id);
	    						}
	        					PageUtils.executeLifecycleListener(parent, listener, opCtx, namedParams);
	        				}
	        				break;
	        			}
	        		}
        		}
        		
        		if (!eventFound) {
        			throw new IllegalArgumentException("Unable to find server event " + event + " for component " + id);
        		}
        	}
        	
        }
        else {
        	
        	if (page.getPersistence() != null) {
	        	String viewState = null;
	        	
	        	switch (page.getPersistence()) {
	        	case REQUEST:
	        		viewState = (String) requestCache.get(page.getClass().getName() + "[pages.viewState]", null);
	        		break;
	        	case SESSION:
	        		viewState = (String) session.get(page.getClass().getName() + "[pages.viewState]", null);
	        		break;
	        	case CONVERSATION:
	        		viewState = (String) conversation.get(page.getClass().getName() + "[pages.viewState]");
	        		break;
        		default:
        			break;
	        	}
	        	
	        	if (StringUtils.isNotBlank(viewState)) {
	        		JsonParser parser = new JsonParser();
	            	JsonObject reqObject = parser.parse(viewState).getAsJsonObject();
	            	
	            	ComponentSerializer serializer = factory.newInstance(ComponentSerializer.class);
	            	serializer.apply(page, page.getDescriptor(), reqObject, opCtx, false);
	        	}
        	}
        	
        	page.onCheckAuthorization();
        }
        
        return page;
        
    }
    
}