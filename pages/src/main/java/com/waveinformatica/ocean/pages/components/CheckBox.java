/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/**
 * 
 */
package com.waveinformatica.ocean.pages.components;

import com.waveinformatica.ocean.pages.annotations.ViewData;
import com.waveinformatica.ocean.pages.annotations.ViewState;
import com.waveinformatica.ocean.pages.annotations.ViewValue;
import com.waveinformatica.ocean.pages.components.AbstractFormComponent;

/**
 * @author Luca Lattore
 *
 */
public class CheckBox extends AbstractFormComponent {

	@ViewState
    @ViewValue
	private String value = "";
	
	@ViewState
	private boolean checked;
	
	@ViewData
	private boolean rightAligned = false;
	
	@Override
	public String getTemplate() {
		return "/templates/components/checkbox.tpl";
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		if(null != value) {
			this.value = value;
		}
	}
	
	public void setChecked(Boolean checked) {
		if (checked != null) {
			this.checked = checked;
		}
	}

	public boolean getChecked() {
		return checked;
	}
	
	public void setRightAligned(boolean rightAligned) {
		this.rightAligned = rightAligned;
	}

	public boolean getRightAligned() {
		return rightAligned;
	}
}
