# Getting started

The Ocean Pages module is meant to implement a different web development approach, more page based (just something like what ASP.NET does with .aspx pages).

The module is based on pages and reusable components, with view state, client events managed server side, etc.

For implementing a page, the developer must define a class extending the `UIPage` class and annotate it with @Page annotation, specifying the full path to invoke the page.

For usage examples, take a look at `SamplePage` class.

## More information

1. [Pages authorization](authorization.md)