# Authorization

In Ocean Pages the authorization is implemented in three steps:

1. UIPage.onPreCheckAuthorization
2. AuthorizeOperationEvent listener
3. UIPage.onCheckAuthorization

## UIPage.onPreCheckAuthorization

This method is invoked just after the UIPage object instantiation, to pre-check if the user can invoke the current page. This method is useful for very early checks, so that nothing of the page elaboration is done if the developer already knows the user cannot execute the page, mainly for performance reasons and to avoid actions in the page's or components `onInitialized` method.

If the page should not be authorized, throw a OperationNotAuthorizedException.

## AuthorizeOperationEvent

This is the usual Ocean authorization process. Actually this event will be called twice, once to authorize the page and once to authorize the internal operation we use to have things working.

Specifically, if you do not authorize the Operations.processPage operation, none of the defined pages will be allowed, so this chapter will focus on the page's AuthorizeOperationEvent.

For the page's event, the `OperationInfo` object in the event will be constructed as follows:

- will be a `PageOperationInfo` instance (which extends OperationInfo)
- the `OperationProvider` and `Operation` annotations will be null
- the `getCls` method will return the UIPage's class
- the `getMethod` method will return the UIPage's `onInitialized` method
- the `getFullName` method will return the page's path
- the `getExtraAnnotation` method will return UIPage's class annotations
- the `getPage` method will return the page instance

This event will be fired after the `onInitialized` method is called and before the view state is applied.

## UIPage.onCheckAuthorization

This method is invoked after the view state is applied, but before the processing of any client event. At this point, implementors of this method will have all the page's data available, because the page will be initialized and the view state already applied.

If the page should not be authorized, throw a OperationNotAuthorizedException.