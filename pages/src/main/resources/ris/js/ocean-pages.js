(function () {
	
	var modules = {};
	
	window.pagesModule = function(dependencies, name, loadFunction) {
		
		if (arguments.length === 1) {
			if (typeof dependencies === 'function') {
				dependencies = dependencies();
			}
			if (typeof dependencies === 'string') {
				dependencies = [ dependencies ];
			}
			
			var result = {};
			for (var i = 0; i < dependencies.length; i++) {
				result[dependencies[i]] = modules[dependencies[i]] && modules[dependencies[i]].loaded ? modules[dependencies[i]].result : null;
			}
			return result;
		}
		else if (arguments.length === 2) {
			loadFunction = arguments[1];
			name = arguments[0];
			dependencies = [];
		}
		
		if (typeof dependencies === 'function') {
			dependencies = dependencies();
		}
		if (typeof dependencies === 'string') {
			dependencies = [ dependencies ];
		}
		if (!Array.isArray(dependencies)) {
			dependencies = [];
		}
		
		modules[name] = {
			deps: dependencies,
			name: name,
			load: loadFunction,
			loaded: false,
			result: null
		};
		
		verifyLoaded(modules[name]);
		
	};
	
	function verifyLoaded(module) {
		var allLoaded = true;
		for (var i = 0; i < module.deps.length; i++) {
			if (!modules[module.deps[i]] || !modules[module.deps[i]].loaded) {
				allLoaded = false;
				break;
			}
		}
		if (allLoaded) {
			var args = [];
			var deps = pagesModule(module.deps);
			for (var i = 0; i < module.deps.length; i++) {
				args.push(deps[module.deps[i]]);
			}
			module.result = module.load.apply(window, args);
			module.loaded = true;
			// Load dependent modules
			for (var i in modules) {
				if (modules[i].loaded) {
					continue;
				}
				verifyLoaded(modules[i]);
			}
		}
	}
	
})();

$(document).ready(function () {
	
	createBaseModule();
	
	var form = $('form[data-toggle="ocean-pages-form"]');
	form.addClass('pages-loading');
	
	var model = form.data('pages-data');
	form.attr('data-pages-data', null);
	
	$.extend(model, {
		pages: pagesModule('pages').pages
	});
	
	var formAction = form.attr('action');
	var view = null;
	
	rivets.components['uicomponent'] = {
		template: function () {
			return '<div>' + this.el.innerHTML + '</div>';
		},
		initialize: function (el, data) {
			var controller = $(el).attr('controller');
			if (controller) {
                controller = $.extend({ $el: $(el) }, pagesModule(controller)[controller]);
                data.scope.controller = controller;
			}
			data.scope.pages = model.pages;
			return data.scope;
		}
	};
	
	rivets.binders['serveron-*'] = function (el, value) {
		
		var event = this.args[0];
		var myId = this.model.id;
		
		$(el).on(this.args[0], function (evt, elm) {
			
			evt.preventDefault();
			
			fireServerEvent(myId, event);
			
		});
		
	};
	
	rivets.binders['readonly'] = function (el, value) {
		$(el).prop('readonly', value ? true : false);
	};
	
	rivets.binders['cssclass'] = function (el, value) {
		$(el).attr('class', value);
	};
	
	rivets.binders['validate'] = function (el, value) {
		$(el).attr('pages-validate', value ? 'validate' : null);
	};
	
	rivets.binders['redirect'] = function (el, value) {
		if (typeof value === 'string') {
			location.href = value;
		}
	};
        
    rivets.binders['init'] = function (el, value) {
        return value.call(this, el);
    };
        
    rivets.binders['submit'] = function (el, value) {
        if (typeof value === 'string') {
            form.attr('action', value);
            form.submit();
        }
    };
    
    rivets.formatters['='] = function (value, arg) { return value == arg; }
    rivets.formatters['<='] = function (value, arg) { return value <= arg; }
    rivets.formatters['<'] = function (value, arg) { return value < arg; }
    rivets.formatters['>'] = function (value, arg) { return value > arg; }
    rivets.formatters['>='] = function (value, arg) { return value >= arg; }
    rivets.formatters['and'] = function (value, arg) { return value && arg; }
    rivets.formatters['or'] = function (value, arg) { return value || arg; }
    rivets.formatters['not'] = function (value) { return !value; }
    rivets.formatters['isEmpty'] = function (value) { return !value || (Array.isArray(value) && value.length == 0); }
    
    form.on('change valuechange', '[pages-validate]:not([rv-serveron-change])', function (evt) {
    	if ($(this).attr('pages-validate') != 'validate') {
    		return;
    	}
    	evt.preventDefault();
    	fireServerEvent('', 'validate');
    });
	
	view = rivets.bind(form, model);
	
	form.removeClass('pages-loading');
	
	function toUrl(url) {
		try {
			return new URL(url);
		} catch (e) {
			var base = new URL(location.href);
			var pos = url.indexOf('#');
			if (pos >= 0) {
				base.hash = url.substr(pos);
				url = url.substr(0, pos);
			}
			pos = url.indexOf('?');
			if (pos >= 0) {
				base.search = url.substr(pos);
				url = url.substr(0, pos);
			}
			if (url.length > 0 && url.charAt(0) == '/') {
				url = url.substr(1);
			}
			base.pathname = $('base').attr('href') + url;
			return base;
		}
	}
	
	function fireServerEvent(componentId, eventName, args) {
		
		if (view.models.viewData.cid) {
			var data = toUrl(formAction);
			var queryData = {};
			var query = (data.search || '?').substr(1);
			query = query.split('&');
			for (var i = 0; i < query.length; i++) {
				var parts = query[i].split('=');
				queryData[parts[0]] = parts[1];
			}
			queryData.cid = view.models.viewData.cid;
			query = '';
			for (var i in queryData) {
				if (i && queryData[i]) {
					query += '&' + i + '=' + queryData[i];
				}
			}
			if (query.length > 0) {
				data.search = '?' + query.substr(1);
				formAction = data.toString();
			}
		}
		
		$.ajax({
			type: 'POST',
			url: formAction,
			data: JSON.stringify({
				model: buildViewState(view.models),
				id: componentId,
				event: eventName,
				eventArgs: args
			}),
			contentType: 'application/json',
			dataType: 'json',
			success: function (data) {
				applyComponent(view.models, data);
			}
		});
		
	}
	
	function buildViewState(m) {
		var viewState = {};
		for (var i in m) {
			if (i == 'id' || i == 'viewState') {
				viewState[i] = m[i];
			}
			else if (i == 'components') {
				viewState['components'] = {};
				for (var j in m.components) {
					viewState.components[j] = buildViewState(m.components[j]);
				}
			}
		}
		return viewState;
	}
	
	function applyComponent(model, data) {
		
		for (var i in data) {
			if (Array.isArray(data[i])) {
				model[i] = data[i];
			}
			else if (typeof data[i] === 'object') {
				if (typeof model[i] !== 'object') {
					model[i] = {};
				}
				applyComponent(model[i], data[i]);
			}
			else {
				model[i] = data[i];
			}
		}
		
	}
	
	function createBaseModule() {
		pagesModule('pages', function () {
			return {
				fireServerEvent: fireServerEvent
			};
		});
	}
	
});