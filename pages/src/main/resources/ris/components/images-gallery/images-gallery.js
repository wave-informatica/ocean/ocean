pagesModule(['pages', 'pages.upload'], 'pages.imagesGallery', function (pages, upload) {
	
	function chooseFile() {
		$(this).closest('.chunk-upload').find('input[type="file"]').click();
	}
	
	function onFilesSelected(evt, model) {
		
		var files = evt.target.files;
		
		for (var i = 0; i < files.length; i++) {
			
			var url = URL.createObjectURL(files[i]);
			
			var tmpFile = {
				url: url,
				loading: true,
				progress: 0
			};
			
			if (!model.viewState.uploadedFiles) {
				model.viewState.uploadedFiles = [];
			}
			model.viewState.uploadedFiles.push(tmpFile);
			
            upload.uploadFile(files[i], function (identifier, progress) {
            	this.transaction = identifier;
            	this.progress = progress;
            	if (progress == 100) {
            		this.loading = false;
            	}
            }, tmpFile);
        }

        $(evt.target).val('');
	}
	
	function progressClass(progress) {
		var value = parseInt(progress);
		return 'c100 p' + (isNaN(value) ? 0 : value);
	}
	
	function mergeFilesList(list1, list2) {
		return list1.concat(list2);
	}
	
	function emptyFilesList(list1, list2) {
		return list1.length === 0 && list2.length === 0;
	}
	
	function deleteImage(file) {
		return function (evt, model) {
			
			evt.preventDefault();
			
			if (file.id) {
				pages.fireServerEvent(model.id, 'itemDeleted', {
					fileId: file.id
				});
			}
			else {
				for (var i = 0; i < model.viewState.uploadedFiles.length; i++) {
					if (model.viewState.uploadedFiles[i].transaction == file.transaction) {
						var fileIndex = i;
						$.post('_pages/delete-upload', {
							identifier: file.transaction
						}, function (data) {
							if (data.success) {
								model.viewState.uploadedFiles.splice(fileIndex, 1);
							}
						}, 'json');
						break;
					}
				}
			}
		};
	}
	
	function manageLightbox (readOnly) {
		if (readOnly) {
			var el = this.$el;
			setTimeout(function () {
				new LuminousGallery(el.find('a.img-lightbox[href!=""][href]'));
			}, 0);
		}
		return readOnly;
	}
	
	return {
		chooseFile: chooseFile,
		filesSelected: onFilesSelected,
		progressClass: progressClass,
		mergeFilesList: mergeFilesList,
		deleteImage: deleteImage,
		emptyFilesList: emptyFilesList,
		manageLightbox: manageLightbox
	};
	
});