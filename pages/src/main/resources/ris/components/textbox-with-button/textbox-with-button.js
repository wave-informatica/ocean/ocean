pagesModule('pages', 'pages.textbox-with-button', function (pages) {
	
	function fireClickEvent(evt, model) {
		pages.fireServerEvent(model.id, 'buttonclick', {});
	}
	
	return {
		fireClickEvent: fireClickEvent
	};
	
});