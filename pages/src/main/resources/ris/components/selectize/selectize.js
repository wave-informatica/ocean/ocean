pagesModule(['pages'], 'pages.selectize', function (pages) {
    
    function wrap(viewData, viewState) {
        return function (el) {
            init(el, viewData, viewState);
        };
    }
    
    function init(el, viewData, viewState) {
        
        if ($(el).data('selectize')) {
            return false;
        }
        
        var me = $(el);

        me.prop('multiple', viewData.multiple);

        me.selectize({
            create: me.attr('create'),
            persist: me.attr('persist'),
            hideSelected: me.attr('hideSelected'),
            options: viewState.selectOptions.slice(),
            items: viewState.values.slice(),
            onChange: function (value) {
                if (value) {
                    viewState.values = value;
                }
                me.trigger('change');
            }
        });
        
        return true;
    }
    
    function disabled(disabled) {
    	if (this.$el.find('select')[0].selectize) {
    		if (disabled) {
    			this.$el.find('select')[0].selectize.disable();
    		}
    		else {
    			this.$el.find('select')[0].selectize.enable();
    		}
    	}
    	return disabled;
    }
    
    function updateValues (options, values, viewData) {
        var selectize = this.$el.find('select').data('selectize');
        if (selectize) {
        	
        	// Clear options
        	selectize.loadedSearches = {};
        	selectize.userOptions = {};
        	selectize.renderCache = {};
        	selectize.options = selectize.sifter.items = {};
        	selectize.lastQuery = null;
        	selectize.trigger('option_clear');
        	
        	// Clear all
            selectize.clear(true);
            
            // Refill
            selectize.addOption(options);
            for (var i = 0; i < values.length; i++) {
                selectize.addItem(values[i], true);
            }
        }
        return values;
    }
    
    return {
        wrap: wrap,
        updateValues: updateValues,
        disabled: disabled
    };
    
});