pagesModule(['pages'], 'pages.upload', function (pages) {
	
	var askBeforeClose = false;
	var uploaders = [];
	
	function uploadFile(file, progressFn, scope) {

        askBeforeClose = true;

        var uploader = new ChunkedUploader(file, {
            onUploadComplete: function () {
            	if (typeof progressFn === 'function') {
            		progressFn.call(scope || window, this.uploadIdentifier, 100);
            	}
            	startNextUpload.apply(this, arguments);
            },
            onProgress: function (end) {
            	if (typeof progressFn === 'function') {
            		var progress = Math.floor(end / this.file_size * 100);
            		progressFn.call(scope || window, this.uploadIdentifier, progress);
            	}
            }
        });

        uploaders.push(uploader);

        startNextUpload();

	}

    function startNextUpload() {
        if (uploaders.length > 0) {
            uploaders[0].start();
            uploaders = uploaders.slice(1);
        }
    }
	
	function ChunkedUploader(file, options) {
	    if (!this instanceof ChunkedUploader) {
	        return new ChunkedUploader(file, options);
	    }

	    this.file = file;

	    this.options = $.extend({
	        url: '_pages/chunk-upload',
	        onUploadComplete: function () {
	        },
	        scope: this
	    }, options);

	    this.file_size = this.file.size;
	    this.chunk_size = (1024 * 1024); // 1MB
	    this.range_start = 0;
	    this.range_end = this.chunk_size;
	    this.state = 'prepared';

	    if ('mozSlice' in this.file) {
	        this.slice_method = 'mozSlice';
	    } else if ('webkitSlice' in this.file) {
	        this.slice_method = 'webkitSlice';
	    } else {
	        this.slice_method = 'slice';
	    }
	    var uplReq = new XMLHttpRequest();
	    this.upload_request = uplReq;
	    this.upload_request.scope = this;
	    this.upload_request.onload = function () {
	        uplReq.scope._onChunkComplete();
	    };
	}

	ChunkedUploader.prototype = {
	    // Internal Methods __________________________________________________

	    _guid: function () {
	        function s4() {
	            return Math.floor((1 + Math.random()) * 0x10000)
	                    .toString(16)
	                    .substring(1);
	        }
	        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
	                s4() + '-' + s4() + s4() + s4();
	    },
	    _upload: function () {
	        var self = this,
	                chunk;

	        if (self.state !== 'uploading') {
	            return;
	        }

	        // Slight timeout needed here (File read / AJAX readystate conflict?)
	        setTimeout(function () {
	            // Prevent range overflow
	            if (self.range_end > self.file_size) {
	                self.range_end = self.file_size;
	            }

	            chunk = self.file[self.slice_method](self.range_start, self.range_end);

	            self.upload_request.open('PUT', self.options.url, true);
	            //self.upload_request.overrideMimeType('application/octet-stream');
	            self.upload_request.setRequestHeader('Accept', 'application/json');
	            self.upload_request.setRequestHeader('Content-Range', 'bytes ' + self.range_start + '-' + self.range_end + '/' + self.file_size);

	            if (self.range_start === 0) {
	                self.uploadIdentifier = self._guid();
	                self.upload_request.setRequestHeader('Upload-Filename', self.file.name);
	                self.upload_request.setRequestHeader('Upload-Content-Type', self.file.type);
	            }

	            self.upload_request.setRequestHeader('Upload-Identifier', self.uploadIdentifier);

	            self.upload_request.send(chunk);

	            // TODO
	            // From the looks of things, jQuery expects a string or a map
	            // to be assigned to the "data" option. We'll have to use
	            // XMLHttpRequest object directly for now...
	            /*$.ajax(self.options.url, {
	             data: chunk,
	             type: 'PUT',
	             mimeType: 'application/octet-stream',
	             headers: (self.range_start !== 0) ? {
	             'Content-Range': ('bytes ' + self.range_start + '-' + self.range_end + '/' + self.file_size)
	             } : {},
	             success: self._onChunkComplete
	             });*/
	        }, 20);
	    },
	    // Event Handlers ____________________________________________________

	    _onChunkComplete: function () {

	        // If the end range is already the same size as our file, we
	        // can assume that our last chunk has been processed and exit
	        // out of the function.
	        if (this.range_end === this.file_size) {
	            if (this.options.onUploadComplete) {
	                this.options.onUploadComplete.call(this.options.scope != null ? this.options.scope : this);
	            }
	            this.state = 'complete';
	            return;
	        }
	        else if (typeof this.options.onProgress === 'function') {
	        	this.options.onProgress.call(this.options.scope != null ? this.options.scope : this, this.range_end);
	        }

	        // Update our ranges
	        this.range_start = this.range_end;
	        this.range_end = this.range_start + this.chunk_size;

	        // Continue as long as we aren't paused
	        if (!this.is_paused) {
	            this._upload();
	        }
	    },
	    // Public Methods ____________________________________________________

	    start: function () {
	        this.state = 'uploading';
	        this._upload();
	    },
	    pause: function () {
	        this.state = 'paused';
	        this.is_paused = true;
	    },
	    resume: function () {
	        this.is_paused = false;
	        this.state = 'uploading';
	        this._upload();
	    },
	    cancel: function () {
	        this.state = 'cancelled';
	    }
	};
	
	return {
		uploadFile: uploadFile
	};
	
});