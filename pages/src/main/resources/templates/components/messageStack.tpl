<div class="message-stack-container">
    
    <div rv-each-msg="viewData.messages" rv-class-alert-info="msg.info" rv-class-alert-warning="msg.warning" rv-class-alert-danger="msg.critical" rv-class-alert-success="msg.success" class="alert alert-dismissible">

        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <p rv-html="msg.message"></p>

    </div>
    
</div>