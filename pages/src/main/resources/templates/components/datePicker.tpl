<div class="form-group">
	<label for="${id}" class="control-label" rv-text="viewData.label"></label>
	<div class="form-element">
        <div class="input-group date ocean-date-component" data-ocean="bootstrapDatePicker.init">
		    <input type="text" name="${id?html}" id="${id}" rv-data-format="viewData.clientFormat" ${buildAttributes()}>
		    <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
	    </div>
	</div>
</div>