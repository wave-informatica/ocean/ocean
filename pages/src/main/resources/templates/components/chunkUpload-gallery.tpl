<div class="form-group">
	<label class="control-label" rv-text="viewData.label"></label>
	<div class="form-element chunk-upload">
	    <input name="${id}" style="display:none;" multiple type="file" rv-accept="viewData.accept" rv-on-change="controller.filesSelected" rv-unless="viewData.readOnly">
	    <div class="files-container gallery" rv-lightbox="controller.manageLightbox | call viewData.readOnly">
	    	<div class="element-container" rv-each-file="controller.mergeFilesList | call viewState.existingFiles viewState.uploadedFiles">
	    		<input type="radio" class="element-radio" name="${id}__FileUpload_Selection" rv-value="file.transaction" rv-checked="file.transaction | = viewState.selected" rv-if="viewData.selectable">
	    		<div class="element">
	    			<div class="img">
	    				<a rv-href="file.bigUrl" class="img-lightbox" rv-if="viewData.readOnly">
	    					<img rv-src="file.url">
	    				</a>
	    				<img rv-src="file.url" rv-unless="viewData.readOnly">
	                    <div class="load-progress" rv-if="viewData.readOnly | not | and file.loading">
	                        <div rv-cssclass="controller.progressClass | call file.progress">
	                            <span>{ file.progress }%</span>
	                            <div class="slice">
	                                <div class="bar"></div>
	                                <div class="fill"></div>
	                            </div>
	                        </div>
	                    </div>
	    			</div>
	    			<div class="btn-toolbar" rv-unless="viewData.readOnly">
	    				<div class="btn-group">
		    				<label class="btn btn-link btn-xs" rv-if="viewData.checkable">
		    					<input type="checkbox" name="${id}__FileUpload_Check" rv-value="file.transaction" rv-checked="file.checked">
		                        <i class="fa fa-eye"></i>
		    				</label>
		    				<input type="hidden" name="${id}__FileUpload_Description" rv-value="file.description" rv-if="viewData.descriptable">
		                    <a class="btn btn-link btn-xs btn-description" rv-if="viewData.descriptable"><i class="fa fa-paragraph"></i></a>
		                </div>
	                    <div class="btn-group pull-right">
	                        <a href="#" class="btn btn-link btn-xs delete-file" rv-on-click="controller.deleteImage | call file"><i class="fa fa-trash"></i></a>
	                    </div>
	                </div>
	    		</div>
	    	</div>
	    </div>
    	<div class="upload-not-empty" rv-unless="controller.emptyFilesList | call viewState.existingFiles viewState.uploadedFiles" rv-hide="viewData.readOnly">
    		<button class="btn btn-default file-chooser" type="button" rv-on-click="controller.chooseFile"><span class="glyphicon glyphicon-plus"></span> <@i18n key="new" /></button>
    	</div>
    	<div class="upload-empty" rv-if="controller.emptyFilesList | call viewState.existingFiles viewState.uploadedFiles" rv-hide="viewData.readOnly">
    		<div>
	    		<i class="fa fa-picture-o fa-5x"></i><br>
	    		<button class="btn btn-default file-chooser" type="button" rv-on-click="controller.chooseFile"><span class="glyphicon glyphicon-plus"></span> <@i18n key="new" /></button>
	  		</div>
    	</div>
	</div>
</div>
                    
<div class="modal fade" tabindex="-1" role="dialog" id="fld_${id}_modal" rv-unless="viewData.readOnly">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="<@i18n key="close" />"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><@i18n key="ocean.chunkUpload.gallery.editDescription" /></h4>
            </div>
            <div class="modal-body">
                <textarea rows="6" class="form-control"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><@i18n key="close" /></button>
                <button type="button" class="btn btn-primary"><@i18n key="save" /></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->