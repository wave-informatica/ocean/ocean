<div class="form-group">
	<label for="${id}" class="control-label" rv-text="viewData.label"></label>
	<div class="form-element">
	    <div class="input-group" rv-if="viewData.title">
	        <input name="${id?html}" class="form-control" rv-unless="viewData.isTextarea" ${buildAttributes()}>
            <textarea name="${id?html}" class="form-control" rv-if="viewData.isTextarea" rv-rows="viewData.rows" ${buildAttributes()}></textarea>
	        <span class="input-group-btn">
	            <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" rv-title="viewData.title" data-html="true">
	                <i class="fa fa-info-circle"></i>  
	            </button>
	        </span>
	    </div>
	    <input type="text" name="${id?html}" id="${id}" class="form-control" rv-unless="viewData.title | or viewData.isTextarea" ${buildAttributes()}>
	    <textarea name="${id?html}" class="form-control" rv-if="viewData.title | not | and viewData.isTextarea" rv-rows="viewData.rows" ${buildAttributes()}></textarea>
	</div>
</div>