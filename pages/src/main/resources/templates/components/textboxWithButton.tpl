<div class="form-group">
	<label for="${id}" class="control-label" rv-text="viewData.label"></label>
	<div class="form-element">
	    <div class="input-group" rv-if="components.button.viewData.show">
	        <input name="${id?html}" class="form-control" rv-unless="viewData.isTextarea" ${buildAttributes()}>
            <textarea name="${id?html}" class="form-control" rv-if="viewData.isTextarea" rv-rows="viewData.rows" ${buildAttributes()}></textarea>
	        <span class="input-group-btn">
	            <button type="button" rv-on-click="controller.fireClickEvent" rv-disabled="viewData.readOnly" rv-class="components.button.viewData.cssClass" data-toggle="tooltip" rv-html="components.button.viewData.html" data-placement="bottom" rv-title="components.button.viewData.title" data-html="true">
	            </button>
	        </span>
	    </div>
	    <input type="text" name="${id?html}" id="${id}" class="form-control" rv-unless="components.button.viewData.show" ${buildAttributes()}>
	    <textarea name="${id?html}" class="form-control" rv-if="components.button.viewData.show | not | and viewData.isTextarea" rv-rows="viewData.rows" ${buildAttributes()}></textarea>
	</div>
</div>