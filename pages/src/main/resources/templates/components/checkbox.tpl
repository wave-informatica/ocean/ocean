<div class="form-check" rv-class="viewData.cssClass">
    <div class="form-check gr-checkbox">
        <input type="checkbox" id="${id}" rv-checked="viewState.checked" class="form-check-input">
        <label for="${id}" class="form-check-label gr-width-label" rv-html="viewData.label"></label>
    </div>
</div>