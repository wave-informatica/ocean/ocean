<div class="form-group" rv-show="viewData.show">
    <label for="${id}" class="control-label" rv-text="viewData.label"></label>
    <div class="form-element">
        <#if tooltip?has_content>
        <div class="input-group">
        </#if>
            <select class="selectize" rv-init="controller.wrap | call viewData viewState" name="${id}" id="${id}" class="form-control" rv-disabled="viewData.readOnly" rv-update="controller.updateValues | call viewState.selectOptions viewState.values" ${buildAttributes()}>
                <option value=""></option>
            </select>
            <input type="hidden" name="${id}" rv-value="viewState.value" rv-if="viewData.readOnly">
            <div rv-each-option="viewState.options">
                    { option.value }: { option.text }
            </div>
        <#if tooltip?has_content>
            <span class="input-group-btn">
                <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="<@i18n key=tooltip />" data-html="true">
                    <i class="fa fa-info-circle"></i>  
                </button>
            </span>
        </div>
        </#if>
    </div>
</div>