<form action="${_scope.pageUrl}" data-pages-data="${jsonData?html}" data-toggle="ocean-pages-form" method="post" enctype="multipart/form-data" rv-redirect="viewData.redirect" rv-submit="viewData.submit" rv-class="viewData.cssClass" data-messages="i18n/messages" <#if (_scope.page.validateOperation)?has_content>data-remote="${_scope.page.validateOperation}"</#if>>
	
    <fieldset>