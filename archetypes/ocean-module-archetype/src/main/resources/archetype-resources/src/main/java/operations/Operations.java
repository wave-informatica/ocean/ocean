/*******************************************************************************
 * Copyright 2015, 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package ${package}.operations;

import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.MessageResult;
import javax.inject.Inject;

@OperationProvider(namespace = "${artifactId}")
public class Operations {
    
    @Inject
    private ObjectFactory factory;
    
    @Operation("test")
    public MessageResult testOp() {
        
        MessageResult result = factory.newInstance(MessageResult.class);
        
        result.setMessage("Hello, world!");
        result.setType(MessageResult.MessageType.INFO);
        
        return result;
        
    }
    
}