/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.ocean.filesystem.listener;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.OceanPersistenceContext;
import com.waveinformatica.ocean.core.annotations.Service;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.IService;
import com.waveinformatica.ocean.core.controllers.dto.ServiceStatus;
import com.waveinformatica.ocean.core.exceptions.OperationNotFoundException;
import com.waveinformatica.ocean.core.tasks.OceanExecutorService;
import com.waveinformatica.ocean.core.tasks.OceanRunnable;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.PersistedProperties;
import com.waveinformatica.ocean.filesystem.listener.dto.FileEventInfo;
import com.waveinformatica.ocean.filesystem.listener.entities.DirectoryInstance;

/**
 *
 * @author Ivano
 * @author Marco Merli
 */
@Service("FileSystemListener")
public class ListenerService implements IService {

	private static final Logger logger = LoggerUtils.getCoreLogger();

	@Inject
	private CoreController coreController;

	@Inject
	private Configuration configuration;

	@Inject
	private OceanExecutorService executor;
	
	private boolean listening;
	private Map<String, DirectoryListener> listeners;

	@Override
	public boolean start()
	{
		if (listening) {
			return true;
		}

		listeners = new HashMap<String, DirectoryListener>();

		Set<DirectoryInstance> instances = new HashSet<>();

		for (DirectoryInstance dir : readConfiguration()) {
			if (instances.contains(dir))
				throw new IllegalStateException("Directory already defined: " + dir.getDirectory());

			instances.add(dir);
		}

		for (DirectoryInstance dir : instances) {

			DirectoryListener l = new DirectoryListener(this, dir);
			listeners.put(dir.getDirectory(), l);
			executor.submit(l);
		}

		listening = true;
		return true;
	}

	@Override
	public boolean stop()
	{
		if (! listening) {
			return true;
		}

		listeners = null;

		listening = false;

		return true;
	}

	@Override
	public ServiceStatus getServiceStatus()
	{
		ServiceStatus ss = new ServiceStatus();
		ss.setRunning(listening);
		ss.setManageOperation("filelistener/manage");
		return ss;

	}

	/**
	 * Adds a path to listen for modifications
	 * 
	 * @param group the group name
	 * @param path the path to listen to
	 * @param createdOperation the operation to call on creation
	 * @param modifiedOperation the operation to call on modification
	 * @param deletedOperation the operation to call on deletion
	 */
	public void addPath(String group, File path, String createdOperation, String modifiedOperation, String deletedOperation)
	{
		if (listening && listeners.containsKey(path.getAbsolutePath())) {
			return;
		}

		DirectoryInstance i = new DirectoryInstance();
		i.setName(group);
		i.setDirectory(path.getAbsolutePath());
		i.setCreatedOperation(createdOperation);
		i.setModifiedOperation(modifiedOperation);
		i.setDeletedOperation(deletedOperation);

		try {
			PersistedProperties conf = getConfiguration();
			conf.put(String.format("filesystem-listener.%s.name", group), i.getName());
			conf.put(String.format("filesystem-listener.%s.directory", group), i.getDirectory());
			conf.put(String.format("filesystem-listener.%s.deletedOperation", group),
				StringUtils.stripToEmpty(i.getDeletedOperation()));
			conf.put(String.format("filesystem-listener.%s.modifiedOperation", group),
				StringUtils.stripToEmpty(i.getModifiedOperation()));
			conf.put(String.format("filesystem-listener.%s.createdOperation", group),
				StringUtils.stripToEmpty(i.getCreatedOperation()));
			conf.store("");

			if (listening) {
				DirectoryListener dl = new DirectoryListener(this, i);
				listeners.put(path.getAbsolutePath(), dl);
				executor.submit(dl);
			}
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	/**
	 * Removes a path from list of listened paths.
	 * 
	 * @param group the group name
	 * @param path the path to remove
	 */
	public void removePath(String group, File path)
	{
		if (! listening || ! listeners.containsKey(path.getAbsolutePath())) {
			return;
		}

		DirectoryListener dl = listeners.get(path.getAbsolutePath());
		try {
			List<String> mark = new ArrayList<>();

			PersistedProperties conf = getConfiguration();
			for (Object propKey : conf.keySet()) {
				String key = (String) propKey;

				if (key.contains(group))
					mark.add(key);
			}

			for (String key : mark)
				conf.remove(key);
			conf.store("");

			dl.close();
			listeners.remove(path.getAbsolutePath());
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	/**
	 * Returns the set of watched paths
	 * 
	 * @return the set of watched paths
	 */
	public Set<String> getWatchedPaths()
	{
		if (listeners == null)
			return null;

		return listeners.keySet();
	}

	protected DirectoryInstance getDirectoryInstance(String path)
	{
		if (listeners == null || listeners.get(path) == null) {
			return null;
		}
		return listeners.get(path).getDirectory();
	}

	private Collection<DirectoryInstance> readConfiguration()
	{
		PersistedProperties prop = getConfiguration();
		if (! prop.isEmpty()) {

			Map<String, DirectoryInstance> conf = new HashMap<>();
			for (Object propKey : prop.keySet()) {
				String key = (String) propKey;

				if (key.startsWith("filesystem-listener.")) {
					int a = key.indexOf(".") + 1;
					int b = key.indexOf(".", a);

					if (b == - 1)
						throw new IllegalArgumentException("FileSystemListener configuration element is not valid: " + key);

					String group = key.substring(a, b);
					try {
						DirectoryInstance instance = conf.get(group);
						if (instance == null) {
							conf.put(group, (instance = new DirectoryInstance()));
							set("name", group, instance);
						}

						String name = key.substring(b + 1);
						set(name, prop.get(key), instance);
					}
					catch (NoSuchFieldException | SecurityException | IllegalAccessException e) {
						throw new IllegalArgumentException("FileSystemListener configuration element is not valid: " + key);
					}
				}
				else
					throw new IllegalArgumentException("FileSystemListener configuration element is unknown: " + key);
			}

			return conf.values();
		}

		return Collections.emptyList();
	}

	private void set(String name, Object value, DirectoryInstance instance)
		throws NoSuchFieldException,
		IllegalAccessException
	{
		Field field = instance.getClass().getDeclaredField(name);
		field.setAccessible(true);
		field.set(instance, value);
		field.setAccessible(false);
	}

	private PersistedProperties getConfiguration()
	{
		return configuration.getCustomProperties("filesystem-listener");
	}

	public static class DirectoryListener extends OceanRunnable {

		private final ListenerService service;
		private final DirectoryInstance directory;
		private WatchKey key;
		private WatchService watcher;
		private Path dirPath;
		private boolean listening;
		private Thread listenerThread;

		public DirectoryListener(ListenerService service, DirectoryInstance directory) {
			this.service = service;
			this.directory = directory;

			try {

				watcher = FileSystems.getDefault().newWatchService();

			}
			catch (IOException ex) {
				Logger.getLogger(ListenerService.class.getName()).log(Level.SEVERE, null, ex);
			}

			listening = false;
		}

		@Override
		public void execute()
		{

			listenerThread = Thread.currentThread();

			try {
				File dirFile = new File(directory.getDirectory());
				dirPath = FileSystems.getDefault().getPath(dirFile.getParentFile().getAbsolutePath(), dirFile.getName());
				key = dirPath.register(watcher,
					StandardWatchEventKinds.ENTRY_CREATE,
					StandardWatchEventKinds.ENTRY_DELETE,
					StandardWatchEventKinds.ENTRY_MODIFY);

				listening = true;

			}
			catch (IOException e) {
				logger.log(Level.SEVERE, "Unable to listen to events about path \"" + directory.getDirectory() + "\"", e);
				return;
			}

			while (true) {

				WatchKey wk;

				try {

					wk = watcher.take();

				}
				catch (InterruptedException ex) {
					close();
					return;
				}

				for (WatchEvent evt : wk.pollEvents()) {

					WatchEvent.Kind k = evt.kind();

					if (k == StandardWatchEventKinds.OVERFLOW) {
						continue;
					}

					WatchEvent<Path> ev = evt;
					Path filename = ev.context();
					Path child = dirPath.resolve(filename);

					String operationName = null;

					if (k == StandardWatchEventKinds.ENTRY_CREATE) {
						operationName = directory.getCreatedOperation();
					}
					else if (k == StandardWatchEventKinds.ENTRY_DELETE) {
						operationName = directory.getDeletedOperation();
					}
					else if (k == StandardWatchEventKinds.ENTRY_MODIFY) {
						operationName = directory.getModifiedOperation();
					}

					if (operationName != null && operationName.trim().length() > 0) {
						FileEventInfo info = new FileEventInfo();
						info.setDirPath(dirPath);
						info.setFile(child);
						info.setEvent(ev);
						service.executor.submit(new OperationExecutor(service, operationName, info));
					}

				}

				if (! wk.reset()) {
					close();
					break;
				}

			}

		}

		public DirectoryInstance getDirectory()
		{
			return directory;
		}

		public boolean isListening()
		{
			return listening;
		}

		public void close()
		{
			try {
				watcher.close();
				listenerThread.interrupt();
			}
			catch (IOException ex) {
				Logger.getLogger(ListenerService.class.getName()).log(Level.SEVERE, null, ex);
			}
		}

	}

	public static class OperationExecutor extends OceanRunnable {

		private final ListenerService service;
		private final String name;
		private final FileEventInfo info;

		public OperationExecutor(ListenerService service, String name, FileEventInfo info) {
			this.service = service;
			this.name = name;
			this.info = info;
		}

		@Override
		public void execute()
		{
			try {
				Map<String, String[]> params = new HashMap<String, String[]>();
				params.put("file", new String[] {
					info.getFile().toString()
				});
				service.coreController.executeOperation(name, params, info);
			}
			catch (OperationNotFoundException ex) {
				Logger.getLogger(ListenerService.class.getName()).log(Level.SEVERE, null, ex);
			}
			catch (Exception ex) {
				Logger.getLogger(ListenerService.class.getName()).log(Level.SEVERE, null, ex);
			}
		}

	}

	public static class ServiceAdapter {

		@Inject
		@OceanPersistenceContext
		private EntityManager entityManager;

		public EntityManager getEntityManager()
		{
			return entityManager;
		}

	}

}
