/*******************************************************************************
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.ocean.filesystem.listener;

import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.filesystem.listener.entities.DirectoryInstance;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;

/**
 *
 * @author Ivano
 */
@OperationProvider(namespace = "/filelistener")
public class FileSystemOperations {
    
    @Inject
    private ObjectFactory factory;
    
    @Inject
    private CoreController coreController;
    
    @Operation("manage")
    public TableResult manage() {
        
        TableResult res = factory.newInstance(TableResult.class);
        res.setParentOperation("admin/services");
        
        ListenerService service = (ListenerService) coreController.getService("FileSystemListener");
        
        Set<String> paths = service.getWatchedPaths();
        
        List<DirectoryInstance> data = new ArrayList<DirectoryInstance>(paths.size()){};
        
        for (String p : paths) {
            data.add(service.getDirectoryInstance(p));
        }
        
        res.setTableData(data);
        
        return res;
        
    }
    
}
