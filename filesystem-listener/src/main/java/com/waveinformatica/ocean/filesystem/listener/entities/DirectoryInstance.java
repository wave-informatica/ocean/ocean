/*******************************************************************************
 * Copyright 2015, 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.ocean.filesystem.listener.entities;

import java.io.Serializable;

/**
 * @author Ivano
 * @author Marco Merli
 */
public class DirectoryInstance implements Serializable {

	private static final long serialVersionUID = - 6071698689686020242L;

	private String name;
	private String directory;
	private String deletedOperation;
	private String modifiedOperation;
	private String createdOperation;

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null || ! (obj instanceof DirectoryInstance))
			return false;

		DirectoryInstance other = (DirectoryInstance) obj;
		if (directory != null)
			return directory.equals(other.getDirectory());

		else if (other.getDirectory() != null)
			return other.getDirectory().equals(directory);

		else
			return true;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDirectory()
	{
		return directory;
	}

	public void setDirectory(String directory)
	{
		this.directory = directory;
	}

	public String getDeletedOperation()
	{
		return deletedOperation;
	}

	public void setDeletedOperation(String deletedOperation)
	{
		this.deletedOperation = deletedOperation;
	}

	public String getModifiedOperation()
	{
		return modifiedOperation;
	}

	public void setModifiedOperation(String modifiedOperation)
	{
		this.modifiedOperation = modifiedOperation;
	}

	public String getCreatedOperation()
	{
		return createdOperation;
	}

	public void setCreatedOperation(String createdOperation)
	{
		this.createdOperation = createdOperation;
	}
}
