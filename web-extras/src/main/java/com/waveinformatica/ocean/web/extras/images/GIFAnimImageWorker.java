/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.web.extras.images;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.madgag.gif.fmsware.AnimatedGifEncoder;
import com.madgag.gif.fmsware.GifDecoder;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;

public class GIFAnimImageWorker extends ImageWorker {
	
	private final List<ImageWorker> frames = new ArrayList<ImageWorker>();
	private final List<Integer> delays = new ArrayList<Integer>();
	private Integer loopCount;
	
	public GIFAnimImageWorker() {
		super(MimeTypes.GIF);
	}
	
	@Override
	public void load(InputStream input) throws IOException {
		
		GifDecoder decoder = new GifDecoder();
		
		int result = decoder.read(input);
		if (result != 0) {
			throw new IOException("Animated GIF decoding error: " + result);
		}
		
		setImage(decoder.getImage());
		loopCount = decoder.getLoopCount();
		
		for (int i = 0; i < decoder.getFrameCount(); i++) {
			ImageWorker worker = ImageWorkerFactory.create(MimeTypes.GIF, false);
			worker.setImage(decoder.getFrame(i));
			frames.add(worker);
			delays.add(decoder.getDelay(i));
		}
		
		input.close();
		
	}

	@Override
	public void cut(int x, int y, int width, int height) {
		for (ImageWorker w : frames) {
			w.cut(x, y, width, height);
		}
	}

	@Override
	public void resize(ResizeMode mode, int width, int height) {
		for (ImageWorker w : frames) {
			w.resize(mode, width, height);
		}
	}

	@Override
	public void removeBorders(Rectangle rectangle) {
		if (rectangle == null) {
			// All frames will be cut in the same way, based on first frame borders
			rectangle = new Rectangle();
		}
		
		for (ImageWorker w : frames) {
			w.removeBorders(rectangle);
		}
	}

	@Override
	public byte[] buildBytes() throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		AnimatedGifEncoder encoder = new AnimatedGifEncoder();
		
		encoder.start(baos);
		
		encoder.setBackground(Color.WHITE);
		encoder.setSize(frames.get(0).getImage().getWidth(), frames.get(0).getImage().getHeight());
		encoder.setRepeat(loopCount);
		encoder.setFrameRate(1000.0f / delays.get(0));
		
		for (int i = 0; i < frames.size(); i++) {
			encoder.addFrame(frames.get(i).getImage());
			encoder.setDelay(delays.get(i));
		}
		
		encoder.finish();
		
		return baos.toByteArray();
		
	}

}
