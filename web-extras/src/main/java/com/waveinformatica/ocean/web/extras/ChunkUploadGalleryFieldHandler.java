/*
 * Copyright 2016, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.web.extras;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.util.Results;

/**
 *
 * @author Ivano
 */
public class ChunkUploadGalleryFieldHandler extends ChunkUploadFieldHandler {
	
	private boolean descriptable;

	public ChunkUploadGalleryFieldHandler() {
		setFilter(MimeTypes.JPEG, MimeTypes.PNG, MimeTypes.GIF, MimeTypes.AVI, MimeTypes.MPEG4);
	}
	
	@Override
	public String getFieldTemplate() {
		return "/templates/chunkUpload-gallery.tpl";
	}

	@Override
	public void init(InputResult result, CoreController.OperationInfo opInfo, CoreController.ParameterInfo paramInfo, InputResult.FormField field) {
		Results.addCssSource(result, "ris/css/circle.css");
		Results.addCssSource(result, "ris/css/chunkedUpload.css");
		Results.addScriptSource(result, "ris/js/mustache.min.js");
		Results.addScriptSource(result, "ris/libs/iCheck-1.x-20160316/icheck.min.js");
		Results.addScriptSource(result, "ris/js/ocean.js");
		Results.addScriptSource(result, "ris/js/videoSupport.js");
		Results.addScriptSource(result, "ris/js/chunkedUpload.js");
	}

	public boolean isDescriptable() {
		return descriptable;
	}

	public void setDescriptable(boolean descriptable) {
		this.descriptable = descriptable;
	}

}
