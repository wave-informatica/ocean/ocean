/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.web.extras;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.util.Results;

/**
 *
 * @author Ivano
 */
public class WysiwygFieldHandler implements InputResult.FieldHandler {

	@Override
	public String getFieldTemplate() {
		return "/templates/webextras/inputs/wysihtml5.tpl";
	}

	@Override
	public void init(InputResult result, CoreController.OperationInfo opInfo, CoreController.ParameterInfo paramInfo, InputResult.FormField field) {
		Results.addCssSource(result, "ris/libs/bootstrap-wysihtml5-20160311/bootstrap3-wysihtml5.min.css");
		Results.addScriptSource(result, "ris/libs/bootstrap-wysihtml5-20160311/bootstrap3-wysihtml5.all.min.js");
		Results.addScriptSource(result, "ris/libs/bootstrap-wysihtml5-20160311/inputFields.js");
	}

	@Override
	public String formatValue(InputResult result, InputResult.FormField field, Object value) {
		if (value == null) {
			return "";
		}
		return value.toString();
	}
	
}
