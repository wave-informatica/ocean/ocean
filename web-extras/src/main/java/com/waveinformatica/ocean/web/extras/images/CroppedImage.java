/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.web.extras.images;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.logging.Level;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.google.gson.Gson;
import com.itextpdf.text.pdf.codec.Base64.OutputStream;
import com.waveinformatica.ocean.core.annotations.TypeConverter;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.converters.ITypeConverter;
import com.waveinformatica.ocean.core.converters.OceanUploadConverter;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.OceanUpload;

public class CroppedImage {
	
	private OceanUpload upload;
	private ImageWorker image;
	private ImageWorker thumbnail;
	private Crop crop;
	
	public OceanUpload getUpload() {
		return upload;
	}

	public void setUpload(OceanUpload upload) {
		this.upload = upload;
	}

	public ImageWorker getImage() {
		return image;
	}

	public void setImage(ImageWorker image) {
		this.image = image;
	}

	public ImageWorker getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(ImageWorker thumbnail) {
		this.thumbnail = thumbnail;
	}

	public Crop getCrop() {
		return crop;
	}

	public void setCrop(Crop crop) {
		this.crop = crop;
	}
	
	public void save(OutputStream os) throws IOException {
		if (image != null) {
			os.write(image.buildBytes());
		}
	}
	
	public byte[] getImageBytes() throws IOException {
		if (image == null) {
			return null;
		}
		return image.buildBytes();
	}
	
	public void saveThumbnail(OutputStream os) throws IOException {
		if (thumbnail != null) {
			os.write(thumbnail.buildBytes());
		}
	}
	
	public byte[] getThumbnailBytes() throws IOException {
		if (thumbnail == null) {
			return null;
		}
		return thumbnail.buildBytes();
	}

	public static class Points {
		
		private int x1;
		private int y1;
		private int x2;
		private int y2;

		public int getX1() {
			return x1;
		}

		public void setX1(int x1) {
			this.x1 = x1;
		}

		public int getY1() {
			return y1;
		}

		public void setY1(int y1) {
			this.y1 = y1;
		}

		public int getX2() {
			return x2;
		}

		public void setX2(int x2) {
			this.x2 = x2;
		}

		public int getY2() {
			return y2;
		}

		public void setY2(int y2) {
			this.y2 = y2;
		}
		
	}
	
	public static class Crop {
		
		private Integer orientation;
		private Double zoom;
		private Points points;

		public Integer getOrientation() {
			return orientation;
		}

		public void setOrientation(Integer orientation) {
			this.orientation = orientation;
		}

		public Double getZoom() {
			return zoom;
		}

		public void setZoom(Double zoom) {
			this.zoom = zoom;
		}

		public Points getPoints() {
			return points;
		}

		public void setPoints(Points points) {
			this.points = points;
		}
		
	}
	
	@TypeConverter("com.waveinformatica.ocean.web.extras.images.CroppedImage")
	public static class Converter implements ITypeConverter {
		
		@Inject
		private ObjectFactory factory;
		
		private OceanUploadConverter uploadConverter;
		
		@PostConstruct
		public void init() {
			uploadConverter = factory.newInstance(OceanUploadConverter.class);
		}
		
		@Override
		public Object fromParametersMap(String arg0, Class arg1, Type arg2, Map<String, String[]> arg3, Object[] arg4) {
			
			CroppedImage result = null;
			
			OceanUpload upload = (OceanUpload) uploadConverter.fromParametersMap(arg0, OceanUpload.class, arg2, arg3, arg4);
			
			if (upload != null && upload.isValid()) {
				
				String[] cropParams = arg3.get(arg0 + "_crop");
				if (cropParams == null || cropParams.length != 1) {
					return null;
				}
				
				Gson gson = JsonUtils.getBuilder().create();
				Crop crop = gson.fromJson(cropParams[0], Crop.class);
				
				ImageWorker worker = ImageWorkerFactory.create(upload.getKnownContentType(), true);
				
				InputStream is = null;
				try {
					is = upload.getInputStream();
					worker.load(is);
				} catch (IOException e) {
					LoggerUtils.getLogger(CroppedImage.class).log(Level.SEVERE, null, e);
					return null;
				} finally {
					if (is != null) {
						try {
							is.close();
						} catch (IOException e) {
							LoggerUtils.getLogger(CroppedImage.class).log(Level.SEVERE, null, e);
							return null;
						}
					}
				}
				
				int width = crop.getPoints().x2 - crop.getPoints().x1;
				int height = crop.getPoints().y2 - crop.getPoints().y1;
				int resWidth = (int) Math.round(width * crop.getZoom());
				int resHeight = (int) Math.round(height * crop.getZoom());
				
				worker.cut(crop.getPoints().getX1(), crop.getPoints().getY1(), width, height);
				worker.resize(ResizeMode.CROP, resWidth, resHeight);
				
				result = new CroppedImage();
				result.setUpload(upload);
				result.setCrop(crop);
				result.setImage(null);
				result.setThumbnail(worker);
				
			}
			
			return result;
		}

		@Override
		public Object fromScalar(String arg0, Class arg1, Map<String, String[]> arg2, Object[] arg3) {
			
			return null;
		}
		
	}
	
}
