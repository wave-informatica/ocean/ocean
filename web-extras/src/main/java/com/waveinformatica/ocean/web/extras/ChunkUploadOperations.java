/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.ocean.web.extras;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.OceanSession;

/**
 *
 * @author Ivano
 */
@OperationProvider(namespace = "__ocean_chunk_upload")
public class ChunkUploadOperations {
	
	@Inject
	private OceanSession session;
	
	@Operation("chunk-upload")
	public GenericResult chunkUpload(HttpServletRequest request) {
		
		FileUploadTransaction transaction = (FileUploadTransaction) session.get(request.getHeader("Upload-Identifier"));
		if (transaction == null) {
			transaction = new FileUploadTransaction(request.getHeader("Upload-Identifier"));
			transaction.setContentType(request.getHeader("Upload-Content-Type"));
			transaction.setFileName(request.getHeader("Upload-Filename"));
			try {
				if (transaction.getFileName().lastIndexOf('.') >= 0) {
					transaction.setTempFile(File.createTempFile("chunkUpload", transaction.getFileName().substring(transaction.getFileName().lastIndexOf('.'))));
				}
				else {
					transaction.setTempFile(File.createTempFile("chunkUpload", transaction.getFileName()));
				}
			} catch (IOException ex) {
				Logger.getLogger(ChunkUploadOperations.class.getName()).log(Level.SEVERE, null, ex);
			}
			session.put(request.getHeader("Upload-Identifier"), transaction);
		}
		
		boolean success = false;
		
		try {
			byte[] buff = new byte[4096];
			int read = 0;
			
			InputStream is = request.getInputStream();
			FileOutputStream fos = null;
			int written = 0;
			try {
				fos = new FileOutputStream(transaction.getTempFile(), true);
				while ((read = is.read(buff)) > 0) {
					fos.write(buff, 0, read);
					written += read;
				}
				success = true;
			} catch (IOException e) {
				LoggerUtils.getLogger(ChunkUploadOperations.class).log(Level.SEVERE, "IOException after " + written + " bytes to file " + transaction.getTempFile().getAbsolutePath(), e);
			} finally {
				if (fos != null) {
					fos.close();
				}
			}
			
		} catch (FileNotFoundException ex) {
			Logger.getLogger(ChunkUploadOperations.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(ChunkUploadOperations.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		return new GenericResult(success);
	}
	
	@Operation("delete-upload")
	public void deleteTempFile(@Param("identifier") String identifier) {
		
		FileUploadTransaction transaction = (FileUploadTransaction) session.get(identifier);
		
		if (transaction != null) {
			session.remove(identifier);
			if (transaction.getTempFile().exists()) {
				transaction.getTempFile().delete();
			}
		}
		
	}
	
	public static class GenericResult extends BaseResult {
		
		private final boolean success;
		
		public GenericResult(boolean success) {
			this.success = success;
		}
		
		public boolean isSuccess() {
			return success;
		}
		
	}
	
	public static class FileUploadTransaction implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private final String transaction;
		private String fileName;
		private String contentType;
		private File tempFile;
		private String description;
		private FileUploadTransaction thumbnail;
		
		public FileUploadTransaction(String transaction) {
			this.transaction = transaction;
		}
		
		public String getTransaction() {
			return transaction;
		}
		
		public String getFileName() {
			return fileName;
		}
		
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
		
		public String getContentType() {
			return contentType;
		}
		
		public void setContentType(String contentType) {
			this.contentType = contentType;
		}
		
		public File getTempFile() {
			return tempFile;
		}
		
		public void setTempFile(File tempFile) {
			this.tempFile = tempFile;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public FileUploadTransaction getThumbnail() {
			return thumbnail;
		}

		public void setThumbnail(FileUploadTransaction thumbnail) {
			this.thumbnail = thumbnail;
		}
		
	}
	
	public static class OceanUpload {
		
		private final List<FileUploadTransaction> transactions;
		private String selectedTransaction;
		private List<String> checkedTransactions;
		
		public OceanUpload(List<FileUploadTransaction> transactions) {
			this.transactions = transactions;
		}
		
		public List<FileUploadTransaction> getTransactions() {
			return transactions;
		}
		
		public String getSelectedTransaction() {
			return selectedTransaction;
		}
		
		public void setSelectedTransaction(String selectedTransaction) {
			this.selectedTransaction = selectedTransaction;
		}

		public List<String> getCheckedTransactions() {
			return checkedTransactions;
		}

		public void setCheckedTransactions(List<String> checkedTransactions) {
			this.checkedTransactions = new ArrayList<String>(checkedTransactions);
		}
		
	}
	
}
