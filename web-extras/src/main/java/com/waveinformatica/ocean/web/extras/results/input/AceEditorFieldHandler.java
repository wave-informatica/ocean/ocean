package com.waveinformatica.ocean.web.extras.results.input;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.util.Results;

public class AceEditorFieldHandler implements InputResult.FieldHandler
{
	
	private String language;
	
	public String getFieldTemplate()
	{
		return "/templates/webextras/inputs/ace-editor.tpl";
	}


	public void init(InputResult result, CoreController.OperationInfo opInfo, com.waveinformatica.ocean.core.controllers.CoreController.ParameterInfo paramInfo, InputResult.FormField field)
	{
		Results.addScriptSource(result, "ris/libs/ace-editor/ace.js");
		Results.addScriptSource(result, "ris/libs/ace-editor/ocean-ace.js");
	}


	public String formatValue(InputResult result, InputResult.FormField field, Object value)
	{
		return value != null ? value.toString() : null;
	}


	public String getLanguage() {
		return language;
	}


	public void setLanguage(String language) {
		this.language = language;
	}
	
}