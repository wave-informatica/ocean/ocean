/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.web.extras;

import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.controllers.CoreController.ParameterInfo;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.controllers.results.InputResult.FieldHandler;
import com.waveinformatica.ocean.core.controllers.results.InputResult.FormField;
import com.waveinformatica.ocean.core.util.Results;

public class ImageCroppedUpload implements FieldHandler {
	
	private int width = 300;
	private int height = 225;
	private int extraWidth = 60;
	private int extraHeight = 45;
	private double multiplier = 1;
	
	public static ImageCroppedUpload get(InputResult result, String name) {
		FormField field = result.getField(name);
		if (field == null) {
			return null;
		}
		FieldHandler handler = field.getHandler();
		if (handler != null && handler instanceof ImageCroppedUpload) {
			return (ImageCroppedUpload) handler;
		}
		return null;
	}
	
	@Override
	public String formatValue(InputResult arg0, FormField arg1, Object arg2) {
		if (arg2 instanceof String) {
			return arg2.toString();
		}
		return null;
	}

	@Override
	public String getFieldTemplate() {
		return "/templates/inputs/imageCroppedUpload.tpl";
	}

	@Override
	public void init(InputResult arg0, OperationInfo arg1, ParameterInfo arg2, FormField arg3) {
		
		// Tracking.js
		Results.addScriptSource(arg0, "ris/libs/smartcrop/tracking-min.js");
		Results.addScriptSource(arg0, "ris/libs/smartcrop/tracking-face-min.js");
		// Smartcrop.js
		Results.addScriptSource(arg0, "ris/libs/smartcrop/smartcrop.js");
		// Croppie
		Results.addCssSource(arg0, "ris/libs/croppie/croppie.css");
		Results.addScriptSource(arg0, "ris/libs/croppie/croppie.min.js");
		Results.addScriptSource(arg0, "ris/libs/croppie/ocean-croppie.js");
		
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getExtraWidth() {
		return extraWidth;
	}

	public void setExtraWidth(int extraWidth) {
		this.extraWidth = extraWidth;
	}

	public int getExtraHeight() {
		return extraHeight;
	}

	public void setExtraHeight(int extraHeight) {
		this.extraHeight = extraHeight;
	}

	public double getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(double multiplier) {
		this.multiplier = multiplier;
	}

}
