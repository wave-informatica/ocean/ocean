/*******************************************************************************
 * Copyright 2015, 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.ocean.web.extras;

import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.core.controllers.results.WebPageResult;
import com.waveinformatica.ocean.core.util.Results;
import javax.inject.Inject;

/**
 *
 * @author Ivano
 */
public class DataTablesWebResultBuilder implements TableResult.WrapperResultBuilder {
	
	@Inject
	private ObjectFactory factory;
	
	@Override
	public BaseResult getWrapperResult(TableResult tabRes) {
		
		WebPageResult wrapperResult = factory.newInstance(WebPageResult.class);
		
		wrapperResult.setData(tabRes);
		wrapperResult.setTemplate("/templates/core/tableResult.tpl");
		wrapperResult.setParentOperation(tabRes.getParentOperation());
		wrapperResult.getExtras().putAll(tabRes.getExtras());
		wrapperResult.setFullResult(tabRes.isFullResult());
		
		for (TableResult.TableColumn col : tabRes.getHeader().getColumns()) {
			if (col.getEditType() != TableResult.CellEditType.NONE) {
				wrapperResult.putExtra("table_inline_edit", true);
				Results.addCssSource(wrapperResult, "ris/libs/bootstrap3-editable-1.5.1/css/bootstrap-editable.css");
				Results.addScriptSource(wrapperResult, "ris/libs/bootstrap3-editable-1.5.1/js/bootstrap-editable.min.js");
				break;
			}
		}
		
		return wrapperResult;
		
	}
	
}
