/*
 * Copyright 2016, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.web.extras;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.util.Results;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ivano
 */
public class ChunkUploadFieldHandler implements InputResult.FieldHandler {
	
	private InitialConfiguration initialConfiguration;
	private boolean checkable = false;
	private List<String> checked = null;
	private boolean selectable = false;
	private String selected;
	private MimeTypes[] filter;
	
	@Override
	public String getFieldTemplate() {
		return "/templates/chunkUpload.tpl";
	}

	@Override
	public void init(InputResult result, CoreController.OperationInfo opInfo, CoreController.ParameterInfo paramInfo, InputResult.FormField field) {
		Results.addCssSource(result, "ris/css/chunkedUpload.css");
		Results.addScriptSource(result, "ris/libs/iCheck-1.x-20160316/icheck.min.js");
		Results.addScriptSource(result, "ris/js/videoSupport.js");
		Results.addScriptSource(result, "ris/js/chunkedUpload.js");
	}

	@Override
	public String formatValue(InputResult result, InputResult.FormField field, Object value) {
		return null;
	}

	public InitialConfiguration getInitialConfiguration() {
		return initialConfiguration;
	}

	public void setInitialConfiguration(InitialConfiguration initialConfiguration) {
		this.initialConfiguration = initialConfiguration;
	}

	public boolean isSelectable() {
		return selectable;
	}

	public void setSelectable(boolean selectable) {
		this.selectable = selectable;
	}

	public String getSelected() {
		return selected;
	}

	public void setSelected(String selected) {
		this.selected = selected;
	}

	public boolean isCheckable() {
		return checkable;
	}

	public void setCheckable(boolean checkable) {
		this.checkable = checkable;
		if (checkable) {
			checked = new ArrayList<String>();
		}
		else {
			checked = null;
		}
	}
	
	public void setChecked(List<String> values) {
		if (checked == null) {
			setCheckable(true);
		}
		checked.clear();
		checked.addAll(values);
	}
	
	public List<String> getChecked() {
		return checked;
	}

	public MimeTypes[] getFilter() {
		return filter;
	}

	public void setFilter(MimeTypes...filter) {
		this.filter = filter;
	}
	
	public static interface InitialConfiguration {
		
		public String getDeleteOperation(ChunkUploadOperations.FileUploadTransaction upload);
		
		public String getPreviewOperation(ChunkUploadOperations.FileUploadTransaction upload);
		
		public List<ChunkUploadOperations.FileUploadTransaction> getUploadedFiles();
		
	}
	
}
