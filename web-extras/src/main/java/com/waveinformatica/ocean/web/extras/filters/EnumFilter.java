/*******************************************************************************
 * Copyright 2015, 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.web.extras.filters;

import java.lang.reflect.Field;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.util.I18N;

public class EnumFilter extends MultiSelectFilter {
	
	@Inject
	private I18N i18n;
	
	@Override
	public void init(Object result, Field field) {
		super.init(result, field);
		if (field.getType().isEnum()) {
			for (Object v : field.getType().getEnumConstants()) {
				addOption(((Enum) v).name(), i18n.translate(field.getType().getName() + "." + ((Enum) v).name()));
			}
		}
	}
	
	@Override
	public String getUrl() {
		return null;
	}

	@Override
	public Object getValue(String value, Class<?> expectedType) {
		for (Object v : expectedType.getEnumConstants()) {
			if (((Enum) v).name().equals(value)) {
				return v;
			}
		}
		return null;
	}
	
}
