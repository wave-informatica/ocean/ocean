/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.ocean.web.extras;

import com.waveinformatica.ocean.core.annotations.TypeConverter;
import com.waveinformatica.ocean.core.converters.ITypeConverter;
import com.waveinformatica.ocean.core.util.OceanSession;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
@TypeConverter("com.waveinformatica.ocean.web.extras.ChunkUploadOperations$OceanUpload")
public class ChunkUploadTypeConverter implements ITypeConverter {
	
	@Inject
	private OceanSession session;
	
	@Override
	public Object fromParametersMap(String destName, Class type, Type type1, Map<String, String[]> maps, Object[] os) {
		
		String[] values = maps.get(destName + "__FileUpload_Transaction");
		String[] selections = maps.get(destName + "__FileUpload_Selection");
		String[] checked = maps.get(destName + "__FileUpload_Check");
		String[] descriptions = maps.get(destName + "__FileUpload_Description");
		String[] thumbnails = maps.get(destName + "__FileUpload_Thumb");
		
		if (values == null && selections == null) {
			return null;
		}
		List<ChunkUploadOperations.FileUploadTransaction> transactions = new ArrayList<ChunkUploadOperations.FileUploadTransaction>();
		
		ChunkUploadOperations.OceanUpload upload = new ChunkUploadOperations.OceanUpload(transactions);
		
		if (values != null) {
			
			for (String val : values) {
				transactions.add((ChunkUploadOperations.FileUploadTransaction) session.get(val));
				session.remove(val);
			}
			
		}
		
		if (selections != null && selections.length > 0) {
			upload.setSelectedTransaction(selections[0]);
		}
		if (checked != null && checked.length > 0) {
			upload.setCheckedTransactions(Arrays.asList(checked));
		}
		else {
			upload.setCheckedTransactions(new ArrayList<String>());
		}
		if (descriptions != null && descriptions.length > 0) {
			for (int i = 0; i < descriptions.length; i++) {
				upload.getTransactions().get(i).setDescription(descriptions[i]);
			}
		}
		if (thumbnails != null && thumbnails.length > 0) {
			for (int i = 0; i < thumbnails.length; i++) {
				if (StringUtils.isNotBlank(thumbnails[i])) {
					upload.getTransactions().get(i).setThumbnail((ChunkUploadOperations.FileUploadTransaction) session.get(thumbnails[i]));
				}
			}
		}
		
		return upload;
	}
	
	@Override
	public Object fromScalar(String string, Class destClass, Map<String, String[]> maps, Object[] os) {
		throw new UnsupportedOperationException();
	}
	
}
