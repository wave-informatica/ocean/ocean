/*
 * Copyright 2016, 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.web.extras.filters;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.filtering.Filter;
import com.waveinformatica.ocean.core.filtering.QueryFilter;
import com.waveinformatica.ocean.core.filtering.drivers.DataPath;
import com.waveinformatica.ocean.core.filtering.drivers.DataValue;
import com.waveinformatica.ocean.core.filtering.drivers.QueryBuilderDriver;
import com.waveinformatica.ocean.core.filtering.drivers.ValuesManager;
import com.waveinformatica.ocean.core.util.Results;

/**
 *
 * @author Ivano
 */
public class DateRangeFilter implements Filter {
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	@Override
	public void init(Object result, Field field) {
		if (result instanceof BaseResult) {
			// Moment JS
			Results.addScriptSource((BaseResult) result, "ris/libs/moment-2.12.0/moment.min.js");
			// Bootstrap Date Range Picker
			Results.addCssSource((BaseResult) result, "ris/libs/bootstrap-daterangepicker/daterangepicker.css");
			Results.addCssSource((BaseResult) result, "ris/libs/bootstrap-daterangepicker/daterangepickerfilter.css");
			Results.addScriptSource((BaseResult) result, "ris/libs/bootstrap-daterangepicker/daterangepicker.js");
		}
	}

	@Override
	public QueryFilter getJpqlFilter(String path, String filterValue, Class<?> expectedType) {
		
		if (StringUtils.isBlank(filterValue)) {
			return null;
		}
		
		QueryFilter qf = new QueryFilter();
		
		String[] parts = filterValue.split("-");
		
		try {
			
			String paramName = this.getClass().getSimpleName().toLowerCase() + "_" + path.replaceAll("[^a-zA-Z0-9]", "_");
			
			Date from = parse(parts[0], true);
			Date to = parse(parts[1], false);
			
			StringBuilder builder = new StringBuilder();
			builder.append('(');
			builder.append(path);
			builder.append(" >= :");
			builder.append(paramName);
			builder.append("0 and ");
			builder.append(path);
			builder.append(" <= :");
			builder.append(paramName);
			builder.append("1)");
			
			qf.setCondition(builder.toString());
			qf.set(paramName + "0", from);
			qf.set(paramName + "1", to);
			
		} catch (ParseException e) {
			return null;
		}
		
		return qf;
		
	}

	@Override
	public String getTemplate() {
		return "/templates/webextras/inputs/dateRangeFilter.tpl";
	}

	@Override
	public String getHelpTemplate() {
		return null;
	}
	
	private Date parse(String value, boolean begin) throws ParseException {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(sdf.parse(value));
		
		if (begin) {
			cal.set(Calendar.HOUR, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
		}
		else {
			cal.set(Calendar.HOUR, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			cal.set(Calendar.MILLISECOND, 999);
		}
		
		return cal.getTime();
		
	}

	@Override
	public <T> QueryFilter<T> getQueryFilter(String path, String filterValue, Class<?> expectedType, QueryBuilderDriver<T> driver,
			ValuesManager values) {
		
		if (StringUtils.isBlank(filterValue)) {
			return null;
		}
		
		QueryFilter<T> qf = new QueryFilter<T>(values);
		
		String[] parts = filterValue.split("-");
		
		try {
			
			Date from = parse(parts[0], true);
			Date to = parse(parts[1], false);
			
			qf.setWhereClause(driver.and(
					driver.gte(new DataPath(path), values.getParam(new DataValue(from))),
					driver.lte(new DataPath(path), values.getParam(new DataValue(to)))
			));
			
		} catch (ParseException e) {
			return null;
		}
		
		return qf;
		
	}
	
}
