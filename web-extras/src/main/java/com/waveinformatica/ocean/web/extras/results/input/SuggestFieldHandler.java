/*
* Copyright 2015, 2018 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.web.extras.results.input;

import com.waveinformatica.ocean.core.annotations.ResultDecorator;
import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.controllers.CoreController.ParameterInfo;
import com.waveinformatica.ocean.core.controllers.decorators.JsonDecorator;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.controllers.results.InputResult.FormField;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.results.input.ComboFieldHandler;
import com.waveinformatica.ocean.core.util.Results;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Marco Merli
 */
public abstract class SuggestFieldHandler extends ComboFieldHandler {
	
	private boolean create = false;
	private boolean preload = true;
	private boolean multiple = false;
	private final Map<String, String> attributes = new HashMap<String, String>();
	
	public static void loadResources(BaseResult result) {
		
		Results.addCssSource(result, "ris/libs/selectize/css/selectize.bootstrap3.css");
		Results.addScriptSource(result, "ris/libs/selectize/js/standalone/selectize.js");
		Results.addScriptSource(result, "ris/js/ocean-selectize.js");
		
	}
	
	@Override
	public String getFieldTemplate()
	{
		return "/templates/webextras/inputs/suggestField.tpl";
	}
	
	@Override
	public void init(InputResult result, OperationInfo opInfo, ParameterInfo paramInfo, FormField field)
	{
		getItems().clear();
		
		if (StringUtils.isNotBlank(field.getValue())) {
			changePreload(true);
		}
		
		loadResources(result);
		
		if (paramInfo != null && Arrays.asList(
			    List.class, Collection.class, ArrayList.class, LinkedList.class,
			    Set.class, HashSet.class, TreeSet.class)
			    .contains(paramInfo.getParamClass())) {
			multiple = true;
		}
		
	}
	
	public abstract String getUrl();
	
	public boolean getCreate()
	{
		return create;
	}
	
	public boolean getPreload()
	{
		return preload;
	}
	
	protected void changeCreate(boolean create)
	{
		this.create = create;
	}
	
	protected void changePreload(boolean preload)
	{
		this.preload = preload;
	}

	public boolean isMultiple() {
		return multiple;
	}

	public void setMultiple(boolean multiple) {
		this.multiple = multiple;
	}
	
	public Map<String, String> getAttributes() {
		return attributes;
	}

	public static class SuggestData {
		
		private final List<Item> data;
		private final Map<String, Object> extra = new HashMap<String, Object>();
		
		public SuggestData(List<Item> items) {
			this.data = items;
		}
		
		public SuggestData() {
			data = new ArrayList<Item>();
		}
		
		public List<Item> getData() {
			return data;
		}

		public Map<String, Object> getExtra() {
			return extra;
		}
		
	}
	
	public static class Item {
		
		private String value;
		private String text;
		private Map<String, Object> extra = new HashMap<String, Object>();
		
		public Item(String value, String text) {
			this.value = value;
			this.text = text;
		}
		
		public String getValue() {
			return value;
		}
		
		public void setValue(String value) {
			this.value = value;
		}
		
		public String getText() {
			return text;
		}
		
		public void setText(String text) {
			this.text = text;
		}

		public Map<String, Object> getExtra() {
			return extra;
		}

		public void setExtra(Map<String, Object> extra) {
			this.extra = extra;
		}
		
	}
	
	@ResultDecorator(
		  classes = SuggestData.class,
		  mimeTypes = MimeTypes.JSON
	)
	public static class SuggestDecorator extends JsonDecorator {
		
	}
	
}
