/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.web.extras.images;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.spi.IIORegistry;
import javax.imageio.spi.ImageWriterSpi;
import javax.imageio.spi.ServiceRegistry;
import javax.imageio.stream.MemoryCacheImageOutputStream;

import com.waveinformatica.ocean.core.controllers.results.MimeTypes;

public class ImageWorker {
	
	private final MimeTypes type;
	private BufferedImage image;
	private boolean alpha = false;
	
	public ImageWorker(MimeTypes type) {
		super();
		this.type = type;
	}

	public MimeTypes getType() {
		return type;
	}

	public boolean isAlpha() {
		return alpha;
	}

	public void setAlpha(boolean alpha) {
		this.alpha = alpha;
	}

	/**
	 * Loads an image from the provided {@link InputStream} and <strong>closes</strong> the input stream,
	 * unless an {@link IOException} occurs.
	 * 
	 * @param input
	 * @throws IOException
	 */
	public void load(InputStream input) throws IOException {
		
		image = ImageIO.read(input);
		
		input.close();
		
	}
	
	public void setImage(BufferedImage image) {
		this.image = image;
	}

	public BufferedImage getImage() {
		return image;
	}
	
	public byte[] buildBytes() throws IOException {
		
		ImageWriteParam imageParams = null;
		
		// Saving file
		if (type == MimeTypes.JPEG) {
			JPEGImageWriteParam jpegParams = new JPEGImageWriteParam(null);
			jpegParams.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			jpegParams.setCompressionQuality(0.8f);
			imageParams = jpegParams;
		}
		else if (type == MimeTypes.PNG) {
			
		}
		else if (type == MimeTypes.GIF) {
			
		}
		
		// use IIORegistry to get the available services
		IIORegistry registry = IIORegistry.getDefaultInstance();
		// return an iterator for the available ImageWriterSpi for jpeg images
		Iterator<ImageWriterSpi> services = registry.getServiceProviders(ImageWriterSpi.class,
			  new ServiceRegistry.Filter() {
				  @Override
				  public boolean filter(Object provider) {
					  if (!(provider instanceof ImageWriterSpi)) {
						  return false;
					  }
					  
					  ImageWriterSpi writerSPI = (ImageWriterSpi) provider;
					  String[] formatNames = writerSPI.getFormatNames();
					  for (String formatName : formatNames) {
						  if (formatName.equalsIgnoreCase(type.name())) {
							  return true;
						  }
					  }
					  
					  return false;
				  }
			  },
			  true);
		
		//...assuming that servies.hasNext() == true, I get the first available service.
		ImageWriterSpi writerSpi = services.next();
		ImageWriter writer = writerSpi.createWriterInstance();
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		writer.setOutput(new MemoryCacheImageOutputStream(baos));
		
		writer.write(null, new IIOImage(image, null, null), imageParams);
		
		return baos.toByteArray();
		
	}
	
	public void cut(int x, int y, int width, int height) {
		image = image.getSubimage(x, y, width, height);
	}
	
	public void resize(ResizeMode mode, int width, int height) {
		switch (mode) {
		case CROP:
			cropImage(width, height);
			break;
		case KEEP_ASPECT_RATIO:
			resize(width, height, false);
			break;
		case STRETCH:
			resize(width, height, true);
			break;
		}
	}
	
	private void cropImage(Integer w, Integer h) {
		
		Integer newW, newH, cropW, cropH;
		BufferedImage bdest;
		
		if ((w != null && image.getWidth() > w) || (h != null && image.getHeight() > h)) {
			
			if (w != null && h != null) {
				if (((double) image.getWidth()) / ((double) image.getHeight()) < ((double) w) / ((double) h)) {
					newW = w;
					newH = (int) Math.round(((double) newW) / ((double) image.getWidth()) * image.getHeight());
					cropW = image.getWidth();
					cropH = (int) Math.round((Double) (h.doubleValue() * ((double)image.getWidth()) /  w.doubleValue()));
				}
				else if (((double) image.getWidth()) / ((double) image.getHeight()) > ((double) w) / ((double) h)) {
					newH = h;
					newW = (int) Math.round(((double) newH) / ((double) image.getHeight()) * (double) image.getWidth());
					cropH = image.getHeight();
					cropW = (int) Math.round((Double) (w.doubleValue() * ((double)image.getHeight()) /  h.doubleValue()));
				}
				else {
					newW = w;
					newH = h;
					cropW = image.getWidth();
					cropH = image.getHeight();
				}
			}
			else if (w == null && h != null) {
				newH = h;
				newW = (int) Math.round(((double) newH) / ((double) image.getHeight()) * (double) image.getWidth());
				w = newW;
				cropH = image.getHeight();
				cropW = (int) Math.round((Double) (w.doubleValue() * ((double)image.getHeight()) /  h.doubleValue()));
			}
			else if (w != null && h == null) {
				newW = w;
				newH = (int) Math.round(((double) newW) / ((double) image.getWidth()) * (double) image.getHeight());
				h = newH;
				cropW = image.getWidth();
				cropH = (int) Math.round((Double) (h.doubleValue() * ((double)image.getWidth()) /  w.doubleValue()));
			}
			else {
				w = image.getWidth();
				h = image.getHeight();
				newW = w;
				newH = h;
				cropW = image.getWidth();
				cropH = image.getHeight();
			}
			
			bdest = new BufferedImage(w, h, alpha ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB);
			
			Graphics2D g = bdest.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			AffineTransform at = AffineTransform.getScaleInstance((double) newW / image.getWidth(), (double) newH / image.getHeight());
			g.drawRenderedImage(image.getSubimage((image.getWidth() - cropW) / 2, (image.getHeight() - cropH) / 2, cropW, cropH), at);
			
			image = bdest;
			
		}
		
	}
	
	private void resize(Integer w, Integer h, boolean stretch) {
		
		Integer newW, newH;
		BufferedImage bdest;
		
		if (stretch) {
			newW = w;
			newH = h;
		}
		else {
			double newRatio = w.doubleValue() / h.doubleValue();
			double imgRatio = ((double) image.getWidth()) / ((double) image.getHeight());
			if (newRatio > imgRatio) {
				newH = h;
				newW = (int) Math.ceil(imgRatio * h);
			}
			else {
				newW = w;
				newH = (int) Math.ceil(newW / imgRatio);
			}
		}
		
		bdest = new BufferedImage(w, h, alpha ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB);
		
		Graphics2D g = bdest.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		AffineTransform at = AffineTransform.getScaleInstance((double) newW / image.getWidth(), (double) newH / image.getHeight());
		g.drawRenderedImage(image, at);
		
		image = bdest;
		
	}
	
	public void removeBorders(Rectangle rectangle) {
		
		if (image == null) {
			throw new NullPointerException("No image loaded");
		}
		
		if (rectangle == null) {
			rectangle = new Rectangle();
		}
		
		// Get our top-left pixel color as our "baseline" for cropping
		int baseColor = image.getRGB(0, 0);
		
		int width = image.getWidth();
		int height = image.getHeight();
		
		int topY = Integer.MAX_VALUE, topX = Integer.MAX_VALUE;
		int bottomY = -1, bottomX = -1;
		if (rectangle.getX() != null) {
			topX = rectangle.getX().intValue();
			topY = rectangle.getY().intValue();
			bottomX = rectangle.getX().intValue() + rectangle.getWidth().intValue() - 1;
			bottomY = rectangle.getY().intValue() + rectangle.getHeight().intValue() - 1;
		}
		else {
			for(int y=0; y<height; y++) {
				for(int x=0; x<width; x++) {
					if (colorWithinTolerance(baseColor, image.getRGB(x, y), 0)) {
						if (x < topX) topX = x;
						if (y < topY) topY = y;
						if (x > bottomX) bottomX = x;
						if (y > bottomY) bottomY = y;
					}
				}
			}
		}
		
		int newWidth = (bottomX-topX+1);
		int newHeight = (bottomY-topY+1);
		
		if (newWidth > 0 && newHeight > 0) {
			
			BufferedImage destination = new BufferedImage( newWidth,
				  newHeight, alpha ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB);
			
			rectangle.setX((long) topX);
			rectangle.setY((long) topY);
			rectangle.setWidth((long) newWidth);
			rectangle.setHeight((long) newHeight);
			
			destination.getGraphics().drawImage(image, 0, 0,
				  destination.getWidth(), destination.getHeight(),
				  topX, topY, bottomX, bottomY, null);

			image = destination;
			
		}
	}
	
	private boolean colorWithinTolerance(int a, int b, double tolerance) {
		int aAlpha  = (int)((a & 0xFF000000) >>> 24);   // Alpha level
		int aRed    = (int)((a & 0x00FF0000) >>> 16);   // Red level
		int aGreen  = (int)((a & 0x0000FF00) >>> 8);    // Green level
		int aBlue   = (int)(a & 0x000000FF);            // Blue level
		
		int bAlpha  = (int)((b & 0xFF000000) >>> 24);   // Alpha level
		int bRed    = (int)((b & 0x00FF0000) >>> 16);   // Red level
		int bGreen  = (int)((b & 0x0000FF00) >>> 8);    // Green level
		int bBlue   = (int)(b & 0x000000FF);            // Blue level
		
		double distance = Math.sqrt((aAlpha-bAlpha)*(aAlpha-bAlpha) +
			  (aRed-bRed)*(aRed-bRed) +
			  (aGreen-bGreen)*(aGreen-bGreen) +
			  (aBlue-bBlue)*(aBlue-bBlue));
		
		// 510.0 is the maximum distance between two colors
		// (0,0,0,0 -> 255,255,255,255)
		double percentAway = distance / 510.0d;
		
		return (percentAway > tolerance);
	}
	
}
