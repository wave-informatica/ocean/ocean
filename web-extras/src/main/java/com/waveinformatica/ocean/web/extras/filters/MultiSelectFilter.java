/*
 * Copyright 2016, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.web.extras.filters;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.input.ComboFieldHandler;
import com.waveinformatica.ocean.core.filtering.Filter;
import com.waveinformatica.ocean.core.filtering.QueryFilter;
import com.waveinformatica.ocean.core.filtering.drivers.DataPath;
import com.waveinformatica.ocean.core.filtering.drivers.DataValue;
import com.waveinformatica.ocean.core.filtering.drivers.QueryBuilderDriver;
import com.waveinformatica.ocean.core.filtering.drivers.ValuesManager;
import com.waveinformatica.ocean.core.util.Results;

/**
 *
 * @author Ivano
 */
public abstract class MultiSelectFilter implements Filter {
	
	private final List<ComboFieldHandler.Item> items = new ArrayList<ComboFieldHandler.Item>();
	private boolean create = false;
	private boolean preload = true;
	private final Map<String, String> attributes = new HashMap<String, String>();

	@Override
	public void init(Object result, Field field) {
		if (result instanceof BaseResult) {
			Results.addCssSource((BaseResult) result, "ris/libs/selectize/css/selectize.bootstrap3.css");
			Results.addScriptSource((BaseResult) result, "ris/libs/selectize/js/standalone/selectize.min.js");
		}
	}
	
	@Override
	public QueryFilter getJpqlFilter(String path, String filterValue, Class<?> expectedType) {
		
		if (StringUtils.isBlank(filterValue)) {
			return null;
		}
		
		QueryFilter qf = new QueryFilter();
		
		String[] parts = filterValue.trim().split(",+");
		StringBuilder builder = new StringBuilder();
		int paramIndex = 0;
		String paramName = this.getClass().getSimpleName().toLowerCase() + "_" + path.replaceAll("[^a-zA-Z0-9]", "_");
		for (String p : parts) {
			if (builder.length() > 0) {
				builder.append(", ");
			}
			builder.append(':');
			builder.append(paramName);
			builder.append(paramIndex);
			qf.set(paramName + paramIndex, getValue(p, expectedType));
			paramIndex++;
		}
		
		if (builder.length() > 0) {
			qf.setCondition(path + " in (" + builder.toString() + ")");
			return qf;
		}
		else {
			return null;
		}
	}
	
	@Override
	public <T> QueryFilter<T> getQueryFilter(String path, String filterValue, Class<?> expectedType, QueryBuilderDriver<T> driver,
			ValuesManager values) {
		
		if (StringUtils.isBlank(filterValue)) {
			return null;
		}
		
		List<Object> paramValues = new ArrayList<Object>();
		String[] parts = filterValue.trim().split(",+");
		for (String p : parts) {
			paramValues.add(getValue(p, expectedType));
		}
		if (paramValues.isEmpty()) {
			return null;
		}
		else {
			QueryFilter<T> qf = new QueryFilter<T>(values);
			qf.setWhereClause(driver.in(new DataPath(path), values.getParam(new DataValue(paramValues))));
			return qf;
		}
		
	}

	@Override
	public String getTemplate() {
		return "/templates/webextras/inputs/suggestFieldFilter.tpl";
	}

	@Override
	public String getHelpTemplate() {
		return null;
	}

	public boolean isCreate() {
		return create;
	}

	public void setCreate(boolean create) {
		this.create = create;
	}

	public boolean isPreload() {
		return preload;
	}

	public void setPreload(boolean preload) {
		this.preload = preload;
	}

	public List<ComboFieldHandler.Item> getItems() {
		return items;
	}
	
	protected void addOption(String value, String label) {
		items.add(new ComboFieldHandler.Item(value, label));
	}
	
	public abstract String getUrl();
	
	public abstract Object getValue(String value, Class<?> expectedType);

	public Map<String, String> getAttributes() {
		return attributes;
	}
	
}
