uiModule('selectize', function () {
	
	$(document).on('reset', 'form', function (evt) {
		
		var me = this;
		
		setTimeout(function () {
			$(me).find('.selectized').each(function () {
				this.selectize.clear();
			});
		}, 0);
		
	});
	
	function init() {
		
		var options = {
			dropdownParent: 'body',
			create: false,
			preload: false,
			first: true
		};
		
		$.extend(options, $(this).data());
		
		if (options.url) {
			var me = this;
			options.load = function (query, callback) {
				if (options.preload && (query || query.length > 0)) {
					return callback();
				}
				
				$.ajax({
					url: options.url,
					params: {
						query: query
					},
					type: 'GET',
					dataType: 'json',
					error: function () {
						callback();
					},
					success: function (res) {
						var prevItems = $.map($(me).find('option'), function () {
							if ($(this).attr('value')) {
								return $(this).attr('value');
							}
						});
						if (options.first) {
							options.first = false;
							if (prevItems.length === 0) {
								prevItems = prevItems.concat(('' + options.value).split(','));
							}
						}
						callback(res.data);
						for (var i = 0; i < prevItems.length; i++) {
							me.selectize.addItem(prevItems[i]);
						}
					}
				});
			};
		}
		
		$(this).selectize(options);
		
	}
	
	return {
		init: init
	};
	
});