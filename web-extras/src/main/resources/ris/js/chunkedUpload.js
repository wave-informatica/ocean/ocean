uiModule(['videoSupport'], 'chunkUpload', function (video) {
	
	function ChunkedUploader(file, options) {
	    if (!this instanceof ChunkedUploader) {
	        return new ChunkedUploader(file, options);
	    }

	    this.file = file;

	    this.options = $.extend({
	        url: '__ocean_chunk_upload/chunk-upload',
	        fileObj: null,
	        onUploadComplete: function () {
	        },
	        scope: this
	    }, options);

	    this.file_size = this.file.size;
	    this.chunk_size = (1024 * 1024); // 1MB
	    this.range_start = 0;
	    this.range_end = this.chunk_size;
	    this.state = 'prepared';

	    if ('mozSlice' in this.file) {
	        this.slice_method = 'mozSlice';
	    } else if ('webkitSlice' in this.file) {
	        this.slice_method = 'webkitSlice';
	    } else {
	        this.slice_method = 'slice';
	    }
	    var uplReq = new XMLHttpRequest();
	    this.upload_request = uplReq;
	    this.upload_request.scope = this;
	    this.upload_request.onload = function () {
	        uplReq.scope._onChunkComplete();
	    };
	}

	ChunkedUploader.prototype = {
	    // Internal Methods __________________________________________________

	    _guid: function () {
	        function s4() {
	            return Math.floor((1 + Math.random()) * 0x10000)
	                    .toString(16)
	                    .substring(1);
	        }
	        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
	                s4() + '-' + s4() + s4() + s4();
	    },
	    _upload: function () {
	        var self = this,
	                chunk;

	        if (self.state !== 'uploading') {
	            return;
	        }

	        // Slight timeout needed here (File read / AJAX readystate conflict?)
	        setTimeout(function () {
	            // Prevent range overflow
	            if (self.range_end > self.file_size) {
	                self.range_end = self.file_size;
	            }

	            chunk = self.file[self.slice_method](self.range_start, self.range_end);

	            self.upload_request.open('PUT', self.options.url, true);
	            //self.upload_request.overrideMimeType('application/octet-stream');
	            self.upload_request.setRequestHeader('Accept', 'application/json');
	            self.upload_request.setRequestHeader('Content-Range', 'bytes ' + self.range_start + '-' + self.range_end + '/' + self.file_size);

	            if (self.range_start === 0) {
	                self.uploadIdentifier = self._guid();
	                if (self.options.fileObj && self.options.fileObj.data('selector')) {
	                    self.options.fileObj.data('selector').find('input[type="radio"]').attr('value', self.uploadIdentifier);
	                }
	                if (self.options.fileObj && self.options.fileObj.data('checker')) {
	                    self.options.fileObj.data('checker').find('input[type="checkbox"]').attr('value', self.uploadIdentifier);
	                }
	                self.upload_request.setRequestHeader('Upload-Filename', self.file.name);
	                self.upload_request.setRequestHeader('Upload-Content-Type', self.file.type);
	            }

	            self.upload_request.setRequestHeader('Upload-Identifier', self.uploadIdentifier);

	            self.upload_request.send(chunk);

	            // TODO
	            // From the looks of things, jQuery expects a string or a map
	            // to be assigned to the "data" option. We'll have to use
	            // XMLHttpRequest object directly for now...
	            /*$.ajax(self.options.url, {
	             data: chunk,
	             type: 'PUT',
	             mimeType: 'application/octet-stream',
	             headers: (self.range_start !== 0) ? {
	             'Content-Range': ('bytes ' + self.range_start + '-' + self.range_end + '/' + self.file_size)
	             } : {},
	             success: self._onChunkComplete
	             });*/
	        }, 20);
	    },
	    // Event Handlers ____________________________________________________

	    _onChunkComplete: function () {

	        if (this.options.onProgress) {
	            this.options.onProgress.call(this.options.scope != null ? this.options.scope : this, this.range_end);
	        }

	        // If the end range is already the same size as our file, we
	        // can assume that our last chunk has been processed and exit
	        // out of the function.
	        if (this.range_end === this.file_size) {
	            if (this.options.fileObj) {
	                if (this.options.isGallery) {
	                    this.options.fileObj.find('.c100').attr('class', 'c100 p100');
	                    this.options.fileObj.find('.c100 span').html('100%');
	                    this.options.fileObj.find('.load-progress').fadeOut(400, function () {
	                        $(this).remove();
	                    });
	                }
	                else {
	                    this.options.fileObj.find('.file-progress').css('width', '100%');
	                }
	                this.options.fileObj.append($('<input type="hidden" name="' + this.options.name + '__FileUpload_Transaction" value="' + this.uploadIdentifier + '">'));
	                this.options.fileObj.append($('<input type="hidden" name="' + this.options.name + '__FileUpload_Thumb" value="' + (this.options.thumbnail ? this.options.thumbnail.uploadIdentifier : '') + '">'));
	            }
	            this.state = 'complete';
	            if (this.options.onUploadComplete) {
	                this.options.onUploadComplete.call(this.options.scope != null ? this.options.scope : this, this.uploadIdentifier, this.options.thumbnail ? this.options.thumbnail.uploadIdentifier : null);
	            }
	            return;
	        }

	        // Update our ranges
	        this.range_start = this.range_end;
	        this.range_end = this.range_start + this.chunk_size;

	        if (this.options.fileObj) {
	            var value = Math.floor(this.range_start / this.file_size * 100);
	            if (this.options.isGallery) {
	                this.options.fileObj.find('.c100').attr('class', 'c100 p' + value);
	                this.options.fileObj.find('.c100 span').html(value + '%');
	            }
	            else {
	                this.options.fileObj.find('.file-progress').css('width', value + '%');
	            }
	        }

	        // Continue as long as we aren't paused
	        if (!this.is_paused) {
	            this._upload();
	        }
	    },
	    // Public Methods ____________________________________________________

	    start: function () {
	        this.state = 'uploading';
	        this._upload();
	    },
	    pause: function () {
	        this.state = 'paused';
	        this.is_paused = true;
	    },
	    resume: function () {
	        this.is_paused = false;
	        this.state = 'uploading';
	        this._upload();
	    },
	    cancel: function () {
	        this.state = 'cancelled';
	    }
	};
	
	return {
		
		ChunkedUploader: ChunkedUploader,
		
		init: function () {

		    var container = $(this);

		    var selectable = container.data('selectable');
		    var checkable = container.data('checkable');
		    var filesList = container.find('.files-container');
		    var chooseButton = container.find('button.file-chooser');
		    var fileChooser = container.find('input[type="file"]');
		    var uploaders = [];
		    var filesToBeLoaded = 0;
		    var askBeforeClose = false;
		    var enableTrash = true;

		    // gallery specific elements
		    var isGallery = filesList.hasClass('gallery');

		    container.closest('form').submit(function () {
		        fileChooser.remove();
		    });

		    container.find('input[type="radio"],input[type="checkbox"]').iCheck({
		        cursor: true
		    });
		    container.find('.file-element').each(function () {
		        $(this).data('selector', container.find('input[type="radio"][value="' + $(this).data('transaction') + '"]').closest('.iradio'));
		        $(this).data('checker', container.find('input[type="checkbox"][value="' + $(this).data('transaction') + '"]').closest('.icheckbox'));
		    });

		    filesList.find('a.delete-file').click(function (evt) {
		        evt.preventDefault();
		        $.post($(this).attr('href'), {});
		        if (isGallery) {
		            var element = $(this).closest('.element-container');
		            element.fadeOut(400, function () {
		                element.remove();
		            });
		        }
		        else {
		            $(this).closest('.file-element').data('selector').slideUp(400);
		            $(this).closest('.file-element').data('checker').slideUp(400);
		            $(this).closest('.file-element').slideUp(400, function () {
		                $(this).closest('.file-element').data('selector').remove();
		                $(this).closest('.file-element').data('checker').remove();
		                $(this).closest('.file-element').remove();
		            });
		        }
		    });

		    $('.file-element').find('.fa-eye[data-operation]').each(function () {
		        $(this).popover({
		            container: 'body',
		            html: true,
		            title: "<strong>Preview</strong>",
		            content: $('<img/>', {
		                id: 'dynamic',
		                width: 250,
		                src: $(this).data('operation')
		            })[0].outerHTML,
		            placement: 'right',
		            trigger: 'hover'
		        });
		    });

		    if (isGallery) {
		        filesList.find('.img img').click(function () {
		            $(this).closest('.element-container').prev().find('input').iCheck('toggle');
		        });
		        filesList.find('.btn-description').click(editDescription);
		        
		        var modal = container.next('.modal').first();
		        modal.find('.btn-primary').click(function () {
		            var value = $(this).closest('.modal').find('textarea').val().trim();
		            var target = $(this).closest('.modal').data('input');
		            target.val(value);
		            $(this).closest('.modal').modal('hide');
		        });
		        modal.on('hidden.bs.modal', function () {
		            $(this).find('textarea').val('');
		            $(this).data('input', null);
		        });
		    }

		    function onFilesSelected(evt) {
		        var files = evt.target.files;
		        filesToBeLoaded = files.length;

		        if (files.length > 0) {
		            var submits = container.closest('form').find('[type="submit"]');
		            submits.addClass('disabled');
		            submits.attr('disabled', 'disabled');
		            chooseButton.addClass('disabled');
		            chooseButton.attr('disabled', 'disabled');
		        }

		        for (var i = 0; i < files.length; i++) {
		            var file = files[i];
		            if (isGallery) {
		                appendFileUploaderGallery(file);
		            }
		            else {
		                appendFileUploader(file);
		            }
		        }

		        $(evt.target).val('');
		    }
		    
		    function appendFileUploaderGallery(file) {
		    	if (video.isVideo(file.type)) {
		    		video.getThumbnail(file, function (blob) {
		    			appendImageUploaderGallery(file, URL.createObjectURL(blob), blob);
		    		});
		    	}
		    	else {
		    		appendImageUploaderGallery(file);
		    	}
		    }
		    
		    function appendImageUploaderGallery(file, tmpPath, thumbnail) {

		        askBeforeClose = true;
		        
		        if (!tmpPath) {
		        	tmpPath = URL.createObjectURL(file);
		        }
		        
		        var tpl = container.find('script[type="text/template"]').html();
		        Mustache.parse(tpl);
		        var fileTpl = Mustache.render(tpl, {
		            fieldName: fileChooser.attr('name'),
		            fileName: file.name,
		            src: tmpPath
		        });
		        
		        fileTpl = $(fileTpl);
		        
		        filesList.append(fileTpl);
		        
		        fileTpl = fileTpl.last();
		        
		        var thumbnailUploader = null;;
		        
		        if (thumbnail) {
		        	thumbnailUploader = new ChunkedUploader(thumbnail, {
		        		onUploadComplete: startNextUpload
		        	});
		        	uploaders.push(thumbnailUploader);
		        }

		        var uploader = new ChunkedUploader(file, {
		            onUploadComplete: startNextUpload,
		            fileObj: fileTpl,
		            name: fileChooser.attr('name'),
		            isGallery: isGallery,
		            thumbnail: thumbnailUploader
		        });

		        uploaders.push(uploader);

		        fileTpl.data('uploader', uploader);
		        
		        if (fileTpl.iCheck) {
		            fileTpl.find('input[type="checkbox"],input[type="radio"]').iCheck();
		            if (fileTpl.prev().is('input[type="radio"]')) {
		                fileTpl.prev().iCheck();
		            }
		            fileTpl.find('.img img').click(function () {
		                $(this).closest('.element-container').prev().find('input').iCheck('toggle');
		            });
		        }
		        
		        fileTpl.data('checker', fileTpl.find('label i.fa-eye').prev());
		        fileTpl.data('selector', fileTpl.prev());

		        if (enableTrash) {
		            var trashBtn = fileTpl.find('.delete-file');
		            trashBtn.click(function (evt) {
		                evt.preventDefault();
		                var me = $(this);
		                var uploader = fileTpl.data('uploader');
		                if (uploader) {
		                    var deleteServer = false;
		                    var state = uploader.state;
		                    uploader.cancel();
		                    if (state === 'uploading' || state === 'complete') {
		                        deleteServer = true;
		                    }
		                    for (var i = 0; i < uploaders.length; i++) {
		                        if (uploaders[i] == uploader) {
		                            uploaders = uploaders.slice(0, i).concat(uploaders.slice(i + 1));
		                            break;
		                        }
		                    }
		                    if (uploaders.length === 0 || state === 'uploading') {
		                        startNextUpload();
		                    }
		                    if (deleteServer) {
		                        $.post('__ocean_chunk_upload/delete-upload', {
		                            identifier: uploader.uploadIdentifier
		                        });
		                    }
		                }
		                me.closest('.element-container').fadeOut(400, function () {
		                    $(this).remove();
		                });
		            });
		        }
		        
		        fileTpl.find('.btn-description').click(editDescription);

		        filesToBeLoaded--;
		        if (filesToBeLoaded === 0) {
		            startNextUpload();
		        }

		        return fileTpl;
		        
		    }

		    function appendFileUploader(file) {

		        askBeforeClose = true;

		        var tmpPath = URL.createObjectURL(file);

		        var fileTpl = $('<div class="file-element"><div class="file-progress" style="width:0px;"><i class="fa fa-eye"></i> ' + file.name + '</div><div class="file-name"><i class="fa fa-eye"></i> ' + file.name + '</div></div>');
		        if (selectable) {
		            var selector = $('<input type="radio" name="' + fileChooser.attr('name') + '__FileUpload_Selection" value="">');
		            filesList.find('.vertical-selector.selector').append(selector);
		            selector.iCheck({
		                cursor: true
		            });
		            fileTpl.data('selector', selector.closest('.iradio'));
		        }
		        if (checkable) {
		            var checker = $('<input type="checkbox" name="' + fileChooser.attr('name') + '__FileUpload_Check" value="">');
		            filesList.find('.vertical-selector.checker').append(checker);
		            checker.iCheck({
		                cursor: true
		            });
		            fileTpl.data('checker', checker.closest('.icheckbox'));
		        }

		        if (file.name.toLowerCase().indexOf('.jpg') !== -1 ||
		                file.name.toLowerCase().indexOf('.gif') !== -1 ||
		                file.name.toLowerCase().indexOf('.png') !== -1) {
		            var eyePreview = fileTpl.find('.fa-eye');
		            var img = $('<img/>', {
		                id: 'dynamic',
		                width: 250,
		                src: tmpPath
		            });
		            eyePreview.popover({
		                container: 'body',
		                html: true,
		                title: "<strong>Preview</strong>",
		                content: $(img)[0].outerHTML,
		                placement: 'right',
		                trigger: 'hover'
		            });
		        }

		        if (enableTrash) {
		            var trashBtn = $('<a href="#" class="delete-file"><i class="fa fa-trash"></i></a>');
		            fileTpl.find('.file-name').append(trashBtn);
		            trashBtn.click(function (evt) {
		                evt.preventDefault();
		                var me = $(this);
		                var uploader = me.closest('.file-element').data('uploader');
		                if (uploader) {
		                    var deleteServer = false;
		                    var state = uploader.state;
		                    uploader.cancel();
		                    if (state === 'uploading' || state === 'complete') {
		                        deleteServer = true;
		                    }
		                    for (var i = 0; i < uploaders.length; i++) {
		                        if (uploaders[i] == uploader) {
		                            uploaders = uploaders.slice(0, i).concat(uploaders.slice(i + 1));
		                            break;
		                        }
		                    }
		                    if (uploaders.length === 0 || state === 'uploading') {
		                        startNextUpload();
		                    }
		                    if (deleteServer) {
		                        $.post('__ocean_chunk_upload/delete-upload', {
		                            identifier: uploader.uploadIdentifier
		                        });
		                    }
		                }
		                if (me.closest('.file-element').data('selector')) {
		                    me.closest('.file-element').data('selector').slideUp(400);
		                }
		                if (me.closest('.file-element').data('checker')) {
		                    me.closest('.file-element').data('checker').slideUp(400);
		                }
		                me.closest('.file-element').slideUp(400, function () {
		                    if ($(this).data('selector')) {
		                        $(this).data('selector').remove();
		                    }
		                    if ($(this).data('checker')) {
		                        $(this).data('checker').remove();
		                    }
		                    $(this).remove();
		                });
		            });
		        }

		        var uploader = new ChunkedUploader(file, {
		            onUploadComplete: startNextUpload,
		            fileObj: fileTpl,
		            name: fileChooser.attr('name'),
		            isGallery: isGallery
		        });

		        uploaders.push(uploader);

		        fileTpl.data('uploader', uploader);
		        filesList.append(fileTpl);

		        filesToBeLoaded--;
		        if (filesToBeLoaded === 0) {
		            startNextUpload();
		        }

		        return fileTpl;
		    }

		    function startNextUpload() {
		        if (uploaders.length > 0) {
		            uploaders[0].start();
		            uploaders = uploaders.slice(1);
		        } else {
		            var submits = container.closest('form').find('[type="submit"]');
		            submits.removeClass('disabled');
		            submits.attr('disabled', null);
		            chooseButton.removeClass('disabled');
		            chooseButton.attr('disabled', null);
		        }
		    }
		    
		    function editDescription() {
		        var modal = $(this).closest('.chunk-upload').next('.modal');
		        modal.find('textarea').val($(this).prev().val());
		        modal.data('input', $(this).prev());
		        modal.modal('show');
		    }

		    window.onbeforeunload = function () {
		        if (askBeforeClose) {
		            return "Chiudendo la finestra si perderanno le modifiche non salvate. Sicuri di voler abbandonare la pagina?";
		        }
		    };

		    chooseButton.click(function (evt) {
		        evt.preventDefault();
		        evt.stopPropagation();
		        fileChooser.click();
		    });

		    fileChooser.change(onFilesSelected);

		    $('[type="submit"]').click(function (evt) {
		        askBeforeClose = false;
		    });

		    // Confiuring drag&drop files upload
		    var draggingContainer = null;

		    $(document).on('dragenter', function (evt) {
		        $('.chunk-upload').addClass('files-dragover');
		        draggingContainer = evt.target;
		        return false;
		    });
		    $(document).on('dragleave', function (evt) {
		        if (draggingContainer !== evt.target) {
		            return false;
		        }
		        draggingContainer = null;
		        $('.chunk-upload').removeClass('files-dragover');
		        return false;
		    });
		    $('.chunk-upload').on('dragover', function (evt) {
		        evt.preventDefault();
		        evt.stopPropagation();
		        $(this).addClass('drag-over');
		    });
		    $('.chunk-upload').on('dragleave', function (evt) {
		        evt.preventDefault();
		        evt.stopPropagation();
		        $(this).removeClass('drag-over');
		    });
		    container.on('drop', function (e) {
		        e.preventDefault();
		        $('.chunk-upload').removeClass('files-dragover');
		        $('.chunk-upload').removeClass('drag-over');

		        var files;
		        if (e.dataTransfer) {
		            files = e.dataTransfer.files;
		        } else {
		            files = e.originalEvent.dataTransfer.files;
		        }

		        filesToBeLoaded = files.length;

		        if (files.length > 0) {
		            var submits = container.closest('form').find('[type="submit"]');
		            submits.addClass('disabled');
		            submits.attr('disabled', 'disabled');
		            chooseButton.addClass('disabled');
		            chooseButton.attr('disabled', 'disabled');
		        }

		        for (var i = 0; i < files.length; i++) {
		            var file = files[i];
		            if (isGallery) {
		                appendFileUploaderGallery(file);
		            }
		            else {
		                appendFileUploader(file);
		            }
		        }
		    });

		    $(document).bind('drop dragover', function (e) {
		        e.preventDefault();
		    });
		}
		
	};
	
});