uiModule('videoSupport', function () {
	
	function getThumbnail(url, callback) {
		
		if (typeof url !== 'string') {
			url = URL.createObjectURL(url);
		}
		
		var div = $('<div>');
		div.css({
			display: 'none'
		});
		$(document.body).append(div);
		
		var video = $('<video>');
		video.prop('controls', true);
		div.append(video);
		
		var source = $('<source>');
		source.attr('src', url);
		video.append(source);
		
		var canvas = $('<canvas>');
		div.append(canvas);
		
		video.on('loadeddata', function () {
			this.currentTime = this.duration / 2;
		});
		
		video.on('seeked', function () {
			canvas.attr('width', video[0].videoWidth);
			canvas.attr('height', video[0].videoHeight);
			var context = canvas[0].getContext('2d');
			context.drawImage(video[0], 0, 0, video[0].videoWidth, video[0].videoHeight);
			canvas[0].toBlob(function (blob) {
				div.remove();
				if (callback) {
					callback(blob);
				}
			});
		});
		
		video[0].load();
		
	}
	
	function isVideo(type) {
		return type && type.trim().toLowerCase().substr(0, "video/".length) === 'video/';
	}
	
	return {
		getThumbnail: getThumbnail,
		isVideo: isVideo
	};
	
});