uiModule('ocean-ace', function () {
	
	function init() {
		var textarea = $(this).prev();
		var editor = ace.edit(this, {
			mode: 'ace/mode/' + ($(this).data('language') || 'javascript'),
			theme: 'ace/theme/monokai'
		});
		editor.getSession().setValue(textarea.val());
		editor.getSession().on('change', function () {
			textarea.val(editor.getSession().getValue());
		});
	}
	
	return {
		init: init
	};
	
});