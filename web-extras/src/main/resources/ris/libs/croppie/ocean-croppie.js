$(document).ready(function () {
	
	$('.ocean-croppie').each(function () {
		
		var me = $(this);
		var img = me.find('img')[0];
		
		me.croppie({
			enableZoom: true,
			viewport: {
				width: me.data('width'),
				height: me.data('height'),
				type: 'square'
			},
			boundary: {
				width: me.width(),
				height: me.height()
			}
		});
		
		var p = $(this).parent();
		var button = p.find('button');
		var file = p.find('input[type="file"]');
		var json = p.find('input[type="hidden"]');
		
		me.on('update.croppie', function(ev, cropData) {
			json.val(JSON.stringify({
				orientation: cropData.orientation,
				zoom: cropData.zoom * me.data('multiplier'),
				points: {
					x1: parseInt(cropData.points[0]),
					y1: parseInt(cropData.points[1]),
					x2: parseInt(cropData.points[2]),
					y2: parseInt(cropData.points[3])
				}
			}));
		});
		
		button.click(function () {
			file.click();
		});
		
		file.change(function () {
			
			if (file[0].files.length <= 0) {
				return;
			}
			
			var url = URL.createObjectURL(file[0].files[0]);
			
			me.croppie('bind', {
				url: url
			}).then(autocrop);
			
		});
		
		function autocrop () {
			
			if (typeof img === 'undefined') {
				img = me.find('img')[0];
			}
			
			var tracker = new tracking.ObjectTracker('face');
			tracking.track(img, tracker);
			tracker.on('track', function(event) {
				console.log(
					'tracking.js detected ' + event.data.length + ' faces',
					event.data
				);
				smartcrop.crop(img, {
					width: me.data('width'),
					height: me.data('height'),
					ruleOfThirds: true,
					boost: event.data.map(function(face) {
						return {
							x: face.x,
							y: face.y,
							width: face.width,
							height: face.height,
							weight: 1.0
						};
					})
				}).then(function (result) {
					result = result.topCrop;
					me.croppie('bind', {
						url: img.src,
						points: [result.x, result.y, result.x + result.width, result.y + result.height]
					});
				});
				
			});
			
		}

	});
	
});