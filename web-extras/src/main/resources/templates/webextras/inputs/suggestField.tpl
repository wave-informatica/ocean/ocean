<label for="fld_${field.name}" class="control-label">${field.label}</label>
<div class="form-element">
    <select name="${field.name}" id="fld_${field.name}" class="form-control"<#if field.required> required</#if><#if field.handler.multiple> multiple</#if><#list field.handler.attributes?keys as k> ${k}="${(field.handler.attributes[k]?html)!}"</#list> data-ocean="selectize.init" data-create="${field.handler.create?c}" data-url="${(field.handler.url?html)!}" data-preload="${field.handler.preload?c}" data-value="${field.values?join(",")}">
        <option value=""></option>
        <#list field.handler.items as item>
        	<#if item.value?? && field.value??>
        		<option value="${item.value?html}" <#if field.values?seq_contains(item.value)>selected="selected"</#if>>${item.label?html}</option>
        	</#if>
        </#list>
    </select>
</div>
