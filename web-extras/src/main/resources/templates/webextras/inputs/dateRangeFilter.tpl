<div id="fw_filter_${head.name}" class="date-range-picker-filter" data-value="${data.getFilterValue(head.sourceField)!}">
    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
    <span></span>
    <button type="button" class="btn btn-danger btn-xs reset-value">&times;</button>
    <input type="hidden" name="fw:filter:${head.name}" value="">
</div>

<script type="text/javascript">
$(document).ready(function () {
    function updateFilterValue(start, end, label) {
        var el = this.element;
        el.find('span').html(start.format('DD MMM YYYY') + ' - ' + end.format('DD MMM YYYY'));
        el.find('input[type="hidden"]').val(start.format('DD/MM/YYYY') + '-' + end.format('DD/MM/YYYY'));
        el.addClass('with-value');
    }
    $('#fw_filter_${head.name}').daterangepicker({
        "showDropdowns": true,
        "autoApply": true,
        "ranges": {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        "locale": {
            "format": "DD MMM YYYY",
            "separator": " - ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "Custom",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
        },
        "opens": "center"
    }, updateFilterValue);
    $('#fw_filter_${head.name} button.reset-value').click(function () {
        var el = $(this).closest('.date-range-picker-filter');
        el.removeClass('with-value');
        el.find('span').html('');
        el.find('input[type="hidden"]').val('');
    });
    var initialValue = $('#fw_filter_${head.name}').data('value');
    if (initialValue) {
        var parts = initialValue.split('-');
        var drp = $('#fw_filter_${head.name}').data('daterangepicker');
        drp.setStartDate(moment(parts[0], 'DD/MM/YYYY'));
        drp.setEndDate(moment(parts[1], 'DD/MM/YYYY'));
        updateFilterValue.call(drp, drp.startDate, drp.endDate);
    }
});
</script>