<select name="fw:filter:${head.name}" class="form-control" multiple<#list head.filter.attributes?keys as k> ${k}="${(head.filter.attributes[k]?html)!}"</#list>>
    <#list head.filter.items as item>
            <#if item.value??>
                    <option value="${item.value?html}" <#if data.getFilterValue(head.sourceField)?has_content && data.getFilterValue(head.sourceField)?split(",")?seq_contains(item.value)>selected="selected"</#if>>${item.label?html}</option>
            </#if>
    </#list>
</select>
			
<script type="text/javascript">
	
	$(document).ready(function () {

		var $selects = $('select[name="fw:filter:${head.name}"]').selectize({
			create: ${head.filter.create?c},
                        dropdownParent: 'body'
                        <#if head.filter.url?has_content>
			, preload: ${head.filter.preload?c}
			, load: function (query, callback) {

			<#if head.filter.preload>
				if (query || query.legth > 0)
					return callback();
			</#if>
				$.ajax({
					url: '${head.filter.url}',
					type: 'GET',
                                        dataType: 'json',
					error: function () {
						callback();
					},
					success: function (res) {
						callback(res.data);
					<#if data.getFilterValue(head.sourceField)?has_content>
                                            <#list data.getFilterValue(head.sourceField)?split(",") as sel>
						$selects[0].selectize.addItem("${sel}");
                                            </#list>
					</#if>
					}
				});
			}
                        </#if>
		});

		$("form").bind("reset", function() {

			$selects.each(function(idx, element) {
				element.selectize.clear();
			});
		});
	});
</script>
