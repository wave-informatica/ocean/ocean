<label for="fld_${field.name}" class="control-label">${field.label}</label>
<div class="form-element">
    <textarea name="${field.name}" id="fld_${field.name}" class="form-control wysihtml5"<#if field.required> required</#if>>${field.value!""?html}</textarea>
</div>