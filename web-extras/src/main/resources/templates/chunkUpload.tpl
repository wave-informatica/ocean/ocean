<label for="fld_${field.name}" class="control-label">${field.label}</label>
<div class="form-element chunk-upload" data-ocean="chunkUpload.init" data-selectable="<#if field.handler.selectable>true<#else>false</#if>" data-checkable="<#if field.handler.checkable>true<#else>false</#if>">
    <input name="${field.name}" id="fld_${field.name}" style="display:none;" multiple type="file"<#if field.required> required</#if><#if field.handler.filter?has_content> accept="<#list field.handler.filter as filter><#list filter.extensions as ext>.${ext}<#sep>,</#sep></#list><#sep>,</#sep></#list>"</#if>>
    <div class="files-container<#if field.handler.selectable> selectable</#if><#if field.handler.checkable> checkable</#if>">
        <div class="placeholder">
            <div class="placeholder-icon-container">
                <div class="icon">
                    <span class="icon"><i class="fa fa-upload"></i></span>
                    <span class="text">Sposta qui i file per aggiungerli alla lista</span>
                </div>
            </div>
        </div>
        <#if field.handler.selectable>
        <div class="vertical-selector selector">
            <#if field.handler.initialConfiguration??>
            <#list field.handler.initialConfiguration.uploadedFiles as upload>
            <input type="radio" name="${field.name}__FileUpload_Selection" value="${upload.transaction}"<#if upload.transaction == field.handler.selected!""> checked="checked"</#if>>
            </#list>
            </#if>
        </div>
        </#if>
        <#if field.handler.checkable>
        <div class="vertical-selector checker">
            <#if field.handler.initialConfiguration??>
            <#list field.handler.initialConfiguration.uploadedFiles as upload>
            <input type="checkbox" name="${field.name}__FileUpload_Check" value="${upload.transaction}"<#if field.handler.checked?seq_contains(upload.transaction)> checked="checked"</#if>>
            </#list>
            </#if>
        </div>
        </#if>
        <#if field.handler.initialConfiguration?? && field.handler.initialConfiguration.uploadedFiles?has_content>
        <#list field.handler.initialConfiguration.uploadedFiles as upload>
        <div class="file-element" data-transaction="${upload.transaction}">
            <div class="file-progress" style="width:100%;">
                <#if field.handler.initialConfiguration.getPreviewOperation(upload)??><i class="fa fa-eye" data-operation="${field.handler.initialConfiguration.getPreviewOperation(upload)}"></i><#else><i class="fa fa-eye disabled"></i></#if>
                ${upload.fileName!"[Nome sconosciuto]"}
            </div>
            <div class="file-name">
                <#if field.handler.initialConfiguration.getPreviewOperation(upload)??><i class="fa fa-eye" data-operation="${field.handler.initialConfiguration.getPreviewOperation(upload)}"></i><#else><i class="fa fa-eye disabled"></i></#if>
                ${upload.fileName!"[Nome sconosciuto]"}
                <a href="${field.handler.initialConfiguration.getDeleteOperation(upload)}" class="delete-file"><i class="fa fa-trash"></i></a>
            </div>
        </div>
        </#list>
        </#if>
    </div>
    <button class="btn btn-default file-chooser" type="button"><span class="glyphicon glyphicon-plus"></span> <@i18n key="chunkUpload.browse" /></button>
</div>