<label for="fld_${field.name}" class="control-label">${field.label}</label>
<div class="form-element">
    <input type="text" name="${field.name}" id="fld_${field.name}" value="${field.value!""}" class="form-control"<#if field.required> required</#if>>
</div>