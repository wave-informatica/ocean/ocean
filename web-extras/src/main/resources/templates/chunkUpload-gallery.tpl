<label for="fld_${field.name}" class="control-label">${field.label}</label>
<div class="form-element chunk-upload" data-ocean="chunkUpload.init" data-selectable="<#if field.handler.selectable>true<#else>false</#if>" data-checkable="<#if field.handler.checkable>true<#else>false</#if>">
    <script type="text/template">
        <input type="radio" class="element-radio" name="{{fieldName}}__FileUpload_Selection" value="">
        <div class="element-container">
            <div class="element">
                <div class="img">
                    <img src="{{{src}}}">
                    <div class="load-progress">
                        <div class="c100 p0">
                            <span>0%</span>
                            <div class="slice">
                                <div class="bar"></div>
                                <div class="fill"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-toolbar">
                    <div class="btn-group">
                        <#if field.handler.checkable>
                        <label class="btn btn-link btn-xs">
                            <input type="checkbox" name="${field.name}__FileUpload_Check" value="">
                            <i class="fa fa-eye"></i>
                        </label>
                        </#if>
                        <#if field.handler.descriptable>
                        <input type="hidden" name="${field.name}__FileUpload_Description" value="">
                        <a class="btn btn-link btn-xs btn-description"><i class="fa fa-paragraph"></i></a>
                        </#if>
                    </div>
                    <div class="btn-group pull-right">
                        <a href="" class="btn btn-link btn-xs delete-file"><i class="fa fa-trash"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </script>
    <input name="${field.name}" id="fld_${field.name}" style="display:none;" multiple type="file"<#if field.required> required</#if><#if field.handler.filter?has_content> accept="<#list field.handler.filter as filter><#list filter.extensions as ext>.${ext}<#sep>,</#sep></#list><#sep>,</#sep></#list>"</#if>>
    <div class="files-container gallery">
        <#if field.handler.initialConfiguration?? && field.handler.initialConfiguration.uploadedFiles?has_content>
        <#list field.handler.initialConfiguration.uploadedFiles as upload>
        <#if field.handler.selectable>
        <input type="radio" class="element-radio" name="${field.name}__FileUpload_Selection" value="${upload.transaction}"<#if upload.transaction == field.handler.selected!""> checked="checked"</#if>>
        </#if>
        <div class="element-container">
            <div class="element">
                <div class="img">
                    <img src="${field.handler.initialConfiguration.getPreviewOperation(upload)}">
                </div>
                <div class="btn-toolbar">
                    <div class="btn-group">
                        <#if field.handler.checkable>
                        <label class="btn btn-link btn-xs">
                            <input type="checkbox" name="${field.name}__FileUpload_Check" value="${upload.transaction}"<#if field.handler.checked?seq_contains(upload.transaction)> checked="checked"</#if>>
                            <i class="fa fa-eye"></i>
                        </label>
                        </#if>
                        <#if field.handler.descriptable>
                        <input type="hidden" name="${field.name}__FileUpload_Description_${upload.transaction}" value="${(upload.description!)?html}">
                        <a class="btn btn-link btn-xs btn-description"><i class="fa fa-paragraph"></i></a>
                        </#if>
                    </div>
                    <div class="btn-group pull-right">
                        <a href="${field.handler.initialConfiguration.getDeleteOperation(upload)}" class="btn btn-link btn-xs delete-file"><i class="fa fa-trash"></i></a>
                    </div>
                </div>
            </div>
        </div>
        </#list>
        </#if>
    </div>
    <button class="btn btn-default file-chooser" type="button"><span class="glyphicon glyphicon-plus"></span> <@i18n key="chunkUpload.browse" /></button>
</div>
                    
<div class="modal fade" tabindex="-1" role="dialog" id="fld_${field.name}_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="<@i18n key="close" />"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><@i18n key="ocean.chunkUpload.gallery.editDescription" /></h4>
            </div>
            <div class="modal-body">
                <textarea rows="6" class="form-control"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><@i18n key="close" /></button>
                <button type="button" class="btn btn-primary"><@i18n key="save" /></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->