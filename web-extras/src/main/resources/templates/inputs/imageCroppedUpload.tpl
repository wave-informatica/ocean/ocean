<label class="col-lg-2 control-label">${field.label}</label>
<div class="col-lg-5">
    <div class="ocean-croppie" style="width: ${(field.handler.width + field.handler.extraWidth * 2)?c}px; height: ${(field.handler.height + field.handler.extraHeight * 2)?c}px;margin-bottom: 50px;" data-width="${field.handler.width?c}" data-height="${field.handler.height?c}" data-multiplier="${field.handler.multiplier?c}">
    </div>
    <div class="croppie-choose">
    	<button class="btn btn-default" type="button"><i class="fa fa-folder"></i> Browse</button>
    </div>
    <div class="hidden">
    	<input type="file" name="${field.name?html}" accept=".gif,.jpg,.jpeg,.png">
    	<input type="hidden" name="${field.name?html}_crop" value="">
    </div>
</div>
<#if field.value?has_content>
<div class="col-lg-5">
	<div class="thumbnail">
		<img src="${field.value}" style="max-height: ${field.handler.height?c}px; margin: ${field.handler.extraHeight?c}px auto;">
	</div>
</div>
</#if>