/*
* Copyright 2015, 2017 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.persistence.hibernate43;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.persistence.SharedCacheMode;
import javax.persistence.ValidationMode;
import javax.persistence.spi.ClassTransformer;
import javax.persistence.spi.PersistenceUnitInfo;
import javax.persistence.spi.PersistenceUnitTransactionType;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.hibernate.jpa.HibernatePersistenceProvider;

import com.waveinformatica.ocean.core.internal.PersistenceConfigurationManager;
import com.waveinformatica.ocean.core.modules.Module;
import com.waveinformatica.ocean.core.modules.ModuleClassLoader;
import com.waveinformatica.ocean.core.persistence.DataSourceProvider;
import com.waveinformatica.ocean.core.persistence.EntitiesStore;
import com.waveinformatica.ocean.core.util.LoggerUtils;

/**
 *
 * @author Ivano
 */
public class ModulePersistenceUnitInfo implements PersistenceUnitInfo {
	
	private Module module;
	private List<String> managedClassNames;
	private final Properties properties = new Properties();
	
	@Inject
	private PersistenceConfigurationManager manager;
	
	@Inject
	private DataSourceProvider dataSourceProvider;
	
	@Inject
	private EntitiesStore entityStore;
	
	private String connectionName;
	
	private PersistenceConfigurationManager.Element element;
	
	public void init (String connectionName, Module module) {
		
		this.module = module;
		this.element = StringUtils.isBlank(connectionName) ? manager.getDefaultConnection() : manager.getConnection(connectionName);
		this.connectionName = connectionName;
		
		Set<String> entityClasses = entityStore.getEntityClassNames(module.getName(), connectionName);
		if (entityClasses != null && entityClasses.size() > 0) {
			managedClassNames = new ArrayList<String>(entityClasses);
		}
		else {
			managedClassNames = new ArrayList<String>(0);
		}
		
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		properties.setProperty("hibernate.hbm2ddl.auto", "update");
		properties.setProperty("hibernate.show_sql", "true");
		
		properties.putAll(this.element.getProperties());
	}
	
	public boolean isValid() {
		return !managedClassNames.isEmpty();
	}
	
	@Override
	public String getPersistenceUnitName() {
		return StringUtils.isNotBlank(connectionName) ? module.getName() + ":" + connectionName : module.getName();
	}
	
	@Override
	public String getPersistenceProviderClassName() {
		return HibernatePersistenceProvider.class.getName();
	}
	
	@Override
	public PersistenceUnitTransactionType getTransactionType() {
		return PersistenceUnitTransactionType.RESOURCE_LOCAL;
	}
	
	@Override
	public DataSource getJtaDataSource() {
		return null;
	}
	
	@Override
	public synchronized DataSource getNonJtaDataSource() {
		try {
			return dataSourceProvider.getDataSource(StringUtils.isNotBlank(connectionName) ? connectionName : null);
		} catch (SQLException ex) {
			LoggerUtils.getLogger(CorePersistenceUnitInfo.class).log(Level.SEVERE, "Unable to get DataSource", ex);
		}
		return null;
	}
	
	@Override
	public List<String> getMappingFileNames() {
		return new ArrayList<String>(0);
	}
	
	@Override
	public List<URL> getJarFileUrls() {
		return new ArrayList<URL>(0);
	}
	
	@Override
	public URL getPersistenceUnitRootUrl() {
		String url = module.getModuleClassLoader().getModuleFile().toURI().toString();
		if (StringUtils.isNotBlank(connectionName)) {
			url += "#" + connectionName;
		}
		try {
			return new URL(url);
		} catch (MalformedURLException ex) {
			Logger.getLogger(ModulePersistenceUnitInfo.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}
	
	@Override
	public List<String> getManagedClassNames() {
		return managedClassNames;
	}
	
	@Override
	public boolean excludeUnlistedClasses() {
		return true;
	}
	
	@Override
	public SharedCacheMode getSharedCacheMode() {
		return SharedCacheMode.ENABLE_SELECTIVE;
	}
	
	@Override
	public ValidationMode getValidationMode() {
		return ValidationMode.AUTO;
	}
	
	@Override
	public Properties getProperties() {
		return properties;
	}
	
	@Override
	public String getPersistenceXMLSchemaVersion() {
		return "2.0";
	}
	
	@Override
	public ClassLoader getClassLoader() {
		return module.getModuleClassLoader();
	}
	
	@Override
	public void addTransformer(ClassTransformer ct) {
		//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	
	@Override
	public ClassLoader getNewTempClassLoader() {
		try {
			return new ModuleClassLoader(module.getModuleClassLoader());
		} catch (IOException ex) {
			Logger.getLogger(ModulePersistenceUnitInfo.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}
	}
	
}
