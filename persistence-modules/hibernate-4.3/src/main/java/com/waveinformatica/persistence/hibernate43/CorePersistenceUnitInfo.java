/*
* Copyright 2015, 2017 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.persistence.hibernate43;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import javax.persistence.SharedCacheMode;
import javax.persistence.ValidationMode;
import javax.persistence.spi.ClassTransformer;
import javax.persistence.spi.PersistenceUnitInfo;
import javax.persistence.spi.PersistenceUnitTransactionType;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.hibernate.jpa.HibernatePersistenceProvider;

import com.waveinformatica.ocean.core.Constants;
import com.waveinformatica.ocean.core.internal.PersistenceConfigurationManager;
import com.waveinformatica.ocean.core.persistence.DataSourceProvider;
import com.waveinformatica.ocean.core.persistence.EntitiesStore;
import com.waveinformatica.ocean.core.util.LoggerUtils;

/**
 *
 * @author Ivano
 */
public class CorePersistenceUnitInfo implements PersistenceUnitInfo {
	
	private final DataSourceProvider dataSourceProvider;
	private final List<String> managedClassNames;
	private final Properties properties = new Properties();
	private final String connectionName;
	
	public CorePersistenceUnitInfo(String connectionName, EntitiesStore entityStore, PersistenceConfigurationManager manager, DataSourceProvider dataSourceProvider) {
		
		this.dataSourceProvider = dataSourceProvider;
		this.connectionName = connectionName;
		
		managedClassNames = new ArrayList<String>(entityStore.getEntityClassNames(Constants.CORE_MODULE_NAME, connectionName));
		
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		properties.setProperty("hibernate.hbm2ddl.auto", "update");
		properties.setProperty("hibernate.show_sql", "true");
		
		PersistenceConfigurationManager.Element el = StringUtils.isBlank(connectionName) ? manager.getDefaultConnection() : manager.getConnection(connectionName);
		
		properties.putAll(el.getProperties());
		
	}
	
	public boolean isValid() {
		return !managedClassNames.isEmpty();
	}
	
	@Override
	public String getPersistenceUnitName() {
		return Constants.CORE_MODULE_NAME;
	}
	
	@Override
	public String getPersistenceProviderClassName() {
		return HibernatePersistenceProvider.class.getName();
	}
	
	@Override
	public PersistenceUnitTransactionType getTransactionType() {
		return PersistenceUnitTransactionType.RESOURCE_LOCAL;
	}
	
	@Override
	public DataSource getJtaDataSource() {
		return null;
	}
	
	@Override
	public DataSource getNonJtaDataSource() {
		try {
			return dataSourceProvider.getDataSource(StringUtils.isNotBlank(connectionName) ? connectionName : null);
		} catch (SQLException ex) {
			LoggerUtils.getLogger(CorePersistenceUnitInfo.class).log(Level.SEVERE, null, ex);
		}
		return null;
	}
	
	@Override
	public List<String> getMappingFileNames() {
		return new ArrayList<String>(0);
	}
	
	@Override
	public List<URL> getJarFileUrls() {
		return new ArrayList<URL>(0);
	}
	
	@Override
	public URL getPersistenceUnitRootUrl() {
		return this.getClass().getClassLoader().getResource("");
	}
	
	@Override
	public List<String> getManagedClassNames() {
		return managedClassNames;
	}
	
	@Override
	public boolean excludeUnlistedClasses() {
		return false;
	}
	
	@Override
	public SharedCacheMode getSharedCacheMode() {
		return SharedCacheMode.ENABLE_SELECTIVE;
	}
	
	@Override
	public ValidationMode getValidationMode() {
		return ValidationMode.AUTO;
	}
	
	@Override
	public Properties getProperties() {
		return properties;
	}
	
	@Override
	public String getPersistenceXMLSchemaVersion() {
		return "2.0";
	}
	
	@Override
	public ClassLoader getClassLoader() {
		return this.getClass().getClassLoader();
	}
	
	@Override
	public void addTransformer(ClassTransformer ct) {
		//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	
	@Override
	public ClassLoader getNewTempClassLoader() {
		return this.getClassLoader();
	}
	
}
