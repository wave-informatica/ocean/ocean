/*
* Copyright 2015, 2017 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.persistence.hibernate43;

import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.internal.PersistenceConfigurationManager;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.persistence.spi.PersistenceUnitInfo;

import com.waveinformatica.ocean.core.modules.Module;
import com.waveinformatica.ocean.core.persistence.DataSourceProvider;
import com.waveinformatica.ocean.core.persistence.EntitiesStore;
import com.waveinformatica.ocean.core.persistence.OceanPersistenceProvider;
import javax.inject.Inject;
import org.hibernate.jpa.HibernatePersistenceProvider;


/**
 *
 * @author Ivano
 */
public class WavePersistenceProvider implements OceanPersistenceProvider {
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private DataSourceProvider dataSourceProvider;
	
	@Inject
	private PersistenceConfigurationManager configManager;
	
	@Inject
	private EntitiesStore entityStore;
	
	private final HibernatePersistenceProvider hibernate = new HibernatePersistenceProvider();
	
	@SuppressWarnings("rawtypes")
	@Override
	public EntityManagerFactory createModuleEntityManagerFactory(String connectionName, Module module, Map map) {
		ModulePersistenceUnitInfo pui = factory.newInstance(ModulePersistenceUnitInfo.class);
		pui.init(connectionName, module);
		return hibernate.createContainerEntityManagerFactory(pui, map);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public EntityManagerFactory createCoreEntityManagerFactory(String connectionName, Map map) {
		PersistenceUnitInfo pui = new CorePersistenceUnitInfo(
			  connectionName,
			  entityStore,
			  configManager,
			  dataSourceProvider
		);
		return hibernate.createContainerEntityManagerFactory(pui, map);
	}
	
}
