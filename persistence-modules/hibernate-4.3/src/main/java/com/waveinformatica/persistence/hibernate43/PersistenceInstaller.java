/*
* Copyright 2015, 2016 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.persistence.hibernate43;

import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.Event;
import com.waveinformatica.ocean.core.controllers.events.ModuleEvent;
import com.waveinformatica.ocean.core.persistence.PersistenceManager;
import javax.inject.Inject;

/**
 *
 * @author Ivano
 */
//@CoreEventListener(eventNames = CoreEventName.MODULE_LOADED)
public class PersistenceInstaller implements IEventListener {
	
	@Inject
	private PersistenceManager persistenceManager;
	
	@Inject
	private ObjectFactory factory;
	
	@Override
	public void performAction(Event event) {
		
		ModuleEvent evt = (ModuleEvent) event;
		
		if (!evt.getModuleName().equals("com.waveinformatica.ocean.persistence-hibernate-43")) {
			return;
		}
		
		WavePersistenceProvider provider = factory.newInstance(WavePersistenceProvider.class);
		persistenceManager.loadPersistenceProvider(provider);
		
	}
	
}
