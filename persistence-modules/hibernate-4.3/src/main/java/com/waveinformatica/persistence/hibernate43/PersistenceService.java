/*
 * Copyright 2016, 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.persistence.hibernate43;

import com.waveinformatica.ocean.core.annotations.Service;
import com.waveinformatica.ocean.core.controllers.IService;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.ServiceStatus;
import com.waveinformatica.ocean.core.persistence.PersistenceManager;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import javax.inject.Inject;
import org.apache.log4j.Appender;
import org.apache.log4j.Layout;
import org.apache.log4j.spi.ErrorHandler;
import org.apache.log4j.spi.Filter;
import org.apache.log4j.spi.LoggingEvent;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;

/**
 *
 * @author Ivano
 */
@Service("PersistenceService")
public class PersistenceService implements IService {

	@Inject
	private PersistenceManager persistenceManager;

	@Inject
	private ObjectFactory factory;

	private Appender log4jAppender;

	@Override
	public boolean start() {

		// Create logger org.hibernate.tool.hbm2ddl.SchemaUpdate
		boolean found = false;
		org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SchemaUpdate.class.getName());
		Enumeration en = logger.getAllAppenders();
		while (en.hasMoreElements()) {
			Appender a = (Appender) en.nextElement();
			if (a instanceof Log4jAppenderWrapper) {
				found = true;
				break;
			}
		}
		if (!found) {
			log4jAppender = new Log4jAppenderWrapper();
			logger.addAppender(log4jAppender);
		}

		WavePersistenceProvider provider = factory.newInstance(WavePersistenceProvider.class);
		persistenceManager.loadPersistenceProvider(provider);

		return persistenceManager.isAvailable();

	}

	@Override
	public boolean stop() {

		if (log4jAppender != null) {
			org.apache.log4j.Logger.getLogger(SchemaUpdate.class.getName()).removeAppender(log4jAppender);
			log4jAppender = null;
		}

		return true;
	}

	@Override
	public ServiceStatus getServiceStatus() {
		ServiceStatus status = new ServiceStatus();
		status.setRunning(persistenceManager.isAvailable());
		return status;
	}

	public static class Log4jAppenderWrapper implements Appender {

		private final List<Filter> filters = new ArrayList<Filter>();
		private String name;
		private ErrorHandler errorHandler;
		private Layout layout;

		public Log4jAppenderWrapper() {
			this.name = this.getClass().getName();
		}

		@Override
		public void addFilter(Filter filter) {
			filters.add(filter);
		}

		@Override
		public Filter getFilter() {
			return filters.isEmpty() ? null : filters.get(0);
		}

		@Override
		public void clearFilters() {
			filters.clear();
		}

		@Override
		public void close() {

		}

		@Override
		public void doAppend(LoggingEvent le) {

			String msg = (layout != null ? layout.format(le) : le.getMessage().toString());
			Level l = Level.ALL;

			switch (le.getLevel().toInt()) {
				case org.apache.log4j.Level.DEBUG_INT:
				case org.apache.log4j.Level.TRACE_INT:
					l = Level.FINE;
					break;
				case org.apache.log4j.Level.INFO_INT:
					l = Level.INFO;
					break;
				case org.apache.log4j.Level.WARN_INT:
					l = Level.WARNING;
					break;
				case org.apache.log4j.Level.ERROR_INT:
				case org.apache.log4j.Level.FATAL_INT:
					l = Level.SEVERE;
					break;
				default:
					break;
			}

			LoggerUtils.getLogger(PersistenceService.class).log(
				  l,
				  msg,
				  le.getThrowableInformation() != null ? le.getThrowableInformation().getThrowable() : null
			);
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public void setErrorHandler(ErrorHandler eh) {
			this.errorHandler = eh;
		}

		@Override
		public ErrorHandler getErrorHandler() {
			return errorHandler;
		}

		@Override
		public void setLayout(Layout layout) {
			this.layout = layout;
		}

		@Override
		public Layout getLayout() {
			return layout;
		}

		@Override
		public void setName(String string) {
			this.name = string;
		}

		@Override
		public boolean requiresLayout() {
			return false;
		}

	}

}
