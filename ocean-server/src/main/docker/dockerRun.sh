#!/bin/sh

MORE_OPTIONS=""

# Options to improve containerization
MORE_OPTIONS="${MORE_OPTIONS} -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap"

[ ! -z "${OCEAN_JMX}" ] && MORE_OPTIONS="${MORE_OPTIONS} -Dcom.sun.management.jmxremote.port=${OCEAN_JMX} -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote -Djava.rmi.server.hostname=$(hostname -i)"

[ ! -z "${OCEAN_DEBUG}" ] && MORE_OPTIONS="${MORE_OPTIONS} -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=${OCEAN_DEBUG}"

exitCode=255

if [ -d lib ]; then
	CP=""
	for i in `ls lib/`; do
		[ ! -z "$CP" ] && CP="$CP:"
		CP="${CP}lib/$i"
	done
	for i in `ls server/`; do
		[ ! -z "$CP" ] && CP="$CP:"
		CP="${CP}server/$i"
	done
	[ ! -z "$CP" ] && MORE_OPTIONS="${MORE_OPTIONS} -cp $CP"
	java -server -Dfile.encoding=UTF-8 -Djava.awt.headless=true ${MORE_OPTIONS} com.waveinformatica.ocean.server.Main
	exitCode = $?
else
	java -server -Dfile.encoding=UTF-8 -Djava.awt.headless=true ${MORE_OPTIONS} -jar server/ocean-server-2.4.0-SNAPSHOT.jar
	exitCode = $?
fi

exit $exitCode