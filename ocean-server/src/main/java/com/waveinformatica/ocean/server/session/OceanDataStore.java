package com.waveinformatica.ocean.server.session;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.jetty.server.session.SessionContext;
import org.eclipse.jetty.server.session.SessionData;
import org.eclipse.jetty.server.session.SessionDataStore;

import com.waveinformatica.ocean.core.Application;

public class OceanDataStore implements SessionDataStore {
	
	private OceanCacheSessionStore store;
	private boolean started = false;
	
	private void ensureStoreLoaded() {
		if (store == null) {
			store = Application.getInstance().getObjectFactory().newInstance(OceanCacheSessionStore.class);
		}
	}
	
	public void initialize(SessionContext context) throws Exception {
		// TODO Auto-generated method stub

	}

	public SessionData load(String id) throws Exception {
		ensureStoreLoaded();
		return (SessionData) store.get(id);
	}

	public void store(String id, SessionData data) throws Exception {
		ensureStoreLoaded();
		store.put(id, data);
	}

	public boolean delete(String id) throws Exception {
		ensureStoreLoaded();
		store.remove(id);
		return !store.containsKey(id);
	}

	public void start() throws Exception {
		started = true;
	}

	public void stop() throws Exception {
		started = false;
	}

	public boolean isRunning() {
		return started;
	}

	public boolean isStarted() {
		return started;
	}

	public boolean isStarting() {
		return false;
	}

	public boolean isStopping() {
		return false;
	}

	public boolean isStopped() {
		return !started;
	}

	public boolean isFailed() {
		return false;
	}

	public void addLifeCycleListener(Listener listener) {
		// TODO Auto-generated method stub

	}

	public void removeLifeCycleListener(Listener listener) {
		// TODO Auto-generated method stub

	}

	public SessionData newSessionData(String id, long created, long accessed, long lastAccessed, long maxInactiveMs) {
		SessionData sessionData = new SessionData(id, "/", "localhost", created, accessed, lastAccessed, maxInactiveMs);
		return sessionData;
	}

	public Set<String> getExpired(Set<String> candidates) {
		if (candidates == null) {
			return null;
		}
		ensureStoreLoaded();
		
		Set<String> result = new HashSet<String>(candidates);
		Iterator<String> iter = result.iterator();
		while (iter.hasNext()) {
			String k = iter.next();
			SessionData sd = (SessionData) store.get(k);
			if (sd == null) {
				continue;
			}
			if (!sd.isExpiredAt(System.currentTimeMillis())) {
				iter.remove();
			}
		}
		
		return candidates;
	}

	public boolean isPassivating() {
		return false;
	}

	public boolean exists(String id) throws Exception {
		ensureStoreLoaded();
		return store.get(id) != null;
	}

}
