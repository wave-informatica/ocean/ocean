package com.waveinformatica.ocean.server.urlhandlers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.net.URLStreamHandlerFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.util.LoggerUtils;

public class OceanUrlHandlers {
	
	private static final String URLHANDLERS_PROPERTY = "java.protocol.handler.pkgs";
	
	public static void init() {
		
		String prev = System.getProperty(URLHANDLERS_PROPERTY);
		if (StringUtils.isBlank(prev)) {
			prev = "";
		}
		
		List<String> packages = new ArrayList<>(Arrays.asList(prev.split("\\|")));
		
		if (!packages.contains(OceanUrlHandlers.class.getPackage().getName())) {
			
			packages.add(OceanUrlHandlers.class.getPackage().getName());
			
			System.setProperty(URLHANDLERS_PROPERTY, StringUtils.join(packages, '|'));
			
		}
		
	}
	
}
