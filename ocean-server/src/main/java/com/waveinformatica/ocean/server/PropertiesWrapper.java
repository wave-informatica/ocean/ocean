package com.waveinformatica.ocean.server;

import java.util.Properties;

import org.apache.commons.lang.StringUtils;

public class PropertiesWrapper {
	
	private final Properties properties;

	public PropertiesWrapper(Properties properties) {
		super();
		this.properties = properties;
	}

	public Properties getProperties() {
		return properties;
	}
	
	public String getOrDefault(String name, String defValue) {
		return StringUtils.defaultIfBlank(
				System.getProperty("ocean." + name), StringUtils.defaultIfBlank(
						properties.getProperty(name), StringUtils.defaultIfBlank(
								System.getenv("ocean." + name),
								defValue)));
	}
	
	public int intOrDefault(String name, int defValue) {
		return Integer.parseInt(getOrDefault(name, String.valueOf(defValue)));
	}
	
}
