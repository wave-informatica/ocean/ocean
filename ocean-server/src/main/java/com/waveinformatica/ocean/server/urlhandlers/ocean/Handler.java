package com.waveinformatica.ocean.server.urlhandlers.ocean;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.modules.Module;

public class Handler extends URLStreamHandler {

	@Override
	protected URLConnection openConnection(URL u) throws IOException {
		
		String url = u.toString().substring("ocean:".length());
		
		int pos = url.indexOf(':');
		String moduleName = url.substring(0, pos);
		String res = url.substring(pos + 1);
		
		Module module = Application.getInstance().getModulesManager().getModule(moduleName);
		
		return module.getModuleClassLoader().getResourceConnection(res);
		
	}

}
