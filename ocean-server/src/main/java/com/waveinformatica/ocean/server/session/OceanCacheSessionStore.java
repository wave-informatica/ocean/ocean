package com.waveinformatica.ocean.server.session;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.annotations.ManagedCache;
import com.waveinformatica.ocean.core.cache.OceanCache;

public class OceanCacheSessionStore {
	
	@Inject
	@ManagedCache(value = "Sessions Cache", register = true)
	private OceanCache sessionsCache;

	public int size() {
		return sessionsCache.size();
	}

	public boolean isEmpty() {
		return sessionsCache.isEmpty();
	}

	public boolean containsKey(Object key) {
		return sessionsCache.containsKey(key);
	}

	public boolean containsValue(Object value) {
		return sessionsCache.containsValue(value);
	}

	public Object get(Object key) {
		return sessionsCache.get(key);
	}

	public Object put(Object key, Object value) {
		return sessionsCache.put(key, value);
	}

	public Object remove(Object key) {
		return sessionsCache.remove(key);
	}

	public void putAll(Map m) {
		sessionsCache.putAll(m);
	}

	public void clear() {
		sessionsCache.clear();
	}

	public Set keySet() {
		return sessionsCache.keySet();
	}

	public Collection values() {
		return sessionsCache.values();
	}

	public Set entrySet() {
		return sessionsCache.entrySet();
	}

	public boolean equals(Object o) {
		return sessionsCache.equals(o);
	}

	public int hashCode() {
		return sessionsCache.hashCode();
	}

	public String toString() {
		return sessionsCache.toString();
	}
	
}
