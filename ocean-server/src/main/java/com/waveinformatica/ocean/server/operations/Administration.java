/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.server.operations;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.MessageResult;
import com.waveinformatica.ocean.core.controllers.results.MessageResult.MessageType;

@OperationProvider(namespace = "admin/server")
public class Administration {
	
	@Inject
	private ObjectFactory factory;
	
	@Operation("")
	public MessageResult test() {
		
		MessageResult result = factory.newInstance(MessageResult.class);
		
		result.setMessage("OK, it works!");
		result.setType(MessageType.CONFIRM);
		
		return result;
		
	}
	
}
