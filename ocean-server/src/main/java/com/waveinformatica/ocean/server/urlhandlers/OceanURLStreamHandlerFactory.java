package com.waveinformatica.ocean.server.urlhandlers;

import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;

public class OceanURLStreamHandlerFactory implements URLStreamHandlerFactory {
	
	private final URLStreamHandlerFactory wrappedFactory;
	
	public OceanURLStreamHandlerFactory(URLStreamHandlerFactory wrappedFactory) {
		
		this.wrappedFactory = wrappedFactory;
		
	}

	@Override
	public URLStreamHandler createURLStreamHandler(String protocol) {
		
		ObjectFactory factory = Application.getInstance().getObjectFactory();
		
		if ("ocean".equals(protocol)) {
			return factory.newInstance(com.waveinformatica.ocean.server.urlhandlers.ocean.Handler.class);
		}
		
		if (wrappedFactory != null) {
			return wrappedFactory.createURLStreamHandler(protocol);
		}
		
		return null;
	}

}
