package com.waveinformatica.ocean.server.urlhandlers;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;

import com.waveinformatica.ocean.core.urlhandlers.OceanUrlFactory;
import com.waveinformatica.ocean.core.util.LoggerUtils;

public class OceanServerModuleResourceFactory implements OceanUrlFactory {

	public URL createResourceUrl(JarFile file, JarEntry entry) {
		try {
			File f = new File(file.getName());
			return new URL("ocean:" + f.getName().substring(f.getName().lastIndexOf('/') + 1, f.getName().lastIndexOf('.')) + ":/" + entry.getName());
		} catch (MalformedURLException e) {
			LoggerUtils.getLogger(OceanServerModuleResourceFactory.class).log(Level.SEVERE, "Unable to build ocean: URL for file " + file.getName() + " with entry " + entry.getName(), e);
			return null;
		}
	}

}
