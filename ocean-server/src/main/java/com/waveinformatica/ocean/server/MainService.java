/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.server;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.MalformedURLException;
import java.util.EnumSet;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.servlet.DispatcherType;
import javax.websocket.server.ServerContainer;

import org.eclipse.jetty.jmx.MBeanContainer;
import org.eclipse.jetty.jndi.InitialContextFactory;
import org.eclipse.jetty.plus.jndi.EnvEntry;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.LowResourceMonitor;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.SessionIdManager;
import org.eclipse.jetty.server.session.DefaultSessionCache;
import org.eclipse.jetty.server.session.DefaultSessionIdManager;
import org.eclipse.jetty.server.session.SessionCache;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.util.thread.ScheduledExecutorScheduler;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.websocket.jsr356.server.deploy.WebSocketServerContainerInitializer;

import com.waveinformatica.ocean.core.FrameworkConfiguration;
import com.waveinformatica.ocean.core.cdi.GlobalSessionContextFilter;
import com.waveinformatica.ocean.core.features.websocket.RootEndpoint;
import com.waveinformatica.ocean.server.session.OceanDataStore;
import com.waveinformatica.ocean.server.urlhandlers.OceanServerModuleResourceFactory;
import com.waveinformatica.ocean.server.urlhandlers.OceanUrlHandlers;

public class MainService {
	
	private Server server;
	private final Logger logger = Logger.getLogger("com.waveinformatica.ocean.server");
	
	public void initialize() throws Exception {
		
		File configFile = new File("." + File.separatorChar + "Ocean" + File.separatorChar + "server.properties").getAbsoluteFile();
		Properties serverProps = new Properties();
		FileReader reader = null;
		try {
			reader = new FileReader(configFile);
			serverProps.load(reader);
			logger.log(Level.INFO, "Server configuration successfully loaded from file " + configFile.getAbsolutePath());
		} catch (FileNotFoundException e) {
			logger.log(Level.WARNING, "Server configuration file " + configFile.getAbsolutePath() + " not found. Using defaults.");
		} catch (IOException e) {
			logger.log(Level.WARNING, "Error loading file " + configFile.getAbsolutePath(), e);
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
		PropertiesWrapper spw = new PropertiesWrapper(serverProps);
		
		QueuedThreadPool threadPool = new QueuedThreadPool();
		threadPool.setMaxThreads(500);
		
		server = new Server(threadPool);
		
		// Scheduler
		server.addBean(new ScheduledExecutorScheduler());
		
		// Extra options
		server.setDumpAfterStart(false);
		server.setDumpBeforeStop(false);
		server.setStopAtShutdown(true);
		
		server.setAttribute("org.eclipse.jetty.server.Request.maxFormContentSize", 128*1024*1024);
		
		MBeanContainer mbContainer = new MBeanContainer(ManagementFactory.getPlatformMBeanServer());
		server.addBean(mbContainer);
		
		// HTTP Configuration
		HttpConfiguration httpConfig = new HttpConfiguration();
		httpConfig.setSecureScheme("https");
		httpConfig.setSecurePort(spw.intOrDefault("connectors.https.port", 8443));
		httpConfig.setOutputBufferSize(32768);
		httpConfig.setRequestHeaderSize(8192);
		httpConfig.setResponseHeaderSize(8192);
		httpConfig.setSendServerVersion(true);
		httpConfig.setSendDateHeader(false);
		
		ServerConnector http = new ServerConnector(server, new HttpConnectionFactory(httpConfig));
		http.setHost(spw.getOrDefault("connectors.http.host", "0.0.0.0"));
		http.setPort(spw.intOrDefault("connectors.http.port", 8080));
		http.setIdleTimeout(spw.intOrDefault("connectors.http.idleTimeout", 30000));
		server.addConnector(http);
		
		Configuration.ClassList classlist = Configuration.ClassList.setServerDefault(server);
		classlist.addAfter("org.eclipse.jetty.webapp.FragmentConfiguration", "org.eclipse.jetty.plus.webapp.EnvConfiguration", "org.eclipse.jetty.plus.webapp.PlusConfiguration");
		classlist.addBefore("org.eclipse.jetty.webapp.JettyWebXmlConfiguration", "org.eclipse.jetty.annotations.AnnotationConfiguration");
        
		SessionIdManager sessionIdManager = new DefaultSessionIdManager(server);
		server.setSessionIdManager(sessionIdManager);
		
		SessionHandler sessionHandler = new SessionHandler();
		SessionCache sessionCache = new DefaultSessionCache(sessionHandler);
		sessionHandler.setSessionCache(sessionCache);
		sessionCache.setSessionDataStore(new OceanDataStore());
		
        WebAppContext context = new WebAppContext();
//        context.setHandler(sessionHandler);
        context.setSessionHandler(sessionHandler);
        context.setContextPath("/");
        context.setDescriptor(MainService.class.getResource("/WEB-INF/web.xml").toExternalForm());
        try {
        	context.setBaseResource(Resource.newClassPathResource("WEB-INF/web.xml").addPath("../.."));
        } catch (MalformedURLException e) {
        	context.setBaseResource(Resource.newClassPathResource("/"));
        }
        context.setParentLoaderPriority(true);
        context.addFilter(GlobalSessionContextFilter.class, "/*", EnumSet.of(DispatcherType.ASYNC, DispatcherType.ERROR, DispatcherType.FORWARD, DispatcherType.INCLUDE, DispatcherType.REQUEST));
        server.setHandler(context);
        
        ServerContainer container = WebSocketServerContainerInitializer.configureContext(context);
        container.addEndpoint(RootEndpoint.class);
		
		// Low resource monitor
		LowResourceMonitor lowResourcesMonitor=new LowResourceMonitor(server);
		lowResourcesMonitor.setPeriod(1000);
		lowResourcesMonitor.setLowResourcesIdleTimeout(200);
		lowResourcesMonitor.setMonitorThreads(true);
		lowResourcesMonitor.setMaxConnections(0);
		lowResourcesMonitor.setMaxMemory(0);
		lowResourcesMonitor.setMaxLowResourcesTime(5000);
		server.addBean(lowResourcesMonitor);
		
		if (System.getProperty(Context.INITIAL_CONTEXT_FACTORY) == null) {
            System.setProperty(Context.INITIAL_CONTEXT_FACTORY, InitialContextFactory.class.getName());
        }
		
		OceanUrlHandlers.init();
		
		FrameworkConfiguration config = new FrameworkConfiguration();
		config.getAppClassBases().add(MainService.class);
		config.setUrlFactory(new OceanServerModuleResourceFactory());
		new EnvEntry("ocean/configuration", config);
		
		//NamingUtil.bind(new InitialContext(), "java:app/AppName", "Server");
		
	}
	
	public void start() {
		
		try {
			
			server.start();
			
			server.join();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void terminate() {
		
		try {
			
			server.stop();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public boolean isRunning() {
		return server != null && server.isRunning();
	}
	
}
