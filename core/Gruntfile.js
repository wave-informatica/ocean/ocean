module.exports = function(grunt) {
	grunt.initConfig({
    	pkg: grunt.file.readJSON('package.json'),
    	copy: {
    		main: {
    			files: [
    				{ expand: true, cwd: 'node_modules/eonasdan-bootstrap-datetimepicker/build/', src: [ '**' ], dest: 'target/generated-resources/frontend/libs/eonasdan-bootstrap-datetimepicker/' }
    			]
    		}
    	}
	});
	
	grunt.loadNpmTasks('grunt-contrib-copy');
};