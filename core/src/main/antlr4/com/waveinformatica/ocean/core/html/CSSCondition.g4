grammar CSSCondition;

mediaQueryList
  :
    mediaQuery (',' mediaQuery)* EOF
  ;

mediaQuery
  :
    (ONLY | NOT)? IDENT (AND mediaExpression)*
  | mediaExpression (AND mediaExpression)*
  ;

mediaExpression
  :
    '(' IDENT (':' expression (',' expression)*)? ')'
;

property
  :
    IDENT ':' expression (',' expression)* IMPORTANT?
;

expression
  :
    (term)+
;

term
  :
    number    #numberExpr
  | STRING    #stringExpr
  | IDENT     #idExpr
  | URL       #urlExpr
  | HEX_COLOR #hexColorExpr
  | calc      #calcExpr
  | function  #functionExpr
;

calc
  :
    'calc' '(' sum ')'
;

sum
  :
    product (('+' | '-') product)*
  ;

product
  :
    unit (('*' unit | '/' NUMBER))*
  ;

attributeReference
  :
    'attr' + '(' name=IDENT (type=IDENT)? (',' (unit | calc))? ')'
  ;

unit
  :
    NUMBER      #calcNumberDecl
  | '(' sum ')' #calcSumDecl
  | calc        #calcDecl
  | attributeReference #attributeReferenceDecl
  ;

function
  :
   IDENT '(' expression (',' expression)* ')'
  ;

number
  :
    NUMBER UNIT?
  ;

IMPORTANT
    :
        '!'[iI][mM][pP][oO][rR][tT][aA][nN][tT]
;

NOT
  :
    [nN][oO][tT]
;

ONLY
  :
    [oO][nN][lL][yY]
;

AND
  :
    [aA][nN][dD]
;

URL
  :
    [uU][rR][lL] '(' STRING ')'
  | [uU][rR][lL] '(' .*? ')'
;

UNIT
  :
    PERCENTAGE
  | DISTANCE
  | RESOLUTION
  | ANGLE
  | TIME
  | FREQUENCY
  ;

fragment
PERCENTAGE
  :
    '%'
  ;

fragment
DISTANCE
  :
  // relative length
    [eE][mM]
  | [eE][xX]
  | [cC][hH]
  | [rR][eE][mM]
  | [vV][wW]
  | [vV][hH]
  | [vV][mM][iI][nN]
  | [vV][mM][aA][xX]
  // absolute length
  | [cC][mM]
  | [mM][mM]
  | [iI][nN]
  | [pP][xX]
  | [pP][tT]
  | [pP][cC]
  ;

fragment
ANGLE
  :
    [dD][eE][gG]
  | [gG][rR][aA][dD]
  | [rR][aA][dD]
  | [tT][uU][rR][nN]
  ;

fragment
TIME
  :
    [sS]
  | [mM][sS]
  ;

fragment
FREQUENCY
  :
    [hH][zZ]
  | [kK][hH][zZ]
  ;

fragment
RESOLUTION
  :
    [dD][pP][iI]
  | [dD][pP][cC][mM]
  | [dD][pP][pP][xX]
  ;

NUMBER
  :
    ('+' | '-')? NUM
;

fragment
NUM
  :
    INT
  | FLOAT
;

IDENT
    :
        '-'? NMSTART NMCHAR*
;

fragment
FLOAT
  :
    DIGIT* '.' DIGIT+
;

fragment
INT
  :
    DIGIT+
  ;

fragment
NAME
  :
    NMCHAR+
;

fragment
NMSTART
  :
    [a-zA-Z_]
  | NONASCII
  | ESCAPE
;

fragment
NMCHAR
  :
    [a-zA-Z_\-]
  | DIGIT
  | NONASCII
  | ESCAPE
;

fragment
NONASCII
  :
    [\u0100-\uFFFE]
;

fragment
ESCAPE
  :
    '\\' (["\\/bfnrt] | UNICODE)
  ;

fragment
UNICODE
  :
    'u' HEX HEX HEX HEX
  ;

fragment
HEX
  :
    [a-fA-F]
  | DIGIT
  ;

fragment
DIGIT
  :
    [0-9]
  ;

STRING
  :
    '"'  (ESC | (~[\r\n]))*? '"'
  | '\'' (ESC | (~[\r\n]))*? '\''
  ;

HEX_COLOR
  :
    '#' HEX HEX HEX
  | '#' HEX HEX HEX HEX HEX HEX
  ;

fragment
ESC
  :
    '\\' ('"' | '\'' | '\n')
;

fragment
SPACE
  :
    [ \t\r\n\f]
  ;

WS
  :
    SPACE+? -> skip
;