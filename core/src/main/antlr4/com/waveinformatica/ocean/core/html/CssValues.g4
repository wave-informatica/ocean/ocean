// Define a grammar called Hello
grammar CssValues;

valuesList          : values (',' values)* EOF ;
values              : value+ IMPORTANT?;
args                : value (',' value)* ;
value               : url | percentage | string | dimension | color | function | NUMBER | IDENT ;
url                 : 'url' '(' STRING_LITERAL_S ')' | 'url' '(' STRING_LITERAL_D ')' | 'url' '(\'' STRING_LITERAL_S '\')' | 'url' '("' STRING_LITERAL_D '")' ;
percentage          : NUMBER '%' ;
dimension           : NUMBER IDENT ;
string              : '"' STRING_LITERAL_D '"' | '\'' STRING_LITERAL_S '\'' | '\'' '\'' | '"' '"' ;
color               : COLOR | 'rgb' '(' NUMBER ',' NUMBER ',' NUMBER ')' | 'rgba' '(' NUMBER ',' NUMBER ',' NUMBER ',' NUMBER ')' ;
function            : IDENT '(' args ')' | IDENT '(' ')' ;

IMPORTANT           : '!important' ;
COLOR               : '#'[0-9a-f][0-9a-f][0-9a-f]|'#'[0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f] ;
IDENT               : [-]?[_a-z][_a-z0-9-]* ;
NUMBER              : [0-9]+|[0-9]*'.'[0-9]+ ;

STRING_LITERAL_S    : (~('\'' | '\\' | '\r' | '\n') | '\\' ('\'' | '\\'))+;
STRING_LITERAL_D    : (~('"' | '\\' | '\r' | '\n') | '\\' ('"' | '\\'))+;
SP                  : [ \t\r\n]+ -> skip ;