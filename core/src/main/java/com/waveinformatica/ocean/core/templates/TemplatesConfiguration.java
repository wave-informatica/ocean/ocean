/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.templates;

import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.util.Context;
import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateNotFoundException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
public class TemplatesConfiguration extends Configuration {

	@Inject
	private com.waveinformatica.ocean.core.Configuration config;

	@Override
	public Template getTemplate(String name, Locale locale, Object customLookupCondition, String encoding, boolean parseAsFTL, boolean ignoreMissing)
		  throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException {

		String theme = config.getSiteProperty("theme", "");

		Template result = null;

		MimeTypes type = Context.getExpectedType();

		if (!name.startsWith("/")) {
			name = "/" + name;
		}

		if (type != null) {

			int pos = name.lastIndexOf('.');
			String typedName = name.substring(0, pos) + "." + type.name().toLowerCase() + name.substring(pos);

			if (StringUtils.isNotBlank(theme)) {
				String themedTypedName = "/themes/" + theme + typedName;
				String themedName = "/themes/" + theme + name;
				try {
					result = super.getTemplate(themedTypedName, locale, customLookupCondition, encoding, parseAsFTL, ignoreMissing);
				} catch (FileNotFoundException e) {
					try {
						result = super.getTemplate(themedName, locale, customLookupCondition, encoding, parseAsFTL, ignoreMissing);
					} catch (FileNotFoundException e2) {
						try {
							result = super.getTemplate(typedName, locale, customLookupCondition, encoding, parseAsFTL, ignoreMissing);
						} catch (FileNotFoundException e3) {
							result = super.getTemplate(name, locale, customLookupCondition, encoding, parseAsFTL, ignoreMissing);
						}
					}
				}
			} else {
				try {
					result = super.getTemplate(typedName, locale, customLookupCondition, encoding, parseAsFTL, ignoreMissing);
				} catch (FileNotFoundException e) {
					result = super.getTemplate(name, locale, customLookupCondition, encoding, parseAsFTL, ignoreMissing);
				}
			}

		}

		if (result == null) {
			if (StringUtils.isNotBlank(theme)) {
				String themedName = "/themes/" + theme + name;
				try {
					result = super.getTemplate(themedName, locale, customLookupCondition, encoding, parseAsFTL, ignoreMissing);
				} catch (FileNotFoundException e2) {
					result = super.getTemplate(name, locale, customLookupCondition, encoding, parseAsFTL, ignoreMissing);
				}
			} else {
				result = super.getTemplate(name, locale, customLookupCondition, encoding, parseAsFTL, ignoreMissing);
			}
		}

		return result;
	}

}
