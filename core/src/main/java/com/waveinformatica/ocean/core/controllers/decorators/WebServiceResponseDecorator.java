package com.waveinformatica.ocean.core.controllers.decorators;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.Binder;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.waveinformatica.ocean.core.annotations.ExcludeSerialization;
import com.waveinformatica.ocean.core.annotations.ResultDecorator;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.ObjectProperty;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.results.WebServiceResult;
import com.waveinformatica.ocean.core.controllers.webservice.WebServiceInvocationScope;
import com.waveinformatica.ocean.core.controllers.webservice.WebServiceScope;
import com.waveinformatica.ocean.core.controllers.webservice.WsdlBuilder;
import com.waveinformatica.ocean.core.interfaces.InputStreamProvider;
import com.waveinformatica.ocean.core.mime.OceanMultipartDataSource;
import com.waveinformatica.ocean.core.util.ObjectUpdater;
import com.waveinformatica.ocean.core.util.XmlUtils;
import com.waveinformatica.ocean.schemas.soapenv.Body;
import com.waveinformatica.ocean.schemas.soapenv.Envelope;
import com.waveinformatica.ocean.schemas.soapenv.Fault;

@ResultDecorator(classes = WebServiceResult.class, mimeTypes = MimeTypes.XML, inScope = WebServiceInvocationScope.class)
public class WebServiceResponseDecorator implements IDecorator<WebServiceResult> {
	
	public static final String NS_XOP_INCLUDE = "http://www.w3.org/2004/08/xop/include";
	public static final String NS_SOAP_ENVELOPE = "http://schemas.xmlsoap.org/soap/envelope/";
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private XmlUtils xmlUtils;
	
	@Inject
	private ObjectUpdater updater;
	
	private WsdlBuilder builder;
	
	private Envelope response;
	private final Map<String, InputStream> attachments = new HashMap<>();
	private WebServiceResult wsResult;
	
	@PostConstruct
	public void init() {
		builder = factory.newInstance(WsdlBuilder.class);
	}
	
	@Override
	public void decorate(WebServiceResult result, OutputStream out, MimeTypes type) {
		
		wsResult = result;
		
		try {
			
			buildResponse(result);
			
			write(generateCid(), out, new OceanMultipartDataSource());
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void decorate(WebServiceResult result, HttpServletRequest request, HttpServletResponse response, MimeTypes type) {
		
		wsResult = result;
		
		try {
			
			buildResponse(result);
			
			String startCid = null;
			OceanMultipartDataSource ds = null;
			
			if (attachments.isEmpty()) {
				response.setContentType("text/xop+xml");
			}
			else {
				startCid = generateCid();
				ds = new OceanMultipartDataSource();
				ds.setSubType("related");
				ds.setContentTypeParameter("type", "\"application/xop+xml\"");
				ds.setContentTypeParameter("start", "<" + startCid + ">");
				ds.setContentTypeParameter("start-info", "\"text/xml; charset=utf-8\"");
				response.setContentType(ds.getContentType());
			}
			
			write(startCid, response.getOutputStream(), ds);
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	protected String getNamespace(WebServiceResult result) {
		
		return ((WebServiceScope) result.getScope().getParent()).getNamespace();
		
	}
	
	protected void buildResponse(WebServiceResult result) throws ParserConfigurationException, JAXBException {
		
		String namespace = getNamespace(result);
		
		response = new Envelope();
		
		Body body = new Body();
		response.setBody(body);
		
		if (result.getResult() instanceof Throwable) {
			
			Fault fault = new Fault();
			fault.setFaultcode(new QName(NS_SOAP_ENVELOPE, "Server"));
			
			Throwable t = (Throwable) result.getResult();
			if (t instanceof InvocationTargetException) {
				t = t.getCause();
			}
			String message = t.getMessage();
			if (StringUtils.isNotBlank(message)) {
				message = t.getClass().getSimpleName() + ": ";
			}
			else {
				message = t.getClass().getSimpleName();
			}
			fault.setFaultstring(message);
			
			body.getAny().add(new JAXBElement<Fault>(new QName(NS_SOAP_ENVELOPE, "Fault"), Fault.class, fault));
			
		}
		else {
			
			Binder<Node> binder = xmlUtils.getContext().createBinder();
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
	        DocumentBuilder builder = dbf.newDocumentBuilder();
	        DOMImplementation domImpl = builder.getDOMImplementation();
	        Document doc = domImpl.createDocument(namespace, result.getOperationInfo().getMethod().getName() + "Response", null);
	        
	        buildElement(doc.getDocumentElement(), result.getResult(), "return");
	        
	        body.getAny().add(doc.getDocumentElement());
	        
		}
	}
	
	private void buildElement(Element parent, Object obj, String name) {
		
		if (obj == null) {
			return;
		}
		
		Class<?> cl = obj.getClass();
		
		if (cl.isArray()) {
			for (Object o : (Object[]) obj) {
				buildElement(parent, o, name);
			}
		}
		else if (Collection.class.isAssignableFrom(cl)) {
			for (Object o : (Collection) obj) {
				buildElement(parent, o, name);
			}
		}
		else if (builder.isPrimitive(cl) && !builder.isStream(cl) && !builder.isObject(cl)) {
			
			parent.setAttributeNS(getNamespace(wsResult), name, obj.toString());
			
		}
		else if (builder.isObject(cl)) {
			
		}
		else if (builder.isStream(cl)) {
			Element child = parent.getOwnerDocument().createElementNS(parent.getNamespaceURI(), name);
			parent.appendChild(child);
			
			if (InputStreamProvider.class.isAssignableFrom(cl)) {
				obj = ((InputStreamProvider) obj).getStream();
				cl = obj.getClass();
			}
			if (InputStream.class.isAssignableFrom(cl)) {
				String cid = generateCid();
				attachments.put(cid, (InputStream) obj);
				Element include = parent.getOwnerDocument().createElementNS(NS_XOP_INCLUDE, "Include");
				include.setAttribute("href", "cid:" + cid);
				child.appendChild(include);
			}
		}
		else if (Map.class.isAssignableFrom(cl)) {
			
		}
		else {
			
			Element el = parent.getOwnerDocument().createElementNS(getNamespace(wsResult), name);
			parent.appendChild(el);
			
			Map<String, ObjectProperty> props = updater.getObjectProperties(cl);
			
			for (ObjectProperty op : props.values()) {
				
				if (op.getGetter() == null) {
					continue;
				}
				
				if (op.getField().getAnnotation(Inject.class) != null ||
						op.getField().getAnnotation(ExcludeSerialization.class) != null) {
					continue;
				}
				
				Object value = updater.read(obj, op.getField().getName());
				
				if (value != null) {
					buildElement(el, value, op.getField().getName());
				}
				
			}
			
		}
		
	}
	
	private String generateCid() {
		return StringUtils.leftPad(String.valueOf(attachments.size() + 1), 10, '0') + "-" + System.currentTimeMillis();
	}
	
	private void write(String startCid, OutputStream out, OceanMultipartDataSource ds) throws JAXBException, MessagingException, IOException {
		
		Marshaller marshaller = xmlUtils.getContext().createMarshaller();
		
		if (attachments.isEmpty()) {
			
			marshaller.marshal(new JAXBElement<Envelope>(new QName(NS_SOAP_ENVELOPE, "Envelope"), Envelope.class, response), out);
			
		}
		else {
			
			BodyPart bp = new MimeBodyPart();
			StringWriter writer = new StringWriter();
			marshaller.marshal(new JAXBElement<Envelope>(new QName(NS_SOAP_ENVELOPE, "Envelope"), Envelope.class, response), writer);
			bp.setText(writer.toString());
			bp.setHeader("Content-Type", "application/xop+xml; charset=utf-8; type=\"application/soap+xml;\"");
			bp.setHeader("Content-Transfer-Encoding", "binary");
			bp.setHeader("Content-id", "<" + startCid + ">");
			
			ds.addBodyPart(bp);
			
			for (Map.Entry<String, InputStream> entry : attachments.entrySet()) {
				
				BodyPart abp = new MimeBodyPart();
				abp.addHeader("Content-Type", "application/octet-stream");
				abp.addHeader("Content-Transfer-Encoding", "binary");
				abp.addHeader("Content-Id", "<" + entry.getKey() + ">");
				abp.setDataHandler(new DataHandler(new InputStreamDataSource("application/octet-stream", entry.getValue(), entry.getKey())));
				ds.addBodyPart(abp);
				
			}
			
			MimeMultipart mm = new MimeMultipart(ds);
			mm.writeTo(out);
			
		}
		
	}
	
	public static class InputStreamDataSource implements DataSource {
		
		private final String contentType;
		private final InputStream inputStream;
		private final String name;
		
		public InputStreamDataSource(String contentType, InputStream inputStream, String name) {
			this.contentType = contentType;
			this.inputStream = inputStream;
			this.name = name;
		}
		
		@Override
		public String getContentType() {
			return contentType;
		}

		@Override
		public InputStream getInputStream() throws IOException {
			return inputStream;
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public OutputStream getOutputStream() throws IOException {
			return null;
		}
		
	}
	
}
