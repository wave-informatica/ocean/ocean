/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers;

import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.annotations.EventListener;
import com.waveinformatica.ocean.core.controllers.events.Event;

/**
 * This interface represents a EventListener. All the event listeners must implement
 * this interface. Event listeners must be annotated with one of {@link CoreEventListener}
 * or {@link EventListener} too.
 * 
 * @param <T> Event's type
 * 
 * @author Ivano
 */
public interface IEventListener<T extends Event> {
    
    /**
     * This method is invoked when a event matches the current EventListener.
     * 
     * @param event the event to be processed
     */
    public void performAction(T event);
    
}
