/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.controllers.decorators;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.annotations.ResultDecorator;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.events.AfterResultEvent;
import com.waveinformatica.ocean.core.controllers.events.BeforeOperationEvent;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.results.RedirectResult;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.OceanSession;
import com.waveinformatica.ocean.core.util.UrlBuilder;

/**
 *
 * @author Ivano
 */
@ResultDecorator(classes = RedirectResult.class, mimeTypes = MimeTypes.HTML)
public class WebRedirectDecorator implements IDecorator {
	
	@Inject
	private OceanSession session;
	
	@Inject
	private Configuration configuration;
	
	@Override
	public void decorate(Object result, OutputStream out, MimeTypes type) {
		
	}
	
	@Override
	public void decorate(Object result, HttpServletRequest request, HttpServletResponse response, MimeTypes type) {
		
		RedirectResult res = (RedirectResult) result;
		
		if (res.getExtras() != null && !res.getExtras().isEmpty()) {
			UUID id = UUID.randomUUID();
			int pos = res.getRedirect().indexOf('?');
			try {
				session.put("redirect-data-" + id, (Serializable) res.getExtras());
				
				UrlBuilder pBuilder = new UrlBuilder(res.getRedirect());
				pBuilder.getParameters().replace("fw:redirect-data", URLEncoder.encode(id.toString(), "UTF-8"));
				res = new RedirectResult(pBuilder.getUrl(), res.getCode());
				
			} catch (UnsupportedEncodingException ex) {
				Logger.getLogger(WebRedirectDecorator.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		
		try {
			
			URL url = new URL(res.getRedirect());
			
			response.setStatus(res.getCode());
			response.setHeader("Location", url.toString());
			
		} catch (MalformedURLException e) {
			
			// La tilde sara' sostituita con il context path dell'applicazione
			if (! res.getRedirect().isEmpty() && res.getRedirect().charAt(0) == '~') {
				StringBuilder builder = new StringBuilder();
				builder.append(request.getContextPath());
				if ((res.getRedirect().length() > 1 && res.getRedirect().charAt(1) != '/') ||
					  (res.getRedirect().length() == 1 && res.getRedirect().charAt(0) == '~')) {
					// Il secondo if aggiunge lo slash anche se il redirect e' stato effettuato
					// semplicemente verso '~'
					builder.append('/');
				}
				builder.append(res.getRedirect().substring(1));
				res = new RedirectResult(builder.toString(), res.getCode());
			}

			response.setStatus(res.getCode());
			response.setHeader("Location", absolutize(res.getRedirect()));
			
		} catch (IOException ex) {
			Logger.getLogger(WebRedirectDecorator.class.getName()).log(Level.SEVERE, null, ex);
		}
		
	}
	
	private String absolutize(String url) {
		String base = configuration.getSiteProperty("site.url", "http://localhost:8080/");
		String context = Context.get().getRequest().getContextPath();
		if (base.endsWith("/")) {
			base = base.substring(0, base.length() - 1);
		}
		if (url.startsWith(context)) {
			url = url.substring(context.length());
		}
		if (!base.endsWith(context)) {
			base = base + context;
		}
		if (!url.startsWith("/")) {
			url = "/" + url;
		}
		return base + url;
	}
	
	@CoreEventListener(eventNames = CoreEventName.AFTER_RESULT)
	public static class RedirectDataCleaner implements IEventListener<AfterResultEvent> {
		
		@Inject
		private OceanSession session;
		
		@Override
		public void performAction(AfterResultEvent evt) {
			
			String redirectDataId = evt.getRequest().getParameter("fw:redirect-data");
			
			if (redirectDataId != null) {
				session.remove("redirect-data-" + redirectDataId);
			}
			
		}
		
	}
	
	@CoreEventListener(eventNames = CoreEventName.BEFORE_OPERATION)
	public static class RedirectDataSupplier implements IEventListener<BeforeOperationEvent> {
		
		@Inject
		private OceanSession session;

		@Override
		public void performAction(BeforeOperationEvent evt) {
			
			try {
				String[] redirectDataId = evt.getArgs().get("fw:redirect-data");
				
				if (redirectDataId != null && redirectDataId.length >= 1) {
					String id = redirectDataId[0];
					Map<String, Object> map = (Map<String, Object>) session.get("redirect-data-" + id);
					RedirectResult result = new RedirectResult(null);
					if (map != null) {
						result.getExtras().putAll(map);
					}
					evt.getExtraObjects().add(result);
				}
			} catch (Exception e) {
				// Ignore errors
			}
			
		}
		
	}
	
}
