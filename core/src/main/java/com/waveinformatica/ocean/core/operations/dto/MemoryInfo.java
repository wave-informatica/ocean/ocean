/*
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.operations.dto;

import java.lang.management.MemoryPoolMXBean;

/**
 *
 * @author Ivano
 */
public class MemoryInfo {
    
    private final String name;
    private final String type;
    private final long usage;
    private final long peak;
    private final long collections;
    
    public MemoryInfo(MemoryPoolMXBean item) {
        name = item.getName();
        type = item.getType().name();
        usage = item.getUsage() != null ? item.getUsage().getUsed() : -1;
        peak = item.getPeakUsage() != null ? item.getPeakUsage().getUsed() : -1;
        collections = item.getCollectionUsage() != null ? item.getCollectionUsage().getUsed() : -1;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public long getUsage() {
        return usage;
    }

    public long getPeak() {
        return peak;
    }

    public long getCollections() {
        return collections;
    }
    
}
