/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.modules;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.Event;
import com.waveinformatica.ocean.core.controllers.events.ModuleEvent;

/**
 * This class implements a Class based information cache, to avoid as much as possible the use of
 * reflection, for performance. When a module is unloaded all information about module's classes
 * are cleared out.
 * 
 * @author Ivano
 */
@ApplicationScoped
public class ClassInfoCache {
    
    private final Map<Class, Map<String, Object>> cache = new WeakHashMap<Class, Map<String, Object>>();
    
    /**
     * This method returns information from the cache and, optionally, uses the provided
     * factory to create missing elements.
     * 
     * @param cls the class used for cleaning on module unload
     * @param name the key for required information
     * @param factory a factory to produce a element on cache miss
     * @return the cached object or <code>null</code> if missing and no factory is provided
     */
    public Object get(Class cls, String name, Factory factory) {
	Object value = getMap(cls).get(name);
	if (value == null && factory != null) {
	    value = factory.create();
	    getMap(cls).put(name, value);
	}
	return value;
    }
    
    /**
     * This method puts information into the cache.
     * 
     * @param cls the class used for cleaning on module unload
     * @param name the key for information
     * @param value the object to be stored
     * @return the existing element or <code>null</code> if it is a new element
     */
    public Object put(Class cls, String name, Object value) {
	return getMap(cls).put(name, value);
    }
    
    /**
     * Removes a element from the cache
     * 
     * @param cls the class used for cleaning on module unload
     * @param name the key for the object to remove
     * @return  the existing element or <code>null</code> if it is a new element
     */
    public Object remove(Class cls, String name) {
	return getMap(cls).remove(name);
    }
    
    /**
     * This interface is used to produce missing items on the fly when getting
     * from the cache.
     */
    public static interface Factory {
	/**
	 * Implement this method to produce a missing item on the fly instead of
	 * returning <code>null</code> from the <code>get</code> method of the cache.
	 * @return the new object to be stored into the cache
	 */
	public Object create();
    }
    
    private Map<String, Object> getMap(Class cls) {
	Map<String, Object> map = cache.get(cls);
	if (map == null) {
	    map = new HashMap<String, Object>();
	    cache.put(cls, map);
	}
	return map;
    }
    
    private void clearByClassLoader(ModuleClassLoader classLoader) {
	Iterator<Class> iter = cache.keySet().iterator();
	while (iter.hasNext()) {
	    Class cls = iter.next();
	    if (cls.getClassLoader() == classLoader) {
		iter.remove();
	    }
	}
    }
    
    @CoreEventListener(eventNames = CoreEventName.MODULE_UNLOADED)
    public static class ClassInfoCacheCleaner implements IEventListener {
	
	@Inject
	private ClassInfoCache cache;
	
	@Inject
	private ModulesManager manager;
	
	@Override
	public void performAction(Event event) {
	    Module module = manager.getModule(((ModuleEvent) event).getModuleName());
	    if (module != null) {
		cache.clearByClassLoader(module.getModuleClassLoader());
	    }
	}
	
    }
}
