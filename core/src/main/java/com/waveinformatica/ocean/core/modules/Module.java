/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.modules;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import javax.persistence.EntityManagerFactory;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.Constants;
import com.waveinformatica.ocean.core.exceptions.ModuleException;
import com.waveinformatica.ocean.core.util.PersistedProperties;

/**
 *
 * @author Ivano
 */
public class Module {
	
	private final ModulesManager manager;
	private final String name;
	private final File moduleJar;
	private ModuleClassLoader moduleClassLoader;
	private boolean loaded = false;
	private final Map<String, EntityManagerFactory> moduleEntityManagerFactories = new HashMap<String, EntityManagerFactory>();
	private Logger logger;
	private List<LogRecord> logBuffer = new LinkedList<LogRecord>();
	private final Map<Class<? extends Annotation>, Set<Class<?>>> markedComponents = new HashMap<Class<? extends Annotation>, Set<Class<?>>>();
	
	private String version;
	private String build;
	
	Module(ModulesManager manager, File moduleJar) throws IOException {
		this.manager = manager;
		this.name = moduleJar.getName().substring(0, moduleJar.getName().indexOf(".jar"));
		this.moduleJar = moduleJar;
		
		// Module logging
		String loggerName = name;
		if (!loggerName.startsWith(Constants.CORE_LOGGER_NAME)) {
			loggerName = Constants.CORE_LOGGER_NAME + "." + loggerName;
		}
		logger = Logger.getLogger(Application.getInstance().getNamespace() + "." + loggerName);
		boolean managedLogger = false;
		for (Handler h : logger.getHandlers()) {
			if (h instanceof ModuleLogHandler) {
				managedLogger = true;
				break;
			}
		}
		if (!managedLogger) {
			PersistedProperties props = Application.getInstance().getConfiguration().getLoggingProperties();
			String level = props.getProperty(name + ".level");
			if (level != null) {
				logger.setLevel(Level.parse(level));
			}
			else {
				logger.setLevel(Level.INFO);
			}
			boolean found = false;
			for (Handler h : logger.getHandlers()) {
				if (h instanceof ModuleLogHandler) {
					found = true;
					((ModuleLogHandler) h).setRecords(logBuffer);
					break;
				}
			}
			if (!found) {
				logger.addHandler(new ModuleLogHandler(logBuffer));
			}
		}
		
		// Initialization
		moduleClassLoader = new ModuleClassLoader(this, moduleJar);
		loaded = false;
		version = moduleClassLoader.getModuleAttributes().getVersion();
		build = moduleClassLoader.getModuleAttributes().getBuild();
	}
	
	void load() throws ModuleException {
		
		try {
			// Starting loading transaction
			markedComponents.clear();
			ModuleTransaction mt = Application.getInstance().getObjectFactory().newInstance(ModuleTransaction.class);
			checkDependencies(manager);
			mt.loadComponentsCatalog(name, moduleClassLoader, moduleClassLoader.getAnnotatedClassesMap());
			mt.loadComponents(moduleClassLoader, moduleClassLoader.getAnnotatedClassesMap());
			mt.checkPersistenceAvailability(moduleClassLoader.getAnnotatedClassesMap());
			mt.loadPersistenceUnit(this);
			mt.commit(this);
			markedComponents.putAll(mt.getComponents());
	
			moduleClassLoader.cleanGarbage();
		} catch (Error e) {
			throw new ModuleException(e);
		}
		
	}
	
	public void clearEntityManagerFactories() {
		
		Iterator<Map.Entry<String, EntityManagerFactory>> iter = moduleEntityManagerFactories.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry<String, EntityManagerFactory> entry = iter.next();
			if (entry.getValue().isOpen()) {
				entry.getValue().close();
			}
			iter.remove();
		}
		
	}
	
	void unload (boolean delete) {
		
		ModuleClassLoader tmp = moduleClassLoader;
		moduleClassLoader = null;
		if (tmp != null) {
			tmp.close();
		}
		
		clearEntityManagerFactories();
		
		Handler removeLoggerHandler = null;
		for (Handler h : logger.getHandlers()) {
			if (h instanceof ModuleLogHandler) {
				removeLoggerHandler = h;
				break;
			}
		}
		if (removeLoggerHandler != null) {
			logger.removeHandler(removeLoggerHandler);
		}
		
		if (delete) {
			moduleJar.delete();
			if (moduleJar.exists()) {
				moduleJar.deleteOnExit();
			}
		}
		
		setLoaded(false);
	}
	
	public String getName() {
		return name;
	}
	
	public ModuleClassLoader getModuleClassLoader() {
		return moduleClassLoader;
	}
	
	public boolean isLoaded() {
		return loaded;
	}
	
	public EntityManagerFactory getModuleEntityManagerFactory(String name) {
		return moduleEntityManagerFactories.get(name);
	}
	
	public void setModuleEntityManagerFactory(String name, EntityManagerFactory moduleEntityManagerFactory) {
		EntityManagerFactory old = this.moduleEntityManagerFactories.put(name, moduleEntityManagerFactory);
		if (old != null) {
			old.close();
		}
	}
	
	public Set<String> getDependencies() {
		return moduleClassLoader.getDependencies();
	}
	
	public String getVersion() {
		return version;
	}
	
	public String getBuild() {
		return build;
	}
	
	final ModulesManager getModulesManager() {
		return manager;
	}
	
	void setLoaded(boolean loaded) {
		logger.log(Level.INFO, "Module successfully " + (loaded ? "loaded" : "unloaded"));
		this.loaded = loaded;
	}
	
	public List<LogRecord> getLogRecords() {
		return logBuffer;
	}
	
	public Logger getLogger() {
		return logger;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	/**
	 * Returns an unmodifiable map of set of components classes per annotation
	 * 
	 * @return the map of components per annotation
	 */
	public Map<Class<? extends Annotation>, Set<Class<?>>> getMarkedComponents() {
		Map<Class<? extends Annotation>, Set<Class<?>>> result = new HashMap<>();
		for (Entry<Class<? extends Annotation>, Set<Class<?>>> e : markedComponents.entrySet()) {
			result.put(e.getKey(), Collections.unmodifiableSet(e.getValue()));
		}
		return Collections.unmodifiableMap(result);
	}

	private void checkDependencies(ModulesManager manager) throws ModuleException {
		for (String dep : getDependencies()) {
            Module dependency = manager.getModule(dep);
            if (dependency == null || !dependency.isLoaded()) {
                throw new ModuleException("Missing dependency \"" + dep + "\"");
            }
        }
	}
	
	void checkDependencies(ModulesManager manager, Set<String> missing) {
		for (String dep : getDependencies()) {
            Module dependency = manager.getModule(dep);
            if (dependency == null || !dependency.isLoaded()) {
                missing.add(dep);
            }
        }
	}
	
}
