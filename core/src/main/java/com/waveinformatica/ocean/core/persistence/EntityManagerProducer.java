/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */
package com.waveinformatica.ocean.core.persistence;

import com.waveinformatica.ocean.core.Constants;
import com.waveinformatica.ocean.core.annotations.OceanPersistenceContext;
import com.waveinformatica.ocean.core.annotations.OceanPersistenceUnit;
import com.waveinformatica.ocean.core.controllers.InstanceListener;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.modules.Module;
import com.waveinformatica.ocean.core.modules.ModuleClassLoader;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.RequestCache;
import java.util.logging.Level;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Ivano
 */
@ApplicationScoped
public class EntityManagerProducer {

	@Inject
	private ObjectFactory factory;
	
	@Inject
	private PersistenceManager manager;

	@Produces
	@OceanPersistenceContext
	@Dependent
	public EntityManager obtainEntityManager(InjectionPoint point) {

		EntityManager entityManager = null;

		RequestCache cache = factory.getBean(RequestCache.class);
		
		OceanPersistenceUnit ctxAnnotation = point.getAnnotated().getAnnotation(OceanPersistenceUnit.class);
		final String persistenceUnit = ctxAnnotation != null && ctxAnnotation.value().length == 1 ? ctxAnnotation.value()[0] : "";

		ClassLoader loader = point.getMember().getDeclaringClass().getClassLoader();
		if (loader instanceof ModuleClassLoader) {
			final Module module = ((ModuleClassLoader) loader).getModule();
			String key = module.getName() + "$EntityManager:" + persistenceUnit;
			if (cache.get(key, null) == null || !((EntityManager) cache.get(key, null)).isOpen()) {
				cache.remove(key);
			}
			entityManager = (EntityManager) cache.get(key, new RequestCache.Factory() {
				@Override
				public Object create() {
					EntityManagerFactory emFactory = module.getModuleEntityManagerFactory(persistenceUnit);
					if (emFactory == null) {
						module.getLogger().log(Level.WARNING, "The module \"" + module.getName() + "\" is trying to get injected a EntityManager instance without defining entities");
						return null;
					}
					EntityManager entityManager = emFactory.createEntityManager();
					return entityManager;
				}
			});
		} else if (manager.getCoreEntityManagerFactory(persistenceUnit) != null) {
			String key = Constants.CORE_MODULE_NAME + "$EntityManager:" + persistenceUnit;
			if (cache.get(key, null) == null || !((EntityManager) cache.get(key, null)).isOpen()) {
				cache.remove(key);
			}
			entityManager = (EntityManager) cache.get(key, new RequestCache.Factory() {
				@Override
				public Object create() {
					EntityManagerFactory emFactory = manager.getCoreEntityManagerFactory(persistenceUnit);
					if (emFactory == null) {
						LoggerUtils.getCoreLogger().log(Level.WARNING, "The application is trying to get injected a EntityManager instance without defining entities");
						return null;
					}
					return emFactory.createEntityManager();
				}
			});
		}
		if (entityManager != null) {
			InstanceListener.get().getEntityManagers().add(entityManager);
		}
		return entityManager;
	}

	public void closeEntityManager(@Disposes @OceanPersistenceContext EntityManager em) {
		if (em.isOpen()) {
			em.close();
		}
	}

}
