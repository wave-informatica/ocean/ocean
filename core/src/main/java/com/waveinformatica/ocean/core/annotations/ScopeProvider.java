/*
 * Copyright 2017, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.annotations;

import com.waveinformatica.ocean.core.controllers.scopes.IScopeProvider;
import com.waveinformatica.ocean.core.controllers.scopes.RootScope;
import com.waveinformatica.ocean.core.controllers.scopes.ScopesController;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation allows to define a {@link IScopeProvider} which you can implement
 * to provide scopes to the current operation. Scopes allow to authorize operations,
 * execute operations on a specific subset of data, plug components and operations
 * in specific paths and other components. This new feature is studied to provide a
 * brand new flexibility to Ocean.
 * 
 * @author ivano
 * @since 2.2.0
 */
@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@OceanComponent(requiresBean = ScopesController.class, mustExtend = IScopeProvider.class)
public @interface ScopeProvider {
	
	/**
	 * Filter by parent scope. The annotated {@code IScopeProvider} is invoked only when
	 * the parent scope is of the type specified in this attribute, or extends it.
	 * 
	 * @return the type of the parent scope
	 */
	Class<?>[] inScope() default RootScope.class;
	
	/**
	 * Filter by path. The annotated {@code IScopeProvider} is invoked only when the remaining
	 * part of the URL path starts with the spcified value.
	 * 
	 * @return the starting path to match URLs
	 */
	String startsWith() default "";
	
	/**
	 * Execution order (reverse). The {@code IScopeProvider} instances are executed in reverse order for
	 * each path evaluation: higher values are executed before lower values.
	 * 
	 * @return the execution order
	 */
	int order() default 0;
	
}
