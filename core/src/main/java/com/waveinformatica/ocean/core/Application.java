/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.modules.ModulesManager;

/**
 *
 * @author Ivano
 */
@Named
@ApplicationScoped
public class Application {
	
	private static Application instance;

	public static Application getInstance() {
		return instance;
	}
	
	@Inject
	private CoreController coreController;
	
	@Inject
	private ObjectFactory objectFactory;
	
	@Inject
	private Configuration configuration;
	
	@Inject
	private ModulesManager modulesManager;
	
	private ServletContext context;
	private String namespace;
	
	@PostConstruct
	public void postConstruct() {
		instance = this;
	}

	public ServletContext getContext() {
		return context;
	}

	public void init(ServletContext context, String appName) {
		this.context = context;
		
		if (context != null) {
			namespace = appName;
		}
		else {
			namespace = "test";	// For testing
		}
		if (namespace.startsWith("/")) {
			namespace = namespace.substring(1);
		}
		if (StringUtils.isBlank(namespace)) {
			namespace = "ROOT";
		}
		else {
			namespace = namespace.replaceAll("/", ".");
		}
	}

	public CoreController getCoreController() {
		return coreController;
	}

	public ObjectFactory getObjectFactory() {
		return objectFactory;
	}

	public String getNamespace() {
		return namespace;
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public ModulesManager getModulesManager() {
		return modulesManager;
	}
	
	public String getBaseUrl() {
		String baseUrl = configuration.getSiteProperty("site.contextPath", "");
		if (StringUtils.isBlank(baseUrl)) {
			if (context != null) {
				return StringUtils.defaultIfBlank(context.getContextPath(), "");
			}
			else {
				return "";
			}
		}
		else {
			if (baseUrl.endsWith("/")) {
				baseUrl = baseUrl.substring(0, baseUrl.length() - 1);
			}
			return baseUrl;
		}
	}
	
}
