/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.dto;

import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

/**
 *
 * @author Ivano
 */
public class ResourceInfo {

	private URL url;
	private Date lastModified;
	private InputStream inputStream;
	private String name;
	private MimeTypes mimeType;
	private Long length;
	private ClassLoader sourceClassLoader;

	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MimeTypes getMimeType() {
		return mimeType;
	}

	public void setMimeType(MimeTypes mimeType) {
		this.mimeType = mimeType;
	}

	public Long getLength() {
		return length;
	}

	public void setLength(Long length) {
		this.length = length;
	}

	public ClassLoader getSourceClassLoader() {
		return sourceClassLoader;
	}

	public void setSourceClassLoader(ClassLoader sourceClassLoader) {
		this.sourceClassLoader = sourceClassLoader;
	}

}
