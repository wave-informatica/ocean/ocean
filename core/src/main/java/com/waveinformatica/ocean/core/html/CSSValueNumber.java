/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.html;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
public class CSSValueNumber extends CSSValue {
	
	private double value;
	private String unit;
	
	public CSSValueNumber(double v) {
		this.value = v;
	}
	
	public CSSValueNumber(double v, String unit) {
		this.value = v;
		this.unit = unit;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public String toString() {
		
		StringBuilder builder = new StringBuilder();
		
		builder.append(value);
		if (StringUtils.isNotBlank(unit)) {
			builder.append(unit.trim());
		}
		
		return builder.toString();
		
	}
	
}
