/*
* Copyright 2017, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */
package com.waveinformatica.ocean.core.internal;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Image;
import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.controllers.dto.ResourceInfo;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.ResourceRetriever;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import org.apache.commons.lang.StringUtils;
import org.xhtmlrenderer.pdf.ITextFSImage;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.pdf.ITextUserAgent;
import org.xhtmlrenderer.resource.ImageResource;
import org.xhtmlrenderer.swing.Java2DRenderer;
import org.xhtmlrenderer.util.XRLog;

/**
 *
 * @author Ivano
 */
public class OceanUserAgentCallback extends ITextUserAgent {

	private final MimeTypes mimeType;

	public OceanUserAgentCallback(ITextRenderer renderer) {
		super(renderer.getOutputDevice());
		setSharedContext(renderer.getSharedContext());
		mimeType = MimeTypes.PDF;
	}

	public OceanUserAgentCallback(Java2DRenderer renderer) {
		super(null);
		setSharedContext(renderer.getSharedContext());
		mimeType = MimeTypes.PNG;
	}
	
	@Override
	protected ImageResource createImageResource(String uri, java.awt.Image img) {
		return super.createImageResource(uri, img);
	}
	
	@Override
	public String resolveURI(String uri) {

		if (StringUtils.isNotBlank(getBaseURL()) && uri.startsWith(getBaseURL())) {
			uri = uri.substring(getBaseURL().length());
		}
		if (uri.startsWith("/")) {
			uri = uri.substring(1);
		}

		return uri;
	}

	@Override
	protected InputStream resolveAndOpenStream(String uri) {

		String oceanUri = resolveURI(uri);

		LoggerUtils.getCoreLogger().log(Level.INFO, "Resolving and opening stream for URI " + uri);

		ResourceRetriever retriever = Application.getInstance().getObjectFactory().getBean(ResourceRetriever.class);

		ResourceInfo result = retriever.getResource(oceanUri);

		if (result != null) {
			return result.getInputStream();
		} else {
			return super.resolveAndOpenStream(uri);
		}
	}

	@Override
	public byte[] getBinaryResource(String uri) {

		InputStream is = resolveAndOpenStream(uri);
		if (is == null) {
			throw new RuntimeException("Unable to find " + uri);
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		byte[] buffer = new byte[4096];
		int read;
		try {
			while ((read = is.read(buffer)) > 0) {
				baos.write(buffer, 0, read);
			}
		} catch (IOException ex) {
			LoggerUtils.getLogger(OceanUserAgentCallback.class).log(Level.SEVERE, null, ex);
		} finally {
			try {
				is.close();
			} catch (IOException ex) {
				LoggerUtils.getLogger(OceanUserAgentCallback.class).log(Level.WARNING, null, ex);
			}
		}

		return baos.toByteArray();
	}

	@Override
	public ImageResource getImageResource(String uri) {
		ImageResource resource = null;
		uri = resolveURI(uri);
		InputStream is = resolveAndOpenStream(uri);
		if (is == null) {
			return null;
		}
		byte[] bytes = new byte[4096];
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			int read = 0;
			while ((read = is.read(bytes)) > 0) {
				baos.write(bytes, 0, read);
			}
			bytes = baos.toByteArray();
			LoggerUtils.getLogger(OceanUserAgentCallback.class).log(Level.INFO, "Got " + bytes.length + " bytes");
		} catch (IOException ex) {
			LoggerUtils.getLogger(OceanUserAgentCallback.class).log(Level.SEVERE, null, ex);
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				// ignore
			}
		}
		if (is != null) {
			if (mimeType != MimeTypes.PDF) {
				try {
					BufferedImage img = ImageIO.read(new ByteArrayInputStream(bytes));
					if (img == null) {
						XRLog.exception("Can't read image file; unexpected problem for URI '" + uri + "'");
					}
					resource = createImageResource(uri, img);
				} catch (IOException e) {
					XRLog.exception("Can't read image file; unexpected problem for URI '" + uri + "'", e);
				}
			} else {
				try {
					Image image = Image.getInstance(bytes);
					scaleToOutputResolution(image);
					resource = new ImageResource(uri, new ITextFSImage(image));
				} catch (IOException e) {
					XRLog.exception("Can't read image file; unexpected problem for URI '" + uri + "'", e);
				} catch (BadElementException e) {
					XRLog.exception("Can't read image file; unexpected problem for URI '" + uri + "'", e);
				}
			}
		}
		if (resource == null) {
			resource = new ImageResource(uri, null);
		}
		return resource;
	}

	private void scaleToOutputResolution(Image image) {
		float factor = getSharedContext().getDotsPerPixel();
		image.scaleAbsolute(image.getPlainWidth() * factor, image.getPlainHeight() * factor);
	}

}
