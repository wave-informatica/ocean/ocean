/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.internal;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.ExcludeSerialization;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.PersistedProperties;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ivano
 */
@ApplicationScoped
public class PersistenceConfigurationManager {
	
	private static final String DEFAULT_PROPERTY_NAME = "ocean.persistence.defaultConnection";
	
	@Inject
	private Configuration config;
	
	@Inject
	private Event<PersistenceConfigurationManager> storeEvent;
	
	private Element defaultConnection;
	private final List<Element> elements = new ArrayList<Element>();
	
	@PostConstruct
	public synchronized void init() {
		
		Gson gson = JsonUtils.getBuilder().create();
		
		PersistedProperties props = config.getPersistenceProperties();
		
		try {
			for (String k : props.stringPropertyNames()) {
				if (!k.equals(DEFAULT_PROPERTY_NAME)) {
					Element el = gson.fromJson(props.getProperty(k), Element.class);
					el.setConnectionName(k);
					elements.add(el);
				}
			}
		} catch (IllegalStateException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, "Invalid stored persistence properties format", e);
		} catch (JsonSyntaxException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, "Invalid stored persistence properties format", e);
		}
		
		String defaultName = props.getProperty(DEFAULT_PROPERTY_NAME);
		if (StringUtils.isNotBlank(defaultName)) {
			for (Element el : elements) {
				if (el.getConnectionName().equals(defaultName)) {
					defaultConnection = el;
					break;
				}
			}
		}
		if (defaultConnection == null && elements.size() == 1) {
			defaultConnection = elements.get(0);
			try {
				store();
			} catch (IOException ex) {
				LoggerUtils.getLogger(PersistenceConfigurationManager.class).log(Level.WARNING, null, ex);
			}
		}
		
	}
	
	public synchronized void store() throws IOException {
		
		Gson gson = JsonUtils.getBuilder().create();
		
		PersistedProperties props = config.getPersistenceProperties();
		
		props.clear();
		
		if (defaultConnection != null) {
			props.setProperty(DEFAULT_PROPERTY_NAME, defaultConnection.getConnectionName());
		}
		
		for (Element el : elements) {
			props.setProperty(el.getConnectionName(), gson.toJson(el));
		}
		
		props.store("");
		
		storeEvent.fire(this);
		
	}

	public synchronized Element getDefaultConnection() {
		return defaultConnection;
	}

	public void setDefaultConnection(Element defaultConnection) {
		this.defaultConnection = defaultConnection;
	}
	
	public synchronized Element getConnection(String connectionName) {
		for (Element el : elements) {
			if (el.getConnectionName().equals(connectionName)) {
				return el;
			}
		}
		return null;
	}
	
	public synchronized Element deleteConnection(String connectionName) {
		Iterator<Element> iter = elements.iterator();
		while (iter.hasNext()) {
			Element el = iter.next();
			if (el.getConnectionName().equals(connectionName)) {
				iter.remove();
				return el;
			}
		}
		return null;
	}

	public synchronized List<Element> getConnections() {
		return Collections.unmodifiableList(new ArrayList<Element>(elements));
	}
	
	public synchronized Element createConnection(String connectionName, ConnectionType type, String jndiName, String driverClass,
		  String connectionUrl, String userName, String password) {
		
		if (StringUtils.isBlank(connectionName)) {
			throw new IllegalArgumentException("Invalid empty connectionName");
		}
		
		if (connectionName.equals(DEFAULT_PROPERTY_NAME)) {
			throw new IllegalArgumentException("Creating a connection with disallowed name \"" + connectionName + "\"");
		}
		
		if (type == null) {
			throw new NullPointerException("A ConnectionType must be specified");
		}
		
		if (type == ConnectionType.JNDI && StringUtils.isBlank(jndiName)) {
			throw new IllegalArgumentException("Invalid JNDI name specified for connection type JNDI");
		}
		else if (type == ConnectionType.JDBC) {
			if (StringUtils.isBlank(driverClass)) {
				throw new IllegalArgumentException("Invalid driver class specified for connection type JDBC");
			}
			if (StringUtils.isBlank(connectionUrl)) {
				throw new IllegalArgumentException("Invalid dconnection URL specified for connection type JDBC");
			}
		}
		
		for (Element el : elements) {
			if (el.getConnectionName().equals(connectionName)) {
				throw new IllegalArgumentException("Connection name " + connectionName + " already defined");
			}
		}
		
		Element el = new Element(connectionName, type, jndiName, driverClass, connectionUrl, userName, password);
		elements.add(el);
		
		if (elements.size() == 1) {
			defaultConnection = el;
		}
		
		return el;
		
	}
	
	public static class Element {
		
		@ExcludeSerialization
		private String connectionName;
		
		private ConnectionType type;
		private String jndiName;
		private String driverClass;
		private String connectionUrl;
		private String userName;
		private String password;
		private Properties properties;

		public Element() {
			properties = new Properties();
		}

		public Element(String connectionName, ConnectionType type, String jndiName, String driverClass, String connectionUrl, String userName, String password) {
			this();
			this.connectionName = connectionName;
			this.type = type;
			this.jndiName = jndiName;
			this.driverClass = driverClass;
			this.connectionUrl = connectionUrl;
			this.userName = userName;
			this.password = password;
		}

		public String getConnectionName() {
			return connectionName;
		}

		public void setConnectionName(String connectionName) {
			this.connectionName = connectionName;
		}

		public ConnectionType getType() {
			return type;
		}

		public void setType(ConnectionType type) {
			this.type = type;
		}

		public String getJndiName() {
			return jndiName;
		}

		public void setJndiName(String jndiName) {
			this.jndiName = jndiName;
		}

		public String getDriverClass() {
			return driverClass;
		}

		public void setDriverClass(String driverClass) {
			this.driverClass = driverClass;
		}

		public String getConnectionUrl() {
			return connectionUrl;
		}

		public void setConnectionUrl(String connectionUrl) {
			this.connectionUrl = connectionUrl;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public Properties getProperties() {
			return properties;
		}

		public void setProperties(Properties properties) {
			this.properties = properties;
		}
		
	}
	
	public static enum ConnectionType {
		JNDI,
		JDBC
	}
	
}
