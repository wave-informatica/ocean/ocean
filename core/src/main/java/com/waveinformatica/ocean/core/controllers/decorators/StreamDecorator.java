/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.decorators;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.ResultDecorator;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.results.StreamResult;

/**
 *
 * @author Ivano
 */
@ResultDecorator(
		classes = StreamResult.class,
		mimeTypes = {MimeTypes.HTML, MimeTypes.JSON, MimeTypes.CSS, MimeTypes.CSV, MimeTypes.EXCEL, MimeTypes.GIF,
				MimeTypes.JAVASCRIPT, MimeTypes.JPEG, MimeTypes.PDF, MimeTypes.PNG, MimeTypes.RTF, MimeTypes.TEXT,
				MimeTypes.A3GP, MimeTypes.A3GP2, MimeTypes.AAC, MimeTypes.ABIWORD, MimeTypes.AC3, MimeTypes.ACCESS_MDB,
				MimeTypes.AMR, MimeTypes.ARCHIVE_7Z, MimeTypes.AVI, MimeTypes.AZW, MimeTypes.BMP, MimeTypes.BZIP,
				MimeTypes.BZIP2, MimeTypes.CSH, MimeTypes.EML, MimeTypes.EOT, MimeTypes.EPUB, MimeTypes.EXCEL_XLS,
				MimeTypes.FLASHVIDEO, MimeTypes.GZIP, MimeTypes.ICO, MimeTypes.IFF, MimeTypes.JAR, MimeTypes.JSON,
				MimeTypes.M3U, MimeTypes.MIDI, MimeTypes.MKV, MimeTypes.MP2T, MimeTypes.MP3, MimeTypes.MPEG1, MimeTypes.MPEG2,
				MimeTypes.MPEG4, MimeTypes.OCTET_STREAM, MimeTypes.ODP, MimeTypes.ODS, MimeTypes.ODT, MimeTypes.OGA,
				MimeTypes.OGV, MimeTypes.OTF, MimeTypes.PCX, MimeTypes.PICT, MimeTypes.PPT, MimeTypes.PPTX, MimeTypes.PSD,
				MimeTypes.QUICKTIME, MimeTypes.RAR, MimeTypes.RTF, MimeTypes.SGI, MimeTypes.SH, MimeTypes.SVG, MimeTypes.SWF,
				MimeTypes.TAR, MimeTypes.TGA, MimeTypes.TIFF, MimeTypes.TTF, MimeTypes.V3GP, MimeTypes.VISIO, MimeTypes.WAV,
				MimeTypes.WEBA, MimeTypes.WEBM, MimeTypes.WEBP, MimeTypes.WMV, MimeTypes.WOFF, MimeTypes.WOFF2, MimeTypes.WORD,
				MimeTypes.WORD_DOC, MimeTypes.XHTML, MimeTypes.XML, MimeTypes.XUL, MimeTypes.ZIP}
		)
public class StreamDecorator implements IDecorator<StreamResult> {

	@Override
	public void decorate(StreamResult strRes, OutputStream out, MimeTypes type) {

		byte[] buffer = new byte[4096*1024];
		int read = 0;

		try {
			while ((read = strRes.getStream().read(buffer)) > 0) {
				out.write(buffer, 0, read);
			}
			out.flush();
		} catch (IOException e) {
			//throw new RuntimeException(e);
		} finally {
			try {
				strRes.getStream().close();
			} catch (IOException e) {

			}
		}

	}

	@Override
	public void decorate(StreamResult strRes, HttpServletRequest request,
			HttpServletResponse response, MimeTypes type) {

		try {

			if (strRes.getContentType() != null) {

				response.setHeader("Content-Type", strRes.getContentType());
				response.setContentType(strRes.getContentType());

			}

			StringBuilder builder = new StringBuilder();
			builder.append(strRes.isAttachment() ? "attachment" : "inline");
			if (strRes.getName() != null && strRes.getName().trim().length() > 0) {
				builder.append("; filename=\"");
				builder.append(strRes.getName().trim());
				builder.append("\"");
			}

			response.setHeader("Content-Disposition", builder.toString());
			
			response.setHeader("Accept-Ranges", "bytes");

			for (StreamResult.Header h : strRes.getHeaders()) {

				String[] values = h.getValues();

				if (values.length == 1) {
					response.setHeader(h.getName(), values[0]);
				} else if (values.length > 1) {
					boolean first = true;
					for (String v : values) {
						if (first) {
							response.setHeader(h.getName(), v);
							first = false;
						} else {
							response.addHeader(h.getName(), v);
						}
					}
				}

			}
			
			List<Range> ranges = new ArrayList<>();
			if (StringUtils.isNotBlank(request.getHeader("Range"))) {
				try {
					String r = request.getHeader("Range");
					int pos = r.indexOf('=');
					if (pos >= 0) {
						String unit = r.substring(0, pos).trim();
						if (unit.equals("bytes")) {
							String[] rs = r.substring(pos + 1).split(",");
							for (String c : rs) {
								String[] cp = c.trim().split("-");
								Range range = new Range();
								if (StringUtils.isNotBlank(cp[0])) {
									range.setStart(Long.parseLong(cp[0].trim()));
								}
								if (cp.length == 2 && StringUtils.isNotBlank(cp[1])) {
									range.setEnd(Long.parseLong(cp[1].trim()));
								}
								if (range.getStart() == null) {
									range.setStart(strRes.getContentLength() - range.getEnd());
									range.setEnd(strRes.getContentLength() - 1);
								}
								else if (range.getEnd() == null) {
									range.setEnd(strRes.getContentLength() - 1);
								}
								if (strRes.getContentLength() != null) {
									range.setSize(strRes.getContentLength());
								}
								if (range.getEnd() >= range.getSize()) {
									range.setEnd(range.getSize() - 1);
								}
								ranges.add(range);
							}
						}
					}
					Collections.sort(ranges, new Comparator<Range>() {
						@Override
						public int compare(Range o1, Range o2) {
							return o1.getStart().intValue() - o2.getStart().intValue();
						}
					});
					Range lastIr = null;
					Iterator<Range> iter = ranges.iterator();
					while (iter.hasNext()) {
						Range ir = iter.next();
						if (lastIr != null) {
							if (ir.getStart() <= lastIr.getEnd()) {
								ir.setStart(lastIr.getEnd() + 1);
							}
						}
						if (lastIr != null && ir.getStart().longValue() == lastIr.getEnd().longValue() + 1 && ir.getEnd() >= lastIr.getEnd()) {
							lastIr.setEnd(ir.getEnd());
							iter.remove();
						}
						else if (ir.getStart() > ir.getEnd()) {
							iter.remove();
						}
						else {
							lastIr = ir;
						}
					}
				} catch (Exception e) {
					/*response.setStatus(416); // 416 Range Not Satisfiable
					return;*/
					ranges.clear();
				}
			}
			if (!ranges.isEmpty()) {
				
				try (InputStream resIs = strRes.getStream()) {
					
					response.setStatus(206); // 206 Partial Content
					if (ranges.size() == 1) {
						Range range = ranges.get(0);
						response.setContentLength(range.getEnd().intValue() - range.getStart().intValue() + 1);
						response.setHeader("Content-Range", "bytes " + range.getStart().toString() + "-" + range.getEnd().toString() + "/" + range.getSize().toString());
						
						if (!"HEAD".equalsIgnoreCase(request.getMethod())) {
							writeRange(resIs, response.getOutputStream(), range, 0);
						}
						
					}
					else {
						final String boundary = "THIS_STRING_SEPARATES";
						response.setContentType("multipart/byteranges; boundary=" + boundary);
						
						long totalResponseLength = 0;
						for (Range r : ranges) {
							totalResponseLength += r.getContentBefore(boundary, strRes.getContentType()).getBytes().length
									+ r.getContentAfter().getBytes().length	// range headers
									+ r.getRangeSize();						// range data size
						}
						totalResponseLength += boundary.getBytes().length	// final boundary
													+ 4;							// final --boundary--
						
						response.setContentLength((int) totalResponseLength);
						
						if (!"HEAD".equalsIgnoreCase(request.getMethod())) {
							response.setBufferSize(16*4096);
							OutputStream os = response.getOutputStream();
							long lastRead = 0;
							for (Range r : ranges) {
								
								os.write(r.getContentBefore(boundary, strRes.getContentType()).getBytes());
								
								lastRead = writeRange(resIs, os, r, lastRead);
								
								os.write(r.getContentAfter().getBytes());
								
							}
							os.write(("--" + boundary + "--").getBytes());
						}
					}
					
				}
				
			}
			else if (strRes.getContentLength() != null) {
				
				response.setContentLength(strRes.getContentLength().intValue());
				
				if (!"HEAD".equalsIgnoreCase(request.getMethod())) {
					decorate(strRes, response.getOutputStream(), type);
				}
				
			}
			
			response.flushBuffer();
			
		} catch (IOException e) {
			if (!"Connection reset by peer".equals(e.getMessage())) {
				throw new RuntimeException(e);
			}
		}

	}
	
	private long writeRange(InputStream is, OutputStream os, Range range, long lastRead) throws IOException {
		
		long totalRead = is.skip(range.getStart() - lastRead);
		
		byte[] buffer = new byte[4096];
		do {
			
			int read = is.read(buffer, 0, (int) Math.min(4096, range.getEnd() - totalRead - lastRead + 1));
			totalRead += read;
			os.write(buffer, 0, read);
			
		} while (lastRead + totalRead < range.getEnd());
		
		return totalRead + lastRead;
		
	}
	
	private static class Range {
		
		private Long start;
		private Long end;
		private Long size;
		
		public Long getStart() {
			return start;
		}
		
		public void setStart(Long start) {
			this.start = start;
		}
		
		public Long getEnd() {
			return end;
		}
		
		public void setEnd(Long end) {
			this.end = end;
		}
		
		public Long getSize() {
			return size;
		}
		
		public void setSize(Long size) {
			this.size = size;
		}
		
		public String toString() {
			return start.toString() + "-" + end.toString() + "/" + size.toString();
		}
		
		public long getRangeSize() {
			return end - start + 1;
		}
		
		public String getContentBefore(String boundary, String contentType) {
			
			StringBuilder builder = new StringBuilder();
			
			builder.append("--" + boundary + "\r\n");
			builder.append("Content-Type: " + contentType + "\r\n");
			builder.append("Content-Range: bytes " + toString() + "\r\n");
			builder.append("\r\n");
			
			return builder.toString();
			
		}
		
		public String getContentAfter() {
			return "\r\n";
		}
		
	}

}
