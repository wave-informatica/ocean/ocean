/*
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.events;

import com.waveinformatica.ocean.core.security.OceanUser;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ivano
 */
public class LoginEvent extends Event {
    
    private String userName;
    private String password;
    private OceanUser loggedUser;
    private boolean managed = false;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private final List<String> errors = new ArrayList<String>();
    
    public LoginEvent(Object sender, String userName, String password, HttpServletRequest request, HttpServletResponse response) {
        super(sender, CoreEventName.LOGIN.name());
        this.userName = userName;
        this.password = password;
        this.request = request;
        this.response = response;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public OceanUser getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(OceanUser loggedUser) {
        this.loggedUser = loggedUser;
    }

    public boolean isManaged() {
        return managed;
    }

    public void setManaged(boolean managed) {
        this.managed = managed;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    public List<String> getErrors() {
        return errors;
    }
    
}
