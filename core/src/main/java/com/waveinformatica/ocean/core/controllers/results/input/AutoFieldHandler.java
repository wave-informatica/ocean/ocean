/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.results.input;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.controllers.results.InputResult.FieldHandler;

import javax.inject.Inject;

/**
 * <p>This handler is the default {@link FieldHandler}. It will try to
 * find a specific handler based on the parameter's type.</p>
 * 
 * <p>The following list shows the current way this handler handles data types:</p>
 * 
 * <dl>
 *   <dt><code>enum</code></dt> <dd>{@link EnumFieldHandler}</dd>
 *   <dt><code>bool</code> or <code>Boolean</code></dt> <dd>{@link BooleanFieldHandler}</dd>
 *   <dt><code>javax.servlet.http.Part</code></dt> <dd>{@link PartFieldHandler}</dd>
 *   <dt><code>java.util.Date</code> or <code>java.sql.Date</code></dt> <dd>{@link DateFieldHandler}</dd>
 *   <dt><code>com.waveinformatica.ocean.core.controllers.results.input.Range&lt;?&gt;</code> <dd>{@link RangeFieldHandler}</dd>
 *   <dt>if none of the previous apply</dt> <dd>{@link TextFieldHandler}
 * </dl>
 * @author Ivano
 */
public class AutoFieldHandler implements InputResult.FieldHandler {

	@Inject
	private ObjectFactory factory;

	private InputResult.FieldHandler delegatedHandler;

	@Override
	public String getFieldTemplate() {
		return delegatedHandler.getFieldTemplate();
	}

	@Override
	public void init(InputResult result, CoreController.OperationInfo opInfo, CoreController.ParameterInfo paramInfo, InputResult.FormField field) {

		Class<? extends InputResult.FieldHandler> handlerType = TextFieldHandler.class;

		if (paramInfo.getParamClass().isEnum()) {
			handlerType = EnumFieldHandler.class;
		} else if (BooleanFieldHandler.canHandle(paramInfo.getParamClass())) {
			handlerType = BooleanFieldHandler.class;
		} else if (PartFieldHandler.canHandle(paramInfo.getParamClass())) {
			handlerType = PartFieldHandler.class;
		} else if (DateFieldHandler.canHandle(paramInfo.getParamClass())) {
			handlerType = DateFieldHandler.class;
		} else if (RangeFieldHandler.canHandle(paramInfo.getParamClass())) {
			handlerType = RangeFieldHandler.class;
		}

		field.setHandler(factory.newInstance(handlerType));
		field.getHandler().init(result, opInfo, paramInfo, field);
	}

	@Override
	public String formatValue(InputResult result, InputResult.FormField field, Object value) {
		throw new UnsupportedOperationException("This invocation had to be handled by the delegated FieldHandler.");
	}

}
