/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.converters;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.logging.Logger;

import javax.inject.Inject;


import com.waveinformatica.ocean.core.annotations.TypeConverter;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.input.Range;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import java.lang.reflect.ParameterizedType;

/**
 *
 * @author Ivano
 */
@TypeConverter({
	"com.waveinformatica.ocean.core.controllers.results.input.Range"
})
public class RangeConverter implements ITypeConverter {

	private static final Logger logger = LoggerUtils.getCoreLogger();

	@Inject
	private CoreController coreController;

	@Inject
	private ObjectFactory factory;

	@Override
	@SuppressWarnings("unchecked")
	public Object fromParametersMap(String destName, Class destClass, Type genericType,
		Map<String, String[]> source, Object[] ctxObjects)
	{
		Range range = new Range();
		
		ParameterizedType type = (ParameterizedType) genericType;
		Class cls = (Class) type.getActualTypeArguments()[0];
		
		ITypeConverter conv = null;
		if (cls.isEnum()) {
			conv = factory.newInstance(EnumConverter.class);
		}
		else if (cls != String.class) {
			Class<? extends ITypeConverter> convCls = coreController.findConverter(cls.getName());
			if (convCls != null) {
				conv = factory.newInstance(convCls);
			}
		}

		String[] args = source.get(destName + ".from");
		if (args != null && args.length == 1) {

			try {
				if (conv != null) {
					range.setFrom(conv.fromParametersMap(destName + ".from", cls, null, source, ctxObjects));
				}
				else {
					range.setFrom(args[0]);
				}
			}
			catch (Exception e) {
				logger.warning("Unable to parse request parameter \"" + destName + "\": \"" + args[0] + "\" (" + e.getMessage() + ")");
			}
		}

		args = source.get(destName + ".to");
		if (args != null && args.length == 1) {

			try {
				if (conv != null) {
					range.setTo(conv.fromParametersMap(destName + ".to", cls, null, source, ctxObjects));
				}
				else {
					range.setTo(args[0]);
				}
			}
			catch (Exception e) {
				logger.warning("Unable to parse request parameter \"" + destName + "\": \"" + args[0] + "\" (" + e.getMessage() + ")");
			}
		}

		return range;
	}

	@Override
	public Object fromScalar(String value, Class destClass, Map<String, String[]> source, Object[] ctxObjects)
	{
		throw new IllegalStateException("Not supported.");
	}
}
