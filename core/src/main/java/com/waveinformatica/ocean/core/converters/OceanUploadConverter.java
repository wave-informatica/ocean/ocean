/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.converters;

import com.waveinformatica.ocean.core.annotations.TypeConverter;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.util.OceanUpload;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
@TypeConverter("com.waveinformatica.ocean.core.util.OceanUpload")
public class OceanUploadConverter implements ITypeConverter {

    @Override
    public Object fromParametersMap(String destName, Class destClass, Type genericType, Map<String, String[]> source, Object[] ctxObjects) {
        
        HttpServletRequest req = null;
        for (Object o : ctxObjects) {
            if (o instanceof HttpServletRequest) {
                req = (HttpServletRequest) o;
                break;
            }
        }
        
        if (req == null) {
            return null;
        }
        
        Part result = null;
        
        try {
            result = req.getPart(destName);
            
            if (result != null) {
            	
            	String header = result.getHeader("content-disposition");
            	String name = header.substring(header.toLowerCase().indexOf("filename=\"") + "filename=\"".length());
            	name = name.substring(Math.max(name.lastIndexOf('/'), name.lastIndexOf('\\')) + 1, name.indexOf("\""));
            	
            	if (StringUtils.isBlank(name)) {
            		return null;
            	}
            	
            	OceanUpload upload = new OceanUpload(result);
            	
            	upload.setContentType(result.getContentType());
            	
            	for (MimeTypes mt : MimeTypes.values()) {
            		if (result.getContentType().startsWith(mt.getMimeType())) {
            			upload.setKnownContentType(mt);
            			break;
            		}
            	}
            	
            	upload.setSize(result.getSize());
    			
    			upload.setFileName(name);
    			
    			return upload;
            	
            }
            
        } catch (IOException ex) {
            Logger.getLogger(OceanUploadConverter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(OceanUploadConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
        
    }

    @Override
    public Object fromScalar(String value, Class destClass, Map<String, String[]> source, Object[] ctxObjects) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
