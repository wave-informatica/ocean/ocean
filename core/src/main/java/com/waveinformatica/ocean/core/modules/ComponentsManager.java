/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.modules;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ComponentsManager {
	
	private final Map<Class<?>, ComponentsBin> binInstances = new HashMap<Class<?>, ComponentsBin>();
	private final Map<String, List<String>> componentsCatalog = new HashMap<String, List<String>>();
	private final Map<Class<? extends Annotation>, Set<Class<?>>> components = new HashMap<>();
	
	protected Set<Class<?>> getComponents(Class<? extends Annotation> annotation) {
		Set<Class<?>> components = this.components.get(annotation);
		if (components == null) {
			components = new HashSet<>(0);
		}
		return components;
	}
	
	protected Map<String, List<String>> getComponentsCatalog() {
		return componentsCatalog;
	}
	
	protected void loadCatalog(String module, List<String> annotations) {
		if (annotations == null || annotations.isEmpty()) {
			return;
		}
		List<String> old = componentsCatalog.get(module);
		if (old == null) {
			old = new ArrayList<>();
			componentsCatalog.put(module, old);
		}
		old.addAll(annotations);
	}
	
	protected void loadComponents(String module, Map<Class<? extends Annotation>, Set<Class<?>>> classes) {
		
		for (Class<? extends Annotation> a : classes.keySet()) {
			
			Set<Class<?>> components = this.components.get(a);
			if (components == null) {
				components = new HashSet<>();
				this.components.put(a, components);
			}
			components.addAll(classes.get(a));
			
			for (ComponentsBin cb : binInstances.values()) {
				
				if (cb.getManagedComponents().contains(a)) {
					cb.loadClasses(a, classes.get(a));
				}
				
			}
			
		}
		
	}
	
	protected void unloadComponents(Module module) {
		
		if (module == null) { // In this case, the unloaded module should be Ocean's core
			return;
		}
		
		ModuleClassLoader mcl = module.getModuleClassLoader();
		
		Iterator<Map.Entry<Class<?>, ComponentsBin>> entries = binInstances.entrySet().iterator();
		while (entries.hasNext()) {
			Map.Entry<Class<?>, ComponentsBin> entry = entries.next();
			ComponentsBin curBin = entry.getValue();
			for (Class<? extends Annotation> mc : curBin.getManagedComponents()) {
				Set<Class<?>> unloading = new HashSet<>();
				Set<Class<?>> cs = components.get(mc);
				if (cs != null) {
					for (Class<?> mco : cs) {
						if (mco.getClassLoader() == mcl) {
							unloading.add(mco);
						}
					}
				}
				if (!unloading.isEmpty()) {
					curBin.unloadClasses(mc, unloading);
				}
			}
			if (entry.getKey().getClassLoader() == mcl) {
				entry.getValue().destroy();
				entries.remove();
			}
		}
		
		Iterator<Entry<Class<? extends Annotation>, Set<Class<?>>>> fIter = components.entrySet().iterator();
		while (fIter.hasNext()) {
			Entry<Class<? extends Annotation>, Set<Class<?>>> e = fIter.next();
			if (e.getKey().getClassLoader() instanceof ModuleClassLoader &&
					((ModuleClassLoader) e.getKey().getClassLoader()).getModule() == module) {
				fIter.remove();
			}
			else {
				Iterator<Class<?>> cIter = e.getValue().iterator();
				while (cIter.hasNext()) {
					Class<?> c = cIter.next();
					if (c.getClassLoader() instanceof ModuleClassLoader &&
							((ModuleClassLoader) c.getClassLoader()).getModule() == module) {
						cIter.remove();
					}
				}
				if (e.getValue().isEmpty()) {
					fIter.remove();
				}
			}
		}
		
		componentsCatalog.remove(module.getName());
		
	}
	
	/**
	 * This method is intended for analysis only.
	 * 
	 * @return an unmodifiable map of defined components bins and their contents
	 */
	public Map<String, ComponentsBin> getContent() {
		
		Map<String, ComponentsBin> result = new TreeMap<>();
		
		for (Entry<Class<?>, ComponentsBin> e : binInstances.entrySet()) {
			ComponentsBin newCB = e.getValue().copy();
			result.put(e.getKey().getName(), newCB);
		}
		
		return Collections.unmodifiableMap(result);
		
	}
	
	protected Map<Class<?>, ComponentsBin> getBinInstances() {
		return binInstances;
	}
	
	protected void registerBin(Class<?> ownerClass, ComponentsBin bin) {
		binInstances.put(ownerClass, bin);
	}
	
}
