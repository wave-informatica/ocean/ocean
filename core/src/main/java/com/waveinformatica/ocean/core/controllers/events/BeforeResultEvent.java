/*
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.events;

import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 * This event is fired after the operation returns a not null result. It can be used to enrich it with more information, or to
 * change contained information.
 * 
 * @author Ivano
 */
public class BeforeResultEvent extends Event {
    
    private static String[] getClassNames(BaseResult result) {
        List<String> names = new ArrayList<String>();
        
        Class cls = result.getClass();
        while (cls != Object.class) {
            names.add(cls.getName());
            cls = cls.getSuperclass();
        }
        
        return names.toArray(new String[names.size()]);
    }
    
    private final BaseResult result;
    private final OperationInfo operationInfo;
    private final HttpServletRequest request;
    
    public BeforeResultEvent(Object source, OperationInfo operationInfo, BaseResult result, HttpServletRequest request) {
        super(source, CoreEventName.BEFORE_RESULT.name(), getClassNames(result));
        this.operationInfo = operationInfo;
        this.result = result;
        this.request = request;
    }
    
    /**
     * Returns the result of the operation
     * @return the operation's result
     */
    public BaseResult getResult() {
        return result;
    }
    
    /**
     * Returns the executed operation
     * 
     * @return the executed operation's info
     */
    public OperationInfo getOperationInfo() {
        return operationInfo;
    }

    /**
     * Returns the received request
     * 
     * @return the received request
     */
    public HttpServletRequest getRequest() {
        return request;
    }
    
}
