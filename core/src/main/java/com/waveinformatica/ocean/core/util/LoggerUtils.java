/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.util;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.Constants;
import com.waveinformatica.ocean.core.modules.Module;
import com.waveinformatica.ocean.core.modules.ModuleClassLoader;
import java.util.logging.Logger;

/**
 * Utility functions for logging purposes
 * 
 * @author Ivano
 */
public class LoggerUtils {

	private static final ThreadLocal<Logger> forcedLogger = new ThreadLocal<Logger>();
	private static Logger coreLogger;

	/**
	 * Retrieves the main Logger instance
	 * 
	 * @return the main logger
	 */
	public static Logger getCoreLogger() {

		if (forcedLogger.get() != null) {
			return forcedLogger.get();
		}

		if (coreLogger == null) {
			coreLogger = Logger.getLogger(Application.getInstance().getNamespace() + "." + Constants.CORE_LOGGER_NAME);
		}

		return coreLogger;
	}

	/**
	 * Retrieves the right logger based on a given class (core logger or module logger)
	 * 
	 * @param cls the reference class
	 * @return the right logger
	 */
	public static Logger getLogger(Class<?> cls) {
		if (forcedLogger.get() != null) {
			return forcedLogger.get();
		}
		ClassLoader clsLoader = cls.getClassLoader();
		if (clsLoader instanceof ModuleClassLoader) {
			Module m = ((ModuleClassLoader) clsLoader).getModule();
			return m.getLogger();
		}
		return getCoreLogger();
	}

	/**
	 * Retrieves a module logger based on the given name
	 * 
	 * @param moduleName the module's name
	 * @return the module's logger
	 * @deprecated
	 */
	@Deprecated
	public static Logger getLogger(String moduleName) {
		if (forcedLogger.get() != null) {
			return forcedLogger.get();
		}
		return Logger.getLogger(Application.getInstance().getNamespace() + "." + Constants.CORE_LOGGER_NAME + "." + moduleName);
	}

	/**
	 * Allows to force the use of a specific logger (i.e. a batch operation with
	 * its own logger).
	 * 
	 * @param logger the required logger or <code>null</code> to disable this feature
	 */
	public static void forceLogger(Logger logger) {
		forcedLogger.set(logger);
	}

	private LoggerUtils() {}

}
