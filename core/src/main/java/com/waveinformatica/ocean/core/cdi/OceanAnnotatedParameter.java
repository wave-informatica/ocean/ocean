/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.cdi;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.inject.spi.AnnotatedCallable;
import javax.enterprise.inject.spi.AnnotatedParameter;

/**
 *
 * @author Ivano
 */
public class OceanAnnotatedParameter<T> implements AnnotatedParameter<T> {
    
    private final AnnotatedCallable<T> callable;
    private final int position;
    private final Class<T> type;
    private final Set<Annotation> annotations = new HashSet<Annotation>();
    private final Set<Type> typeClosure;

    public OceanAnnotatedParameter(AnnotatedCallable<T> callable, int position, Class<T> type) {
        this.callable = callable;
        this.position = position;
        this.type = type;
        
        // Annotations
        Annotation[] annotations = null;
        Member member = callable.getJavaMember();
        if (member instanceof Constructor) {
            Constructor c = (Constructor) member;
            annotations = c.getParameterAnnotations()[position];
        }
        else if (member instanceof Method) {
            Method m = (Method) member;
            annotations = m.getParameterAnnotations()[position];
        }
        
        if (annotations != null) {
	        for (Annotation a : annotations) {
	            this.annotations.add(a);
	        }
        }
        
        // Type Closure
        typeClosure = CDIHelper.getTypeClosure(type);
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public AnnotatedCallable<T> getDeclaringCallable() {
        return callable;
    }

    @Override
    public Type getBaseType() {
        return type;
    }

    @Override
    public Set<Type> getTypeClosure() {
        return typeClosure;
    }

    @Override
    public <T extends Annotation> T getAnnotation(Class<T> annotationType) {
        for (Annotation a : annotations) {
            if (a.annotationType() == annotationType) {
                return (T) a;
            }
        }
        return null;
    }

    @Override
    public Set<Annotation> getAnnotations() {
        return annotations;
    }

    @Override
    public boolean isAnnotationPresent(Class<? extends Annotation> annotationType) {
        for (Annotation a : annotations) {
            if (a.annotationType() == annotationType) {
                return true;
            }
        }
        return false;
    }
    
}
