/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.converters;

import com.google.gson.Gson;
import com.waveinformatica.ocean.core.util.JsonUtils;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ivano
 */
public class JsonConverter implements ITypeConverter {

    @Override
    public Object fromParametersMap(String destName, Class destClass, Type genericType, Map<String, String[]> source, Object[] ctxObjects) {
        
        if (source.get(destName) == null) {
            return null;
        }
        
        Gson gson = JsonUtils.getBuilder().create();
        
        if (source.get(destName).length > 1 || Collection.class.isAssignableFrom(destClass)) {
            try {
                return destClass.newInstance();
            } catch (InstantiationException ex) {
                Logger.getLogger(JsonConverter.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(JsonConverter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else {
            return gson.fromJson(source.get(destName)[0], genericType);
        }
        
        return null;
        
    }

    @Override
    public Object fromScalar(String value, Class destClass, Map<String, String[]> source, Object[] ctxObjects) {
        
        return JsonUtils.getBuilder().create().fromJson(value, destClass);
        
    }
    
}
