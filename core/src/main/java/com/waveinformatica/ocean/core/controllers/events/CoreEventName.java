/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.events;

import com.waveinformatica.ocean.core.controllers.decorators.WebPageDecorator;

/**
 *
 * @author Ivano
 */
public enum CoreEventName {
    
    WEB_REQUEST,
    LOGIN,
    LOGOUT,
    BEFORE_RESULT,
    SECURITY_CONFIGURATION,
    MODULE_LOADED,
    MODULE_UNLOADED,
    AUTHORIZE_OPERATION,
    BEFORE_OPERATION,
    OPERATION_ERROR,
    AFTER_RESULT,
    MAIL_SENT,
    DASHBOARD,
    
    /**
     * Fired by {@link WebPageDecorator} after creation of the XHTMLRenderer
     * to be able to enrich or modify the renderer.
     */
    PDF_RENDERER_PRE_LAYOUT,
    PDF_RENDERER_POST_LAYOUT
}
