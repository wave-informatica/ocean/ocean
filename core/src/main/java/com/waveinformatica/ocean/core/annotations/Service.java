/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.annotations;

import com.waveinformatica.ocean.core.controllers.IService;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation defines Service classes. Services must implement {@link IService} interface too.
 * 
 * @author Ivano
 */
@OceanComponent(mustExtend = IService.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Service {
    
    /**
     * The unique name of the defined service
     * 
     * @return the service's unique name
     */
    String value();
    
    /**
     * <code>true</code> to start the service automatically after the module is loaded
     * 
     * @return if the service must be started after module startup
     */
    boolean autoStart() default true;
    
}
