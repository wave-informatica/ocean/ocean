/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.controllers.results;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.events.DashboardEvent;
import com.waveinformatica.ocean.core.controllers.scopes.ScopeChain;

public class DashboardResult extends WebPageResult {
	
	@Inject
	private CoreController controller;
	
	private final List<DashboardResult.Counter> counters = new ArrayList<DashboardResult.Counter>();
	private final List<DashboardResult.Tile> tiles = new ArrayList<DashboardResult.Tile>();
	
	public DashboardResult () {
		setTemplate("/templates/core/dashboardResult.tpl");
	}
	
	public void fireEvent(String operation, ScopeChain chain) {
		DashboardEvent event = new DashboardEvent(this, this, chain, operation, "*");
		controller.dispatchEvent(event);
	}
	
	public List<DashboardResult.Counter> getCounters() {
		return counters;
	}
	
	public List<DashboardResult.Tile> getTiles() {
		return tiles;
	}

	public Counter addCounter(String icon, String title) {
		Counter counter = new DashboardResult.Counter(icon, title);
		counters.add(counter);
		return counter;
	}
	
	public Tile addTile(String icon, String title) {
		Tile tile = new Tile(icon, title);
		tiles.add(tile);
		return tile;
	}

	public static class Counter {
		
		private final String icon;
		private final String title;
		private String background = "aqua";
		private String value;

		Counter(String icon, String title) {
			super();
			this.icon = icon;
			this.title = title;
		}

		public String getIcon() {
			return icon;
		}

		public String getBackground() {
			return background;
		}

		public void setBackground(String background) {
			this.background = background;
		}

		public String getTitle() {
			return title;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
		
	}
	
	public static class Tile {
		
		private Long serverId;
		private String clientUniqueId;
		private final String icon;
		private final String title;
		private String template;
		private Object data;
		private final List<Button> options = new ArrayList<>();
		private final List<Button> buttons = new ArrayList<>();
		private Integer order;

		Tile(String icon, String title) {
			super();
			this.icon = icon;
			this.title = title;
		}

		public Long getServerId() {
			return serverId;
		}

		public void setServerId(Long serverId) {
			this.serverId = serverId;
		}

		public String getClientUniqueId() {
			return clientUniqueId;
		}

		public void setClientUniqueId(String clientUniqueId) {
			this.clientUniqueId = clientUniqueId;
		}

		public String getIcon() {
			return icon;
		}

		public String getTitle() {
			return title;
		}

		public String getTemplate() {
			return template;
		}

		public void setTemplate(String template) {
			this.template = template;
		}

		public Object getData() {
			return data;
		}

		public void setData(Object data) {
			this.data = data;
		}

		public List<Button> getOptions() {
			return options;
		}
		
		public Button addOption(String icon, String text, String url) {
			return addOption(icon, text, url, "");
		}
		
		public Button addOption(String icon, String text, String url, String cls) {
			Button button = new Button();
			button.setIcon(icon);
			button.setText(text);
			button.setUrl(url);
			button.setCls(cls);
			options.add(button);
			return button;
		}

		public List<Button> getButtons() {
			return buttons;
		}
		
		public Integer getOrder() {
			return order;
		}

		public void setOrder(Integer order) {
			this.order = order;
		}

		public Button addButton(String icon, String text, String url) {
			Button button = new Button();
			button.setIcon(icon);
			button.setText(text);
			button.setUrl(url);
			buttons.add(button);
			return button;
		}
		
		public static class Button {
			
			private String icon;
			private String text;
			private String url;
			private String cls;

			public String getIcon() {
				return icon;
			}

			public void setIcon(String icon) {
				this.icon = icon;
			}

			public String getText() {
				return text;
			}

			public void setText(String text) {
				this.text = text;
			}

			public String getUrl() {
				return url;
			}

			public void setUrl(String url) {
				this.url = url;
			}

			public String getCls() {
				return cls;
			}

			public void setCls(String cls) {
				this.cls = cls;
			}
			
		}
	}
	
}
