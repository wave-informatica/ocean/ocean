/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.controllers.results.input;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.cache.CacheManager;
import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.controllers.CoreController.ParameterInfo;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.controllers.results.InputResult.FormField;

public class CacheProviderFieldHandler extends ComboFieldHandler {
	
	@Inject
	private CacheManager manager;
	
	@Override
	public void init(InputResult result, OperationInfo opInfo, ParameterInfo paramInfo, FormField field) {
		
		getItems().clear();
		
		for (String h : manager.getProviderNames()) {
			addOption(h, h);
		}
		
	}

}
