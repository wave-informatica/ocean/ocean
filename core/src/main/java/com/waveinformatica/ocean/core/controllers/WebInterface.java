/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers;

import com.waveinformatica.ocean.core.controllers.events.WebRequestEvent;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ivano
 */
@MultipartConfig
@WebServlet(urlPatterns = "/*")
public class WebInterface extends HttpServlet {

	private static final Logger logger = LoggerUtils.getCoreLogger();

	@Inject
	private CoreController controller;

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");

		Context.setCoreController(controller);

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		response.setHeader("X-UA-Compatible", "IE=edge");

		Context.init(request);

		logger.log(Level.INFO, "Requested " + request.getMethod() + " " + request.getRequestURI());

		try {

			WebRequestEvent event = new WebRequestEvent(this, request, response);
			controller.dispatchEvent(event);

			if (event.getRequestManagedBy() == null) {
				response.sendError(404);
			}

		} catch (Exception e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, "Error dispatching WebRequest", e);
		} finally {
			Context.clear();
			Context.setCoreController(null);
			InstanceListener.get().clear();
		}

	}

}
