/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.results;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ivano
 */
public enum MimeTypes {
    
    HTML("text/html", new String[]{"html", "htm"}, true),
    XHTML("application/xhtml+xml", new String[]{"html", "htm"}, false),
    TEXT("text/plain", new String[]{"txt"}, false),
    PDF("application/pdf", new String[]{"pdf"}, false),
    CSV("text/csv", new String[]{"csv"}, false),
    EXCEL("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", new String[]{"xlsx"}, false),
    WORD("application/vnd.openxmlformats-officedocument.wordprocessingml.document", new String[]{"docx"}, false),
    PPTX("application/vnd.openxmlformats-officedocument.presentationml.presentation", new String[] {"pptx"}, false),
    WORD_DOC("application/msword", new String[] {"doc", "dot"}, false),
    EXCEL_XLS("application/vnd.ms-excel", new String[] {"xls", "xlt", "xla"}, false),
    PPT("application/vnd.ms-powerpoint", new String[] {"ppt", "pot", "pps", "ppa"}, false),
    ACCESS_MDB("application/vnd.ms-access", new String[] {"mdb"}, false),
    JSON("application/json", new String[]{"json"}, true),
    CSS("text/css", new String[]{"css"}, true),
    JAVASCRIPT("text/javascript", new String[]{"js"}, true),
    JPEG("image/jpeg", new String[]{"jpg", "jpeg"}, false),
    GIF("image/gif", new String[]{"gif"}, false),
    PNG("image/png", new String[]{"png"}, false),
    WEBP("image/webp", new String[]{"webp"}, false),
    RTF("application/rtf", new String[]{"rtf"}, false),
    TTF("application/x-font-ttf", new String[]{"ttf"}, false),
    WOFF("application/x-font-woff", new String[]{"woff"}, false),
    WOFF2("application/font-woff2", new String[]{"woff2"}, false),
    EOT("application/vnd.ms-fontobject", new String[]{"eot"}, false),
    SVG("image/svg+xml", new String[]{"svg"}, false),
    EML("message/rfc822", new String[]{"eml"}, false),
    ICS("text/calendar", new String[]{"ics"}, false),
    JAR("application/java-archive", new String[]{"jar"}, false),
    XML("text/xml", new String[] {"xml"}, false),
    MPEG1("video/mpeg", new String[] {"mp2", "mpe", "mpv", "m1v", "mpg", "mpeg"}, false),
    MPEG2("video/mpeg", new String[] {"m2v", "mpg", "mpeg"}, false),
    MPEG4("video/mp4", new String[] {"mp4", "m4p", "m4v"}, false),
    M3U("audio/x-mpegurl", new String[] {"m3u"}, false),
    AVI("video/x-msvideo", new String[] {"avi"}, false),
    WMV("video/x-ms-wmv", new String[] {"wmv"}, false),
    QUICKTIME("video/quicktime", new String[] {"mov", "qt"}, false),
    FLASHVIDEO("video/x-flv", new String[] {"flv", "f4v", "f4p", "f4a", "f4b"}, false),
    OGV("video/ogg", new String[] {"ogv", "ogg"}, false),
    OGA("audio/ogg", new String[] {"oga", "ogg"}, false),
    MKV("video/x-matroska", new String[] {"mkv"}, false),
    MP3("audio/mpeg", new String[] {"mp3"}, false),
    WAV("audio/wav", new String[] {"wav"}, false),
    AMR("audio/amr", new String[] {"amr"}, false),
    AC3("audio/ac3", new String[] {"ac3"}, false),
    AAC("audio/aac", new String[] {"aac"}, false),
    ZIP("application/zip", new String[] {"zip"}, false),
    RAR("application/x-rar-compressed", new String[] {"rar"}, false),
    ARCHIVE_7Z("application/x-7z-compressed", new String[] {"7z"}, false),
    GZIP("application/gzip", new String[] {"gz", "tgz"}, false),
    BZIP("application/bzip", new String[] {"bz"}, false),
    BZIP2("application/bzip2", new String[] {"bz2"}, false),
    MP2T("video/mp2t", new String[] {"mts"}, false),
    WEBM("video/webm", new String[] {"webm"}, false),
    WEBA("audio/webm", new String[] {"weba"}, false),
    V3GP("video/3gpp", new String[] {"3gp"}, false),
    A3GP("audio/3gpp", new String[] {"3gp"}, false),
    V3GP2("video/3gpp2", new String[] {"3g2"}, false),
    A3GP2("audio/3gpp2", new String[] {"3g2"}, false),
    BMP("image/bmp", new String[] {"bmp"}, false),
    EPUB("application/epub+zip", new String[] {"epub"}, false),
    AZW("application/vnd.amazon.ebook", new String[] {"azw"}, false),
    ICO("image/x-icon", new String[] {"ico"}, false),
    MIDI("audio/midi", new String[] {"mid", "midi"}, false),
    SWF("application/x-shockwave-flash", new String[] {"swf"}, false),
    TAR("application/x-tar", new String[] {"tar"}, false),
    TIFF("image/tiff", new String[] {"tiff", "tif"}, false),
    VISIO("application/vnd.visio", new String[] {"vsd"}, false),
    XUL("application/vnd.mozilla.xul+xml", new String[] {"xul"}, false),
    ABIWORD("application/x-abiword", new String[] {"abw"}, false),
    CSH("application/x-csh", new String[] {"csh"}, false),
    SH("application/x-sh", new String[] {"sh"}, false),
    OTF("font/otf", new String[] {"otf"}, false),
    ODP("application/vnd.oasis.opendocument.presentation", new String[] {"odp"}, false),
    ODS("application/vnd.oasis.opendocument.spreadsheet", new String[] {"ods"}, false),
    ODT("application/vnd.oasis.opendocument.text", new String[] {"odt"}, false),
    OCTET_STREAM("application/octet-stream", new String[] {}, false),
    PSD("image/vnd.adobe.photoshop", new String[] {"psd"}, false),
    IFF("image/iff", new String[] { "iff" }, false),
    PCX("image/pcx", new String[] { "pcx" }, false),
    PICT("image/x-pict", new String[] { "pict", "pct", "pic" }, false),
    SGI("image/sgi", new String[] { "sgi" }, false),
    TGA("image/tga", new String[] { "tga" }, false)
    ;
    
    public static MimeTypes[] getByType(String...prefix) {
    	
    	List<MimeTypes> list = new ArrayList<>();
    	
    	for (String p : prefix) {
    		p = p + "/";
    		for (MimeTypes mt : MimeTypes.values()) {
	    		if (mt.getMimeType().startsWith(p)) {
	    			list.add(mt);
	    		}
    		}
    	}
    	
    	return list.toArray(new MimeTypes[list.size()]);
    	
    }
    
    private final String mimeType;
    private final String[] extensions;
    private final boolean allowAcceptHeader;
    
    private MimeTypes (String mimeType, String[] extensions, boolean allowAcceptHeader) {
        this.mimeType = mimeType;
        this.extensions = extensions;
        this.allowAcceptHeader = allowAcceptHeader;
    }

    public String getMimeType() {
        return mimeType;
    }

    public String[] getExtensions() {
        return extensions;
    }

    public boolean isAllowAcceptHeader() {
        return allowAcceptHeader;
    }
}
