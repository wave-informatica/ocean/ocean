/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.results.input;

import java.util.ArrayList;
import java.util.List;

import com.waveinformatica.ocean.core.controllers.results.InputResult;
import java.util.Comparator;

/**
 *
 * @author Ivano
 */
public abstract class ComboFieldHandler implements InputResult.FieldHandler {

	private final List<Item> items = new ArrayList<Item>();

	@Override
	public String getFieldTemplate() {
		return "/templates/core/components/fieldCombo.tpl";
	}

	protected void addOption(String value, String label) {
		items.add(new Item(value, label));
	}

	public List<Item> getItems() {
		return items;
	}

	@Override
	public String formatValue(InputResult result, InputResult.FormField field, Object value) {
		return value != null ? value.toString() : "";
	}

	//ELM 12/07/2018
	//This class can be used by anyone to sort combo options by label (label is the visible part
	//of any options. Below we report an example of this sorting tyle
	// Collections.sort(getItems(), new SortByLabel());
	//Where 'getItems' is a member of ComboFieldHandler class (see above) 
	public static class SortByLabel implements Comparator<Item> {

		// Used for sorting in ascending order of
		// roll number
		public int compare(Item a, Item b) {
			return a.getLabel().compareToIgnoreCase(b.getLabel());
		}
	}

	public static class Item {

		private final String value;
		private final String label;

		public Item(String value, String label) {
			this.value = value;
			this.label = label;
		}

		public String getValue() {
			return value;
		}

		public String getLabel() {
			return label;
		}
	}
}
