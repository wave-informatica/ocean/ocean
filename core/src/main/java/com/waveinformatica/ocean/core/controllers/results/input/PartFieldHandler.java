/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.results.input;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.util.OceanUpload;

import javax.servlet.http.Part;

/**
 *
 * @author Ivano
 */
public class PartFieldHandler implements InputResult.FieldHandler {

    @Override
    public String getFieldTemplate() {
        return "/templates/core/components/fieldFileUpload.tpl";
    }

    @Override
    public void init(InputResult result, CoreController.OperationInfo opInfo, CoreController.ParameterInfo paramInfo, InputResult.FormField field) {
        
    }

    protected static boolean canHandle(Class<?> type) {
        return Part.class.isAssignableFrom(type) || OceanUpload.class.isAssignableFrom(type);
    }

    @Override
    public String formatValue(InputResult result, InputResult.FormField field, Object value) {
        return null;
    }
    
}
