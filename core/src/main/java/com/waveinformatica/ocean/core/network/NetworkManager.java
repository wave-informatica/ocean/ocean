/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.network;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy.Type;
import java.net.URL;
import java.net.URLConnection;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.util.PersistedProperties;

@ApplicationScoped
public class NetworkManager {
	
	@Inject
	private Configuration configuration;
	
	public URLConnection getURLConnection(URL url) throws IOException {
		
		Proxy proxy = getProxy(url.getProtocol());
		
		if (proxy == null) {
			return url.openConnection();
		}
		else {
			if (StringUtils.isNotBlank(proxy.getUserName()) && "https".equals(url.getProtocol())) {
				return new ProxiedHttpsConnection(url, proxy);
			}
			else {
				java.net.Proxy.Type type = Type.HTTP;
				java.net.Proxy netProxy = new java.net.Proxy(type, new InetSocketAddress(proxy.getHost(), proxy.getPort()));
				URLConnection connection = url.openConnection(netProxy);
				if (StringUtils.isNotBlank(proxy.getUserName())) {
					String auth = Base64.encodeBase64String((proxy.getUserName() + ":" + proxy.getPassword()).getBytes());
					connection.setRequestProperty("Proxy-Authorization", "Basic " + auth);
				}
				return connection;
			}
		}
		
	}
	
	public Proxy getProxy(String protocol) {
		
		if (StringUtils.isBlank(protocol)) {
			return null;
		}
		
		protocol = protocol.toLowerCase().trim();
		
		PersistedProperties props = configuration.getNetworkProperties();
		
		String proxySpec = props.getProperty("proxy." + protocol);
		
		if (StringUtils.isBlank(proxySpec)) {
			proxySpec = props.getProperty("proxy.socks");
		}
		if (StringUtils.isBlank(proxySpec)) {
			proxySpec = props.getProperty("proxy.http");
		}
		if (StringUtils.isBlank(proxySpec) || "none".equals(proxySpec.toLowerCase().trim())) {
			return null;
		}
		
		com.waveinformatica.ocean.core.network.Proxy proxy = new com.waveinformatica.ocean.core.network.Proxy();
		proxy.setHost("localhost");
		proxy.setPort(3128);
		
		int atPos = proxySpec.indexOf('@');
		if (atPos >= 0) {
			// Authentication required
			int colPos = proxySpec.indexOf(':');
			if (colPos >= 0 && colPos < atPos) {
				proxy.setPassword(proxySpec.substring(colPos + 1, atPos));
			}
			else {
				colPos = atPos;
			}
			proxy.setUserName(proxySpec.substring(0, colPos));
			proxySpec = proxySpec.substring(atPos + 1);
		}
		
		int port = 3128;
		String host = "localhost";
		int colPos = proxySpec.indexOf(':');
		if (colPos >= 0) {
			proxy.setPort(Integer.parseInt(proxySpec.substring(colPos + 1)));
			proxySpec = proxySpec.substring(0, colPos);
		}
		
		proxy.setHost(proxySpec);
		
		return proxy;
		
	}

}
