/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core;

import com.waveinformatica.ocean.core.util.PersistedProperties;
import java.io.File;
import javax.servlet.ServletContext;

/**
 *
 * @author Ivano
 */
public interface Configuration {
	
	public void initPackageConfiguration(ServletContext context);
	
	public File getConfigDir();
	
	public void setConfigDir(File configDir);
	
	public PersistedProperties getPersistenceProperties();
	
	public PersistedProperties getSiteProperties();
	
	public String getSiteProperty(String key, String def);
	
	public PersistedProperties getCustomProperties(String name);
	
	public String getPackageVersion();
	
	public String getOrganization();
	
	public String getOrganizationUrl();
	
	public PersistedProperties getSmtpProperties();
	
	public PersistedProperties getLoggingProperties();
	
	public PersistedProperties getCachingProperties();
	
	public PersistedProperties getNetworkProperties();
	
}
