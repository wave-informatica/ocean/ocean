package com.waveinformatica.ocean.core.filtering.drivers;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.filtering.QueryFilter;
import com.waveinformatica.ocean.core.filtering.drivers.DataPath;
import com.waveinformatica.ocean.core.filtering.drivers.DataQuery;
import com.waveinformatica.ocean.core.filtering.drivers.DataReference;
import com.waveinformatica.ocean.core.filtering.drivers.DataValue;
import com.waveinformatica.ocean.core.filtering.drivers.QueryBuilderDriver;
import com.waveinformatica.ocean.core.filtering.drivers.ValuesManager;

public class SQLBuilderDriver implements QueryBuilderDriver<String> {
	
	private ValuesManager params;

	public SQLBuilderDriver(ValuesManager params) {
		super();
		this.params = params;
	}

	@Override
	public String and(String... arg0) {
		if (arg0 == null || arg0.length == 0) {
			return null;
		}
		else if (arg0.length == 1) {
			return arg0[0];
		}
		else {
			return "(" + StringUtils.join(arg0, ") AND (") + ")";
		}
	}

	@Override
	public String between(DataReference arg0, DataReference arg1, DataReference arg2) {
		return toQuery(arg0) + " BETWEEN " + toQuery(arg1) + " AND " + toQuery(arg2);
	}

	@Override
	public String cast(DataReference arg0, String arg1) {
		return "CAST (" + toQuery(arg0) + " AS " + arg1 + ")";
	}

	@Override
	public String concat(DataReference... arg0) {
		StringBuilder builder = new StringBuilder();
		for (DataReference dr : arg0) {
			builder.append(", ");
			builder.append(toQuery(dr));
		}
		return "CONCAT(" + builder.substring(2) + ")";
	}

	@Override
	public String contains(DataReference arg0, DataReference arg1) {
		return toQuery(arg0) + " LIKE " + concat(new DataQuery<String>("'%'"), arg1, new DataQuery<String>("'%'"));
	}

	@Override
	public String endsWith(DataReference arg0, DataReference arg1) {
		return toQuery(arg0) + " LIKE " + concat(new DataQuery<String>("'%'"), arg1);
	}

	@Override
	public String equal(DataReference arg0, DataReference arg1) {
		return toQuery(arg0) + " = " + toQuery(arg1);
	}

	@Override
	public String function(String arg0, DataReference... arg1) {
		StringBuilder builder = new StringBuilder();
		for (DataReference dr : arg1) {
			builder.append(", ");
			builder.append(toQuery(dr));
		}
		return arg0 + "(" + builder.substring(2) + ")";
	}

	@Override
	public Class<String> getQueryType() {
		return String.class;
	}

	@Override
	public String gt(DataReference arg0, DataReference arg1) {
		return toQuery(arg0) + " > " + toQuery(arg1);
	}

	@Override
	public String gte(DataReference arg0, DataReference arg1) {
		return toQuery(arg0) + " >= " + toQuery(arg1);
	}

	@Override
	public String in(DataReference arg0, DataReference arg1) {
		return toQuery(arg0) + " IN " + toQuery(arg1);
	}

	@Override
	public String isNotNull(DataReference arg0) {
		return toQuery(arg0) + " IS NOT NULL";
	}

	@Override
	public String isNull(DataReference arg0) {
		return toQuery(arg0) + " IS NULL";
	}

	@Override
	public String lower(DataReference arg0) {
		return "LOWER(" + toQuery(arg0) + ")";
	}

	@Override
	public String lt(DataReference arg0, DataReference arg1) {
		return toQuery(arg0) + " < " + toQuery(arg1);
	}

	@Override
	public String lte(DataReference arg0, DataReference arg1) {
		return toQuery(arg0) + " <= " + toQuery(arg1);
	}

	@Override
	public String not(String arg0) {
		return "NOT (" + arg0 + ")";
	}

	@Override
	public String or(String... arg0) {
		if (arg0 == null || arg0.length == 0) {
			return null;
		}
		else if (arg0.length == 1) {
			return arg0[0];
		}
		else {
			return "(" + StringUtils.join(arg0, ") OR (") + ")";
		}
	}

	@Override
	public String startsWith(DataReference arg0, DataReference arg1) {
		return toQuery(arg0) + " LIKE " + concat(arg1, new DataQuery<String>("'%'"));
	}

	@Override
	public DataQuery<String> toDataQuery(String arg0) {
		return new DataQuery<String>(arg0);
	}

	@Override
	public String toQuery(DataReference dr) {
		if (dr instanceof DataPath) {
			return ((DataPath) dr).getPath();
		}
		else if (dr instanceof DataValue) {
			dr = params.getParam((DataValue) dr);
			return ":" + ((DataValue) dr).getName();
		}
		else if (dr instanceof DataQuery) {
			return ((DataQuery<String>) dr).getValue();
		}
		return null;
	}

	@Override
	public String upper(DataReference arg0) {
		return "UPPER(" + toQuery(arg0) + ")";
	}
	
	public static String getCondition(String prefix, QueryFilter<String> filter) {
		if (filter != null && StringUtils.isNotBlank(filter.getWhereClause())) {
			return prefix + filter.getWhereClause();
		}
		else {
			return "";
		}
	}

}
