/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */
package com.waveinformatica.ocean.core.util;

import com.waveinformatica.ocean.core.controllers.dto.ResourceInfo;
import com.waveinformatica.ocean.core.modules.ModuleClassLoader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 *
 * @author Ivano
 */
@Named
@ApplicationScoped
public class GlobalClassLoader {

	private final Date creationDate;

	private List<ClassLoader> classLoaders = new LinkedList<ClassLoader>();
	private final ClassLoader mainClassLoader;

	public GlobalClassLoader() {
		mainClassLoader = this.getClass().getClassLoader();
		classLoaders.add(mainClassLoader);
		creationDate = new Date();
	}

	public void addClassLoader(ClassLoader classLoader) {
		classLoaders.add(0, classLoader);
	}

	public void removeClassLoader(ClassLoader classLoader) {
		classLoaders.remove(classLoader);
	}

	public URL getResource(String name) {
		URL foundRes = null;
		for (ClassLoader cl : classLoaders) {
			foundRes = cl.getResource(name);
			if (foundRes != null) {
				break;
			}
		}
		return foundRes;
	}

	/**
	 * This method looks for resources into the whole application and into all
	 * loaded modules. It returns a {@link ResourceInfo} object if it is able
	 * to find the requested resource.
	 *
	 * @param name the requested resource path
	 * @param openStream if <code>true</code>
	 * {@link ResourceInfo#getInputStream()} will return a already open
	 * {@link InputStream}
	 * @return a {@link ResourceInfo} object matching the requested resource
	 * if any found, <code>null</code> if no resource is found
	 */
	public ResourceInfo getResourceInfo(String name, boolean openStream) {

		ClassLoader foundCl = null;
		URL foundRes = null;
		for (ClassLoader cl : classLoaders) {
			foundRes = cl.getResource(name);
			if (foundRes != null) {
				foundCl = cl;
				break;
			}
		}

		if (foundRes != null) {
			ResourceInfo ri = new ResourceInfo();
			ri.setUrl(foundRes);
			ri.setSourceClassLoader(foundCl);
			if (foundCl instanceof ModuleClassLoader) {
				ModuleClassLoader mcl = (ModuleClassLoader) foundCl;
				ri.setLastModified(mcl.getCreationDate());
				ri.setLength(mcl.getResourceSize(name));
				if (ri.getLength() < 0) {
					ri.setLength(null);
				}
				if (openStream) {
					ri.setInputStream(mcl.getResourceAsStream(name));
				}
			} else {
				try {
					int length = foundRes.openConnection().getContentLength();
					ri.setLength(length >= 0 ? (long) length : null);
				} catch (IOException ex) {
					LoggerUtils.getLogger(GlobalClassLoader.class).log(Level.SEVERE, "Unexpected error retrieving resource's size", ex);
				}
				ri.setLastModified(creationDate);
				if (openStream) {
					ri.setInputStream(mainClassLoader.getResourceAsStream(name));
				}
			}
			return ri;
		}

		return null;
	}

	public ClassLoader getMainClassLoader() {
		return mainClassLoader;
	}

}
