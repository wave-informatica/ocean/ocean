/*
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.modules;

import java.io.IOException;
import java.io.InputStream;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import org.scannotation.archiveiterator.Filter;
import org.scannotation.archiveiterator.InputStreamWrapper;
import org.scannotation.archiveiterator.StreamIterator;

public class OceanJarIterator implements StreamIterator {

	private JarInputStream jar;
	private JarEntry next;
	private Filter filter;
	private boolean initial = true;
	private boolean closed = false;

	public OceanJarIterator(JarInputStream inputStream, Filter filter) {
		jar = inputStream;
		this.filter = filter;
	}

	private void setNext() {
		initial = true;
		try {
			if (next != null)
				jar.closeEntry();
			next = null;
			do {
				next = jar.getNextJarEntry();
			} while (next != null && (next.isDirectory() || (filter == null || !filter.accepts(next.getName()))));
			if (next == null) {
				close();
			}
		} catch (IOException e) {
			throw new RuntimeException("failed to browse jar", e);
		}
	}

	public InputStream next() {
		if (closed || (next == null && !initial))
			return null;
		setNext();
		if (next == null)
			return null;
		return new InputStreamWrapper(jar);
	}

	public void close() {
		try {
			closed = true;
			jar.close();
		} catch (IOException ignored) {

		}

	}

}
