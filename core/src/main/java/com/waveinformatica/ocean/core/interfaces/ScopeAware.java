package com.waveinformatica.ocean.core.interfaces;

import com.waveinformatica.ocean.core.controllers.scopes.AbstractScope;

public interface ScopeAware {
	
	public void setScope(AbstractScope scope);
	
}
