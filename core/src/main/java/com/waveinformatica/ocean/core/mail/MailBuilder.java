/*
* Copyright 2016, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */
package com.waveinformatica.ocean.core.mail;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.OperationContext;
import com.waveinformatica.ocean.core.controllers.dto.ResourceInfo;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.results.StreamResult;
import com.waveinformatica.ocean.core.controllers.scopes.EmailRootScope;
import com.waveinformatica.ocean.core.exceptions.OperationNotFoundException;
import com.waveinformatica.ocean.core.html.CSSUtils;
import com.waveinformatica.ocean.core.html.XHTMLUtils;
import com.waveinformatica.ocean.core.templates.TemplatesManager;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.DecoratorHelper;
import com.waveinformatica.ocean.core.util.GlobalClassLoader;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.ResourceRetriever;

import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * This class is a great helper class to build awesome HTML e-mail in the
 * easiest way possible. It takes views from templates, operations or strings,
 * can load attachments from files, operations and templates. It can try to
 * generate a default "text view" if none is provided, loads referred images as
 * inline attachments, adjusts CSS and inlines it to HTML tags.
 * 
 * <p>To easily send a HTML e-mail, do the following:
 * <pre class="language-java"><code class="language-java"> @Inject
 * private ObjectFactory factory;
 * ...
 * 
 * MailData model = new MailData();  // MailData is a class you define to provide a data model to the template
 * 
 * MailBuilder builder = factory.newInstance(MailBuilder.class);
 * builder.setFrom(...);
 * builder.setSubject(...);
 * builder.addRecipient(RecipientType.TO, ...);
 * builder.loadViewFromTempalte(MimeTypes.HTML, "/templates/...", model);
 * 
 * builder.send();</code></pre>
 *
 * @author Ivano
 */
public class MailBuilder {
	
	private static final Pattern urlPattern = Pattern.compile("^[a-zA-Z0-9-]+:.*$");

	@Inject
	protected ObjectFactory factory;

	@Inject
	protected CoreController controller;

	@Inject
	protected TemplatesManager templatesManager;

	@Inject
	protected GlobalClassLoader globalClassLoader;

	@Inject
	protected MailSender sender;

	@Inject
	protected Configuration configuration;

	@Inject
	protected ResourceRetriever retriever;

	protected final List<MailResource> alternateViews = new ArrayList<MailResource>();
	protected final List<MailResource> attachments = new ArrayList<MailResource>();
	protected final Map<RecipientType, List<InternetAddress>> recipients = new HashMap<RecipientType, List<InternetAddress>>();
	protected String subject;
	protected InternetAddress from;
	protected Message message;
	
	/**
	 * Do not use constructor directly, instantiate this class through {@link ObjectFactory#newInstance(Class)} instead.
	 */
	public MailBuilder () {
		
	}

	protected void check() {
		if (factory == null || controller == null || templatesManager == null) {
			throw new IllegalStateException("MailBuilder MUST be instantiated through ObjectFactory.newInstance(MailBuilder.class)");
		}
	}

	/**
	 * Loads a view from a freemarker template.
	 *
	 * @param mimeType the output mime type
	 * @param template the template path
	 * @param model the template processing root data model
	 * @throws TemplateException if the template processing producess an error
	 * @throws IOException if I/O errors occur
	 */
	public void loadViewFromTemplate(MimeTypes mimeType, String template, Object model) throws TemplateException, IOException {

		check();

		MimeTypes oldType = Context.getExpectedType();

		try {

			Context.setExpectedType(mimeType);

			Template tpl = templatesManager.getTemplate(template);

			StringWriter writer = new StringWriter();

			tpl.process(model, writer);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			baos.write(writer.toString().getBytes(Charset.forName("UTF-8")));

			MailResource resource = new MailResource();
			resource.setContentType(mimeType);
			resource.setContent(baos.toByteArray());

			alternateViews.add(resource);

		} finally {
			Context.setExpectedType(oldType);
		}

	}

	/**
	 * Loads a view from the result of an operation. It invokes the specified
	 * operation and renders the result in the same way as Ocean usually does.
	 *
	 * @param operation the operation to be invoked
	 * @param params the operation's parameters
	 * @param mimeTypes the requested mime types (like Accept HTTP header)
	 * @param extraObjects extra objects for invoking the operation (i.e.
	 * HttpServletRequest, etc.)
	 * @throws OperationNotFoundException if the specified operation does not
	 * exist
	 */
	public void loadViewFromOperation(String operation, Map<String, String[]> params, MimeTypes[] mimeTypes, Object... extraObjects) throws OperationNotFoundException {

		check();

		if (!operation.startsWith("/")) {
			operation = "/" + operation;
		}

		MimeTypes oldType = Context.getExpectedType();

		try {
			
			OperationContext opCtx = new OperationContext();

			List<Object> extras = new ArrayList<Object>();
			extras.add(opCtx);
			extras.addAll(Arrays.asList(extraObjects));
			
			EmailRootScope rootScope = factory.newInstance(EmailRootScope.class);
			rootScope.setSubject(subject);

			Object result = controller.executeOperation(rootScope, operation, params, extras.toArray(new Object[extras.size()]));

			if (result == null) {
				throw new IllegalStateException("Operation \"" + operation + "\" returned null result");
			}
			
			DecoratorHelper helper = factory.newInstance(DecoratorHelper.class);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			
			helper.decorateResult(result, baos, opCtx, mimeTypes);
			
			if (opCtx.getDecoratorInfo() != null) {
				
				MailResource resource = new MailResource();
				resource.setContentType(opCtx.getDecoratorInfo().getMimeType());
				resource.setContent(baos.toByteArray());

				alternateViews.add(resource);
				
			}

		} finally {
			Context.setExpectedType(oldType);
		}

	}

	/**
	 * Loads a view from the a specified string as content.
	 *
	 * @param mimeType the output mime type
	 * @param source the string source for view's content
	 */
	public void loadViewFromString(MimeTypes mimeType, String source) {

		check();

		MailResource resource = new MailResource();
		resource.setContentType(mimeType);
		resource.setContent(source.getBytes(Charset.forName("UTF-8")));

		alternateViews.add(resource);

	}

	/**
	 * Loads an attachment from a template as an attachment (not inline).
	 *
	 * @param mimeType the output attachment mime type
	 * @param template the template
	 * @param model the root data model to merge with the template
	 * @param name the attachment file name
	 * @throws TemplateException if a template processing error occurs
	 * @throws IOException if a I/O error occurs
	 */
	public void loadAttachmentFromTemplate(MimeTypes mimeType, String template, Object model, String name) throws TemplateException, IOException {

		check();

		MimeTypes oldType = Context.getExpectedType();

		try {

			Context.setExpectedType(mimeType);

			Template tpl = templatesManager.getTemplate(template);

			StringWriter writer = new StringWriter();

			tpl.process(model, writer);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			baos.write(writer.toString().getBytes(Charset.forName("UTF-8")));

			MailResource resource = new MailResource();
			resource.setContentType(mimeType);
			resource.setContent(baos.toByteArray());
			resource.setName(name + "." + mimeType.getExtensions()[0]);
			resource.setContentDisposition(ContentDisposition.ATTACHMENT);
			resource.setContentId(generateContentId(attachments.size() + 1));

			attachments.add(resource);

		} finally {
			Context.setExpectedType(oldType);
		}

	}

	/**
	 * Loads an attachment from the result of invoking an operation.
	 *
	 * @param operation the operation to be invoked
	 * @param params the operation's parameters
	 * @param mimeTypes the requested mime types
	 * @param extraObjects extra parameters for invoking the operation (i.e.
	 * HttpServletRequest, etc.)
	 * @throws OperationNotFoundException if the requested operation is not
	 * found
	 */
	public void loadAttachmentsFromOperation(String operation, Map<String, String[]> params, MimeTypes[] mimeTypes, Object... extraObjects) throws OperationNotFoundException {

		check();

		if (!operation.startsWith("/")) {
			operation = "/" + operation;
		}

		MimeTypes oldType = Context.getExpectedType();

		try {
			
			OperationContext opCtx = new OperationContext();
			
			List<Object> extras = new ArrayList<Object>();
			extras.add(opCtx);
			extras.addAll(Arrays.asList(extraObjects));

			Object result = controller.executeOperation(operation, params, extras.toArray(new Object[extras.size()]));

			if (result == null) {
				throw new IllegalStateException("Operation \"" + operation + "\" returned null result");
			}
			
			DecoratorHelper helper = factory.newInstance(DecoratorHelper.class);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			
			helper.decorateResult(result, baos, opCtx, mimeTypes);
			
			if (opCtx.getDecoratorInfo() != null) {
				
				MimeTypes mt = opCtx.getDecoratorInfo().getMimeType();
				
				MailResource resource = new MailResource();
				resource.setContentType(mt);
				resource.setContent(baos.toByteArray());
				resource.setContentDisposition(ContentDisposition.ATTACHMENT);
				resource.setContentId(generateContentId(attachments.size() + 1));
				if (result instanceof BaseResult) {
					if (result instanceof StreamResult && StringUtils.isNotBlank(((StreamResult) result).getContentType())) {
						boolean found = false;
						for (MimeTypes mte : MimeTypes.values()) {
							if (((StreamResult) result).getContentType().startsWith(mte.getMimeType())) {
								resource.setContentType(mte);
								found = true;
								break;
							}
						}
						if (!found && StringUtils.isNotBlank(((BaseResult) result).getName())) {
							for (MimeTypes mte : MimeTypes.values()) {
								for (String ext : mte.getExtensions()) {
									if (((BaseResult) result).getName().toLowerCase().endsWith("." + ext.toLowerCase())) {
										found = true;
										resource.setContentType(mte);
										break;
									}
								}
							}
						}
						if (!found) {
							resource.setRawContentType(((StreamResult)result).getContentType());
						}
					}
					if (StringUtils.isBlank(resource.getName())) {
						resource.setName(((BaseResult) result).getName());
					}
					if (resource.getContentType() != null) {
						boolean found = false;
						for (String ext : resource.getContentType().getExtensions()) {
							if (resource.getName().endsWith("." + ext)) {
								found = true;
								break;
							}
						}
						if (!found) {
							resource.setName(resource.getName() + "." + resource.getContentType().getExtensions()[0]);
						}
					}
				} else {
					resource.setName("attachment." + mt.getExtensions()[0]);
				}
	
				attachments.add(resource);
				
			}

		} finally {
			Context.setExpectedType(oldType);
		}

	}

	/**
	 * Loads an attachment from a file.
	 *
	 * @param contentType the file mime type
	 * @param fileName the file name
	 * @param file the file to be attached
	 * @throws IOException if an I/O exception occurs
	 */
	public void loadAttachmentFromFile(MimeTypes contentType, String fileName, File file) throws IOException {

		MailResource attachment = new MailResource();
		attachment.setContentDisposition(ContentDisposition.ATTACHMENT);
		attachment.setContentType(contentType);
		attachment.setName(fileName);
		attachment.setContentId(fileName + "@" + StringUtils.leftPad(String.valueOf(attachments.size() + 1), 3, "0"));

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		FileInputStream fis = new FileInputStream(file);
		byte[] buffer = new byte[4096];
		int read = 0;
		while ((read = fis.read(buffer)) > 0) {
			baos.write(buffer, 0, read);
		}
		fis.close();

		attachment.setContent(baos.toByteArray());

		attachments.add(attachment);

	}

	/**
	 * Adds one or more recipients to the mail message. If you want to add
	 * more recipients with a single call, please separate them with a
	 * <strong>semicolon</strong> (";").
	 *
	 * @param type the RecipientType (i.e. RecipientType.TO, etc.)
	 * @param address the recipient address(es) semicolon (";") separated
	 * @throws AddressException if parsing the recipients fails
	 */
	public void addRecipient(RecipientType type, String address) throws AddressException {
		for (String r : address.split(";")) {
			InternetAddress addr = new InternetAddress(r);
			List<InternetAddress> addresses = recipients.get(type);
			if (addresses == null) {
				addresses = new ArrayList<InternetAddress>();
				recipients.put(type, addresses);
			}
			addresses.add(addr);
		}
	}

	/**
	 * Adds a recipient to the mail message. This would be the preferred way
	 * to add recipients.
	 *
	 * @param type the RecipientType (i.e. RecipientType.TO, etc.)
	 * @param address the recipient address
	 */
	public void addRecipient(RecipientType type, InternetAddress address) {
		List<InternetAddress> addresses = recipients.get(type);
		if (addresses == null) {
			addresses = new ArrayList<InternetAddress>();
			recipients.put(type, addresses);
		}
		addresses.add(address);
	}

	/**
	 * Sets the mail's subject
	 *
	 * @param subject the mail's subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * Sets the mail's from address
	 *
	 * @param from the mail's from address
	 */
	public void setFrom(InternetAddress from) {
		this.from = from;
	}

	/**
	 * Sets the mail's from address
	 *
	 * @param from the mail's from address
	 * @throws AddressException if the address is not well-formed
	 */
	public void setFrom(String from) throws AddressException {
		this.from = new InternetAddress(from);
	}

	/**
	 * Builds the final Message. It invokes
	 * {@link MailBuilder#loadExtraData() loadExtraData()} method before.
	 *
	 * @return the final Message
	 * @throws MessagingException if an error occurs
	 */
	public Message build() throws MessagingException {

		loadExtraData();
		
		if (from == null) {
			setFrom(StringUtils.defaultIfBlank(configuration.getSmtpProperties().getProperty("mail.from"), "admin@localhost"));
		}

		Message msg = new MimeMessage((Session) null);
		msg.setSubject(subject);
		msg.setFrom(from);
		msg.setReplyTo(new InternetAddress[]{from});
		for (RecipientType type : recipients.keySet()) {
			msg.setRecipients(type, recipients.get(type).toArray(new InternetAddress[recipients.get(type).size()]));
		}

		Multipart bodyMultipart = new MimeMultipart("alternative");

		for (MailResource res : alternateViews) {
			MimeBodyPart body = new MimeBodyPart();
			if (res.getContentType() == MimeTypes.TEXT) {
				body.setText(new String(res.getContent(), Charset.forName("UTF-8")), "UTF-8");
			} else if ((StringUtils.isNotBlank(res.getRawContentType()) && res.getRawContentType().startsWith("text/")) ||
					(res.getContentType().getMimeType().startsWith("text/"))) {
				body.setContent(new String(res.getContent(), Charset.forName("UTF-8")), res.getContentType() != null ? res.getContentType().getMimeType() + "; charset=utf-8" : res.getRawContentType());
			} else {
				body.setContent(res.getContent(), res.getRawContentType());
			}
			bodyMultipart.addBodyPart(body);
		}

		MimeBodyPart altBodyPart = new MimeBodyPart();
		altBodyPart.setContent(bodyMultipart);

		Multipart mainMultipart = new MimeMultipart("related");
		mainMultipart.addBodyPart(altBodyPart);

		for (MailResource mr : attachments) {
			MimeBodyPart attachment = new MimeBodyPart();
			DataSource ds = new ByteArrayDataSource(mr.getContent(), StringUtils.isNotBlank(mr.getRawContentType()) ? mr.getRawContentType() : (mr.getContentType() != null ? mr.getContentType().getMimeType() : "application/unknown"));
			attachment.setDataHandler(new DataHandler(ds));
			attachment.setHeader("Content-ID", "<" + mr.getContentId() + ">");
			attachment.setFileName(mr.getName());
			attachment.setDisposition(mr.getContentDisposition().name().toLowerCase());
			mainMultipart.addBodyPart(attachment);
		}

		msg.setContent(mainMultipart);
		msg.setSentDate(new Date());

		this.message = msg;

		return msg;
	}

	/**
	 * Sends the built mail message. It invokes the
	 * {@link MailBuilder#build() build()} if the message needs to be built
	 * yet.
	 *
	 * @return the generated Message-ID
	 * @throws MessagingException if a MessagingException occurs
	 * @throws IOException if an I/O error occurs
	 */
	public String send() throws MessagingException, IOException {

		if (message == null) {
			build();
		}

		return sender.send(message);

	}

	/**
	 * Returns the built mail message, if any
	 *
	 * @return the built mail message or null if not built yet
	 */
	public Message getMessage() {
		return message;
	}
	
	protected void optimizeHtml(Element el) {
		
		NodeList children = el.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node child = children.item(i);
			if (child.getNodeType() == Node.ELEMENT_NODE) {
				optimizeHtml((Element) child);
			}
			else if (child.getNodeType() == Node.COMMENT_NODE) {
				if (StringUtils.isBlank(child.getTextContent()) || !child.getTextContent().startsWith("[")) {
					el.removeChild(child);
					i--;
				}
			}
			else if (child.getNodeType() == Node.TEXT_NODE) {
				child.setTextContent(child.getTextContent().replaceAll("\\s+", " "));
			}
		}
		
	}

	/**
	 * Prepares the HTML views and, if not present, tries to build a text
	 * view. In HTML views it applies the following actions:
	 * <ol>
	 * <li>inlines CSS rules to HTML tags (as inline style="..."
	 * attributes)</li>
	 * <li>modifies relative URLs in "A" href attributes to refer the
	 * application's URL</li>
	 * <li>includes IMG sources as inline attachments</li>
	 * </ol>
	 */
	protected void loadExtraData() {

		for (MailResource resource : new ArrayList<MailResource>(alternateViews)) {
			if (resource.getContentType() == MimeTypes.HTML) {

				List<MailResource> extraAttachments = new ArrayList<MailResource>();

				Document document = XHTMLUtils.toDocument(new String(resource.getContent(), Charset.forName("UTF-8")));

				optimizeHtml(document.getDocumentElement());
				
				if (document.getElementsByTagName("link").getLength() > 0
					  || document.getElementsByTagName("style").getLength() > 0) {
					// Inlineing stylesheets
					CSSUtils.inlineCss(document);
				}

				// Anchors
				NodeList anchors = document.getElementsByTagName("a");
				for (int i = 0; i < anchors.getLength(); i++) {
					Element a = (Element) anchors.item(i);
					String href = a.getAttribute("href");
					if (href != null) {
						try {
							new URL(href);
						} catch (MalformedURLException e) {
							Matcher m = urlPattern.matcher(href);
							if (!m.matches()) {
								String siteUrl = configuration.getSiteProperty("site.url", "http://localhost:8080/");
								if (siteUrl.endsWith("/")) {
									siteUrl = siteUrl.substring(0, siteUrl.length() - 1);
								}
								String context = Application.getInstance().getBaseUrl();
								if (siteUrl.indexOf(context) == -1) {
									siteUrl += context;
								}
								if (href.startsWith("/")) {
									href = href.substring(1);
								}
								href = siteUrl + "/" + href;
								a.setAttribute("href", href);
							}
						}
					}
				}

				// Loading images
				NodeList images = document.getElementsByTagName("img");
				Map<String, MailResource> loadedImages = new HashMap<>();
				for (int i = 0; i < images.getLength(); i++) {
					Element el = (Element) images.item(i);
					String src = el.getAttribute("src");
					MailResource addRes = loadedImages.get(src);
					if (addRes == null) {
						ResourceInfo res = retriever.getResource(src);
						if (res != null) {
							try {
								InputStream is = res.getInputStream();
								ByteArrayOutputStream baos = new ByteArrayOutputStream();
								byte[] buffer = new byte[4096];
								int read = 0;
								while ((read = is.read(buffer)) > 0) {
									baos.write(buffer, 0, read);
								}
								is.close();
								addRes = new MailResource();
								addRes.setContent(baos.toByteArray());
								addRes.setContentDisposition(ContentDisposition.INLINE);
								addRes.setContentId(generateContentId(attachments.size() + extraAttachments.size() + 1));
								addRes.setName(res.getName());
								addRes.setContentType(res.getMimeType());
								el.setAttribute("src", "cid:" + addRes.getContentId());
								extraAttachments.add(addRes);
								loadedImages.put(src, addRes);
							} catch (IOException ex) {
								LoggerUtils.getLogger(MailBuilder.class).log(Level.SEVERE, null, ex);
							}
						} else {
							LoggerUtils.getLogger(MailBuilder.class).log(Level.WARNING, "Unable to retrieve resource from URL \"" + src + "\"");
						}
					}
					else {
						el.setAttribute("src", "cid:" + addRes.getContentId());
					}
				}

				resource.setContent(XHTMLUtils.toHTML(document).getBytes(Charset.forName("UTF-8")));
				attachments.addAll(extraAttachments);

				boolean textVersionFound = false;
				for (MailResource txtRes : alternateViews) {
					if (txtRes.getContentType() == MimeTypes.TEXT) {
						textVersionFound = true;
						break;
					}
				}

				if (!textVersionFound) {
					MailResource txtRes = new MailResource();
					txtRes.setContentType(MimeTypes.TEXT);
					txtRes.setContent(XHTMLUtils.toSimpleText((Element) document.getElementsByTagName("body").item(0)).getBytes(Charset.forName("UTF-8")));
					alternateViews.add(0, txtRes);
				}

			}
		}

	}

	/**
	 * Generates a content-id
	 *
	 * @param index the index of the content
	 * @return a content-id string
	 */
	protected String generateContentId(int index) {
		String host = "localhost";
		try {
			host = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException ex) {
			LoggerUtils.getLogger(MailBuilder.class).log(Level.WARNING, "Error retrieving host name, falling back to localhost", ex);
		}

		StringBuilder contentId = new StringBuilder();
		contentId.append("attachment.");
		contentId.append(StringUtils.leftPad(String.valueOf(index), 3, '0'));
		contentId.append('@');
		contentId.append(host);

		return contentId.toString();
	}

	protected static class MailResource {

		private MimeTypes contentType;
		private String rawContentType;
		private byte[] content;
		private String name;
		private ContentDisposition contentDisposition;
		private String contentId;

		public MimeTypes getContentType() {
			return contentType;
		}

		public void setContentType(MimeTypes contentType) {
			this.contentType = contentType;
			this.rawContentType = contentType != null ? contentType.getMimeType() : null;
		}

		public String getRawContentType() {
			return rawContentType;
		}

		public void setRawContentType(String rawContentType) {
			this.rawContentType = rawContentType;
			contentType = null;
			if (StringUtils.isNotBlank(rawContentType)) {
				for (MimeTypes mt : MimeTypes.values()) {
					if (rawContentType.trim().startsWith(mt.getMimeType())) {
						contentType = mt;
						break;
					}
				}
			}
		}

		public byte[] getContent() {
			return content;
		}

		public void setContent(byte[] content) {
			this.content = content;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public ContentDisposition getContentDisposition() {
			return contentDisposition;
		}

		public void setContentDisposition(ContentDisposition contentDisposition) {
			this.contentDisposition = contentDisposition;
		}

		public String getContentId() {
			return contentId;
		}

		public void setContentId(String contentId) {
			this.contentId = contentId;
		}

	}

	protected static enum ContentDisposition {
		ATTACHMENT,
		INLINE
	}

}
