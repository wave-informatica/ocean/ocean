/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.tasks;

import java.util.logging.Level;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.cdi.CDIHelper;
import com.waveinformatica.ocean.core.cdi.CDISession;
import com.waveinformatica.ocean.core.cdi.GlobalSessionScopeContext;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.InstanceListener;
import com.waveinformatica.ocean.core.controllers.events.OperationErrorEvent;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.LoggerUtils;

/**
 * <p>This utility class is used whenever a Runnable is required. This class ensures
 * that all Ocean's features are available to subsequent executed code and all needed
 * cleanings are made at the end of the operation.</p>
 *
 * <p>It is important to use this class for example to correctly manage instances
 * of <code>EntityManager</code>s, which will be automatically closed after operation.</p>
 *
 * @author Ivano
 */
public abstract class OceanRunnable implements Runnable {
	
	private static final ThreadLocal<HttpSession> propagatedSession = new ThreadLocal<HttpSession>();
	
	@Inject
	protected CoreController coreController;
	
	private final CDISession cdiSession;
	
	private ServletContext context;
	private HttpSession httpSession;
	
	public OceanRunnable() {
		
		this.context = Application.getInstance().getContext();
		
		HttpServletRequest request = Context.get() != null ? Context.get().getRequest() : null;
		if (request != null) {
			this.httpSession = request.getSession();
			this.cdiSession = GlobalSessionScopeContext.getCDISession(this.httpSession);
		}
		else if (this.httpSession == null && propagatedSession.get() != null) {
			httpSession = propagatedSession.get();
			cdiSession = GlobalSessionScopeContext.getCDISession(this.httpSession);
		}
		else {
			httpSession = null;
			cdiSession = new CDISession();
		}
	}
	
	public OceanRunnable(CDISession cdiSession) {
		this.cdiSession = cdiSession;
	}
	
	@Override
	public final void run() {
		
		propagatedSession.set(httpSession);
		
		if (coreController == null) {
			coreController = Application.getInstance().getCoreController();
		}
		
		Context.setCoreController(coreController);
		
		Context.init(context);
		
		CDIHelper.startCdiContexts(cdiSession, httpSession);
		
		try {
			
			execute();
			
		} catch (Throwable t) {
			
			LoggerUtils.getLogger(this.getClass()).log(Level.SEVERE, "Error in OceanRunnable instance", t);
			
			OperationErrorEvent evt = new OperationErrorEvent(this, t);
			coreController.dispatchEvent(evt);
			
		} finally {
			Context.clear();
			Context.setCoreController(null);
			CDIHelper.stopCdiContexts();
			InstanceListener.get().clear();
		}
		
	}
	
	/**
	 * Implement this method instead of the final <code>run</code>.
	 */
	public abstract void execute();
	
	/**
	 * This method wraps any {@link Runnable} instance in an {@link OceanRunnable} instance.
	 * 
	 * @param task the runnable to be wrapped
	 * @return the {@link OceanRunnable} instance
	 */
	public static OceanRunnable create(Runnable task) {
		
		DefaultOceanRunnable dor = Application.getInstance().getObjectFactory().newInstance(DefaultOceanRunnable.class);
		dor.init(task);
		return dor;
		
	}
	
	/**
	 * The default {@link OceanRunnable} implementation which wraps another {@link Runnable}. Use the {@link #create(Runnable) create(Runnable)} method
	 * for convenience.
	 * 
	 * @author ivano
	 *
	 */
	public static class DefaultOceanRunnable extends OceanRunnable {
		
		private Runnable task;

		void init(Runnable task) {
			this.task = task;
		}

		@Override
		public void execute() {
			task.run();
		}
		
	}
	
}
