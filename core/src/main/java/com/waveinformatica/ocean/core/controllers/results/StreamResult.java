/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.results;

import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.waveinformatica.ocean.core.interfaces.InputStreamProvider;

/**
 *
 * @author Ivano
 */
public class StreamResult extends BaseResult implements InputStreamProvider {

    private InputStream stream;
    private String contentType;
    private Long contentLength;
    private final Map<String, Header> headers = new HashMap<String, Header>();

    public InputStream getStream() {
        return stream;
    }

    public void setStream(InputStream stream) {
        this.stream = stream;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getContentLength() {
        return contentLength;
    }

    public void setContentLength(Long contentLength) {
        this.contentLength = contentLength;
    }

    public Set<Header> getHeaders() {

        Set<Header> headers = new TreeSet<StreamResult.Header>();

        headers.addAll(this.headers.values());

        return headers;

    }

    public void setHeader(String name, String value) {
        Header h = new Header(name);
        h.values.add(value);
        headers.put(name, h);
    }

    public void addHeader(String name, String value) {
        Header h = headers.get(name);
        if (h == null) {
            h = new Header(name);
            headers.put(name, h);
        }
        h.values.add(value);
    }

    public static class Header implements Comparable<Header> {

        private final String name;
        private final List<String> values = new LinkedList<String>();

        public Header(String name) {
            super();
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public String[] getValues() {
            return values.toArray(new String[values.size()]);
        }

        @Override
        public int compareTo(Header arg0) {
            return name.compareTo(arg0.name);
        }

    }

}
