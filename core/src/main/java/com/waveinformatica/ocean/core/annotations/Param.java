/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.annotations;

import com.waveinformatica.ocean.core.converters.ITypeConverter;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines associations between operation parameters and request parameters by name, to allow automagic mapping.
 * 
 * @author Ivano
 * @see Operation
 */
@Target({ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Param {

	/**
	 * @return the name of the request parameter
	 */
	String value();

	/**
	 * The designated converter to fill the parameter. This option will force to use
	 * the specified converter instead of any automatically determined (if a converter
	 * is available for the parameter type).
	 * 
	 * @return the designated converter to fill the parameter
	 */
	Class<? extends ITypeConverter> converter() default ITypeConverter.class;

	/**
	 *
	 * @return a list of strings representing the default value for the
	 * field (managed by the handler)
	 */
	String[] defaultValue() default {};
}
