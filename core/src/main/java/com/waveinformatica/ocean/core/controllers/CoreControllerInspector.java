/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;

/**
 *
 * @author Ivano
 */
public class CoreControllerInspector extends CoreController.ControllerAccessor {

	private CoreController controller;

	protected CoreControllerInspector(CoreController controller) {
		this.controller = controller;
	}

	// TODO: review
	public Set<String> getOperationNames() {
		
		TreeSet<String> res = new TreeSet<String>();
		
		Map<Class<?>, Map<String, List<OperationInfo>>> scopedHandlers = getOperationHandlers(controller);
		for (Map<String, List<OperationInfo>> entry : scopedHandlers.values()) {
			res.addAll(entry.keySet());
		}
		
		return res;
	}
	
	public List<OperationInfo> getOperationInfos(String operation) {
		
		List<OperationInfo> result = new ArrayList<>();
		
		Map<Class<?>, Map<String, List<OperationInfo>>> scopedHandlers = getOperationHandlers(controller);
		for (Map<String, List<OperationInfo>> entry : scopedHandlers.values()) {
			List<OperationInfo> ops = entry.get(operation);
			if (ops != null) {
				result.addAll(ops);
			}
		}
		
		return result;
		
	}

}
