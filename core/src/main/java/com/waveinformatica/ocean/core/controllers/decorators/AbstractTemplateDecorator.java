/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.decorators;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.controllers.dto.OperationContext;
import com.waveinformatica.ocean.core.templates.TemplatesManager;

import freemarker.template.Template;

/**
 *
 * @author Ivano
 */
public abstract class AbstractTemplateDecorator<T> implements IDecorator<T> {
    
    @Inject
    private TemplatesManager templatesManager;
    
    private Map<String, Object> extraObjects;
    
    protected Template loadTemplate(String name) {
        return templatesManager.getTemplate(name);
    }

	public Map<String, Object> getExtraObjects() {
		return extraObjects;
	}

	public void setExtraObjects(Map<String, Object> extraObjects) {
		this.extraObjects = extraObjects;
	}
	
	public OperationContext getOperationContext() {
		return (OperationContext) (extraObjects != null ? extraObjects.get("operationContext") : null);
	}
    
}
