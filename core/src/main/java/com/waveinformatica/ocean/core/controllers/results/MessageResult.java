/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.results;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.util.OceanConversation;

/**
 *
 * @author Ivano
 */
public class MessageResult extends BaseResult {
	
	@Inject
	private OceanConversation conversation;
    
    private String message;
    private MessageType type;
    private String confirmAction;
    private String cancelAction;
    private String exception;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getConfirmAction() {
        return confirmAction;
    }

    public void setConfirmAction(String confirmAction) {
        this.confirmAction = confirmAction;
    }

    public String getCancelAction() {
        return cancelAction;
    }

    public void setCancelAction(String cancelAction) {
        this.cancelAction = cancelAction;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }
    
    public void setException(Exception e) {
        StringWriter strWr = new StringWriter();
        PrintWriter writer = new PrintWriter(strWr);
        e.printStackTrace(writer);
        exception = strWr.toString();
    }
    
    public String getFullOperation(String op) {
    	if (!conversation.isActive()) {
    		return op;
    	}
    	return op + (op.contains("?") ? "&" : "?") + "cid=" + conversation.getId();
    }
    
    public static enum MessageType {
        INFO,
        WARNING,
        CRITICAL,
        CONFIRM
    }
    
}
