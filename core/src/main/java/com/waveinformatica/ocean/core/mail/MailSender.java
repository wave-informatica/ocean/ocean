/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.mail;

import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.MailSenderComponent;
import com.waveinformatica.ocean.core.annotations.Preferred;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.IMailSenderComponent;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.events.MailSentEvent;
import com.waveinformatica.ocean.core.modules.ComponentsBin;
import com.waveinformatica.ocean.core.util.PersistedProperties;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
@ApplicationScoped
public class MailSender {
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private CoreController controller;
	
	@Inject
	private Configuration configuration;
	
	@Inject @Preferred
	@MailSenderComponent
	private ComponentsBin senders;
	
	public String send(Message msg) throws MessagingException, IOException {
		
		Set<Class<?>> components = senders.getComponents(MailSenderComponent.class);
		
		String messageId = null;
		
		for (Class<?> c : components) {
			IMailSenderComponent imsc = (IMailSenderComponent) factory.newInstance(c);
			messageId = imsc.send(msg);
			if (StringUtils.isNotBlank(messageId)) {
				break;
			}
		}
		
		if (StringUtils.isBlank(messageId)) {
			
			PersistedProperties smtpProperties = configuration.getSmtpProperties();

			Properties props = new Properties();
			for (Object k : smtpProperties.keySet()) {
				String key = k.toString();
				if (smtpProperties.get(k) != null && StringUtils.isNotBlank(smtpProperties.get(k).toString())) {
					props.setProperty(key, smtpProperties.get(k).toString().trim());
				}
			}

			Session session;

			if (StringUtils.isNotBlank(props.getProperty("mail.user"))) {
				props.setProperty("mail.smtp.auth", "true");
				final String userName = props.getProperty("mail.user");
				final String password = props.getProperty("mail.password");
				props.remove("mail.user");
				props.remove("mail.password");
				session = Session.getInstance(props, new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(userName, password);
					}
				});
			}
			else {
				session = Session.getInstance(props);
			}

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			msg.writeTo(baos);

			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());

			MimeMessage attachedMsg = new MimeMessage(session, bais);

			Transport.send(attachedMsg);
			
			messageId = ((MimeMessage) msg).getMessageID();
			
		}
		
		if (StringUtils.isNotBlank(messageId)) {
			
			MailSentEvent evt = new MailSentEvent(this, msg);
			controller.dispatchEvent(evt);
			
		}
		
		return messageId;
		
	}
	
}
