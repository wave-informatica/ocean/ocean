package com.waveinformatica.ocean.core.controllers.webservice;

public interface WebServiceScope {
	
	public String getNamespace();
	
	public String getName();
	
	public String getServicePath();
	
}
