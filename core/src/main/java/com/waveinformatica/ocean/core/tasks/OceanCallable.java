/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.tasks;

import java.util.concurrent.Callable;
import java.util.logging.Level;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.cdi.CDIHelper;
import com.waveinformatica.ocean.core.cdi.CDISession;
import com.waveinformatica.ocean.core.cdi.GlobalSessionScopeContext;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.InstanceListener;
import com.waveinformatica.ocean.core.controllers.events.OperationErrorEvent;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.LoggerUtils;

/**
 * <p>This utility class is used whenever a Callable is required. This class ensures
 * that all Ocean's features are available to subsequent executed code and all needed
 * cleanings are made at the end of the operation.</p>
 *
 * <p>It is important to use this class for example to correctly manage instances
 * of {@link EntityManager}s, which will be automatically closed after operation.</p>
 *
 * @author Ivano
 */
public abstract class OceanCallable<V> implements Callable<V> {
	
	private static final ThreadLocal<HttpSession> propagatedSession = new ThreadLocal<HttpSession>();
	
	@Inject
	protected CoreController coreController;
	
	private final CDISession cdiSession;
	
	private ServletContext context;
	private HttpSession httpSession;
	
	public OceanCallable() {
		
		this.context = Application.getInstance().getContext();
		
		HttpServletRequest request = Context.get().getRequest();
		if (request != null) {
			this.httpSession = request.getSession();
			this.cdiSession = GlobalSessionScopeContext.getCDISession(this.httpSession);
		}
		else if (this.httpSession == null && propagatedSession.get() != null) {
			httpSession = propagatedSession.get();
			cdiSession = GlobalSessionScopeContext.getCDISession(this.httpSession);
		}
		else {
			httpSession = null;
			cdiSession = new CDISession();
		}
	}
	
	public OceanCallable(CDISession cdiSession) {
		this.cdiSession = cdiSession;
	}
	
	@Override
	public final V call() throws Exception {
		
		propagatedSession.set(httpSession);
		
		if (coreController == null) {
			coreController = Application.getInstance().getCoreController();
		}
		
		Context.setCoreController(coreController);
		
		Context.init(context);
		
		CDIHelper.startCdiContexts(cdiSession, httpSession);
		
		try {
			
			return execute();
			
		} catch (Throwable t) {
			
			LoggerUtils.getLogger(this.getClass()).log(Level.SEVERE, "Error in OceanRunnable instance", t);
			
			OperationErrorEvent evt = new OperationErrorEvent(this, t);
			coreController.dispatchEvent(evt);
			
			throw new Exception(t);
			
		} finally {
			Context.clear();
			Context.setCoreController(null);
			CDIHelper.stopCdiContexts();
			InstanceListener.get().clear();
		}
		
	}
	
	/**
	 * Implement this method instead of the final <code>call</code>.
	 */
	public abstract V execute() throws Exception;
	
	/**
	 * This method wraps any {@link Callable} instance in an {@link OceanCallable} instance.
	 * 
	 * @param task the callable to be wrapped
	 * @return the {@link OceanCallable} instance
	 */
	@SuppressWarnings("unchecked")
	public static <V> OceanCallable<V> create(Callable<V> task) {
		
		DefaultOceanCallable<V> dor = Application.getInstance().getObjectFactory().newInstance(DefaultOceanCallable.class);
		dor.init(task);
		return dor;
		
	}
	
	/**
	 * The default {@link OceanCallable} implementation which wraps another {@link Callable}. Use the {@link #create(Callable) create(Callable)} method
	 * for convenience.
	 * 
	 * @author ivano
	 *
	 */
	public static class DefaultOceanCallable<T> extends OceanCallable<T> {
		
		private Callable<T> task;

		void init(Callable<T> task) {
			this.task = task;
		}

		@Override
		public T execute() throws Exception {
			return task.call();
		}
		
	}
	
}
