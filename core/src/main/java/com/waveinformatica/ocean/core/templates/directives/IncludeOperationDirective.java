/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.templates.directives;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.annotations.FreemarkerDirective;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.OperationContext;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.results.WebPageResult;
import com.waveinformatica.ocean.core.exceptions.OperationNotAuthorizedException;
import com.waveinformatica.ocean.core.exceptions.OperationNotFoundException;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.DecoratorHelper;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.ParametersBuilder;
import com.waveinformatica.ocean.core.util.UrlBuilder;

import freemarker.core.Environment;
import freemarker.template.SimpleCollection;
import freemarker.template.SimpleSequence;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateModelIterator;

/**
 *
 * @author Ivano
 */
@FreemarkerDirective("invoke")
public class IncludeOperationDirective implements TemplateDirectiveModel {
    
    private static final String VAR_NAME = "operation";
    
    @Inject
    private CoreController coreController;
    
    @Inject
    private ObjectFactory factory;
    
    @Override
    public void execute(Environment e, Map map, TemplateModel[] tms, TemplateDirectiveBody tdb) throws TemplateException, IOException {
        
        if (map.get(VAR_NAME) == null) {
            throw new TemplateModelException(VAR_NAME + " parameter is required");
        }
        if (tms.length > 0) {
            throw new TemplateModelException("No loop variables are supported");
        }
        
        // Getting operation name
        String operationName = ((TemplateModel) map.get(VAR_NAME)).toString();
        if (!operationName.startsWith("/")) {
            operationName = "/" + operationName;
        }
        
        // Build operation parameters
        UrlBuilder operationBuilder = new UrlBuilder(operationName);
        ParametersBuilder params = new ParametersBuilder();
        params.replaceAll(operationBuilder.getParameters());
        params.replaceAll(Context.get().getRequest().getParameterMap());
        for (Object k : map.keySet()) {
            List<String> values = new ArrayList<String>();
            if (map.get(k) instanceof SimpleSequence) {
                SimpleSequence seq = (SimpleSequence) map.get(k);
                int size = seq.size();
                for (int i = 0; i < size; i++) {
                    values.add(seq.get(i).toString());
                }
            }
            else if (map.get(k) instanceof SimpleCollection) {
                TemplateModelIterator iter = ((SimpleCollection) map.get(k)).iterator();
                while (iter.hasNext()) {
                    values.add(iter.next().toString());
                }
            }
            else {
                values.add(map.get(k).toString());
            }
            params.replace(k.toString(), values.toArray(new String[values.size()]));
        }
        
        // Call operation
        OperationContext opCtx = new OperationContext();
        Object result = null;
        try {
            result = coreController.executeOperation(operationBuilder.getUrl(false), params.build(), opCtx, Context.get().getRequest());
            // FIXME: usiamo gli eventi, magari...
            if (result instanceof BaseResult) {
                ((BaseResult) result).setFullResult(false);
            }
            if (result instanceof WebPageResult) {
                ((WebPageResult)result).setContextPath(Application.getInstance().getBaseUrl());
            }
        } catch (OperationNotFoundException ex) {
            throw new TemplateModelException("Included operation was not found", ex);
        } catch (OperationNotAuthorizedException ex) {
            LoggerUtils.getCoreLogger().log(Level.WARNING, "Included operation was not authorized", ex);
            return;
        }
        
        DecoratorHelper helper = factory.newInstance(DecoratorHelper.class);
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
        helper.decorateResult(result, baos, opCtx, MimeTypes.HTML);
        
        e.getOut().write(new String(baos.toByteArray(), "UTF-8"));
        
    }
    
}
