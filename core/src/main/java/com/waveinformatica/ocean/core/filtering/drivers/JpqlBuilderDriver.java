package com.waveinformatica.ocean.core.filtering.drivers;

import java.util.Arrays;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.filtering.QueryFilter;

public class JpqlBuilderDriver implements QueryBuilderDriver<String> {
	
	private final String prefix;
	private final ValuesManager params;
	
	public static final JpqlBuilderDriver COMPOSER = new JpqlBuilderDriver(null, null);
	
	public JpqlBuilderDriver(ValuesManager params, String prefix) {
		this.params = params;
		this.prefix = prefix;
	}
	
	@Override
	public Class<String> getQueryType() {
		return String.class;
	}
	
	@Override
	public String toQuery(DataReference dr) {
		if (dr instanceof DataPath) {
			return prefix + "." + ((DataPath) dr).getPath();
		}
		else if (dr instanceof DataValue) {
			dr = params.getParam((DataValue) dr);
			return ":" + ((DataValue) dr).getName();
		}
		else if (dr instanceof DataQuery) {
			return (String) ((DataQuery) dr).getValue();
		}
		return null;
	}

	@Override
	public String equal(DataReference v1, DataReference v2) {
		return toQuery(v1) + " = " + toQuery(v2);
	}

	@Override
	public String gt(DataReference v1, DataReference v2) {
		return toQuery(v1) + " > " + toQuery(v2);
	}

	@Override
	public String gte(DataReference v1, DataReference v2) {
		return toQuery(v1) + " >= " + toQuery(v2);
	}

	@Override
	public String lt(DataReference v1, DataReference v2) {
		return toQuery(v1) + " < " + toQuery(v2);
	}

	@Override
	public String lte(DataReference v1, DataReference v2) {
		return toQuery(v1) + " <= " + toQuery(v2);
	}

	@Override
	public String isNull(DataReference v) {
		return toQuery(v) + " is null";
	}

	@Override
	public String isNotNull(DataReference v) {
		return toQuery(v) + " is not null";
	}

	@Override
	public String endsWith(DataReference v1, DataReference v2) {
		return toQuery(v1) + " like " + concat(new DataQuery<String>("'%'"), v2);
	}

	@Override
	public String startsWith(DataReference v1, DataReference v2) {
		return toQuery(v1) + " like " + concat(v2, new DataQuery<String>("'%'"));
	}

	@Override
	public String contains(DataReference v1, DataReference v2) {
		return toQuery(v1) + " like " + concat(new DataQuery<String>("'%'"), v2, new DataQuery<String>("'%'"));
	}

	@Override
	public String between(DataReference p, DataReference v1, DataReference v2) {
		return toQuery(p) + " between " + toQuery(v1) + " and " + toQuery(v2);
	}

	@Override
	public String in(DataReference v, DataReference list) {
		return toQuery(v) + " in " + toQuery(list);
	}

	@Override
	public String concat(DataReference... v) {
		StringBuilder builder = new StringBuilder();
		for (DataReference dr : v) {
			builder.append(',');
			builder.append(toQuery(dr));
		}
		return "CONCAT(" + builder.substring(1) + ")";
	}

	@Override
	public String lower(DataReference v) {
		return "LOWER(" + toQuery(v) + ")";
	}

	@Override
	public String upper(DataReference v) {
		return "UPPER(" + toQuery(v) + ")";
	}

	@Override
	public String and(String... q) {
		return "(" + StringUtils.join(Arrays.asList(q), ") and (") + ")";
	}

	@Override
	public String or(String... q) {
		return "(" + StringUtils.join(Arrays.asList(q), ") or (") + ")";
	}

	@Override
	public String not(String q) {
		return "not (" + q + ")";
	}

	@Override
	public DataQuery<String> toDataQuery(String query) {
		return new DataQuery<String>(query);
	}
	
	public static String getCondition(QueryFilter<String> filter, String prefix) {
		String condition = filter.getWhereClause();
		if (StringUtils.isNotBlank(condition)) {
			return prefix + condition;
		}
		else {
			return "";
		}
	}

	@Override
	public String function(String name, DataReference... params) {
		StringBuilder builder = new StringBuilder();
		builder.append("function('" + name + "'");
		for (DataReference dr : params) {
			builder.append(',');
			builder.append(toQuery(dr));
		}
		builder.append(")");
		return builder.toString();
	}

	@Override
	public String cast(DataReference v, String type) {
		return "cast(" + toQuery(v) + " as " + type + ")";
	}

}
