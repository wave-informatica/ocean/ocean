/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.util;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.waveinformatica.ocean.core.annotations.ExcludeSerialization;

/**
 *
 * @author Ivano
 */
public class JsonUtils {
	
	private static SimpleDateFormat getDateFormat() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		return dateFormat;
	}
	
	public static GsonBuilder getBuilder() {
		
		GsonBuilder builder = new GsonBuilder();
		
		builder.registerTypeAdapter(Date.class, new JsonDateUTCSerializer());
		builder.registerTypeAdapter(Date.class, new JsonDateUTCDeserializer());
		builder.registerTypeHierarchyAdapter(Date.class, new JsonDateUTCAdapter());
		builder.addSerializationExclusionStrategy(new JsonExcludeSerializationAnnotation());
		
		return builder;
		
	}
	
	public static String formatDate(Date date) {
		if (date == null) {
			return null;
		}
		return getDateFormat().format(date);
	}
	
	public static Date parseDate(String date) throws ParseException {
		if (StringUtils.isBlank(date)) {
			return null;
		}
		return getDateFormat().parse(date);
	}
	
	private JsonUtils() {
		
	}
	
	public static class JsonDateUTCAdapter extends TypeAdapter<Date> {

		@Override
		public void write(JsonWriter writer, Date t) throws IOException {
			writer.value(formatDate(t));
		}

		@Override
		public Date read(JsonReader reader) throws IOException {
			try {
				if (reader.peek() == JsonToken.NULL) {
					reader.nextNull();
					return null;
				}
				return parseDate(reader.nextString());
			} catch (ParseException ex) {
				throw new IOException(ex);
			}
		}
		
	}
	
	public static class JsonDateUTCSerializer implements JsonSerializer<Date> {
		@Override
		public JsonElement serialize(Date t, Type type, JsonSerializationContext jsc) {
			return new JsonPrimitive(getDateFormat().format(t));
		}
	}
	
	public static class JsonDateUTCDeserializer implements JsonDeserializer<Date> {
		
		@Override
		public Date deserialize(JsonElement je, Type type, JsonDeserializationContext jdc) throws JsonParseException {
			try {
				return getDateFormat().parse(je.getAsString());
			} catch (ParseException e) {
				throw new JsonParseException(e);
			}
		}
		
	}
	
	// TODO: ExclusionStrategy for Gson have to be plugable from modules
	public static class JsonExcludeSerializationAnnotation implements ExclusionStrategy {
		
		@Override
		public boolean shouldSkipField(FieldAttributes fa)
		{
			return fa.getAnnotation(ExcludeSerialization.class) != null;
		}
		
		@Override
		public boolean shouldSkipClass(Class<?> type)
		{
			return false;
		}
	}
	
}
