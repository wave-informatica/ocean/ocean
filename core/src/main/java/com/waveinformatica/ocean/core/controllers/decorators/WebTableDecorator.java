/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.controllers.decorators;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.annotations.ResultDecorator;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.core.controllers.results.WebPageResult;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.Results;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
@ResultDecorator(
	  classes = TableResult.class,
	  mimeTypes = {MimeTypes.HTML, MimeTypes.PDF}
)
public class WebTableDecorator extends AbstractTemplateDecorator<TableResult> {
	
	private static final Logger logger = LoggerUtils.getCoreLogger();
	
	@Inject
	private ObjectFactory factory;
	
	@Override
	public void decorate(TableResult tabRes, HttpServletRequest request, HttpServletResponse response, MimeTypes type) {
		
		WebPageDecorator wpd = factory.newInstance(WebPageDecorator.class);
		wpd.setExtraObjects(getExtraObjects());
		
		WebPageResult wrapperResult = wrapResult(request, tabRes);
		
		wpd.decorate(wrapperResult, request, response, type); //To change body of generated methods, choose Tools | Templates.
	}
	
	@Override
	public void decorate(TableResult tabRes, OutputStream out, MimeTypes type) {
		
		WebPageDecorator wpd = factory.newInstance(WebPageDecorator.class);
		wpd.setExtraObjects(getExtraObjects());
		
		WebPageResult wrapperResult = wrapResult(Context.get().getRequest(), tabRes);
		
		wpd.decorate(wrapperResult, out, type); //To change body of generated methods, choose Tools | Templates.
	}
	
	protected WebPageResult wrapResult(HttpServletRequest request, TableResult tabRes) {
		
		StringBuilder pathBase = new StringBuilder(request.getRequestURI().substring(request.getContextPath().length() + 1));
		Enumeration<String> names = request.getParameterNames();
		if (names.hasMoreElements()) {
			pathBase.append("?");
		}
		while (names.hasMoreElements()) {
			String n = names.nextElement();
			if (!n.equals(TableResult.getPageParameter()) && !n.equals("fw:sort")) {
				for (String value : request.getParameterValues(n)) {
					if (pathBase.charAt(pathBase.length() - 1) != '?') {
						pathBase.append('&');
					}
					try {
						pathBase.append(URLEncoder.encode(n, "UTF-8"));
						pathBase.append('=');
						pathBase.append(URLEncoder.encode(value, "UTF-8"));
					} catch (UnsupportedEncodingException ex) {
						Logger.getLogger(WebTableDecorator.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}
		}
		
		tabRes.setPathBase(pathBase.toString());
		if (StringUtils.isNotBlank(request.getParameter(TableResult.getPageParameter()))) {
			try {
				tabRes.setPage(Integer.parseInt(request.getParameter(TableResult.getPageParameter())));
			} catch (NumberFormatException e) {
				logger.warning("Unable to parse \"" + TableResult.getPageParameter() + "\" request parameter: \"" + request.getParameter(TableResult.getPageParameter()) + "\"");
			}
		}
		
		tabRes.analyzeData();
		
		TableResult.WrapperResultBuilder wrapperBuilder = factory.newInstance(tabRes.getWrapperResultBuilders().get(MimeTypes.HTML));
		WebPageResult wrapperResult = (WebPageResult) wrapperBuilder.getWrapperResult(tabRes);
		
		wrapperResult.setContextPath(Application.getInstance().getBaseUrl());
		wrapperResult.setName(tabRes.getName());
		
		Results.copyHeadResources(tabRes, wrapperResult);
		
		return wrapperResult;
		
	}
	
}
