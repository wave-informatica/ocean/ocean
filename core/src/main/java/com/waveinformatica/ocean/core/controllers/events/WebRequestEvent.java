/*
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.events;

import com.waveinformatica.ocean.core.controllers.IEventListener;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ivano
 */
public class WebRequestEvent extends Event {
    
    private final HttpServletRequest request;
    private final HttpServletResponse response;
    private IEventListener requestManagedBy;
    
    public WebRequestEvent(HttpServlet sender, HttpServletRequest req, HttpServletResponse resp) {
        super(sender,
                CoreEventName.WEB_REQUEST.name(),
                new String[]{ req.getMethod().toUpperCase() }
        );
        this.request = req;
        this.response = resp;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    public IEventListener getRequestManagedBy() {
        return requestManagedBy;
    }

    public void setRequestManagedBy(IEventListener requestManagedBy) {
        this.requestManagedBy = requestManagedBy;
    }
    
}
