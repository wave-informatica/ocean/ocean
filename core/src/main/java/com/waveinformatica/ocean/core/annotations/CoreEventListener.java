/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.annotations;

import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation defines a Event Listener. The EventListener must implement the
 * {@link IEventListener} interface too.
 * 
 * @author Ivano
 * 
 * @see IEventListener
 */
@OceanComponent(mustExtend = IEventListener.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface CoreEventListener {
    
    /**
     * Filter event names to listen to (required)
     * 
     * @return the event names
     */
    CoreEventName[] eventNames();
    
    /**
     * Filter source class names to listen to (optional)
     * 
     * @return the source class names
     */
    String[] sourceNames() default {};
    
    /**
     * Filter event by flags (optional)
     * 
     * @return the flags
     */
    String[] flags() default {};
    
}
