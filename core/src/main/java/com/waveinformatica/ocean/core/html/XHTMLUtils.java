/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.html;

import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.logging.Level;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.DomSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.waveinformatica.ocean.core.util.LoggerUtils;

import se.fishtank.css.selectors.Selectors;
import se.fishtank.css.selectors.dom.W3CNode;

/**
 * This class is useful to parse, print and edit HTML documents in a simple way.
 * 
 * @author Ivano
 */
public class XHTMLUtils {
	
	private static final String EOL = "\r\n";
	private static final String INDENT = "    ";
	private static final String DASHED_LINE = "-----------------------------------------";
	
	private static final List<String> EMPTY_ELEMENTS = Arrays.asList("AREA", "BASE", "BR", "COL", "EMBED",
			"HR", "IMG", "INPUT", "KEYGEN","LINK", "META", "PARAM", "SOURCE", "TRACK", "WBR");
	
	protected static CleanerProperties getCleanerProperties() {
		
		CleanerProperties props = new CleanerProperties();
		/* Meglio senza cambiare nulla...
		props.setDeserializeEntities(true);
		props.setAdvancedXmlEscape(true);
	    props.setTranslateSpecialEntities(false);
	    props.setAllowInvalidAttributeNames(true);
	    props.setRecognizeUnicodeChars(true);*/
	    
	    return props;
		
	}
	
	public static Document toDocument(String html) {
		
		CleanerProperties props = getCleanerProperties();
	    
		HtmlCleaner cleaner = new HtmlCleaner(props);
		TagNode cleaned = cleaner.clean(html);
		Document xmlDoc = null;
		try {
			
			xmlDoc = new DomSerializer(cleaner.getProperties(), false).createDOM(cleaned);
			
		} catch (ParserConfigurationException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
		}
		
		return xmlDoc;
		
	}
	
	public static Document toDocument(Reader reader) throws IOException {
		
		CleanerProperties props = getCleanerProperties();
		
		HtmlCleaner cleaner = new HtmlCleaner(props);
		TagNode cleaned = cleaner.clean(reader);
		Document xmlDoc = null;
		try {
			
			xmlDoc = new DomSerializer(cleaner.getProperties(), false).createDOM(cleaned);
			
		} catch (ParserConfigurationException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
		}
		
		return xmlDoc;
		
	}
	
	public static String toText(Node node) {
		
		StringBuilder builder = new StringBuilder();
		
		switch (node.getNodeType()) {
		case Node.TEXT_NODE:
			builder.append(node.getTextContent());
			break;
		case Node.ELEMENT_NODE:
			if (!EMPTY_ELEMENTS.contains(node.getNodeName().toUpperCase())) {
				NodeList children = node.getChildNodes();
				for (int i = 0; i < children.getLength(); i++) {
					builder.append(toText(children.item(i)));
				}
			}
			break;
		case Node.CDATA_SECTION_NODE:
			CDATASection cdata = (CDATASection) node;
			builder.append(cdata.getData());
			break;
		}
		
		return builder.toString();
		
	}
	
	public static String toSimpleText(Element doc) {
		return toSimpleText(doc, new ConversionInfo());
	}
	
	private static String toSimpleText(Element doc, ConversionInfo context) {
		
		StringBuilder builder = new StringBuilder();
		
		NodeList children = doc.getChildNodes();
		int len = children.getLength();
		for (int i = 0; i < len; i++) {
			Node node = children.item(i);
			if (node.getNodeType() == Node.TEXT_NODE) {
				builder.append(node.getNodeValue());
			}
			else if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getNodeName().equalsIgnoreCase("P")) {
					builder.append(EOL);
					builder.append(toSimpleText((Element) node, context));
					builder.append(EOL);
				}
				else if (node.getNodeName().equalsIgnoreCase("BR")) {
					builder.append(EOL);
				}
				else if (node.getNodeName().equalsIgnoreCase("IMG")) {
					Node alt = node.getAttributes().getNamedItem("alt");
					builder.append("[immagine");
					if (alt != null && alt.getNodeValue() != null && alt.getNodeValue().trim().length() > 0) {
						builder.append(' ');
						builder.append(alt.getNodeValue().trim());
					}
					builder.append(']');
				}
				else if (node.getNodeName().equalsIgnoreCase("A")) {
					builder.append(toSimpleText((Element) node, context));
					Node href = node.getAttributes().getNamedItem("href");
					if (href != null && href.getNodeValue() != null && href.getNodeValue().trim().length() > 0) {
						builder.append(" (");
						builder.append(href.getNodeValue().trim());
						builder.append(")");
					}
				}
				else if (node.getNodeName().equalsIgnoreCase("UL") ||
					  node.getNodeName().equalsIgnoreCase("OL")) {
					builder.append(EOL);
					context.push();
					context.get().setOrdered(node.getNodeName().equalsIgnoreCase("OL"));
					context.get().setIndent(context.get().getIndent() + 1);
					context.get().setIndex(0);
					builder.append(toSimpleText((Element) node, context));
					context.pop();
					builder.append(EOL);
				}
				else if (node.getNodeName().equalsIgnoreCase("LI")) {
					for (int j = 0; j < context.get().getIndent(); j++) {
						builder.append(INDENT);
					}
					if (context.get().isOrdered()) {
						context.get().setIndex(context.get().getIndex() + 1);
						builder.append(context.get().getIndex());
						builder.append(". ");
					}
					else {
						builder.append(" * ");
					}
					builder.append(toSimpleText((Element) node, context));
					builder.append(EOL);
				}
				else if (node.getNodeName().equalsIgnoreCase("HR")) {
					builder.append(EOL);
					builder.append(DASHED_LINE);
					builder.append(EOL);
				}
				else if (node.getNodeName().equalsIgnoreCase("INPUT") ||
					  node.getNodeName().equalsIgnoreCase("TEXTAREA") ||
					  node.getNodeName().equalsIgnoreCase("BUTTON") ||
					  node.getNodeName().equalsIgnoreCase("STYLE") ||
					  node.getNodeName().equalsIgnoreCase("SCRIPT") ||
					  node.getNodeName().equalsIgnoreCase("IFRAME")) {
					// Ignore tags
				}
				else {
					builder.append(toSimpleText((Element) node, context));
				}
			}
		}
		
		return builder.toString().trim();
		
	}
	
	public static String toHTML(Document doc) {
		
		StringBuilder builder = new StringBuilder();
		
		builder.append(serializeDocType(doc.getDoctype()));
		
		if (builder.length() > 0) {
			builder.append("\n");
		}
		
		return builder.toString() + toHTML((Node) doc.getDocumentElement(), false);
		
	}
	
	public static String toHTML(Node node, boolean xml) {
		
		StringBuilder builder = new StringBuilder();
		
		switch (node.getNodeType()) {
		case Node.TEXT_NODE:
			builder.append(node.getTextContent());
			break;
		case Node.ATTRIBUTE_NODE:
			builder.append(' ');
			builder.append(node.getNodeName());
			if (StringUtils.isNotBlank(node.getNodeValue()) || xml) {
				builder.append("=\"");
				if (StringUtils.isNotBlank(node.getNodeValue())) {
					builder.append(node.getNodeValue());
				}
				builder.append('\"');
			}
			break;
		case Node.ELEMENT_NODE:
			builder.append('<');
			builder.append(node.getNodeName());
			NamedNodeMap map = node.getAttributes();
			for (int i = 0; i < map.getLength(); i++) {
				builder.append(toHTML(map.item(i), xml));
			}
			if (EMPTY_ELEMENTS.contains(node.getNodeName().toUpperCase()) && xml) {
				builder.append("/");
			}
			builder.append('>');
			if (!EMPTY_ELEMENTS.contains(node.getNodeName().toUpperCase())) {
				NodeList children = node.getChildNodes();
				for (int i = 0; i < children.getLength(); i++) {
					builder.append(toHTML(children.item(i), xml));
				}
				builder.append("</");
				builder.append(node.getNodeName());
				builder.append('>');
			}
			break;
		case Node.CDATA_SECTION_NODE:
			builder.append("<![CDATA[");
			CDATASection cdata = (CDATASection) node;
			builder.append(cdata.getData());
			builder.append("]]>");
			break;
		case Node.COMMENT_NODE:
			Comment comment = (Comment) node;
			builder.append("<!--");
			builder.append(comment.getData());
			builder.append("-->");
			break;
		default:
			System.err.println("Unrecognized node type: " + node.getNodeType());
			break;
		}
		
		return builder.toString();
		
	}
	
	public static String toXHTML(Document doc) {
		
		return toXHTML(doc.getDocumentElement());
		
	}
	
	public static String toXHTML(Element doc) {

		doc.setAttribute("xmlns", "http://www.w3.org/1999/xhtml");
		return toHTML(doc, true);
		
		/*DOMSource domSource = new DOMSource(doc);
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.transform(domSource, result);
		return writer.toString();*/
	}
	
	public static String getBase(Document doc, String defaultValue) {
		
		Selectors selectors = new Selectors(new W3CNode(doc));
		
		List<Node> nodes = selectors.querySelectorAll("base");
		if (nodes != null && !nodes.isEmpty()) {
			String base = null;
			for (Node n : nodes) {
				if (n instanceof Element) {
					Element el = (Element) n;
					if (base == null) {
						base = el.getAttribute("href");
						if (base != null) {
							defaultValue = base.trim();
							break;
						}
					}
				}
			}
		}
		
		return defaultValue;
	}
	
	public static String serializeDocType(DocumentType docType) {
		
		if (docType == null) {
			return "";
		}
		
		StringBuilder builder = new StringBuilder();
		
		builder.append("<!DOCTYPE ");
		
		builder.append(docType.getName());
		
		if (StringUtils.isNotBlank(docType.getPublicId())) {
			builder.append(" PUBLIC \"");
			builder.append(docType.getPublicId());
			builder.append("\"");
		}
		
		if (StringUtils.isNotBlank(docType.getSystemId())) {
			builder.append(" \"");
			builder.append(docType.getSystemId());
			builder.append("\"");
		}
		
		builder.append(">");
		
		return builder.toString();
		
	}
	
	private static class ConversionInfo {
		
		private final Stack<ConversionContext> contexts = new Stack<ConversionContext>();
		
		public ConversionInfo() {
			contexts.push(new ConversionContext());
		}
		
		public ConversionContext get() {
			return contexts.peek();
		}
		
		public void push() {
			contexts.push(new ConversionContext(get()));
		}
		
		public void pop() {
			contexts.pop();
		}
		
	}
	
	private static class ConversionContext {
		
		private boolean ordered = false;
		private int indent = 0;
		private int index = 1;
		
		public ConversionContext() {
			
		}
		
		public ConversionContext(ConversionContext source) {
			this.ordered = source.ordered;
			this.indent = source.indent;
			this.index = source.index;
		}
		
		public boolean isOrdered() {
			return ordered;
		}
		
		public void setOrdered(boolean ordered) {
			this.ordered = ordered;
		}
		
		public int getIndent() {
			return indent;
		}
		
		public void setIndent(int indent) {
			this.indent = indent;
		}
		
		public int getIndex() {
			return index;
		}
		
		public void setIndex(int index) {
			this.index = index;
		}
		
	}
	
}
