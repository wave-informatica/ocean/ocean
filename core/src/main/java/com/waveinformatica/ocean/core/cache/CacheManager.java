/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.cache;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;
import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.CacheProvider;
import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.annotations.Preferred;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.ModuleEvent;
import com.waveinformatica.ocean.core.modules.ComponentsBin;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.PersistedProperties;

@ApplicationScoped
public class CacheManager implements ComponentsBin.ComponentLoaded, ComponentsBin.ComponentUnloaded {
	
	@Inject @Preferred
	@CacheProvider("")
	private ComponentsBin cacheProviders;
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private Configuration config;
	
	private final Map<String, Class<ICacheProvider<?>>> providers = new HashMap<String, Class<ICacheProvider<?>>>();
	private final Map<String, OceanCache> caches = new HashMap<String, OceanCache>();
	
	@PostConstruct
	public void init() {
		cacheProviders.register(this);
	}
	
	public Set<String> getProviderNames() {
		return new TreeSet<String>(providers.keySet());
	}
	
	public ICacheProvider getProvider(String name) {
		if (StringUtils.isBlank(name)) {
			name = "Default";
		}
		Class<? extends ICacheProvider> cl = providers.get(name);
		if (cl != null) {
			return factory.newInstance(cl);
		}
		else {
			return factory.newInstance(DefaultCacheProvider.class);
		}
	}
	
	public List<RegisteredCache> getRegisteredCaches() {
		List<RegisteredCache> result = new ArrayList<CacheManager.RegisteredCache>(caches.size());
		for (Entry<String, OceanCache> entry : caches.entrySet()) {
			RegisteredCache rc = new RegisteredCache(entry.getKey(), entry.getValue());
			ICacheProvider provider = getProvider(rc.getProviderName());
			rc.setConfigurationOk(provider.checkConfiguration(getProviderConfiguration(rc.getName())));
			rc.setSize((long) entry.getValue().size());
			rc.setMapType(entry.getValue().getMapClass().getName());
			result.add(rc);
		}
		Collections.sort(result);
		return result;
	}
	
	public void register(OceanCache cache) {
		caches.put(cache.getName(), cache);
	}
	
	public void unregister(OceanCache cache) {
		caches.remove(cache.getName());
	}
	
	public Object getProviderConfiguration(String cacheName) {
		OceanCache cache = caches.get(cacheName);
		if (cache != null) {
			PersistedProperties props = config.getCachingProperties();
			String v = props.getProperty(cacheName);
			if (StringUtils.isBlank(v)) {
				return null;
			}
			Gson gson = JsonUtils.getBuilder().create();
			CacheElement ce = gson.fromJson(v, CacheElement.class);
			ICacheProvider provider = getProvider(ce.getProviderName());
			Type gt = null;
			Type[] types = provider.getClass().getGenericInterfaces();
			for (Type t : types) {
				if (t instanceof ParameterizedType) {
					ParameterizedType pt = (ParameterizedType) t;
					if (pt.getRawType() == ICacheProvider.class) {
						gt = pt.getActualTypeArguments()[0];
						break;
					}
				}
			}
			if (gt != null) {
				Object conf = null;
				if (gt != Object.class) {
					conf = gson.fromJson(ce.getConfigJson(), gt);
				}
				return conf;
			}
		}
		return null;
	}
	
	public void updateCache(String name) {
		OceanCache cache = caches.get(name);
		if (cache != null) {
			PersistedProperties props = config.getCachingProperties();
			String v = props.getProperty(name);
			if (StringUtils.isBlank(v)) {
				return;
			}
			Gson gson = JsonUtils.getBuilder().create();
			CacheElement ce = gson.fromJson(v, CacheElement.class);
			ICacheProvider provider = getProvider(ce.getProviderName());
			Type gt = null;
			Type[] types = provider.getClass().getGenericInterfaces();
			for (Type t : types) {
				if (t instanceof ParameterizedType) {
					ParameterizedType pt = (ParameterizedType) t;
					if (pt.getRawType() == ICacheProvider.class) {
						gt = pt.getActualTypeArguments()[0];
						break;
					}
				}
			}
			if (gt != null) {
				Object conf = null;
				if (gt != Object.class) {
					conf = gson.fromJson(ce.getConfigJson(), gt);
				}
				cache.setProviderName(ce.getProviderName());
				cache.setMap(provider.getCache(conf));
			}
		}
	}
	
	public void updateCaches() {
		for (String k : caches.keySet()) {
			updateCache(k);
		}
	}

	@Override
	public void componentUnloaded(Class<? extends Annotation> annotation, Class<?> componentClass) {
		CacheProvider ann = componentClass.getAnnotation(CacheProvider.class);
		providers.remove(ann.value());
	}

	@Override
	public void componentLoaded(Class<? extends Annotation> annotation, Class<?> componentClass) {
		CacheProvider ann = componentClass.getAnnotation(CacheProvider.class);
		providers.put(ann.value(), (Class<ICacheProvider<?>>) componentClass);
	}
	
	public static class RegisteredCache implements Comparable<RegisteredCache> {
		
		private String name;
		private String providerName;
		private OceanCache cache;
		private boolean configurationOk;
		private Long size;
		private String mapType;
		
		public RegisteredCache() {
			super();
		}

		public RegisteredCache(String name, OceanCache cache) {
			super();
			this.name = name;
			this.cache = cache;
			this.providerName = cache.getProviderName();
		}

		public String getName() {
			return name;
		}
		
		public void setName(String name) {
			this.name = name;
		}
		
		public String getProviderName() {
			return providerName;
		}

		public void setProviderName(String providerName) {
			this.providerName = providerName;
		}

		public OceanCache getCache() {
			return cache;
		}
		
		public void setCache(OceanCache cache) {
			this.cache = cache;
		}

		public boolean isConfigurationOk() {
			return configurationOk;
		}

		public void setConfigurationOk(boolean configurationOk) {
			this.configurationOk = configurationOk;
		}

		public Long getSize() {
			return size;
		}

		public void setSize(Long size) {
			this.size = size;
		}

		public String getMapType() {
			return mapType;
		}

		public void setMapType(String mapType) {
			this.mapType = mapType;
		}

		@Override
		public int compareTo(RegisteredCache o) {
			return name.compareTo(o.name);
		}
		
	}
	
	@CoreEventListener(eventNames = { CoreEventName.MODULE_LOADED, CoreEventName.MODULE_UNLOADED })
	public static class CacheModuleListener implements IEventListener<ModuleEvent> {

		@Inject
		private CacheManager manager;
		
		@Override
		public void performAction(ModuleEvent event) {
			manager.updateCaches();
		}
		
	}
	
}
