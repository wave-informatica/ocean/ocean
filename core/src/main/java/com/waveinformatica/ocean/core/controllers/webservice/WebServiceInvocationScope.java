package com.waveinformatica.ocean.core.controllers.webservice;

import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.controllers.scopes.AbstractScope;

public class WebServiceInvocationScope extends AbstractScope {
	
	private OperationInfo operationInfo;

	public OperationInfo getOperationInfo() {
		return operationInfo;
	}

	public void setOperationInfo(OperationInfo opInfo) {
		this.operationInfo = opInfo;
	}
	
}
