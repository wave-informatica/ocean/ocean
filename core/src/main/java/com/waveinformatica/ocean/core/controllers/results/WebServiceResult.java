package com.waveinformatica.ocean.core.controllers.results;

import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.controllers.scopes.ScopeChain;
import com.waveinformatica.ocean.core.controllers.webservice.WebServiceInvocationScope;

public class WebServiceResult extends BaseResult {
	
	private OperationInfo operationInfo;
	private WebServiceInvocationScope scope;
	private ScopeChain scopeChain;
	private Object result;

	public OperationInfo getOperationInfo() {
		return operationInfo;
	}

	public void setOperationInfo(OperationInfo operationInfo) {
		this.operationInfo = operationInfo;
	}

	public WebServiceInvocationScope getScope() {
		return scope;
	}

	public void setScope(WebServiceInvocationScope scope) {
		this.scope = scope;
	}

	public ScopeChain getScopeChain() {
		return scopeChain;
	}

	public void setScopeChain(ScopeChain scopeChain) {
		this.scopeChain = scopeChain;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}
	
}
