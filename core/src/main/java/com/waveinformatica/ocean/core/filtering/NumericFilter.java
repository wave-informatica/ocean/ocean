/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.filtering;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.filtering.drivers.DataPath;
import com.waveinformatica.ocean.core.filtering.drivers.DataValue;
import com.waveinformatica.ocean.core.filtering.drivers.QueryBuilderDriver;
import com.waveinformatica.ocean.core.filtering.drivers.ValuesManager;

/**
 *
 * @author Ivano
 */
public class NumericFilter implements Filter {

	private static final Pattern pattern = Pattern.compile("((([+-]?[0-9]*)(\\.\\.)([+-]?[0-9]*))|(=[+-]?[0-9,\\.]+)|\\s+)");
	private static final Pattern fullPattern = Pattern.compile("^((([+-]?[0-9]*)(\\.\\.)([+-]?[0-9]*))|(=[+-]?[0-9,\\.]+)|\\s+)*$");
	
	@Override
	public QueryFilter getJpqlFilter(String path, String filterValue, Class<?> expectedType) {
		
		if (StringUtils.isBlank(filterValue)) {
			return null;
		}
		
		if (!fullPattern.matcher(filterValue).matches()) {
			return null;
		}
		
		Matcher matcher = pattern.matcher(filterValue);
		
		QueryFilter qf = new QueryFilter();
		
		StringBuilder builder = new StringBuilder();
		int paramIndex = 0;
		String paramName = NumericFilter.class.getSimpleName().toLowerCase() + "_" + path.replaceAll("[^a-zA-Z0-9]", "_");
		
		while (matcher.find()) {
			if (StringUtils.isNotBlank(matcher.group(6))) {
				StringBuilder lBuilder = new StringBuilder();
				String[] parts = matcher.group(6).substring(1).split(",+");
				for (String p : parts) {
					if (StringUtils.isBlank(p)) {
						continue;
					}
					if (lBuilder.length() > 0) {
						lBuilder.append(", ");
					}
					lBuilder.append(':');
					lBuilder.append(paramName);
					lBuilder.append(paramIndex);
					qf.set(paramName + paramIndex, getValue(p, expectedType));
					paramIndex++;
				}
				if (lBuilder.length() > 0) {
					if (builder.length() > 0) {
						builder.append(" or ");
					}
					builder.append(path);
					builder.append(" in (");
					builder.append(lBuilder.toString());
					builder.append(")");
				}
			}
			else if (StringUtils.isNotBlank(matcher.group(4))) {
				StringBuilder lBuilder = new StringBuilder();
				if (StringUtils.isNotBlank(matcher.group(3))) {
					lBuilder.append(path);
					lBuilder.append(" >= :");
					lBuilder.append(paramName);
					lBuilder.append(paramIndex);
					qf.set(paramName + paramIndex, getValue(matcher.group(3), expectedType));
					paramIndex++;
				}
				if (StringUtils.isNotBlank(matcher.group(5))) {
					if (lBuilder.length() > 0) {
						lBuilder.append(" and ");
					}
					lBuilder.append(path);
					lBuilder.append(" <= :");
					lBuilder.append(paramName);
					lBuilder.append(paramIndex);
					qf.set(paramName + paramIndex, getValue(matcher.group(5), expectedType));
					paramIndex++;
				}
				if (lBuilder.length() > 0) {
					if (builder.length() > 0) {
						builder.append(" or ");
					}
					builder.append("(");
					builder.append(lBuilder.toString());
					builder.append(")");
				}
			}
		}
		
		if (builder.length() > 0) {
			qf.setCondition(builder.toString());
			return qf;
		}
		else {
			return null;
		}
		
	}

	@Override
	public String getTemplate() {
		return "/templates/filters/stdFilter.tpl";
	}

	@Override
	public String getHelpTemplate() {
		return "/templates/filters/numericFilterHelp.tpl";
	}
	
	private Number getValue(String value, Class<?> expectedType) {
		if (expectedType.isAssignableFrom(Double.class)) {
			return Double.parseDouble(value);
		}
		else if (expectedType.isAssignableFrom(Float.class)) {
			return Float.parseFloat(value);
		}
		else if (expectedType.isAssignableFrom(Long.class)) {
			return Long.parseLong(value);
		}
		else if (expectedType.isAssignableFrom(Integer.class)) {
			return Integer.parseInt(value);
		}
		else if (expectedType.isAssignableFrom(Short.class)) {
			return Short.parseShort(value);
		}
		else {
			return null;
		}
	}

	@Override
	public void init(Object result, Field field) {
		
	}

	@Override
	public <T> QueryFilter<T> getQueryFilter(String path, String filterValue, Class<?> expectedType, QueryBuilderDriver<T> driver,
			ValuesManager values) {
		
		if (StringUtils.isBlank(filterValue)) {
			return null;
		}
		
		if (!fullPattern.matcher(filterValue).matches()) {
			return null;
		}
		
		Matcher matcher = pattern.matcher(filterValue);
		
		List<T> conditions = new ArrayList<>();
		
		while (matcher.find()) {
			if (StringUtils.isNotBlank(matcher.group(6))) {
				List<Number> data = new ArrayList<>();
				String[] parts = matcher.group(6).substring(1).split(",+");
				for (String p : parts) {
					if (StringUtils.isBlank(p)) {
						continue;
					}
					data.add(getValue(p, expectedType));
				}
				if (!data.isEmpty()) {
					conditions.add(driver.in(new DataPath(path), values.getParam(new DataValue(data))));
				}
			}
			else if (StringUtils.isNotBlank(matcher.group(4))) {
				List<T> lConditions = new ArrayList<>();
				if (StringUtils.isNotBlank(matcher.group(3))) {
					lConditions.add(driver.gte(new DataPath(path), values.getParam(new DataValue(getValue(matcher.group(3), expectedType)))));
				}
				if (StringUtils.isNotBlank(matcher.group(5))) {
					lConditions.add(driver.lte(new DataPath(path), values.getParam(new DataValue(getValue(matcher.group(5), expectedType)))));
				}
				if (!lConditions.isEmpty()) {
					conditions.add(driver.and(lConditions.toArray((T[]) Array.newInstance(driver.getQueryType(), lConditions.size()))));
				}
			}
		}
		
		if (conditions.isEmpty()) {
			return null;
		}
		else {
			QueryFilter<T> qf = new QueryFilter<>(values);
			qf.setWhereClause(driver.or(conditions.toArray((T[]) Array.newInstance(driver.getQueryType(), conditions.size()))));
			return qf;
		}
		
	}
	
}
