/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */
package com.waveinformatica.ocean.core.operations;

import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.annotations.RootOperation;
import com.waveinformatica.ocean.core.annotations.SkipAuthorization;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.MenuResult;
import com.waveinformatica.ocean.core.controllers.results.MessageResult;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.scopes.RootScope;
import com.waveinformatica.ocean.core.controllers.scopes.ScopeChain;
import com.waveinformatica.ocean.core.mail.MailBuilder;
import com.waveinformatica.ocean.core.operations.dto.ErrorMailModel;
import com.waveinformatica.ocean.core.util.I18N;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 * @author Luca Lattore
 */
@OperationProvider
public class Home {

	@Inject
	private ObjectFactory objectFactory;

	@Inject
	private I18N i18n;

	@Inject
	private com.waveinformatica.ocean.core.security.SecurityManager sm;

	@Inject
	private Configuration config;

	@Operation("")
	public MenuResult home(ScopeChain chain, HttpServletRequest request) {

		MenuResult result = objectFactory.newInstance(MenuResult.class);

		result.initFromScope(chain.getCurrent().getParent());

		result.setProvider(this);

		return result;

	}

	@Operation(value = "i18n/messages", defaultOutputType = MimeTypes.JSON)
	@SkipAuthorization
	public Map<String, String> messages() {

		return i18n.getAllMessages();

	}

	@Operation(value = "exceptions/sendError")
	public MessageResult sendError(
		  @Param("exception") String exception,
		  @Param("method") String method,
		  @Param("parameters") String parameters,
		  HttpServletRequest request
	) throws TemplateException, IOException, AddressException, MessagingException {

		MessageResult result = objectFactory.newInstance(MessageResult.class);

		Properties props = config.getLoggingProperties();
		String recipient = props.getProperty("ocean.errors.feedback.recipient");

		if (StringUtils.isBlank(recipient)) {
			result.setType(MessageResult.MessageType.CRITICAL);
			result.setMessage("Sorry, no mail address for error feedbacks is set.");
			return result;
		}

		ErrorMailModel model = new ErrorMailModel();
		model.setLoggedUser(sm.getPrincipal());
		model.setHref(request != null ? request.getHeader("Referer") : null);
		model.setException(exception);
		model.setMethod(method);
		model.setRequestInput(parameters);

		MailBuilder builder = objectFactory.newInstance(MailBuilder.class);

		builder.setFrom(props.getProperty("ocean.errors.feedback.application.sender"));
		builder.setSubject("Ocean Errors Feedback - " + props.getProperty("ocean.errors.feedback.application.code"));
		builder.addRecipient(Message.RecipientType.TO, recipient);
		builder.loadViewFromTemplate(MimeTypes.HTML, "/templates/errors/mail-error-feedback.tpl", model);

		builder.loadAttachmentFromTemplate(MimeTypes.TEXT, "/templates/stringTemplate.tpl", new StringWrapper(parameters), "requestInput");
		builder.loadAttachmentFromTemplate(MimeTypes.TEXT, "/templates/stringTemplate.tpl", new StringWrapper(exception), "exception");

		builder.send();

		result.setType(MessageResult.MessageType.CONFIRM);
		result.setMessage("Error details successfully sent to the application's support team");
		result.setConfirmAction("");

		return result;

	}

	public static class StringWrapper {

		private final String value;

		public StringWrapper(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

	}

}
