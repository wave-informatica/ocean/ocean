/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.controllers.decorators;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.ResultDecorator;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.util.LoggerUtils;

@ResultDecorator(classes = String.class, mimeTypes = {
		MimeTypes.HTML,
		MimeTypes.CSS,
		MimeTypes.JAVASCRIPT,
		MimeTypes.CSV,
		MimeTypes.EML,
		MimeTypes.ICS,
		MimeTypes.JSON,
		MimeTypes.TEXT,
		MimeTypes.XHTML,
		MimeTypes.XML
})
public class StringDecorator implements IDecorator<String> {

	@Override
	public void decorate(String result, OutputStream out, MimeTypes type) {
		if (StringUtils.isNotBlank(result)) {
			try {
				out.write(result.getBytes("UTF-8"));
			} catch (UnsupportedEncodingException e) {
				LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
			} catch (IOException e) {
				LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
			}
		}
	}

	@Override
	public void decorate(String result, HttpServletRequest request, HttpServletResponse response, MimeTypes type) {
		
		response.setContentType(type.getMimeType() + "; charset=utf-8");
		
		try {
			
			decorate(result, response.getOutputStream(), type);
			
		} catch (IOException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
		}
		
	}

}
