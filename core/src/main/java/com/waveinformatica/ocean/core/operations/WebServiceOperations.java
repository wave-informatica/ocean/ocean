package com.waveinformatica.ocean.core.operations;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.wsdl.Definition;
import javax.wsdl.WSDLException;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLWriter;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.SkipAuthorization;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.CoreController.ParameterInfo;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.OperationContext;
import com.waveinformatica.ocean.core.controllers.events.BeforeOperationEvent;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.results.WebServiceResult;
import com.waveinformatica.ocean.core.controllers.scopes.AbstractScope;
import com.waveinformatica.ocean.core.controllers.webservice.WebServiceInvocationScope;
import com.waveinformatica.ocean.core.controllers.webservice.WebServiceScope;
import com.waveinformatica.ocean.core.controllers.webservice.WsdlBuilder;
import com.waveinformatica.ocean.core.converters.ITypeConverter;
import com.waveinformatica.ocean.core.exceptions.OperationNotAuthorizedException;
import com.waveinformatica.ocean.core.security.SecurityManager;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.OperationParameters;
import com.waveinformatica.ocean.core.util.XmlUtils;
import com.waveinformatica.ocean.schemas.soapenv.Envelope;

@OperationProvider(inScope = WebServiceInvocationScope.class, namespace = "webservice")
public class WebServiceOperations {
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private XmlUtils xmlUtils;
	
	@Inject
	private SecurityManager sm;
	
	@Inject
	private CoreController controller;
	
	@Operation("wsdl")
	@SkipAuthorization
	public void wsdl(WebServiceInvocationScope scope, HttpServletResponse response) {
		
		try {
			
			WsdlBuilder builder = factory.newInstance(WsdlBuilder.class);
			Definition def = builder.build((WebServiceScope) scope.getParent());
			
			WSDLFactory factory = WSDLFactory.newInstance();
			WSDLWriter writer = factory.newWSDLWriter();
			
			response.setContentType("text/xml");
			
			writer.writeWSDL(def, response.getWriter());
			
		} catch (WSDLException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
		} catch (IOException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
		}
	}
	
	@Operation(value = "invoke", defaultOutputType = MimeTypes.XML)
	@SkipAuthorization
	public Object invoke(WebServiceInvocationScope scope, HttpServletRequest request, HttpServletResponse response) {
		
		Throwable error = null;
		
		try {
			
			Unmarshaller unmarshaller = xmlUtils.getContext().createUnmarshaller();
			
			Envelope env = ((JAXBElement<Envelope>) unmarshaller.unmarshal(new BufferedReader(request.getReader()))).getValue();
			
			Element el = (Element) env.getBody().getAny().get(0);
			
			if (!el.getLocalName().equals(scope.getOperationInfo().getMethod().getName() + "Request")) {
				throw new RuntimeException("Invalid request");
			}
			
			if (!sm.authorize(scope.getOperationInfo(), scope.getChain())) {
				throw new OperationNotAuthorizedException("Operation not authorized");
			}
			
			Object[] extraObjects = new Object[] { request, response };
			Map<String, String[]> args = new HashMap<>();
			
			BeforeOperationEvent boe = new BeforeOperationEvent(this, scope.getOperationInfo(), new HashMap<String, String[]>(), extraObjects);
			controller.dispatchEvent(boe);

			boe.getExtraObjects().add(scope.getOperationInfo());
			boe.getExtraObjects().add(scope.getChain());
			boe.getExtraObjects().add(new OperationParameters(args));
			for (AbstractScope s : scope.getChain()) {
				boe.getExtraObjects().add(s);
			}
			if (sm.isLogged()) {
				boe.getExtraObjects().add(sm.getPrincipal());
			}
			extraObjects = boe.getExtraObjects().toArray();
			
			OperationContext ctx = null;
			for (Object o : boe.getExtraObjects()) {
				if (o instanceof OperationContext) {
					ctx = (OperationContext) o;
					break;
				}
			}
			if (ctx == null) {
				ctx = new OperationContext();
			}
			ctx.setChain(scope.getChain());
			ctx.setOperation(scope.getOperationInfo());
			ctx.setExtraObjects(boe.getExtraObjects());
			
			Object[] arguments = parseRequestParameters(ctx, args, el);
			
			Object provider = factory.newInstance(scope.getOperationInfo().getCls());
			
			try {
				
				Object opResult = scope.getOperationInfo().getMethod().invoke(provider, arguments);
				
				WebServiceResult result = factory.newInstance(WebServiceResult.class);
				result.setOperationInfo(ctx.getOperation());
				result.setScope(scope);
				result.setScopeChain(ctx.getChain());
				result.setResult(opResult);
				
				return result;
				
			} catch (Throwable t) {
				LoggerUtils.getCoreLogger().log(Level.SEVERE, null, t);
				error = t;
			}
			
		} catch (JAXBException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
			error = e;
		} catch (IOException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
			error = e;
		} catch (IllegalArgumentException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
			error = e;
		}
		
		// Error
		WebServiceResult result = factory.newInstance(WebServiceResult.class);
		result.setOperationInfo(scope.getOperationInfo());
		result.setScope(scope);
		result.setScopeChain(scope.getChain());
		result.setResult(error);
		
		return result;
		
	}
	
	private Object[] parseRequestParameters(OperationContext ctx, Map<String, String[]> args, Element elt) throws JAXBException {
		
		Marshaller marshaller = xmlUtils.getContext().createMarshaller();
		
		Map<String, List<Object>> attributes = new HashMap<>();
		NamedNodeMap attrMap = elt.getAttributes();
		for (int i = 0; i < attrMap.getLength(); i++) {
			Node attr = attrMap.item(i);
			List<Object> list = attributes.get(attr.getNodeName());
			if (list == null) {
				list = new ArrayList<>(1);
				attributes.put(attr.getNodeName(), list);
			}
			list.add(attr.getNodeValue());
		}
		NodeList params = elt.getChildNodes();
		for (int i = 0; i < params.getLength(); i++) {
			Node node = params.item(i);
			if (node instanceof Element) {
				Element paramEl = (Element) node;
				List<Object> list = attributes.get(paramEl.getNodeName());
				if (list == null) {
					list = new ArrayList<>(1);
					attributes.put(paramEl.getNodeName(), list);
				}
				list.add(paramEl);
			}
		}
		
		Object[] extraObjects = ctx.getExtraObjects().toArray(new Object[ctx.getExtraObjects().size()]);
		
		Object[] arguments = new Object[ctx.getOperation().getParameters().size()];
		for (int i = 0; i < arguments.length; i++) {
			ParameterInfo pInfo = ctx.getOperation().getParameters().get(i);
			if (StringUtils.isNotBlank(pInfo.getName())) {
				Object value = attributes.get(pInfo.getName());
				if (value == null) {
					if (pInfo.getDefaultValue() != null && pInfo.getDefaultValue().length > 0) {
						args.put(pInfo.getName(), pInfo.getDefaultValue());
						if (pInfo.getConverter() != null) {
							ITypeConverter converter = factory.newInstance(pInfo.getConverter());
							arguments[i] = converter.fromParametersMap(pInfo.getName(), pInfo.getParamClass(), pInfo.getGenericType(), args, extraObjects);
						}
						else if (pInfo.getParamClass() == String.class && pInfo.getDefaultValue().length == 1) {
							arguments[i] = pInfo.getDefaultValue()[0];
						}
						else {
							arguments[i] = null;
						}
					}
					else {
						arguments[i] = null;
					}
				}
				else {
					if (((List) value).isEmpty()) {
						if (pInfo.getDefaultValue() != null && pInfo.getDefaultValue().length > 0) {
							args.put(pInfo.getName(), pInfo.getDefaultValue());
							if (pInfo.getConverter() != null) {
								ITypeConverter converter = factory.newInstance(pInfo.getConverter());
								arguments[i] = converter.fromParametersMap(pInfo.getName(), pInfo.getParamClass(), pInfo.getGenericType(), args, extraObjects);
							}
							else if (pInfo.getParamClass() == String.class && pInfo.getDefaultValue().length == 1) {
								arguments[i] = pInfo.getDefaultValue()[0];
							}
							else {
								arguments[i] = null;
							}
						}
						else {
							arguments[i] = null;
						}
					}
					else {
						Object obj = ((List) value).get(0);
						if (obj == null) {
							arguments[i] = null;
						}
						else if (obj instanceof Element) {
							arguments[i] = marshal((List) value, pInfo.getGenericType(), pInfo, ctx);
						}
						else {
							if (pInfo.getConverter() != null) {
								args.put(pInfo.getName(), ((List<String>) value).toArray(new String[((List) value).size()]));
								ITypeConverter converter = factory.newInstance(pInfo.getConverter());
								arguments[i] = converter.fromParametersMap(pInfo.getName(), pInfo.getParamClass(), pInfo.getGenericType(), args, extraObjects);
							}
							else if (pInfo.getParamClass() == String.class && pInfo.getDefaultValue().length == 1) {
								arguments[i] = pInfo.getDefaultValue()[0];
							}
							else {
								arguments[i] = null;
							}
						}
					}
				}
			}
			else {
				boolean found = false;
				if (pInfo.getParamClass().isAssignableFrom(OperationContext.class)) {
					arguments[i] = ctx;
					found = true;
				}
				else {
					for (Object eo : ctx.getExtraObjects()) {
						if (eo != null && pInfo.getParamClass().isAssignableFrom(eo.getClass())) {
							arguments[i] = eo;
							found = true;
							break;
						}
					}
				}
				if (!found &&
						// Exclude HttpServletRequest, HttpServletResponse, etc.
						!pInfo.getParamClass().getPackage().getName().equals(HttpServletRequest.class.getPackage().getName())) {
					Object eo = factory.getBean(ctx.getOperation().getMethod(), pInfo.getPosition());
					arguments[i] = eo;
					found = true;
				}
				if (!found) {
					arguments[i] = null;
				}
			}
		}
		
		return arguments;
	}
	
	private Object marshal(List nodes, Type type, ParameterInfo pInfo, OperationContext ctx) {
		
		List<String> args = new ArrayList<>();
		for (Element el : (List<Element>) nodes) {
			args.add(el.getChildNodes().item(0).getNodeValue());
		}
		
		Map<String, String[]> argsMap = new HashMap<>();
		
		if (pInfo.getConverter() != null) {
			argsMap.put(pInfo.getName(), args.toArray(new String[args.size()]));
			ITypeConverter converter = factory.newInstance(pInfo.getConverter());
			return converter.fromParametersMap(pInfo.getName(), pInfo.getParamClass(), pInfo.getGenericType(), argsMap, ctx.getExtraObjects().toArray(new Object[ctx.getExtraObjects().size()]));
		}
		else if (type == String.class && args.size() == 1) {
			return args.get(0);
		}
		
		return null;
		
	}
	
}
