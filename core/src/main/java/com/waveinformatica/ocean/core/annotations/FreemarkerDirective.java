/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.annotations;

import com.waveinformatica.ocean.core.templates.TemplatesManager;

import freemarker.template.TemplateDirectiveModel;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is intended to mark java implementations of freemarker custom
 * macros.
 * 
 * @see <a href="http://freemarker.incubator.apache.org/docs/pgui_datamodel_directive.html">Freemarker documentation</a>
 * @author Ivano
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD})
@OceanComponent(requiresBean = TemplatesManager.class, mustExtend = TemplateDirectiveModel.class)
public @interface FreemarkerDirective {
	String value();
}
