package com.waveinformatica.ocean.core.mime;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.MultipartDataSource;
import javax.mail.internet.ContentType;
import javax.mail.internet.ParameterList;
import javax.mail.internet.ParseException;

/**
 * This class is useful to use MimeMultipart class to build multipart messages for any use
 * 
 * @author ivano
 *
 */
public class OceanMultipartDataSource implements MultipartDataSource {

	private final static char[] MULTIPART_CHARS =
	        "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	             .toCharArray();
	
	private ContentType contentType = new ContentType("multipart", "mixed", new ParameterList());
	private final List<BodyPart> bodyParts = new ArrayList<>();
	
	public OceanMultipartDataSource() {
		contentType.setParameter("boundary", generateBoundary());
	}
	
	public ContentType getContentTypeObject() {
		return contentType;
	}
	
	public void setContentType(String contentType) throws ParseException {
		this.contentType = new ContentType(contentType);
	}
	
	public void setContentTypeParameter(String name, String value) {
		contentType.setParameter(name, value);
	}
	
	public void setSubType(String subType) {
		contentType.setSubType(subType);
	}
	
	public void addBodyPart(BodyPart part) {
		bodyParts.add(part);
	}
	
	@Override
	public String getContentType() {
		return contentType.toString();
	}

	@Override
	public InputStream getInputStream() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OutputStream getOutputStream() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getCount() {
		return bodyParts.size();
	}

	@Override
	public BodyPart getBodyPart(int index) throws MessagingException {
		return bodyParts.get(index);
	}
	
	public String generateBoundary() {
        StringBuilder buffer = new StringBuilder();
        Random rand = new Random();
        int count = rand.nextInt(11) + 30; // a random size from 30 to 40
        for (int i = 0; i < count; i++) {
        	buffer.append(MULTIPART_CHARS[rand.nextInt(MULTIPART_CHARS.length)]);
        }
        return buffer.toString();
   }

}
