/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.html;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
public class CSSContext {
	
	private String media;
	private Measure width;
	private Measure height;
	private Integer color;
	private Integer monochrome;
	private Measure deviceWidth;
	private Measure deviceHeight;
	private Measure resolution;
	private String orientation;
	private String updateFrequency;
	
	public boolean ruleApplies(CSSRule rule) {
		if (rule.getMediaQuery() == null || rule.getMediaQuery().isEmpty()) {
			return true;
		}
		outerCycle:
		for (CSSCondition mq : rule.getMediaQuery()) {
			if (StringUtils.isNotBlank(mq.getMedia()) && !mq.getMedia().equalsIgnoreCase("all") && !mq.getMedia().equalsIgnoreCase(media)) {
				continue;
			}
			for (CSSProperty p : mq.getProperties()) {
				if (p.getName().equalsIgnoreCase("aspect-ratio")) {
					if (width == null || height == null || height.getValue().equals(0.0)) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Double ctxRatio = width.div(height);
					Measure reqRatio = new Measure(p.getValues().get(0));
					if (reqRatio.getUnit() == null) {
						if (!ctxRatio.equals(reqRatio.getValue())) {
							continue outerCycle;
						}
					}
					else {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("color")) {
					
				}
				else if (p.getName().equalsIgnoreCase("color-index")) {
					
				}
				else if (p.getName().equalsIgnoreCase("device-aspect-ratio")) {
					if (deviceWidth == null || deviceHeight == null || deviceHeight.getValue().equals(0.0)) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Double ctxRatio = deviceWidth.div(deviceHeight);
					Measure reqRatio = new Measure(p.getValues().get(0));
					if (reqRatio.getUnit() == null) {
						if (!ctxRatio.equals(reqRatio.getValue())) {
							continue outerCycle;
						}
					}
					else {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("device-height")) {
					if (deviceHeight == null) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Measure reqHeight = new Measure(p.getValues().get(0));
					if (reqHeight.compareTo(deviceHeight) != 0) {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("device-width")) {
					if (deviceWidth == null) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Measure reqWidth = new Measure(p.getValues().get(0));
					if (reqWidth.compareTo(deviceWidth) != 0) {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("grid")) {
					
				}
				else if (p.getName().equalsIgnoreCase("height")) {
					if (height == null) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Measure reqHeight = new Measure(p.getValues().get(0));
					if (reqHeight.compareTo(height) != 0) {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("max-aspect-ratio")) {
					if (width == null || height == null || height.getValue().equals(0.0)) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Double ctxRatio = width.div(height);
					Measure reqRatio = new Measure(p.getValues().get(0));
					if (reqRatio.getUnit() == null) {
						if (ctxRatio > reqRatio.getValue()) {
							continue outerCycle;
						}
					}
					else {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("max-color")) {
					
				}
				else if (p.getName().equalsIgnoreCase("max-color-index")) {
					
				}
				else if (p.getName().equalsIgnoreCase("max-device-aspect-ratio")) {
					if (deviceWidth == null || deviceHeight == null || deviceHeight.getValue().equals(0.0)) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Double ctxRatio = deviceWidth.div(deviceHeight);
					Measure reqRatio = new Measure(p.getValues().get(0));
					if (reqRatio.getUnit() == null) {
						if (ctxRatio > reqRatio.getValue()) {
							continue outerCycle;
						}
					}
					else {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("max-device-height")) {
					if (deviceHeight == null) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Measure reqHeight = new Measure(p.getValues().get(0));
					if (reqHeight.compareTo(deviceHeight) < 0) {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("max-device-width")) {
					if (deviceWidth == null) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Measure reqWidth = new Measure(p.getValues().get(0));
					if (reqWidth.compareTo(deviceWidth) < 0) {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("max-height")) {
					if (height == null) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Measure reqHeight = new Measure(p.getValues().get(0));
					if (reqHeight.compareTo(height) < 0) {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("max-monochrome")) {
					
				}
				else if (p.getName().equalsIgnoreCase("max-resolution")) {
					if (resolution == null) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Measure reqResolution = new Measure(p.getValues().get(0));
					if (reqResolution.compareTo(resolution) < 0) {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("max-width")) {
					if (width == null) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Measure reqWidth = new Measure(p.getValues().get(0));
					if (reqWidth.compareTo(width) < 0) {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("min-aspect-ratio")) {
					if (width == null || height == null || height.getValue().equals(0.0)) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Double ctxRatio = width.div(height);
					Measure reqRatio = new Measure(p.getValues().get(0));
					if (reqRatio.getUnit() == null) {
						if (ctxRatio < reqRatio.getValue()) {
							continue outerCycle;
						}
					}
					else {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("min-color")) {
					
				}
				else if (p.getName().equalsIgnoreCase("min-color-index")) {
					
				}
				else if (p.getName().equalsIgnoreCase("min-device-aspect-ratio")) {
					if (deviceWidth == null || deviceHeight == null || deviceHeight.getValue().equals(0.0)) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Double ctxRatio = deviceWidth.div(deviceHeight);
					Measure reqRatio = new Measure(p.getValues().get(0));
					if (reqRatio.getUnit() == null) {
						if (ctxRatio < reqRatio.getValue()) {
							continue outerCycle;
						}
					}
					else {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("min-device-width")) {
					if (deviceWidth == null) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Measure reqWidth = new Measure(p.getValues().get(0));
					if (reqWidth.compareTo(deviceWidth) > 0) {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("min-device-height")) {
					if (deviceHeight == null) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Measure reqHeight = new Measure(p.getValues().get(0));
					if (reqHeight.compareTo(deviceHeight) > 0) {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("min-height")) {
					if (height == null) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Measure reqHeight = new Measure(p.getValues().get(0));
					if (reqHeight.compareTo(height) > 0) {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("min-monochrome")) {
					
				}
				else if (p.getName().equalsIgnoreCase("min-resolution")) {
					if (resolution == null) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Measure reqResolution = new Measure(p.getValues().get(0));
					if (reqResolution.compareTo(resolution) > 0) {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("min-width")) {
					if (width == null) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Measure reqWidth = new Measure(p.getValues().get(0));
					if (reqWidth.compareTo(width) > 0) {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("monochrome")) {
					
				}
				else if (p.getName().equalsIgnoreCase("orientation")) {
					if (orientation != null &&
						  (p.getValues() == null || p.getValues().size() != 1 || !orientation.equalsIgnoreCase(p.getValues().get(0)))) {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("overflow-block")) {
					
				}
				else if (p.getName().equalsIgnoreCase("overflow-inline")) {
					
				}
				else if (p.getName().equalsIgnoreCase("resolution")) {
					if (resolution == null) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Measure reqResolution = new Measure(p.getValues().get(0));
					if (reqResolution.compareTo(resolution) != 0) {
						continue outerCycle;
					}
				}
				else if (p.getName().equalsIgnoreCase("scan")) {
					
				}
				else if (p.getName().equalsIgnoreCase("update-frequency")) {
					
				}
				else if (p.getName().equalsIgnoreCase("width")) {
					if (width == null) {
						continue;
					}
					else if (p.getValues() == null || p.getValues().size() != 1) {
						continue outerCycle;
					}
					Measure reqWidth = new Measure(p.getValues().get(0));
					if (reqWidth.compareTo(width) != 0) {
						continue outerCycle;
					}
				}
			}
			return true;
		}
		return false;
	}

	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	public Measure getWidth() {
		return width;
	}

	public void setWidth(Measure width) {
		this.width = width;
	}

	public Measure getHeight() {
		return height;
	}

	public void setHeight(Measure height) {
		this.height = height;
	}

	public Integer getColor() {
		return color;
	}

	public void setColor(Integer color) {
		this.color = color;
	}

	public Integer getMonochrome() {
		return monochrome;
	}

	public void setMonochrome(Integer monochrome) {
		this.monochrome = monochrome;
	}

	public Measure getDeviceWidth() {
		return deviceWidth;
	}

	public void setDeviceWidth(Measure deviceWidth) {
		this.deviceWidth = deviceWidth;
	}

	public Measure getDeviceHeight() {
		return deviceHeight;
	}

	public void setDeviceHeight(Measure deviceHeight) {
		this.deviceHeight = deviceHeight;
	}

	public Measure getResolution() {
		return resolution;
	}

	public void setResolution(Measure resolution) {
		this.resolution = resolution;
	}

	public String getOrientation() {
		return orientation;
	}

	public void setOrientation(String orientation) {
		this.orientation = orientation;
	}

	public String getUpdateFrequency() {
		return updateFrequency;
	}

	public void setUpdateFrequency(String updateFrequency) {
		this.updateFrequency = updateFrequency;
	}
	
}
