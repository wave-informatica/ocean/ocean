/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.controllers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

import javax.persistence.EntityManager;

import com.waveinformatica.ocean.core.util.LoggerUtils;

/**
 *
 * @author Ivano
 */
public class InstanceListener {
	
	private static final ThreadLocal<InstanceListener> instance = new ThreadLocal<InstanceListener>();
	
	public static InstanceListener get() {
		if (instance.get() == null) {
			instance.set(new InstanceListener());
		}
		return instance.get();
	}
	
	private final List<EntityManager> entityManagers = new LinkedList<EntityManager>();
	private final List<Connection> connections = new LinkedList<Connection>();
	
	public List<EntityManager> getEntityManagers() {
		return entityManagers;
	}
	
	public List<Connection> getConnections() {
		return connections;
	}
	
	public void clear() {
		
		instance.set(null);
		
		for (EntityManager em : entityManagers) {
			try {
				if (em.isOpen()) {
					if (em.getTransaction().isActive()) {
						em.getTransaction().rollback();
					}
					em.close();
				}
			} catch (Throwable e) {
				LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unable to close entity manager", e);
			}
		}
		
		for (Connection conn : connections) {
			try {
				if (!conn.isClosed()) {
					if (conn.getAutoCommit() == false) {
						try {
							conn.rollback();
						} catch (SQLException e) {
							// Nothing to do here
						}
					}
					conn.close();
				}
			} catch (Throwable ex) {
				LoggerUtils.getCoreLogger().log(Level.WARNING, "Error checking connection status or closing open connection", ex);
			}
		}
	}
}
