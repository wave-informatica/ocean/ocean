/*
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers;

import com.waveinformatica.ocean.core.annotations.Service;
import com.waveinformatica.ocean.core.controllers.dto.ServiceStatus;

/**
 * <p>This interface presents a Service. A Service is a component which can be started
 * and stopped and can be used to control some aspects of the application or to controls
 * threads.</p>
 * 
 * <p>Service classes must be annotated with {@link Service} too.
 * 
 * @author Ivano
 * @see Service
 */
public interface IService {
    
    /**
     * Starts the service
     * 
     * @return <code>true</code> on success
     */
    public boolean start();
    
    /**
     * Stops the service
     * 
     * @return <code>true</code> on success
     */
    public boolean stop();
    
    /**
     * Returns info about the state of the service
     * 
     * @return info about the state of the service
     */
    public ServiceStatus getServiceStatus();
    
}
