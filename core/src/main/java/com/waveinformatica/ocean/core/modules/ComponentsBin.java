/*
* Copyright 2016, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.modules;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.annotations.OceanComponent;
import com.waveinformatica.ocean.core.annotations.Preferred;
import com.waveinformatica.ocean.core.annotations.Service;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;

/**
 * <p>This class provides a easy way to manage pluggable components by implementing what is needed to catch
 * modules loading and unloading.
 *
 * <p>To use this class, you only need to to the following:
 * <pre><strong>@Inject @Preferred</strong>
 * ... List of @OceanComponent annotated annotations about components you want to be managed by this instance ...
 * private <strong>ComponentsBin</strong> bin;</pre>
 *
 * <p>Optionally, you can make the owning class implement one or both the interfaces
 * {@link ComponentLoaded} or {@link ComponentUnloaded} and add the following code:
 * <pre> @PostConstruct
 * public void init() {
 *   bin.register(this);
 * }</pre>
 * to receive notifications about loaded or unloaded components.
 *
 * @author Ivano
 */
public class ComponentsBin {
	
	@Inject
	private ComponentsManager manager;
	
	private Class<?> ownerClass;
	private final List<Class<? extends Annotation>> managedComponents = new ArrayList<Class<? extends Annotation>>();
	private final Set<Object> listenerObjects = new HashSet<>();
	
	/**
	 * Retrieves components classes per annotation
	 *
	 * @param annotation the required component's annotation
	 * @return the set of loaded components
	 */
	public Set<Class<?>> getComponents(Class<? extends Annotation> annotation) {
		
		if (annotation == null) {
			throw new IllegalArgumentException("The \"annotation\" argument MUST NOT be null");
		}
		
		if (annotation.getAnnotation(OceanComponent.class) == null) {
			throw new IllegalArgumentException("The \"annotation\" argument MUST be a @OceanComponent annotated annotation");
		}
		
		return manager.getComponents(annotation);
		
	}
	
	/**
	 * Registers the owning object to receive notifications
	 *
	 * @param t the owning object
	 */
	public void register(Object t) {
		if (!(t instanceof ComponentLoaded || t instanceof ComponentUnloaded)) {
			throw new IllegalArgumentException("The specified owning object MUST implment at least one of the \"" + ComponentLoaded.class.getName() + "\" or \"" + ComponentUnloaded.class.getName() + "\" interfaces");
		}
		listenerObjects.add(t);
		for (Class<? extends Annotation> a : managedComponents) {
			Set<Class<?>> components = manager.getComponents(a);
			if (components != null) {
				loadClasses(a, components);
			}
		}
	}
	
	/**
	 * Unregisters the owning object to stop receiving notifactions
	 * @param t the owning object
	 */
	public void unregister(Object t) {
		if (!(t instanceof ComponentLoaded || t instanceof ComponentUnloaded)) {
			throw new IllegalArgumentException("The specified owning object MUST implment at least one of the \"" + ComponentLoaded.class.getName() + "\" or \"" + ComponentUnloaded.class.getName() + "\" interfaces");
		}
		listenerObjects.remove(t);
	}
	
	protected void init (Class<?> injectionClass, Annotation[] annotations) {
		
		if (!managedComponents.isEmpty()) {
			throw new IllegalStateException("Component already initialized");
		}
		
		if (injectionClass == null) {
			throw new IllegalArgumentException("\"injectionClass\" parameter is null");
		}
		
		if (annotations == null) {
			throw new IllegalArgumentException("\"annotations\" parameter is null");
		}
		
		if (annotations.length == 0) {
			throw new IllegalArgumentException("\"annotations\" parameter is empty");
		}
		
		this.ownerClass = injectionClass;
		
		for (Annotation a : annotations) {
			if (a.annotationType() == Preferred.class) {
				for (Class<? extends Annotation> b : ((Preferred) a).value()) {
					managedComponents.add(b);
				}
			}
			if (a.annotationType().getAnnotation(OceanComponent.class) != null) {
				managedComponents.add(a.annotationType());
			}
		}
		
	}
	
	void loadClasses(Class<? extends Annotation> annotation, Set<Class<?>> annotatedClasses) {
		// Notifying listeners
		for (Object listener : listenerObjects) {
			if (listener instanceof ComponentLoaded) {
				for (Class<?> c : annotatedClasses) {
					((ComponentLoaded) listener).componentLoaded(annotation, c);
				}
			}
		}
	}
	
	void unloadClasses(Class<? extends Annotation> annotation, Set<Class<?>> annotatedClasses) {
		// Notifying listeners
		for (Object listener : listenerObjects) {
			if (listener instanceof ComponentUnloaded) {
				for (Class<?> c : annotatedClasses) {
					((ComponentUnloaded) listener).componentUnloaded(annotation, c);
				}
			}
		}
	}
	
	void destroy() {
		ownerClass = null;
		managedComponents.clear();
	}
	
	/**
	 * This method is intended for analysis only.
	 * 
	 * @return an unmodifiable list of managed components types
	 */
	public List<Class<? extends Annotation>> getManagedComponents() {
		return Collections.unmodifiableList(managedComponents);
	}
	
	/**
	 * This method is intended for analysis only.
	 * 
	 * @return an unmodifiable set of components per annotation
	 */
	public Set<Class<?>> getManagedComponents(String annotation) {
		for (Class<? extends Annotation> mc : managedComponents) {
			if (mc.getName().equals(annotation)) {
				Set<Class<?>> set = manager.getComponents(mc);
				if (set == null) {
					set = new HashSet<>();
				}
				return Collections.unmodifiableSet(set);
			}
		}
		return null;
	}
	
	/**
	 * Gives ability to receive notifications about a loaded component
	 */
	public static interface ComponentLoaded {
		public void componentLoaded(Class<? extends Annotation> annotation, Class<?> componentClass);
	}
	
	/**
	 * Gives ability to receive notifications about a unloaded component
	 */
	public static interface ComponentUnloaded {
		public void componentUnloaded(Class<? extends Annotation> annotation, Class<?> componentClass);
	}
	
	/**
	 * Do not use this class directly. It is only a producer class.
	 */
	@Dependent
	public static class Producer {
		
		@Inject
		private ObjectFactory factory;
		
		@Inject
		private ComponentsManager manager;
		
		@Produces
		@Preferred
		@Dependent
		public ComponentsBin produce(InjectionPoint point) {
			
			ComponentsBin bin = manager.getBinInstances().get(point.getMember().getDeclaringClass());
			
			if (bin != null) {
				return bin;
			}
			
			if (!(point.getMember() instanceof Field)) {
				throw new IllegalStateException("Trying to inject a ComponentsBin into a illegal place: " + point.getMember().toString());
			}
			
			Annotation[] clsAnnotations = point.getMember().getDeclaringClass().getAnnotations();
			boolean found = false;
			for (Annotation a : clsAnnotations) {
				if (ApplicationScoped.class.isAssignableFrom(a.annotationType()) || Service.class.isAssignableFrom(a.annotationType())) {
					found = true;
					break;
				}
			}
			
			if (!found) {
				throw new IllegalStateException("ComponentsBins can only be injected into @ApplicationScoped beans or @Service annotated components");
			}
			
			Set<Annotation> annotations = point.getAnnotated().getAnnotations();
			ComponentsBin binBean = factory.newInstance(ComponentsBin.class);
			binBean.init(point.getMember().getDeclaringClass(), annotations.toArray(new Annotation[annotations.size()]));
			
			manager.registerBin(point.getMember().getDeclaringClass(), binBean);
			
			return binBean;
			
		}
		
	}
	
	ComponentsBin copy() {
		ComponentsBin newCB = Application.getInstance().getObjectFactory().newInstance(ComponentsBin.class);
		newCB.ownerClass = ownerClass;
		newCB.managedComponents.addAll(managedComponents);
		return newCB;
	}
	
}
