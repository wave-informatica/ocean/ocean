/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core;

import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.PersistedProperties;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import javax.enterprise.context.ApplicationScoped;
import javax.servlet.ServletContext;

/**
 *
 * @author Ivano
 */
@ApplicationScoped
public class DefaultConfigurationBean implements Configuration {
	
	protected File configDir;
	protected PersistedProperties persistenceProperties;
	protected PersistedProperties siteProperties;
	protected PersistedProperties smtpProperties;
	protected PersistedProperties loggingProperties;
	protected PersistedProperties cachingProperties;
	protected PersistedProperties networkProperties;
	protected final Map<String, PersistedProperties> customConfigurations = new HashMap<String, PersistedProperties>();
	private static final Properties packageProperties = new Properties();
	
	@Override
	public void initPackageConfiguration(ServletContext context) {
		
		try {
			packageProperties.load(getClass().getResourceAsStream("/version.info"));
		} catch (IOException ex) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, "Error reading version.info file", ex);
		}
		
	}
	
	@Override
	public File getConfigDir() {
		return configDir;
	}
	
	@Override
	public void setConfigDir(File configDir) {
		this.configDir = configDir;
	}
	
	@Override
	public PersistedProperties getPersistenceProperties() {
		if (persistenceProperties == null) {
			File persistenceCfg = new File(configDir, "persistence.properties");
			persistenceProperties = new PersistedProperties(persistenceCfg);
			if (persistenceCfg.exists()) {
				persistenceProperties.load();
			}
		}
		else {
			persistenceProperties.reloadIfModified();
		}
		return persistenceProperties;
	}
	
	@Override
	public PersistedProperties getSiteProperties() {
		if (siteProperties == null) {
			File persistenceCfg = new File(configDir, "site.properties");
			siteProperties = new PersistedProperties(persistenceCfg);
			if (persistenceCfg.exists()) {
				siteProperties.load();
			}
		}
		else {
			siteProperties.reloadIfModified();
		}
		return siteProperties;
	}
	
	@Override
	public String getSiteProperty(String key, String def) {
		Object val = getSiteProperties().get(key);
		if(val==null)
			return def;
		return val.toString();
	}
	
	@Override
	public PersistedProperties getCustomProperties(String name) {
		PersistedProperties props = customConfigurations.get(name);
		if (props == null) {
			File propsFile = new File(configDir, "custom_" + name.replaceAll("[^a-zA-Z0-9]+", " ").trim().replaceAll("\\s+", "-") + ".properties");
			props = new PersistedProperties(propsFile);
			if (propsFile.exists()) {
				props.load();
			}
			customConfigurations.put(name, props);
		}
		else {
			props.reloadIfModified();
		}
		return props;
	}
	
	@Override
	public String getPackageVersion() {
		return packageProperties.getProperty("ocean.version");
	}
	
	@Override
	public String getOrganization() {
		return packageProperties.getProperty("organization.name");
	}
	
	@Override
	public String getOrganizationUrl() {
		return packageProperties.getProperty("organization.url");
	}
	
	@Override
	public PersistedProperties getSmtpProperties() {
		if (smtpProperties == null) {
			File persistenceCfg = new File(configDir, "smtp.properties");
			smtpProperties = new PersistedProperties(persistenceCfg);
			if (persistenceCfg.exists()) {
				smtpProperties.load();
			}
		}
		else {
			smtpProperties.reloadIfModified();
		}
		return smtpProperties;
	}
	
	@Override
	public PersistedProperties getLoggingProperties() {
		if (loggingProperties == null) {
			File persistenceCfg = new File(configDir, "logging.properties");
			loggingProperties = new PersistedProperties(persistenceCfg);
			if (persistenceCfg.exists()) {
				loggingProperties.load();
			}
		}
		else {
			loggingProperties.reloadIfModified();
		}
		return loggingProperties;
	}
	
	@Override
	public PersistedProperties getCachingProperties() {
		if (cachingProperties == null) {
			File persistenceCfg = new File(configDir, "caching.properties");
			cachingProperties = new PersistedProperties(persistenceCfg);
			if (persistenceCfg.exists()) {
				cachingProperties.load();
			}
		}
		else {
			cachingProperties.reloadIfModified();
		}
		return cachingProperties;
	}

	@Override
	public PersistedProperties getNetworkProperties() {
		if (networkProperties == null) {
			File persistenceCfg = new File(configDir, "network.properties");
			networkProperties = new PersistedProperties(persistenceCfg);
			if (persistenceCfg.exists()) {
				networkProperties.load();
			}
		}
		else {
			networkProperties.reloadIfModified();
		}
		return networkProperties;
	}
	
}
