/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.controllers.results.input;

import java.util.ArrayList;
import java.util.List;

import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.controllers.CoreController.ParameterInfo;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.controllers.results.InputResult.FieldHandler;
import com.waveinformatica.ocean.core.controllers.results.InputResult.FormField;

public abstract class NestedFieldHandler implements FieldHandler {
	
	private List<FormField> fields = null;
	
	@Override
	public String getFieldTemplate() {
		return "/templates/core/components/fieldNested.tpl";
	}

	@Override
	public void init(InputResult result, OperationInfo opInfo, ParameterInfo paramInfo, FormField field) {
		
		fields = new ArrayList<InputResult.FormField>();
		
	}
	
	public void addField(FormField field) {
		fields.add(field);
	}

	public List<FormField> getFields() {
		return fields;
	}
	
	public FormField getField(String name) {
		for (FormField ff : fields) {
			if (ff.getName().equals(name)) {
				return ff;
			}
		}
		return null;
	}

}
