/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.controllers.decorators;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import com.waveinformatica.ocean.core.annotations.ResultDecorator;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.core.util.I18N;
import com.waveinformatica.ocean.core.util.LoggerUtils;

/**
 *
 * @author Ivano
 */
@ResultDecorator(classes = TableResult.class, mimeTypes = {MimeTypes.EXCEL})
public class ExcelTableDecorator implements IDecorator<TableResult> {
	
	@Inject
	private ObjectFactory factory;
	
	@Override
	public void decorate(TableResult tbl, HttpServletRequest request, HttpServletResponse response, MimeTypes type) {
		
		if (tbl.getName() == null) {
			tbl.setName("export");
		}
		response.setContentType(type.getMimeType());
		response.setHeader("Content-disposition", "attachment; filename=\"" + tbl.getName() + ".xlsx\"");
		
		try {
			
			decorate(tbl, response.getOutputStream(), type);
			
		} catch (IOException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
		}
			
	}
	
	@Override
	public void decorate(TableResult tbl, OutputStream out, MimeTypes type) {
		
		if (tbl.getName() == null) {
			tbl.setName("export");
		}
		
		tbl.setRowHandler(factory.newInstance(ExcelRowHandler.class));
		
		Workbook wb = createWorkbook();
		
		try {
			
			Sheet sh = createSheet(wb, tbl.getName());
			
			decorate(tbl, sh);
			
			wb.write(out);
			
		} catch (IOException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
		} finally {
			try {
				wb.close();
			} catch (IOException e) {
				LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
			}
		}
	}
	
	protected Workbook createWorkbook() {
		SXSSFWorkbook wb = new SXSSFWorkbook();
		wb.setCompressTempFiles(true);
		return wb;
	}
	
	protected Sheet createSheet(Workbook wb, String name) {
		Sheet sheet = wb.createSheet(name);
		if (sheet instanceof SXSSFSheet) {
			((SXSSFSheet) sheet).setRandomAccessWindowSize(30);
		}
		return sheet;
	}
	
	protected void decorate(TableResult tbl, Sheet sh) {
		
		Workbook wb = sh.getWorkbook();
		
		Font headerFont = wb.createFont();
		headerFont.setFontName("Arial");
		headerFont.setBold(true);
		
		Font normalFont = wb.createFont();
		normalFont.setFontName("Arial");
		
		CellStyle headerStyle = wb.createCellStyle();
		headerStyle.setFont(headerFont);
		headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		headerStyle.setAlignment(HorizontalAlignment.CENTER);
		
		CellStyle normalStyle = wb.createCellStyle();
		normalStyle.setFont(normalFont);
		CellStyle[] colsStyle = new CellStyle[tbl.getHeader().getColumns().size()];
		
		Row header = sh.createRow(0);
		int i = 0;
		for (TableResult.TableColumn s : tbl.getHeader().getColumns()) {
			Cell cell = header.createCell(i++);
			cell.setCellStyle(headerStyle);
			cell.setCellValue(s.getLabel());
		}
		
		Drawing drawing = sh.createDrawingPatriarch();
		CreationHelper creationHelper = wb.getCreationHelper();
		
		if (tbl.getRowsBufferSize() != null) {
			tbl.setRowsPerPage(tbl.getRowsBufferSize());
		}
		
		// Ensure re-initialization
		tbl.setPage(0);
		tbl.nextPage();
		
		int j = 1;
		do {
			for (TableResult.TableRow resRow : tbl.getAllRows()) {
				Row contentRow = sh.createRow(j++);
				for (i = 0; i < resRow.getCells().size(); i++) {
					TableResult.TableCell tblCell = resRow.getCells().get(i);
					TableResult.TableColumn column = (TableResult.TableColumn) tbl.getHeader().getColumns().get(i);
					Cell cell = contentRow.createCell(i);
					cell.setCellStyle(normalStyle);
					if (tblCell.getOriginalValue() != null) {
						Object v = tblCell.getOriginalValue();
						if (v instanceof Date) {
							cell.setCellValue((Date) v);
							if (colsStyle[i] == null) {
								colsStyle[i] = this.getDateCellStyle((Workbook) wb, column.getFormat());
							}
							cell.setCellStyle(colsStyle[i]);
						} else if (v instanceof Calendar) {
							cell.setCellValue((Calendar) v);
							if (colsStyle[i] == null) {
								colsStyle[i] = this.getDateCellStyle((Workbook) wb, column.getFormat());
							}
							cell.setCellStyle(colsStyle[i]);
						} else if (v instanceof Number) {
							cell.setCellValue(((Number) v).doubleValue());
						} else {
							cell.setCellValue(tblCell.getValue());
						}
					} else {
						cell.setCellValue(tblCell.getValue());
					}
					if (!tblCell.getComments().isEmpty()) {
						StringBuilder builder = new StringBuilder();
						for (String c : tblCell.getComments()) {
							if (c.trim().length() == 0) {
								continue;
							}
							if (builder.length() > 0) {
								builder.append("\r\n");
							}
							builder.append(c);
						}
						if (builder.length() > 0) {
							ClientAnchor anchor = creationHelper.createClientAnchor();
							anchor.setCol1(cell.getColumnIndex());
							anchor.setCol2(cell.getColumnIndex() + 1);
							anchor.setRow1(cell.getRowIndex());
							anchor.setRow2(cell.getRowIndex() + 3);
							Comment comment = drawing.createCellComment(anchor);
							RichTextString str = creationHelper.createRichTextString(builder.toString());
							comment.setVisible(Boolean.FALSE);
							comment.setString(str);
							cell.setCellComment(comment);
						}
					}
				}
			}
		} while (tbl.nextPage());
		
	}
	
	protected CellStyle getDateCellStyle(Workbook wb, String format) {
		CellStyle dateCellStyle = wb.createCellStyle();
		CreationHelper createHelper = wb.getCreationHelper();
		if (format == null) {
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy hh:mm"));
		} else if ("%1$td/%1$tm/%1$ty".equals(format)) {
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy"));
		} else if ("%1%tH:%1$tM:%1$tS".equals(format)) {
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("hh:mm"));
		} else if ("%1$td/%1$tm/%1$ty %1$tH:%1$tM:%1$tS".equals(format)) {
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy hh:mm"));
		} else {
			StringBuilder builder = new StringBuilder();
			Pattern p = Pattern.compile("((%([0-9]+\\$)?t([HIklMSLNpzZsQBbhAaCYyjmdeRTrDFc]))|(.+?))");
			Matcher m = p.matcher(format);
			while (m.find()) {
				if (m.group(4) != null && m.group(4).length() > 0) {
					switch (m.group(4).charAt(0)) {
						case 'Y':
							builder.append("yyyy");
							break;
						case 'm':
							builder.append("MM");
							break;
						case 'd':
							builder.append("dd");
							break;
						case 'H':
							builder.append("hh");
							break;
						case 'M':
							builder.append("mm");
							break;
						case 'S':
							builder.append("ss");
							break;
						case 'b':
						case 'h':
							builder.append("MMM");
							break;
						case 'B':
							builder.append("MMMM");
							break;
						case 'A':
							builder.append("dddd");
							break;
						case 'a':
							builder.append("ddd");
							break;
						case 'e':
							builder.append("d");
							break;
					}
				}
				else if (m.group(5) != null && m.group(5).length() > 0) {
					builder.append(m.group(5));
				}
				
			}
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat(builder.toString()));
		}
		return dateCellStyle;
	}
	
	public static class ExcelRowHandler implements TableResult.RowHandler {
		
		private final String resultClassName = ExcelTableDecorator.class.getName() + ".";
		
		@Inject
		private I18N i18n;
		
		@Override
		public void handleRow(TableResult.TableHeader header, TableResult.TableRow row) {
			for (TableResult.TableCell tc : row.getCells()) {
				if (tc.getFieldType() == null) {
					continue;
				}
				if (StringUtils.isNotBlank(tc.getValue()) && Boolean.class.isAssignableFrom(tc.getFieldType())
					  || (tc.getFieldType().isPrimitive() && tc.getFieldType() == Boolean.TYPE)) {
					try {
						boolean val = Boolean.parseBoolean(tc.getValue());
						tc.setValue(i18n.translate(resultClassName + val));
					} catch (Exception e) {
					}
				}
			}
		}
		
		@Override
		public List<TableResult.TableOperation> handleRowActions(List<TableResult.TableOperation> operations, TableResult.TableRow rowSource) {
			return operations;
		}
		
	}
	
}
