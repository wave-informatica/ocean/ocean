/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.tasks;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.Configuration;

/**
 * This class wraps an configurable ExecutorService and allows to use both a default generated cached thread pool
 * or a custom executor provided by a JNDI lookup.
 * 
 * @author ivano
 *
 */
@ApplicationScoped
public class OceanExecutorService implements ExecutorService {
	
	@Inject
	private Configuration config;
	
	// Configuration
	private String jndi;
	
	// Executor
	private ExecutorService executor;
	
	@PostConstruct
	public void updateConfig() throws NamingException {
		
		String jndi = config.getSiteProperty("concurrency.executor.jndi", "");
		
		if (executor != null && StringUtils.isBlank(jndi) && StringUtils.isBlank(this.jndi)) {
			return;
		}
		else if (executor != null && StringUtils.isNotBlank(jndi) && jndi.equals(this.jndi)) {
			return;
		}
		
		ExecutorService old = executor;
		String oldJndi = this.jndi;
		
		if (StringUtils.isNotBlank(jndi)) {
			
			InitialContext ctx = new InitialContext();
			ExecutorService newExecutor = (ExecutorService) ctx.lookup(jndi);
			
			if (newExecutor != null) {
				executor = newExecutor;
				this.jndi = jndi;
				
				if (old != null && StringUtils.isBlank(oldJndi)) {
					old.shutdown();
				}
			}
			else {
				throw new NamingException("Unable to find executor with JNDI name \"" + jndi + "\"");
			}
			
		}
		else {
			
			executor = Executors.newCachedThreadPool();
			this.jndi = null;
			
			if (old != null && StringUtils.isBlank(oldJndi)) {
				old.shutdown();
			}
			
		}
		
	}
	
	/**
	 * This method must only be invoked at application shutdown.
	 * 
	 * @throws InterruptedException
	 */
	public void stop() throws InterruptedException {
		
		if (StringUtils.isBlank(jndi)) {
			
			executor.shutdown();
			
			executor.awaitTermination(2, TimeUnit.MINUTES);
			
		}
		
	}

	@Override
	public void execute(Runnable command) {
		if (!(command instanceof OceanRunnable)) {
			command = OceanRunnable.create(command);
		}
		executor.execute(command);
	}

	@Override
	public void shutdown() {
		
	}

	@Override
	public List<Runnable> shutdownNow() {
		return new ArrayList<>(0);
	}

	@Override
	public boolean isShutdown() {
		return executor.isShutdown();
	}

	@Override
	public boolean isTerminated() {
		return executor.isTerminated();
	}

	@Override
	public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
		return false;
	}

	@Override
	public <T> Future<T> submit(Callable<T> task) {
		if (!(task instanceof OceanCallable)) {
			task = OceanCallable.create(task);
		}
		return executor.submit(task);
	}

	@Override
	public <T> Future<T> submit(Runnable task, T result) {
		if (!(task instanceof OceanRunnable)) {
			task = OceanRunnable.create(task);
		}
		return executor.submit(task, result);
	}

	@Override
	public Future<?> submit(Runnable task) {
		if (!(task instanceof OceanRunnable)) {
			task = OceanRunnable.create(task);
		}
		return executor.submit(task);
	}

	@Override
	public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
		List<Callable<T>> callables = new ArrayList<>(tasks);
		for (int i = 0; i < callables.size(); i++) {
			if (!(callables.get(i) instanceof OceanCallable)) {
				callables.set(i, OceanCallable.<T>create(callables.get(i)));
			}
		}
		return executor.invokeAll(callables);
	}

	@Override
	public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit)
			throws InterruptedException {
		List<Callable<T>> callables = new ArrayList<>(tasks);
		for (int i = 0; i < callables.size(); i++) {
			if (!(callables.get(i) instanceof OceanCallable)) {
				callables.set(i, OceanCallable.<T>create(callables.get(i)));
			}
		}
		return executor.invokeAll(callables, timeout, unit);
	}

	@Override
	public <T> T invokeAny(Collection<? extends Callable<T>> tasks) throws InterruptedException, ExecutionException {
		List<Callable<T>> callables = new ArrayList<>(tasks);
		for (int i = 0; i < callables.size(); i++) {
			if (!(callables.get(i) instanceof OceanCallable)) {
				callables.set(i, OceanCallable.<T>create(callables.get(i)));
			}
		}
		return executor.invokeAny(callables);
	}

	@Override
	public <T> T invokeAny(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit)
			throws InterruptedException, ExecutionException, TimeoutException {
		List<Callable<T>> callables = new ArrayList<>(tasks);
		for (int i = 0; i < callables.size(); i++) {
			if (!(callables.get(i) instanceof OceanCallable)) {
				callables.set(i, OceanCallable.<T>create(callables.get(i)));
			}
		}
		return executor.invokeAny(callables, timeout, unit);
	}
	
}
