/*
* Copyright 2017, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.controllers.scopes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.ContextNotActiveException;
import javax.inject.Inject;

import com.waveinformatica.ocean.core.annotations.Preferred;
import com.waveinformatica.ocean.core.annotations.ScopeProvider;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.modules.ComponentsBin;
import com.waveinformatica.ocean.core.util.RequestCache;

/**
 * This class is the main scopes controller component. It allows to get the {@link ScopeChain}
 * given a path.
 * 
 * @author ivano
 * @since 2.2.0
 */
@ApplicationScoped
public class ScopesController {
	
	@Inject
	private ObjectFactory factory;
	
	@Inject @Preferred
	@ScopeProvider
	private ComponentsBin components;
	
	/**
	 * This method is a shortcut for {@link ScopesController#_build(String, AbstractScope)}
	 * with a {@link RootScope} empty instance as root scope.
	 * 
	 * @param _path the operation's path
	 * @return the built {@link ScopeChain}
	 */
	public ScopeChain build(final String _path) {
		
		RootScope root = factory.newInstance(RootScope.class);
		
		return build(_path, root);
		
	}
	
	/**
	 * This method is the main method to build the {@link ScopeChain} object given
	 * its path and a root scope.
	 * 
	 * @param _path the operation's path
	 * @return the built {@link ScopeChain}
	 */
	public ScopeChain build(final String _path, final AbstractScope rootScope) {
		
		try {
			
			RequestCache cache = factory.getBean(RequestCache.class);

			return (ScopeChain) cache.get(ScopesController.class.getName() + ":" + _path, new RequestCache.Factory() {
				@Override
				public Object create() {
					return _build(_path, rootScope);
				}
			});

		} catch (ContextNotActiveException e) {
			return _build(_path, rootScope);
		}
		
	}
	
	private ScopeChain _build(String path, AbstractScope root) {
		
		if (root == null) {
			throw new NullPointerException("The root scope must be provided");
		}
		
		ScopeChain chain = new ScopeChain();
		
		chain.add(root);
		chain.commit();
		
		do {
			String tmpPath = null;
			
			List<IScopeProvider> providers = new ArrayList<IScopeProvider>();
			for (Class<?> cls : components.getComponents(ScopeProvider.class)) {
				providers.add((IScopeProvider) factory.newInstance(cls));
			}
			Collections.sort(providers, new Comparator<IScopeProvider>() {
				@Override
				public int compare(IScopeProvider o1, IScopeProvider o2) {
					// TODO Auto-generated method stub
					return o2.getClass().getAnnotation(ScopeProvider.class).order() - o1.getClass().getAnnotation(ScopeProvider.class).order();
				}
			});
			
			componentsLoop:
			for (IScopeProvider provider : providers) {
				ScopeProvider ann = provider.getClass().getAnnotation(ScopeProvider.class);
				for (Class<?> annCls : ann.inScope()) {
					if (annCls.isAssignableFrom(chain.getCurrent().getClass()) && path.startsWith(ann.startsWith())) {
						tmpPath = provider.produce(chain, path);
						if (tmpPath != null) {
							if (tmpPath.equals("$")) {
								tmpPath = null;
							}
							chain.commit();
							break componentsLoop;
						}
						else {
							chain.rollback();
						}
					}
				}
			}
			
			path = tmpPath;
			
		} while (path != null);
		
		chain.completeChain();
		
		return chain;
		
	}
	
}
