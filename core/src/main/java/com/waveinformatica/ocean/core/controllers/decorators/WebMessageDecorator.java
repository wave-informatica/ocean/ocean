/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.decorators;

import java.io.OutputStream;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.annotations.ResultDecorator;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.MessageResult;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.results.WebPageResult;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.Results;

/**
 *
 * @author Ivano
 */
@ResultDecorator(classes = MessageResult.class, mimeTypes = { MimeTypes.HTML, MimeTypes.PDF })
public class WebMessageDecorator extends AbstractTemplateDecorator<MessageResult> {

	@Inject
	private ObjectFactory factory;

	@Override
	public void decorate(MessageResult msgRes, HttpServletRequest request, HttpServletResponse response,
			MimeTypes type) {

		WebPageDecorator wpd = factory.newInstance(WebPageDecorator.class);
		wpd.setExtraObjects(getExtraObjects());

		WebPageResult wrapperResult = factory.newInstance(WebPageResult.class);

		wrapperResult.setData(msgRes);
		wrapperResult.setTemplate("/templates/core/messageResult.tpl");
		wrapperResult.setParentOperation(msgRes.getParentOperation());
		wrapperResult.getExtras().putAll(msgRes.getExtras());
		wrapperResult.setFullResult(msgRes.isFullResult());
		Results.copyHeadResources(msgRes, wrapperResult);

		wpd.decorate(wrapperResult, request, response, type); // To change body of generated methods, choose Tools |
																// Templates.
	}

	@Override
	public void decorate(MessageResult msgRes, OutputStream out, MimeTypes type) {

		WebPageDecorator wpd = factory.newInstance(WebPageDecorator.class);
		wpd.setExtraObjects(getExtraObjects());

		WebPageResult wrapperResult = factory.newInstance(WebPageResult.class);

		wrapperResult.setData(msgRes);
		wrapperResult.setTemplate("/templates/core/messageResult.tpl");
		wrapperResult.setParentOperation(msgRes.getParentOperation());
		wrapperResult.getExtras().putAll(msgRes.getExtras());
		wrapperResult.setContextPath(Application.getInstance().getBaseUrl());
		wrapperResult.setFullResult(msgRes.isFullResult());
		Results.copyHeadResources(msgRes, wrapperResult);

		wpd.decorate(wrapperResult, out, type); // To change body of generated methods, choose Tools | Templates.
	}

}
