/*
 * Copyright 2017, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.templates;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.dto.ResourceInfo;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.ModuleEvent;
import com.waveinformatica.ocean.core.modules.Module;
import com.waveinformatica.ocean.core.modules.ModuleClassLoader;
import com.waveinformatica.ocean.core.modules.ModulesManager;
import com.waveinformatica.ocean.core.util.ResourceScanner;

/**
 *
 * @author ivano
 */
@CoreEventListener(eventNames = {CoreEventName.MODULE_LOADED, CoreEventName.MODULE_UNLOADED})
public class ThemesManager implements IEventListener<ModuleEvent> {

	@Inject
	private ModulesManager manager;

	@Inject
	private TemplatesManager tplManager;

	@Override
	public void performAction(ModuleEvent evt) {

		Module module = manager.getModule(evt.getModuleName());
		if (module != null) {
			ModuleClassLoader mcl = module.getModuleClassLoader();

			if (evt.isLoaded()) {
				Set<String> themes = new TreeSet<String>();
				List<ResourceInfo> resources = ResourceScanner.findResources(mcl, "themes/", false);
				for (ResourceInfo ri : resources) {
					String t = ri.getName().substring("themes/".length());
					String themeName = t.substring(0, t.indexOf('/'));
					themes.add(themeName);
				}
				tplManager.loadThemes(module.getName(), themes);
			} else {
				tplManager.unloadThemes(module.getName());
			}
		}

	}

}
