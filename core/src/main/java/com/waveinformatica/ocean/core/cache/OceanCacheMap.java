/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.cache;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A weak referenced value extension of the {@link ConcurrentHashMap} class.
 * 
 * @author ivano
 *
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class OceanCacheMap<K, V> extends ConcurrentHashMap {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public boolean containsValue(Object value) {
		return super.containsValue(new WeakReference(value));
	}

	@Override
	public V get(Object key) {
		WeakReference<V> ref = (WeakReference<V>) super.get(key);
		if (ref == null || ref.get() == null) {
			super.remove(key);
			return null;
		}
		else {
			return ref.get();
		}
	}

	@Override
	public Object put(Object key, Object value) {
		WeakReference ref = (WeakReference) super.put(key, new WeakReference(value));
		if (ref == null) {
			return null;
		}
		else {
			return ref.get();
		}
	}

	@Override
	public Object remove(Object key) {
		WeakReference ref = (WeakReference) super.remove(key);
		if (ref == null) {
			return null;
		}
		else {
			return ref.get();
		}
	}

	@Override
	public void putAll(Map m) {
		for (Entry e : (Set<Entry>) m.entrySet()) {
			if (e.getValue() != null) {
				super.put(e.getKey(), new WeakReference(e.getValue()));
			}
		}
	}

	@Override
	public KeySetView keySet() {
		return super.keySet();
	}

	@Override
	public Collection values() {
		List l = new ArrayList<>(super.size());
		for (Object o : super.values()) {
			WeakReference wr = (WeakReference) o;
			if (wr != null && wr.get() != null) {
				l.add(wr.get());
			}
		}
		return l;
	}

	@Override
	public Set entrySet() {
		Set<Entry> set = new HashSet<>();
		for (Map.Entry entry : (Set<Map.Entry>) super.entrySet()) {
			if (entry.getValue() != null && ((WeakReference) entry.getValue()).get() != null) {
				set.add(new Entry(entry));
			}
		}
		return set;
	}
	
	public class Entry implements Map.Entry {
		
		private Object key;
		private WeakReference value;
		
		public Entry(Map.Entry source) {
			this.key = source.getKey();
			this.value = (WeakReference) source.getValue();
		}
		
		@Override
		public Object getKey() {
			return key;
		}

		@Override
		public Object getValue() {
			return value != null ? value.get() : null;
		}

		@Override
		public Object setValue(Object value) {
			this.value = new WeakReference(value);
			return put(key, this.value);
		}
		
	}

}