/*
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.results;

import com.waveinformatica.ocean.core.annotations.ExcludeSerialization;
import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>This type represents the base class for all operation result types. All operations
 * should return a <code>void</code> type, a <code>BaseResult</code> type or a sub-class of <code>BaseResult</code>.</p>
 * 
 * <p><strong>Please NOTE:</strong> never instantiate a result using its constructor directly!
 * Use injected ObjectFactory instead.</p>
 * 
 * @author Ivano
 * @see Operation
 * @see ObjectFactory
 */
public class BaseResult {
    
    private String parentOperation;
    
    @ExcludeSerialization
    private boolean fullResult = true;
    
    @ExcludeSerialization
    private final Map<String, Object> extras = new HashMap<String, Object>();
    
    @ExcludeSerialization
    private String name;
    
    @ExcludeSerialization
    private boolean attachment = false;
    
    /**
     * <strong>DEPRECATED; Use scopes instead</strong>
     * Returns the parent operation, used to define the "Back" link
     * 
     * @return the back operation
     */
    @Deprecated
    public String getParentOperation() {
        return parentOperation;
    }

    /**
     * <strong>DEPRECATED; Use scopes instead</strong>
     * Sets the parent operation, used to define the "Back" link
     * 
     * @param parentOperation the back operation
     */
    @Deprecated
    public void setParentOperation(String parentOperation) {
        this.parentOperation = parentOperation;
    }
    
    /**
     * Contains extra data for the result. Useful in BEFORE_RESULT event handlers
     * to add data to the result for all pages.
     * 
     * @return a map of extra data
     */
    public Map<String, Object> getExtras() {
        return extras;
    }
    
    /**
     * Adds extra data to the result's map.
     * 
     * @param name the name
     * @param value the value
     */
    public void putExtra(String name, Object value) {
        extras.put(name, value);
    }
    
    /**
     * Returns extra data from the result's map
     * 
     * @param name the name
     * @return the value or <code>null</code> if not found
     */
    public Object getExtra(String name) {
        return extras.get(name);
    }

    /**
     * A full result usually is used to include header and footer in result output
     * @return <code>true</code> if header and footer should be included in result output
     */
    public boolean isFullResult() {
        return fullResult;
    }

    /**
     * A full result usually is used to include header and footer in result output
     * @param fullResult <code>true</code> if header and footer should be included in result output
     */
    public void setFullResult(boolean fullResult) {
        this.fullResult = fullResult;
    }

    /**
     * The name for the result. If specified, a decorator could use it to name the produced file (via Content-Disposition HTTP header)
     * @return the name for the result
     */
    public String getName() {
	return name;
    }

    /**
     * The name for the result. If specified, a decorator could use it to name the produced file (via Content-Disposition HTTP header)
     * @param name the name for the result
     */
    public void setName(String name) {
	this.name = name;
    }

    /**
     * Says if output for the current result should be downloaded or shown inline in case of a HTTP response
     * @return <code>true</code> if the output should be downloaded, <code>false</code> otherwise
     */
    public boolean isAttachment() {
	return attachment;
    }

    /**
     * Says if output for the current result should be downloaded or shown inline in case of a HTTP response
     * @param attachment <code>true</code> if the output should be downloaded, <code>false</code> otherwise
     */
    public void setAttachment(boolean attachment) {
	this.attachment = attachment;
    }
    
}
