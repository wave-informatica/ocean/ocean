/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.persistence;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManagerFactory;

import com.waveinformatica.ocean.core.Constants;
import com.waveinformatica.ocean.core.exceptions.ModuleException;
import com.waveinformatica.ocean.core.modules.Module;
import com.waveinformatica.ocean.core.modules.ModulesManager;
import com.waveinformatica.ocean.core.util.LoggerUtils;

/**
 *
 * @author Ivano
 */
@Named
@ApplicationScoped
public class PersistenceManager {
	
	private OceanPersistenceProvider provider;
	
	@Inject
	private ModulesManager modulesManager;
	
	@Inject
	private EntitiesStore entitiesStore;
	
	private final Map<String, EntityManagerFactory> coreEntityManagerFactories = new HashMap<String, EntityManagerFactory>();
	
	public boolean isAvailable() {
		return provider != null;
	}
	
	public synchronized void loadModulePersistence(Module module) {

		entitiesStore.loadEntitiesInfo(module.getModuleClassLoader(), module.getModuleClassLoader().getAnnotatedClassesMap());

		for (String n : entitiesStore.getConnectionNames()) {
			Set<Class<?>> classes = entitiesStore.getEntityClasses(module.getName(), n);
			if (!classes.isEmpty()) {

				EntityManagerFactory emFactory = provider.createModuleEntityManagerFactory(n, module, null);
				
				module.setModuleEntityManagerFactory(n, emFactory);
				
			}
		}
		
	}
	
	public synchronized void loadCorePersistence(Map<String, Set<String>> annotations) throws ModuleException {
		
		if (isAvailable()) {
			
			entitiesStore.loadEntitiesInfo(PersistenceManager.class.getClassLoader(), annotations);
			
			for (String n : entitiesStore.getConnectionNames()) {
				Set<Class<?>> classes = entitiesStore.getEntityClasses(Constants.CORE_MODULE_NAME, n);
				if (!classes.isEmpty()) {
					
					EntityManagerFactory emFactory = provider.createCoreEntityManagerFactory(n, null);
					
					coreEntityManagerFactories.put(n, emFactory);
					
				}
			}
			
		}
		else {
			throw new ModuleException("Core persistence not available");
		}
		
	}
	
	public synchronized void reloadPersistence(DataSourceProvider dsProvider) {
		
		// Clearing DataSources cache
		dsProvider.clearDataSources();
		
		// Reloading core persistence...
		Set<String> coreNames = new TreeSet<String>(coreEntityManagerFactories.keySet());
		unloadCore();
		for (String n : coreNames) {
			EntityManagerFactory emFactory = provider.createCoreEntityManagerFactory(n, null);
			coreEntityManagerFactories.put(n, emFactory);
		}
		
		// Reloading modules persistence...
		for (Module m : modulesManager.getModules()) {
			Set<String> connNames = new TreeSet<String>();
			for (String n : entitiesStore.getConnectionNames()) {
				EntityManagerFactory emf = m.getModuleEntityManagerFactory(n);
				if (emf != null) {
					connNames.add(n);
					if (emf.isOpen()) {
						emf.close();
					}
				}
			}
			m.clearEntityManagerFactories();
			for (String n : connNames) {
				EntityManagerFactory emFactory = provider.createModuleEntityManagerFactory(n, m, null);
				m.setModuleEntityManagerFactory(n, emFactory);
			}
		}
		
	}
	
	public synchronized void loadPersistenceProvider(OceanPersistenceProvider provider) {
		
		this.provider = provider;
		
		if (provider != null) {
			
			LoggerUtils.getCoreLogger().log(Level.INFO, "Installed persistence provider " + provider.getClass().getName());
			
			modulesManager.loadModules();
			
		}
		
	}
	
	public OceanPersistenceProvider getProvider() {
		return provider;
	}
	
	public EntityManagerFactory getCoreEntityManagerFactory(String name) {
		return coreEntityManagerFactories.get(name);
	}
	
	public void unloadCore() {
		
		Iterator<Map.Entry<String, EntityManagerFactory>> iter = coreEntityManagerFactories.entrySet().iterator();
		
		while (iter.hasNext()) {
			
			Map.Entry<String, EntityManagerFactory> entry = iter.next();
			
			if (entry.getValue().isOpen()) {
				entry.getValue().close();
			}
			
			iter.remove();
		}
		
	}
	
}
