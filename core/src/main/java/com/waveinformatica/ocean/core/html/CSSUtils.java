/*
* Copyright 2016, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */
package com.waveinformatica.ocean.core.html;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.controllers.dto.ResourceInfo;
import com.waveinformatica.ocean.core.util.FsUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.ResourceRetriever;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import java.util.logging.Level;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import se.fishtank.css.selectors.Selectors;
import se.fishtank.css.selectors.dom.W3CNode;

/**
 * This class provides several useful methods to parse and edit CSS and HTML documents.
 * 
 * @author Ivano
 */
public class CSSUtils {

	/**
	 * Given a HTML or XHTML document, inlines all CSS rules found in
	 * &lt;style&gt; tags as <code>style="..."</code> attributes of matched
	 * elements.
	 *
	 * @param doc the HTML or XHTML document
	 */
	public static void inlineCss(Document doc) {
		inlineCss(doc, null);
	}

	/**
	 * This method is very useful to extract all the CSS rules from a
	 * HTML/XHTML document.
	 *
	 * @param doc the document to extract CSS from
	 * @param context the device context for CSS parsing and rule filtering
	 * @param baseUrl the base url of the document (if no
	 * <code>&lt;base&gt;</code> tag is not present)
	 * @param remove remove all the extracted tags (&lt;style&gt;,
	 * &lt;link&gt;)
	 * @return the parsed stylesheet
	 */
	public static CSSStyleSheet extractCss(Document doc, CSSContext context, String baseUrl, boolean remove) {

		/*if (context == null) {
			context = new CSSContext();
		}*/

		CSSStyleSheet styleSheet = new CSSStyleSheet();

		Selectors selectors = new Selectors(new W3CNode(doc));

		ResourceRetriever retriever = Application.getInstance().getObjectFactory().getBean(ResourceRetriever.class);

		baseUrl = XHTMLUtils.getBase(doc, baseUrl);

		/*if (!baseUrl.endsWith("/")) {
			int pos = baseUrl.lastIndexOf("/");
			baseUrl = baseUrl.substring(0, pos + 1);
		}*/
		// Parsing all CSS rules
		List<Node> nodes = selectors.querySelectorAll("style,link");
		for (Node node : nodes) {
			if (node instanceof Element) {
				Element el = (Element) node;
				if (el.getTagName().equalsIgnoreCase("style")) {
					String styleRules = XHTMLUtils.toText(node).replace("/**/", "").replaceAll("\n", "").trim();
					styleSheet.parse(baseUrl, styleRules);
					if (remove) {
						node.getParentNode().removeChild(node);
					}
				} else if (el.getTagName().equalsIgnoreCase("link")
					  && "stylesheet".equalsIgnoreCase(el.getAttribute("rel"))) {
					String source = el.getAttribute("href");
					if (StringUtils.isNotBlank(source)) {
						ResourceInfo res = retriever.getResource(source);
						if (res != null) {
							try {

								InputStreamReader reader = new InputStreamReader(res.getInputStream(), "UTF-8");
								CSSStyleSheet tmpSS = new CSSStyleSheet();
								tmpSS.parse(source, reader);
								reader.close();
								tmpSS.setPath(source);

								final String finalBaseUrl = source.substring(0, source.lastIndexOf('/') + 1);

								rewriteUrls(tmpSS, new UrlRewriteCallback() {
									@Override
									public String rewrite(String url) {
										try {

											URL pUrl = new URL(url);
											return pUrl.toExternalForm();

										} catch (MalformedURLException e) {

											if (url.startsWith("/")) {
												return FsUtils.normalizePath(url);
											} else {
												return FsUtils.normalizePath(finalBaseUrl + url);
											}

										}
									}
								});

								styleSheet.merge(tmpSS);

							} catch (UnsupportedEncodingException ex) {
								LoggerUtils.getLogger(CSSUtils.class).log(Level.SEVERE, null, ex);
							} catch (IOException ex) {
								LoggerUtils.getLogger(CSSUtils.class).log(Level.SEVERE, null, ex);
							}
						}
					}
					if (remove) {
						node.getParentNode().removeChild(node);
					}
				}
			}
		}

		return styleSheet;

	}

	/**
	 * Given a HTML or XHTML document, inlines all CSS rules found in
	 * &lt;style&gt; tags as <code>style="..."</code> attributes of matched
	 * elements.
	 *
	 * @param doc the HTML or XHTML document
	 * @param cssCtx the {@link CSSContext} describing the output device
	 */
	public static void inlineCss(Document doc, CSSContext cssCtx) {
		
		String idAttribute = "id" + System.currentTimeMillis();
		
		CSSStyleSheet styleSheet = extractCss(doc, cssCtx, "", true);

		Selectors selectors = new Selectors(new W3CNode(doc));

		CSSStyleSheet mediaDependent = new CSSStyleSheet();
		Iterator<CSSRule> rIter = styleSheet.getRules().iterator();
		while (rIter.hasNext()) {
			CSSRule r = rIter.next();
			if ((cssCtx != null && !cssCtx.ruleApplies(r))
				  || (cssCtx == null && r.getMediaQuery() != null && !r.getMediaQuery().isEmpty())) {
				mediaDependent.addRule(r);
				rIter.remove();
			}
		}
		if (!mediaDependent.getRules().isEmpty()) {
			Element newStyleTag = doc.createElement("style");
			newStyleTag.appendChild(doc.createTextNode(mediaDependent.toString()));
			Node body = (Node) selectors.querySelector("body");
			body.insertBefore(newStyleTag, body.getFirstChild());
		}
		
		int serverId = 0;
		
		// Inlining CSS
		Map<String, List<CSSWeightedRule>> elementsMap = new HashMap<String, List<CSSWeightedRule>>();
		List<CSSWeightedRule> wRules = styleSheet.getExplodedRules(cssCtx);
		for (CSSWeightedRule rule : wRules) {
			if (rule.getSelector().trim().startsWith("@")) {
				continue;
			}
			List<Node> nodes = selectors.querySelectorAll(rule.getSelector());
			for (Node n : nodes) {
				if (n.getNodeType() == Node.ELEMENT_NODE) {
					Element el = (Element) n;
					String elId = el.getAttribute(idAttribute);
					if (StringUtils.isBlank(elId)) {
						elId = String.valueOf(serverId++);
						el.setAttribute(idAttribute, elId);
					}
					List<CSSWeightedRule> wr = elementsMap.get(elId);
					if (wr == null) {
						wr = new ArrayList<CSSWeightedRule>();
						elementsMap.put(elId, wr);
					}
					if (StringUtils.isNotBlank(el.getAttribute("style"))) {
						CSSRule r = new CSSRule(new ArrayList<String>(0), el.getAttribute("style"));
						for (CSSProperty p : r.getProperties()) {
							CSSWeightedRule csswr = new CSSWeightedRule(wRules.size(), null, p);
							csswr.setStyleAttribute(true);
							wr.add(csswr);
						}
					}
					el.removeAttribute("style");
					wr.add(rule);
				}
			}
		}
		List<Node> nodes = selectors.querySelectorAll("[" + idAttribute + "]");
		for (Node n : nodes) {
			if (n.getNodeType() == Node.ELEMENT_NODE) {
				Element el = (Element) n;
				Map<String, String> properties = new TreeMap<String, String>();
				List<CSSWeightedRule> rules = elementsMap.get(el.getAttribute(idAttribute));
				Collections.sort(rules);
				for (CSSWeightedRule r : rules) {
					properties.put(r.getName(), r.getValue());
				}
				StringBuilder builder = new StringBuilder();
				for (String k : properties.keySet()) {
					builder.append(';');
					builder.append(k);
					builder.append(':');
					builder.append(properties.get(k));
				}
				el.setAttribute("style", builder.substring(1));
				el.removeAttribute(idAttribute);
			}
		}

	}

	public static CSSStyleSheet rescopeSelectors(CSSStyleSheet styleSheet, String prefix) {

		prefix = prefix.trim();

		for (CSSRule rule : styleSheet.getRules()) {
			List<String> selectors = new ArrayList<String>(rule.getSelector());
			rule.getSelector().clear();
			for (String s : selectors) {

				if (s.startsWith("body")) {
					s = s.substring(4);
				}

				rule.getSelector().add((prefix + " " + s.trim()).trim());
			}
		}

		return styleSheet;

	}

	public static CSSStyleSheet rewriteUrls(CSSStyleSheet styleSheet, UrlRewriteCallback callback) {

		for (CSSRule rule : styleSheet.getRules()) {
			for (CSSProperty prop : rule.getProperties()) {

				if (prop instanceof CSSBlockProperty) {

				} else {
					boolean hasUrl = false;
					for (String s : prop.getValues()) {
						if (s.toLowerCase().contains("url")) {
							hasUrl = true;
							break;
						}
					}

					if (hasUrl) {
						List<CSSValue> parsed = prop.getParsedValues();
						List<String> values = new ArrayList<String>(prop.getValues().size());
						for (int i = 0; i < parsed.size(); i++) {
							CSSValue v = parsed.get(i);
							values.add(rewriteUrlValue(v, callback));
						}
						prop.setValues(values);
					}
				}

			}
		}

		return styleSheet;

	}

	private static String rewriteUrlValue(CSSValue value, UrlRewriteCallback callback) {
		if (value instanceof CSSValueUrl) {
			CSSValueUrl urlVal = (CSSValueUrl) value;
			if (urlVal.getValue().startsWith("data:")) {
				return "url('" + urlVal.getValue() + "')";
			} else {
				String newUrl = callback.rewrite(urlVal.getValue());
				return "url('" + newUrl + "')";
			}
		} else if (value instanceof CSSValueComposite) {
			CSSValueComposite vc = (CSSValueComposite) value;
			StringBuilder builder = new StringBuilder();
			for (CSSValue v : vc.getParts()) {
				if (builder.length() > 0) {
					builder.append(" ");
				}
				builder.append(rewriteUrlValue(v, callback));
			}
			return builder.toString();
		} else {
			return value.toString();
		}
	}

	public static CSSStyleSheet buildStyleSheet(String value) {

		CSSStyleSheet styleSheet = new CSSStyleSheet();

		styleSheet.parse(value);

		return styleSheet;

	}

	public static String removeComments(String source) {

		int pos;

		do {
			pos = source.indexOf("/*");
			if (pos > -1) {
				int end = source.indexOf("*/", pos);
				if (end < 0) {
					source = source.substring(0, pos);
				} else {
					source = source.substring(0, pos) + source.substring(end + "*/".length());
				}
			}
		} while (pos > -1);

		return source;
	}

	public static List<CSSProperty> parseInlineRules(String source) {

		String[] props = source.split(";");

		List<CSSProperty> properties = new ArrayList<CSSProperty>(props.length);

		for (String p : props) {
			if (StringUtils.isNotBlank(p)) {
				try {
					properties.add(new CSSProperty(p));
				} catch (Exception e) {
					LoggerUtils.getLogger(CSSUtils.class).log(Level.WARNING, e.getMessage());
				}
			}
		}

		return properties;

	}

	public static interface UrlRewriteCallback {

		public String rewrite(String url);

	}

}
