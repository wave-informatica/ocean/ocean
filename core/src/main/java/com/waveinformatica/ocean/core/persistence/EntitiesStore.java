/*
 * Copyright 2017, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.persistence;

import com.waveinformatica.ocean.core.Constants;
import com.waveinformatica.ocean.core.annotations.OceanPersistenceUnit;
import com.waveinformatica.ocean.core.modules.ModuleClassLoader;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Entity;

/**
 *
 * @author ivano
 */
@ApplicationScoped
public class EntitiesStore {
	
	// module name, connection name, entities
	private final Map<String, Map<String, Set<Class<?>>>> entitiesMap = new HashMap<String, Map<String, Set<Class<?>>>>();
	
	private final Set<String> connectionNames = new TreeSet<String>();
	
	public void loadEntitiesInfo(ClassLoader classLoader, Map<String, Set<String>> annotatedClasses) {
		
		Set<String> entityNames = annotatedClasses.get(Entity.class.getName());
		if (entityNames.isEmpty()) {
			return;
		}
		
		String moduleName = Constants.CORE_MODULE_NAME;
		if (classLoader instanceof ModuleClassLoader) {
			moduleName = ((ModuleClassLoader) classLoader).getModule().getName();
		}
		
		Map<String, Set<Class<?>>> moduleEntities = entitiesMap.get(moduleName);
		if (moduleEntities == null) {
			moduleEntities = new HashMap<String, Set<Class<?>>>();
			entitiesMap.put(moduleName, moduleEntities);
		}
		
		for (String s : entityNames) {
			try {
				
				Class<?> entityCls = classLoader.loadClass(s);
				
				OceanPersistenceUnit opu = entityCls.getAnnotation(OceanPersistenceUnit.class);
				
				String[] catalog = new String[] { "" };
				if (opu != null) {
					catalog = opu.value();
				}
				
				for (String c : catalog) {
					Set<Class<?>> entities = moduleEntities.get(c);
					if (entities == null) {
						entities = new HashSet<Class<?>>();
						moduleEntities.put(c, entities);
					}
					entities.add(entityCls);
					
					connectionNames.add(c);
				}
				
			} catch (ClassNotFoundException ex) {
				LoggerUtils.getCoreLogger().log(Level.SEVERE, "Entity class " + s + " not found", ex);
			}
		}
		
	}

	public Set<String> getConnectionNames() {
		return Collections.unmodifiableSet(connectionNames);
	}
	
	public Set<Class<?>> getEntityClasses(String moduleName, String connectionName) {
		
		Map<String, Set<Class<?>>> moduleEntities = entitiesMap.get(moduleName);
		if (moduleEntities == null) {
			return Collections.emptySet();
		}
		
		Set<Class<?>> classes = moduleEntities.get(connectionName);
		if (classes == null) {
			return Collections.emptySet();
		}
		
		return Collections.unmodifiableSet(classes);
		
	}
	
	public Set<String> getEntityClassNames(String moduleName, String connectionName) {
		
		Set<Class<?>> classes = getEntityClasses(moduleName, connectionName);
		
		Set<String> names = new TreeSet<String>();
		
		for (Class<?> c : classes) {
			names.add(c.getName());
		}
		
		return names;
		
	}
	
	public void unloadModule(String moduleName) {
		entitiesMap.remove(moduleName);
	}
	
}
