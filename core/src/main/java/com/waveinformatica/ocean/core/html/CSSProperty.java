/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.html;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
public class CSSProperty {
	
	private String name;
	private List<String> values;
	private List<CSSValue> parsedValues;
	private boolean important;

	public CSSProperty() {
	}
	
	public CSSProperty(CSSConditionParser.MediaExpressionContext me) {
		name = me.IDENT().getText();
		this.parsedValues = parseExpressions(me.expression());
		if (parsedValues != null) {
			values = new ArrayList<String>(parsedValues.size());
			for (CSSValue pv : parsedValues) {
				values.add(pv.toString());
			}
		}
	}
	
	public CSSProperty(CSSConditionParser.PropertyContext pc) {
		name = pc.IDENT().getText();
		if (pc.IMPORTANT() != null && "!important".equalsIgnoreCase(pc.IMPORTANT().getText())) {
			important = true;
		}
		this.parsedValues = parseExpressions(pc.expression());
	}
	
	public CSSProperty(String declaration) {
		
		if (declaration == null) {
			throw new NullPointerException();
		}
		
		declaration = declaration.trim();
		
		int pos = declaration.indexOf(':');
		
		if (pos == -1) {
			throw new IllegalArgumentException("Invalid CSS property \"" + declaration + "\"");
		}
		
		name = declaration.substring(0, pos).trim();
		String value = declaration.substring(pos + 1).trim();
		important = value.contains("!important");
		
		values = new ArrayList<String>();
		for (String s : Arrays.asList(value.split(","))) {
			s = s.replaceAll("!important", "").replaceAll("\\s+", " ").trim();
			if (StringUtils.isNotBlank(s)) {
				values.add(s);
			}
		}
	}
	
	public CSSProperty(String name, String value) {
		
		this.name = name;
		
		if (value != null) {
			important = value.contains("!important");

			values = new ArrayList<String>();
			for (String s : Arrays.asList(value.replaceAll("\\!important", "").split(","))) {
				if (StringUtils.isNotBlank(s)) {
					values.add(s);
				}
			}
		}
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getValues() {
		return values;
	}
	
	public void setValues(List<String> values) {
		this.values = values;
		this.parsedValues = null;
	}
	
	public void setValue(String value) {
		this.values = new ArrayList<String>(1);
		this.values.add(value);
		this.parsedValues = null;
	}

	public boolean isImportant() {
		return important;
	}

	public void setImportant(boolean important) {
		this.important = important;
	}

	public List<CSSValue> getParsedValues() {
		if (parsedValues == null) {
			parseValues();
		}
		return parsedValues;
	}

	@Override
	public String toString() {
		return name + ":" + StringUtils.join(values, ',') + (important ? " !important" : "") + ";";
	}
	
	private void parseValues() {
		
		String prop = name + ":" + StringUtils.join(values, ',');
		
		ANTLRInputStream input = new ANTLRInputStream(prop);
		TokenStream tokens = new CommonTokenStream(new CSSConditionLexer(input));

		CSSConditionParser parser = new CSSConditionParser(tokens);
		this.parsedValues = parseExpressions(parser.property().expression());
		
	}
	
	private List<CSSValue> parseExpressions(List<CSSConditionParser.ExpressionContext> expressions) {
		
		if (expressions != null) {
			
			List<CSSValue> parsedValues = new ArrayList<CSSValue>(expressions.size());
			
			for (CSSConditionParser.ExpressionContext e : expressions) {
				
				List<CSSValue> parts = new ArrayList<CSSValue>();
				
				for (CSSConditionParser.TermContext tc : e.term()) {
					if (tc instanceof CSSConditionParser.FunctionExprContext) {
						CSSConditionParser.FunctionExprContext fec = (CSSConditionParser.FunctionExprContext) tc;
						if (fec.function().IDENT().getText().equalsIgnoreCase("rgb")) {
							parts.add(new CSSValueColor(
								  Double.parseDouble(fec.function().expression().get(0).term().get(0).getText()),
								  Double.parseDouble(fec.function().expression().get(1).term().get(0).getText()),
								  Double.parseDouble(fec.function().expression().get(2).term().get(0).getText())
							));
						}
						else if (fec.function().IDENT().getText().equalsIgnoreCase("rgba")) {
							parts.add(new CSSValueColor(
								  Double.parseDouble(fec.function().expression().get(0).term().get(0).getText()),
								  Double.parseDouble(fec.function().expression().get(1).term().get(0).getText()),
								  Double.parseDouble(fec.function().expression().get(2).term().get(0).getText()),
								  Double.parseDouble(fec.function().expression().get(3).term().get(0).getText())
							));
						}
						else {
							parts.add(new CSSValueFunction(
								  fec.function().IDENT().getText(),
								  parseExpressions(fec.function().expression())
							));
						}
					}
					else if (tc instanceof CSSConditionParser.StringExprContext) {
						CSSConditionParser.StringExprContext sec = (CSSConditionParser.StringExprContext) tc;
						String v = sec.STRING().getText();
						CSSValueString str = new CSSValueString(v.substring(1, v.length() - 1));
						parts.add(str);
					}
					else if (tc instanceof CSSConditionParser.CalcExprContext) {
						CSSConditionParser.CalcExprContext cec = (CSSConditionParser.CalcExprContext) tc;
					}
					else if (tc instanceof CSSConditionParser.HexColorExprContext) {
						CSSConditionParser.HexColorExprContext hcec = (CSSConditionParser.HexColorExprContext) tc;
						parts.add(new CSSValueColor(hcec.HEX_COLOR().getText()));
					}
					else if (tc instanceof CSSConditionParser.IdExprContext) {
						CSSConditionParser.IdExprContext iec = (CSSConditionParser.IdExprContext) tc;
						parts.add(new CSSValueName(iec.IDENT().getText()));
					}
					else if (tc instanceof CSSConditionParser.UrlExprContext) {
						CSSConditionParser.UrlExprContext uec = (CSSConditionParser.UrlExprContext) tc;
						String url = uec.URL().getText();
						url = url.substring("url(".length(), url.length() - 1);
						parts.add(new CSSValueUrl(url));
					}
					else if (tc instanceof CSSConditionParser.NumberExprContext) {
						CSSConditionParser.NumberExprContext nec = (CSSConditionParser.NumberExprContext) tc;
						CSSValueNumber num = new CSSValueNumber(
							  Double.parseDouble(nec.number().NUMBER().getText()),
							  nec.number().UNIT() != null ? nec.number().UNIT().getText() : null
						);
						parts.add(num);
					}
				}
				
				if (parts.size() == 1) {
					parsedValues.add(parts.get(0));
				}
				else if (parts.size() > 1) {
					parsedValues.add(new CSSValueComposite(parts));
				}
			}
			
			return parsedValues;
			
		}
		else {
			return null;
		}
		
	}
	
}
