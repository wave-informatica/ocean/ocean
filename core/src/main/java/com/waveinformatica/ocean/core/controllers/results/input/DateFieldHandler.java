/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.controllers.results.input;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.i18n.DateFormatConverter;
import com.waveinformatica.ocean.core.util.Results;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;

/**
 *
 * @author Ivano
 */
public class DateFieldHandler implements InputResult.FieldHandler {
	
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	private final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	
	public static void loadResources(BaseResult result) {
		Results.addScriptSource(result, "ris/libs/momentjs/moment-with-locales.js");
		Results.addCssSource(result, "ris/libs/eonasdan-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");
		Results.addScriptSource(result, "ris/libs/eonasdan-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");
		Results.addScriptSource(result, "ris/libs/datepicker/datepicker.js");
	}
	
	@Override
	public String getFieldTemplate() {
		return "/templates/core/components/fieldDate.tpl";
	}
	
	@Override
	public void init(InputResult result, CoreController.OperationInfo opInfo, CoreController.ParameterInfo paramInfo, InputResult.FormField field) {
		loadResources(result);
	}
	
	public static boolean canHandle(Class<?> type) {
		return type == java.util.Date.class || type == java.sql.Date.class;
	}
	
	@Override
	public String formatValue(InputResult result, InputResult.FormField field, Object value) {
		if (DateUtils.truncate(value, Calendar.DATE).equals(value)) {
			return dateFormat.format(value);
		}
		else {
			return dateTimeFormat.format(value);
		}
	}
	
	public String getClientFormat(String sdfFormat) {
		
		StringBuilder builder = new StringBuilder();
		
		DateFormatConverter converter = new DateFormatConverter();
		
		List<DateFormatConverter.Token> tokens = converter.tokenize(sdfFormat);
		for (DateFormatConverter.Token t : tokens) {
			if (t.getType() == null) {
				builder.append(t.getText());
			}
			else {
				switch (t.getType()) {
					case DAY_WEEK_NAME_3:
						builder.append("ddd");
						break;
					case DAY_WEEK_NAME_FULL:
						builder.append("dddd");
						break;
					case MONTH_1_DIGIT:
						builder.append("M");
						break;
					case MONTH_2_DIGITS:
						builder.append("MM");
						break;
					case MONTH_NAME_3:
						builder.append("MMM");
						break;
					case MONTH_NAME_FULL:
						builder.append("MMMM");
						break;
					case YEAR_2_DIGITS:
						builder.append("YY");
						break;
					case YEAR_4_DIGITS:
						builder.append("YYYY");
						break;
					case DAY_IN_MONTH_1:
						builder.append("D");
						break;
					case DAY_IN_MONTH_2:
						builder.append("DD");
						break;
					case HOUR_IN_DAY_23_2:
						builder.append("HH");
						break;
					case HOUR_IN_DAY_23_1:
						builder.append("H");
						break;
					case MINUTE_2:
						builder.append("mm");
						break;
					case MINUTE_1:
						builder.append("m");
						break;
					case SECOND_2:
						builder.append("ss");
						break;
					case SECOND_1:
						builder.append("s");
						break;
					case MILLISECOND_3:
						builder.append("SSS");
						break;
					case MILLISECOND_1:
						builder.append("S");
						break;
				}
			}
		}
		
		return builder.toString();
		
	}
	
}
