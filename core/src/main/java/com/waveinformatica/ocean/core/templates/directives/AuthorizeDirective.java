/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.templates.directives;

import com.waveinformatica.ocean.core.annotations.FreemarkerDirective;
import com.waveinformatica.ocean.core.controllers.scopes.OperationScope;
import com.waveinformatica.ocean.core.controllers.scopes.ScopeChain;
import com.waveinformatica.ocean.core.controllers.scopes.ScopesController;
import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import java.io.IOException;
import java.util.Map;
import javax.inject.Inject;

/**
 *
 * @author Ivano
 */
@FreemarkerDirective("authorize")
public class AuthorizeDirective implements TemplateDirectiveModel {
	
	@Inject
	private ScopesController scopesController;
	
	@Override
	public void execute(Environment e, Map map, TemplateModel[] tms, TemplateDirectiveBody tdb) throws TemplateException, IOException {
		
		if (map.size() != 1) {
			throw new TemplateModelException("Only the \"operation\" parameter is required in <@authorize> directive");
		}
		
		Object par = map.get("operation");
		
		if (par == null || par.toString().trim().length() == 0) {
			throw new TemplateModelException("\"operation\" parameter is required in <@authorize> directive");
		}
		
		String opName = par.toString().trim();
		
		boolean negazione = opName.charAt(0) == '!';
		if (negazione) {
			opName = opName.substring(1);
		}
		
		ScopeChain chain = scopesController.build(opName);
		if (!(chain.getCurrent() instanceof OperationScope)) {
			throw new TemplateModelException("Operation with name defined in operation=\"" + opName + "\" parameter in <@authorize> directive was not found");
		}
		
		OperationScope opScope = (OperationScope) chain.getCurrent();
		
		if (opScope.isAuthorized() == !negazione) {
			tdb.render(e.getOut());
		}
		
	}
	
}
