/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.controllers.results.input;

import java.lang.reflect.ParameterizedType;

/**
 * @author Marco Merli
 * @author Ivano Culmine
 */
public class Range<T> {

	private T from;
	private T to;

	public Range() {}

	public Range(T from, T to) {

		this.from = from;
		this.to = to;
	}

	@SuppressWarnings("unchecked")
	public Class<T> getType()
	{
		ParameterizedType paramType = (ParameterizedType) getClass().getGenericSuperclass();
		return (Class<T>) paramType.getActualTypeArguments()[0];
	}

	public void setFrom(T from)
	{
		this.from = from;
	}

	public void setTo(T to)
	{
		this.to = to;
	}

	public T getFrom()
	{
		return from;
	}

	public T getTo()
	{
		return to;
	}
}
