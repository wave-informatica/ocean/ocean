/*
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.modules;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.ConcurrentHashMap;
import java.util.jar.JarInputStream;

import org.scannotation.archiveiterator.DirectoryIteratorFactory;
import org.scannotation.archiveiterator.FileProtocolIteratorFactory;
import org.scannotation.archiveiterator.Filter;
import org.scannotation.archiveiterator.IteratorFactory;
import org.scannotation.archiveiterator.JarIterator;
import org.scannotation.archiveiterator.StreamIterator;

public class OceanIteratorFactory extends IteratorFactory {

	private static final ConcurrentHashMap<String, DirectoryIteratorFactory> registry = new ConcurrentHashMap<String, DirectoryIteratorFactory>();

	static {
		registry.put("file", new FileProtocolIteratorFactory());
	}

	public static StreamIterator create(URL url, Filter filter)
			throws IOException {
		String urlString = url.toString();
		if (urlString.endsWith("!/")) {
			urlString = urlString.substring(4);
			urlString = urlString.substring(0, urlString.length() - 2);
			url = new URL(urlString);
		}

		if ("vfs".equals(url.getProtocol()) || !urlString.endsWith("/")) {
			InputStream is = url.openStream();
			if (is instanceof JarInputStream) {
				return new OceanJarIterator((JarInputStream) is, filter);
			}
			else {
				return new JarIterator(url.openStream(), filter);
			}
		} else {
			DirectoryIteratorFactory factory = registry.get(url.getProtocol());
			if (factory == null)
				throw new IOException("Unable to scan directory of protocol: "
						+ url.getProtocol());
			return factory.create(url, filter);
		}
	}

}
