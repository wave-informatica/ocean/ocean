/*
 * Copyright 2017, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.management;

import com.waveinformatica.ocean.core.annotations.OceanMBean;
import com.waveinformatica.ocean.core.exceptions.ModuleException;
import com.waveinformatica.ocean.core.modules.Module;
import com.waveinformatica.ocean.core.modules.ModulesManager;
import com.waveinformatica.ocean.core.util.LoggerUtils;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import javax.inject.Inject;

/**
 *
 * @author ivano
 */
@OceanMBean
public class OceanManager implements OceanManagerMBean {

	@Inject
	private ModulesManager modulesManager;
	
	@Override
	public String[] listLoadedModules() {
		
		List<Module> modules = modulesManager.getModules();
		
		String[] result = new String[modules.size()];
		
		for (int i = 0; i < modules.size(); i++) {
			result[i] = modules.get(i).getName();
		}
		
		return result;
		
	}

	@Override
	public boolean reloadModule(String name) {
		
		try {
			
			modulesManager.reloadModule(name);
			return true;
			
		} catch (ModuleException ex) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, null, ex);
			return false;
		} catch (IOException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
			return false;
		}
		
	}

	@Override
	public boolean unloadModule(String name) {
		
		Module m = modulesManager.getModule(name);
		
		if (m == null) {
			return false;
		}
		else {
			try {
				modulesManager.unloadModule(name, true);
				return true;
			} catch (ModuleException e) {
				LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unexpected error unloading module " + name + " from JMX console");
				return false;
			}
		}
		
	}

	@Override
	public boolean getModuleStatus(String name) {
		
		Module m = modulesManager.getModule(name);
		
		return m != null && m.isLoaded();
		
	}
	
}
