/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.html;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Ivano
 */
public class Measure implements Comparable<Measure> {
	
	private static final Pattern numPattern = Pattern.compile("^[\\+-]?[0-9\\.]+");
	private static final Pattern unitPattern = Pattern.compile("[a-zA-Z]+$");
	
	private Double value;
	private Unit unit;

	public Measure(double value) {
		this.value = value;
	}

	public Measure(double value, Unit unit) {
		this.value = value;
		this.unit = unit;
	}

	public Measure(double value, String unit) {
		this.value = value;
		Matcher matcher = unitPattern.matcher(unit);
		if (matcher.find()) {
			String u = matcher.group().toUpperCase();
			try {
				this.unit = Unit.valueOf(u);
			} catch (IllegalArgumentException e) {
				this.unit = null;
			}
		}
	}

	public Measure(String value) {
		Matcher matcher = numPattern.matcher(value);
		if (matcher.find()) {
			this.value = Double.parseDouble(matcher.group());
		}
		matcher = unitPattern.matcher(value);
		if (matcher.find()) {
			String u = matcher.group().toUpperCase();
			try {
				unit = Unit.valueOf(u);
			} catch (IllegalArgumentException e) {
				unit = null;
			}
		}
	}

	public Measure() {
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}
	
	public Measure convert(Unit unit) {
		if (this.unit == null || unit == null) {
			return null;
		}
		switch (this.unit) {
			case PX:
				switch (unit) {
					case PX:
						return new Measure(this.value, this.unit);
					case PT:
						return new Measure(this.value / 96.0 * 72.0, unit);
					case MM:
						return new Measure(this.value / 96.0 * 25.4, unit);
					case CM:
						return new Measure(this.value / 96.0 * 2.54, unit);
					case IN:
						return new Measure(this.value / 96.0, unit);
				}
				break;
			case PT:
				switch (unit) {
					case PX:
						return new Measure(this.value / 72.0 * 96.0, unit);
					case PT:
						return new Measure(this.value, this.unit);
					case MM:
						return new Measure(this.value / 72.0 * 25.4, unit);
					case CM:
						return new Measure(this.value / 72.0 * 2.54, unit);
					case IN:
						return new Measure(this.value / 72.0, unit);
				}
				break;
			case MM:
				switch (unit) {
					case PX:
						return new Measure(this.value / 25.4 * 96.0, unit);
					case PT:
						return new Measure(this.value / 25.4 * 72.0, unit);
					case MM:
						return new Measure(this.value, this.unit);
					case CM:
						return new Measure(this.value / 10.0, unit);
					case IN:
						return new Measure(this.value / 25.4, unit);
				}
				break;
			case CM:
				switch (unit) {
					case PX:
						return new Measure(this.value / 2.54 * 96.0, unit);
					case PT:
						return new Measure(this.value / 2.54 * 72.0, unit);
					case MM:
						return new Measure(this.value * 10.0, unit);
					case CM:
						return new Measure(this.value, this.unit);
					case IN:
						return new Measure(this.value / 2.54, unit);
				}
				break;
			case IN:
				switch (unit) {
					case PX:
						return new Measure(this.value * 96.0, unit);
					case PT:
						return new Measure(this.value * 72.0, unit);
					case MM:
						return new Measure(this.value / 25.4, unit);
					case CM:
						return new Measure(this.value * 2.54, unit);
					case IN:
						return new Measure(this.value, this.unit);
				}
				break;
			case DPI:
				switch (unit) {
					case DPI:
						return new Measure(this.value, this.unit);
					case DPCM:
						return new Measure(this.value / 2.54, unit);
				}
				break;
			case DPCM:
				switch (unit) {
					case DPI:
						return new Measure(this.value * 2.54, unit);
					case DPCM:
						return new Measure(this.value, this.unit);
				}
				break;
		}
		return null;
	}

	@Override
	public int compareTo(Measure o) {
		
		if (unit == null && o.unit == null) {
			return value.compareTo(o.value);
		}
		else if (unit == null || o.unit == null) {
			throw new NullPointerException();
		}
		else {
			return value.compareTo(o.convert(unit).value);
		}
		
	}
	
	public Double div(Measure m) {
		return value / m.convert(unit).value;
	}
	
	public static enum Unit {
		PX,
		PT,
		MM,
		CM,
		IN,
		DPI,
		DPCM
	}
	
}
