/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.util;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.decorators.AbstractTemplateDecorator;
import com.waveinformatica.ocean.core.controllers.dto.DecoratorInfo;
import com.waveinformatica.ocean.core.controllers.dto.OperationContext;
import com.waveinformatica.ocean.core.controllers.events.BeforeResultEvent;
import com.waveinformatica.ocean.core.controllers.events.WebRequestEvent;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.HttpErrorResult;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.scopes.AbstractScope;
import com.waveinformatica.ocean.core.interfaces.ScopeAware;

public class DecoratorHelper {

	private static final Logger logger = LoggerUtils.getCoreLogger();

	@Inject
	private ObjectFactory factory;

	@Inject
	private CoreController controller;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void decorateResult(Object result, OutputStream out, OperationContext opCtx, MimeTypes... types) {
		
		if (result == null) {
			return;
		}
		
		if (result instanceof BaseResult) {
			BeforeResultEvent evt = new BeforeResultEvent(this, opCtx != null ? opCtx.getOperation() : null, (BaseResult) result, Context.get() != null ? Context.get().getRequest() : null);
			controller.dispatchEvent(evt);
		}

		DecoratorInfo decorator = controller.findDecorator(result.getClass(), Arrays.asList(types), getScope(opCtx));
		if (decorator != null) {
			
			if (opCtx != null) {
				opCtx.setDecoratorInfo(decorator);
			}
			
			if (opCtx != null && opCtx.getChain() != null && decorator.getDecorator() instanceof ScopeAware) {
				((ScopeAware) decorator.getDecorator()).setScope(opCtx.getChain().getCurrent());
			}

			MimeTypes mt = Context.getExpectedType();

			try {

				Context.setExpectedType(decorator.getMimeType());

				if (opCtx != null && decorator.getDecorator() instanceof AbstractTemplateDecorator) {
					((AbstractTemplateDecorator) decorator.getDecorator()).setExtraObjects(opCtx.getAllObjects());
					((AbstractTemplateDecorator) decorator.getDecorator()).getExtraObjects().put("operationContext", opCtx);
				}

				decorator.getDecorator().decorate(result, out, decorator.getMimeType());

			} catch (Exception ex) {

				LoggerUtils.getLogger(decorator.getDecorator().getClass()).log(Level.SEVERE, "Error decorating result of type \"" + result.getClass().getName() + "\" from operation \"" + (opCtx.getOperation() != null ? opCtx.getOperation().getFullName() : "[null]") + "\" with decorator \"" + decorator.getDecorator().getClass().getName() + "\"", ex);
				if (!(result instanceof HttpErrorResult)) {
					result = factory.newInstance(HttpErrorResult.class);
					((HttpErrorResult) result).setData(ex);
					((HttpErrorResult) result).setErrorCode(500);
					decorateResult(result, out, opCtx, types);
				}

			} finally {

				Context.setExpectedType(mt);

			}
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void decorateResult(Object result, WebRequestEvent e, OperationContext opCtx) {

		if (opCtx != null && result instanceof BaseResult) {
			BeforeResultEvent evt = new BeforeResultEvent(this, opCtx.getOperation(), (BaseResult) result, e.getRequest());
			controller.dispatchEvent(evt);
		}
		
		AbstractScope scope = getScope(opCtx);

		// Look for a decorator
		Class cls = result.getClass();

		List<MimeTypes> types = new LinkedList<MimeTypes>();
		if (e.getRequest().getParameter("fw:type") != null) {
			try {
				types.add(MimeTypes.valueOf(e.getRequest().getParameter("fw:type").toUpperCase().trim()));
			} catch (Exception ex) {
				logger.warning("Unknown fw:type \"" + e.getRequest().getParameter("fw:type") + "\"");
			}
		}
		String acceptHeader = e.getRequest().getHeader("Accept");
		if (StringUtils.isNotBlank(acceptHeader)) {
			List<String> parts = new ArrayList<String>(Arrays.asList(acceptHeader.split(",")));
			Iterator<String> iter = parts.iterator();
			while (iter.hasNext()) {
				String p = iter.next();
				if (p.trim().startsWith("*/*")) {
					iter.remove();
				}
			}
			acceptHeader = StringUtils.join(parts, ", ");
		}
		if (StringUtils.isNotBlank(acceptHeader)) {
			Map<String, MimeTypes> knownMimeTypes = new HashMap<String, MimeTypes>();
			for (final MimeTypes mt : MimeTypes.values()) {
				List<MimeTypes> tmpTypes = Arrays.asList(mt);
				if (controller.findDecorator(cls, tmpTypes, scope) != null) {
					knownMimeTypes.put(mt.getMimeType(), mt);
				}
			}
			if (!knownMimeTypes.isEmpty()) {
				String acceptMime = MIMEParse.bestMatch(knownMimeTypes.keySet(), acceptHeader, 1.0f);
				if (acceptMime != null && acceptMime.trim().length() > 0
						&& knownMimeTypes.get(acceptMime) != MimeTypes.HTML && knownMimeTypes.get(acceptMime).isAllowAcceptHeader()) {
					types.add(knownMimeTypes.get(acceptMime));
				}
			}
		}
		if (opCtx != null && opCtx.getOperation() != null) {
			types.add(opCtx.getOperation().getOperation().defaultOutputType());
		}
		if (!types.contains(MimeTypes.HTML)) {
			types.add(MimeTypes.HTML);
		}

		DecoratorInfo decorator = controller.findDecorator(cls, types, scope);
		if (decorator != null) {

			if (opCtx != null) {
				opCtx.setDecoratorInfo(decorator);
			}
			
			if (opCtx.getChain() != null && decorator.getDecorator() instanceof ScopeAware) {
				((ScopeAware) decorator.getDecorator()).setScope(opCtx.getChain().getCurrent());
			}

			try {
				e.getResponse().setContentType(decorator.getMimeType().getMimeType());
				Context.setExpectedType(decorator.getMimeType());
				if (opCtx != null && decorator.getDecorator() instanceof AbstractTemplateDecorator) {
					((AbstractTemplateDecorator) decorator.getDecorator()).setExtraObjects(opCtx.getAllObjects());
					((AbstractTemplateDecorator) decorator.getDecorator()).getExtraObjects().put("operationContext", opCtx);
				}
				decorator.getDecorator().decorate(result, e.getRequest(), e.getResponse(), decorator.getMimeType());
			} catch (Exception ex) {
				LoggerUtils.getLogger(decorator.getDecorator().getClass()).log(Level.SEVERE, "Error decorating result of type \"" + result.getClass().getName() + "\" from operation \"" + (opCtx != null && opCtx.getOperation() != null ? opCtx.getOperation().getFullName() : "null") + "\" with decorator \"" + decorator.getDecorator().getClass().getName() + "\"", ex);
				if (!(result instanceof HttpErrorResult)) {
					result = factory.newInstance(HttpErrorResult.class);
					((HttpErrorResult) result).setData(ex);
					((HttpErrorResult) result).setErrorCode(500);
					decorateResult(result, e, opCtx);
				}
			}
		}

	}
	
	private AbstractScope getScope(OperationContext opCtx) {
		
		AbstractScope scope = null;
		if (opCtx != null && opCtx.getChain() != null) {
			AbstractScope curr = opCtx.getChain().getCurrent();
			if (curr instanceof OperationProvider) {
				scope = curr.getParent();
			}
			else if (curr != null) {
				scope = curr;
			}
		}
		
		return scope;
		
	}

}
