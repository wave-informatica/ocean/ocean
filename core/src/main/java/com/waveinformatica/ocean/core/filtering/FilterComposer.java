/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.filtering;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.filtering.drivers.JpqlBuilderDriver;
import com.waveinformatica.ocean.core.filtering.drivers.QueryBuilderDriver;
import com.waveinformatica.ocean.core.filtering.drivers.ValuesManager;

/**
 * The FilterComposer is useful to compose filters with several {@link QueryFilter} objects.
 * It can be used as a {@link QueryFilter} too to feed other {@code FilterComposer}s.
 * 
 * @author ivano
 *
 */
public class FilterComposer<T> extends QueryFilter<T> {
	
	private FilterMode mode;
	private final List<QueryFilter<T>> filters = new ArrayList<>();
	private final QueryBuilderDriver driver;
	
	@Deprecated
	public FilterComposer(FilterMode mode, QueryFilter<T> ...filters) {
		this.mode = mode;
		this.filters.addAll(Arrays.asList(filters));
		this.driver = new JpqlBuilderDriver(new ValuesManager(), "");
	}
	
	public FilterComposer(FilterMode mode, QueryBuilderDriver<T> driver, ValuesManager values, QueryFilter<T> ...filters) {
		super(values);
		this.mode = mode;
		this.filters.addAll(Arrays.asList(filters));
		this.driver = driver;
	}
	
	public void add(QueryFilter<T> filter) {
		filters.add(filter);
	}
	
	public void remove(QueryFilter<T> filter) {
		filters.remove(filter);
	}

	public FilterMode getMode() {
		return mode;
	}

	public void setMode(FilterMode mode) {
		this.mode = mode;
	}

	public List<QueryFilter<T>> getFilters() {
		return Collections.unmodifiableList(filters);
	}
	
	@Override
	@Deprecated
	public String getCondition(String prefix) {
		String condition = getCondition();
		return StringUtils.isNotBlank(condition) ? prefix + condition : "";
	}

	@Override
	@Deprecated
	public String getCondition() {
		
		String conjunction = " " + mode.name().toLowerCase() + " ";
		
		boolean group = false;
		
		StringBuilder builder = new StringBuilder();
		for (QueryFilter filter : filters) {
			String condition = filter.getCondition();
			if (StringUtils.isNotBlank(condition)) {
				if (!group && builder.length() > 0) {
					group = true;
				}
				builder.append(conjunction);
				builder.append('(');
				builder.append(condition);
				builder.append(')');
			}
		}
		
		if (builder.length() == 0) {
			return "";
		}
		
		String result = builder.substring(conjunction.length());
		
		if (StringUtils.isNotBlank(result)) {
			result = "(" + result + ")";
		}
		
		return result;
	}
	
	@Override
	@Deprecated
	public <T extends Query> T fillQueryParams(T query) {
		for (QueryFilter filter : filters) {
			filter.fillQueryParams(query);
		}
		return query;
	}
	
	@Override
	public T getWhereClause() {
		List<T> conditions = new ArrayList<>();
		for (QueryFilter<T> qf : filters) {
			T wc = qf.getWhereClause();
			if (wc != null) {
				conditions.add(wc);
			}
		}
		if (conditions.isEmpty()) {
			return null;
		}
		else if (mode == FilterMode.AND) {
			return (T) driver.and(conditions.toArray((T[]) Array.newInstance(driver.getQueryType(), conditions.size())));
		}
		else {
			return (T) driver.or(conditions.toArray((T[]) Array.newInstance(driver.getQueryType(), conditions.size())));
		}
	}
	
}
