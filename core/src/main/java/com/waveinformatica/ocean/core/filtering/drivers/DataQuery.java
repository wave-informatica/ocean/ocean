package com.waveinformatica.ocean.core.filtering.drivers;

public class DataQuery<T> implements DataReference {
	
	private final T value;
	
	public DataQuery(T value) {
		this.value = value;
	}

	public T getValue() {
		return value;
	}
	
}
