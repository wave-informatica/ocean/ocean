/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.events;

import com.waveinformatica.ocean.core.security.AuthenticationHandler;
import com.waveinformatica.ocean.core.security.OceanAuthenticationHandler;
import com.waveinformatica.ocean.core.security.SecurityManager;

/**
 *
 * @author Ivano
 */
public class SecurityConfigurationEvent extends Event {
    
    private final SecurityManager securityManager;
    
    private boolean authenticationNeeded = false;
    private String loginOperationBase = "~/auth/loginPage";
    private AuthenticationHandler handler = new OceanAuthenticationHandler();
    
    public SecurityConfigurationEvent(Object source, SecurityManager securityManager) {
        super(source, CoreEventName.SECURITY_CONFIGURATION.name());
        this.securityManager = securityManager;
    }

    public boolean isAuthenticationNeeded() {
        return authenticationNeeded;
    }

    public void setAuthenticationNeeded(boolean authenticationNeeded) {
        this.authenticationNeeded = authenticationNeeded;
    }

    public String getLoginOperationBase() {
        return loginOperationBase;
    }

    public void setLoginOperationBase(String loginOperationBase) {
        this.loginOperationBase = loginOperationBase;
    }

    public SecurityManager getSecurityManager() {
        return securityManager;
    }

	public AuthenticationHandler getHandler() {
		return handler;
	}

	public void setHandler(AuthenticationHandler handler) {
		this.handler = handler;
	}

}
