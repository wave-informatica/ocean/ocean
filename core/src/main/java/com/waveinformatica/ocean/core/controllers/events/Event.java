/*
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.events;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class represents the base class for events.
 * 
 * @author Ivano
 */
public class Event {
    
    private final String name;
    private final Object source;
    private final List<String> flags;
    
    /**
     * Constructs an Event with the source object and the event name.
     * 
     * @param source the source object (which fired the event)
     * @param name the name of the event
     */
    public Event(Object source, String name) {
        this.name = name;
        this.source = source;
        this.flags = new ArrayList<String>(0);
    }
    
    /**
     * Constructs an Event with the source object, the event name and a list of flags.
     * 
     * When a Event is fired with a list of flags, a EventListener will be called only
     * if it specified almost one of the event's flags.
     * 
     * @param source the source object (which fired the event)
     * @param name the name of the event
     * @param flags the list of flags
     */
    public Event(Object source, String name, String[] flags) {
        this.name = name;
        this.source = source;
        this.flags = Arrays.asList(flags);
    }
    
    /**
     * Returns the name of the event
     * 
     * @return the name of the event
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the object which fired the event (if available)
     * 
     * @return the object which fired the event
     */
    public Object getSource() {
        return source;
    }

    /**
     * Returns the flags specified for the event (if any) or an empty list.
     * 
     * The value is never <code>null</code>
     * 
     * @return the list of flags for the event
     */
    public List<String> getFlags() {
        return flags;
    }
    
}
