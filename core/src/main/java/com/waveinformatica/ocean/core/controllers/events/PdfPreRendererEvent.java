/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.events;

import org.w3c.dom.Document;
import org.xhtmlrenderer.layout.SharedContext;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.waveinformatica.ocean.core.controllers.results.WebPageResult;

/**
 * @author Ivano Culmine
 * @author Marco Merli
 */
public class PdfPreRendererEvent extends Event {

	private final Document document;
	private final ITextRenderer renderer;
	private final SharedContext sharedContext;
	private final WebPageResult result;

	public PdfPreRendererEvent(Object source, String operation,
		Document document, ITextRenderer renderer, SharedContext sharedContext, WebPageResult result) {

		super(source, CoreEventName.PDF_RENDERER_PRE_LAYOUT.name(), new String[] {
			operation, "*"
		});

		this.document = document;
		this.renderer = renderer;
		this.sharedContext = sharedContext;
		this.result = result;
	}

	public Document getDocument()
	{
		return document;
	}

	public ITextRenderer getRenderer()
	{
		return renderer;
	}

	public SharedContext getSharedContext() {
		return sharedContext;
	}

	public WebPageResult getResult()
	{
		return result;
	}
}
