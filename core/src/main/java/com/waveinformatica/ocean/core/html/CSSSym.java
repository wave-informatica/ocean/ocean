/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.html;

/**
 *
 * @author Ivano
 */
public enum CSSSym {
	SELECTOR,
	SELECTOR_COMMA,
	PROPERTY_NAME,
	PROPERTY_VALUE,
	PROP_SPEC_SEMICOLON,
	PROP_SPEC_COLON,
	CSS_DIRECTIVE,
	DIR_CHARSET,
	DIR_IMPORT,
	DIR_NAMESPACE,
	DIR_DOCUMENT,
	DIR_KEYFRAMES,
	DIR_MEDIA,
	DIR_PAGE,
	DIR_SUPPORTS,
	DIR_END,
	DIR_VALUE,
	DIR_ARG,
	BODY_BEGIN,
	BODY_END
}
