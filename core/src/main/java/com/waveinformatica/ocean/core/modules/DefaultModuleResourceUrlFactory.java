package com.waveinformatica.ocean.core.modules;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;

import com.waveinformatica.ocean.core.urlhandlers.OceanUrlFactory;
import com.waveinformatica.ocean.core.util.LoggerUtils;

public class DefaultModuleResourceUrlFactory implements OceanUrlFactory {

	@Override
	public URL createResourceUrl(JarFile file, JarEntry entry) {
		
		try {
			
			String entryPath = entry.getName();
			if (!entryPath.startsWith("/")) {
				entryPath = "/" + entryPath;
			}
			return new URL("jar:" + new File(file.getName()).toURI() + "!/" + entryPath);
			
		} catch (MalformedURLException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unable to create jar URL", e);
			return null;
		}
	}

}
