/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.events;

import com.waveinformatica.ocean.core.modules.Module;
import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Ivano
 */
public class ModuleEvent extends Event {
    
    private final boolean loaded;
    private final String moduleName;
    private Map<Class<? extends Annotation>, Set<Class<?>>> annotatedClasses;
    
    public ModuleEvent(Object source, boolean loaded, Module module) {
        
        super(source, loaded ? CoreEventName.MODULE_LOADED.name() : CoreEventName.MODULE_UNLOADED.name());
        
        this.loaded = loaded;
        this.moduleName = module.getName();
        
    }
    
    public ModuleEvent(Object source, boolean loaded, String moduleName) {
        
        super(source, loaded ? CoreEventName.MODULE_LOADED.name() : CoreEventName.MODULE_UNLOADED.name());
        
        this.loaded = loaded;
        this.moduleName = moduleName;
        
    }

    public boolean isLoaded() {
        return loaded;
    }

    public String getModuleName() {
        return moduleName;
    }

    public Map<Class<? extends Annotation>, Set<Class<?>>> getAnnotatedClasses() {
        return annotatedClasses;
    }
    
    public Set<Class<?>> getAnnotatedClasses(Class<? extends Annotation> annotation) {
        return annotatedClasses.get(annotation);
    }

    public void setAnnotatedClasses(Map<Class<? extends Annotation>, Set<Class<?>>> annotatedClasses) {
        this.annotatedClasses = annotatedClasses;
    }
    
}
