/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.persistence;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.waveinformatica.ocean.core.controllers.InstanceListener;
import com.waveinformatica.ocean.core.internal.PersistenceConfigurationManager;
import com.waveinformatica.ocean.core.modules.ModulesManager;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.apache.commons.lang.StringUtils;

/**
 * This class, when injected in a bean, can be used to obtain a connection from the web application's connection pool.
 *
 * @author Ivano
 */
@Named
@ApplicationScoped
public class DataSourceProvider {
	
	private final Map<String, DataSource> dataSources = new HashMap<String, DataSource>();
	private final Map<String, ComboPooledDataSource> poolDataSources = new HashMap<String, ComboPooledDataSource>();
	
	@Inject
	private ModulesManager modulesManager;
	
	@Inject
	private PersistenceConfigurationManager manager;
	
	/**
	 * Returns a connection to the web application's database. The developer must ensure to
	 * close the connection when his job is done.
	 *
	 * @return a connection to the database
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		
		return getConnection(manager.getDefaultConnection());
		
	}
	
	public Connection getConnection(String connectionName) throws SQLException {
		
		return getConnection(manager.getConnection(connectionName));
		
	}
	
	public DataSource getDataSource(String configurationName) throws SQLException {
		
		PersistenceConfigurationManager.Element element = null;
		if (StringUtils.isBlank(configurationName)) {
			element = manager.getDefaultConnection();
		} else {
			element = manager.getConnection(configurationName);
		}
		
		DataSource ds = dataSources.get(element.getConnectionName());
		
		if (ds == null) {
			if (element.getType() == PersistenceConfigurationManager.ConnectionType.JNDI) {
				try {
					
					InitialContext ctx = new InitialContext();
					ds = DataSourceProxy.wrap((DataSource) ctx.lookup(element.getJndiName()));
					dataSources.put(element.getConnectionName(), ds);
					
				} catch (NamingException ex) {
					LoggerUtils.getCoreLogger().log(Level.SEVERE, null, ex);
				}
			}
			else if (element.getType() == PersistenceConfigurationManager.ConnectionType.JDBC) {
				
				String module = null;
				String driver = element.getDriverClass();
				int sep = driver.indexOf(':');
				if (sep >= 0) {
					module = driver.substring(0, sep);
					driver = driver.substring(sep + 1);
				}
				
				ClassLoader cl = DataSourceProvider.class.getClassLoader();
				if (module != null) {
					cl = modulesManager.getModule(module).getModuleClassLoader();
				}
				
				try {
					
					Class<?> driverCls = cl.loadClass(driver);
					
					boolean found = false;
					Enumeration<Driver> en = DriverManager.getDrivers();
					while (en.hasMoreElements()) {
						
						Driver d = en.nextElement();
						
						if (driverCls.isAssignableFrom(d.getClass())) {
							found = true;
							break;
						}
						
					}
					
					if (!found) {
						try {
							
							Driver d = (Driver) driverCls.newInstance();
							DriverManager.registerDriver(d);
							
						} catch (InstantiationException e) {
							throw new SQLException("Unable to register " + driver + " JDBC driver", e);
						} catch (IllegalAccessException e) {
							throw new SQLException("Unable to register " + driver + " JDBC driver", e);
						}
					}
					
				} catch (ClassNotFoundException ex) {
					throw new SQLException("Unable to find the driver class " + driver, ex);
				}
				
				ComboPooledDataSource cpds = new ComboPooledDataSource();
				try {
					cpds.setDriverClass(driver);
				} catch (PropertyVetoException ex) {
					throw new SQLException("Unexpected error configuring pooled data source", ex);
				}
				
				Properties properties = new Properties();
				properties.setProperty("connectTimeout", "30");
				properties.setProperty("socketTimeout", "30");
				cpds.setProperties(properties);
				
				cpds.setMinPoolSize(3);
				cpds.setAcquireIncrement(3);
				cpds.setMaxPoolSize(50);
				
				cpds.setLoginTimeout(30);
				
				cpds.setMaxStatements(200);
				cpds.setMaxIdleTimeExcessConnections(15);
				cpds.setUnreturnedConnectionTimeout(3600);
				
				cpds.setJdbcUrl(element.getConnectionUrl());
				cpds.setUser(element.getUserName());
				cpds.setPassword(element.getPassword());
				
				/*
				DataSource upDs = DataSources.unpooledDataSource(element.getConnectionUrl(), element.getUserName(), element.getPassword());
				upDs.setLoginTimeout(60);
				
				Map overrides = new HashMap();
				// TODO: manage these properties
				overrides.put("maxStatements", 200);
				overrides.put("maxPoolSize", 50);
				overrides.put("maxIdleTimeExcessConnections", 15);
				overrides.put("unreturnedConnectionTimeout", 3600);
				overrides.put("connectTimeout", 60);
				
				ds = DataSources.pooledDataSource(upDs, overrides);
				ds.setLoginTimeout(60);
				*/
				
				ds = cpds;
				
				poolDataSources.put(element.getConnectionName(), (ComboPooledDataSource) ds);
				
				ds = DataSourceProxy.wrap(ds);
				
				dataSources.put(element.getConnectionName(), ds);
			}
		}
		
		return ds;
		
	}
	
	private Connection getConnection(PersistenceConfigurationManager.Element element) throws SQLException {
		
		if (element == null) {
			return null;
		}
		
		DataSource ds = getDataSource(element.getConnectionName());
		
		if (ds == null) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unable to get a usable connection. Please check the persistence configuration.");
			return null;
		}
		
		Connection res = ds.getConnection();
		
		if (res != null) {
			InstanceListener.get().getConnections().add(res);
		}
		
		return res;
		
	}
	
	protected void clearDataSources() {
		
		for (String k : dataSources.keySet()) {
			ComboPooledDataSource ds = poolDataSources.get(k);
			if (ds != null) {
				ds.close();
			}
		}
		
		dataSources.clear();
		poolDataSources.clear();
		
	}
	
	public Properties getConnectionStatistics(String connectionName) throws SQLException {
		
		ComboPooledDataSource cpds = poolDataSources.get(connectionName);
		
		if (cpds != null) {
			
			Properties res = new Properties();
			
			res.setProperty("DataSourceName", String.valueOf(cpds.getDataSourceName()));
			res.setProperty("LoginTimeout", String.valueOf(cpds.getLoginTimeout()));
			res.setProperty("NumBusyConnections", String.valueOf(cpds.getNumBusyConnections()));
			res.setProperty("NumBusyConnectionsAllUsers", String.valueOf(cpds.getNumBusyConnectionsAllUsers()));
			res.setProperty("NumBusyConnectionsDefaultUser", String.valueOf(cpds.getNumBusyConnectionsDefaultUser()));
			res.setProperty("NumConnections", String.valueOf(cpds.getNumConnections()));
			res.setProperty("NumConnectionsAllUsers", String.valueOf(cpds.getNumConnectionsAllUsers()));
			res.setProperty("NumConnectionsDefaultUser", String.valueOf(cpds.getNumConnectionsDefaultUser()));
			res.setProperty("NumFailedCheckinsDefaultUser", String.valueOf(cpds.getNumFailedCheckinsDefaultUser()));
			res.setProperty("NumFailedCheckoutsDefaultUser", String.valueOf(cpds.getNumFailedCheckoutsDefaultUser()));
			res.setProperty("NumFailedIdleTestsDefaultUser", String.valueOf(cpds.getNumFailedIdleTestsDefaultUser()));
			res.setProperty("NumHelperThreads", String.valueOf(cpds.getNumHelperThreads()));
			res.setProperty("NumIdleConnections", String.valueOf(cpds.getNumIdleConnections()));
			res.setProperty("NumIdleConnectionsAllUsers", String.valueOf(cpds.getNumIdleConnectionsAllUsers()));
			res.setProperty("NumIdleConnectionsDefaultUser", String.valueOf(cpds.getNumIdleConnectionsDefaultUser()));
			res.setProperty("NumThreadsAwaitingCheckoutDefaultUser", String.valueOf(cpds.getNumThreadsAwaitingCheckoutDefaultUser()));
			res.setProperty("NumUnclosedOrphanedConnections", String.valueOf(cpds.getNumUnclosedOrphanedConnections()));
			res.setProperty("NumUnclosedOrphanedConnectionsAllUsers", String.valueOf(cpds.getNumUnclosedOrphanedConnectionsAllUsers()));
			res.setProperty("NumUnclosedOrphanedConnectionsDefaultUser", String.valueOf(cpds.getNumUnclosedOrphanedConnectionsDefaultUser()));
			res.setProperty("NumUserPools", String.valueOf(cpds.getNumUserPools()));
			res.setProperty("StartTimeMillisDefaultUser", String.valueOf(cpds.getStartTimeMillisDefaultUser()));
			res.setProperty("StatementCacheNumCheckedOutDefaultUser", String.valueOf(cpds.getStatementCacheNumCheckedOutDefaultUser()));
			res.setProperty("StatementCacheNumCheckedOutStatementsAllUsers", String.valueOf(cpds.getStatementCacheNumCheckedOutStatementsAllUsers()));
			res.setProperty("StatementCacheNumConnectionsWithCachedStatementsAllUsers", String.valueOf(cpds.getStatementCacheNumConnectionsWithCachedStatementsAllUsers()));
			res.setProperty("StatementCacheNumConnectionsWithCachedStatementsDefaultUser", String.valueOf(cpds.getStatementCacheNumConnectionsWithCachedStatementsDefaultUser()));
			res.setProperty("StatementCacheNumStatementsAllUsers", String.valueOf(cpds.getStatementCacheNumStatementsAllUsers()));
			res.setProperty("StatementCacheNumStatementsDefaultUser", String.valueOf(cpds.getStatementCacheNumStatementsDefaultUser()));
			res.setProperty("StatementDestroyerNumActiveThreads", String.valueOf(cpds.getStatementDestroyerNumActiveThreads()));
			res.setProperty("StatementDestroyerNumConnectionsInUseAllUsers", String.valueOf(cpds.getStatementDestroyerNumConnectionsInUseAllUsers()));
			res.setProperty("StatementDestroyerNumConnectionsInUseDefaultUser", String.valueOf(cpds.getStatementDestroyerNumConnectionsInUseDefaultUser()));
			res.setProperty("StatementDestroyerNumConnectionsWithDeferredDestroyStatementsAllUsers", String.valueOf(cpds.getStatementDestroyerNumConnectionsWithDeferredDestroyStatementsAllUsers()));
			res.setProperty("StatementDestroyerNumConnectionsWithDeferredDestroyStatementsDefaultUser", String.valueOf(cpds.getStatementDestroyerNumConnectionsWithDeferredDestroyStatementsDefaultUser()));
			res.setProperty("StatementDestroyerNumDeferredDestroyStatementsAllUsers", String.valueOf(cpds.getStatementDestroyerNumDeferredDestroyStatementsAllUsers()));
			res.setProperty("StatementDestroyerNumDeferredDestroyStatementsDefaultUser", String.valueOf(cpds.getStatementDestroyerNumDeferredDestroyStatementsDefaultUser()));
			res.setProperty("StatementDestroyerNumIdleThreads", String.valueOf(cpds.getStatementDestroyerNumIdleThreads()));
			res.setProperty("StatementDestroyerNumTasksPending", String.valueOf(cpds.getStatementDestroyerNumTasksPending()));
			res.setProperty("StatementDestroyerNumThreads", String.valueOf(cpds.getStatementDestroyerNumThreads()));
			res.setProperty("ThreadPoolNumActiveThreads", String.valueOf(cpds.getThreadPoolNumActiveThreads()));
			res.setProperty("ThreadPoolNumIdleThreads", String.valueOf(cpds.getThreadPoolNumIdleThreads()));
			res.setProperty("ThreadPoolNumTasksPending", String.valueOf(cpds.getThreadPoolNumTasksPending()));
			res.setProperty("ThreadPoolSize", String.valueOf(cpds.getThreadPoolSize()));
			res.setProperty("UpTimeMillisDefaultUser", String.valueOf(cpds.getUpTimeMillisDefaultUser()));
			
			return res;
			
		}
		else {
			return new Properties();
		}
		
	}
	
}
