/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.operations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.annotations.SkipAuthorization;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.OperationContext;
import com.waveinformatica.ocean.core.controllers.events.LoginEvent;
import com.waveinformatica.ocean.core.controllers.events.LogoutEvent;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.RedirectResult;
import com.waveinformatica.ocean.core.controllers.results.WebPageResult;
import com.waveinformatica.ocean.core.exceptions.OperationNotFoundException;
import com.waveinformatica.ocean.core.security.UserSession;
import com.waveinformatica.ocean.core.util.LoggerUtils;

/**
 *
 * @author Ivano
 */
@OperationProvider(namespace = "/auth")
public class UserSessionOperations {

	private static final Logger logger = LoggerUtils.getCoreLogger();

	@Inject
	private ObjectFactory objectFactory;

	@Inject
	private UserSession userSession;

	@Inject
	private CoreController coreController;

	@SkipAuthorization
	@Operation("/loginPage")
	public BaseResult loginPage(HttpServletRequest request, LoginEvent loginEvent) {

		LoginData loginData = new LoginData();
		loginData.setFrom(request.getParameter("redirect"));

		if (userSession.isLogged()) {
			if (StringUtils.isNotBlank(loginData.getFrom())) {
				return new RedirectResult("~" + loginData.getFrom());
			} else {
				return new RedirectResult("~/");
			}
		}

		if (loginEvent != null) {
			loginData.getErrors().addAll(loginEvent.getErrors());
		}

		WebPageResult result = objectFactory.newInstance(WebPageResult.class);
		result.setProvider(this);
		result.setTemplate("/templates/login.tpl");
		result.setData(loginData);

		return result;

	}

	@SkipAuthorization
	@Operation("/login")
	public BaseResult login(@Param("userName") String userName,
		  @Param("password") String password, @Param("redirect") String redirect,
		  OperationContext opCtx, HttpServletRequest request, HttpServletResponse response) throws OperationNotFoundException {

		LoginEvent event = new LoginEvent(this, userName, password, request, response);
		coreController.dispatchEvent(event);

		if (event.isManaged() && event.getLoggedUser() != null) {

			userSession.login(event.getLoggedUser());
			
			if (StringUtils.isBlank(redirect)) {
				redirect = "";
			}
			if (!redirect.startsWith("/")) {
				redirect = "/" + redirect;
			}
			
			return new RedirectResult("~" + redirect, 303);
			
		} else {
			return (BaseResult) coreController.executeOperation("/auth/loginPage",
				  Collections.<String, String[]>emptyMap(), opCtx, request, event);
		}
	}

	@Operation("/logout")
	public RedirectResult logout(HttpServletRequest request, HttpServletResponse response) {

		userSession.logout();

		request.getSession().invalidate();

		LogoutEvent evt = new LogoutEvent(this, request, response);
		coreController.dispatchEvent(evt);
		
		return new RedirectResult("~/");
		
	}

	public static class LoginData {

		private String from;
		private final List<String> errors = new ArrayList<String>();

		public String getFrom() {
			return from;
		}

		public void setFrom(String from) {
			this.from = from;
		}

		public List<String> getErrors() {
			return errors;
		}

	}

}
