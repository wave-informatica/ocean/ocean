/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers;

import java.lang.reflect.Method;

/**
 * This class fills the gap between modules, core and Context Dependency
 * Injection, providing object instances through CDI in module classes too.
 *
 * @author Ivano
 */
public interface ObjectFactory {

	/**
	 * Returns a object instance of the specified type <code>T</code>
	 * injecting need beans.
	 *
	 * @param <T> the required type
	 * @param cls the required type class
	 * @return a filled instance of the required type
	 */
	public <T> T newInstance(Class<T> cls);

	/**
	 * FOR INTERNAL USE ONLY!
	 *
	 * @param name
	 */
	public void moduleUnloaded(String name);

	public Object getBean(Method m, int parameterPosition);

	public <T> T getBean(Class<T> beanClass);

}
