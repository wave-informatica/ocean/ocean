/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.persistence;

import com.waveinformatica.ocean.core.controllers.InstanceListener;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.util.logging.Level;
import javax.sql.DataSource;

/**
 *
 * @author Ivano
 */
public class DataSourceProxy implements InvocationHandler {
	
	public static DataSource wrap(DataSource ds) {
		
		DataSourceProxy proxy = new DataSourceProxy(ds);
		
		try {
			
			return (DataSource) Proxy.getProxyClass(DataSource.class.getClassLoader(), DataSource.class).
				  getConstructor(new Class[] { InvocationHandler.class }).
				  newInstance(new Object[] { proxy });
			
		} catch (Exception ex) {
			LoggerUtils.getLogger(DataSourceProxy.class).log(Level.WARNING, "Unable to proxy data source instance", ex);
			return ds;
		}
	}
	
	private final DataSource dataSource;

	private DataSourceProxy(DataSource ds) {
		dataSource = ds;
	}
	
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Object res = method.invoke(dataSource, args);
		if (res != null && res instanceof Connection) {
			InstanceListener.get().getConnections().add((Connection) res);
		}
		return res;
	}
	
}
