/*
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.cdi;

import java.io.Serializable;
import javax.enterprise.context.spi.Context;
import javax.enterprise.context.spi.Contextual;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;

import com.waveinformatica.ocean.core.cdi.GenericScopeContextHolder.GenericScopeInstance;

@SuppressWarnings({"unchecked", "rawtypes"})
public abstract class AbstractScopeContext implements Context, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public <T> T get(Contextual<T> contextual) {
		Bean<T> bean = (Bean) contextual;
		
		GenericScopeContextHolder contextHolder = getScopeContextHolder();
		
        if (contextHolder.getBeans().containsKey(bean.getBeanClass())) {
            return (T) contextHolder.getBean(bean.getBeanClass()).instance;
        } else {
            return null;
        }
	}

	@Override
	public <T> T get(Contextual<T> contextual, CreationalContext<T> creationalContext) {
		
		GenericScopeContextHolder contextHolder = getScopeContextHolder();
		
		Bean bean = (Bean) contextual;
        if (contextHolder.getBeans().containsKey(bean.getBeanClass())) {
            return (T) contextHolder.getBean(bean.getBeanClass()).instance;
        } else {
            T t = (T) bean.create(creationalContext);
            GenericScopeInstance customInstance = new GenericScopeInstance();
            customInstance.bean = bean;
            customInstance.ctx = creationalContext;
            customInstance.instance = t;
            contextHolder.putBean(customInstance);
            return t;
        }
	}
	
	protected abstract GenericScopeContextHolder getScopeContextHolder();

}
