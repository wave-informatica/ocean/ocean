/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.operations.dto;

import com.waveinformatica.ocean.core.security.OceanUser;

/**
 *
 * @author ivano
 */
public class ErrorMailModel {

	private OceanUser loggedUser;
	private String href;
	private String exception;
	private String method;
	private String requestInput;

	public OceanUser getLoggedUser() {
		return loggedUser;
	}

	public void setLoggedUser(OceanUser loggedUser) {
		this.loggedUser = loggedUser;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getRequestInput() {
		return requestInput;
	}

	public void setRequestInput(String requestInput) {
		this.requestInput = requestInput;
	}

}
