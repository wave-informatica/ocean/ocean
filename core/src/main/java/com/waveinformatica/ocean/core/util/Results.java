/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.util;

import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
public class Results {
	
	public static void addScriptSource(BaseResult result, String url) {
		HeadPartWrapper part = new HeadPartWrapper();
		part.setType(HeadPartType.SCRIPT_SOURCE);
		part.setContent(url);
		getHeadContainer(result).addUnique(part);
	}
	
	public static void addScript(BaseResult result, String content) {
		HeadPartWrapper part = new HeadPartWrapper();
		part.setType(HeadPartType.SCRIPT);
		part.setContent(content);
		getHeadContainer(result).add(part);
	}
	
	public static void addCssSource(BaseResult result, String url) {
		HeadPartWrapper part = new HeadPartWrapper();
		part.setType(HeadPartType.CSS_SOURCE);
		part.setContent(url);
		getHeadContainer(result).addUnique(part);
	}
	
	public static void addCss(BaseResult result, String content) {
		HeadPartWrapper part = new HeadPartWrapper();
		part.setType(HeadPartType.CSS);
		part.setContent(content);
		getHeadContainer(result).add(part);
	}
	
	public static void copyHeadResources(BaseResult from, BaseResult to) {
		to.getExtras().remove(HeadWrappersContainer.class.getName());
		getHeadContainer(to).parts.addAll(getHeadContainer(from).parts);
	}
	
	public static HeadWrappersContainer getHeadContainer(BaseResult result) {
		
		String key = HeadWrappersContainer.class.getName();
		
		HeadWrappersContainer container = (HeadWrappersContainer) result.getExtra(key);
		
		if (container == null) {
			container = new HeadWrappersContainer();
			result.putExtra(key, container);
		}
		
		return container;
	}
	
	public static class HeadWrappersContainer {
		
		private final List<HeadPartWrapper> parts = new ArrayList<HeadPartWrapper>();

		public List<HeadPartWrapper> getParts() {
			return parts;
		}
		
		public void add(HeadPartWrapper part) {
			if (!normalizePart(part)) {
				return;
			}
			for (HeadPartWrapper p : parts) {
				if (p.getType() == part.getType() && p.getContent().equals(part.getContent()) && p.isUnique()) {
					return;
				}
			}
			parts.add(part);
		}
		
		public void addUnique(HeadPartWrapper part) {
			if (!normalizePart(part)) {
				return;
			}
			for (HeadPartWrapper p : parts) {
				if (p.getType() == part.getType() && p.getContent().equals(part.getContent())) {
					if (!p.isUnique()) {
						p.setUnique(true);
					}
					return;
				}
			}
			parts.add(part);
		}
		
		private boolean normalizePart(HeadPartWrapper part) {
			if (part == null || StringUtils.isBlank(part.getContent()) || part.getType() == null) {
				return false;
			}
			part.setContent(part.getContent().trim());
			return true;
		}
		
	}
	
	public static class HeadPartWrapper {
		
		private HeadPartType type;
		private String content;
		private boolean unique = false;

		public HeadPartType getType() {
			return type;
		}

		public void setType(HeadPartType type) {
			this.type = type;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		public boolean isUnique() {
			return unique;
		}

		public void setUnique(boolean unique) {
			this.unique = unique;
		}
		
	}
	
	public static enum HeadPartType {
		SCRIPT_SOURCE,
		SCRIPT,
		CSS_SOURCE,
		CSS
	}
	
}
