/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.annotations;

import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.inject.Qualifier;

/**
 * This annotation is a CDI qualifier for <code>@Inject</code>ed <code>EntityManager</code>s.
 * This is not required, but preferred to obtain the right <code>EntityManager</code> instance,
 * if the default {@link ObjectFactory}'s instance is used. It may be required in the future.
 *
 * @author Ivano
 */
@Qualifier
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface OceanPersistenceContext {
	
}
