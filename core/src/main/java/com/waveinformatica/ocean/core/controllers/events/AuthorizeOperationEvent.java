/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.events;

import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.controllers.scopes.ScopeChain;


public class AuthorizeOperationEvent extends Event {
	
	private final OperationInfo operationInfo;
	private final ScopeChain chain;
	private Boolean authorized;
	
	public AuthorizeOperationEvent(Object source, OperationInfo operationInfo, ScopeChain chain) {
		super(source, CoreEventName.AUTHORIZE_OPERATION.name());
		this.operationInfo = operationInfo;
		this.chain = chain;
	}

	public Boolean getAuthorized() {
		return authorized;
	}

	public void setAuthorized(Boolean authorized) {
		this.authorized = authorized;
	}

	public OperationInfo getOperationInfo() {
		return operationInfo;
	}

	public ScopeChain getChain() {
		return chain;
	}
	
}
