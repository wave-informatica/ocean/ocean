/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.controllers.results.input;

import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.controllers.CoreController.ParameterInfo;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.controllers.results.InputResult.Field;
import com.waveinformatica.ocean.core.controllers.results.InputResult.FieldHandler;
import com.waveinformatica.ocean.core.controllers.results.InputResult.FormField;
import com.waveinformatica.ocean.core.util.I18N;

public class OperationFieldHandler extends NestedFieldHandler {
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private I18N i18n;
	
	private OperationInfo operation;
	
	@Override
	public void init(InputResult result, OperationInfo opInfo, ParameterInfo paramInfo, FormField field) {
		
		super.init(result, opInfo, paramInfo, field);
		
		if (operation == null) {
			return;
		}
		
		initFieldsFromOperation(result, field);
		
	}
	
	@Override
	public String formatValue(InputResult result, FormField field, Object data) {
		
		initFieldsFromOperation(result, field);
		
		List<FormField> fields = getFields();
		for (FormField ff : fields) {
			if (data instanceof Map) {
				Map map = (Map) data;
				Object val = map.get(ff.getAttribute());
				if (val == null) {
					continue;
				}
				if (val instanceof String[] && ((String[]) val).length > 0) {
					val = ((String[]) val)[0];
				}
				String value = ff.getHandler().formatValue(result, ff, val);
				if (value != null) {
					ff.setValue(value);
				}
			}
			else {
				String value = null;
				Class cls = data.getClass();
				while (cls != Object.class && value == null) {
					try {
						java.lang.reflect.Field lField = cls.getDeclaredField(ff.getAttribute());
						lField.setAccessible(true);
						Object v = lField.get(data);
						lField.setAccessible(false);
						if (v != null) {
							if (v instanceof Collection) {
								value = null;
								for (Object o : (Collection) v) {
									if (o != null) {
										String formattedValue = ff.getHandler().formatValue(result, ff, o);
										if (formattedValue != null) {
											ff.addValue(formattedValue);
										}
									}
								}
							} else {
								value = ff.getHandler().formatValue(result, ff, v);
							}
						}
						break;
					} catch (NoSuchFieldException ex) {
						cls = cls.getSuperclass();
					} catch (SecurityException ex) {
						Logger.getLogger(InputResult.class.getName()).log(Level.SEVERE, null, ex);
					} catch (IllegalArgumentException ex) {
						Logger.getLogger(InputResult.class.getName()).log(Level.SEVERE, null, ex);
					} catch (IllegalAccessException ex) {
						Logger.getLogger(InputResult.class.getName()).log(Level.SEVERE, null, ex);
					}
				}

				if (value != null) {
					ff.setValue(value);
				}
			}
		}
		
		// TODO Auto-generated method stub
		return null;
	}

	public OperationInfo getOperation() {
		return operation;
	}

	public void setOperation(OperationInfo operation) {
		this.operation = operation;
	}
	
	private void initFieldsFromOperation(InputResult result, FormField me) {
		
		if (!getFields().isEmpty()) {
			return;
		}
		
		for (CoreController.ParameterInfo p : operation.getParameters()) {
			if (p.getName() == null) {
				continue;
			}
			FormField field = new FormField();
			if (p.hasAnnotation(Field.class)) {
				Field f = p.getAnnotation(Field.class);
				if (f.ignore()) {
					continue;
				}
				if (f.handler() == FieldHandler.class) {
					throw new IllegalArgumentException("The field handler for parameter \"" + p.getName() + "\" in operation \"" + operation.getFullName() + "\" must be a class implementing " + FieldHandler.class.getName() + " interface. Not the interface itself.");
				} else if (Modifier.isAbstract(f.handler().getModifiers())) {
					throw new IllegalArgumentException("The field handler for parameter \"" + p.getName() + "\" in operation \"" + operation.getFullName() + "\" must NOT be abstract ");
				}
				field.setHandler(factory.newInstance(f.handler()));
				field.setRequired(f.required());
				field.setReadOnly(f.readOnly());
				if (f.attribute().equals("")) {
					field.setAttribute(p.getName());
				} else {
					field.setAttribute(f.attribute());
				}
				field.setValue("");
				field.setDefaultValue(p.getDefaultValue());
			} else {
				field.setHandler(factory.newInstance(AutoFieldHandler.class));
				field.setAttribute(p.getName());
				field.setDefaultValue(p.getDefaultValue());
			}
			field.setParamInfo(p);
			field.setLabel(i18n.translate(operation.getCls().getName() + "." + operation.getMethod().getName() + "." + p.getName()));
			field.setName(me.getName() + "." + p.getName());
			field.setType(p.getParamClass());
			
			addField(field);
		}
		
		for (FormField f : getFields()) {
			if (f.getHandler() != null) {
				f.getHandler().init(result, operation, f.getParamInfo(), f);
			}
		}
		
	}
	
}
