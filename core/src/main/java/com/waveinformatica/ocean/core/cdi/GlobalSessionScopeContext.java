/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.cdi;

import java.lang.annotation.Annotation;
import javax.servlet.http.HttpSession;


public class GlobalSessionScopeContext extends AbstractScopeContext {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final ThreadLocal<CDISession> session = new ThreadLocal<CDISession>();

	public GlobalSessionScopeContext() {
		super();
		CDIHelper.setGlobalSessionScopeContext(this);
	}

	@Override
	public Class<? extends Annotation> getScope() {
		return GlobalSessionScoped.class;
	}

	@Override
	public boolean isActive() {
		return session.get() != null;
	}

	@Override
	protected GenericScopeContextHolder getScopeContextHolder() {
		return session.get().getContextHolder();
	}
	
	protected CDISession init(HttpSession httpSession) {
		
		if (httpSession == null) {
			GlobalSessionScopeContext.session.set(null);
			return null;
		}
		
		CDISession cdiSession = null;
		try {
			cdiSession = (CDISession) httpSession.getAttribute(this.getClass().getName() + ".CDISession");
		} catch (IllegalStateException e) {
			// Session not found?
			httpSession = null;
		}
		if (cdiSession == null) {
			cdiSession = new CDISession();
			if (httpSession != null) {
				httpSession.setAttribute(this.getClass().getName() + ".CDISession", cdiSession);
			}
		}
		
		GlobalSessionScopeContext.session.set(cdiSession);
		
		return cdiSession;
	}
	
	protected CDISession init(CDISession cdiSession) {
		
		GlobalSessionScopeContext.session.set(cdiSession);
		
		return cdiSession;
		
	}
	
	protected void close() {
		session.set(null);
	}
	
	public static CDISession getCDISession(HttpSession session) {
		try {
			if (session == null) {
				return new CDISession();
			}
			return (CDISession) session.getAttribute(GlobalSessionScopeContext.class.getName() + ".CDISession");
		} catch (IllegalStateException e) {
			return new CDISession();
		}
	}

}
