/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.html;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
public class CSSWeightedRule implements Comparable<CSSWeightedRule> {
	
	private static final Pattern selExtrPattern = Pattern.compile("([#\\.:]|::)?[a-z0-9_-]+", Pattern.CASE_INSENSITIVE);
	
	private final int order;
	private final String selector;
	private final String name;
	private final String value;
	private final int[] weights = new int[]{0, 0, 0, 0, 0};

	public static Pattern getSelExtrPattern() {
		return selExtrPattern;
	}

	public CSSWeightedRule(int order, String selector, CSSProperty property) {
		this.order = order;
		this.selector = selector;
		this.name = property.getName();
		this.value = StringUtils.join(property.getValues(), ",");
		weights[0] = property.isImportant() ? 1 : 0; // !important
		weights[1] = StringUtils.isBlank(selector) ? 1 : 0; // style attribute of element
		if (StringUtils.isNotBlank(selector)) {
			Matcher matcher = selExtrPattern.matcher(selector);
			while (matcher.find()) {
				String p = matcher.group();
				if (p.startsWith("#")) {
					weights[2]++;
				} else if (p.startsWith(":")) {
					if (p.equals(":not")) {
						// No specificity
						continue;
					}
					String pn = p.substring(1);
					if (pn.startsWith(":")) {
						pn = pn.substring(1);
					}
					if (pn.equals("first-line") || pn.equals("first-letter") || pn.equals("before") || pn.equals("after") || pn.equals("selection")) {
						weights[4]++; // Elements
					} else if (pn.equals("active") || pn.equals("checked") || pn.equals("disabled") || pn.equals("empty") || pn.equals("enabled") || pn.equals("first-child") || pn.equals("first-of-type") || pn.equals("focus") || pn.equals("hover") || pn.equals("in-range") || pn.equals("invalid") || pn.equals("lang") || pn.equals("last-child") || pn.equals("last-of-type") || pn.equals("link") || pn.equals("nth-child") || pn.equals("nth-last-child") || pn.equals("nth-last-of-type") || pn.equals("nth-of-type") || pn.equals("only-of-type") || pn.equals("only-child") || pn.equals("optional") || pn.equals("out-of-range") || pn.equals("read-only") || pn.equals("read-write") || pn.equals("required") || pn.equals("root") || pn.equals("target") || pn.equals("valid") || pn.equals("visited")) {
						weights[3]++; // Classes
					}
				} else if (p.startsWith(".")) {
					weights[3]++; // Classes
				}
			}
		}
	}

	public int getOrder() {
		return order;
	}

	public String getSelector() {
		return selector;
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	public boolean isImportant() {
		return weights[0] == 1;
	}
	
	public void setStyleAttribute(boolean styleAttribute) {
		weights[1] = styleAttribute ? 1 : 0;
	}
	
	public boolean isStyleAttribute() {
		return weights[1] == 1;
	}

	@Override
	public int compareTo(CSSWeightedRule o) {
		int res = name.compareTo(o.name);
		if (res != 0) {
			return res;
		}
		for (int i = 0; i < weights.length; i++) {
			if (weights[i] != o.weights[i]) {
				return weights[i] - o.weights[i];
			}
		}
		return order - o.order;
	}
	
}
