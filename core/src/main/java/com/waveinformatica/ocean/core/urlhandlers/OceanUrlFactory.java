package com.waveinformatica.ocean.core.urlhandlers;

import java.net.URL;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public interface OceanUrlFactory {
	
	public URL createResourceUrl(JarFile file, JarEntry entry);
	
}
