/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.controllers.scopes;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.ExcludeSerialization;
import com.waveinformatica.ocean.core.annotations.ScopeProvider;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.NavigationLink;
import com.waveinformatica.ocean.core.controllers.results.MenuResult;
import com.waveinformatica.ocean.core.controllers.results.MenuResult.MenuSection;
import com.waveinformatica.ocean.core.security.SecurityManager;
import com.waveinformatica.ocean.core.security.UserSession;

@ScopeProvider(inScope = RootScope.class, startsWith = "/admin/")
public class AdminScopeProvider implements IScopeProvider {

	@Inject
	private ObjectFactory factory;
	
	@Override
	public String produce(ScopeChain chain, String path) {
		
		if (chain.getCurrent().getClass() != RootScope.class) {
			return null;
		}
		
		AdminRootScope scope = factory.newInstance(AdminRootScope.class);
		
		chain.add(scope);
		
		return path;
		
	}
	
	public static class AdminRootScope extends AbstractScope implements TemplateScope, NavigationScope {

		@Inject
		@ExcludeSerialization
		private ObjectFactory factory;
		
		@Inject
		@ExcludeSerialization
		private UserSession userSession;
		
		@Inject
		@ExcludeSerialization
		private SecurityManager securityManager;
		
		@Inject
		@ExcludeSerialization
		private Configuration configuration;
		
		private String scopeName = "";
		List<MenuSection> sections;
		
		public String getScopeName() {
			return scopeName;
		}

		public void setScopeName(String name) {
			this.scopeName = name;
		}
		
		public List<MenuSection> getSections() {
			if (sections == null) {
				MenuResult result = factory.newInstance(MenuResult.class);
				result.initFromScope(this);
				sections = result.getSections();
			}
			return sections;
		}

		@Override
		public String getHeaderTemplate() {
			return "/templates/includes/header-admin.tpl";
		}
		
		@Override
		public String getFooterTemplate() {
			return "/templates/includes/footer-admin.tpl";
		}
		
		@Override
		public AbstractScope getParent() {
			return null;
		}

		public UserSession getUserSession() {
			return userSession;
		}

		public SecurityManager getSecurityManager() {
			return securityManager;
		}

		public Configuration getConfiguration() {
			return configuration;
		}
		
		public String getContextPath() {
			return getChain().getParent(RootScope.class, this).getContextPath();
		}

		@Override
		public List<NavigationLink> getNavigationLinks() {
			List<NavigationLink> links = new ArrayList<>();
			links.add(new NavigationLink("admin/", "administration"));
			return links;
		}
		
		public String getBasePath() {
			return "admin/";
		}

	}

}
