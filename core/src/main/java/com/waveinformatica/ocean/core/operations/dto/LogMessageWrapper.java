/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.operations.dto;

import com.waveinformatica.ocean.core.controllers.results.MessageResult;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 *
 * @author Ivano
 */
public class LogMessageWrapper {
    
    private SimpleDateFormat dateFmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    
    private MessageResult.MessageType level;
    private String message;
    private Date time;
    private Throwable throwable;

    public LogMessageWrapper(LogRecord record) {
        
        time = new Date(record.getMillis());
        message = record.getMessage();
        throwable = record.getThrown();
        
        if (record.getLevel().intValue() <= Level.INFO.intValue()) {
            level = MessageResult.MessageType.INFO;
        }
        else if (record.getLevel().intValue() <= Level.WARNING.intValue()) {
            level = MessageResult.MessageType.WARNING;
        }
        else if (record.getLevel().intValue() <= Level.SEVERE.intValue()) {
            level = MessageResult.MessageType.CRITICAL;
        }
        
    }

    public MessageResult.MessageType getLevel() {
        return level;
    }

    public void setLevel(MessageResult.MessageType level) {
        this.level = level;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }
    
    public String getFormattedDate() {
        return dateFmt.format(time);
    }
    
    public boolean isException() {
        return throwable != null;
    }
    
    public String getFormattedException() {
        StringWriter strWr = new StringWriter();
        PrintWriter writer = new PrintWriter(strWr);
        throwable.printStackTrace(writer);
        return strWr.toString();
    }
    
}
