/*
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.cdi;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.enterprise.inject.spi.AnnotatedMethod;
import javax.enterprise.inject.spi.AnnotatedParameter;
import javax.enterprise.inject.spi.AnnotatedType;

/**
 *
 * @author Ivano
 */
public class OceanAnnotatedMethod<T> implements AnnotatedMethod<T> {
    
    private final OceanAnnotatedType<T> type;
    private final Method method;
    private final List<AnnotatedParameter<T>> parameters = new ArrayList<AnnotatedParameter<T>>();

    public OceanAnnotatedMethod(OceanAnnotatedType<T> type, Method method) {
        this.type = type;
        this.method = method;
        
        for (int i = 0; i < method.getParameterTypes().length; i++) {
            parameters.add(new OceanAnnotatedParameter<T>(this, i, (Class<T>) method.getParameterTypes()[i]));
        }
    }
    
    @Override
    public Method getJavaMember() {
        return method;
    }

    @Override
    public List<AnnotatedParameter<T>> getParameters() {
        return parameters;
    }

    @Override
    public boolean isStatic() {
        return (method.getModifiers() & Modifier.STATIC) != 0;
    }

    @Override
    public AnnotatedType<T> getDeclaringType() {
        return type;
    }

    @Override
    public Type getBaseType() {
        return type.getBaseType();
    }

    @Override
    public Set<Type> getTypeClosure() {
        return type.getTypeClosure();
    }

    @Override
    public <T extends Annotation> T getAnnotation(Class<T> annotationType) {
        return method.getAnnotation(annotationType);
    }

    @Override
    public Set<Annotation> getAnnotations() {
        Set<Annotation> annotations = new HashSet<Annotation>();
        for (Annotation a : method.getAnnotations()) {
            annotations.add(a);
        }
        return annotations;
    }

    @Override
    public boolean isAnnotationPresent(Class<? extends Annotation> annotationType) {
        return method.isAnnotationPresent(annotationType);
    }
    
}
