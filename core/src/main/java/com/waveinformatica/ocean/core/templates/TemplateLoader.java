/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */
package com.waveinformatica.ocean.core.templates;

import com.waveinformatica.ocean.core.Constants;
import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.annotations.ManagedCache;
import com.waveinformatica.ocean.core.cache.OceanCache;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.waveinformatica.ocean.core.controllers.dto.ResourceInfo;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.ModuleEvent;
import com.waveinformatica.ocean.core.modules.ModuleClassLoader;
import com.waveinformatica.ocean.core.util.GlobalClassLoader;
import java.io.Serializable;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.Iterator;

/**
 *
 * @author Ivano
 */
@Named
@ApplicationScoped
public class TemplateLoader implements freemarker.cache.TemplateLoader {

	@Inject
	private GlobalClassLoader globalClassLoader;

	@Inject
	@ManagedCache(value = "Templates Cache", register = true)
	private OceanCache templatesCache;

	@Override
	public Object findTemplateSource(String string) throws IOException {

		TemplateInfo ti = (TemplateInfo) templatesCache.get(string);
		if (ti != null) {
			return ti;
		}

		ResourceInfo res = globalClassLoader.getResourceInfo(string, true);

		if (res != null) {
			ti = new TemplateInfo();
			ti.setModuleName(res.getSourceClassLoader() instanceof ModuleClassLoader ? ((ModuleClassLoader) res.getSourceClassLoader()).getModule().getName() : Constants.CORE_MODULE_NAME);
			ti.setLastModified(res.getLastModified().getTime());
			StringBuilder builder = new StringBuilder();
			char[] buff = new char[4096];
			int read;
			Reader reader = new InputStreamReader(res.getInputStream(), Charset.forName("UTF-8"));
			while ((read = reader.read(buff)) > 0) {
				builder.append(buff, 0, read);
			}
			reader.close();
			res.getInputStream().close();
			ti.setContent(builder.toString());
			templatesCache.put(string, ti);
		}

		return ti;

	}

	@Override
	public long getLastModified(Object o) {
		TemplateInfo ri = (TemplateInfo) o;
		return ri.getLastModified();
	}

	@Override
	public Reader getReader(Object o, String string) throws IOException {
		return new StringReader(((TemplateInfo) o).getContent());
	}

	@Override
	public void closeTemplateSource(Object o) throws IOException {

	}
	
	protected void clearCache() {
		templatesCache.clear();
	}
	
	protected void clearCacheByModule(String name) {
		Iterator<TemplateInfo> tIter = templatesCache.values().iterator();
		while (tIter.hasNext()) {
			TemplateInfo ti = tIter.next();
			if (ti.getModuleName().equals(name)) {
				tIter.remove();
			}
		}
	}

	private static class TemplateInfo implements Serializable {

		private String moduleName;
		private long lastModified;
		private String content;

		public String getModuleName() {
			return moduleName;
		}

		public void setModuleName(String moduleName) {
			this.moduleName = moduleName;
		}

		public long getLastModified() {
			return lastModified;
		}

		public void setLastModified(long lastModified) {
			this.lastModified = lastModified;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

	}

	@CoreEventListener(eventNames = { CoreEventName.MODULE_UNLOADED, CoreEventName.MODULE_LOADED })
	public static class TemplatesCleaner implements IEventListener<ModuleEvent> {

		@Inject
		private TemplateLoader loader;

		@Override
		public void performAction(ModuleEvent event) {
			
			if (loader == null) {
				return;
			}
			
			if (event.isLoaded()) {
				loader.clearCache();
			}
			else {
				loader.clearCacheByModule(event.getModuleName());
			}

		}

	}

}
