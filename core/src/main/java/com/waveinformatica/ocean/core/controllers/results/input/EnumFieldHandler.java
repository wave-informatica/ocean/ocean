/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.controllers.results.input;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.util.I18N;
import java.lang.reflect.ParameterizedType;
import javax.inject.Inject;

/**
 *
 * @author Ivano
 */
public class EnumFieldHandler extends ComboFieldHandler {
	
	@Inject
	private I18N i18n;
	
	@Override
	public void init(InputResult result, CoreController.OperationInfo opInfo, CoreController.ParameterInfo paramInfo, InputResult.FormField field) {
		
		getItems().clear();
		
		Object[] values = paramInfo.getParamClass().getEnumConstants();
		if (values == null && paramInfo.getGenericType() != null) {
			ParameterizedType type = (ParameterizedType) paramInfo.getGenericType();
			Class cls = (Class) type.getActualTypeArguments()[0];
			values = cls.getEnumConstants();
		}
		if (values != null) {
			for (Object v : values) {
				addOption(v.toString(), i18n.translate(paramInfo.getParamClass().getName() + "." + v.toString()));
			}
		}
	}
	
}
