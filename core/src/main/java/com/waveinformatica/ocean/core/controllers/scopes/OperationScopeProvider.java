/*
 * Copyright 2017, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.scopes;

import com.waveinformatica.ocean.core.annotations.ScopeProvider;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import javax.inject.Inject;

/**
 *
 * @author ivano
 */
@ScopeProvider(inScope = AbstractScope.class, order = -1)
public class OperationScopeProvider implements IScopeProvider {

	@Inject
	private CoreController controller;
	
	@Inject
	private ObjectFactory factory;
	
	@Override
	public String produce(ScopeChain chain, String path) {
		
		if (!path.startsWith("/")) {
			path = "/" + path;
		}
		
		OperationInfo opInfo = controller.getOperationInfo(chain.getCurrent().getClass(), path);
		
		if (opInfo == null) {
			return null;
		}
		
		if (opInfo != null) {
			OperationScope scope = factory.newInstance(OperationScope.class);
			scope.setOperationInfo(opInfo);
			chain.add(scope);
		}
		
		return "$";
		
	}
	
}
