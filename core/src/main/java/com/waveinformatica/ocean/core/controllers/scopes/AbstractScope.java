/*
 * Copyright 2017, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.scopes;

import com.waveinformatica.ocean.core.annotations.ExcludeSerialization;

/**
 * This is the base class for all scopes. This class can be used as filter scope
 * to attach a scope provider or an operation to any scope.
 * 
 * @author ivano
 * @since 2.2.0
 */
public abstract class AbstractScope {
	
	@ExcludeSerialization
	private ScopeChain chain;
	
	/**
	 * This method should be overridden by all sub classes to implement authorization
	 * behaviour. Each implementing class SHOULD always call the super method before
	 * its own implementation and take care of its result.
	 * 
	 * @return <code>true</code> if the scope is authorized in the current request
	 */
	public boolean isAuthorized() {
		
		AbstractScope parent = chain.getParent(this);
		
		return parent == null || parent.isAuthorized();
		
	}

	/**
	 * Allows to retrieve the {@link ScopeChain} the current scope is in.
	 * 
	 * @return the current scope's {@link ScopeChain}
	 */
	public ScopeChain getChain() {
		return chain;
	}
	
	/**
	 * This method returns the scope's path.
	 * 
	 * @return the scope's path
	 */
	public final String getPath() {
		
		AbstractScope parent = chain.getParent(this);
		
		if (parent != null) {
			return parent.getPath() + "\\" + getName();
		}
		else {
			return getName();
		}
		
	}
	
	/**
	 * This method returns the parent scope of this one or <code>null</code> if this is the
	 * first one.
	 * 
	 * @return the parent scope or <code>null</code> if none
	 */
	public AbstractScope getParent() {
		return chain.getParent(this);
	}
	
	/**
	 * This method returns the scope's name. This method is invoked by the
	 * {@link AbstractScope#getPath() getPath()} method and should be overridden by
	 * all the implementing classes to provide the specific scope's name. By default
	 * this method returns the class's simple name.
	 * 
	 * @return the scope name
	 */
	protected String getName() {
		return this.getClass().getSimpleName();
	}
	
	void complete(ScopeChain chain) {
		this.chain = chain;
	}

}
