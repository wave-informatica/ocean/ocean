/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.annotations;

import com.waveinformatica.ocean.core.controllers.scopes.RootScope;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>This annotation defines classes providing operations to the application.</p>
 * 
 * <p>For instance a class providing operations will look like the following:</p>
 * 
 * <p><pre class="language-java"><code class="language-java"> {@literal @}OperationProvider
 * public class MyOperations {
 *   
 *   {@literal @}Inject
 *   private ObjectFactory factory;
 *   
 *   {@literal @}Operation("myOperation")
 *   public WebPageResult myOperation ({@literal @}Param("web-param") String webParam) {
 *       
 *     WebPageResult result = factory.newInstance(WebPageResult.class);
 *     
 *     // ... operation's code
 *     
 *     return result;
 *   }
 * 
 * }</code></pre>
 * 
 * <p>The operation <code>myOperation</code> in this operations provider will be callable with the URL http://example.com/context_path/myOperation.
 * The <code>webParam</code> parameter will be filled reading the "web-param" request parameter.
 * 
 * <p>You can set a namespace too and it will be used as path between the context path and the operation name.
 * 
 * @author Ivano
 * @see Operation
 */
@OceanComponent
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface OperationProvider {
    
    /**
     * The operations base namespace, used for building URIs. Defaults to the root of the web application.
     * 
     * @return the operations base namespace
     */
    String namespace() default "/";
    
    Class<?>[] inScope() default RootScope.class;
    
}
