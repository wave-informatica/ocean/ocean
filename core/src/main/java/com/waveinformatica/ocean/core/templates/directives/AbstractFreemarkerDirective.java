/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.templates.directives;

import com.waveinformatica.ocean.core.templates.TemplatesManager;
import com.waveinformatica.ocean.core.util.LoggerUtils;

import freemarker.core.Environment;
import freemarker.template.Template;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;

/**
 *
 * @author Ivano
 */
public abstract class AbstractFreemarkerDirective implements TemplateDirectiveModel {
	
	@Inject
	private TemplatesManager templatesManager;
	
	private Field rootDataModel = null;
	
	protected void include(String template, Environment e, Map map, TemplateModel[] tms, TemplateDirectiveBody tdb) {
		
		Template tpl = templatesManager.getTemplate(template);
		
		try {
			
			e.include(tpl);
			
		} catch (TemplateException e1) {
			LoggerUtils.getLogger(this.getClass()).log(Level.SEVERE, "Unexpected error including template " + template, e1);
		} catch (IOException e1) {
			LoggerUtils.getLogger(this.getClass()).log(Level.SEVERE, "Unexpected error including template " + template, e1);
		}
		
	}
	
	protected Object getRootDataModel(Environment e) {
		
		try {
			
			if (rootDataModel == null) {
				rootDataModel = e.getClass().getDeclaredField("rootDataModel");
				rootDataModel.setAccessible(true);
			}
			return rootDataModel.get(e);
			
		} catch (NoSuchFieldException ex) {
			Logger.getLogger(HtmlHeaderDirective.class.getName()).log(Level.SEVERE, null, ex);
		} catch (SecurityException ex) {
			Logger.getLogger(HtmlHeaderDirective.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalArgumentException ex) {
			Logger.getLogger(HtmlHeaderDirective.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(HtmlHeaderDirective.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		return null;
		
	}
	
}
