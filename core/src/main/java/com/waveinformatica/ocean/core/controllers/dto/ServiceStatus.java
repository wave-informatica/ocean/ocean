/*
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.dto;

import com.waveinformatica.ocean.core.annotations.Service;

/**
 *
 * @author Ivano
 */
public class ServiceStatus {
    
    private String name;
    private boolean running;
    private Service annotation;
    private String manageOperation;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public Service getAnnotation() {
        return annotation;
    }

    public void setAnnotation(Service annotation) {
        this.annotation = annotation;
    }

    public String getManageOperation() {
        return manageOperation;
    }

    public void setManageOperation(String manageOperation) {
        this.manageOperation = manageOperation;
    }
    
}
