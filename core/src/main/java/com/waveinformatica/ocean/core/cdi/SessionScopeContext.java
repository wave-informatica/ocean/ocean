/*
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.cdi;

import java.lang.annotation.Annotation;

import javax.enterprise.context.SessionScoped;

public class SessionScopeContext extends AbstractScopeContext {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static ThreadLocal<CDISession> session = new ThreadLocal<CDISession>();

	public SessionScopeContext() {
		super();
		CDIHelper.setSessionScopeContext(this);
	}

	@Override
	public Class<? extends Annotation> getScope() {
		return SessionScoped.class;
	}

	@Override
	public boolean isActive() {
		return session.get() != null;
	}

	@Override
	protected GenericScopeContextHolder getScopeContextHolder() {
		return session.get().getContextHolder();
	}
	
	protected void init(CDISession session) {
		SessionScopeContext.session.set(session);
	}
	
	protected void close() {
		session.set(null);
	}

}
