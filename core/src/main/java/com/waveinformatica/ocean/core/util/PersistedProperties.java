/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;

/**
 *
 * @author Ivano
 */
public class PersistedProperties extends Properties {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Date lastLoad;
	private final File persistedFile;
	
	public PersistedProperties(File persistedFile) {
		this.persistedFile = persistedFile;
	}
	
	public synchronized void load() {
		try {
			clear();
			FileInputStream fis = new FileInputStream(persistedFile);
			super.load(fis);
			fis.close();
			lastLoad = new Date();
		} catch (IOException ex) {
			LoggerUtils.getCoreLogger().log(Level.WARNING, "Unable to load PersistedProperties from \"" + persistedFile.getAbsolutePath() + "\"");
		}
	}
	
	public void store(String comments) throws IOException {
		OutputStream fos = new FileOutputStream(persistedFile);
		super.store(fos, comments); //To change body of generated methods, choose Tools | Templates.
		fos.close();
	}
	
	public void reloadIfModified() {
		if (lastLoad == null || (persistedFile.lastModified() > lastLoad.getTime())) {
			load();
		}
	}
	
}
