/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.cdi;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.spi.Context;
import javax.enterprise.context.spi.Contextual;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;

import org.jboss.weld.bean.builtin.BeanManagerProxy;
import org.jboss.weld.context.ConversationContext;
import org.jboss.weld.context.ManagedConversation;
import org.jboss.weld.context.PassivatingContextWrapper;
import org.jboss.weld.context.conversation.ConversationImpl;

public class ConversationScopeContext extends AbstractScopeContext implements ConversationContext {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static ThreadLocal<GenericScopeContextHolder> contextHolder = new ThreadLocal<GenericScopeContextHolder>();
	private static ThreadLocal<ManagedConversation> conversationHolder = new ThreadLocal<>();
	private static ThreadLocal<String> conversationIdHolder = new ThreadLocal<>();
	
	@Inject
	private BeanManager manager;

	public ConversationScopeContext() {
		super();
		CDIHelper.setConversationScopeContext(this);
	}
	
	@PostConstruct
	public void postLoad() {
		Context context = PassivatingContextWrapper.unwrap(manager.getContext(getScope()));
		if (context instanceof ConversationScopeContext) {
			ConversationScopeContext csc = (ConversationScopeContext) context;
			csc.manager = manager;
		}
	}

	@Override
	public Class<? extends Annotation> getScope() {
		return ConversationScoped.class;
	}

	@Override
	public boolean isActive() {
		return (com.waveinformatica.ocean.core.util.Context.get() == null || com.waveinformatica.ocean.core.util.Context.get().getRequest() == null) &&
				contextHolder.get() != null;
	}

	@Override
	protected GenericScopeContextHolder getScopeContextHolder() {
		return contextHolder.get();
	}
	
	protected void init() {
		contextHolder.set(new GenericScopeContextHolder());
	}
	
	protected void destroy() {
		contextHolder.set(null);
	}

	@Override
	public void deactivate() {
		
	}

	@Override
	public void destroy(Contextual<?> contextual) {
		
	}

	@Override
	public void invalidate() {
		
	}

	@Override
	public void activate(String cid) {
		getConversation(cid);
	}

	@Override
	public void activate() {
		activate(generateConversationId());
	}

	@Override
	public void setParameterName(String cid) {
		
	}

	@Override
	public String getParameterName() {
		return "cid";
	}

	@Override
	public void setConcurrentAccessTimeout(long timeout) {
		
	}

	@Override
	public long getConcurrentAccessTimeout() {
		return 0;
	}

	@Override
	public void setDefaultTimeout(long timeout) {
		
	}

	@Override
	public long getDefaultTimeout() {
		return 0;
	}

	@Override
	public Collection<ManagedConversation> getConversations() {
		List<ManagedConversation> convs = new ArrayList<>();
		if (conversationHolder.get() != null) {
			convs.add(conversationHolder.get());
		}
		return convs;
	}

	@Override
	public ManagedConversation getConversation(String id) {
		if (conversationHolder.get() != null && conversationIdHolder.get().equals(id)) {
			return conversationHolder.get();
		}
		return null;
	}

	@Override
	public String generateConversationId() {
		return UUID.randomUUID().toString();
	}

	@Override
	public ManagedConversation getCurrentConversation() {
		ManagedConversation conv = conversationHolder.get();
		if (conv == null) {
			conv = new ConversationImpl(((BeanManagerProxy) manager).unwrap());
			conversationHolder.set(conv);
			conversationIdHolder.set(conv.getId());
		}
		return conv;
	}
	
	/*public static class ArtificialConversation implements ManagedConversation {
		
		private String id;
		private long timeout = 30000;
		private boolean transientConv = true;
		private Date lastUsed = new Date();
		
		public ArtificialConversation() {
			super();
			id = UUID.randomUUID().toString();
		}

		@Override
		public void begin() {
			transientConv = false;
		}

		@Override
		public void begin(String id) {
			begin();
			this.id = id;
		}

		@Override
		public void end() {
			transientConv = true;
		}

		@Override
		public String getId() {
			return id;
		}

		@Override
		public long getTimeout() {
			return timeout;
		}

		@Override
		public void setTimeout(long milliseconds) {
			timeout = milliseconds;
		}

		@Override
		public boolean isTransient() {
			return transientConv;
		}

		@Override
		public boolean unlock() {
			return true;
		}

		@Override
		public boolean lock(long timeout) {
			return true;
		}

		@Override
		public long getLastUsed() {
			return lastUsed.getTime();
		}

		@Override
		public void touch() {
			lastUsed = new Date();
		}
		
	}*/

}
