/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.scopes;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.logging.Level;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.ScopeProvider;
import com.waveinformatica.ocean.core.annotations.SkipAuthorization;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.ResourceInfo;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.results.StreamResult;
import com.waveinformatica.ocean.core.exceptions.OperationNotFoundException;
import com.waveinformatica.ocean.core.util.GlobalClassLoader;
import com.waveinformatica.ocean.core.util.LoggerUtils;

@ScopeProvider(startsWith = StaticResourceScopeProvider.STARTS_WITH)
public class StaticResourceScopeProvider implements IScopeProvider {
	
	public static final String STARTS_WITH = "/ris/";
	
	@Inject
	private ObjectFactory factory;
	
	@Override
	public String produce(ScopeChain chain, String path) {
		
		StaticResourceScope scope = factory.newInstance(StaticResourceScope.class);
		scope.setResourcePath(path);
		
		chain.add(scope);
		
		return "$ocean$/staticResource";
		
	}
	
	public static class StaticResourceScope extends AbstractScope {
		
		private String resourcePath;
		
		public String getResourcePath() {
			return resourcePath;
		}

		public void setResourcePath(String path) {
			this.resourcePath = path;
		}
		
	}
	
	@OperationProvider(inScope = StaticResourceScope.class, namespace = "$ocean$")
	public static class StaticResourceOperations {
		
		@Inject
		private ObjectFactory factory;

		@Inject
		private GlobalClassLoader globalClassLoader;
		
		@Inject
		private Configuration config;
		
		@Operation("staticResource")
		@SkipAuthorization
		public StreamResult resource(StaticResourceScope scope, HttpServletRequest request, HttpServletResponse response) throws OperationNotFoundException, IOException {
			
			StreamResult result = factory.newInstance(StreamResult.class);
			
			String resName = scope.getResourcePath();
			
			ResourceInfo res = null;
			String theme = config.getSiteProperty("theme", "");
			if (StringUtils.isNotBlank(theme)) {
				res = globalClassLoader.getResourceInfo("themes/" + theme + resName, true);
			}
			if (res == null) {
				res = globalClassLoader.getResourceInfo(resName.substring(1), true);
			}
			if (res != null) {

				// Checking cache validity
				if (request != null) {
					long ifModifiedSince = request.getDateHeader("If-Modified-Since");
					if (ifModifiedSince >= 0) {
						Date date = new Date(ifModifiedSince);
						if (!res.getLastModified().after(date)) {
							// The cached resource is valid
							if (response != null) {
								response.setStatus(304);
							}
							return null;
						}
					}
				}

				MimeTypes mt = null;
				String ext = resName.substring(resName.lastIndexOf(".") + 1);
				external:
				for (MimeTypes type : MimeTypes.values()) {
					for (String extension : type.getExtensions()) {
						if (extension.equals(ext)) {
							mt = type;
							break external;
						}
					}
				}

				result.addHeader("Cache-Control", "PUBLIC");
				if (response != null) {
					response.setDateHeader("Expires", new Date(new Date().getTime() + 7 * 24 * 3600 * 1000).getTime());
					response.setDateHeader("Last-Modified", res.getLastModified().getTime());
				}
				result.setContentType(mt != null ? mt.getMimeType() : "application/unknown");
				
				InputStream is = null;
				if (res.getLength() == null) {
					try {
	
						ByteArrayOutputStream out = new ByteArrayOutputStream();
						is = res.getInputStream();
	
						int read = 0;
						byte[] buffer = new byte[4096];
						while ((read = is.read(buffer)) > 0) {
							out.write(buffer, 0, read);
						}
	
						result.setContentLength((long) out.size());
						result.setStream(new ByteArrayInputStream(out.toByteArray()));
						
					} finally {
						if (is != null) {
							try {
								is.close();
							} catch (IOException ex) {
								LoggerUtils.getCoreLogger().log(Level.SEVERE, null, ex);
							}
						}
					}

				} else {

					result.setContentLength(res.getLength().longValue());

					is = res.getInputStream();

					result.setStream(is);
					
				}
				
			}
			else {
				throw new OperationNotFoundException(scope.getResourcePath());
			}
			
			return result;
			
		}
		
	}

}
