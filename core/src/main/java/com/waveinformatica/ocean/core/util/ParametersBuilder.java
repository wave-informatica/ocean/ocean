/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.ocean.core.util;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
public class ParametersBuilder implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final Map<String, List<String>> params = new HashMap<String, List<String>>();
	
	public ParametersBuilder() {
		
	}
	
	public ParametersBuilder(Map<String, String[]> map) {
		for (String k : map.keySet()) {
			if (map.get(k) != null && map.get(k).length > 0) {
				add(k, (String[]) map.get(k));
			}
		}
	}
	
	public ParametersBuilder(HttpServletRequest request) {
		this(request.getParameterMap());
	}
	
	public ParametersBuilder (String url) {
		
		String query = null;
		
		try {
			URL urlParsed = new URL(url);
			query = urlParsed.getQuery();
		} catch (MalformedURLException e) {
			query = url;
		}
		
		if (StringUtils.isBlank(query)) {
			query = "";
		}
		
		int pos = query.indexOf('?');
		if (pos >= 0) {
			query = query.substring(pos + 1);
		}
		
		String[] args = query.split("&");
		for (String p : args) {
			pos = p.indexOf('=');
			try {
				if (pos >= 0) {
					getList(URLDecoder.decode(p.substring(0, pos), "UTF-8")).add(URLDecoder.decode(p.substring(pos + 1), "UTF-8"));
				}
				else {
					getList(URLDecoder.decode(p, "UTF-8"));
				}
			} catch (UnsupportedEncodingException e) {
				
			}
		}
	}
	
	private List<String> getList(String name) {
		List<String> list = params.get(name);
		if (list == null) {
			list = new ArrayList<String>();
			params.put(name, list);
		}
		return list;
	}
	
	public void add(String name, String value) {
		getList(name).add(value);
	}
	
	public void add(String name, String[] values) {
		getList(name).addAll(Arrays.asList(values));
	}
	
	public void add(String name, List<String> values) {
		getList(name).addAll(values);
	}
	
	public void addAll(Map<String, String[]> params) {
		for (String k : params.keySet()) {
			getList(k).addAll(Arrays.asList(params.get(k)));
		}
	}
	
	public void addAll(ParametersBuilder builder) {
		addAll(builder.build());
	}
	
	public void replace(String name, String value) {
		remove(name);
		getList(name).add(value);
	}
	
	public void replace(String name, String[] values) {
		remove(name);
		getList(name).addAll(Arrays.asList(values));
	}
	
	public void replace(String name, List<String> values) {
		remove(name);
		getList(name).addAll(values);
	}
	
	public void replaceAll(Map<String, String[]> params) {
		for (String k : params.keySet()) {
			remove(k);
			getList(k).addAll(Arrays.asList(params.get(k)));
		}
	}
	
	public void replaceAll(ParametersBuilder builder) {
		replaceAll(builder.build());
	}
	
	public void remove(String name) {
		params.remove(name);
	}
	
	public void clear() {
		params.clear();
	}
	
	public Map<String, String[]> build() {
		
		Map<String, String[]> result = new HashMap<String, String[]>();
		
		for (String k : params.keySet()) {
			if (StringUtils.isBlank(k)) {
				continue;
			}
			List<String> list = params.get(k);
			result.put(k, list.toArray(new String[list.size()]));
		}
		
		return result;
		
	}

	@Override
	public String toString() {
		
		StringBuilder queryBuilder = new StringBuilder();
		
		try {
			
			for (String k : params.keySet()) {
				List<String> values = params.get(k);
				if (values != null && !values.isEmpty()) {
					for (String v : values) {
						queryBuilder.append('&');
						queryBuilder.append(URLEncoder.encode(k, "UTF-8"));
						queryBuilder.append('=');
						queryBuilder.append(URLEncoder.encode(v, "UTF-8"));
					}
				}
				else if (StringUtils.isNotBlank(k)) {
					queryBuilder.append('&');
					queryBuilder.append(URLEncoder.encode(k, "UTF-8"));
					if (values != null) {
						queryBuilder.append('=');
					}
				}
			}
			
		} catch (UnsupportedEncodingException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unexpected error building query string", e);
		}
		
		return queryBuilder.length() > 0 ? queryBuilder.substring(1) : "";
	}
	
}
