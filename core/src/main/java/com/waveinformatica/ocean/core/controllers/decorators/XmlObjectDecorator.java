/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.controllers.decorators;

import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.waveinformatica.ocean.core.annotations.ResultDecorator;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.XmlUtils;

@ResultDecorator(classes = Object.class, mimeTypes = MimeTypes.XML)
public class XmlObjectDecorator implements IDecorator<Object> {

	@Inject
	private XmlUtils xmlUtils;
	
	@Override
	public void decorate(Object result, OutputStream out, MimeTypes type) {
		
		try {
			
			Marshaller marshaller = xmlUtils.getContext().createMarshaller();
			
			marshaller.marshal(result, out);
			
		} catch (JAXBException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unexpected error marshalling result to XML.", e);
		}
		
	}

	@Override
	public void decorate(Object result, HttpServletRequest request, HttpServletResponse response, MimeTypes type) {

		try {
			
			response.setContentType("text/html; charset=\"utf-8\"");
			
			Marshaller marshaller = xmlUtils.getContext().createMarshaller();
			
			marshaller.marshal(result, response.getOutputStream());
			
		} catch (JAXBException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unexpected error marshalling result to XML.", e);
		} catch (IOException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unexpected error marshalling result to XML.", e);
		}
		
	}
	
}
