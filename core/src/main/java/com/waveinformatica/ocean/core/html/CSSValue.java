/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.html;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ivano
 */
public class CSSValue {
	
	private boolean important;

	public boolean isImportant() {
		return important;
	}

	public void setImportant(boolean important) {
		this.important = important;
	}
	
	protected static CSSValue parse(CssValuesParser.ValueContext v) {
		if (v.IDENT() != null) {
			return new CSSValueName(v.IDENT().getText());
		}
		else if (v.NUMBER() != null) {
			return new CSSValueNumber(Double.parseDouble(v.NUMBER().getText()));
		}
		else if (v.color() != null) {
			if (v.color().COLOR() != null) {
				return new CSSValueColor(v.color().COLOR().getText());
			}
			else if (v.color().NUMBER().size() == 3) {
				return new CSSValueColor(
					  Double.parseDouble(v.color().NUMBER(0).getText()),
					  Double.parseDouble(v.color().NUMBER(1).getText()),
					  Double.parseDouble(v.color().NUMBER(2).getText())
				);
			}
			else if (v.color().NUMBER().size() == 4) {
				return new CSSValueColor(
					  Double.parseDouble(v.color().NUMBER(0).getText()),
					  Double.parseDouble(v.color().NUMBER(1).getText()),
					  Double.parseDouble(v.color().NUMBER(2).getText()),
					  Double.parseDouble(v.color().NUMBER(3).getText())
				);
			}
			else {
				return null;
			}
		}
		else if (v.dimension() != null) {
			return new CSSValueNumber(Double.parseDouble(v.dimension().NUMBER().getText()), v.dimension().IDENT().getText());
		}
		else if (v.function() != null) {
			List<CSSValue> args = new ArrayList<CSSValue>();
			for (CssValuesParser.ValueContext vc : v.function().args().value()) {
				args.add(CSSValue.parse(vc));
			}
			return new CSSValueFunction(v.function().IDENT().getText(), args);
		}
		else if (v.percentage() != null) {
			return new CSSValueNumber(Double.parseDouble(v.percentage().NUMBER().getText()), "%");
		}
		else if (v.string() != null) {
			if (v.string().STRING_LITERAL_D() != null) {
				return new CSSValueString(v.string().STRING_LITERAL_D().getText());
			}
			else if (v.string().STRING_LITERAL_S() != null) {
				return new CSSValueString(v.string().STRING_LITERAL_S().getText());
			}
			else {
				return new CSSValueString("");
			}
		}
		else if (v.url() != null) {
			if (v.url().STRING_LITERAL_D() != null) {
				return new CSSValueUrl(v.url().STRING_LITERAL_D().getText());
			}
			else if (v.url().STRING_LITERAL_S() != null) {
				return new CSSValueUrl(v.url().STRING_LITERAL_S().getText());
			}
			else {
				return new CSSValueUrl("");
			}
		}
		else {
			return null;
		}
	}
	
}
