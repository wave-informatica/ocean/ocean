/*
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.events;

import com.waveinformatica.ocean.core.controllers.CoreController;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Ivano
 */
public class BeforeOperationEvent extends Event {
    
    private final CoreController.OperationInfo operation;
    private final Map<String, String[]> args;
    private final List<Object> extraObjects = new ArrayList<Object>();

    public BeforeOperationEvent(Object source, CoreController.OperationInfo operation, Map<String, String[]> args, Object[] extraObjects) {
        super(source, CoreEventName.BEFORE_OPERATION.name());
        
        this.operation = operation;
        this.args = args;
        
        if (extraObjects != null) {
            for (Object o : extraObjects) {
                this.extraObjects.add(o);
            }
        }
    }

    public CoreController.OperationInfo getOperation() {
        return operation;
    }

    public Map<String, String[]> getArgs() {
        return args;
    }

    public List<Object> getExtraObjects() {
        return extraObjects;
    }
    
}
