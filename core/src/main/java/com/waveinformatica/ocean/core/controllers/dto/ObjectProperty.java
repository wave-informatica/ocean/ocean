/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.dto;

import com.waveinformatica.ocean.core.util.LoggerUtils;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;

/**
 *
 * @author Ivano
 */
public class ObjectProperty {
	
	private final Field field;
	private final Method getter;
	private final Method setter;
	
	public ObjectProperty (Field field) {
		this.field = field;
		
		String propName = field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
		
		Method m = null;
		
		try {
			m = field.getDeclaringClass().getMethod("is" + propName);
		} catch (NoSuchMethodException ex) {
			try {
				m = field.getDeclaringClass().getMethod("get" + propName);
			} catch (NoSuchMethodException ex1) {
				// Nessun getter
			} catch (SecurityException ex1) {
				// Getter non accessibile
			}
		} catch (SecurityException ex) {
			// Getter non accessibile
		}
		
		getter = m;
		
		m = null;
		
		for (Method pm : field.getDeclaringClass().getMethods()) {
			if (pm.getName().equals("set" + propName) && pm.getParameterTypes().length == 1) {
				m = pm;
				break;
			}
		}
		
		setter = m;
		
	}
	
	public ObjectProperty (Class<?> objectClass, String propName) {
		
		Field field = null;
		
		try {
			field = objectClass.getDeclaredField(propName);
		} catch (NoSuchFieldException e) {
			// Ignore...
		} catch (SecurityException e) {
			// Ignore...
		}
		
		this.field = field;
		
		Method m = null;
		
		try {
			m = objectClass.getMethod("is" + propName);
		} catch (NoSuchMethodException ex) {
			try {
				m = objectClass.getMethod("get" + propName);
			} catch (NoSuchMethodException ex1) {
				// Nessun getter
			} catch (SecurityException ex1) {
				// Getter non accessibile
			}
		} catch (SecurityException ex) {
			// Getter non accessibile
		}
		
		getter = m;
		
		m = null;
		
		for (Method pm : objectClass.getMethods()) {
			if (pm.getName().equals("set" + propName) && pm.getParameterTypes().length == 1) {
				m = pm;
				break;
			}
		}
		
		setter = m;
		
	}
	
	public ObjectProperty(Class<?> objectClass, String propName, Method getter) {
		this.field = null;
		this.getter = getter;
		
		Method m = null;
		for (Method pm : objectClass.getMethods()) {
			if (pm.getName().equals("set" + propName) && pm.getParameterTypes().length == 1) {
				m = pm;
				break;
			}
		}
		
		setter = m;
	}

	public void set(Object dest, Object value) {
		
		if (setter != null) {
			try {
				setter.invoke(dest, value);
			} catch (IllegalAccessException ex) {
				LoggerUtils.getLogger(ObjectProperty.class).log(Level.SEVERE, null, ex);
			} catch (IllegalArgumentException ex) {
				LoggerUtils.getLogger(ObjectProperty.class).log(Level.SEVERE, null, ex);
			} catch (InvocationTargetException ex) {
				LoggerUtils.getLogger(ObjectProperty.class).log(Level.SEVERE, null, ex);
			}
		}
		else if (field != null) {
			try {
				field.setAccessible(true);
				field.set(dest, value);
			} catch (IllegalArgumentException e) {
				LoggerUtils.getLogger(ObjectProperty.class).log(Level.SEVERE, null, e);
			} catch (IllegalAccessException e) {
				LoggerUtils.getLogger(ObjectProperty.class).log(Level.SEVERE, null, e);
			}
		}
	}
	
	public Object get(Object dest) {
		
		if (getter != null) {
			try {
				return getter.invoke(dest);
			} catch (IllegalAccessException ex) {
				LoggerUtils.getLogger(ObjectProperty.class).log(Level.SEVERE, null, ex);
			} catch (IllegalArgumentException ex) {
				LoggerUtils.getLogger(ObjectProperty.class).log(Level.SEVERE, null, ex);
			} catch (InvocationTargetException ex) {
				LoggerUtils.getLogger(ObjectProperty.class).log(Level.SEVERE, null, ex);
			}
		}
		else if (field != null) {
			try {
				field.setAccessible(true);
				return field.get(dest);
			} catch (IllegalArgumentException e) {
				LoggerUtils.getLogger(ObjectProperty.class).log(Level.SEVERE, null, e);
			} catch (IllegalAccessException e) {
				LoggerUtils.getLogger(ObjectProperty.class).log(Level.SEVERE, null, e);
			}
		}
		
		return null;
	}

	public Field getField() {
		return field;
	}

	public Method getGetter() {
		return getter;
	}

	public Method getSetter() {
		return setter;
	}
	
}
