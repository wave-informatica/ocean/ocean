/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.annotations;

import com.waveinformatica.ocean.core.controllers.results.MenuResult;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>This annotation defines a root operation: a root operation is a operation visible
 * in home page. For this reason, @RootOperation annotation defines a title and a iconUrl
 * to build the menu item.</p>
 * 
 * <p>This annotation must be used to complete <code>@Operation</code> annotated methods</p>
 * 
 * @author Ivano
 * @author Luca Lattore
 * @see Operation
 * @see MenuResult
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RootOperation {
    
    /**
     * the menu item title
     * 
     * @return the menu item title
     */
    String title() default "";
    
    /**
     * the menu item icon's URL
     * 
     * @return the menu item icon's URL
     */
    String iconUrl() default "";
    
    /**
     * the menu item section
     * 
     * @return the menu item section
     */
    String section() default "Home";
}
