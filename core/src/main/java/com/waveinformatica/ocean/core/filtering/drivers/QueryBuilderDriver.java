package com.waveinformatica.ocean.core.filtering.drivers;

/**
 * This interface provides a common way to build any kind of conditions.
 * It allows to build conditions for any query system, just providing
 * the right driver (i.e. JPA queries, JPA criterias, etc.).
 * 
 * @author ivano
 *
 * @param <T> resulting condition type (String, etc.)
 * @since 2.4.0
 */
public interface QueryBuilderDriver<T> {
	
	public Class<T> getQueryType();
	
	public T equal(DataReference v1, DataReference v2);
	public T gt(DataReference v1, DataReference v2);
	public T gte(DataReference v1, DataReference v2);
	public T lt(DataReference v1, DataReference v2);
	public T lte(DataReference v1, DataReference v2);
	public T isNull(DataReference v);
	public T isNotNull(DataReference v);
	public T startsWith(DataReference v1, DataReference v2);
	public T endsWith(DataReference v1, DataReference v2);
	public T contains(DataReference v1, DataReference v2);
	public T between(DataReference p, DataReference v1, DataReference v2);
	public T in(DataReference v, DataReference list);
	
	// Functions
	public T concat(DataReference...v);
	public T lower(DataReference v);
	public T upper(DataReference v);
	public T cast(DataReference v, String type);
	public T function(String name, DataReference...params);
	
	public T toQuery(DataReference v);
	public DataQuery<T> toDataQuery(T query);
	
	public T and(T...q);
	public T or(T...q);
	public T not(T q);
	
}
