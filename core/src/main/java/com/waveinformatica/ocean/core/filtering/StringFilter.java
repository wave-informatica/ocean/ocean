/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.filtering;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.filtering.drivers.DataPath;
import com.waveinformatica.ocean.core.filtering.drivers.DataValue;
import com.waveinformatica.ocean.core.filtering.drivers.QueryBuilderDriver;
import com.waveinformatica.ocean.core.filtering.drivers.ValuesManager;

/**
 *
 * @author Ivano
 */
public class StringFilter implements Filter {
	
	private static final Pattern pattern = Pattern.compile("((!?)(\")([^\"]*)\\3)|(^(\\!?)(\\?)(.+))|((!?)([^\\s\"]+))|\\s+");
	private static final Pattern fullPattern = Pattern.compile("^(((!?)(\")([^\"]*)\\4)|(^(\\!?)(\\?)(.+))|((!?)[^\\s\"]+)|\\s+)*$");
	private QueryBuilderDriver driver;
	private ValuesManager values;

	@Override
	public QueryFilter getJpqlFilter(String path, String filterValue, Class<?> expectedType) {
		
		if (StringUtils.isBlank(filterValue)) {
			return null;
		}
		
		if (!fullPattern.matcher(filterValue).matches()) {
			return null;
		}
		
		filterValue = filterValue.trim();
		
		Matcher matcher = pattern.matcher(filterValue);
		
		QueryFilter qf = new QueryFilter();
		
		StringBuilder builder = new StringBuilder();
		int paramIndex = 0;
		String paramName = StringFilter.class.getSimpleName().toLowerCase() + "_" + path.replaceAll("[^a-zA-Z0-9]", "_");
		
		while (matcher.find()) {
			if ("?".equals(matcher.group(7))) {		// ^[!]?regularexpression
				boolean negate = "!".equals(matcher.group(6));
				// Per il momento non lo implementiamo
			}
			else if (StringUtils.isNotBlank(matcher.group(4)) || StringUtils.isNotBlank(matcher.group(11))) {
				String search = null;
				boolean negate = false;
				if (StringUtils.isNotBlank(matcher.group(4))) {	// [!]"..."
					negate = "!".equals(matcher.group(2));
					search = matcher.group(4);
				}
				else if (StringUtils.isNotBlank(matcher.group(11))) {	// [!]singleword
					negate = "!".equals(matcher.group(10));
					search = matcher.group(11);
				}
				
				if (StringUtils.isBlank(search)) {
					continue;
				}
				
				StringBuilder lBuilder = new StringBuilder();
				if (negate) {
					lBuilder.append("not ");
				}
				lBuilder.append("upper(");
				lBuilder.append(path);
				lBuilder.append(") like concat('%', concat(upper(:");
				lBuilder.append(paramName);
				lBuilder.append(paramIndex);
				lBuilder.append("), '%'))");
				qf.set(paramName + paramIndex, search);
				paramIndex++;
				if (builder.length() > 0) {
					builder.append(" and ");
				}
				builder.append(lBuilder.toString());
			}
		}
		
		if (builder.length() > 0) {
			qf.setCondition(builder.toString());
			return qf;
		}
		else {
			return null;
		}
		
	}

	@Override
	public String getTemplate() {
		return "/templates/filters/stdFilter.tpl";
	}

	@Override
	public String getHelpTemplate() {
		return "/templates/filters/stringFilterHelp.tpl";
	}

	@Override
	public void init(Object result, Field field) {
		
	}

	@Override
	public <T> QueryFilter<T> getQueryFilter(String path, String filterValue, Class<?> expectedType, QueryBuilderDriver<T> driver, ValuesManager values) {
		
		if (StringUtils.isBlank(filterValue)) {
			return null;
		}
		
		if (!fullPattern.matcher(filterValue).matches()) {
			return null;
		}
		
		filterValue = filterValue.trim();
		
		Matcher matcher = pattern.matcher(filterValue);
		
		List<T> conditions = new ArrayList<>();
		
		while (matcher.find()) {
			if ("?".equals(matcher.group(7))) {		// ^[!]?regularexpression
				boolean negate = "!".equals(matcher.group(6));
				// Per il momento non lo implementiamo
			}
			else if (StringUtils.isNotBlank(matcher.group(4)) || StringUtils.isNotBlank(matcher.group(11))) {
				String search = null;
				boolean negate = false;
				if (StringUtils.isNotBlank(matcher.group(4))) {	// [!]"..."
					negate = "!".equals(matcher.group(2));
					search = matcher.group(4);
				}
				else if (StringUtils.isNotBlank(matcher.group(11))) {	// [!]singleword
					negate = "!".equals(matcher.group(10));
					search = matcher.group(11);
				}
				
				if (StringUtils.isBlank(search)) {
					continue;
				}
				
				T condition = driver.contains(driver.toDataQuery(driver.upper(new DataPath(path))), driver.toDataQuery(driver.upper(values.getParam(new DataValue(search)))));
				if (negate) {
					condition = driver.not(condition);
				}
				conditions.add(condition);
			}
		}
		
		if (conditions.isEmpty()) {
			return null;
		}
		else {
			
			QueryFilter qf = new QueryFilter(values);
			qf.setWhereClause(driver.and(conditions.toArray((T[]) Array.newInstance(driver.getQueryType(), conditions.size()))));
			return qf;
			
		}
		
	}
	
}
