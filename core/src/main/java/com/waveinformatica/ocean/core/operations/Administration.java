/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.operations;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.Constants;
import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.annotations.RootOperation;
import com.waveinformatica.ocean.core.cache.CacheElement;
import com.waveinformatica.ocean.core.cache.CacheManager;
import com.waveinformatica.ocean.core.cache.CacheManager.RegisteredCache;
import com.waveinformatica.ocean.core.cache.ICacheProvider;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.controllers.CoreControllerInspector;
import com.waveinformatica.ocean.core.controllers.IService;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.ServiceStatus;
import com.waveinformatica.ocean.core.controllers.results.DashboardResult;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.controllers.results.MenuResult;
import com.waveinformatica.ocean.core.controllers.results.MenuResult.MenuSection;
import com.waveinformatica.ocean.core.controllers.results.MessageResult;
import com.waveinformatica.ocean.core.controllers.results.MessageResult.MessageType;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.results.RedirectResult;
import com.waveinformatica.ocean.core.controllers.results.StackResult;
import com.waveinformatica.ocean.core.controllers.results.StreamResult;
import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.core.controllers.results.WebPageResult;
import com.waveinformatica.ocean.core.controllers.results.input.BooleanFieldHandler;
import com.waveinformatica.ocean.core.controllers.results.input.CacheProviderFieldHandler;
import com.waveinformatica.ocean.core.controllers.results.input.ComboFieldHandler.Item;
import com.waveinformatica.ocean.core.controllers.results.input.HiddenFieldHandler;
import com.waveinformatica.ocean.core.controllers.results.input.LogLevelFieldHandler;
import com.waveinformatica.ocean.core.controllers.results.input.OperationFieldHandler;
import com.waveinformatica.ocean.core.controllers.results.input.PasswordFieldHandler;
import com.waveinformatica.ocean.core.controllers.scopes.AdminScopeProvider.AdminRootScope;
import com.waveinformatica.ocean.core.controllers.scopes.OperationScope;
import com.waveinformatica.ocean.core.controllers.scopes.ScopeChain;
import com.waveinformatica.ocean.core.controllers.scopes.ScopesController;
import com.waveinformatica.ocean.core.exceptions.ModuleException;
import com.waveinformatica.ocean.core.exceptions.OperationNotFoundException;
import com.waveinformatica.ocean.core.internal.PersistenceConfigurationManager;
import com.waveinformatica.ocean.core.modules.ComponentsManager;
import com.waveinformatica.ocean.core.modules.Module;
import com.waveinformatica.ocean.core.modules.ModuleClassLoader;
import com.waveinformatica.ocean.core.modules.ModulesManager;
import com.waveinformatica.ocean.core.network.NetworkManager;
import com.waveinformatica.ocean.core.operations.dto.LogMessageWrapper;
import com.waveinformatica.ocean.core.operations.dto.MemoryInfo;
import com.waveinformatica.ocean.core.operations.dto.ModuleLoggingInfo;
import com.waveinformatica.ocean.core.operations.dto.OceanInfo;
import com.waveinformatica.ocean.core.operations.dto.ServiceInfo;
import com.waveinformatica.ocean.core.persistence.DataSourceProvider;
import com.waveinformatica.ocean.core.persistence.PersistenceManager;
import com.waveinformatica.ocean.core.tasks.OceanExecutorService;
import com.waveinformatica.ocean.core.templates.ThemesFieldHandler;
import com.waveinformatica.ocean.core.util.FsUtils;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.LazyRequestParameterMap;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.PersistedProperties;
import com.waveinformatica.ocean.core.util.Results;

/**
 *
 * @author Ivano
 */
@OperationProvider(namespace = "/admin", inScope = AdminRootScope.class)
public class Administration {
	
	private static final Logger logger = LoggerUtils.getCoreLogger();
	
	@Inject
	private ModulesManager modulesManager;
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private Configuration configuration;
	
	@Inject
	private FsUtils fsUtils;
	
	@Inject
	private CoreController coreController;
	
	@Operation("")
	public DashboardResult adminMenu(OperationScope opScope) {
		
		DashboardResult result = factory.newInstance(DashboardResult.class);
		
		result.fireEvent("/admin/", opScope.getChain());
		
		return result;
		
	}
	
	@Operation("site/main")
	@RootOperation(title = "site", section = "systemInformation", iconUrl = "gear")
	public StackResult configureSiteMain(OperationScope opScope) {
		
		StackResult result = factory.newInstance(StackResult.class);
		result.putExtra("page-title", "Administration");
		
		result.addOperation("admin/site/saveProperties");
		result.addOperation("admin/site/input");
		
		Results.addScriptSource(result, "ris/js/ocean-validator.js");

		opScope.addBreadcrumbLink("admin/site/main", "site");
		
		return result;
		
	}
	
	@Operation("variables")
	@RootOperation(title = "systemProperties", section = "systemInformation", iconUrl = "coffee")
	public TableResult variables(OperationScope opScope) {
		
		TableResult res = factory.newInstance(TableResult.class);
		
		res.putExtra("page-title", "Administration");
		
		Map<Object, Object> properties = new TreeMap<Object, Object>();
		Properties props = System.getProperties();
		Set<Map.Entry<Object, Object>> entries = props.entrySet();
		for (Map.Entry<Object, Object> entry : entries) {
			properties.put(entry.getKey(), entry.getValue());
		}
		
		res.setTableData(properties);
		
		opScope.addBreadcrumbLink("admin/variables", "systemProperties");
		
		return res;
	}
	
	@Operation("memoryInfo")
	@RootOperation(title = "memoryStatus", section = "systemInformation", iconUrl = "hdd-o")
	public TableResult memoryInfo(OperationScope opScope) {
		
		TableResult res = factory.newInstance(TableResult.class);
		res.putExtra("page-title", "Administration");
		
		List<MemoryInfo> memoryInfoList = new ArrayList<MemoryInfo>() {
		};
		
		Iterator<MemoryPoolMXBean> iter = ManagementFactory.getMemoryPoolMXBeans().iterator();
		while (iter.hasNext()) {
			memoryInfoList.add(new MemoryInfo(iter.next()));
		}
		
		res.setTableData(memoryInfoList);

		opScope.addBreadcrumbLink("admin", "administration");
		opScope.addBreadcrumbLink("admin/memoryInfo", "memoryStatus");
		
		return res;
	}
	
	@Operation("info")
	@RootOperation(title = "info", section = "systemInformation", iconUrl = "info-circle")
	public WebPageResult info(OperationScope opScope) {
		
		WebPageResult result = factory.newInstance(WebPageResult.class);
		result.putExtra("page-title", "Administration");
		
		result.setTemplate("/templates/oceanInfo.tpl");
		
		OceanInfo info = new OceanInfo();
		
		info.setOrganization(configuration.getOrganization());
		info.setOrganizationUrl(configuration.getOrganizationUrl());
		info.setVersion(configuration.getPackageVersion());
		try {
			info.setLicense(IOUtils.toString(Administration.class.getResourceAsStream("/LICENSE.txt"), Charset.forName("UTF-8")));
		} catch (IOException ex) {
			logger.log(Level.SEVERE, "Unable to read LICENSE.txt file", ex);
		}
		
		result.setData(info);

		opScope.addBreadcrumbLink("admin/info", "info");
		
		return result;
		
	}
	
	@Operation("modules")
	@RootOperation(title = "modules", section = "framework", iconUrl = "shopping-cart")
	public TableResult modulesList(OperationScope opScope) {
		
		TableResult res = factory.newInstance(TableResult.class);
		
		res.putExtra("page-title", "Administration");
		
		List<Module> modules = new ArrayList<Module>(modulesManager.getModules()) {
		};
		Collections.sort(modules, new Comparator<Module>() {
			@Override
			public int compare(Module o1, Module o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		res.setTableData(modules);
		
		res.addTableOperation("admin/loadModule", "loadModule");
		res.addRowOperation("admin/moduleDetails", "details");
		res.addRowOperation("admin/moduleDownload", "download");
		res.addRowOperation("admin/reloadModule", "reload");
		res.addRowOperation("admin/unloadModule", "unloadModule");
		
		res.setMaxOperations(3);
		
		opScope.addBreadcrumbLink("admin/modules", "modules");
		
		return res;
		
	}
	
	@Operation("loadModule")
	public InputResult loadModuleInput(OperationScope opScope) {
		
		InputResult form = factory.newInstance(InputResult.class);
		
		form.setAction("admin/loadModuleConfirm");
		form.setProvider(this);

		opScope.addBreadcrumbLink("admin/modules", "modules");
		opScope.addBreadcrumbLink("admin/loadModule", "loadModule");
		
		return form;
		
	}
	
	@Operation("loadModuleConfirm")
	public MessageResult loadModule(@Param("moduleJar") Part moduleJar) {
		if (moduleJar != null) {
			String header = moduleJar.getHeader("content-disposition");
			String name = header.substring(header.toLowerCase().indexOf("filename=\"") + "filename=\"".length());
			name = name.substring(Math.max(name.lastIndexOf('/'), name.lastIndexOf('\\')) + 1, name.indexOf("\""));
			
			if (!name.endsWith(".jar")) {
				MessageResult res = factory.newInstance(MessageResult.class);

				res.setCancelAction("admin/modules");
				res.setMessage("Invalid module file name \"" + name + "\". The module file must be a JAR file.");
				res.setType(MessageResult.MessageType.CRITICAL);
				return res;
			}
			
			try {
				
				final File moduleFile = fsUtils.renameTempFile(moduleJar.getInputStream(), name);
				final String moduleName = name;
				
				coreController.runLockedAsync(new Runnable() {
					@Override
					public void run() {
						try {
							modulesManager.loadModule(moduleFile);
						} catch (ModuleException e) {
							logger.log(Level.SEVERE, "Error loading module " + moduleName, e);
						} catch (IOException e) {
							logger.log(Level.SEVERE, "Error loading module " + moduleName, e);
						}
					}
				}, true);
				
			} catch (IOException ex) {
				
				logger.log(Level.SEVERE, null, ex);
				
				MessageResult res = factory.newInstance(MessageResult.class);

				res.setConfirmAction("admin/modules");
				res.setMessage("Unable to load module \"" + name + "\"");
				res.setType(MessageResult.MessageType.CRITICAL);
				res.setException(ex);
				return res;
			}
			
			MessageResult res = factory.newInstance(MessageResult.class);

			res.setConfirmAction("admin/modules");
			res.setMessage("Module " + name + " loading has been successfully submitted.");
			res.setType(MessageResult.MessageType.INFO);
			return res;
			
		}
		
		MessageResult res = factory.newInstance(MessageResult.class);
		res.putExtra("page-title", "Administration");

		res.setCancelAction("admin/loadModule");
		res.setMessage("No module file uploaded");
		res.setType(MessageResult.MessageType.WARNING);
		return res;
		
	}
	
	@Operation("moduleDetails")
	public WebPageResult moduleDetails(@Param("name") String name, OperationScope opScope) throws UnsupportedEncodingException {
		
		Module mod = modulesManager.getModule(name);
		
		WebPageResult res = factory.newInstance(WebPageResult.class);
		res.putExtra("page-title", "Administration");

		
		res.setTemplate("templates/moduleDetails.tpl");
		
		List<LogMessageWrapper> messages = new ArrayList<LogMessageWrapper>(mod.getLogRecords().size());
		
		for (LogRecord lr : mod.getLogRecords()) {
			messages.add(new LogMessageWrapper(lr));
		}
		
		res.setData(messages);

		opScope.addBreadcrumbLink("admin/modules", "modules");
		opScope.addBreadcrumbLink("admin/moduleDetails?name=" + URLEncoder.encode(name, "UTF-8"), name);
		
		return res;
		
	}
	
	@Operation("unloadModule")
	public MessageResult unloadModuleInput(@Param("name") String name) {
		
		MessageResult result = factory.newInstance(MessageResult.class);
		result.putExtra("page-title", "Administration");
		result.setType(MessageResult.MessageType.CONFIRM);

		result.setCancelAction("admin/modules");
		result.setConfirmAction("admin/unloadModuleConfirm?name=" + name);
		result.setMessage("Are you sure to definitely delete the module \"" + name + "\"?");
		
		return result;
		
	}
	
	@Operation("unloadModuleConfirm")
	public MessageResult unloadModuleConfirm(@Param("name") final String name) {
		
		coreController.runLockedAsync(new Runnable() {
			@Override
			public void run() {
				try {
					modulesManager.unloadModule(name, true);
				} catch (ModuleException e) {
					logger.log(Level.SEVERE, "Error loading module " + name);
				}
			}
		}, true);
		
		MessageResult result = factory.newInstance(MessageResult.class);
		result.putExtra("page-title", "Administration");
		result.setType(MessageResult.MessageType.INFO);

		result.setConfirmAction("admin/modules");
		result.setMessage("The module \"" + name + "\" unloading has been successfully submitted.");
		
		return result;
		
	}
	
	@Operation("reloadModule")
	public MessageResult reloadModule(@Param("name") String name) {
		
		MessageResult result = factory.newInstance(MessageResult.class);
		result.putExtra("page-title", "Administration");
		result.setType(MessageResult.MessageType.CONFIRM);

		result.setCancelAction("admin/modules");
		result.setConfirmAction("admin/reloadModuleConfirm?name=" + name);
		result.setMessage("Are you sure to fully reload the module \"" + name + "\"?");
		
		return result;
		
	}
	
	@Operation("reloadModuleConfirm")
	public MessageResult reloadModuleConfirm(@Param("name") final String name) {
		
		coreController.runLockedAsync(new Runnable() {
			@Override
			public void run() {
				try {
					modulesManager.reloadModule(name);
				} catch (ModuleException e) {
					logger.log(Level.SEVERE, "Error loading module " + name, e);
				} catch (IOException e) {
					logger.log(Level.SEVERE, "Error loading module " + name, e);
				}
			}
		}, true);
		
		MessageResult result = factory.newInstance(MessageResult.class);
		result.putExtra("page-title", "Administration");
		result.setType(MessageResult.MessageType.INFO);

		result.setConfirmAction("admin/modules");
		result.setMessage("The module \"" + name + "\" reload has been successfully submitted.");
		
		return result;
		
	}
	
	@Operation("moduleDownload")
	public StreamResult moduleDownload(@Param("name") String name) throws FileNotFoundException {
		
		StreamResult result = factory.newInstance(StreamResult.class);
		
		Module module = modulesManager.getModule(name);
		FileInputStream fis = new FileInputStream(module.getModuleClassLoader().getModuleFile());
		
		result.setAttachment(true);
		result.setContentLength(module.getModuleClassLoader().getModuleFile().length());
		result.setContentType(MimeTypes.JAR.getMimeType());
		result.setName(name + ".jar");
		result.setStream(fis);
		
		return result;
		
	}
	
	@Operation("persistence/")
	@RootOperation(title = "persistence", section = "framework", iconUrl = "database")
	public TableResult persistenceMain(PersistenceConfigurationManager manager, OperationScope opScope) {
		
		TableResult result = factory.newInstance(TableResult.class);
		result.putExtra("page-title", "Administration");
		
		result.setTableData(manager.getConnections(), PersistenceConfigurationManager.Element.class);
		result.getHeader().selectColumns("connectionName", "type");
		
		result.addTableOperation("admin/persistence/edit", "new");
		result.addTableOperation("admin/persistence/reload", "reload");
		result.addRowOperation("admin/persistence/info", "info");
		result.addRowOperation("admin/persistence/edit", "edit");
		result.addRowOperation("admin/persistence/properties/", "properties");
		result.addRowOperation("admin/persistence/delete", "delete");

		opScope.addBreadcrumbLink("admin/persistence/", "persistence");
		
		return result;
		
	}
	
	@Operation("persistence/info")
	public TableResult persistenceInfo(
			@Param("connectionName") String name,
			DataSourceProvider provider,
			OperationScope opScope
	) throws SQLException, UnsupportedEncodingException {
		
		TableResult result = factory.newInstance(TableResult.class);
		result.putExtra("page-title", "Administration");
		
		Properties props = provider.getConnectionStatistics(name);
		TreeMap<String, String> map = new TreeMap<String, String>();
		Enumeration<String> pEnum = (Enumeration<String>) props.propertyNames();
		while (pEnum.hasMoreElements()) {
			String k = pEnum.nextElement();
			map.put(k, props.getProperty(k));
		}
		
		result.setTableData(map);
		result.setRowsPerPage(Math.max(props.size(), 20));
		
		result.addTableOperation("admin/persistence/test?connectionName=" + URLEncoder.encode(name, "UTF-8"), "test");

		opScope.addBreadcrumbLink("admin/persistence/", "persistence");
		opScope.addBreadcrumbLink("admin/persistence/info?connectionName=" + URLEncoder.encode(name, "UTF-8"), name);
		
		return result;
		
	}
	
	@Operation("persistence/test")
	public MessageResult persistenceTest(
			@Param("connectionName") String name,
			DataSourceProvider dsProvider,
			OperationScope opScope
	) throws SQLException, UnsupportedEncodingException {
		
		MessageResult result = factory.newInstance(MessageResult.class);
		
		Connection conn = dsProvider.getConnection(name);
		
		if (conn != null) {
			result.setType(MessageResult.MessageType.CONFIRM);
			result.setMessage("Connection successfully obtained from data source");
			result.setConfirmAction("admin/persistence/info?connectionName=" + URLEncoder.encode(name, "UTF-8"));
		}
		else {
			result.setType(MessageResult.MessageType.WARNING);
			result.setMessage("No connection obtanined from data source, but no errors too");
			result.setCancelAction("admin/persistence/info?connectionName=" + URLEncoder.encode(name, "UTF-8"));
		}

		opScope.addBreadcrumbLink("admin/persistence/", "persistence");
		opScope.addBreadcrumbLink("admin/persistence/info?connectionName=" + URLEncoder.encode(name, "UTF-8"), name);
		opScope.addBreadcrumbLink("admin/persistence/test?connectionName=" + URLEncoder.encode(name, "UTF-8"), "test");
		
		return result;
		
	}
	
	@Operation("persistence/reload")
	public MessageResult persistenceReload(PersistenceManager manager, DataSourceProvider dsProvider) {
		
		MessageResult result = factory.newInstance(MessageResult.class);
		
		manager.reloadPersistence(dsProvider);
		
		result.setType(MessageResult.MessageType.CONFIRM);
		result.setMessage("Persistence successfully reloaded");
		result.setConfirmAction("admin/persistence/");
		
		return result;
		
	}
	
	@Operation("persistence/edit")
	public InputResult persistenceEdit(
			@Param("connectionName") String name,
			PersistenceConfigurationManager manager,
			OperationScope opScope
	) throws UnsupportedEncodingException {
		
		InputResult result = factory.newInstance(InputResult.class);
		result.putExtra("page-title", "Administration");
		
		result.setAction("admin/persistence/save");

		opScope.addBreadcrumbLink("admin/persistence/", "persistence");
		
		if (StringUtils.isNotBlank(name)) {
			PersistenceConfigurationManager.Element element = manager.getConnection(name);
			result.setData(element);
			if (element == manager.getDefaultConnection()) {
				result.setValue("defaultConnection", "true");
			}
			opScope.addBreadcrumbLink("admin/persistence/edit?connectionName=" + URLEncoder.encode(name, "UTF-8"), name);
		}
		else {
			opScope.addBreadcrumbLink("admin/persistence/edit", "new");
		}
		
		return result;
		
	}
	
	@Operation("persistence/save")
	public MessageResult persistenceSave(
		  @Param("connectionName") @InputResult.Field(required = true) String connectionName,
		  @Param("type") @InputResult.Field(required = true) PersistenceConfigurationManager.ConnectionType type,
		  @Param("jndiName") @InputResult.Field(tooltip = "ocean.persistence.jndiName.tooltip") String jndiName,
		  @Param("driverClass") @InputResult.Field(tooltip = "ocean.persistence.driverClass.tooltip") String driverClass,
		  @Param("connectionUrl") @InputResult.Field(tooltip = "ocean.persistence.connectionUrl.tooltip") String connectionUrl,
		  @Param("userName") @InputResult.Field(tooltip = "ocean.persistence.userName.tooltip") String userName,
		  @Param("password") @InputResult.Field(handler = PasswordFieldHandler.class, tooltip = "ocean.persistence.password.tooltip") String password,
		  @Param("defaultConnection") Boolean defaultConnection,
		  PersistenceConfigurationManager manager
	) throws IOException {
		
		MessageResult result = factory.newInstance(MessageResult.class);
		result.putExtra("page-title", "Administration");
		
		PersistenceConfigurationManager.Element el = manager.getConnection(connectionName);
		if (el == null) {
			el = manager.createConnection(connectionName, type, jndiName, driverClass, connectionUrl, userName, password);
		}
		else {
			el.setType(type);
			if (type == PersistenceConfigurationManager.ConnectionType.JNDI) {
				el.setJndiName(jndiName);
				el.setDriverClass(null);
				el.setConnectionUrl(null);
				el.setPassword(null);
				el.setUserName(null);
			}
			else if (type == PersistenceConfigurationManager.ConnectionType.JDBC) {
				el.setJndiName(null);
				el.setDriverClass(driverClass);
				el.setConnectionUrl(connectionUrl);
				el.setUserName(userName);
				if (StringUtils.isNotBlank(password)) {
					el.setPassword(password);
				}
			}
		}
		
		if (Boolean.TRUE.equals(defaultConnection)) {
			manager.setDefaultConnection(el);
		}
		
		manager.store();
		
		result.setType(MessageResult.MessageType.CONFIRM);
		result.setMessage("Data successfully saved");
		result.setConfirmAction("admin/persistence/");
		
		return result;
		
	}
	
	@Operation("persistence/delete")
	public MessageResult removePersistence(
		  @Param("connectionName") String connectionName
	) throws UnsupportedEncodingException {
		
		MessageResult result = factory.newInstance(MessageResult.class);
		result.putExtra("page-title", "Administration");
		
		result.setType(MessageResult.MessageType.INFO);
		result.setConfirmAction("admin/persistence/deleteConfirm?connectionName=" + URLEncoder.encode(connectionName, "UTF-8"));
		result.setCancelAction("admin/persistence/");
		result.setMessage("Are you sure you want to remove the persistence configuration \"" + connectionName + "\"?");
		
		return result;
		
	}
	
	@Operation("persistence/deleteConfirm")
	public MessageResult removePersistenceConfirm(
		  @Param("connectionName") String connectionName,
		  PersistenceConfigurationManager manager
	) throws IOException {
		
		PersistenceConfigurationManager.Element el = manager.deleteConnection(connectionName);
		
		MessageResult result = factory.newInstance(MessageResult.class);
		result.putExtra("page-title", "Administration");

		
		if (el != null) {
			
			manager.store();
			
			result.setType(MessageResult.MessageType.CONFIRM);
			result.setConfirmAction("admin/persistence/");
			result.setMessage("Data source successfully removed and configuration stored.");
			
		}
		else {
			
			result.setType(MessageResult.MessageType.WARNING);
			result.setCancelAction("admin/persistence/");
			result.setMessage("No data source found with name \"" + connectionName + "\"");
			
		}
		
		return result;
		
	}
	
	@Operation("persistence/properties/")
	public StackResult editPersistencePropertiesContainer(
			@Param("connectionName") String name,
			OperationScope opScope
	) throws UnsupportedEncodingException {
		StackResult result = factory.newInstance(StackResult.class);
		result.putExtra("page-title", "Administration");

		result.addOperation("admin/persistence/properties/save");
		result.addOperation("admin/persistence/properties/list");
		
		Results.addCssSource(result, "ris/libs/bootstrap3-editable-1.5.1/css/bootstrap-editable.css");
		Results.addScriptSource(result, "ris/libs/bootstrap3-editable-1.5.1/js/bootstrap-editable.min.js");

		opScope.addBreadcrumbLink("admin/persistence/", "persistence");
		opScope.addBreadcrumbLink("admin/persistence/info?connectionName=" + URLEncoder.encode(name, "UTF-8"), name);
		opScope.addBreadcrumbLink("admin/persistence/properties/?connectionName=" + URLEncoder.encode(name, "UTF-8"), "properties");
		
		return result;
	}
	
	@Operation("persistence/properties/list")
	public TableResult persistencePropertiesList(
		  @Param("connectionName") String connectionName,
		  PersistenceConfigurationManager manager
	) throws UnsupportedEncodingException {
		
		TableResult result = factory.newInstance(TableResult.class);
		
		PersistenceConfigurationManager.Element conn = manager.getConnection(connectionName);
		
		result.setTableData(conn.getProperties());
		
		result.getColumn("value").setEditable(TableResult.CellEditType.TEXT, "admin/persistence/properties/save?connectionName=" + URLEncoder.encode(connectionName, "UTF-8"));
		
		result.addTableOperation("admin/persistence/properties/edit?connectionName=" + URLEncoder.encode(connectionName, "UTF-8"), "new");
		result.addRowOperation("admin/persistence/properties/delete?connectionName=" + URLEncoder.encode(connectionName, "UTF-8"), "delete");
		
		return result;
		
	}
	
	@Operation("persistence/properties/edit")
	public InputResult editPersistenceInput(
		  @Param("connectionName") String connectionName,
		  @Param("key") String key,
		  PersistenceConfigurationManager manager,
		  OperationScope opScope
	) throws UnsupportedEncodingException {
		
		InputResult form = factory.newInstance(InputResult.class);
		form.putExtra("page-title", "Administration");
		
		form.setAction("admin/persistence/properties/?connectionName=" + URLEncoder.encode(connectionName, "UTF-8"));
		form.setRefAction("admin/persistence/properties/save");
		form.setProvider(this);
		
		if (key != null) {
			PersistenceConfigurationManager.Element el = manager.getConnection(connectionName);
			if (el != null) {
				form.setValue("pk", key);
				form.setValue("value", el.getProperties().getProperty(key));
			}
		}
		
		form.getFields().remove(0);

		opScope.addBreadcrumbLink("admin/persistence/", "persistence");
		opScope.addBreadcrumbLink("admin/persistence/info?connectionName=" + URLEncoder.encode(connectionName, "UTF-8"), connectionName);
		opScope.addBreadcrumbLink("admin/persistence/properties/?connectionName=" + URLEncoder.encode(connectionName, "UTF-8"), "properties");
		
		return form;
		
	}
	
	@Operation("persistence/properties/save")
	public MessageResult editPersistenceConfirm(
		  @Param("connectionName") String connectionName,
		  @Param("pk") @InputResult.Field(required = true) String key,
		  @Param("value") String value,
		  PersistenceConfigurationManager manager,
		  HttpServletRequest request) throws IOException {
		
		if (!request.getMethod().equals("POST") || key == null) {
			return null;
		}
		
		PersistenceConfigurationManager.Element el = manager.getConnection(connectionName);
		el.getProperties().setProperty(key, value);
		
		manager.store();
		
		MessageResult result = factory.newInstance(MessageResult.class);
		result.setType(MessageResult.MessageType.CONFIRM);
		result.setMessage("Property successfully modified and configuration stored.");
		
		return result;
		
	}
	
	@Operation("persistence/properties/delete")
	public MessageResult removePersistenceProperty(
		  @Param("connectionName") String connectionName,
		  @Param("key") String key
	) throws UnsupportedEncodingException {
		
		MessageResult result = factory.newInstance(MessageResult.class);
		result.putExtra("page-title", "Administration");
		
		result.setType(MessageResult.MessageType.INFO);
		result.setConfirmAction("admin/persistence/properties/deleteConfirm?connectionName=" + URLEncoder.encode(connectionName, "UTF-8") + "&key=" + URLEncoder.encode(key, "UTF-8"));
		result.setCancelAction("admin/persistence/properties/?connectionName=" + URLEncoder.encode(connectionName, "UTF-8"));
		result.setMessage("Are you sure you want to remove the persistence configuration property \"" + key + "\"?");
		
		return result;
		
	}
	
	@Operation("persistence/properties/deleteConfirm")
	public MessageResult removePersistencePropertyConfirm(
		  @Param("connectionName") String connectionName,
		  @Param("key") String key,
		  PersistenceConfigurationManager manager
	) throws IOException {
		
		PersistenceConfigurationManager.Element el = manager.getConnection(connectionName);
		
		el.getProperties().remove(key);
		
		manager.store();
		
		configuration.getPersistenceProperties().remove(key);
		
		MessageResult result = factory.newInstance(MessageResult.class);
		result.putExtra("page-title", "Administration");

		
		result.setType(MessageResult.MessageType.CONFIRM);
		result.setConfirmAction("admin/persistence/properties/?connectionName=" + URLEncoder.encode(connectionName, "UTF-8"));
		result.setMessage("Property successfully removed and configuration stored.");
		
		return result;
		
	}
	
	@Operation("services")
	@RootOperation(title = "services", section = "framework", iconUrl = "briefcase")
	public TableResult servicesList(OperationScope opScope) {
		
		final TableResult res = factory.newInstance(TableResult.class);
		res.putExtra("page-title", "Administration");
		
		List<ServiceStatus> statuses = coreController.getServices();
		List<ServiceInfo> services = new ArrayList<ServiceInfo>(statuses.size()) {
		};
		for (ServiceStatus ss : statuses) {
			ServiceInfo si = new ServiceInfo();
			si.setName(ss.getName());
			si.setRunning(ss.isRunning());
			si.setAutoStart(ss.getAnnotation().autoStart());
			si.setServiceStatus(ss);
			services.add(si);
		}
		
		Collections.sort(services, new Comparator<ServiceInfo>() {
			@Override
			public int compare(ServiceInfo o1, ServiceInfo o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		res.setTableData(services);
		
		res.addRowOperation("admin/startService", "start");
		res.addRowOperation("admin/stopService", "stop");
		res.addRowOperation("admin/manageService", "manage");
		
		res.setRowHandler(new TableResult.BaseRowHandler() {
			
			@Override
			public List<TableResult.TableOperation> handleRowActions(List<TableResult.TableOperation> operations, TableResult.TableRow rowSource) {
				ServiceInfo si = (ServiceInfo) rowSource.getSource();
				List<TableResult.TableOperation> ops = super.handleRowActions(operations, rowSource);
				operations = new ArrayList<TableResult.TableOperation>();
				Iterator<TableResult.TableOperation> iter = ops.iterator();
				while (iter.hasNext()) {
					TableResult.TableOperation to = iter.next();
					TableResult.TableOperation newOp = new TableResult.TableOperation(res);
					newOp.setName(to.getName());
					newOp.setTitle(to.getTitle());
					if (to.getName().equals("admin/manageService") && si.getServiceStatus().getManageOperation() == null) {
						newOp.setEnabled(false);
					} else {
						newOp.setEnabled(true);
					}
					operations.add(newOp);
				}
				return operations;
			}
			
		});
		
		opScope.addBreadcrumbLink("admin/services", "services");
		
		return res;
		
	}
	
	@Operation("startService")
	public MessageResult startService(@Param("name") String name) {
		
		MessageResult res = factory.newInstance(MessageResult.class);
		res.putExtra("page-title", "Administration");
		
		try {
			
			IService s = coreController.getService(name);
			
			if (s.start()) {
				res.setType(MessageResult.MessageType.INFO);
				res.setConfirmAction("admin/services");
				res.setMessage("Service \"" + name + "\" successfully started");
			} else {
				res.setType(MessageResult.MessageType.WARNING);
				res.setConfirmAction("admin/services");
				res.setMessage("Service returned that it was unable to start, but no error was caught.");
			}
			
		} catch (Exception e) {
			
			res.setType(MessageResult.MessageType.CRITICAL);
			res.setCancelAction("admin/services");
			res.setMessage("Unable to start the service \"" + name + "\"");
			res.setException(e);
			
		}
		
		return res;
	}
	
	@Operation("stopService")
	public MessageResult stopService(@Param("name") String name) {
		
		MessageResult res = factory.newInstance(MessageResult.class);
		res.putExtra("page-title", "Administration");
		
		try {
			
			IService s = coreController.getService(name);
			
			s.stop();
			
			res.setType(MessageResult.MessageType.INFO);
			res.setConfirmAction("admin/services");
			res.setMessage("Service \"" + name + "\" successfully stopped");
			
		} catch (Exception e) {
			
			res.setType(MessageResult.MessageType.CRITICAL);
			res.setCancelAction("admin/services");
			res.setMessage("Unable to stop the service \"" + name + "\"");
			res.setException(e);
			
		}
		
		return res;
	}
	
	@Operation("manageService")
	public RedirectResult manageService(@Param("name") String name, HttpServletRequest req) {
		
		IService s = coreController.getService(name);
		
		String path = s.getServiceStatus().getManageOperation();
		if (!path.startsWith("/")) {
			path = "/" + path;
		}
		
		RedirectResult rr = new RedirectResult(req.getContextPath() + path);
		return rr;
		
	}
	
	@Operation("site/saveProperties")
	public MessageResult configureSiteSave(
		  HttpServletRequest request,
		  @Param("title") String title,
		  @Param("siteUrl") String siteUrl,
		  @Param("contextPath") String contextPath,
		  @Param("theme") @InputResult.Field(handler = ThemesFieldHandler.class) String theme,
		  @Param("mergeResources") @InputResult.Field(handler = BooleanFieldHandler.class) Boolean mergeResources,
		  @Param("workerJndi") String workerJndi,
		  OceanExecutorService executorService
	) {
		
		if (!request.getMethod().equalsIgnoreCase("POST")) {
			return null;
		}
		
		MessageResult result = factory.newInstance(MessageResult.class);
		
		configuration.getSiteProperties().setProperty("site-title", title);
		configuration.getSiteProperties().setProperty("site.url", siteUrl);
		configuration.getSiteProperties().setProperty("site.contextPath", contextPath);
		configuration.getSiteProperties().setProperty("theme", theme);
		configuration.getSiteProperties().setProperty("resources.merge", mergeResources ? "true" : "false");
		
		String oldWorkerJndi = configuration.getSiteProperty("concurrency.executor.jndi", null);
		try {
			configuration.getSiteProperties().setProperty("concurrency.executor.jndi", workerJndi);
			executorService.updateConfig();
		} catch (NamingException e) {
			configuration.getSiteProperties().setProperty("concurrency.executor.jndi", oldWorkerJndi);
		}
		
		try {
			
			configuration.getSiteProperties().store("");
			
			result.setMessage("Configuration changed and successfully stored.");
			result.setType(MessageResult.MessageType.INFO);
			
		} catch (IOException ex) {
			logger.log(Level.WARNING, "Configuration changed but it was unable to store", ex);
			result.setMessage("Configuration changed but it was unable to store");
			result.setType(MessageResult.MessageType.WARNING);
		}
		
		return result;
		
	}
	
	@Operation("site/input")
	public InputResult configureSiteInput(OperationScope opScope) {
		
		InputResult result = factory.newInstance(InputResult.class);
		
		result.setAction("admin/site/main");
		result.setRefAction("admin/site/saveProperties");
		
		result.setValue("title", configuration.getSiteProperties().getProperty("site-title"));
		result.setValue("siteUrl", configuration.getSiteProperty("site.url", ""));
		result.setValue("contextPath", configuration.getSiteProperty("site.contextPath", ""));
		result.setValue("theme", configuration.getSiteProperty("theme", ""));
		result.setValue("mergeResources", configuration.getSiteProperty("resources.merge", "false"));
		result.setValue("workerJndi", configuration.getSiteProperty("concurrency.executor.jndi", ""));

		opScope.addBreadcrumbLink("admin/site/main", "site");
		
		return result;
		
	}
	
	@Operation("smtp")
	@RootOperation(title = "smtp", section = "framework", iconUrl = "envelope")
	public TableResult smtp(OperationScope opScope) {
		
		TableResult result = factory.newInstance(TableResult.class);
		result.putExtra("page-title", "Administration");

		
		PersistedProperties properties = configuration.getSmtpProperties();
		
		boolean init = false;
		
		if (!properties.containsKey("mail.smtp.host")) {
			properties.setProperty("mail.smtp.host", "localhost");
			init = true;
		}
		if (!properties.containsKey("mail.smtp.port")) {
			properties.setProperty("mail.smtp.port", "25");
			init = true;
		}
		if (!properties.containsKey("mail.smtp.starttls.enable")) {
			properties.setProperty("mail.smtp.starttls.enable", "false");
			init = true;
		}
		if (!properties.containsKey("mail.user")) {
			properties.setProperty("mail.user", "");
			init = true;
		}
		if (!properties.containsKey("mail.password")) {
			properties.setProperty("mail.password", "");
			init = true;
		}
		if (!properties.containsKey("mail.smtp.socketFactory.port")) {
			properties.setProperty("mail.smtp.socketFactory.port", "");
			init = true;
		}
		if (!properties.containsKey("mail.smtp.socketFactory.class")) {
			properties.setProperty("mail.smtp.socketFactory.class", "");
			init = true;
		}
		if (!properties.containsKey("mail.from")) {
			properties.setProperty("mail.from", "");
			init = true;
		}
		
		if (init) {
			try {
				properties.store("");
			} catch (IOException ex) {
				logger.log(Level.WARNING, "Unable to initialize SMTP configuration", ex);
			}
		}
		
		result.setTableData(properties);
		result.getColumn("value").setEditable(TableResult.CellEditType.TEXT, "admin/smtpSet");

		opScope.addBreadcrumbLink("admin/smtp", "smtp");
		
		return result;
		
	}
	
	@Operation("smtpSet")
	public MessageResult smtpSet(@Param("value") String value, @Param("pk") String name) {
		
		PersistedProperties properties = configuration.getSmtpProperties();
		
		MessageResult result = factory.newInstance(MessageResult.class);
		
		properties.setProperty(name, value);
		
		try {
			
			properties.store("");
			
			result.setType(MessageResult.MessageType.CONFIRM);
			result.setMessage("Configuration successfully saved");
			
		} catch (IOException ex) {
			logger.log(Level.WARNING, "Unable to store SMTP configuration", ex);
			result.setType(MessageResult.MessageType.WARNING);
			result.setMessage("Configuration successfully changed but unable to store");
		}
		
		return result;
		
	}
	
	@Operation("logging")
	@RootOperation(title = "logging", section = "framework", iconUrl = "bug")
	public StackResult logging(OperationScope opScope) {
		
		StackResult result = factory.newInstance(StackResult.class);
		result.putExtra("page-title", "Administration");
		
		Results.addCssSource(result, "ris/libs/bootstrap3-editable-1.5.1/css/bootstrap-editable.css");
		Results.addScriptSource(result, "ris/libs/bootstrap3-editable-1.5.1/js/bootstrap-editable.min.js");
		Results.addScriptSource(result, "ris/js/ocean-validator.js");
		
		result.addOperation("admin/logging/save");
		result.addOperation("admin/logging/core");
		result.addOperation("admin/logging/modules");

		opScope.addBreadcrumbLink("admin/logging", "logging");
		
		return result;
		
	}
	
	@Operation("logging/save")
	public MessageResult loggingSave(
		  @Param("coreLevel") @InputResult.Field(handler = LogLevelFieldHandler.class) String coreLevel,
		  @Param("feedbackAppCode") String feedbackAppCode,
		  @Param("feedbackSender") String feedbackSender,
		  @Param("feedbackRecipient") String feedbackRecipient
	) {
		
		PersistedProperties properties = configuration.getLoggingProperties();
		
		boolean saveProperties = false;
		
		if (coreLevel != null) {
			properties.setProperty("core.level", coreLevel);
			properties.setProperty("ocean.errors.feedback.recipient", feedbackRecipient);
			properties.setProperty("ocean.errors.feedback.application.sender", feedbackSender);
			properties.setProperty("ocean.errors.feedback.application.code", feedbackAppCode);
			logger.setLevel(Level.parse(coreLevel));
			saveProperties = true;
		}
		
		if (saveProperties) {
			MessageResult result = factory.newInstance(MessageResult.class);
			try {
				properties.store("");
				result.setType(MessageResult.MessageType.CONFIRM);
				result.setMessage("Logging configuration successfully changed and stored");
			} catch (IOException ex) {
				logger.log(Level.WARNING, "Logging configuration changed, but not stored", ex);
				result.setType(MessageResult.MessageType.WARNING);
				result.setMessage("Logging configuration successfully changed, but not stored");
				result.setException(ex);
			}
			return result;
		}
		
		return null;
		
	}
	
	@Operation("logging/core")
	public InputResult loggingCore() throws IOException {
		
		PersistedProperties props = configuration.getLoggingProperties();
		
		InputResult result = factory.newInstance(InputResult.class);
		result.putExtra("page-title", "Administration");
		
		result.setAction("admin/logging");
		result.setRefAction("admin/logging/save");
		
		Map<String, String> initial = new HashMap<String, String>();
		initial.put("coreLevel", props.getProperty("core.level"));
		
		boolean init = false;
		if (props.getProperty("core.level") == null) {
			props.setProperty("core.level", logger.getLevel().getName());
			init = true;
		}
		if (!props.containsKey("ocean.errors.feedback.recipient")) {
			props.setProperty("ocean.errors.feedback.recipient", "support@localhost");
			init = true;
		}
		if (!props.containsKey("ocean.errors.feedback.application.sender")) {
			props.setProperty("ocean.errors.feedback.application.sender", "admin@localhost");
			init = true;
		}
		if (!props.containsKey("ocean.errors.feedback.application.code")) {
			props.setProperty("ocean.errors.feedback.application.code", "OCEAN-APP");
			init = true;
		}
		if (init) {
			props.store("");
		}
		
		result.setData(initial);
		result.setValue("feedbackAppCode", props.getProperty("ocean.errors.feedback.application.code"));
		result.setValue("feedbackSender", props.getProperty("ocean.errors.feedback.application.sender"));
		result.setValue("feedbackRecipient", props.getProperty("ocean.errors.feedback.recipient"));
		
		return result;
		
	}
	
	@Operation("logging/modules")
	public TableResult loggingModules() {
		
		PersistedProperties props = configuration.getLoggingProperties();
		
		TableResult result = factory.newInstance(TableResult.class);
		result.putExtra("page-title", "Administration");
		
		List<Module> modules = modulesManager.getModules();
		
		List<ModuleLoggingInfo> info = new ArrayList<ModuleLoggingInfo>(modules.size());
		
		for (Module m : modules) {
			ModuleLoggingInfo mli = new ModuleLoggingInfo();
			mli.setName(m.getName());
			mli.setLevel(props.getProperty(m.getName() + ".level"));
			if (StringUtils.isBlank(mli.getLevel()) && m.getLogger().getLevel() != null) {
				mli.setLevel(m.getLogger().getLevel().getName());
			}
			info.add(mli);
		}
		
		result.setTableData(info, ModuleLoggingInfo.class);
		
		result.getColumn("level").setEditable(TableResult.CellEditType.SELECT, "admin/logging/module").setEditSource("admin/logging/levels");
		
		return result;
		
	}
	
	@Operation("logging/module")
	public MessageResult saveModuleLogging(@Param("pk") String module, @Param("name") String name, @Param("value") String value) {
		
		PersistedProperties props = configuration.getLoggingProperties();
		
		MessageResult result = factory.newInstance(MessageResult.class);
		result.putExtra("page-title", "Administration");
		
		Module m = modulesManager.getModule(module);
		
		Logger logger = m.getLogger();
		
		try {
			
			if ("level".equals(name)) {
				logger.setLevel(Level.parse(value));
			}
			
			props.setProperty(module + "." + name, value);
			props.store("");
			
			result.setMessage("Configuration successfully changed and stored");
			result.setType(MessageResult.MessageType.CONFIRM);
			
		} catch (Exception e) {
			result.setMessage("Unable to change or store configuration");
			result.setType(MessageResult.MessageType.CRITICAL);
			result.setException(e);
		}
		
		return result;
		
	}
	
	@Operation("logging/levels")
	public List<Map<String, String>> levels() {
		
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		
		LogLevelFieldHandler fieldHandler = new LogLevelFieldHandler();
		fieldHandler.init(null, null, null, null);
		
		for (Item item : fieldHandler.getItems()) {
			Map<String, String> el = new HashMap<String, String>();
			el.put("text", item.getLabel());
			el.put("value", item.getValue());
			result.add(el);
		}
		
		return result;
		
	}
	
	@Operation("network/")
	@RootOperation(title = "network", section = "framework", iconUrl = "wifi")
	public InputResult network(
			@Param("httpProxy") String httpProxy,
			@Param("httpsProxy") String httpsProxy,
			NetworkManager nm,
			OperationScope opScope,
			HttpServletRequest request
	) throws IOException {
		
		InputResult result = factory.newInstance(InputResult.class);
		
		result.setAction("admin/network/");
		
		PersistedProperties props = configuration.getNetworkProperties();
		
		MessageResult msg = factory.newInstance(MessageResult.class);
		if ("POST".equals(request.getMethod())) {
			props.setProperty("proxy.http", httpProxy);
			props.setProperty("proxy.https", httpsProxy);
			props.store("");
			
			msg.setMessage("Proxy configuration successfully stored");
			msg.setType(MessageType.CONFIRM);
		}
		else {
			msg.setMessage("Use syntax [username:password@]server[:port] for proxy settings. If port is missing, the default 3128 port will be used.");
			msg.setType(MessageType.WARNING);
		}
		result.setMessage(msg);
		
		result.setValue("httpProxy", props.getProperty("proxy.http"));
		result.setValue("httpsProxy", props.getProperty("proxy.https"));

		opScope.addBreadcrumbLink("admin/network/", "network");
		
		return result;
		
	}
	
	@Operation("caches/")
	@RootOperation(title = "caches", section = "framework", iconUrl = "clone")
	public TableResult caches(CacheManager manager, OperationScope opScope) {
		
		TableResult result = factory.newInstance(TableResult.class);
		
		List<RegisteredCache> caches = manager.getRegisteredCaches();
		
		result.setTableData(caches, RegisteredCache.class);
		
		result.addRowOperation("admin/caches/edit", "edit");
		result.addRowOperation("admin/caches/clear", "clear");

		opScope.addBreadcrumbLink("admin/caches/", "caches");
		
		return result;
		
	}
	
	@Operation("caches/edit")
	public InputResult cachesEdit(@Param("name") String name, CacheManager manager, ScopesController scopes, OperationScope opScope) {
		
		InputResult result = factory.newInstance(InputResult.class);
		
		result.setAction("admin/caches/save");
		
		RegisteredCache cache = null;
		List<RegisteredCache> caches = manager.getRegisteredCaches();
		for (RegisteredCache rc : caches) {
			if (rc.getName().equals(name)) {
				cache = rc;
			}
		}
		
		ICacheProvider provider = manager.getProvider(cache.getProviderName());
		
		OperationFieldHandler configHandler = ((OperationFieldHandler) result.getField("configuration").getHandler());
		if (provider != null && provider.getOperation() != null) {
			ScopeChain chain = scopes.build(provider.getOperation());
			configHandler.setOperation(((OperationScope) chain.getCurrent()).getOperationInfo());
		}
		
		result.reInitializeHandlers();
		
		PersistedProperties props = configuration.getCachingProperties();
		String confStr = props.getProperty(cache.getName());
		
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("name", name);
		values.put("handler", cache.getProviderName());
		if (provider != null && StringUtils.isNotBlank(confStr)) {
			values.put("configuration", manager.getProviderConfiguration(name));
		}
		
		result.setData(values);

		opScope.addBreadcrumbLink("admin/caches/", "caches");
		
		return result;
		
	}
	
	@Operation("caches/save")
	public MessageResult cachesSave(
			@Param("name") @InputResult.Field(handler = HiddenFieldHandler.class) String name,
			@Param("handler") @InputResult.Field(handler = CacheProviderFieldHandler.class, required = true) String cacheProvider,
			@Param("configuration") @InputResult.Field(handler = OperationFieldHandler.class) String data,
			CacheManager manager,
			HttpServletRequest request,
			HttpServletResponse response
	) throws OperationNotFoundException, IOException {
		
		MessageResult result = factory.newInstance(MessageResult.class);
		
		RegisteredCache cache = null;
		List<RegisteredCache> caches = manager.getRegisteredCaches();
		for (RegisteredCache rc : caches) {
			if (rc.getName().equals(name)) {
				cache = rc;
			}
		}
		
		PersistedProperties props = configuration.getCachingProperties();
		
		ICacheProvider provider = manager.getProvider(cacheProvider);
		
		CacheElement ce = new CacheElement();
		ce.setProviderName(cacheProvider);
		
		if (provider != null && StringUtils.isNotBlank(provider.getOperation())) {
			LazyRequestParameterMap reqMap = new LazyRequestParameterMap(request, "configuration.");
			Object res = coreController.executeOperation(provider.getOperation(), reqMap, request, response);
			ce.setConfigJson(JsonUtils.getBuilder().create().toJson(res));
		}
		
		props.setProperty(name, JsonUtils.getBuilder().create().toJson(ce));
		
		props.store("");
		
		manager.updateCache(name);
		
		result.setMessage("Cache configuration successfully updated");
		result.setType(MessageType.CONFIRM);
		result.setConfirmAction("admin/caches/");
		
		return result;
		
	}
	
	@Operation("caches/clear")
	public MessageResult cachesClear(@Param("name") String name, CacheManager manager, ScopesController scopes, OperationScope opScope) {
		
		MessageResult result = factory.newInstance(MessageResult.class);
		
		for (RegisteredCache rc : manager.getRegisteredCaches()) {
			if (rc.getName().equals(name)) {
				rc.getCache().clear();
				result.setMessage("Cache successfully cleared");
				result.setType(MessageType.CONFIRM);
				result.setConfirmAction("admin/caches/");
				return result;
			}
		}
		
		result.setMessage("Cache not found");
		result.setType(MessageType.CRITICAL);
		result.setCancelAction("admin/caches/");
		
		return result;
		
	}
	
	@Operation("inspect/")
	@RootOperation(section = "systemInformation", iconUrl = "search", title = "inspect")
	public MenuResult inspectMain(OperationScope opScope) {
		
		MenuResult result = factory.newInstance(MenuResult.class);
		
		MenuSection sect = result.addSection("information");
		
		sect.add("search", "operations", "admin/inspect/operations/");
		sect.add("search", "componentsBins", "admin/inspect/componentsBins");
		
		opScope.addBreadcrumbLink("admin/inspect/", "inspect");
		
		return result;
		
	}
	
	@Operation("inspect/operations/")
	public TableResult inspectOperations(OperationScope opScope) {
		
		CoreControllerInspector inspector = coreController.getInspector();
		
		TableResult result = factory.newInstance(TableResult.class);
		
		List<OpInfo> infos = new ArrayList<>();
		
		for (String op : inspector.getOperationNames()) {
			OpInfo oi = new OpInfo();
			oi.setOp(op);
			List<OperationInfo> ois = inspector.getOperationInfos(op);
			Set<Class<?>> scopes = new HashSet<>();
			for (OperationInfo opInf : ois) {
				for (Class<?> c : opInf.getProvider().inScope()) {
					scopes.add(c);
				}
			}
			oi.setHandlers(ois.size());
			oi.setScopes(scopes.size());
			infos.add(oi);
		}
		
		result.setTableData(infos, OpInfo.class);
		
		result.addRowOperation("admin/inspect/operations/details", "details");
		
		opScope.addBreadcrumbLink("admin/inspect/", "inspect");
		opScope.addBreadcrumbLink("admin/inspect/operations/", "operations");
		
		return result;
		
	}
	
	@Operation("inspect/operations/details")
	public WebPageResult inspectOperationsDetails(@Param("op") String op, OperationScope opScope) {
		
		WebPageResult result = factory.newInstance(WebPageResult.class);
		
		CoreControllerInspector inspector = coreController.getInspector();
		
		List<OpInfo> data = new ArrayList<>();
		
		{
			OpInfo oi = new OpInfo();
			oi.setOp(op);
			List<OperationInfo> ois = inspector.getOperationInfos(op);
			Set<Class<?>> scopes = new HashSet<>();
			for (OperationInfo opInf : ois) {
				for (Class<?> c : opInf.getProvider().inScope()) {
					scopes.add(c);
				}
			}
			oi.setHandlers(ois.size());
			oi.setScopes(scopes.size());
			result.putExtra("main", oi);
		}
		
		Map<String, OpInfo> m = new HashMap<>();
		
		List<OperationInfo> info = inspector.getOperationInfos(op);
		for (OperationInfo oi : info) {
			for (Class<?> s : oi.getProvider().inScope()) {
				OpInfo opInfo = new OpInfo();
				opInfo.setInfo(oi);
				opInfo.setOp(op);
				opInfo.setScopeCls(s.getName());
				if (oi.getCls().getClassLoader() instanceof ModuleClassLoader) {
					opInfo.setModule(((ModuleClassLoader) oi.getCls().getClassLoader()).getModule().getName());
				}
				else {
					opInfo.setModule(Constants.CORE_MODULE_NAME);
				}
				OpInfo old = m.get(s.getName());
				if (old == null) {
					old = new OpInfo();
					old.setOp(op);
					old.setScopeCls(s.getName());
					m.put(s.getName(), old);
				}
				old.getHandlersList().add(opInfo);
			}
		}
		
		data.addAll(m.values());
		
		Collections.sort(data, new Comparator<OpInfo>() {
			@Override
			public int compare(OpInfo o1, OpInfo o2) {
				return o1.getScopeCls().compareTo(o2.getScopeCls());
			}
		});
		
		result.setData(data);
		result.setTemplate("/templates/admin/inspect/operation.tpl");
		
		result.setProvider(this);
		
		opScope.addBreadcrumbLink("admin/inspect/", "inspect");
		opScope.addBreadcrumbLink("admin/inspect/operations/", "operations");
		opScope.addBreadcrumbLink("admin/inspect/operations/details?op=" + op, op);
		
		return result;
		
	}
	
	@Operation("inspect/componentsBins")
	public WebPageResult inspectComponentsBins(ComponentsManager manager, OperationScope opScope) {
		
		WebPageResult result = factory.newInstance(WebPageResult.class);
		
		result.setTemplate("/templates/admin/inspect/componentsBins.tpl");
		
		result.setData(manager.getContent());
		
		opScope.addBreadcrumbLink("admin/inspect/", "inspect");
		opScope.addBreadcrumbLink("admin/inspect/componentsBins", "componentsBins");
		
		return result;
		
	}
	
	public static class OpInfo {
		
		private String op;
		private int scopes;
		private int handlers;
		private OperationInfo info;
		@TableResult.TableResultColumn(ignore = true)
		private String scopeCls;
		@TableResult.TableResultColumn(ignore = true)
		private String module;
		private final List<OpInfo> handlersList = new ArrayList<>(1);

		public String getOp() {
			return op;
		}

		public void setOp(String op) {
			this.op = op;
		}

		public int getScopes() {
			return scopes;
		}

		public void setScopes(int scopes) {
			this.scopes = scopes;
		}

		public int getHandlers() {
			return handlers;
		}

		public void setHandlers(int operations) {
			this.handlers = operations;
		}

		public OperationInfo getInfo() {
			return info;
		}

		public void setInfo(OperationInfo info) {
			this.info = info;
		}

		public String getScopeCls() {
			return scopeCls;
		}

		public void setScopeCls(String scopeCls) {
			this.scopeCls = scopeCls;
		}

		public String getModule() {
			return module;
		}

		public void setModule(String module) {
			this.module = module;
		}

		public List<OpInfo> getHandlersList() {
			return handlersList;
		}
		
	}
	
}
