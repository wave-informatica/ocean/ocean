package com.waveinformatica.ocean.core.controllers.webservice;

import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.inject.Inject;
import javax.wsdl.Binding;
import javax.wsdl.BindingFault;
import javax.wsdl.BindingOperation;
import javax.wsdl.Definition;
import javax.wsdl.Fault;
import javax.wsdl.Input;
import javax.wsdl.Message;
import javax.wsdl.Operation;
import javax.wsdl.Output;
import javax.wsdl.Part;
import javax.wsdl.Port;
import javax.wsdl.PortType;
import javax.wsdl.Service;
import javax.wsdl.Types;
import javax.wsdl.WSDLException;
import javax.wsdl.extensions.schema.Schema;
import javax.wsdl.extensions.soap.SOAPAddress;
import javax.wsdl.extensions.soap.SOAPBinding;
import javax.wsdl.extensions.soap.SOAPBody;
import javax.wsdl.extensions.soap.SOAPFault;
import javax.wsdl.extensions.soap.SOAPOperation;
import javax.wsdl.factory.WSDLFactory;
import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ibm.wsdl.extensions.schema.SchemaImpl;
import com.ibm.wsdl.extensions.soap.SOAPAddressImpl;
import com.ibm.wsdl.extensions.soap.SOAPBindingImpl;
import com.ibm.wsdl.extensions.soap.SOAPBodyImpl;
import com.ibm.wsdl.extensions.soap.SOAPFaultImpl;
import com.ibm.wsdl.extensions.soap.SOAPOperationImpl;
import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.ExcludeSerialization;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.controllers.CoreController.ParameterInfo;
import com.waveinformatica.ocean.core.controllers.CoreControllerInspector;
import com.waveinformatica.ocean.core.controllers.dto.ObjectProperty;
import com.waveinformatica.ocean.core.controllers.scopes.AbstractScope;
import com.waveinformatica.ocean.core.interfaces.InputStreamProvider;
import com.waveinformatica.ocean.core.util.ObjectUpdater;
import com.waveinformatica.ocean.core.util.OceanUpload;

public class WsdlBuilder {
	
	public static final String NS_SOAP = "http://schemas.xmlsoap.org/wsdl/soap/";
	
	// Switch to export exceptions
	private static final boolean manageExceptions = false;

	@Inject
	private CoreController controller;
	
	@Inject
	private ObjectUpdater updater;
	
	@Inject
	private Configuration config;

	public Map<Class<?>, Set<String>> getWebServiceOperations(Class<?> baseScope) {

		CoreControllerInspector inspector = controller.getInspector();
		Set<String> names = inspector.getOperationNames();

		Map<Class<?>, Set<String>> opsPerScope = new HashMap<>();

		for (String n : names) {
			List<OperationInfo> infos = inspector.getOperationInfos(n);
			for (OperationInfo oi : infos) {
				for (Class<?> scope : oi.getProvider().inScope()) {
					if (!baseScope.isAssignableFrom(scope)) {
						continue;
					}
					Set<String> ops = opsPerScope.get(scope);
					if (ops == null) {
						ops = new TreeSet<>();
						opsPerScope.put(scope, ops);
					}
					ops.add(n);
				}
			}
		}

		return opsPerScope;

	}
	
	public Definition build(WebServiceScope scope) throws WSDLException {
		return build((AbstractScope) scope, scope.getNamespace(), scope.getName());
	}

	public Definition build(AbstractScope scope, String namespace, String name) throws WSDLException {
		
		Map<String, Element> definedTypes = new HashMap<>();
		
		Class<?> scopeCls = scope.getClass();

		WSDLFactory wsdlFactory = WSDLFactory.newInstance();
		Definition definition = wsdlFactory.newDefinition();
		definition.setQName(new QName(namespace, name));
		definition.addNamespace("tns", namespace);
		definition.addNamespace("soap", NS_SOAP);
		definition.addNamespace("xmime",  "http://www.w3.org/2005/05/xmlmime");
		definition.setTargetNamespace(namespace);
		
		Set<Type> types = new HashSet<>();

		// Inspecting all operations and needed types
		Set<String> operations = getWebServiceOperations(scopeCls).get(scopeCls);
		Set<OperationInfo> operationInfos = new HashSet<>();
		for (String n : operations) {
			OperationInfo opInfo = controller.getOperationInfo(scopeCls, n);
			Type rt = opInfo.getMethod().getGenericReturnType();
			if (rt != Void.class) {
				types.add(rt);
			}
			for (ParameterInfo pi : opInfo.getParameters()) {
				if (StringUtils.isNotBlank(pi.getName())) {
					types.add(pi.getGenericType());
				}
			}
			if (manageExceptions) {
				for (Class<?> e : opInfo.getMethod().getExceptionTypes()) {
					types.add(e);
				}
			}
			operationInfos.add(opInfo);
		}
		
		// WSDL types section
		Schema schema = buildSchema(definedTypes, types, operationInfos, namespace);
		Types wsdlTypes = definition.createTypes();
		wsdlTypes.addExtensibilityElement(schema);
		//wsdlTypes.setDocumentationElement(schema.getElement());
		definition.setTypes(wsdlTypes);
		
		// WSDL messages section
		for (OperationInfo oi : operationInfos) {
			// Input
			Message msg = definition.createMessage();
			msg.setUndefined(false);
			msg.setQName(new QName(namespace, oi.getMethod().getName() + "Input"));
			Part part = definition.createPart();
			part.setName("body");
			part.setElementName(new QName(namespace, oi.getMethod().getName() + "Request"));
			msg.addPart(part);
			definition.addMessage(msg);
			// Output
			if (oi.getMethod().getReturnType() != Void.class) {
				msg = definition.createMessage();
				msg.setUndefined(false);
				msg.setQName(new QName(namespace, oi.getMethod().getName() + "Output"));
				part = definition.createPart();
				part.setName("body");
				part.setElementName(new QName(namespace, oi.getMethod().getName() + "Response"));
				msg.addPart(part);
				definition.addMessage(msg);
			}
			if (manageExceptions && oi.getMethod().getExceptionTypes().length > 0) {
				for (Class<?> e : oi.getMethod().getExceptionTypes()) {
					msg = definition.createMessage();
					msg.setUndefined(false);
					msg.setQName(new QName(namespace, e.getSimpleName()));
					part = definition.createPart();
					part.setName("fault");
					part.setElementName(new QName(namespace, exceptionToFaultName(e.getSimpleName())));
					msg.addPart(part);
					definition.addMessage(msg);
				}
			}
		}
		
		// PortType section
		PortType portType = definition.createPortType();
		portType.setUndefined(false);
		portType.setQName(new QName(namespace, name + "PortType"));
		for (OperationInfo oi : operationInfos) {
			Operation op = definition.createOperation();
			op.setUndefined(false);
			op.setName(oi.getMethod().getName());
			Input input = definition.createInput();
			input.setMessage(definition.getMessage(new QName(namespace, oi.getMethod().getName() + "Input")));
			op.setInput(input);
			if (oi.getMethod().getReturnType() != Void.class) {
				Output output = definition.createOutput();
				output.setMessage(definition.getMessage(new QName(namespace, oi.getMethod().getName() + "Output")));
				op.setOutput(output);
			}
			if (manageExceptions) {
				for (Class<?> e : oi.getMethod().getExceptionTypes()) {
					Fault fault = definition.createFault();
					fault.setMessage(definition.getMessage(new QName(namespace, e.getSimpleName())));
					fault.setName(exceptionToFaultName(e.getSimpleName()));
					op.addFault(fault);
				}
			}
			portType.addOperation(op);
		}
		definition.addPortType(portType);
		
		// Binding section
		Binding binding = definition.createBinding();
		binding.setUndefined(false);
		binding.setQName(new QName(namespace, name + "SoapBinding"));
		binding.setPortType(portType);
		SOAPBinding soapBinding = new SOAPBindingImpl();
		soapBinding.setTransportURI("http://schemas.xmlsoap.org/soap/http");
		soapBinding.setStyle("document");
		binding.addExtensibilityElement(soapBinding);
		for (OperationInfo oi : operationInfos) {
			SOAPBody soapBody = new SOAPBodyImpl();
			soapBody.setUse("literal");
			BindingOperation operation = definition.createBindingOperation();
			operation.setName(oi.getMethod().getName());
			operation.setBindingInput(definition.createBindingInput());
			operation.getBindingInput().addExtensibilityElement(soapBody);
			if (oi.getMethod().getReturnType() != Void.class) {
				operation.setBindingOutput(definition.createBindingOutput());
				operation.getBindingOutput().addExtensibilityElement(soapBody);
			}
			SOAPOperation soapOperation = new SOAPOperationImpl();
			soapOperation.setSoapActionURI(oi.getFullName());
			operation.addExtensibilityElement(soapOperation);
			binding.addBindingOperation(operation);
			if (manageExceptions) {
				for (Class<?> e : oi.getMethod().getExceptionTypes()) {
					BindingFault fault = definition.createBindingFault();
					fault.setName(e.getSimpleName());
					SOAPFault soapFault = new SOAPFaultImpl();
					soapFault.setName(exceptionToFaultName(e.getSimpleName()));
					soapFault.setUse("literal");
					fault.addExtensibilityElement(soapFault);
					operation.addBindingFault(fault);
				}
			}
		}
		definition.addBinding(binding);
		
		// Build service location
		String base = config.getSiteProperty("site.url", "http://localhost:8080/");
		if (base.endsWith("/")) {
			base = base.substring(0, base.length() - 1);
		}
		base += Application.getInstance().getBaseUrl();
		if (!base.endsWith("/")) {
			base += "/";
		}
		if (scope instanceof WebServiceScope) {
			String servicePath = ((WebServiceScope) scope).getServicePath();
			if (servicePath != null) {
				if (servicePath.startsWith("/")) {
					servicePath = servicePath.substring(1);
				}
				base += servicePath;
			}
		}
		
		// Service section
		Service service = definition.createService();
		service.setQName(new QName(namespace, name + "Service"));
		Port port = definition.createPort();
		port.setName(name + "Port");
		port.setBinding(binding);
		SOAPAddress soapAddress = new SOAPAddressImpl();
		soapAddress.setLocationURI(base);
		port.addExtensibilityElement(soapAddress);
		service.addPort(port);
		definition.addService(service);

		return definition;

	}
	
	private String exceptionToFaultName(String simpleName) {
		if (simpleName.endsWith("Exception")) {
			simpleName = simpleName.substring(0, simpleName.length() - "Exception".length());
		}
		else if (simpleName.endsWith("Error")) {
			simpleName = simpleName.substring(0, simpleName.length() - "Error".length());
		}
		return simpleName + "Fault";
	}

	protected Schema buildSchema(Map<String, Element> definedTypes, Set<Type> types, Set<OperationInfo> operationInfos, String targetNamespace) {
		
		Schema schema = buildSchema(targetNamespace);
		
		for (Type type : types) {
			buildTypeElement(definedTypes, schema.getElement().getOwnerDocument(), type);
		}
		
		for (Element el : definedTypes.values()) {
			schema.getElement().appendChild(el);
		}
		
		for (OperationInfo opInfo : operationInfos) {
			schema.getElement().appendChild(buildRequest(definedTypes, schema.getElement().getOwnerDocument(), opInfo));
			schema.getElement().appendChild(buildResponse(definedTypes, schema.getElement().getOwnerDocument(), opInfo.getMethod()));
		}
		
		return schema;
		
	}
	
	protected Schema buildSchema(String targetNamespace) {
		
		SchemaImpl schema = new SchemaImpl();
		
		schema.setElementType(new QName(XMLConstants.W3C_XML_SCHEMA_NS_URI, "schema"));
		
		try {
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
	        DocumentBuilder builder = dbf.newDocumentBuilder();
	        DOMImplementation domImpl = builder.getDOMImplementation();
	        Document doc = domImpl.createDocument(XMLConstants.W3C_XML_SCHEMA_NS_URI, "schema", null);
	        
	        //Element rootEl = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "xs:schema");
	        //doc.appendChild(rootEl);
	        
	        schema.setElement(doc.getDocumentElement());
	        schema.getElement().setPrefix("xs");
	        
	        doc.getDocumentElement().setAttribute("targetNamespace", targetNamespace);
	        doc.getDocumentElement().setAttribute("xmlns", targetNamespace);
	        //doc.getDocumentElement().setAttribute("xmlns:xs", XMLConstants.W3C_XML_SCHEMA_NS_URI);
	        
		} catch (ParserConfigurationException e) {
			// ???
		}
		
		return schema;
		
	}
	
	public boolean isPrimitive(Type type) {
		if (type instanceof Class) {
			Class<?> cl = (Class<?>) type;
			if (cl.isArray()) {
				return isPrimitive(cl.getComponentType());
			}
			else if (Collection.class.isAssignableFrom(cl)) {
				boolean primitive = true;
				for (TypeVariable<?> vt : cl.getTypeParameters()) {
					primitive = primitive && isPrimitive(vt);
				}
				return primitive;
			}
			else if (cl.isPrimitive() || Number.class.isAssignableFrom(cl) || Boolean.class.isAssignableFrom(cl) || String.class.isAssignableFrom(cl) || Byte.class.isAssignableFrom(cl)) {
				return true;
			}
			else if (cl == Object.class) {
				return true;
			}
			else if (cl.isEnum()) {
				return true;
			}
			else if (javax.servlet.http.Part.class.isAssignableFrom(cl) ||
					OceanUpload.class.isAssignableFrom(cl) ||
					InputStream.class.isAssignableFrom(cl) ||
					OutputStream.class.isAssignableFrom(cl) ||
					InputStreamProvider.class.isAssignableFrom(cl)) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	public String getPrimitiveType(Type type) {
		if (type instanceof Class) {
			Class<?> cl = (Class<?>) type;
			if (cl.isArray()) {
				if (cl.getComponentType() == Byte.class || cl.getComponentType() == Byte.TYPE) {
					return "xs:base64Binary";
				}
				else {
					return getPrimitiveType(cl.getComponentType());
				}
			}
			else if (Collection.class.isAssignableFrom(cl)) {
				if (cl.getTypeParameters().length == 1) {
					return getPrimitiveType(cl.getTypeParameters()[0]);
				}
				else {
					return null;
				}
			}
			else if (type == Integer.class || type == Integer.TYPE || type == Long.class || type == Long.TYPE) {
				return "xs:integer";
			}
			else if (type == Double.TYPE || type == Float.TYPE || type == Double.class || type == Float.class || type == BigDecimal.class) {
				return "xs:decimal";
			}
			else if (type == Boolean.TYPE || type == Boolean.class) {
				return "xs:boolean";
			}
			else if (type == Object.class) {
				return "xs:anyType";
			}
			else if (type == String.class) {
				return "xs:string";
			}
			else if (type == Date.class || type == Calendar.class || type == java.util.Date.class) {
				return "xs:date";
			}
			else if (type == Time.class) {
				return "xs:time";
			}
			else if (cl.isEnum()) {
				return cl.getSimpleName();
			}
			else if (javax.servlet.http.Part.class.isAssignableFrom(cl) ||
					OceanUpload.class.isAssignableFrom(cl) ||
					InputStream.class.isAssignableFrom(cl) ||
					OutputStream.class.isAssignableFrom(cl) ||
					InputStreamProvider.class.isAssignableFrom(cl)) {
				return "xs:base64Binary";
			}
			else {
				return null;
			}
		}
		else {
			return null;
		}
	}
	
	public boolean isStream(Type type) {
		return "xs:base64Binary".equals(getPrimitiveType(type));
	}
	
	public boolean isObject(Type type) {
		return "xs:anyType".equals(getPrimitiveType(type));
	}
	
	protected void buildTypeElement(Map<String, Element> definedTypes, Document doc, Type type) {
		
		if (type instanceof Class) {
			Class<?> cl = (Class<?>) type;
			
			if (definedTypes.containsKey(cl.getName())) {
				return;
			}
			if (isPrimitive(type)) {
				if (cl.isEnum()) {
					Element el = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "xs:simpleType");
					definedTypes.put(cl.getName(), el);
					el.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "name", cl.getSimpleName());
					Element restriction = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "xs:restriction");
					el.appendChild(restriction);
					restriction.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "base", "xs:string");
					for (Object o : cl.getEnumConstants()) {
						Element enumeration = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "enumeration");
						restriction.appendChild(enumeration);
						enumeration.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "value", o.toString());
					}
				}
				else {
					return;
				}
			}
			else if (cl.isArray()) {
				buildTypeElement(definedTypes, doc, cl.getComponentType());
				return;
			}
			else if (Collection.class.isAssignableFrom(cl)) {
				for (TypeVariable<?> vt : cl.getTypeParameters()) {
					buildTypeElement(definedTypes, doc, cl.getTypeParameters()[0]);
				}
				return;
			}
			else if (Map.class.isAssignableFrom(cl)) {
				// TODO: ???
				return;
			}
			else {
				
				Element el = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "xs:complexType");
				
				definedTypes.put(cl.getName(), el);
				
				if (Throwable.class.isAssignableFrom(cl)) {
					
					el.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "name", exceptionToFaultName(cl.getSimpleName()));
					
				}
				else {
					
					el.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "name", cl.getSimpleName());
					
				}
				
				List<Element> attributes = new ArrayList<>();
				
				boolean hasNonPrimitive = false;
				Map<String, ObjectProperty> props = updater.getObjectProperties(cl);
				for (ObjectProperty op : props.values()) {
					if (op.getField() != null && isPrimitive(op.getField().getType()) && !op.getField().getType().isArray() && !Collection.class.isAssignableFrom(op.getField().getType()) &&
							!isStream(op.getField().getType()) && !isObject(op.getField().getType())) {
						String primitiveType = getPrimitiveType(op.getField().getType());
						Element attr = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "xs:attribute");
						attr.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "name", op.getField().getName());
						attr.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "type", primitiveType);
						attributes.add(attr);
					}
					else {
						hasNonPrimitive = true;
					}
				}
				
				if (hasNonPrimitive) {
					Element seq = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "xs:sequence");
					el.appendChild(seq);
					
					for (ObjectProperty op : props.values()) {
						if (op.getField() == null || op.getField().getAnnotation(ExcludeSerialization.class) != null) {
							continue;
						}
						if (!isPrimitive(op.getField().getType()) && !op.getField().getType().isArray() && !Collection.class.isAssignableFrom(op.getField().getType()) &&
								!Map.class.isAssignableFrom(op.getField().getType())) {
							buildTypeElement(definedTypes, doc, op.getField().getGenericType());
							Element element = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "xs:element");
							element.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "name", op.getField().getName());
							element.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "type", definedTypes.get(op.getField().getType().getName()).getAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "name"));
							element.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "maxOccurs", "1");
							seq.appendChild(element);
						}
						else if (isStream(op.getField().getType())) {
							Element attr = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "xs:element");
							attr.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "name", op.getField().getName());
							attr.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "type", getPrimitiveType(op.getField().getType()));
							attr.setAttributeNS("http://www.w3.org/2005/05/xmlmime", "xmime:expectedContentTypes", "application/octet-stream");
							seq.appendChild(attr);
						}
						else if (isObject(op.getField().getType())) {
							Element attr = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "xs:element");
							attr.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "name", op.getField().getName());
							attr.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "type", getPrimitiveType(op.getField().getType()));
							seq.appendChild(attr);
						}
						else if (op.getField().getType().isArray()) {
							buildTypeElement(definedTypes, doc, op.getField().getType().getComponentType());
							Element element = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "xs:element");
							element.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "name", op.getField().getName());
							if (isPrimitive(op.getField().getType().getComponentType())) {
								element.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "type", getPrimitiveType(op.getField().getType()));
							}
							else {
								element.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "type", definedTypes.get(op.getField().getType().getComponentType().getName()).getAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "name"));
							}
							element.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "minOccurs", "0");
							element.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "maxOccurs", "unbounded");
							seq.appendChild(element);
						}
						else if (Collection.class.isAssignableFrom(op.getField().getType())) {
							for (Type vt : ((ParameterizedType) op.getField().getGenericType()).getActualTypeArguments()) {
								buildTypeElement(definedTypes, doc, vt);
							}
							if (op.getField().getType().getTypeParameters().length == 1) {
								Element element = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "xs:element");
								element.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "name", op.getField().getName());
								if (isPrimitive(((ParameterizedType) op.getField().getGenericType()).getActualTypeArguments()[0])) {
									element.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "type", getPrimitiveType(((ParameterizedType) op.getField().getGenericType()).getActualTypeArguments()[0]));
								}
								else {
									element.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "type", definedTypes.get(((ParameterizedType) op.getField().getGenericType()).getActualTypeArguments()[0].getTypeName()).getAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "name"));
								}
								element.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "minOccurs", "0");
								element.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "maxOccurs", "unbounded");
								seq.appendChild(element);
							}
							else {
								// TODO: ????
							}
						}
						else {
							// TODO: ???
						}
					}
				}
				
				for (Element attr : attributes) {
					el.appendChild(attr);
				}
				
			}
		}
		
	}
	
	protected Element buildRequest(Map<String, Element> definedTypes, Document doc, OperationInfo opInfo) {
		
		Element el = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "xs:element");
		
		el.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "name", opInfo.getMethod().getName() + "Request");
		
		List<ParameterInfo> params = new ArrayList<>();
		for (ParameterInfo pi : opInfo.getParameters()) {
			if (StringUtils.isNotBlank(pi.getName())) {
				params.add(pi);
			}
		}
		if (!params.isEmpty()) {
			Element complexType = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "xs:complexType");
			el.appendChild(complexType);
			Element sequence = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "xs:sequence");
			complexType.appendChild(sequence);
			for (ParameterInfo pi : params) {
				Element pe = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "xs:element");
				sequence.appendChild(pe);
				pe.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "name", pi.getName());
				fillElementType(definedTypes, pe, pi.getParamClass(), null);
			}
		}
		
		return el;
		
	}
	
	protected Element buildResponse(Map<String, Element> definedTypes, Document doc, Method method) {
		
		Element el = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "xs:element");
		el.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "name", method.getName() + "Response");
		
		Element typeEl = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "xs:complexType");
		el.appendChild(typeEl);
		Element seq = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "xs:sequence");
		typeEl.appendChild(seq);
		
		Element returnEl = doc.createElementNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "xs:element");
		seq.appendChild(returnEl);
		
		returnEl.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "name", "return");
		
		fillElementType(definedTypes, returnEl, method.getGenericReturnType(), null);
		
		return el;
		
	}
	
	protected void fillElementType(Map<String, Element> definedTypes, Element el, Type type, Element complexTypeOrSequence) {
		
		final String xs = XMLConstants.W3C_XML_SCHEMA_NS_URI;
		
		if (isPrimitive(type)) {
			String typeName = getPrimitiveType(type);
			el.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "type", typeName);
			if ("xs:base64Binary".equals(typeName)) {
				el.setAttributeNS("http://www.w3.org/2005/05/xmlmime", "xmime:expectedContentTypes", "application/octet-stream");
			}
		}
		else if (type instanceof Class<?>) {
			Class<?> cl = (Class<?>) type;
			if (cl.isArray()) {
				el.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "minOccurs", "0");
				el.setAttributeNS(xs, "maxOccurs", "unbounded");
				fillElementType(definedTypes, el, cl.getComponentType(), null);
			}
			else if (Iterable.class.isAssignableFrom(cl)) {
				el.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "minOccurs", "0");
				el.setAttributeNS(xs, "maxOccurs", "unbounded");
				fillElementType(definedTypes, el, ((ParameterizedType) type).getActualTypeArguments()[0], null);
			}
			else if (Map.class.isAssignableFrom(cl)) {
				// TODO: ????
			}
			else {
				if (definedTypes.get(cl.getName()) != null) {
					el.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "type", definedTypes.get(cl.getName()).getAttributeNS(XMLConstants.W3C_XML_SCHEMA_NS_URI, "name"));
				}
				else {
					Element sequence = el.getOwnerDocument().createElementNS(xs, "sequence");
					Map<String, ObjectProperty> props = updater.getObjectProperties(cl);
					for (ObjectProperty op : props.values()) {
						if (op.getField() == null) {
							continue;
						}
						Element fel = el.getOwnerDocument().createElementNS(xs, "element");
						fillElementType(definedTypes, fel, op.getField().getGenericType(), null);
					}
					if (complexTypeOrSequence == null) {
						complexTypeOrSequence = el.getOwnerDocument().createElementNS(xs, "complexType");
						el.appendChild(complexTypeOrSequence);
					}
					complexTypeOrSequence.appendChild(sequence);
				}
			}
		}
	}

}
