/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.html;

/**
 *
 * @author Ivano
 */
public class CSSValueUrl extends CSSValue {
	
	private String value;
	
	public CSSValueUrl(String val) {
		val = val.trim();
		if (val.length() >= 2) {
			if (val.charAt(0) == '\'' || val.charAt(0) == '"') {
				val = val.substring(1, val.length() - 1);
			}
		}
		this.value = val;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "url('" + value + "')";
	}
	
}
