/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.resources;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.ScopeProvider;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.scopes.AbstractScope;
import com.waveinformatica.ocean.core.controllers.scopes.IScopeProvider;
import com.waveinformatica.ocean.core.controllers.scopes.ScopeChain;
import com.waveinformatica.ocean.core.util.LoggerUtils;

/**
 *
 * @author Ivano
 */
@OperationProvider(inScope = MergedResourcesController.MergedResourcesScope.class)
public class MergedResourcesController {

	private static final Logger logger = LoggerUtils.getCoreLogger();
	
	@Inject
	private Configuration config;

	@Operation("serve")
	public void serve(MergedResourcesScope scope, HttpServletRequest request, HttpServletResponse response)
	{

		File res = new File(config.getConfigDir(), "resmerge/" + scope.getBundlePath());
		if (res.exists()) {

			// Checking cache validity
			long ifModifiedSince = request.getDateHeader("If-Modified-Since");
			if (ifModifiedSince >= 0) {
				Date date = new Date(ifModifiedSince);
				if (! new Date(res.lastModified()).after(date)) {
					// The cached resource is valid
					response.setStatus(304);
					return;
				}
			}

			MimeTypes mt = null;
			String ext = scope.getBundlePath().substring(scope.getBundlePath().lastIndexOf(".") + 1);
			external: for (MimeTypes type : MimeTypes.values()) {
				for (String extension : type.getExtensions()) {
					if (extension.equals(ext)) {
						mt = type;
						break external;
					}
				}
			}

			response.setHeader("Cache-Control", "PUBLIC");
			response.setDateHeader("Expires", new Date(new Date().getTime() + 7 * 24 * 3600 * 1000).getTime());
			response.setDateHeader("Last-Modified", res.lastModified());
			response.setContentType(mt != null ? mt.getMimeType() : "application/unknown");

			InputStream is = null;
			try {
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				is = new FileInputStream(res);

				int read = 0;
				byte[] buffer = new byte[4096];
				while ((read = is.read(buffer)) > 0) {
					out.write(buffer, 0, read);
				}

				response.setContentLength(out.size());
				response.getOutputStream().write(out.toByteArray());
			}
			catch (IOException ex) {

				// TODO: UT010029: Stream is closed in WildFly 10
				logger.log(Level.FINE, "Error sending static resource \"" + scope.getBundlePath() + "\"", ex);
			}
			finally {
				if (is != null) {
					try {
						is.close();
					}
					catch (IOException ex) {
						logger.log(Level.SEVERE, null, ex);
					}
				}
			}
		}
	}

	@ScopeProvider(startsWith = "/resmerge/")
	public static class ResmergeScopeProvider implements IScopeProvider {

		@Override
		public String produce(ScopeChain chain, String path) {
			
			MergedResourcesScope scope = new MergedResourcesScope();
			scope.setBundlePath(path.substring("/resmerge/".length()));
			chain.add(scope);
			
			return "/serve";
		}
		
	}
	
	public static class MergedResourcesScope extends AbstractScope {
		
		private String bundlePath;

		public String getBundlePath() {
			return bundlePath;
		}

		public void setBundlePath(String path) {
			this.bundlePath = path;
		}
		
	}
}
