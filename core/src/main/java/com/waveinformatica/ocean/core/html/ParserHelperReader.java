/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.html;

import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.util.Arrays;

/**
 *
 * @author Ivano
 */
public class ParserHelperReader extends Reader {

	private final Reader originalReader;
	private char[] lastRead;
	private int nextPos = 0;
	private int lastPos = 0;

	public ParserHelperReader(Reader originalReader) {
		this.originalReader = originalReader;
	}
	
	@Override
	public int read(char[] cbuf, int off, int len) throws IOException {
		
		lastPos = nextPos;
		
		int read = this.originalReader.read(cbuf, off, len);
		
		if (read >= 0) {
			lastRead = Arrays.copyOf(cbuf, read);
			nextPos += read;
		}
		else {
			lastRead = new char[0];
		}
		
		return read;
	}

	@Override
	public void close() throws IOException {
		originalReader.close();
	}

	public String getReadData() {
		return "[" + lastPos + "]:" + String.valueOf(lastRead);
	}
	
}
