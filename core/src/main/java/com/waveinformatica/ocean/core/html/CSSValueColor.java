/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.html;

/**
 *
 * @author Ivano
 */
public class CSSValueColor extends CSSValue {
	
	private String value;
	private double red;
	private double green;
	private double blue;
	private double opacity = 1.0;
	
	public CSSValueColor (String val) {
		this.value = val.trim().toLowerCase();
	}
	
	public CSSValueColor(double r, double g, double b) {
		this.red = r;
		this.green = g;
		this.blue = b;
		this.value = String.format("#%02x%02x%02x", (int) r, (int) g, (int) b);
	}
	
	public CSSValueColor(double r, double g, double b, double opacity) {
		this.red = r;
		this.green = g;
		this.blue = b;
		this.opacity = opacity;
		this.value = String.format("#%02x%02x%02x", (int) r, (int) g, (int) b);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public double getRed() {
		return red;
	}

	public void setRed(double red) {
		this.red = red;
	}

	public double getGreen() {
		return green;
	}

	public void setGreen(double green) {
		this.green = green;
	}

	public double getBlue() {
		return blue;
	}

	public void setBlue(double blue) {
		this.blue = blue;
	}

	public double getOpacity() {
		return opacity;
	}

	public void setOpacity(double opacity) {
		this.opacity = opacity;
	}

	@Override
	public String toString() {
		if (opacity != 1.0) {
			return "rgba(" + red + "," + green + "," + blue + "," + opacity + ")";
		}
		else {
			return value;
		}
	}
	
}
