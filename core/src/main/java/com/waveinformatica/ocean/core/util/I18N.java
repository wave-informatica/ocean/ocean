/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.util;

import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.controllers.dto.ResourceInfo;
import com.waveinformatica.ocean.core.i18n.Translate;
import com.waveinformatica.ocean.core.i18n.TranslationImpl;
import com.waveinformatica.ocean.core.modules.ClassInfoCache;
import com.waveinformatica.ocean.core.modules.Module;
import com.waveinformatica.ocean.core.modules.ModuleClassLoader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.inject.Inject;

/**
 *
 * @author Ivano
 */
@Named
@ApplicationScoped
public class I18N {
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private ClassInfoCache i18nClassesCache;

	private final Map<String, Properties> globalMessages = new HashMap<String, Properties>();
	private final Map<String, Map<String, Properties>> modulesMessages = new LinkedHashMap<String, Map<String, Properties>>();
	private final Map<Class<?>, Class<?>> classBasedI18N = new HashMap<Class<?>, Class<?>>();

	public I18N() {

		// Loading global messages
		loadFromClassLoader(I18N.class.getClassLoader(), globalMessages);

	}

	public void loadModuleI18N(Module module)
	{
		Map<String, Properties> messages = new HashMap<String, Properties>();
		loadFromClassLoader(module.getModuleClassLoader(), messages);
		modulesMessages.put(module.getName(), messages);
		
		// Loading module's class based i18n
		ModuleClassLoader classLoader = module.getModuleClassLoader();
		Set<String> names = classLoader.getAnnotatedClassNames(TranslationImpl.class);
		if (names != null) {
			for (String name : names) {
				
				try {
					
					Class<?> impl = classLoader.loadClass(name);
					TranslationImpl annotation = impl.getAnnotation(TranslationImpl.class);
					classBasedI18N.put(annotation.value(), impl);
					
				} catch (ClassNotFoundException ex) {
					LoggerUtils.getCoreLogger().log(Level.SEVERE, null, ex);
				}
				
			}
		}
	}

	public void unloadModule(Module module)
	{
		modulesMessages.remove(module.getName());
		
		Iterator<Map.Entry<Class<?>, Class<?>>> iter = classBasedI18N.entrySet().iterator();
		while (iter.hasNext()) {
			Class<?> c = iter.next().getKey();
			if (c.getClassLoader() instanceof ModuleClassLoader) {
				Module m = ((ModuleClassLoader) c.getClassLoader()).getModule();
				if (m == module) {
					iter.remove();
				}
			}
		}
		
	}
	
	private Locale findLocale(String l) {
		for (Locale locale : Locale.getAvailableLocales()) {
			if (l.equals(locale.toString())) {
				return locale;
			}
		}
		int pos = l.indexOf('_');
		if (pos >= 0) {
			l = l.substring(pos + 1);
			for (Locale locale : Locale.getAvailableLocales()) {
				if (l.equals(locale.toString())) {
					return locale;
				}
			}
		}
		return null;
	}

	private void loadFromClassLoader(ClassLoader classLoader, Map<String, Properties> result)
	{
		List<ResourceInfo> resources = ResourceScanner.findResources(classLoader, "i18n/", true);
		Collections.reverse(resources);
		for (ResourceInfo res : resources) {
			if (! res.getUrl().getFile().endsWith(".properties")) {
				continue;
			}
			int lastSlash = res.getUrl().getFile().lastIndexOf('/') + 1;
			String key = res.getUrl().getFile().substring(lastSlash, res.getUrl().getFile().indexOf('.', lastSlash));
			Locale locale = findLocale(key);
			if (locale == null) {
				locale = Locale.ENGLISH;
			}
			key = locale.toString();
			try {
				InputStream is = res.getInputStream();
				if (is != null) {
					try {
						Properties props = new Properties();
						props.load(is);
						Properties pp = result.get(key);
						if (pp == null) {
							result.put(key, props);
						}
						else {
							pp.putAll(props);
						}
					}
					catch (IOException ex) {
						Logger.getLogger(I18N.class.getName()).log(Level.SEVERE, null, ex);
					}
					finally {
						is.close();
					}
				}
			}
			catch (IOException iOException) {
				LoggerUtils.getCoreLogger().log(Level.WARNING, "Unable to read I18N resource \"" + res.toString() + "\"", iOException);
			}
		}
	}
	
	/**
	 * This method retrieves the implementation of the given interface annotated with @{@link Translate}
	 * annotation for the current internationalization.
	 * 
	 * @param <T> the interface type
	 * @param iface the interface class
	 * @return the implementation object if found, <code>null</code> if none found
	 * @since 2.2.0
	 */
	@SuppressWarnings("unchecked")
	public <T> T getTranslationClass(Class<T> iface) {
		
		Locale[] langs = Context.get().getLanguages();
		
		if (langs == null || langs.length == 0) {
			langs = new Locale[]{ Locale.ENGLISH };
		}
		
		Locale lang = langs[0];
		
		String key = lang.getLanguage() + ":" + iface.getName();
		
		final Class<?> implCls = classBasedI18N.get(iface);
		
		if (implCls == null) {
			return null;
		}
		
		return (T) i18nClassesCache.get(iface, key, new ClassInfoCache.Factory() {
			@Override
			public Object create() {
				return factory.newInstance(implCls);
			}
		});
		
	}

	/**
	 * <p>
	 * Translates the <code>source</code> argument using internationalization
	 * definition .properties files in loaded modules, web application or core.
	 * To find the correct destination language, the browser languages will be
	 * used.
	 * </p>
	 * 
	 * @param source string to be translated
	 * @return translated string or the original string if no translation is found
	 */
	public String translate(String source)
	{
		return translate(source, new String[0]);
	}

	/**
	 * Translates the <code>source</code> argument using internationalization
	 * definition .properties files in loaded modules, web application or core.
	 * To find the correct destination language, the browser languages will be
	 * used.
	 * 
	 * @param source string to be translated
	 * @param param string used as argument when formatting the translated value
	 * @return translated string or the default value if no translation is found
	 */
	public String translate(String source, String ... param)
	{
		for (Map.Entry<String, Map<String, Properties>> entry : modulesMessages.entrySet()) {
			String res = translateFromMap(source, entry.getValue());
			if (res != null) {
				if (param.length > 0)
					return String.format(res, (Object[]) param);
				else
					return res;
			}
		}
		String res = translateFromMap(source, globalMessages);
		if (res != null) {
			if (param.length > 0)
				return String.format(res, (Object[]) param);
			else
				return res;
		}
		else {
			source = source.substring(source.lastIndexOf('.') + 1);
			for (Map.Entry<String, Map<String, Properties>> entry : modulesMessages.entrySet()) {
				res = translateFromMap(source, entry.getValue());
				if (res != null) {
					if (param.length > 0)
						return String.format(res, (Object[]) param);
					else
						return res;
				}
			}
			res = translateFromMap(source, globalMessages);
			if (res != null) {
				if (param.length > 0)
					return String.format(res, (Object[]) param);
				else
					return res;
			}

		}

		return StringUtils.capitalize(source.substring(source.lastIndexOf('.') + 1));
	}

	/**
	 * <p>
	 * Translates the <code>source</code> argument using internationalization
	 * definition .properties files in loaded modules, web application or core.
	 * This method is similar to the {@link #translate(java.lang.String) #translate(java.lang.String)}
	 * except that this method forces the transation to the specified language
	 * instead of using the browser provided information.
	 * </p>
	 * 
	 * @param lang the language to translate to
	 * @param source string to be translated
	 * @return translated string or the original string if no translation is found
	 */
	public String translateLang(String lang, String source)
	{
		return translateLang(lang, source, new String[0]);
	}

	/**
	 * Translates the <code>source</code> argument using internationalization
	 * definition .properties files in loaded modules, web application or core.
	 * This method is similar to the {@link #translate(java.lang.String, java.lang.String) #translate(java.lang.String, java.lang.String)}
	 * except that this method forces the transation to the specified language
	 * instead of using the browser provided information.
	 * 
	 * @param lang the language to translate to
	 * @param source string to be translated
	 * @param param string used as argument when formatting the translated value
	 * @return translated string or the default value if no translation is found
	 */
	public String translateLang(String lang, String source, String ... param)
	{
		for (Map.Entry<String, Map<String, Properties>> entry : modulesMessages.entrySet()) {
			String res = translateFromProperties(source, lang, entry.getValue());
			if (res != null) {
				if (param.length > 0)
					return String.format(res, (Object[]) param);
				else
					return res;
			}
		}
		String res = translateFromProperties(source, lang, globalMessages);
		if (res != null) {
			if (param.length > 0)
				return String.format(res, (Object[]) param);
			else
				return res;
		}
		else {
			source = source.substring(source.lastIndexOf('.') + 1);
			for (Map.Entry<String, Map<String, Properties>> entry : modulesMessages.entrySet()) {
				res = translateFromProperties(source, lang, entry.getValue());
				if (res != null) {
					if (param.length > 0)
						return String.format(res, (Object[]) param);
					else
						return res;
				}
			}
			res = translateFromProperties(source, lang, globalMessages);
			if (res != null) {
				if (param.length > 0)
					return String.format(res, (Object[]) param);
				else
					return res;
			}

		}

		return StringUtils.capitalize(source.substring(source.lastIndexOf('.') + 1));
	}
	
	/**
	 * This method returns all the messages generated from all LANG.properties files for
	 * the current internationalization.
	 * 
	 * @return the messages map
	 * @since 2.1.0
	 */
	public Map<String, String> getAllMessages() {
		
		Set<String> keys = new HashSet<String>();
		
		for (Properties p : globalMessages.values()) {
			for (Object k : p.keySet()) {
				keys.add(k.toString());
			}
		}
		for (Map<String, Properties> m : modulesMessages.values()) {
			for (Properties p : m.values()) {
				for (Object k : p.keySet()) {
					keys.add(k.toString());
				}
			}
		}
		
		Map<String, String> result = new HashMap<String, String>();
		
		for (String k : keys) {
			result.put(k, translate(k));
		}
		
		return result;
		
	}

	private String translateFromMap(String source, Map<String, Properties> map)
	{
		
		for (Locale lang : Context.get().getLanguages()) {

			String key = lang.getLanguage();
			if (StringUtils.isNotEmpty(lang.getCountry()))
				key += "_" + lang.getCountry();

			if (map.get(key) != null && map.get(key).get(source) != null) {
				return map.get(key).get(source).toString();
			}
			else if (map.get(lang.getLanguage()) != null && map.get(lang.getLanguage()).get(source) != null) {
				return map.get(lang.getLanguage()).get(source).toString();
			}
		}

		return null;
	}

	private String translateFromProperties(String source, String lang, Map<String, Properties> map)
	{

		if (map.get(lang) != null && map.get(lang).get(source) != null) {
			return map.get(lang).get(source).toString();
		}

		return null;
	}
	
}