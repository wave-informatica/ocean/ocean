/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.converters;

import java.lang.reflect.Type;
import java.util.Map;

/**
 *
 * @author Ivano
 */
public interface ITypeConverter {
    
    public Object fromParametersMap(String destName, Class destClass, Type genericType, Map<String, String[]> source, Object[] ctxObjects);
    
    public Object fromScalar(String value, Class destClass, Map<String, String[]> source, Object[] ctxObjects);
    
}
