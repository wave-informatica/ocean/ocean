/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.html;

import org.xhtmlrenderer.extend.ReplacedElementFactory;
import org.xhtmlrenderer.extend.ReplacedElement;
import org.xhtmlrenderer.extend.UserAgentCallback;
import org.xhtmlrenderer.layout.LayoutContext;
import org.xhtmlrenderer.render.BlockBox;

import org.w3c.dom.Element;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import org.xhtmlrenderer.simple.extend.FormSubmissionListener;

/**
 * 
 */
public class ChainedReplacedElementFactory implements ReplacedElementFactory {
	
    private List factoryList;

    public ChainedReplacedElementFactory() {
        this.factoryList = new ArrayList();
    }

    public ReplacedElement createReplacedElement(LayoutContext c, BlockBox box, UserAgentCallback uac, int cssWidth, int cssHeight) {
        ReplacedElement re = null;
        for (Iterator it = factoryList.iterator(); it.hasNext();) {
            ReplacedElementFactory  ref =  (ReplacedElementFactory) it.next();
            re = ref.createReplacedElement(c, box, uac, cssWidth, cssHeight);
            if ( re != null) break;
        }
        return re;
    }

    public void addFactory(ReplacedElementFactory ref) {
        this.factoryList.add(ref);
    }

    public void reset() {
        for (Iterator i = this.factoryList.iterator(); i.hasNext(); ) {
            ReplacedElementFactory factory = (ReplacedElementFactory)i.next();
            factory.reset();
        }       
    }

    public void remove(Element e) {
        for (Iterator i = this.factoryList.iterator(); i.hasNext(); ) {
            ReplacedElementFactory factory = (ReplacedElementFactory)i.next();
            factory.remove(e);
        }       
    }

    public void setFormSubmissionListener(FormSubmissionListener listener) {
        // nothing to do ?
    }
}