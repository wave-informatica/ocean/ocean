/*
 * Copyright 2017, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.scopes;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.ExcludeSerialization;
import com.waveinformatica.ocean.core.security.SecurityManager;
import com.waveinformatica.ocean.core.security.UserSession;

/**
 * This scope represents the application's root context
 * 
 * @author ivano
 * @since 2.2.0
 */
public class RootScope extends AbstractScope implements TemplateScope {
	
	@Inject
	@ExcludeSerialization
	private UserSession userSession;
	
	@Inject
	@ExcludeSerialization
	private SecurityManager securityManager;
	
	@Inject
	@ExcludeSerialization
	private Configuration configuration;
	
	private String scopeName = "";
	private String headerTemplate = "/templates/includes/header.tpl";
	private String footerTemplate = "/templates/includes/footer.tpl";
	
	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String name) {
		this.scopeName = name;
	}

	@Override
	public String getHeaderTemplate() {
		return headerTemplate;
	}

	public void setHeaderTemplate(String headerTemplate) {
		this.headerTemplate = headerTemplate;
	}

	@Override
	public String getFooterTemplate() {
		return footerTemplate;
	}

	public void setFooterTemplate(String footerTemplate) {
		this.footerTemplate = footerTemplate;
	}

	public UserSession getUserSession() {
		return userSession;
	}

	public SecurityManager getSecurityManager() {
		return securityManager;
	}

	public Configuration getConfiguration() {
		return configuration;
	}
	
	public String getContextPath() {
		return Application.getInstance().getBaseUrl();
	}
	
	@Override
	protected String getName() {
		return "\\\\" + scopeName;
	}
}
