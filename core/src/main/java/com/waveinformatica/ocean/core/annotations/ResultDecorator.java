/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.waveinformatica.ocean.core.controllers.decorators.IDecorator;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.scopes.AbstractScope;

/**
 * This annotation defines a result's decorator by result types and output mime types.
 * 
 * A result decorator must implement the IDecorator interface too.
 * 
 * @author Ivano
 * @see IDecorator
 * @see BaseResult
 */
@OceanComponent(mustExtend = IDecorator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ResultDecorator {
    
    /**
     * List of result types
     * 
     * @return list of result types
     */
    Class[] classes();
    
    /**
     * List of output MIME types
     * 
     * @return list of output mime types
     */
    MimeTypes[] mimeTypes();
    
    /**
     * List of scopes to filter decorator's applicability
     * 
     * @return list of filter scopes
     */
    Class[] inScope() default AbstractScope.class;
    
}
