/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.cdi.CDIHelper;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.InstanceListener;
import com.waveinformatica.ocean.core.modules.ModulesManager;
import com.waveinformatica.ocean.core.persistence.PersistenceManager;
import com.waveinformatica.ocean.core.tasks.OceanExecutorService;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.PersistedProperties;

/**
 *
 * @author Ivano
 */
@WebListener
public class FrameworkLifecycle implements ServletContextListener {
	
	@Inject
	private ModulesManager modulesManager;
	
	@Inject
	private CoreController coreController;
	
	@Inject
	private Configuration configuration;
	
	@Inject
	private Application application;
	
	@Inject
	private PersistenceManager persistence;
	
	@Inject
	private OceanExecutorService executorService;
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		
		FrameworkConfiguration fwConfig = null;
		
		String appName = sce.getServletContext().getRealPath("/");
		if (appName != null) {
			appName = new File(sce.getServletContext().getRealPath("/")).getName();
		}
		
		try {
			InitialContext ic = new InitialContext();
			
			// App name
			try {
				appName = (String) ic.lookup("java:app/AppName");
			} catch (NamingException ex) {
				Logger.getLogger(FrameworkLifecycle.class.getName()).log(Level.WARNING, "Unable to find resource with name \"java:app/AppName\". Defaulting to legacy mode.");
			}
			
			// Framework config
			try {
				fwConfig = (FrameworkConfiguration) ic.lookup("java:comp/env/ocean/configuration");
			} catch (NamingException ex) {
				Logger.getLogger(FrameworkLifecycle.class.getName()).log(Level.WARNING, "Reading framework configuration from \"java:comp/env/ocean/configuration\" failed");
			}
		} catch (NamingException e) {
			Logger.getLogger(FrameworkLifecycle.class.getName()).log(Level.WARNING, "Unable to instantiate InitialContext.");
		}
		if (StringUtils.isBlank(appName)) {
			appName = "";
		}
		
		if (fwConfig == null) {
			fwConfig = new FrameworkConfiguration();
		}
		
		application.init(sce.getServletContext(), appName);
		
		if (fwConfig.getUrlFactory() != null) {
			modulesManager.setUrlFactory(fwConfig.getUrlFactory());
		}
		
		Logger logger = LoggerUtils.getCoreLogger();
		
		logger.info("Ocean Framework initializing for context \"" + appName + "\" with context root \"" + sce.getServletContext().getContextPath() + "/\"...");
		
		configuration.initPackageConfiguration(sce.getServletContext());
		
		logger.info("Ocean Framework version: " + configuration.getPackageVersion());
		
		CDIHelper.startCdiContexts(null, (HttpSession) null);
		
		try {
			String appBase = System.getProperty("catalina.base");
			if (fwConfig.getBasePath() != null) {
				appBase = fwConfig.getBasePath().getAbsolutePath();
			}
			if (appBase == null) {
				appBase = System.getProperty("jboss.server.base.dir");
			}
			if (appBase == null) {
				appBase = new File(".").getAbsolutePath();
			}
			
			logger.info("Using application base path \"" + appBase + "\"");
			
			// Preparing modules folder
			File cfgDir = new File(appBase + "/Ocean/" + appName);
			if (!cfgDir.exists()) {
				cfgDir.mkdirs();
				File modulesDir = new File(cfgDir, "modules");
				modulesDir.mkdirs();
			}
			configuration.setConfigDir(cfgDir);
			
			Context.init(sce);
			Context.setCoreController(coreController);
			
			// Core logger configuration
			PersistedProperties loggingProps = configuration.getLoggingProperties();
			String loggerLevel = loggingProps.getProperty("core.level");
			if (loggerLevel != null) {
				LoggerUtils.getCoreLogger().setLevel(Level.parse(loggerLevel));
			}
			else {
				LoggerUtils.getCoreLogger().setLevel(Level.INFO);
			}
			
			// Application loading
			try {
				
				modulesManager.loadCore(fwConfig);
				
				modulesManager.loadModules();
				
			} catch (Throwable e) {
				LoggerUtils.getCoreLogger().log(Level.SEVERE, "Error initializing Ocean Framework", e);
			} finally {
				Context.setCoreController(null);
				Context.clear();
			}
			
		} finally {
			CDIHelper.stopCdiContexts();
			InstanceListener.get().clear();
		}
		
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		
		try {
			Logger logger = LoggerUtils.getCoreLogger();

			logger.info("Ocean Framework core stopping for context \"" + sce.getServletContext().getContextPath() + "\"...");

			CDIHelper.startCdiContexts(null, (HttpSession) null);
			
			try {
				
				executorService.stop();
				
			} catch (InterruptedException e) {
				LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unexpected error stopping executor service", e);
			}

			try {

				modulesManager.unloadModules();

			} catch (Throwable t) {
				LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unexpected error unloading modules", t);
			}

			persistence.unloadCore();

		} finally {
			CDIHelper.stopCdiContexts();
			InstanceListener.get().clear();
		}
		
	}
	
}
