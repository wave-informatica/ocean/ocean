/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.results;

import com.waveinformatica.ocean.core.annotations.ExcludeSerialization;
import com.waveinformatica.ocean.core.util.I18N;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;

/**
 *
 * @author Ivano
 */
public class ValidationResult extends BaseResult {
	
	@Inject
	@ExcludeSerialization
	private I18N i18n;
	
	private final List<String> alerts = new ArrayList<String>();
	private final Map<String, List<String>> fieldErrors = new HashMap<String, List<String>>();
	private final Map<String, List<String>> fieldWarnings = new HashMap<String, List<String>>();
	private boolean success = true;
	
	public void error(String fieldName, String messageKey) {
		List<String> errors = fieldErrors.get(fieldName);
		if (errors == null) {
			errors = new ArrayList<String>();
			fieldErrors.put(fieldName, errors);
		}
		errors.add(i18n.translate(messageKey));
		success = false;
	}
	
	public void warning(String fieldName, String messageKey) {
		List<String> warnings = fieldWarnings.get(fieldName);
		if (warnings == null) {
			warnings = new ArrayList<String>();
			fieldWarnings.put(fieldName, warnings);
		}
		warnings.add(i18n.translate(messageKey));
	}
	
	public void alert(String messageKey) {
		alerts.add(i18n.translate(messageKey));
		success = false;
	}

	public List<String> getAlerts() {
		return alerts;
	}

	public Map<String, List<String>> getFieldErrors() {
		return fieldErrors;
	}

	public Map<String, List<String>> getFieldWarnings() {
		return fieldWarnings;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
	
}
