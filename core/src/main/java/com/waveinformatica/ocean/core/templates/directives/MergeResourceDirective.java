/*
 * Copyright 2017, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.templates.directives;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.annotations.FreemarkerDirective;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.ResourceInfo;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.ModuleEvent;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.html.CSSStyleSheet;
import com.waveinformatica.ocean.core.html.CSSUtils;
import com.waveinformatica.ocean.core.html.XHTMLUtils;
import com.waveinformatica.ocean.core.util.FsUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.ResourceRetriever;
import com.waveinformatica.ocean.core.util.Results;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import se.fishtank.css.selectors.Selectors;
import se.fishtank.css.selectors.dom.W3CNode;

/**
 *
 * @author Ivano
 */
@FreemarkerDirective("resmerge")
public class MergeResourceDirective implements TemplateDirectiveModel {
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private ResourceRetriever retriever;
	
	@Inject
	private Configuration config;
	
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
		
		if (!"true".equals(config.getSiteProperty("resources.merge", "false"))) {
			body.render(env.getOut());
			return;
		}
		
		StringWriter writer = new StringWriter();
		
		body.render(writer);
		
		Document doc = XHTMLUtils.toDocument(writer.toString());
		
		Selectors selectors = new Selectors(new W3CNode(doc));
		
		BaseResult fakeRes = factory.newInstance(BaseResult.class);
		
		List<Node> nodes = selectors.querySelectorAll("script,style,link");
		for (Node n : nodes) {
			Element el = (Element) n;
			if (el.getTagName().equalsIgnoreCase("script")) {
				if (StringUtils.isNotBlank(el.getAttribute("src"))) {
					Results.addScriptSource(fakeRes, el.getAttribute("src"));
				}
				else {
					Results.addScript(fakeRes, el.getFirstChild().getNodeValue());
				}
			}
			else if (el.getTagName().equalsIgnoreCase("style")) {
				Results.addCss(fakeRes, el.getFirstChild().getNodeValue());
			}
			else if (el.getTagName().equalsIgnoreCase("link")) {
				Results.addCssSource(fakeRes, el.getAttribute("href"));
			}
		}
		
		performMerge(fakeRes);
		
		Results.HeadWrappersContainer container = Results.getHeadContainer(fakeRes);
		
		for (Results.HeadPartWrapper r : container.getParts()) {
			if (r.getType() == Results.HeadPartType.CSS_SOURCE) {
				env.getOut().write("<link rel=\"stylesheet\" href=\"" + r.getContent() + "\">");
			}
			else if (r.getType() == Results.HeadPartType.SCRIPT_SOURCE) {
				env.getOut().write("<script type=\"text/javascript\" src=\"" + r.getContent() + "\"></script>");
			}
		}
		
	}
	
	public void performMerge(BaseResult result) {
		
		Results.HeadWrappersContainer container = Results.getHeadContainer(result);
		
		List<ResourceElement> css = new ArrayList<>();
		List<ResourceElement> js = new ArrayList<>();
		
		if (container.getParts().isEmpty()) {
			return;
		}
		
		for (Results.HeadPartWrapper r : container.getParts()) {
			switch (r.getType()) {
				case CSS:
					css.add(new ResourceElement(false, r.getContent()));
					break;
				case CSS_SOURCE:
					css.add(new ResourceElement(true, r.getContent()));
					break;
				case SCRIPT:
					js.add(new ResourceElement(false, r.getContent()));
					break;
				case SCRIPT_SOURCE:
					js.add(new ResourceElement(true, r.getContent()));
					break;
			}
		}
		
		container.getParts().clear();
		
		File cacheDir = new File(config.getConfigDir(), "resmerge");
		if (!cacheDir.exists()) {
			cacheDir.mkdirs();
		}
		
		if (!css.isEmpty()) {
			File cssFile = new File(cacheDir, mergeName(css) + ".css");
			if (!cssFile.exists()) {
				loadContents(css);
				FileOutputStream fos = null;
				try {
					fos = new FileOutputStream(cssFile);
					for (ResourceElement el : css) {
						if (el == null) {
							throw new NullPointerException("el is null");
						}
						else if (el.getContent() == null) {
							throw new NullPointerException("el.getContent() is null for " + el.getName());
						}
						fos.write(el.getContent().getBytes(Charset.forName("UTF-8")));
					}
				} catch (FileNotFoundException ex) {
					LoggerUtils.getLogger(MergeResourceDirective.class).log(Level.SEVERE, null, ex);
				} catch (IOException ex) {
					LoggerUtils.getLogger(MergeResourceDirective.class).log(Level.SEVERE, null, ex);
				} finally {
					if (fos != null) {
						try {
							fos.close();
						} catch (IOException ex) {
							LoggerUtils.getLogger(MergeResourceDirective.class).log(Level.SEVERE, null, ex);
						}
					}
				}
			}

			Results.addCssSource(result, "resmerge/" + cssFile.getName());
		}
		
		if (!js.isEmpty()) {
			File jsFile = new File(cacheDir, mergeName(js) + ".js");
			if (!jsFile.exists()) {
				loadContents(js);
				FileOutputStream fos = null;
				try {
					fos = new FileOutputStream(jsFile);
					for (ResourceElement el : js) {
						String outContent = el.getContent() + "\r\n";
						fos.write(outContent.getBytes(Charset.forName("UTF-8")));
					}
				} catch (FileNotFoundException ex) {
					LoggerUtils.getLogger(MergeResourceDirective.class).log(Level.SEVERE, null, ex);
				} catch (IOException ex) {
					LoggerUtils.getLogger(MergeResourceDirective.class).log(Level.SEVERE, null, ex);
				} finally {
					if (fos != null) {
						try {
							fos.close();
						} catch (IOException ex) {
							LoggerUtils.getLogger(MergeResourceDirective.class).log(Level.SEVERE, null, ex);
						}
					}
				}
			}
			
			Results.addScriptSource(result	, "resmerge/" + jsFile.getName());
		}
		
	}
	
	private void loadContents(List<ResourceElement> elements) {
		
		for (final ResourceElement el : elements) {
			
			if (StringUtils.isBlank(el.getContent())) {
				final ResourceInfo info = retriever.getResource(el.getName());
				if (info != null) {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					byte[] buff = new byte[4096];
					int read;
					InputStream is = info.getInputStream();
					try {
						while ((read = is.read(buff)) > 0) {
							baos.write(buff, 0, read);
						}
						is.close();
						String content = new String(baos.toByteArray(), Charset.forName("UTF-8"));
						if (info.getMimeType() == MimeTypes.CSS) {
							CSSStyleSheet css = new CSSStyleSheet();
							css.parse(el.getName(), content);
							CSSUtils.rewriteUrls(css, new CSSUtils.UrlRewriteCallback() {
								@Override
								public String rewrite(String url) {
									if (url.startsWith("data:")) {
										return url;
									}
									try {
										URL pUrl = new URL(url);
										return pUrl.toExternalForm();
									} catch (MalformedURLException e) {
										if (url.startsWith("/")) {
											return url;
										}
										else {
											return "../" + FsUtils.normalizePath(el.getName().substring(0, el.getName().lastIndexOf('/') + 1) + url);
										}
									}
								}
							});
							content = css.toString();
						}
						el.setContent(content);
					} catch (IOException ex) {
						StringWriter swr = new StringWriter();
						PrintWriter pwr = new PrintWriter(swr);
						ex.printStackTrace(pwr);
						el.setContent("/**\r\n" + swr.toString() + "\r\n*/\r\n");
						LoggerUtils.getLogger(MergeResourceDirective.class).log(Level.SEVERE, null, ex);
					}
				}
				else {
					el.setContent("\r\n/* THE RESOURCE " + el.getName() + " WAS NOT FOUND */\r\n");
				}
			}
			
		}
		
	}
	
	private String mergeName(List<ResourceElement> paths) {
		
		StringBuilder builder = new StringBuilder();
		
		for (ResourceElement p : paths) {
			builder.append("|||###|||");
			builder.append(p.getName());
		}
		
		byte[] bytes = builder.substring(1).getBytes(Charset.forName("UTF-8"));
		
		try {
			
			MessageDigest md = MessageDigest.getInstance("MD5");
			
			byte[] dig = md.digest(bytes);
			
			return toHexString(dig);
			
		} catch (NoSuchAlgorithmException ex) {
			LoggerUtils.getLogger(MergeResourceDirective.class).log(Level.SEVERE, null, ex);
			return null;
		}
		
	}
	
	private static String toHexString(byte[] bytes) {
		StringBuilder hexString = new StringBuilder();
		
		for (int i = 0; i < bytes.length; i++) {
			String hex = Integer.toHexString(0xFF & bytes[i]);
			if (hex.length() == 1) {
				hexString.append('0');
			}
			hexString.append(hex);
		}
		
		return hexString.toString();
	}
	
	public static class ResourceElement {
		
		private final boolean path;
		private final String name;
		private String content;

		public ResourceElement(boolean path, String content) {
			this.path = path;
			if (path) {
				this.name = content;
			}
			else {
				this.name = content;
				this.content = content;
			}
		}
		
		public boolean isPath() {
			return path;
		}

		public String getName() {
			return name;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

	}
	
	@CoreEventListener(eventNames = CoreEventName.MODULE_LOADED)
	public static class Cleaner implements IEventListener<ModuleEvent> {
		
		@Inject
		private Configuration config;
		
		@Override
		public void performAction(ModuleEvent event) {
			
			File dir = new File(config.getConfigDir(), "resmerge");
			if (dir.exists()) {
				File[] files = dir.listFiles();
				if (files != null) {
					for (File f : files) {
						f.delete();
					}
				}
			}
		}
		
	}
	
}
