/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.html;

/**
 *
 * @author Ivano
 */
public class CSSParseException extends RuntimeException {
	
	private int sourceRow = -1;
	private int sourceColumn = -1;
	private int state = -1;

	public CSSParseException() {
	}

	public CSSParseException(String message) {
		super(message);
	}

	public CSSParseException(String message, Throwable cause) {
		super(message, cause);
	}

	public CSSParseException(Throwable cause) {
		super(cause);
	}

	protected CSSParseException(CSSLexer lexer) {
		init(lexer);
	}

	protected CSSParseException(CSSLexer lexer, String message) {
		super(message);
		init(lexer);
	}

	protected CSSParseException(CSSLexer lexer, String message, Throwable cause) {
		super(message, cause);
		init(lexer);
	}

	protected CSSParseException(CSSLexer lexer, Throwable cause) {
		super(cause);
		init(lexer);
	}
	
	protected void init(CSSLexer lexer) {
		this.sourceRow = lexer.getLine() + 1;
		this.sourceColumn = lexer.getColumn() + 1;
		this.state = lexer.yystate();
	}

	@Override
	public String getMessage() {
		StringBuilder builder = new StringBuilder();
		if (state > -1) {
			builder.append("[state: ");
			builder.append(state);
			builder.append("] ");
		}
		builder.append(super.getMessage());
		if (sourceRow >= 0 && sourceColumn >= 0) {
			builder.append(" [line: ");
			builder.append(sourceRow);
			builder.append(", column: ");
			builder.append(sourceColumn);
			builder.append("]");
		}
		return builder.toString();
	}
	
}
