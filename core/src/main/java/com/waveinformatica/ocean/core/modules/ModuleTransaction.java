/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.modules;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.Entity;

import com.waveinformatica.ocean.core.Constants;
import com.waveinformatica.ocean.core.annotations.OceanComponent;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.IService;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.ServiceStatus;
import com.waveinformatica.ocean.core.controllers.events.ModuleEvent;
import com.waveinformatica.ocean.core.exceptions.ModuleException;
import com.waveinformatica.ocean.core.persistence.PersistenceManager;
import com.waveinformatica.ocean.core.util.GlobalClassLoader;
import com.waveinformatica.ocean.core.util.I18N;
import com.waveinformatica.ocean.core.util.LoggerUtils;

public class ModuleTransaction {
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private ComponentsManager manager;

    @Inject
    private PersistenceManager persistenceManager;
    
    @Inject
    private CoreController controller;
    
    @Inject
    private GlobalClassLoader globalClassLoader;
    
    @Inject
    private I18N i18n;
	
	private Set<String> committedComponentsCatalog = new HashSet<>();
	private Set<String> componentsCatalog = new HashSet<String>();
	private Map<Class<? extends Annotation>, Set<Class<?>>> components = new HashMap<>();
	
	@PostConstruct
	public void init() {
		for (Entry<String, List<String>> e : manager.getComponentsCatalog().entrySet()) {
			committedComponentsCatalog.addAll(e.getValue());
		}
	}
	
	/**
	 * This method loads componets catalog for the given module name.
	 * 
	 * @param moduleName the module's name
	 * @param classLoader the module's class loader
	 * @param annotations the found annotated classes
	 * @throws ModuleException if an error occurs
	 */
	@SuppressWarnings("unchecked")
	void loadComponentsCatalog(String moduleName, ClassLoader classLoader, Map<String, Set<String>> annotations) throws ModuleException {
		
		try {
			
			if (annotations.containsKey(OceanComponent.class.getName())) {

				for (String c : annotations.get(OceanComponent.class.getName())) {
					try {
						Class<? extends Annotation> ac = (Class<? extends Annotation>) classLoader.loadClass(c);
						OceanComponent oc = ac.getAnnotation(OceanComponent.class);
						if (oc != null) {
							if (oc.requiresBean() != Object.class && !(classLoader instanceof ModuleClassLoader)) {
								Object bean = factory.getBean(oc.requiresBean());
								if (bean != null) {
									bean.toString();
								} else {
									throw new ModuleException("Unable to load component type " + ac.getName() + " because requires bean " + oc.requiresBean().getName() + " which was not found");
								}
							}
							componentsCatalog.add(c);
						}
					} catch (ClassNotFoundException e) {
						throw new ModuleException("Unable to load class " + c, e);
					} catch (Throwable t) {
						throw new ModuleException("Unable to load catalog for module " + moduleName, t);
					}
				}

			}
			
		} catch (Throwable t) {
			throw new ModuleException("Unexpected error loading components catalog for module " + moduleName, t);
		}

	}

	boolean maybeComponent(String annotationName) {
		
		return committedComponentsCatalog.contains(annotationName) || componentsCatalog.contains(annotationName);
		
	}
	
	void loadClasses(Class<? extends Annotation> annotation, Set<Class<?>> annotatedClasses) {
		Set<Class<?>> classes = components.get(annotation);
		if (classes == null) {
			classes = new HashSet<Class<?>>();
			components.put(annotation, classes);
		}
		classes.addAll(annotatedClasses);
	}
	
	@SuppressWarnings("unchecked")
	void loadComponents(ClassLoader classLoader, Map<String, Set<String>> annotationsMap) throws ModuleException {
		for (Map.Entry<String, Set<String>> entry : annotationsMap.entrySet()) {
			if (!maybeComponent(entry.getKey())) {
				continue;
			}
			Class<? extends Annotation> annotationCls = null;
			try {
				annotationCls = (Class<? extends Annotation>) classLoader.loadClass(entry.getKey());
				if (annotationCls.getAnnotation(OceanComponent.class) == null) {
					continue;
				}
			} catch (Throwable ex) {
				throw new ModuleException(ex);
			}
			Set<Class<?>> classes = new HashSet<Class<?>>();
			for (String name : entry.getValue()) {
				try {
					Class<?> annotatedCls = classLoader.loadClass(name);
					if (annotatedCls.getAnnotation(annotationCls) != null) {
						classes.add(annotatedCls);
					}
				} catch (Throwable ex) {
					throw new ModuleException(ex);
				}
			}
			components.put(annotationCls, classes);
		}
	}
	
	void checkPersistenceAvailability(Map<String, Set<String>> annotatedClasses) throws ModuleException {
        if (annotatedClasses.get(Entity.class.getName()) != null && !annotatedClasses.get(Entity.class.getName()).isEmpty()) {
            if (!persistenceManager.isAvailable()) {
                throw new ModuleException("Required persistence is not available");
            }
        }
	}
	
	void loadPersistenceUnit(Module module) throws ModuleException {
        Map<String, Set<String>> annotatedClasses = module.getModuleClassLoader().getAnnotatedClassesMap();
        
        if (annotatedClasses.get(Entity.class.getName()) != null && !annotatedClasses.get(Entity.class.getName()).isEmpty() && persistenceManager.isAvailable()) {
            module.getLogger().info("Creating persistence unit \"" + module.getName() + "\"...");
            try {
                persistenceManager.loadModulePersistence(module);
            } catch (Throwable e) {
                throw new ModuleException(e);
            }
        }
	}
	
	void loadPersistenceUnit(Map<String, Set<String>> annotatedClasses) throws ModuleException {
        if (annotatedClasses.get(Entity.class.getName()) != null && !annotatedClasses.get(Entity.class.getName()).isEmpty() && persistenceManager.isAvailable()) {
            LoggerUtils.getCoreLogger().info("Creating core persistence unit...");
            try {
                persistenceManager.loadCorePersistence(annotatedClasses);
            } catch (Throwable e) {
                throw new ModuleException(e);
            }
        }
	}
	
	/**
	 * This method will actually load all components into the application like in a transaction
	 * @throws ModuleException 
	 */
	void commit(Module module) throws ModuleException {
		
		manager.loadCatalog(module.getName(), new ArrayList<>(componentsCatalog));
		
		controller.loadComponents(module.getName(), components);
		
		manager.loadComponents(module.getName(), components);
        
        globalClassLoader.addClassLoader(module.getModuleClassLoader());
        
        controller.initializeServices(module.getName());
        
        module.setLoaded(true);
        
		i18n.loadModuleI18N(module);
		
		List<ServiceStatus> modServices = controller.getServices(module.getName());
		for (ServiceStatus ss : modServices) {
			if (ss.getAnnotation().autoStart()) {
				IService srv = controller.getService(ss.getName());
				boolean status = false;
				try {
					status = srv.start();
					if (!status) {
						module.getLogger().warning("Service returned that it was unable to start, but no error was caught.");
					}
				} catch (Throwable e) {
					module.getLogger().log(Level.SEVERE, "Error starting service \"" + ss.getName() + "\"", e);
				}
			}
		}
		
        ModuleEvent modEvent = new ModuleEvent(this, true, module);
		modEvent.setAnnotatedClasses(components);
		controller.dispatchEvent(modEvent);
        
	}
	
	/**
	 * This method will actually load all components into the application like in a transaction
	 * @throws ModuleException 
	 */
	void commit() throws ModuleException {
		
		manager.loadCatalog(Constants.CORE_MODULE_NAME, new ArrayList<>(componentsCatalog));
		
		controller.loadComponents(Constants.CORE_MODULE_NAME, components);
		
		manager.loadComponents(Constants.CORE_MODULE_NAME, components);
        
        controller.initializeServices(Constants.CORE_MODULE_NAME);
		
        List<ServiceStatus> modServices = controller.getServices(Constants.CORE_MODULE_NAME);
		for (ServiceStatus ss : modServices) {
			if (ss.getAnnotation().autoStart()) {
				IService srv = controller.getService(ss.getName());
				boolean status = false;
				try {
					status = srv.start();
					if (!status) {
						LoggerUtils.getCoreLogger().warning("Service returned that it was unable to start, but no error was caught.");
					}
				} catch (Exception e) {
					LoggerUtils.getCoreLogger().log(Level.SEVERE, "Error starting service \"" + ss.getName() + "\"", e);
				}
			}
		}
        
        ModuleEvent modEvent = new ModuleEvent(this, true, Constants.CORE_MODULE_NAME);
		modEvent.setAnnotatedClasses(components);
		controller.dispatchEvent(modEvent);
		
	}

	public Map<Class<? extends Annotation>, Set<Class<?>>> getComponents() {
		return components;
	}
	
}
