/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */
package com.waveinformatica.ocean.core.cdi;

import com.waveinformatica.ocean.core.controllers.*;
import com.waveinformatica.ocean.core.exceptions.BeanInstantiationException;
import com.waveinformatica.ocean.core.modules.ClassInfoCache;
import com.waveinformatica.ocean.core.modules.ModuleClassLoader;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import java.io.Serializable;
import java.lang.annotation.Annotation;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.spi.Annotated;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Qualifier;
import javax.persistence.Transient;

/**
 * This class fills the gap between modules, core and Context Dependency
 * Injection, providing object instances through CDI in module classes too.
 *
 * @author Ivano
 */
@Named
@ApplicationScoped
@Default
public class DefaultObjectFactoryImpl implements ObjectFactory {

	@Inject
	private BeanManager beanManager;

	private Map<Class, BeanFiller> beanFillers = new HashMap<Class, BeanFiller>();

	@Inject
	private ClassInfoCache infoCache;

	/**
	 * Returns a object instance of the specified type <code>T</code>
	 * injecting need beans.
	 *
	 * @param <T> the required type
	 * @param cls the required type class
	 * @return a filled instance of the required type
	 */
	@Override
	public <T> T newInstance(final Class<T> cls) {

		Logger logger = LoggerUtils.getLogger(cls);

		T inst = null;

		try {

			inst = cls.newInstance();

			BeanFiller filler = beanFillers.get(cls);
			if (filler == null) {
				boolean postConstructFound = false;
				filler = new BeanFiller();
				Class inspCls = cls;
				while (inspCls != Object.class) {
					// Inspecting fields
					Field[] fields = inspCls.getDeclaredFields();
					for (Field f : fields) {
						Inject inject = f.getAnnotation(Inject.class);
						if (inject != null) {
							filler.add(f);
						}
					}
					// Inspecting methods
					Method[] methods = inspCls.getDeclaredMethods();
					for (Method m : methods) {
						if (!postConstructFound) {
							PostConstruct a = m.getAnnotation(PostConstruct.class);
							if (a != null) {
								filler.setPostConstruct(m);
								postConstructFound = true;
								break;
							}
						}
					}
					// Going to super class
					inspCls = inspCls.getSuperclass();
				}
				beanFillers.put(cls, filler);
			}
			filler.fill(inst);

		} catch (InstantiationException ex) {
			logger.log(Level.SEVERE, "Error instantiating bean \"" + cls.getName() + "\"", ex);
			throw new BeanInstantiationException("Error instantiating bean \"" + cls.getName() + "\"", ex);
		} catch (IllegalAccessException ex) {
			logger.log(Level.SEVERE, "Error instantiating bean \"" + cls.getName() + "\"", ex);
			throw new BeanInstantiationException("Error instantiating bean \"" + cls.getName() + "\"", ex);
		}

		return inst;

	}

	/**
	 * FOR INTERNAL USE ONLY!
	 *
	 * @param name
	 */
	@Override
	public void moduleUnloaded(String name) {
		Iterator<Class> iter = beanFillers.keySet().iterator();
		while (iter.hasNext()) {
			Class c = iter.next();
			if (c.getClassLoader() instanceof ModuleClassLoader) {
				ModuleClassLoader mcl = (ModuleClassLoader) c.getClassLoader();
				if (mcl.getModule().getName().equals(name)) {
					iter.remove();
				}
			}
		}
	}

	private <T> T getBean(Member member) {
		Class<?> beanClass = null;
		Annotation[] qualifiers = extractQualifiers(member).toArray(new Annotation[0]);

		if (member instanceof Field) {
			beanClass = ((Field) member).getType();
		}

		Bean b = beanManager.resolve(beanManager.getBeans(beanClass, qualifiers));
		if (b == null) {
			return null;
		}
		InjectionPoint ip = new MemberInjectionPoint(member, b, this);
		return (T) beanManager.getInjectableReference(ip, beanManager.createCreationalContext(b));
	}

	@Override
	public Object getBean(Method m, int parameterPosition) {

		Annotation[] qualifiers = filterQualifiers(m.getParameterAnnotations()[parameterPosition]).toArray(new Annotation[0]);

		Bean b = beanManager.resolve(beanManager.getBeans(m.getParameterTypes()[parameterPosition], qualifiers));
		if (b == null) {
			return null;
		}
		InjectionPoint ip = new ParameterInjectionPoint(b, m, parameterPosition, this);
		return beanManager.getInjectableReference(ip, beanManager.createCreationalContext(b));

	}

	@Override
	public <T> T getBean(Class<T> beanClass) {
		Bean b = beanManager.resolve(beanManager.getBeans(beanClass));
		if (b == null) {
			return null;
		}
		InjectionPoint ip = new GenericInjectionPoint(beanClass, b, new Annotation[0], this);
		return (T) beanManager.getInjectableReference(ip, beanManager.createCreationalContext(b));
	}

	private class BeanFiller {

		private final List<Field> fields = new LinkedList<Field>();
		private Method postConstruct = null;

		public void fill(Object o) {

			Logger logger = LoggerUtils.getLogger(o.getClass());

			for (Field f : fields) {
				Object result = getBean(f);
				if (result != null) {
					f.setAccessible(true);
					try {
						f.set(o, result);
					} catch (IllegalArgumentException ex) {
						logger.log(Level.WARNING, null, ex);
					} catch (IllegalAccessException ex) {
						logger.log(Level.WARNING, null, ex);
					}
				}
			}
			// Calling @PostConstruct method if any
			if (postConstruct != null) {
				try {
					postConstruct.invoke(o);
				} catch (IllegalAccessException ex) {
					logger.log(Level.SEVERE, "Error calling @PostConstruct method \"" + postConstruct.getName() + "\" on bean \"" + o.getClass().getName() + "\"", ex);
					throw new BeanInstantiationException("Error calling @PostConstruct method \"" + postConstruct.getName() + "\" on bean \"" + o.getClass().getName() + "\"", ex);
				} catch (IllegalArgumentException ex) {
					logger.log(Level.SEVERE, "Error calling @PostConstruct method \"" + postConstruct.getName() + "\" on bean \"" + o.getClass().getName() + "\"", ex);
					throw new BeanInstantiationException("Error calling @PostConstruct method \"" + postConstruct.getName() + "\" on bean \"" + o.getClass().getName() + "\"", ex);
				} catch (InvocationTargetException ex) {
					logger.log(Level.SEVERE, "Error calling @PostConstruct method \"" + postConstruct.getName() + "\" on bean \"" + o.getClass().getName() + "\"", ex);
					throw new BeanInstantiationException("Error calling @PostConstruct method \"" + postConstruct.getName() + "\" on bean \"" + o.getClass().getName() + "\"", ex);
				}
			}
		}

		public void add(Field f) {
			fields.add(f);
		}

		public void setPostConstruct(Method postConstruct) {
			this.postConstruct = postConstruct;
		}

	}

	public static class MemberInjectionPoint implements InjectionPoint {

		private final Member member;
		private final Bean<?> bean;
		private Set<Annotation> qualifiers;
		private final DefaultObjectFactoryImpl factory;

		public MemberInjectionPoint(Member member, Bean<?> bean, DefaultObjectFactoryImpl factory) {
			this.member = member;
			this.bean = bean;
			this.factory = factory;
		}

		@Override
		public Type getType() {
			if (member instanceof Field) {
				return ((Field) member).getType();
			}
			return null;
		}

		@Override
		public Set<Annotation> getQualifiers() {
			if (qualifiers == null) {
				qualifiers = factory.extractQualifiers(member);
			}
			return qualifiers;
		}

		@Override
		public Bean<?> getBean() {
			return bean;
		}

		@Override
		public Member getMember() {
			return member;
		}

		@Override
		public Annotated getAnnotated() {
			if (member instanceof Field) {
				final Class<?> declaringClass = member.getDeclaringClass();
				return (OceanAnnotatedField) factory.infoCache.get(declaringClass, declaringClass.getName() + "." + member.getName() + "[annotated]", new ClassInfoCache.Factory() {
					@Override
					public Object create() {
						OceanAnnotatedType type = (OceanAnnotatedType) factory.infoCache.get(declaringClass, declaringClass.getName() + "[annotated]", new ClassInfoCache.Factory() {
							@Override
							public Object create() {
								return new OceanAnnotatedType(declaringClass);
							}
						});
						return new OceanAnnotatedField(type, (Field) member);
					}
				});
			}
			return null;
		}

		@Override
		public boolean isDelegate() {
			return false;
		}

		@Override
		public boolean isTransient() {
			if (member instanceof Field) {
				return ((Field) member).getAnnotation(Transient.class) != null;
			}
			return false;
		}

	}

	public static class ParameterInjectionPoint implements InjectionPoint {

		private final Class<?> beanClass;
		private final Bean<?> bean;
		private final Set<Annotation> qualifiers;
		private final Method method;
		private final int position;
		private final DefaultObjectFactoryImpl factory;
		private final OceanAnnotatedMethod annotatedMethod;

		public ParameterInjectionPoint(Bean<?> bean, Method method, int position, DefaultObjectFactoryImpl factory) {
			this.beanClass = method.getParameterTypes()[position];
			this.bean = bean;
			this.qualifiers = factory.filterQualifiers(method.getParameterAnnotations()[position]);
			this.method = method;
			this.position = position;
			this.factory = factory;
			this.annotatedMethod = new OceanAnnotatedMethod(new OceanAnnotatedType(beanClass), method);
		}

		@Override
		public Type getType() {
			return beanClass;
		}

		@Override
		public Set<Annotation> getQualifiers() {
			return qualifiers;
		}

		@Override
		public Bean<?> getBean() {
			return bean;
		}

		@Override
		public Member getMember() {
			return method;
		}

		@Override
		public Annotated getAnnotated() {
			return (OceanAnnotatedParameter) annotatedMethod.getParameters().get(position);
		}

		@Override
		public boolean isDelegate() {
			return false;
		}

		@Override
		public boolean isTransient() {
			return !Serializable.class.isAssignableFrom(beanClass);
		}

	}

	public static class GenericInjectionPoint implements InjectionPoint {

		private final Class<?> beanClass;
		private final Bean<?> bean;
		private final Set<Annotation> qualifiers;
		private final DefaultObjectFactoryImpl factory;

		public GenericInjectionPoint(Class<?> beanClass, Bean<?> bean, Annotation[] qualifiers, DefaultObjectFactoryImpl factory) {
			this.beanClass = beanClass;
			this.bean = bean;
			this.qualifiers = factory.filterQualifiers(qualifiers);
			this.factory = factory;
		}

		@Override
		public Type getType() {
			return beanClass;
		}

		@Override
		public Set<Annotation> getQualifiers() {
			return qualifiers;
		}

		@Override
		public Bean<?> getBean() {
			return bean;
		}

		@Override
		public Member getMember() {
			return null;
		}

		@Override
		public Annotated getAnnotated() {
			return null;
		}

		@Override
		public boolean isDelegate() {
			return false;
		}

		@Override
		public boolean isTransient() {
			return !Serializable.class.isAssignableFrom(beanClass);
		}

	}

	@SuppressWarnings("unchecked")
	private Set<Annotation> extractQualifiers(final Member member) {

		Set<Annotation> qualifiers = (Set<Annotation>) infoCache.get(member.getDeclaringClass(), member.getDeclaringClass().getName() + "." + member.getName() + "[qualifiers]", new ClassInfoCache.Factory() {

			@Override
			public Object create() {
				Annotation[] annotations = null;

				if (member instanceof Field) {
					annotations = ((Field) member).getAnnotations();
				}

				return filterQualifiers(annotations);
			}

		});

		return qualifiers;
	}

	private Set<Annotation> filterQualifiers(Annotation[] annotations) {
		Set<Annotation> qualifiers = new HashSet<Annotation>();
		if (annotations != null) {
			for (Annotation a : annotations) {
				if (a.annotationType().getAnnotation(Qualifier.class) != null) {
					qualifiers.add(a);
				}
			}
		}
		return qualifiers;
	}

}
