/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.results;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.annotations.ExcludeSerialization;
import com.waveinformatica.ocean.core.annotations.Filterable;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.decorators.ExcelTableDecorator;
import com.waveinformatica.ocean.core.controllers.dto.Align;
import com.waveinformatica.ocean.core.controllers.events.BeforeOperationEvent;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.Event;
import com.waveinformatica.ocean.core.controllers.results.input.Range;
import com.waveinformatica.ocean.core.controllers.scopes.ScopeChain;
import com.waveinformatica.ocean.core.controllers.scopes.ScopesController;
import com.waveinformatica.ocean.core.filtering.Filter;
import com.waveinformatica.ocean.core.filtering.FilterProvider;
import com.waveinformatica.ocean.core.modules.ClassInfoCache;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.I18N;
import com.waveinformatica.ocean.core.util.OceanConversation;
import com.waveinformatica.ocean.core.util.Results;
import com.waveinformatica.ocean.core.util.UrlBuilder;

/**
 * @author Ivano Culmine
 * @author Marco Merli
 * @author Giuseppe Avola
 */
public class TableResult extends BaseResult implements FilterProvider {

	@Inject
	@ExcludeSerialization
	private I18N i18n;

	@Inject
	@ExcludeSerialization
	private com.waveinformatica.ocean.core.security.SecurityManager securityManager;

	@Inject
	@ExcludeSerialization
	private CoreController coreController;

	@Inject
	@ExcludeSerialization
	private ClassInfoCache classInfoCache;

	@Inject
	@ExcludeSerialization
	private ObjectFactory factory;
	
	@Inject
	@ExcludeSerialization
	private ScopesController scopesController;
	
	@Inject
	@ExcludeSerialization
	private OceanConversation conversation;

	// State variables
	@ExcludeSerialization
	private boolean analyzedHeader = false;
	@ExcludeSerialization
	private boolean analyzedData = false;
	@ExcludeSerialization
	private Integer page = 1;
	@ExcludeSerialization
	private boolean hasFilters = false;

	// Configuration
	@ExcludeSerialization
	private boolean pagedData = false;
	@ExcludeSerialization
	private PaginationData<?> paginatedDataSource = null;
	@ExcludeSerialization
	private Object tableData;
	@ExcludeSerialization
	private boolean checkable = false;
	@ExcludeSerialization
	private Type listType;
	@ExcludeSerialization
	private String pathBase;
	@ExcludeSerialization
	private Integer rowsPerPage = 20;
	@ExcludeSerialization
	private Integer rowsBufferSize = 1000;
	@ExcludeSerialization
	private Integer pagesPerBox = 10;
	@ExcludeSerialization
	private final List<TableOperation> tableOperations = new LinkedList<TableOperation>();
	@ExcludeSerialization
	private final List<TableOperation> rowOperations = new LinkedList<TableOperation>();
	@ExcludeSerialization
	private TableDataAnalyzer analyzer = new TableDataAnalyzer();
	@ExcludeSerialization
	private TableResultConfig dataConfiguration = null;
	@ExcludeSerialization
	private final EnumMap<MimeTypes, Class<? extends WrapperResultBuilder>> wrapperResultBuilders = new EnumMap<MimeTypes, Class<? extends WrapperResultBuilder>>(MimeTypes.class);
	@ExcludeSerialization
	private final List<String> selectedRows = new ArrayList<String>();
	@ExcludeSerialization
	private Align alignRowOperations = Align.RIGHT;

	private Integer maxOperations;
	private String maxOperationsLabel;

	// Analyzed data
	@ExcludeSerialization
	private final TableHeader header = new TableHeader();
	private final List<TableRow> rows = new LinkedList<TableRow>();
	private List<String> keys = new LinkedList<String>();
	@ExcludeSerialization
	private RowHandler rowHandler = new BaseRowHandler();
	@ExcludeSerialization
	private int rowIndex = 0;
	private Long totalRowsCount = null;

	public TableResult() {
		super();

		// Initializing mime type wrapper result builders
		wrapperResultBuilders.put(MimeTypes.HTML, WebResultBuilder.class);
		wrapperResultBuilders.put(MimeTypes.PDF, WebResultBuilder.class);
	}

	/**
	 * Normally invoked by the decorator. This method starts the data analysis
	 * invoking the currently set {@link TableDataAnalyzer} and producing
	 * analyzed data for the table.
	 */
	public void analyzeData() {
		analyzeData(false);
	}

	public void analyzeData(boolean headerOnly) {

		if (analyzedData) {
			return;
		}

		// Farlo prima perche' l'analyzer puo' accedere a metodi che potrebbero richiamare nuovamente l'analisi dei dati
		analyzedData = !headerOnly;

		analyzer.analyze(this, tableData, headerOnly);
	}

	/**
	 * Returns the original table data as set from any of the {@code setTableData}
	 * methods.
	 *
	 * @return
	 */
	public Object getTableData() {
		return tableData;
	}

	public static String getPageParameter() {
		return "fw:page";
	}

	/**
	 * Sets data for the table. It's an improvement of the
	 * <code>setTableData(Object tableData)</code> version, it supports
	 * {@link Collection}s only and the listType parameter represents the
	 * {@link Collection}'s generic type. It does not need an anonymous class.
	 *
	 * @param <T> the Collection's generic type
	 * @param tableData the collection for the table's data
	 * @param listType the Collection's generic type
	 */
	public <T> void setTableData(Collection<T> tableData, Class<T> listType) {
		this.tableData = tableData;
		this.listType = listType;
		this.totalRowsCount = (long) tableData.size();
	}

	/**
	 * Sets data for the table. It supports {@link Map}s and
	 * {@link Collection}s. In case of {@link Collection}s, the object MUST be
	 * instantiated as
	 * <code>new CollectionImplementationClass(...)<strong>{}</strong></code>
	 * (please note the {}: it is mandatory for the <code>TableResult</code>
	 * to find the collection's generic type).
	 *
	 * @param tableData the table data
	 */
	public void setTableData(Object tableData) {
		this.tableData = tableData;

		if (Map.class.isAssignableFrom(tableData.getClass())) {
			totalRowsCount = (long) ((Map<?, ?>) tableData).size();
		} else if (List.class.isAssignableFrom(tableData.getClass())) {
			totalRowsCount = (long) ((List<?>) tableData).size();
		}
	}

	@Override
	public String getFilterValue(Field field) {
		String name = null;
		TableResultColumn colConfig = field.getAnnotation(TableResultColumn.class);
		if (colConfig != null) {
			name = colConfig.name();
		}
		if (StringUtils.isBlank(name)) {
			name = field.getName();
		}
		HttpServletRequest request = Context.get().getRequest();
		if (request != null) {
			String[] values = request.getParameterValues("fw:filter:" + name);
			String key = conversation.key(this, "filter:" + name);
			if (values == null) {
				values = (String[]) conversation.get(key);
			}
			if (values != null) {
				conversation.put(key, values);
				return StringUtils.join(values, ",");
			}
		}
		return null;
	}
	
	public String getFullOperation(String op) {
		UrlBuilder url = new UrlBuilder(op);
		if (!conversation.isActive()) {
			url.getParameters().remove("cid");
			return url.getUrl();
		}
		url.getParameters().replace("cid", conversation.getId());
		return url.getUrl();
	}

	public void rebuildKeys() {
		keys.clear();

		for (TableColumn column : getHeader().getColumns()) {
			if (column.isKey()) {
				keys.add(column.getName());
			}
		}
	}

	public boolean isHasFilters() {
		return hasFilters;
	}
	public void disableFilters() {
		hasFilters = false;
	}

    //ELM 02/07/2018 ADDED FUNCTION
    public void enableFilters() {
        hasFilters = true;
    }

	public interface PaginationCount {

		Long getCount();
	}

	public interface PaginationData<T> {

		Collection<T> getData(Range<Long> bound);
	}

	public <T> void setTableData(Class<T> listType, PaginationCount count, PaginationData<T> data, HttpServletRequest request) {

		this.pagedData = true;
		this.totalRowsCount = count.getCount();
		
		Integer sp = (Integer) conversation.get(conversation.key(this, "page"));
		if (sp != null) {
			page = sp;
		}
		
		Integer currPage = page - 1;
		if (request != null) {
			try {
				String fwPage = request.getParameter(TableResult.getPageParameter());
				if (StringUtils.isNotBlank(fwPage)) {
					currPage = Integer.valueOf(fwPage) - 1;
				}
			} catch (NumberFormatException e) {
	
			}
		}

		Long from = (long) (currPage * this.rowsPerPage);
		Long to = from + this.rowsPerPage - 1;

		this.tableData = data.getData(new Range<Long>(from, to));
		this.listType = listType;
		setPage(currPage + 1);

		paginatedDataSource = data;
	}

	/**
	 * Makes the paginated table data to advance by page.
	 *
	 * @return <code>true</code> if more data is available
	 */
	public boolean nextPage() {

		if (!this.pagedData) {
			return false;
		}

		if (rowsPerPage == null || this.page >= Math.ceil(((double) getTotalRowsCount()) / ((double) rowsPerPage))) {
			return false;
		}

		this.page++;

		Long from = (long) ((this.page - 1) * this.rowsPerPage);
		Long to = from + this.rowsPerPage - 1;

		this.tableData = paginatedDataSource.getData(new Range<Long>(from, to));

		this.analyzedData = false;
		this.rows.clear();

		return true;

	}

	/**
	 * Returns current number of rows per page
	 *
	 * @return current number of rows per page
	 */
	public Integer getRowsPerPage() {
		return rowsPerPage;
	}

	/**
	 * Sets the new number of rows per page (for paginating results) or
	 * <code>null</code> to disable results pagination
	 *
	 * @param rowsPerPage the new number of rows per page
	 * @throws IllegalStateException if the paginated version of
	 * {@link setTableData} has been used
	 */
	public void setRowsPerPage(Integer rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}
	
	/**
	 * Returns the rows per page for non web decorators (i.e. {@link ExcelTableDecorator}. It's default value is 1000.
	 * 
	 * @return the rows per page for non web decorators.
	 */
	public Integer getRowsBufferSize() {
		return rowsBufferSize;
	}

	/**
	 * Sets the rows per page for non web decorators (i.e. {@link ExcelTableDecorator}. It's default value is 1000.
	 * 
	 * @param rowsBufferSize the rows per page for non web decorators.
	 */
	public void setRowsBufferSize(Integer rowsBufferSize) {
		this.rowsBufferSize = rowsBufferSize;
	}

	/**
	 * Returns analyzed table rows. It invokes {@link analyzeData} if data has
	 * not been analyzed already.
	 *
	 * @return analyzed rows
	 */
	private List<TableRow> getRows() {
		if (!analyzedData) {
			analyzeData();
		}
		return rows;
	}

	/**
	 * Returns the same result as {@link #getRows()}, but handles with
	 * {@link RowHandler} if necessary. This method can be used generally for
	 * presentation purposes.
	 *
	 * @return the handled rows
	 */
	public List<TableRow> getAllRows() {

		for (TableRow tr : getRows()) {
			if (!tr.isHandled()) {
				getRowHandler().handleRow(header, tr);
				tr.setHandled(true);
			}
		}

		return rows;
	}

	/**
	 * Returns the handled rows for the current page or all the handled rows
	 * if pagination is disabled. Use this method for presentation purposes.
	 *
	 * @return current page's handled rows
	 */
	public List<TableRow> getPageRows() {
		if (pagedData || rowsPerPage == null || page == null || page < 1 || page > getPagesCount()) {
			return getAllRows();
		}
		int max = Math.min(page * rowsPerPage, rows.size());
		List<TableRow> pageRows = rows.subList((page - 1) * rowsPerPage, max);
		for (TableRow tr : pageRows) {
			if (!tr.isHandled()) {
				getRowHandler().handleRow(header, tr);
				tr.setHandled(true);
			}
		}
		return pageRows;
	}

	/**
	 * Returns the current page index
	 *
	 * @return the current page index
	 */
	public Integer getPage() {
		if (page == null || page <= 1) {
			Integer sp = (Integer) conversation.get(conversation.key(this, "page"));
			if (sp != null) {
				page = sp;
			}
		}
		return Math.max(Math.min(page, getPagesCount()), 1);
	}

	public Long getTotalRowsCount() {
		return totalRowsCount != null ? totalRowsCount : rows.size();
	}

	/**
	 * Returns the total number of pages
	 *
	 * @return the total number of pages
	 */
	public Integer getPagesCount() {
		if (rowsPerPage == null) {
			return 1;
		}
		long size = rows.size();
		if (totalRowsCount != null) {
			size = totalRowsCount;
		}
		return ((Double) Math.ceil(size / rowsPerPage.doubleValue())).intValue();
	}

	/**
	 * Sets the current current page index
	 *
	 * @param page the new page index
	 * @throws IllegalStateException if the paginated version of
	 * {@link setTableData} has been used
	 */
	public void setPage(Integer page) {
		conversation.put(conversation.key(this, "page"), page);
		this.page = page;
	}

	public String getPathBase() {
		return pathBase;
	}

	public void setPathBase(String pathBase) {
		this.pathBase = pathBase;
	}

	public List<TableOperation> getTableOperations() {
		return tableOperations;
	}

	public List<TableOperation> getRowOperations() {
		return rowOperations;
	}

	public List<TableOperation> getHandledRowOperations() {
		if (!analyzedData) {
			analyzeData();
		}

		List<TableOperation> operations = null;

		if (pagedData) {
			operations = rowHandler.handleRowActions(rowOperations, rows.get(rowIndex++));
		} else {
			operations = rowHandler.handleRowActions(rowOperations, rows.get((getPage() - 1) * (rowsPerPage != null ? rowsPerPage : 1) + (rowIndex++)));
		}

		return operations;
	}

	public TableDataAnalyzer getAnalyzer() {
		return analyzer;
	}

	public void setAnalyzer(TableDataAnalyzer analyzer) {
		this.analyzer = analyzer;
	}

	public void addTableOperation(String name, String title) {

		String tmpName = name;
		if (!tmpName.startsWith("/")) {
			tmpName = "/" + tmpName;
		}
		
		ScopeChain chain = scopesController.build(tmpName);
		
		if (!chain.isEmpty() && !chain.getCurrent().isAuthorized()) {
			return;
		}

		TableOperation op = new TableOperation(this);
		op.setName(name);
		op.setTitle(title);
		tableOperations.add(op);
	}

	public void addRowOperation(String name, String title) {

		String tmpName = name;
		if (!tmpName.startsWith("/")) {
			tmpName = "/" + tmpName;
		}
		
		ScopeChain chain = scopesController.build(tmpName);
		
		if (!chain.isEmpty() && !chain.getCurrent().isAuthorized()) {
			return;
		}

		TableOperation op = new TableOperation(this);
		op.setName(name);
		op.setTitle(title);
		rowOperations.add(op);
	}

	public Align getAlignRowOperations() {
		return alignRowOperations;
	}

	public void setAlignRowOperations(Align alignRowOperations) {
		if (alignRowOperations == Align.LEFT || alignRowOperations == Align.RIGHT) {
			this.alignRowOperations = alignRowOperations;
		}
	}

	public Integer getMaxOperations() {
		return maxOperations;
	}

	public void setMaxOperations(Integer maxOperations) {
		this.maxOperations = maxOperations;
	}

	public String getMaxOperationsLabel() {
		return maxOperationsLabel;
	}

	public void setMaxOperationsLabel(String maxOperationsLabel) {
		this.maxOperationsLabel = maxOperationsLabel;
	}

	/**
	 * Returns the analyzed table header
	 *
	 * @return the analyzed table header
	 */
	public TableHeader getHeader() {
		if (!analyzedHeader) {
			analyzeData(true);
		}
		return header;
	}

	/**
	 * This is a shortcut to call
	 * <code>{@link TableResult}.getHeader().{@link TableHeader#get(String) get(name)}</code>.
	 *
	 * @param name
	 * @return
	 */
	public TableColumn getColumn(String name) {
		if (!analyzedData) {
			analyzeData(true);
		}
		return header.get(name);
	}

	public boolean isCheckable() {
		return checkable;
	}

	/**
	 * Set this property to <code>true</code> to enable multiple selection of
	 * the table's rows. This will change the table operations behaviour.
	 *
	 * @param checkable
	 */
	public void setCheckable(boolean checkable) {
		this.checkable = checkable;
	}

	public String getKey(TableRow row) {
		if (keys.isEmpty()) {
			return "";
		}
		StringBuilder query = new StringBuilder();
		for (String k : keys) {
			if (query.length() > 0) {
				query.append(';');
			}
			for (TableCell c : row.getCells()) {
				if (c.getName().equals(k)) {
					query.append(c.getValue());
					break;
				}
			}
		}
		return query.toString();
	}

	public TableResultConfig getDataConfiguration() {
		return dataConfiguration;
	}

	/**
	 * To enable extra features (columns sorting, etc.), set this property
	 * from the {@link TableResultConfig} object passed as parameter to the
	 * producing operation or build a custom object accordingly.
	 *
	 * @param dataConfiguration
	 */
	public void setDataConfiguration(TableResultConfig dataConfiguration) {
		this.dataConfiguration = dataConfiguration;
	}

	public Integer getPagesPerBox() {
		return pagesPerBox;
	}

	public void setPagesPerBox(Integer pagesPerBox) {
		this.pagesPerBox = pagesPerBox;
	}

	public EnumMap<MimeTypes, Class<? extends WrapperResultBuilder>> getWrapperResultBuilders() {
		return wrapperResultBuilders;
	}

	public List<String> getSelectedRows() {
		return selectedRows;
	}

	public static class TableCell {

		@ExcludeSerialization
		private Class<?> fieldType;
		private String name;
		private String value;
		@ExcludeSerialization
		private Object originalValue;
		@ExcludeSerialization
		private final List<String> comments = new ArrayList<String>();

		public Class<?> getFieldType() {
			return fieldType;
		}

		public void setFieldType(Class<?> fieldType) {
			this.fieldType = fieldType;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public Object getOriginalValue() {
			return originalValue;
		}

		public void setOriginalValue(Object originalValue) {
			this.originalValue = originalValue;
		}

		public List<String> getComments() {
			return comments;
		}

	}

	public static class TableOperation {

		private final TableResult tableResult;
		private String title;
		private String name;
		private boolean enabled = true;

		public TableOperation(TableResult tableResult) {
			this.tableResult = tableResult;
		}

		public TableOperation(TableOperation tableOperation) {
			tableResult = tableOperation.tableResult;
			name = tableOperation.name;
			title = tableOperation.title;
			enabled = tableOperation.enabled;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getQuery(TableRow row) {
			if (tableResult.keys.isEmpty()) {
				return "";
			}
			StringBuilder query = new StringBuilder();
			for (String k : tableResult.keys) {
				try {
					StringBuilder param = new StringBuilder();
					param.append(URLEncoder.encode(k, "UTF-8"));
					param.append('=');
					for (TableCell c : row.getCells()) {
						if (c.getName().equals(k)) {
							param.append(URLEncoder.encode(c.getValue(), "UTF-8"));
							break;
						}
					}
					if (query.length() > 0) {
						query.append('&');
					}
					query.append(param.toString());
				} catch (UnsupportedEncodingException ex) {
					Logger.getLogger(TableResult.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
			
			if (tableResult.conversation.isActive()) {
				query.append("&cid=");
				query.append(tableResult.conversation.getId());
			}

			if (name.indexOf('?') != -1) {
				return "&" + query.toString();
			} else {
				return "?" + query.toString();
			}
		}

		public boolean isEnabled() {
			return enabled;
		}

		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}

	}

	public List<String> getKeys() {
		return keys;
	}

	public RowHandler getRowHandler() {
		return rowHandler;
	}

	public void setRowHandler(RowHandler rowHandler) {
		this.rowHandler = rowHandler;
	}

	/**
	 * FOR INTERNAL USE BY THE TABLE'S TEMPLATE
	 *
	 * @return
	 */
	public I18N getI18n() {
		return i18n;
	}

	/**
	 * If the table is built from a {@link Collection}, this method should
	 * return its generic type
	 *
	 * @return the collection's generic type if available
	 */
	public Type getListType() {
		return listType;
	}

	public void setListType(Type listType) {
		this.listType = listType;
	}

	public static interface RowHandler {

		public void handleRow(TableHeader header, TableRow rowSource);

		public List<TableOperation> handleRowActions(List<TableOperation> operations, TableRow rowSource);

	}

	public static class BaseRowHandler implements RowHandler {

		@Override
                public void handleRow(TableHeader header, TableRow row) {
                    int index = 0;
                    for (TableCell tc : row.getCells()) {
                        if (tc.getFieldType() == null) {
                            index++;
                            continue;
                        }
                        if (tc.getValue() != null && Boolean.class.isAssignableFrom(tc.getFieldType())
                                || (tc.getFieldType().isPrimitive() && tc.getFieldType() == Boolean.TYPE)) {
                            try {
                                boolean val = Boolean.parseBoolean(tc.getValue());
                                if (val) {
                                    tc.setValue("<img src=\"ris/images/icons/table_ok.png\">");
                                } else {
                                    tc.setValue("<img src=\"ris/images/icons/table_ko.png\">");
                                }
                            } catch (Exception e) {
                            }
                        } else if (StringUtils.isNotBlank(tc.getValue())
                                && !(header.getColumns().get(index).getSerializer() instanceof UnescapedCellSerializer)) {
                            tc.setValue(StringEscapeUtils.escapeHtml(tc.getValue()));
                        }
                        index++;
                    }
                }
		@Override
		public List<TableOperation> handleRowActions(List<TableOperation> operations, TableRow rowSource) {
			return operations;
		}

	}

	public static class TableRow {

		@ExcludeSerialization
		private boolean handled = false;
		@ExcludeSerialization
		private final Object source;
		private final List<TableCell> cells = new ArrayList<TableCell>();
		private final Set<String> classes = new HashSet<String>();

		public TableRow(Object source) {
			this.source = source;
		}

		/**
		 * Retrieves the original object which produced the current row (if
		 * available)
		 *
		 * @return the source object for the row
		 */
		public Object getSource() {
			return source;
		}

		public List<TableCell> getCells() {
			return cells;
		}

		/**
		 * Returns <code>true</code> if the row has already been handled by
		 * a {@link RowHandler} and should not be handled again, for
		 * performance.
		 *
		 * @return <code>true</code> if the row has already been handled
		 */
		public boolean isHandled() {
			return handled;
		}

		/**
		 * A {@link RowHandler} should set this property to
		 * <code>true</code> when handled
		 *
		 * @param handled
		 */
		public void setHandled(boolean handled) {
			this.handled = handled;
		}

		public Set<String> getClasses() {
			return classes;
		}

	}

	public static class TableHeader {

		private final List<TableColumn> columns = new LinkedList<TableColumn>();
		private final Map<String, TableColumn> columnsMap = new HashMap<String, TableColumn>();

		public void add(TableColumn col) {
			columns.add(col);
			columnsMap.put(col.getName(), col);
		}

		public void addAll(Collection<? extends TableColumn> cols) {
			columns.addAll(cols);
			for (TableColumn col : cols) {
				columnsMap.put(col.getName(), col);
			}
		}

		public TableColumn get(String name) {
			return columnsMap.get(name);
		}

		public List<TableColumn> getColumns() {
			return columns;
		}

		/**
		 * Rebuilds table's columns using the order specified by the
		 * parameters list
		 *
		 * @param colsName column names to rebuild header (oredered as
		 * specified)
		 * @return this TableHeader to allow methods chaining
		 */
                //ELM 02/07/2018 This function is not splitted in two functions to permit also the ignore of col. not found.
                public TableHeader selectColumns(String... colsName) {
                    return selectColumns(true, colsName);
                }

                public TableHeader selectColumns(boolean throwsNotFoundColumn, String... colsName) {
                    columns.clear();
                    for (String cN : colsName) {
                        TableColumn tC = columnsMap.get(cN);
                        if (tC == null && throwsNotFoundColumn) {
                            throw new IllegalArgumentException("No such column: " + cN);
                        }
                        columns.add(tC);
                    }
                    return this;
                }

                //ELM 02/07/2018 This function is not splitted in two functions to permit also the ignore of col. not found.
                public TableHeader selectColumns(TableColumn... theColumns) {
                    return selectColumns(true, theColumns);
                }

                public TableHeader selectColumns(boolean throwsNotFoundColumn, TableColumn... theColumns) {
                    columns.clear();
                    for (TableColumn tC : theColumns) {
                        TableColumn tCMap = columnsMap.get(tC.name);
                        if (tCMap == null && throwsNotFoundColumn) {
                            throw new IllegalArgumentException("No such column: " + tC.name);
                        }
                        columns.add(tC);
                    }
                    return this;
                }
	}

	public static class TableColumn {

		private final String name;
		private String label;
		private boolean hidden;
		private boolean key;
		private boolean sortable;
		private CellSerializer serializer;
		private TableResultColumn configuration;
		private Field sourceField;
		private String editOperation;
		private String linkOperation;
		private String format;
		private CellEditType editType = CellEditType.NONE;
		private String editSource;
		private CellLinkType linkType = CellLinkType.NONE;
		private String template;
		private Filter filter;

		public TableColumn(String name, String label) {
			this.name = name;
			this.label = label;
		}

		public String formatLinkOperation(String value) {

			if (value == null) {
				return linkOperation;
			}

			return String.format(linkOperation, value);
		}

		public String getName() {
			return name;
		}

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public boolean isHidden() {
			return hidden;
		}

		public boolean isKey() {
			return key;
		}

		public TableColumn setHidden(boolean hidden) {
			this.hidden = hidden;
			return this;
		}

		public TableColumn setKey(boolean key) {
			this.key = key;
			return this;
		}

		public boolean isSortable() {
			return sortable;
		}

		public TableColumn setSortable(boolean sortable) {
			this.sortable = sortable;
			return this;
		}

		public CellSerializer getSerializer() {
			return serializer;
		}

		public TableColumn setSerializer(CellSerializer serializer) {
			this.serializer = serializer;
			return this;
		}

		public TableResultColumn getConfiguration() {
			return configuration;
		}

		public TableColumn setConfiguration(TableResultColumn configuration) {
			this.configuration = configuration;
			return this;
		}

		public Field getSourceField() {
			return sourceField;
		}

		public TableColumn setSourceField(Field sourceField) {
			this.sourceField = sourceField;
			return this;
		}

		public String getEditOperation() {
			return editOperation;
		}

		public TableColumn setEditOperation(String editOperation) {
			this.editOperation = editOperation;
			return this;
		}

		public String getLinkOperation() {
			return formatLinkOperation(null);
		}

		public TableColumn setLinkOperation(String linkOperation) {
			this.linkOperation = linkOperation;
			return this;
		}

		public CellEditType getEditType() {
			return editType;
		}

		public CellLinkType getLinkType() {
			return linkType;
		}

		public TableColumn setEditType(CellEditType editType) {
			this.editType = editType;
			return this;
		}

		public TableColumn setLinkType(CellLinkType linkType) {
			this.linkType = linkType;
			return this;
		}

		public TableColumn setEditable(CellEditType editType, String editOperation) {
			return setEditType(editType).setEditOperation(editOperation);
		}

		public String getEditSource() {
			return editSource;
		}

		public TableColumn setEditSource(String editSource) {
			this.editSource = editSource;
			return this;
		}

		public String getFormat() {
			return format;
		}

		public TableColumn setFormat(String format) {
			this.format = format;
			return this;
		}

		public String getTemplate() {
			return template;
		}

		public TableColumn setTemplate(String template) {
			this.template = template;
			return this;
		}

		public Filter getFilter() {
			return filter;
		}

		public TableColumn setFilter(Filter filter) {
			this.filter = filter;
			return this;
		}

	}

	public static class TableDataAnalyzer {

		/**
		 * Analyzes table's header only. Same as {@link analyze} method when
		 * <code>headerOnly</code> is <code>true</code>.
		 *
		 * @param result the {@link TableResult} object
		 * @param tableData the data set via one of {@link TableResult#setTableData(Object) setTableData(Object)}, {@link TableResult#setTableData(Collection, Class) setTableData(Collection, Class)} or {@link TableResult#setTableData(Class, PaginationCount, PaginationData, HttpServletRequest) setTableData(Class, PaginationCount, PaginationData, HttpServletRequest)} methods
		 */
		public void analyzeHeader(TableResult result, Object tableData) {

			if (tableData instanceof Collection) {
				analyzeCollection(result, tableData, true);
			} else if (tableData instanceof Map) {
				analyzeMap(result, tableData, true);
			}

		}

		/**
		 * Analyzes table's header and data (only if <code>headerOnly</code>
		 * is <code>false</code>).
		 *
		 * @param result the {@link TableResult} object
		 * @param tableData the data set via one of {@link TableResult#setTableData(Object) setTableData(Object)}, {@link TableResult#setTableData(Collection, Class) setTableData(Collection, Class)} or {@link TableResult#setTableData(Class, PaginationCount, PaginationData, HttpServletRequest) setTableData(Class, PaginationCount, PaginationData, HttpServletRequest)} methods
		 * @param headerOnly if <code>true</code> table's data is not
		 * analyzed
		 */
		public void analyze(TableResult result, Object tableData, boolean headerOnly) {

			if (tableData instanceof Collection) {
				analyzeCollection(result, tableData, headerOnly);
			} else if (tableData instanceof Map) {
				analyzeMap(result, tableData, headerOnly);
			}

		}

		protected void analyzeCollection(TableResult result, Object tableData, boolean headerOnly) {
			if (!result.analyzedHeader) {
				if (result.getListType() == null) {
					analyzeListType(result, tableData);
				}

				buildTableHeader(result, tableData);
				result.analyzedHeader = true;
			}
			if (!headerOnly) {
				Collection<?> c = (Collection<?>) tableData;
				buildTableData(result, c);
			}
		}

		protected void analyzeMap(TableResult result, Object tableData, boolean headerOnly) {
			if (!result.analyzedHeader) {
				result.header.add(new TableColumn("key", result.getI18n().translate(TableResult.class.getName() + ".key")));
				result.header.add(new TableColumn("value", result.getI18n().translate(TableResult.class.getName() + ".value")));
				result.analyzedHeader = true;
			}
			if (!headerOnly) {
				for (Map.Entry entry : (Set<Map.Entry>) ((Map) tableData).entrySet()) {
					List<TableCell> values = new ArrayList<TableCell>(2);
					TableCell cell = new TableCell();
					cell.setName("key");
					cell.setValue(entry.getKey().toString());
					values.add(cell);
					cell = new TableCell();
					cell.setName("value");
					cell.setValue(entry.getValue() != null ? entry.getValue().toString() : "");
					values.add(cell);

					TableRow tr = new TableRow(entry);
					tr.getCells().addAll(values);
					addRow(result, tr);
				}
				result.getKeys().add("key");
			}
		}

		protected void analyzeListType(TableResult result, Object tableData) {
			result.setListType(((ParameterizedType) tableData.getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
		}

		protected void buildTableHeader(final TableResult result, final Object tableData) {

			TableSchema schema = (TableSchema) result.classInfoCache.get((Class) result.getListType(), "TableResult.classInfoCache.analyze.header", new ClassInfoCache.Factory() {
				@Override
				public Object create() {

					TableSchema table = new TableSchema();

					Class resultCls = (Class) result.getListType();

					while (resultCls != Object.class) {

						Field[] fields = resultCls.getDeclaredFields();

						for (Field f : fields) {

							if ((f.getModifiers() & Modifier.STATIC) != 0) {
								continue;
							}

							TableResultColumn columnAnnotation = f.getAnnotation(TableResultColumn.class);
							if (columnAnnotation != null && columnAnnotation.ignore()) {
								continue;
							}

							if ((columnAnnotation != null
								  && (columnAnnotation.columnSerializer() != CellSerializer.class
								  || columnAnnotation.format().trim().length() > 0
								  || StringUtils.isNotBlank(columnAnnotation.template())))
								  || f.getType() == String.class || f.getType().isPrimitive() || Number.class.isAssignableFrom(f.getType())
								  || f.getType() == Date.class || Boolean.class.isAssignableFrom(f.getType())
								  || f.getType().isEnum()) {

								TableSchemaColumn col = new TableSchemaColumn(f, columnAnnotation);

								if (col.getSerializer() == null) {
									col.setSerializer(DefaultCellSerializer.class);
								}

								table.addColumn(col);

							}

						}

						resultCls = resultCls.getSuperclass();

					}

					return table;

				}
			});

			for (TableSchemaColumn schemaCol : schema.getColumns()) {

				TableColumn column = new TableColumn(schemaCol.getName(), result.getI18n().translate(((Class) result.getListType()).getName() + "." + schemaCol.getName()));

				column.setSourceField(schemaCol.getSourceField());

				Class<? extends CellSerializer> serializerClass = schemaCol.getSerializer();
				if (serializerClass == CellSerializer.class) {
					serializerClass = DefaultCellSerializer.class;
				}

				column.setConfiguration(schemaCol.getConfiguration());
				column.setKey(schemaCol.isKey());
				if (schemaCol.isKey()) {
					result.getKeys().add(column.getName());
				}
				column.setHidden(schemaCol.isHidden());
				column.setSortable(schemaCol.isSortable());
				column.setSerializer(result.factory.newInstance(serializerClass));
				column.setEditType(schemaCol.getEditType());
				column.setEditOperation(schemaCol.getEditOperation());
				column.setEditSource(schemaCol.getEditSource());
				column.setFormat(schemaCol.getFormat());
				column.setTemplate(schemaCol.getTemplate());
				if (schemaCol.getFilter() != null && !schemaCol.getFilter().isInterface()) {
					column.setFilter(result.factory.newInstance(schemaCol.getFilter()));
					result.hasFilters = true;
					column.getFilter().init(result, column.getSourceField());
				}

				result.header.add(column);

			}

			if (result.getKeys().isEmpty() && !result.header.getColumns().isEmpty()) {
				result.getKeys().add(result.header.getColumns().get(0).getName());
			}

		}

		protected void buildTableData(TableResult result, Collection tableData) {

			for (Object o : tableData) {

				TableRow tr = new TableRow(o);

				List<TableCell> values = new LinkedList<TableCell>();
				for (TableColumn head : result.header.getColumns()) {
					TableCell cell = new TableCell();
					cell.setName(head.getName());
					cell.setFieldType(head.getSourceField().getType());
					Field f = head.getSourceField();
					f.setAccessible(true);
					Object value = null;
					if (tr.getSource() != null) {
						try {
							value = f.get(tr.getSource());
						} catch (IllegalArgumentException ex) {
							Logger.getLogger(TableResult.class.getName()).log(Level.SEVERE, null, ex);
						} catch (IllegalAccessException ex) {
							Logger.getLogger(TableResult.class.getName()).log(Level.SEVERE, null, ex);
						}
					}
					cell.setOriginalValue(value);
					head.getSerializer().serialize(result, tr, head, cell);
					tr.getCells().add(cell);
				}

				addRow(result, tr);

			}

		}

		protected void addRow(TableResult result, TableRow row) {
			result.getRows().add(row);
		}

	}

	/**
	 * <p>
	 * This interface should be implemented to define a cell serializer. The
	 * CellSerializer is used by the TableResult to obtain cell string
	 * values.</p>
	 *
	 * <p>
	 * Warning: strings produced by implementations of this class will be HTML
	 * escaped by the {@link BaseRowHandler}, if it is used by the
	 * {@link TableResult}. To avoid this behaviour, implement the
	 * {@link UnescapedCellSerializer} interface.</p>
	 *
	 * @see UnescapedCellSerializer
	 */
	public static interface CellSerializer {

		/**
		 * This method is invoked when a cell needs to be serialized. To
		 * produce the output, fill the <code>cell</code> object directly.
		 *
		 * @param result the current TableResult object
		 * @param row the current TableRow object
		 * @param column the current TableColumn object
		 * @param cell the TableCell object to be filled
		 */
		public void serialize(TableResult result, TableRow row, TableColumn column, TableCell cell);

	}

	/**
	 * This interface is the same as the {@link CellSerializer} interface, but
	 * the {@link BaseRowHandler} won't escape the produced string.
	 *
	 * @see CellSerializer
	 */
	public static interface UnescapedCellSerializer extends CellSerializer {

	}

	public static class DefaultCellSerializer implements CellSerializer {

		@Inject
		private I18N i18n;

		private final SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm");

		@Override
		public void serialize(TableResult result, TableRow row, TableColumn column, TableCell cell) {

			try {
				Object value = cell.getOriginalValue();
				if (value != null) {
					if (column.getFormat() != null) {
						cell.setValue(String.format(Context.get().getLanguages()[0], column.getFormat(), value));
					} else if (value instanceof Date) {
						cell.setValue(fmt.format((Date) value));
					} else if (value instanceof Calendar) {
						cell.setValue(fmt.format(((Calendar) value).getTime()));
					} else if (value.getClass().isEnum()) {
						cell.setValue(i18n.translate(value.getClass().getName() + "." + value.toString()));
					} else {
						cell.setValue(value.toString());
					}
				} else {
					cell.setValue("");
				}
			} catch (SecurityException ex) {
				Logger.getLogger(TableResult.class.getName()).log(Level.SEVERE, null, ex);
			} catch (IllegalArgumentException ex) {
				Logger.getLogger(TableResult.class.getName()).log(Level.SEVERE, null, ex);
			}

		}

	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.FIELD)
	public static @interface TableResultColumn {

		/**
		 * Use this property to change the field's name
		 *
		 * @return the custom field's name
		 */
		String name() default "";

		/**
		 * Specify a custom Serializer to produce a string from a not
		 * supported field type
		 *
		 * @return the custom field Serializer
		 */
		Class<? extends CellSerializer> columnSerializer() default CellSerializer.class;

		/**
		 * Specify this field as a key field for the set of rows
		 *
		 * @return if the current field is a key for the rows
		 */
		boolean key() default false;

		/**
		 * Asks to ignore the field
		 *
		 * @return ignores the field
		 */
		boolean ignore() default false;

		/**
		 * Asks to parse the field's column, but hides it in table
		 *
		 * @return hides column in table
		 */
		boolean hidden() default false;

		/**
		 * Makes the column sortable
		 *
		 * @return if the column is made sortable
		 */
		boolean sortable() default false;

		/**
		 * Makes the column inline editable and uses the specified operation
		 * to save changed data (see x-editable's documentation for details)
		 *
		 * @return the edit operation for saving changes
		 */
		String editOperation() default "";

		/**
		 * Makes the column inline editable using the specified field type
		 * (see x-editable's documentation for details)
		 *
		 * @return the edit field type
		 */
		CellEditType editType() default CellEditType.NONE;

		/**
		 * Sets edit source for some edit types (type dependent)
		 *
		 * @return edit source
		 */
		String editSource() default "";

		/**
		 * Specifies a format for the field serialization using the
		 * <code>String.format</code>'s syntax.
		 *
		 * @return format for serializing the cell's value
		 */
		String format() default "";

		/**
		 * Specifies a custom template for rendering cell's content
		 *
		 * @return a template to render cell's content
		 */
		String template() default "";

	}

	public static class TableResultConfig {

		private TableResultSorting sorting;

		public TableResultSorting getSorting() {
			return sorting;
		}

		public void setSorting(TableResultSorting sorting) {
			this.sorting = sorting;
		}

		public boolean isSorted() {
			return sorting != null;
		}
	}

	public static class TableResultSorting {

		private String columnName;
		private SortDirection direction;

		public String getColumnName() {
			return columnName;
		}

		public void setColumnName(String columnName) {
			this.columnName = columnName;
		}

		public SortDirection getDirection() {
			return direction;
		}

		public void setDirection(SortDirection direction) {
			this.direction = direction;
		}

	}

	public static enum SortDirection {
		ASC,
		DESC
	}

	/**
	 * This event listener is for **INTERNAL USE ONLY**
	 */
	@CoreEventListener(eventNames = CoreEventName.BEFORE_OPERATION)
	public static class TableConfigurationOperationFiller implements IEventListener<BeforeOperationEvent> {
		
		@Inject
		private OceanConversation conversation;
		
		@Override
		public void performAction(BeforeOperationEvent evt) {

			if (!TableResult.class.isAssignableFrom(evt.getOperation().getMethod().getReturnType())) {
				return;
			}

			TableResultConfig config = new TableResultConfig();

			String[] sort = evt.getArgs().get("fw:sort");
			String key = conversation.key(new TableResult(), "sort");
			if (sort == null) {
				sort = (String[]) conversation.get(key);
			}
			if (sort != null && sort.length >= 1 && sort[0].trim().length() > 0) {
				String[] cmd = sort[0].trim().split("-");
				TableResultSorting sortCmd = new TableResultSorting();
				sortCmd.setColumnName(cmd[1]);
				sortCmd.setDirection(SortDirection.valueOf(cmd[0]));
				config.setSorting(sortCmd);
				conversation.put(key, sort);
			}

			evt.getExtraObjects().add(config);

		}

	}

	public static enum CellEditType {
		NONE,
		TEXT,
		TEXTAREA,
		SELECT,
		DATE,
		DATETIME,
		DATEUI,
		COMBODATE,
		PASSWORD,
		EMAIL,
		URL,
		TEL,
		NUMBER,
		RANGE,
		TIME,
		CHECKLIST,
		WYSIHTML5,
		TYPEAHEAD,
		TYPEAHEADJS,
		SELECT2
	}

	public static enum CellLinkType {
		NONE,
		OPERATION,
		JAVASCRIPT
	}

	/**
	 * This instance is used by TableResult decorators to obtain a result
	 * wrapper, more suitable for the currently produced output.
	 */
	public static interface WrapperResultBuilder {

		public BaseResult getWrapperResult(TableResult tabRes);

	}

	/**
	 * Internal class used to cache info about table schemas, used to build
	 * initial table header and related information
	 */
	private static class TableSchema implements Serializable {

		private final List<TableSchemaColumn> columns = new ArrayList<TableSchemaColumn>();
		private final List<TableSchemaColumn> keys = new ArrayList<TableSchemaColumn>();

		public List<TableSchemaColumn> getColumns() {
			return new ArrayList<TableSchemaColumn>(columns);
		}

		public List<TableSchemaColumn> getKeys() {
			return new ArrayList<TableSchemaColumn>(keys);
		}

		public TableSchema addColumn(TableSchemaColumn column) {
			columns.add(column);
			if (column.isKey()) {
				keys.add(column);
			}
			return this;
		}

		public TableSchema addColumn(Field field) {
			TableSchemaColumn col = new TableSchemaColumn(field, null);
			columns.add(col);
			if (col.isKey()) {
				keys.add(col);
			}
			return this;
		}

	}

	private static class TableSchemaColumn implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private String name;
		private boolean hidden;
		private boolean key;
		private boolean sortable;
		private Class<? extends CellSerializer> serializer;
		private TableResultColumn configuration;
		private transient Field sourceField;
		private String editOperation;
		private String format;
		private CellEditType editType = CellEditType.NONE;
		private String editSource;
		private String template;
		private Class<? extends Filter> filter;

		public TableSchemaColumn() {

		}

		public TableSchemaColumn(Field field, TableResultColumn config) {
			this.configuration = config;
			this.sourceField = field;
			name = field.getName();
			if (config == null) {
				config = field.getAnnotation(TableResultColumn.class);
			}
			if (config != null) {
				if (StringUtils.isNotBlank(config.name())) {
					name = config.name();
				}
				hidden = config.hidden();
				key = config.key();
				sortable = config.sortable();
				serializer = config.columnSerializer();
				if (config.editType() != CellEditType.NONE) {
					editOperation = config.editOperation().trim();
					editType = config.editType();
					editSource = config.editSource().trim().length() > 0 ? config.editSource().trim() : null;
				}
				format = StringUtils.isNotBlank(config.format()) ? config.format() : null;
				template = StringUtils.isNotBlank(config.template()) ? config.template().trim() : null;
			}
			Filterable filterable = field.getAnnotation(Filterable.class);
			if (filterable != null) {
				this.filter = filterable.filter();
			}
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public boolean isHidden() {
			return hidden;
		}

		public void setHidden(boolean hidden) {
			this.hidden = hidden;
		}

		public boolean isKey() {
			return key;
		}

		public void setKey(boolean key) {
			this.key = key;
		}

		public boolean isSortable() {
			return sortable;
		}

		public void setSortable(boolean sortable) {
			this.sortable = sortable;
		}

		public Class<? extends CellSerializer> getSerializer() {
			return serializer;
		}

		public void setSerializer(Class<? extends CellSerializer> serializer) {
			this.serializer = serializer;
		}

		public TableResultColumn getConfiguration() {
			return configuration;
		}

		public void setConfiguration(TableResultColumn configuration) {
			this.configuration = configuration;
		}

		public Field getSourceField() {
			return sourceField;
		}

		public void setSourceField(Field sourceField) {
			this.sourceField = sourceField;
		}

		public String getEditOperation() {
			return editOperation;
		}

		public void setEditOperation(String editOperation) {
			this.editOperation = editOperation;
		}

		public String getFormat() {
			return format;
		}

		public void setFormat(String format) {
			this.format = format;
		}

		public CellEditType getEditType() {
			return editType;
		}

		public void setEditType(CellEditType editType) {
			this.editType = editType;
		}

		public String getEditSource() {
			return editSource;
		}

		public void setEditSource(String editSource) {
			this.editSource = editSource;
		}

		public String getTemplate() {
			return template;
		}

		public void setTemplate(String template) {
			this.template = template;
		}

		public Class<? extends Filter> getFilter() {
			return filter;
		}

		public void setFilter(Class<? extends Filter> filter) {
			this.filter = filter;
		}

	}

	/**
	 * This implementation of {@link WrapperResultBuilder} is the default
	 * implementation for TableResult in Ocean. It displays a static table. It
	 * supports sorting and filtering.
	 */
	public static class WebResultBuilder implements WrapperResultBuilder {

		@Inject
		private ObjectFactory factory;

		@Override
		public BaseResult getWrapperResult(TableResult tabRes) {

			WebPageResult wrapperResult = factory.newInstance(WebPageResult.class);

			wrapperResult.setData(tabRes);
			wrapperResult.setTemplate("/templates/core/tableResult.tpl");
			wrapperResult.setParentOperation(tabRes.getParentOperation());
			wrapperResult.getExtras().putAll(tabRes.getExtras());
			wrapperResult.setFullResult(tabRes.isFullResult());

			for (TableResult.TableColumn col : tabRes.getHeader().getColumns()) {
				if (col.getEditType() != TableResult.CellEditType.NONE) {
					wrapperResult.putExtra("table_inline_edit", true);
					Results.addCssSource(tabRes, "ris/libs/bootstrap3-editable-1.5.1/css/bootstrap-editable.css");
					Results.addScriptSource(tabRes, "ris/libs/bootstrap3-editable-1.5.1/js/bootstrap-editable.min.js");
					break;
				}
			}

			return wrapperResult;

		}

	}
}
