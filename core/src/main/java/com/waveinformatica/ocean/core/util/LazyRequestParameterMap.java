/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

public class LazyRequestParameterMap implements Map<String, String[]> {
	
	private final String prefix;
	private final HttpServletRequest request;
	private Map<String, String[]> parameterMap;
	
	public LazyRequestParameterMap(HttpServletRequest request, String prefix) {
		super();
		this.request = request;
		this.prefix = prefix;
	}
	
	public LazyRequestParameterMap(HttpServletRequest request) {
		super();
		this.request = request;
		this.prefix = null;
	}
	
	private void load() {
		if (parameterMap != null) {
			return;
		}
		
		if (prefix == null) {
			try {
				parameterMap = new HashMap<>(request.getParameterMap());
			} catch (Exception e) {
				parameterMap = new HashMap<String, String[]>();
			}
		}
		else {
			parameterMap = new HashMap<String, String[]>();
			for (String k : request.getParameterMap().keySet()) {
				if (k.startsWith(prefix)) {
					parameterMap.put(k.substring(prefix.length()), request.getParameterValues(k));
				}
			}
		}
	}
	
	@Override
	public int size() {
		load();
		return parameterMap.size();
	}
	
	@Override
	public boolean isEmpty() {
		load();
		return parameterMap.isEmpty();
	}
	
	@Override
	public boolean containsKey(Object key) {
		load();
		return parameterMap.containsKey(key);
	}
	
	@Override
	public boolean containsValue(Object value) {
		load();
		return parameterMap.containsValue(value);
	}
	
	@Override
	public String[] get(Object key) {
		load();
		return parameterMap.get(key);
	}
	
	@Override
	public String[] put(String key, String[] value) {
		load();
		return parameterMap.put(key, value);
	}
	
	@Override
	public String[] remove(Object key) {
		load();
		return parameterMap.remove(key);
	}
	
	@Override
	public void putAll(Map<? extends String, ? extends String[]> m) {
		load();
		parameterMap.putAll(m);
	}
	
	@Override
	public void clear() {
		load();
		parameterMap.clear();
	}
	
	@Override
	public Set<String> keySet() {
		load();
		return parameterMap.keySet();
	}
	
	@Override
	public Collection<String[]> values() {
		load();
		return parameterMap.values();
	}
	
	@Override
	public Set<Entry<String, String[]>> entrySet() {
		load();
		return parameterMap.entrySet();
	}
	
	@Override
	public boolean equals(Object o) {
		load();
		return parameterMap.equals(o);
	}
	
	@Override
	public int hashCode() {
		load();
		return parameterMap.hashCode();
	}

}
