/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.html;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.controllers.dto.ResourceInfo;
import com.waveinformatica.ocean.core.util.FsUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.ResourceRetriever;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
public class CSSStyleSheet {

	private String path;
	private final List<CSSRule> rules = new ArrayList<CSSRule>();

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public List<CSSRule> getRules() {
		return rules;
	}

	public void addRule(CSSRule rule) {
		rules.add(rule);
	}

	public void parse(String data) {

		parse(new StringReader(data));

	}

	public void parse(String basePath, String data) {
		
		parse(basePath, new StringReader(data));

	}

	public void merge(CSSStyleSheet css) {

		rules.addAll(css.getRules());

	}

	public void parse(Reader reader) {

		parse("", reader);

	}

	public void parse(String basePath, Reader reader) {

		CSSLexer lexer = new CSSLexer(new ParserHelperReader(reader));
		int tok;
		try {

			List<String> selectors = new ArrayList<String>();
			List<String> mediaQueryConditions = new ArrayList<String>();
			List<CSSCondition> conditions = new ArrayList<CSSCondition>();
			String atPageSelector = null;
			StringBuilder selectorBuilder = new StringBuilder();

			while ((tok = lexer.yylex()) != CSSLexer.YYEOF) {
				CSSSym sym = CSSSym.values()[tok];
				switch (lexer.yystate()) {
					case CSSLexer.YYINITIAL:
						switch (sym) {
							case SELECTOR:
								selectorBuilder.append(' ');
								selectorBuilder.append(lexer.yytext());
								break;
							case SELECTOR_COMMA:
								selectors.add(selectorBuilder.toString().trim());
								selectorBuilder = new StringBuilder();
								break;
						}
						break;
					case CSSLexer.RULE_BLOCK:
						switch (sym) {
							case BODY_BEGIN:
								selectors.add(selectorBuilder.toString().trim());
								selectorBuilder = new StringBuilder();
								addRule(new CSSRule(selectors, lexer, conditions));
								selectors.clear();
								break;
						}
						break;
					case CSSLexer.AT_MEDIA:
						switch (sym) {
							case DIR_ARG:
								mediaQueryConditions.add(lexer.yytext().trim());
								break;
							case BODY_END:
								mediaQueryConditions.clear();
								conditions = new ArrayList<CSSCondition>(0);
								lexer.closeState();
								break;
						}
						break;
					case CSSLexer.AT_MEDIA_BODY:
						switch (sym) {
							case SELECTOR:
								selectorBuilder.append(' ');
								selectorBuilder.append(lexer.yytext());
								break;
							case SELECTOR_COMMA:
								selectors.add(selectorBuilder.toString().trim());
								selectorBuilder = new StringBuilder();
								break;
							case BODY_BEGIN:
								selectors.add(selectorBuilder.toString().trim());
								selectorBuilder = new StringBuilder();
								conditions = CSSCondition.parse(StringUtils.join(mediaQueryConditions, ','));
								mediaQueryConditions.clear();
								break;
						}
						break;
					case CSSLexer.AT_DOCUMENT:
						switch (sym) {
							case BODY_END:
								lexer.closeState();
								break;
						}
						break;
					case CSSLexer.AT_KEYFRAMES:
						switch (sym) {
							case DIR_KEYFRAMES:
								selectorBuilder.append(lexer.yytext());
								break;
							case DIR_ARG:
								selectorBuilder.append(' ');
								selectorBuilder.append(lexer.yytext());
								break;
							case BODY_END:
								lexer.closeState();
								break;
						}
						break;
					case CSSLexer.AT_KEYFRAMES_BODY:
						switch (sym) {
							case BODY_BEGIN:
								selectors.add(selectorBuilder.toString().trim());
								selectorBuilder = new StringBuilder();
								addRule(new CSSRule(selectors, lexer, conditions));
								selectors.clear();
								lexer.closeState();
								break;
						}
						break;
					case CSSLexer.AT_PAGE:
						switch (sym) {
							case SELECTOR:
								atPageSelector = ("@page " + lexer.yytext().trim()).trim();
								break;
						}
						break;
					case CSSLexer.AT_PAGE_BODY:
						switch (sym) {
							case BODY_BEGIN:
								if (atPageSelector == null) {
									atPageSelector = "@page";
								}
								addRule(new CSSRule(Arrays.asList(new String[]{atPageSelector}), lexer, conditions));
								atPageSelector = null;
								lexer.closeState();
								atPageSelector = null;
								break;
						}
						break;
					case CSSLexer.AT_SUPPORTS:
						switch (sym) {
							case BODY_END:
								lexer.closeState();
								break;
						}
						break;
					case CSSLexer.AT_IMPORT:
						switch (sym) {
							case DIR_VALUE:
								String val = null;
								CSSProperty prop = new CSSProperty("__", lexer.yytext());
								CSSValue value = prop.getParsedValues().get(0);
								if (value instanceof CSSValueUrl) {
									CSSValueUrl url = (CSSValueUrl) value;
									val = url.getValue();
								} else if (value instanceof CSSValueString) {
									CSSValueString str = (CSSValueString) value;
									val = str.getValue();
								} else {
									throw new CSSParseException(lexer, "Unable to parse @import value \"" + lexer.yytext() + "\"");
								}
								if (val.startsWith("\"") || val.startsWith("'")) {
									val = val.substring(1, val.length() - 1);
								}
								ResourceRetriever retriever = Application.getInstance().getObjectFactory().newInstance(ResourceRetriever.class);
								if (!val.startsWith("/")) {
									try {
										URL url = new URL(val);
									} catch (MalformedURLException e) {
										val = FsUtils.normalizePath(basePath.substring(0, basePath.lastIndexOf('/') + 1) + val);
									}
								}
								ResourceInfo extRes = retriever.getResource(val);
								if (extRes == null) {
									LoggerUtils.getLogger(CSSStyleSheet.class).log(Level.SEVERE, "External resource \"" + val + "\" was not found");
								} else {
									CSSStyleSheet imported = new CSSStyleSheet();
									final String importedBasePath = val.substring(0, val.lastIndexOf('/') + 1);
									imported.parse(importedBasePath, new InputStreamReader(extRes.getInputStream(), Charset.forName("UTF-8")));
									CSSUtils.rewriteUrls(imported, new CSSUtils.UrlRewriteCallback() {
										@Override
										public String rewrite(String url) {
											if (url.startsWith("data:")) {
												return url;
											} else {
												try {
													URL pUrl = new URL(url);
													return pUrl.toExternalForm();
												} catch (MalformedURLException ex) {
													if (url.startsWith("/")) {
														return url;
													} else {
														String[] parts = StringUtils.split(importedBasePath, '/');
														StringBuilder base = new StringBuilder(parts.length * 3);
														for (String p : parts) {
															base.append("../");
														}
														return base.toString() + FsUtils.normalizePath(importedBasePath + url);
													}
												}
											}
										}
									});
									merge(imported);
								}
								break;
						}
						break;
				}
			}

		} catch (CSSParseException e) {
			throw new CSSParseException(lexer, basePath, e);
		} catch (IOException ex) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, null, ex);
		}

	}

	public List<CSSWeightedRule> getExplodedRules() {
		return getExplodedRules(null);
	}

	public List<CSSWeightedRule> getExplodedRules(CSSContext context) {

		if (context == null) {
			context = new CSSContext();
		}

		List<CSSWeightedRule> weightedRules = new ArrayList<CSSWeightedRule>();

		int order = 0;

		for (CSSRule r : rules) {
			if (context.ruleApplies(r)) {
				for (String s : r.getSelector()) {
					for (CSSProperty p : r.getProperties()) {
						weightedRules.add(new CSSWeightedRule(order++, s, p));
					}
				}
			}
		}

		Collections.sort(weightedRules);

		return weightedRules;
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();

		List<CSSCondition> oldCondition = new ArrayList<CSSCondition>();

		for (CSSRule rule : rules) {
			if (!match(rule.getMediaQuery(), oldCondition)) {
				if (oldCondition != null && !oldCondition.isEmpty()) {
					builder.append('}');
				}
				if (rule.getMediaQuery() != null && !rule.getMediaQuery().isEmpty()) {
					builder.append("@media ");
					StringBuilder query = new StringBuilder();
					for (CSSCondition cond : rule.getMediaQuery()) {
						if (query.toString().trim().length() > 0) {
							query.append(',');
						}
						query.append(cond.toString());
					}
					builder.append(query.toString());
					builder.append('{');
				}
			}
			oldCondition = rule.getMediaQuery();
			builder.append(StringUtils.join(rule.getSelector(), ','));
			builder.append('{');
			StringBuilder propBuilder = new StringBuilder();
			for (CSSProperty prop : rule.getProperties()) {
				propBuilder.append(prop.toString());
			}
			if (propBuilder.length() > 0) {
				String res = propBuilder.toString();
				if (res.endsWith(";")) {
					builder.append(res.substring(0, res.length() - 1));
				} else {
					builder.append(res);
				}
			}
			builder.append('}');
		}

		if (oldCondition != null && !oldCondition.isEmpty()) {
			builder.append('}');
		}

		return builder.toString();

	}

	private boolean match(List<CSSCondition> cond1, List<CSSCondition> cond2) {
		if (cond1 == null && cond2 == null) {
			return true;
		}
		if (cond1 != null && cond1.equals(cond2)) {
			return true;
		}
		return false;
	}

}
