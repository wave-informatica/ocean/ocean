/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.filtering;

import java.lang.reflect.Field;

import com.waveinformatica.ocean.core.filtering.drivers.QueryBuilderDriver;
import com.waveinformatica.ocean.core.filtering.drivers.ValuesManager;

/**
 *
 * @author Ivano
 */
public interface Filter {
	
	/**
	 * 
	 * @param result
	 * @param field
	 * @see Filter#init(Object, Field, QueryBuilderDriver, ValuesManager)
	 */
	public void init(Object result, Field field);
	
	/**
	 * Builds a {@link QueryFilter} to be used to filter a JPA query.
	 * 
	 * @param path 
	 * @param filterValue
	 * @param expectedType 
	 * @return 
	 * 
	 * @see Filter#getQueryFilter(String, String, Class)
	 */
	@Deprecated
	public QueryFilter getJpqlFilter(String path, String filterValue, Class<?> expectedType);
	
	/**
	 * Builds a {@link QueryFilter} to be used to filter a query
	 * 
	 * @param path
	 * @param filterValue
	 * @param expectedType
	 * @return
	 */
	public <T> QueryFilter<T> getQueryFilter(String path, String filterValue, Class<?> expectedType, QueryBuilderDriver<T> driver, ValuesManager values);
	
	/**
	 * 
	 * @return
	 */
	public String getTemplate();
	
	/**
	 * 
	 * @return
	 */
	public String getHelpTemplate();
	
}
