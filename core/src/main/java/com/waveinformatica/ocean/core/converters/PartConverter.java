/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.converters;

import com.waveinformatica.ocean.core.annotations.TypeConverter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

/**
 *
 * @author Ivano
 */
@TypeConverter("javax.servlet.http.Part")
public class PartConverter implements ITypeConverter {

    @Override
    public Object fromParametersMap(String destName, Class destClass, Type genericType, Map<String, String[]> source, Object[] ctxObjects) {
        
        HttpServletRequest req = null;
        for (Object o : ctxObjects) {
            if (o instanceof HttpServletRequest) {
                req = (HttpServletRequest) o;
                break;
            }
        }
        
        if (req == null) {
            return null;
        }
        
        Part result = null;
        
        try {
            result = req.getPart(destName);
        } catch (IOException ex) {
            Logger.getLogger(PartConverter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(PartConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
        
    }

    @Override
    public Object fromScalar(String value, Class destClass, Map<String, String[]> source, Object[] ctxObjects) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
