/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.html;

import java.util.ArrayList;
import java.util.List;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
public class CSSCondition {
	
	public static List<CSSCondition> parse(String source) {
		
		if (StringUtils.isBlank(source)) {
			return new ArrayList<CSSCondition>(0);
		}

		ANTLRInputStream input = new ANTLRInputStream(source);
		
		CSSConditionLexer lexer = new CSSConditionLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);

		CSSConditionParser parser = new CSSConditionParser(tokens);
		
		CSSConditionParser.MediaQueryListContext parsed = parser.mediaQueryList();

		List<CSSCondition> result = new ArrayList<CSSCondition>(parsed.mediaQuery().size());

		for (CSSConditionParser.MediaQueryContext cmpExp : parsed.mediaQuery()) {
			CSSCondition cnd = new CSSCondition();
			StringBuilder srcBuilder = new StringBuilder();
			if (cmpExp.NOT()!= null && StringUtils.isNotBlank(cmpExp.NOT().getText())) {
				cnd.not = true;
				appendPart(cmpExp.NOT().getText(), srcBuilder);
			}
			if (cmpExp.ONLY() != null && StringUtils.isNotBlank(cmpExp.ONLY().getText())) {
				appendPart(cmpExp.ONLY().getText(), srcBuilder);
			}
			boolean and = false;
			if (cmpExp.IDENT() != null && StringUtils.isNotBlank(cmpExp.IDENT().getText())) {
				cnd.media = cmpExp.IDENT().getText();
				appendPart(cmpExp.IDENT().getText(), srcBuilder);
				and = true;
			}
			if (cmpExp.mediaExpression() != null) {
				for (CSSConditionParser.MediaExpressionContext me : cmpExp.mediaExpression()) {
					if (and) {
						appendPart("and", srcBuilder);
					}
					srcBuilder.append('(');
					CSSProperty prop = new CSSProperty(me);
					cnd.properties.add(prop);
					String ser = prop.toString();
					srcBuilder.append(ser.substring(0, ser.length() - 1));
					srcBuilder.append(')');
					and = true;
				}
			}
			cnd.source = srcBuilder.toString();
			result.add(cnd);
		}

		return result;
		
	}
	
	private static void appendPart(String txt, StringBuilder builder) {
		if (builder.toString().trim().length() > 0) {
			builder.append(' ');
		}
		builder.append(txt.trim());
	}
	
	private String source;
	private boolean not = false;
	private String media;
	private final List<CSSProperty> properties = new ArrayList<CSSProperty>();

	private CSSCondition() {
	}

	public String getSource() {
		return source;
	}

	public String getMedia() {
		return media;
	}

	public boolean isNot() {
		return not;
	}

	public List<CSSProperty> getProperties() {
		return properties;
	}

	@Override
	public String toString() {
		return getSource();
	}
	
}
