/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.filtering;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.Filterable;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.ObjectProperty;
import com.waveinformatica.ocean.core.filtering.drivers.QueryBuilderDriver;
import com.waveinformatica.ocean.core.filtering.drivers.ValuesManager;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.ObjectUpdater;

/**
 *
 * @author Ivano
 */
public class FilterBuilder {
	
	private final List<ObjectProperty> properties = new ArrayList<ObjectProperty>();

	@Inject
	private ObjectFactory factory;
	
	@Inject
	private ObjectUpdater updater;
	
	private Class<?> entity;
	private FilterMode mode = FilterMode.AND;
	
	public void init(Class<?> entity) {
		
		this.entity = entity;
		
		properties.clear();
		properties.addAll(updater.getObjectProperties(entity).values());
		
	}
	
	public FilterMode getMode() {
		return mode;
	}

	public void setMode(FilterMode mode) {
		if (mode == null) {
			throw new NullPointerException("FilterMode must be set to a value");
		}
		this.mode = mode;
	}

	/**
	 * Deprecated
	 * 
	 * @param provider
	 * @param prefix
	 * @return
	 */
	@Deprecated
	public QueryFilter getJpqlFilter (FilterProvider provider, String prefix) {
		
		QueryFilter result = new QueryFilter(new ValuesManager());
		
		for (ObjectProperty p : properties) {
			String value = provider.getFilterValue(p.getField());
			if (StringUtils.isBlank(value) || p.getField() == null) {
				continue;
			}
			Filterable f = p.getField().getAnnotation(Filterable.class);
			if (f == null) {
				continue;
			}
			Filter filter = factory.newInstance(f.filter());
			filter.init(provider, p.getField());
			String path = f.path();
			if (StringUtils.isBlank(path)) {
				path = p.getField().getName();
			}
			Class<?> expectedType = p.getField().getType();
			String[] pathParts = path.split("\\.");
			for (int i = 1; i < pathParts.length; i++) {
				try {
					expectedType = expectedType.getDeclaredField(pathParts[i]).getType();
				} catch (NoSuchFieldException e) {
					LoggerUtils.getLogger(entity).log(Level.SEVERE, null, e);
				} catch (SecurityException e) {
					LoggerUtils.getLogger(entity).log(Level.SEVERE, null, e);
				}
			}
			path = prefix + "." + path;
			QueryFilter qf = filter.getJpqlFilter(path, value, expectedType);
			if (qf != null) {
				qf.setCondition("(" + qf.getCondition() + ")");
				if (StringUtils.isNotBlank(result.getCondition())) {
					qf.setCondition(result.getCondition() + " " + mode.name().toLowerCase() + " " + qf.getCondition());
				}
				result.setCondition(qf.getCondition());
				result.getParameters().putAll(qf.getParameters());
			}
		}
		
		if (StringUtils.isNotBlank(result.getCondition())) {
			result.setCondition("(" + result.getCondition() + ")");
		}
		
		return result;
	}
	
	public <T> QueryFilter<T> getFilter(FilterProvider provider, QueryBuilderDriver<T> driver, ValuesManager values) {
		
		List<T> conditions = new ArrayList<>();
		
		for (ObjectProperty p : properties) {
			String value = provider.getFilterValue(p.getField());
			if (StringUtils.isBlank(value) || p.getField() == null) {
				continue;
			}
			Filterable f = p.getField().getAnnotation(Filterable.class);
			if (f == null) {
				continue;
			}
			Filter filter = factory.newInstance(f.filter());
			filter.init(provider, p.getField());
			String path = f.path();
			if (StringUtils.isBlank(path)) {
				path = p.getField().getName();
			}
			Class<?> expectedType = p.getField().getType();
			String[] pathParts = path.split("\\.");
			for (int i = 1; i < pathParts.length; i++) {
				try {
					expectedType = expectedType.getDeclaredField(pathParts[i]).getType();
				} catch (NoSuchFieldException e) {
					LoggerUtils.getLogger(entity).log(Level.SEVERE, null, e);
				} catch (SecurityException e) {
					LoggerUtils.getLogger(entity).log(Level.SEVERE, null, e);
				}
			}
			QueryFilter<T> qf = filter.getQueryFilter(path, value, expectedType, driver, values);
			if (qf != null) {
				conditions.add(qf.getWhereClause());
			}
		}
		
		QueryFilter<T> result = new QueryFilter<>(values);
		
		if (conditions.isEmpty()) {
			return result;
		}
		else {
			T[] clauses = conditions.toArray((T[]) Array.newInstance(driver.getQueryType(), conditions.size()));
			switch (mode) {
			case AND:
				result.setWhereClause(driver.and(clauses));
				break;
			case OR:
				result.setWhereClause(driver.or(clauses));
				break;
			}
			return result;
		}
		
	}
	
}
