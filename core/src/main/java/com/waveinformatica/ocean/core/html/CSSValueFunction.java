/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.html;

import java.util.List;

/**
 *
 * @author Ivano
 */
public class CSSValueFunction extends CSSValue {
	
	private String name;
	private List<CSSValue> args;

	public CSSValueFunction(String name, List<CSSValue> args) {
		this.name = name;
		this.args = args;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CSSValue> getArgs() {
		return args;
	}

	public void setArgs(List<CSSValue> args) {
		this.args = args;
	}

	@Override
	public String toString() {
		
		StringBuilder builder = new StringBuilder();
		
		int argsN = args.size();
		
		builder.append(name);
		builder.append('(');
		for (int i = 0; i < argsN; i++) {
			if (i > 0) {
				builder.append(',');
			}
			builder.append(args.get(i).toString());
		}
		builder.append(')');
		
		return builder.toString();
		
	}
	
}
