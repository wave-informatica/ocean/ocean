/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.util;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.http.HttpServletRequest;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;

/**
 *
 * @author Ivano
 */
public class Context {
	
	private static final ThreadLocal<CoreController> coreController = new ThreadLocal<CoreController>();
	private static final ThreadLocal<Context> context = new ThreadLocal<Context>();
	private static final ThreadLocal<MimeTypes> expectedType = new ThreadLocal<MimeTypes>();
	
	public static void setCoreController(CoreController controller) {
		coreController.set(controller);
	}
	
	public static CoreController getCoreController() {
		return coreController.get();
	}
	
	public static void init() {
		Context ctx = new Context();
		
		context.set(ctx);
		
		ctx.languages = new Locale[]{ Locale.getDefault() };
	}
	
	public static void init(HttpServletRequest req) {
		Context ctx = new Context();
		ctx.request = req;
		
		List<Locale> langs = new ArrayList<Locale>();
		
		Enumeration<Locale> locales = req.getLocales();
		while (locales.hasMoreElements()) {
			langs.add(locales.nextElement());
		}
		
		boolean englishFound = false;
		for (Locale l : langs) {
			if (l.getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				englishFound = true;
				break;
			}
		}
		if (!englishFound) {
			langs.add(Locale.ENGLISH);
		}
		
		ctx.languages = langs.toArray(new Locale[langs.size()]);
		ctx.servletContext = req.getServletContext();
		context.set(ctx);
	}
	
	@Deprecated
	public static void init(ServletContextEvent event) {
		
		init(event.getServletContext());
		
	}
	
	@Deprecated
	public static void init(ServletContext servletContext) {
		
		Context ctx = new Context();
		
		ctx.servletContext = servletContext;
		
		context.set(ctx);
		
		ctx.languages = new Locale[]{ Locale.getDefault() };
		
	}
	
	public static void clear() {
		context.set(null);
		expectedType.set(null);
	}
	
	public static Context get() {
		return context.get();
	}
	
	private HttpServletRequest request;
	private Locale[] languages;
	@Deprecated
	private ServletContext servletContext;
	
	private Context() {
		
	}
	
	public Locale[] getLanguages() {
		return languages;
	}
	
	public void setLanguages(Locale[] languages) {
		this.languages = languages;
	}
	
	public void setLanguages(String[] languages) {
		Locale[] locales = new Locale[languages.length];
		for (int i = 0; i < languages.length; i++) {
			locales[i] = new Locale(languages[i]);
		}
		this.languages = locales;
	}
	
	public static MimeTypes getExpectedType() {
		return expectedType.get();
	}
	
	public static void setExpectedType(MimeTypes type) {
		expectedType.set(type);
	}
	
	@Deprecated
	public ServletContext getServletContext() {
		return servletContext;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}
	
}
