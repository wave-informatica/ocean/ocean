/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.controllers.decorators;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.waveinformatica.ocean.core.annotations.ResultDecorator;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.core.util.LoggerUtils;

/**
 *
 * @author Ivano
 */
@ResultDecorator(classes = TableResult.class, mimeTypes = {MimeTypes.JSON})
public class JsonTableDecorator extends JsonDecorator {
	
	@Inject
	private ObjectFactory factory;
	
	@Override
	public void decorate(Object result, HttpServletRequest request, HttpServletResponse response, MimeTypes type) {
		
		if (request.getParameter(TableResult.getPageParameter()) != null) {
			try {
				((TableResult) result).setPage(Integer.parseInt(request.getParameter(TableResult.getPageParameter())));
			} catch (NumberFormatException e) {
				LoggerUtils.getCoreLogger().warning("Unable to parse \"" + TableResult.getPageParameter() + "\" request parameter: \"" + request.getParameter(TableResult.getPageParameter()) + "\"");
			}
		}
		
		JsonTableResult jsonResult = buildResult(result);
		
		if (jsonResult.getName() == null) {
			jsonResult.setName("export");
		}

		super.decorate(jsonResult, request, response, type);
		
	}
	
	@Override
	public void decorate(Object result, OutputStream out, MimeTypes type) {
		
		JsonTableResult jsonResult = buildResult(result);
		
		if (jsonResult.getName() == null) {
			jsonResult.setName("export");
		}
		
		super.decorate(jsonResult, out, type);
	}
	
	public JsonTableResult buildResult(Object result) {
		
		TableResult tbl = (TableResult) result;
		tbl.setRowHandler(factory.newInstance(JsonRowHandler.class));
		
		tbl.analyzeData();
		
		JsonTableResult jsonResult = new JsonTableResult();
		
		for (TableResult.TableColumn col : tbl.getHeader().getColumns()) {
			JsonTableColumn tblCol = new JsonTableColumn();
			tblCol.setHidden(col.isHidden());
			tblCol.setKey(col.isKey());
			tblCol.setName(col.getName());
			tblCol.setLabel(col.getLabel());
			tblCol.setFormat(col.getFormat());
			jsonResult.getColumns().add(tblCol);
		}
		
		jsonResult.setRowsPerPage(tbl.getRowsPerPage().longValue());
		jsonResult.setPage(tbl.getPage().longValue());
		jsonResult.setTotalRows(tbl.getTotalRowsCount());
		
		for (TableResult.TableRow row : tbl.getPageRows()) {
			
			Map<String, Object> jsonRow = new HashMap<String, Object>();
			Map<String, String> jsonRowValues = new HashMap<String, String>();
			
			int i = 0;
			for (TableResult.TableColumn col : tbl.getHeader().getColumns()) {
				
				if (row.getCells().get(i).getOriginalValue() != null) {
					jsonRow.put(col.getName(), row.getCells().get(i).getOriginalValue());
				}
				else {
					jsonRow.put(col.getName(), row.getCells().get(i).getValue());
				}
				
				jsonRowValues.put(col.getName(), row.getCells().get(i).getValue());
				
				i++;
				
			}
			
			jsonResult.getData().add(jsonRow);
			
		}
		
		return jsonResult;
		
	}
	
	public static class JsonTableResult extends BaseResult {
		
		private final List<JsonTableColumn> columns = new ArrayList<JsonTableColumn>();
		private final List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
		private final List<Map<String, String>> values = new ArrayList<Map<String, String>>();
		private Long totalRows;
		private Long rowsPerPage;
		private Long page;

		public List<JsonTableColumn> getColumns() {
			return columns;
		}

		public List<Map<String, Object>> getData() {
			return data;
		}

		public List<Map<String, String>> getValues() {
			return values;
		}

		public Long getTotalRows() {
			return totalRows;
		}

		public void setTotalRows(Long totalRows) {
			this.totalRows = totalRows;
		}

		public Long getRowsPerPage() {
			return rowsPerPage;
		}

		public void setRowsPerPage(Long rowsPerPage) {
			this.rowsPerPage = rowsPerPage;
		}

		public Long getPage() {
			return page;
		}

		public void setPage(Long page) {
			this.page = page;
		}
		
	}
	
	public static class JsonTableColumn {
		
		private String name;
		private String label;
		private boolean hidden;
		private boolean key;
		private String format;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public boolean isHidden() {
			return hidden;
		}

		public void setHidden(boolean hidden) {
			this.hidden = hidden;
		}

		public boolean isKey() {
			return key;
		}

		public void setKey(boolean key) {
			this.key = key;
		}

		public String getFormat() {
			return format;
		}

		public void setFormat(String format) {
			this.format = format;
		}
		
	}
	
	public static class JsonRowHandler implements TableResult.RowHandler {
		
		@Override
		public void handleRow(TableResult.TableHeader header, TableResult.TableRow row) {
		}
		
		@Override
		public List<TableResult.TableOperation> handleRowActions(List<TableResult.TableOperation> operations, TableResult.TableRow rowSource) {
			return operations;
		}
		
	}
	
}
