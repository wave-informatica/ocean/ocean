/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.util;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Map;

public class OperationParameters {
	
	private final Map<String, String[]> map;
	
	public OperationParameters(Map<String, String[]> map) {
		super();
		this.map = map;
	}

	public String getParameter(String name) {
		String[] res = map.get(name);
		if (res == null || res.length > 1) {
			return null;
		}
		return res[0];
	}
	
	public String[] getParameterValues(String name) {
		return map.get(name);
	}
	
	public Map<String, String[]> getParameterMap() {
		return Collections.unmodifiableMap(map);
	}
	
	public Enumeration<String> getParameterNames() {
		return Collections.enumeration(map.keySet());
	}
	
}
