/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.templates;

import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import freemarker.ext.beans.BeanModel;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.AdapterTemplateModel;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateCollectionModel;
import freemarker.template.TemplateHashModelEx;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.WrappingTemplateModel;

/**
 *
 * @author Ivano
 */
public class BaseResultAdapter extends WrappingTemplateModel implements AdapterTemplateModel, TemplateHashModelEx {

	private BaseResult result;
	private BeanModel model;

	public BaseResultAdapter(BaseResult result, ObjectWrapper objectWrapper) {
		super(objectWrapper);
		this.result = result;
		model = new BeanModel(result, (BeansWrapper) objectWrapper);
	}

	public BaseResult getResult() {
		return result;
	}
	
	@Override
	public Object getAdaptedObject(Class hint) {
		return result;
	}

	@Override
	public int size() throws TemplateModelException {
		return model.size();
	}

	@Override
	public TemplateCollectionModel keys() throws TemplateModelException {
		return model.keys();
	}

	@Override
	public TemplateCollectionModel values() throws TemplateModelException {
		return model.values();
	}

	@Override
	public TemplateModel get(String key) throws TemplateModelException {
		return model.get(key);
	}

	@Override
	public boolean isEmpty() throws TemplateModelException {
		return model.isEmpty();
	}
	
}
