/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

@ConversationScoped
@Stateful
public class OceanConversation implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Inject
    private Conversation manager;
    
    private Map<String, Serializable> conversation;

    public OceanConversation() {

        conversation = new HashMap<String, Serializable>();
    }

    public void put(String key, Serializable value) {
        conversation.put(key, value);
        try {
	        if (manager.isTransient()) {
	        	manager.begin();
	        }
        } catch (NullPointerException e) {
        	// Conversation is not available
        }
    }

    public boolean containsKey(String key) {
        return conversation.containsKey(key);
    }

    public boolean containsValue(Serializable value) {
        return conversation.containsValue(value);
    }

    public Serializable get(String key) {
        return conversation.get(key);
    }

    @SuppressWarnings("unchecked")
    public <V extends Serializable> V get(String key, Class<V> type) {
        Object v = get(key);
        if (v == null || !type.isAssignableFrom(v.getClass())) {
            return null;
        }
        return (V) v;
    }

    public Serializable remove(String key) {
        return conversation.remove(key);
    }

    public void clear() {
        conversation.clear();
    }
    
    public Set<String> keySet() {
        return conversation.keySet();
    }
    
    public String key(Object scope, String name) {
    	if (Context.get() == null || scope == null) {
    		return null;
    	}
    	HttpServletRequest request = Context.get().getRequest();
    	if (request != null) {
    		UrlBuilder url = new UrlBuilder(request.getRequestURI());
    		Map<String, String[]> params = url.getParameters().build();
    		for (String p : params.keySet()) {
    			if (p.startsWith("fw:") || p.equals("cid")) {
    				url.getParameters().remove(p);
    			}
    		}
    		return "[" + url.getUrl() + "]:" + scope.getClass().getSimpleName() + "." + name;
    	}
    	return null;
    }
    
    public boolean isActive() {
    	return !manager.isTransient();
    }
    
    public String getId() {
    	return isActive() ? manager.getId() : null;
    }
    
    public void forceActivation() {
    	if (!isActive()) {
    		manager.begin();
    	}
    }

}
