/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.util;

import com.waveinformatica.ocean.core.Configuration;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
@Named
@ApplicationScoped
public class FsUtils {
	
	private static final Logger logger = LoggerUtils.getCoreLogger();
	
	@Inject
	private Configuration configuration;
	
	public File copyTempFile(File source, String newName) {
		File newFile = new File(configuration.getConfigDir(), "temp");
		if (!newFile.isDirectory()) {
			newFile.mkdirs();
		}
		newFile = new File(newFile, newName);
		try {
			byte[] buffer = new byte[4096];
			FileInputStream is = new FileInputStream(source);
			FileOutputStream os = new FileOutputStream(newFile);
			int read = 0;
			while ((read = is.read(buffer)) > 0) {
				os.write(buffer, 0, read);
			}
			os.close();
			is.close();
		} catch (IOException ex) {
			Logger.getLogger(FsUtils.class.getName()).log(Level.SEVERE, null, ex);
		}
		return newFile;
	}
	
	public File renameTempFile(InputStream source, String newName) {
		File newFile = new File(configuration.getConfigDir(), "temp");
		if (!newFile.isDirectory()) {
			newFile.mkdirs();
		}
		newFile = new File(newFile, newName);
		try {
			byte[] buffer = new byte[4096];
			FileOutputStream os = new FileOutputStream(newFile);
			int read = 0;
			while ((read = source.read(buffer)) > 0) {
				os.write(buffer, 0, read);
			}
			os.close();
		} catch (IOException ex) {
			Logger.getLogger(FsUtils.class.getName()).log(Level.SEVERE, null, ex);
		}
		return newFile;
	}
	
	public File copyFile(File source, File destDir, String name) {
		if (!destDir.isDirectory()) {
			destDir.mkdirs();
		}
		File newFile = new File(destDir, name);
		try {
			byte[] buffer = new byte[4096];
			FileInputStream is = new FileInputStream(source);
			FileOutputStream os = new FileOutputStream(newFile);
			int read = 0;
			while ((read = is.read(buffer)) > 0) {
				os.write(buffer, 0, read);
			}
			os.close();
			is.close();
		} catch (IOException ex) {
			Logger.getLogger(FsUtils.class.getName()).log(Level.SEVERE, null, ex);
		}
		return newFile;
	}
	
	public static String normalizePath(String path) {
		
        if (path == null) {
            return null;
        }
        
		String[] parts = path.split("[\\\\/]");
		
		LinkedList<String> stack = new LinkedList<String>();
		
		for (String part : parts) {
			if (StringUtils.isBlank(part) || part.equals(".")) {
				continue;
			} else if (!stack.isEmpty() && part.equals("..")) {
				stack.removeLast();
			} else {
				stack.add(part);
			}
		}
		
		String result = StringUtils.join(stack, '/');
		if (path.startsWith("/")) {
			result = "/" + result;
		}
		
		return result;
		
	}
	
}
