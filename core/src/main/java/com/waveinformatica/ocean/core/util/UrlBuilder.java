/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.util;

import java.io.Serializable;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

/**
 * The UrlBuilder class is a utility class for managing URLs. This class allows to easily manage
 * URLs with a query string and its parameters.
 * 
 * @author Marco Merli
 */
public class UrlBuilder implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final String url;
	private final ParametersBuilder params;

	public UrlBuilder(String url) {
		
		int qPos = url.indexOf('?');
		if (qPos < 0) {
			this.url = url;
			this.params = new ParametersBuilder();
		}
		else {
			this.url = url.substring(0, qPos);
			this.params = new ParametersBuilder(url);
		}
		
	}
	
	/**
	 * This method is a shortcut for {@link #getUrl(boolean) getUrl(true)}.
	 */
	public String getUrl()
	{
		String p = params.toString();
		if (StringUtils.isNotBlank(p)) {
			return String.format("%s?%s", url, p);
		}
		else {
			return url;
		}
	}
	
	/**
	 * Returns the final URL as a string.
	 * 
	 * @param queryString <code>true</code> to get the URL with its query string or <code>false</code> otherwise.
	 * @return the final URL
	 */
	public String getUrl(boolean queryString)
	{
		if (queryString) {
			return getUrl();
		}
		else {
			return url;
		}
	}

	/**
	 * Loads parameters from the {@link HttpServletRequest} parameter
	 * 
	 * @param request the {@link HttpServletRequest} to load parameters from
	 */
	public void load(HttpServletRequest request)
	{
		load(request.getParameterMap());
	}

	/**
	 * Loads parameters from a Map&lt;String, String[]&gt; map (like the map returned by
	 * {@link HttpServletRequest#getParameterMap() HttpServletRequest.getParameterMap()}).
	 * 
	 * @param parameterMap the parameters map
	 */
	public void load(Map<String, String[]> parameterMap)
	{
		for (String name : parameterMap.keySet()) {
			for (String value : parameterMap.get(name)) {
				append(name, value);
			}
		}
	}

	/**
	 * Adds a parameter to the parameter's list
	 * 
	 * @param name the parameter's name
	 * @param value the parameter's value
	 */
	public void append(String name, String value)
	{
		params.add(name, value);
	}
	
	/**
	 * Returns the {@link ParametersBuilder} used to manage URL's query string parameters.
	 * 
	 * @return the internal {@link ParametersBuilder} instance
	 */
	public ParametersBuilder getParameters() {
		return params;
	}
}
