/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.html;

import com.waveinformatica.ocean.core.util.LoggerUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
public class CSSRule {
	
	private final List<String> selector = new ArrayList<String>(1);
	private final List<CSSProperty> properties = new ArrayList<CSSProperty>();
	private final List<CSSCondition> mediaQuery = new ArrayList<CSSCondition>();
	
	protected CSSRule(List<String> selector, CSSLexer lexer, List<CSSCondition> mediaQuery) {
		this.mediaQuery.addAll(mediaQuery);
		for (String s : selector) {
			if (s.trim().length() == 0) {
				continue;
			}
			this.selector.add(s.trim().replaceAll("\\s+", " "));
		}
		int tok;
		try {
			String property = "";
			String blockSelector = null;
			CSSBlockProperty blockProperty = null;
			
			while ((tok = lexer.yylex()) != CSSLexer.YYEOF) {
				CSSSym sym = CSSSym.values()[tok];
				if (sym == CSSSym.BODY_END) {
					if (lexer.yystate() == CSSLexer.PROPERTY) {
						lexer.closeState();
						if (StringUtils.isNotBlank(property)) {
							properties.add(new CSSProperty(property, lexer.getBuiltValue()));
							property = "";
						}
						else {
							lexer.closeState();
						}
					}
					break;
				}
				switch (lexer.yystate()) {
					case CSSLexer.AT_BLOCK_PROPERTY:
						switch (sym) {
							case SELECTOR:
								blockSelector = lexer.yytext().trim();
								blockProperty = new CSSBlockProperty(blockSelector);
								break;
							case BODY_BEGIN:
								CSSRule body = new CSSRule(Arrays.asList(blockSelector.split(",")), lexer, new ArrayList<CSSCondition>());
								blockProperty.getProperties().addAll(body.getProperties());
								if (lexer.yystate() == CSSLexer.AT_BLOCK_PROPERTY) {
									lexer.closeState();
								}
								properties.add(blockProperty);
								blockProperty = null;
								break;
						}
						// No break here: continue with same rules as the other cases
					case CSSLexer.AT_PAGE_BODY:
					case CSSLexer.PROPERTY:
						switch (sym) {
							case PROPERTY_NAME:
								property = lexer.yytext().trim().toLowerCase();
								break;
							case PROPERTY_VALUE:
								if (StringUtils.isBlank(property)) {
									throw new CSSParseException(lexer, "Syntax error in CSS rule block.");
								}
								properties.add(new CSSProperty(property, lexer.getBuiltValue()));
								property = "";
								break;
							case PROP_SPEC_SEMICOLON:
								property = "";
								break;
						}
						break;
				}
			}
		} catch (IOException ex) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, null, ex);
		}
	}

	public CSSRule(List<String> selector, String block) {
		for (String s : selector) {
			this.selector.add(s.trim().replaceAll("\\s+", " "));
		}
		String[] properties = block.split(";");
		for (String prop : properties) {
			if (StringUtils.isNotBlank(prop)) {
				try {
					CSSProperty p = new CSSProperty(prop.trim());
					if (StringUtils.isBlank(p.getName()) || p.getValues() == null || p.getValues().isEmpty()) {
						continue;
					}
					this.properties.add(p);
				} catch (Exception e) {
					LoggerUtils.getLogger(CSSRule.class).log(Level.WARNING, e.getMessage());
				}
			}
		}
	}

	public List<String> getSelector() {
		return selector;
	}

	public List<CSSProperty> getProperties() {
		return properties;
	}
	
	public CSSProperty getProperty(String name) {
		CSSProperty prop = null;
		for (CSSProperty p : properties) {
			if (p.getName().equals(name)) {
				if (prop == null) {
					prop = p;
				}
				else {
					return null;
				}
			}
		}
		return prop;
	}
	
	public String getPropertyValue(String propertyName) {
		
		CSSProperty prop = getProperty(propertyName);
		
		if (prop == null) {
			return null;
		}
		else {
			List<CSSValue> values = prop.getParsedValues();
			if (values.isEmpty() || values.size() > 1) {
				return null;
			}
			else {
				CSSValue v = values.get(0);
				if (v instanceof CSSValueColor) {
					return ((CSSValueColor) v).getValue();
				}
				else if (v instanceof CSSValueName) {
					return ((CSSValueName) v).getName();
				}
				else if (v instanceof CSSValueNumber) {
					return v.toString();
				}
				else if (v instanceof CSSValueString) {
					return ((CSSValueString) v).getValue();
				}
				else if (v instanceof CSSValueUrl) {
					return ((CSSValueUrl) v).getValue();
				}
				return prop.getValues().get(0);
			}
		}
	}
	
	public CSSProperty addProperty(String name, String value) {
		CSSProperty property = new CSSProperty(name, value);
		properties.add(property);
		return property;
	}
	
	public String bodyToString() {
		StringBuilder builder = new StringBuilder();
		for (CSSProperty p : properties) {
			builder.append(p.toString());
		}
		return builder.toString();
	}

	public List<CSSCondition> getMediaQuery() {
		return mediaQuery;
	}
	
}
