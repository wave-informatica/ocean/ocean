/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.modules;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;

import javax.enterprise.context.spi.CreationalContext;

import org.scannotation.AnnotationDB;

import com.waveinformatica.ocean.core.annotations.UseClassloader;
import com.waveinformatica.ocean.core.cache.OceanCacheMap;
import com.waveinformatica.ocean.core.urlhandlers.OceanUrlFactory;
import com.waveinformatica.ocean.core.util.FsUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;

/**
 *
 * @author Ivano
 */
public class ModuleClassLoader extends ClassLoader {

	private static final Logger logger = LoggerUtils.getCoreLogger();

	private final Date creationDate;
	private final Module module;
	private final File moduleFile;
	private final JarFile jar;
	private final Map<String, Set<String>> annotationsIndex;
	private final OceanCacheMap<String, Class<?>> classesCache = new OceanCacheMap<>();
	private final List<ModuleClassLoader> tempClassLoaders = new LinkedList<ModuleClassLoader>();
	private final Set<String> dependencies = new TreeSet<String>();
	private final ModuleAttributes moduleAttributes;
	private OceanUrlFactory urlFactory = new DefaultModuleResourceUrlFactory();

	ModuleClassLoader(Module module, File jf) throws IOException {
		
		super(ModuleClassLoader.class.getClassLoader());
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MILLISECOND, 0);
		creationDate = cal.getTime();

		this.module = module;
		this.jar = new JarFile(jf);
		this.moduleFile = new File(FsUtils.normalizePath(jf.getAbsolutePath()));
		
		if (module.getModulesManager().getUrlFactory() != null) {
			urlFactory = module.getModulesManager().getUrlFactory();
		}
		
		ModuleAttributes moduleAttributes = null;
		
		try {
			Object dependenciesObj = jar.getManifest().getMainAttributes().getValue("Dependencies");
			if (dependenciesObj != null) {
				String depStr = dependenciesObj.toString();
				String[] parts = depStr.split(",");
				for (String str : parts) {
					str = str.trim();
					if (str.length() > 0) {
						dependencies.add(str);
					}
				}
			}

			moduleAttributes = new ModuleAttributes(jar.getManifest().getMainAttributes());
			
		} catch (Exception e) {
			LoggerUtils.getCoreLogger().log(Level.WARNING, "Error reading manifest for module " + module.getName(), e);
		}
		
		this.moduleAttributes = moduleAttributes;

		// Inspect classes from JAR file for components
		AnnotationDB annotationDb = new AnnotationDB();
		inspectJarClasses(this.jar, this.jar.entries(), annotationDb);
		annotationsIndex = annotationDb.getAnnotationIndex();

	}

	public ModuleClassLoader(ModuleClassLoader loader) throws IOException {
		this(loader.module, loader.moduleFile);
		loader.tempClassLoaders.add(this);
	}
	
	/**
	 * This method returns all the class names in the module annotated
	 * with the given annotation type
	 * 
	 * @param cls the annotation type
	 * @return the set of all annotated class names
	 */
	public Set<String> getAnnotatedClassNames(Class<?> cls) {

		Set<String> res = annotationsIndex.get(cls.getName());

		if (res == null) {
			res = new HashSet<String>();
		}

		return res;
	}

	public Map<String, Set<String>> getAnnotatedClassesMap() {
		return annotationsIndex;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Class loadClass(String name) throws ClassNotFoundException {
		
		Class<?> ref = classesCache.get(name);
		if (ref != null) {
			return ref;
		}
		classesCache.remove(name);

		try {
			
			if (name.startsWith("java.") || name.startsWith("javax.")) {
				Class<?> result = super.loadClass(name);
				classesCache.put(name, result);
				return result;
			}
			
			throw new ClassNotFoundException("Looking in module");

		} catch (ClassNotFoundException e) {

			try {

				ZipEntry entry = jar.getEntry(name.replace('.', '/') + ".class");

				if (entry == null) {
					throw new ClassNotFoundException("Error looking for class \"" + name + "\" in module " + module.getName());
				}

				try {

					String pkgName = null;
					int pkgEndPos = name.lastIndexOf('.');
					if (pkgEndPos != -1) {
						pkgName = name.substring(0, pkgEndPos);
						if (getPackage(pkgName) == null) {
							definePackage(pkgName, null, null, null, null, null, null, null);
						}
					}

					InputStream input = jar.getInputStream(entry);
					ByteArrayOutputStream buffer = new ByteArrayOutputStream();
					int data = input.read();

					while (data != -1) {
						buffer.write(data);
						data = input.read();
					}

					input.close();

					byte[] classData = buffer.toByteArray();
					
					Class<?> result = defineClass(name, classData, 0, classData.length);
					
					UseClassloader annotation = (UseClassloader) result.getAnnotation(UseClassloader.class);
					if (annotation != null) {
						ClassLoader loader = annotation.value().getClassLoader();
						try {
							Method defineClassMethod = ClassLoader.class.getDeclaredMethod("defineClass", String.class, byte[].class, int.class, int.class);
							defineClassMethod.setAccessible(true);
							result = (Class) defineClassMethod.invoke(loader, name, classData, 0, classData.length);
							defineClassMethod.setAccessible(false);
						} catch (NoSuchMethodException e1) {
							LoggerUtils.getLogger(result).log(Level.WARNING, "Unable to define class " + name + " in classloader of class " + annotation.value().getName(), e1);
						} catch (SecurityException e1) {
							LoggerUtils.getLogger(result).log(Level.WARNING, "Unable to define class " + name + " in classloader of class " + annotation.value().getName(), e1);
						} catch (IllegalAccessException e1) {
							LoggerUtils.getLogger(result).log(Level.WARNING, "Unable to define class " + name + " in classloader of class " + annotation.value().getName(), e1);
						} catch (IllegalArgumentException e1) {
							LoggerUtils.getLogger(result).log(Level.WARNING, "Unable to define class " + name + " in classloader of class " + annotation.value().getName(), e1);
						} catch (InvocationTargetException e1) {
							LoggerUtils.getLogger(result).log(Level.WARNING, "Unable to define class " + name + " in classloader of class " + annotation.value().getName(), e1);
						}
					}
					classesCache.put(name, result);
					return result;

				} catch (IOException ex) {
					throw new ClassNotFoundException("Error looking for class \"" + name + "\" in module " + module.getName(), ex);
				}

			} catch (ClassNotFoundException ex) {

				if (dependencies != null && !dependencies.isEmpty()) {
					for (String d : dependencies) {
						Module dep = module.getModulesManager().getModule(d);
						if (dep != null) {
							try {
								return dep.getModuleClassLoader().loadClass(name);
							} catch (ClassNotFoundException cnfe) {
								continue;
							}
						}
					}
				}
				
				Class<?> result = super.loadClass(name);
				classesCache.put(name, result);
				return result;

			}

		}

	}
	
	public URLConnection getResourceConnection(String path) throws MalformedURLException {
		
		OceanResourceConnection connection = new OceanResourceConnection(new URL("jar:" + jar.getName() + "!" + path), jar, jar.getJarEntry(path));
		
		return connection;
		
	}

	@Override
	protected URL findResource(String name) {

		if (!module.isLoaded()) {
			return null;
		}

		ZipEntry entry = jar.getJarEntry(name);
		if (entry != null) {
			return urlFactory.createResourceUrl(jar, (JarEntry) entry);
		}

		return null;

	}

	@Override
	protected Enumeration<URL> findResources(String name) throws IOException {
		List<URL> resources = new ArrayList<URL>();
		//URL context = new URL("jar:" + moduleFile.toURI() + "!/");

		Enumeration<JarEntry> entries = jar.entries();
		while (entries.hasMoreElements()) {

			JarEntry entry = entries.nextElement();
			String resName = entry.getName();
			if ((resName.equals(name) && !name.endsWith("/"))
				  || (resName.startsWith(name) && name.endsWith("/") && !resName.equals(name))) {
				resources.add(urlFactory.createResourceUrl(jar, entry));
				//resources.add(new URL("ocean:" + module.getName() + ":" + resName));
			}
		}

		return Collections.enumeration(resources);
	}

	@Override
	public Enumeration<URL> getResources(String name) throws IOException {

		if (!name.startsWith("META-INF/") && !name.endsWith(".class") && (!module.isLoaded())) {
			return null;
		}

		return findResources(name);
	}

	@Override
	public URL getResource(String name) {

		if (!module.isLoaded()) {
			return null;
		}

		URL url = findResource(name);
		return url;
	}

	public long getResourceSize(String name) {

		if (!module.isLoaded()) {
			return -1;
		}

		if (name.endsWith("/")) {
			return -1;
		} else {
			ZipEntry entry = null;
			try {
				entry = jar.getJarEntry(name);
				if (entry != null) {
					return entry.getSize();
				}
				return -1;
			} catch (Exception e) {
				return -1;
			}
		}

	}

	@Override
	public InputStream getResourceAsStream(String name) {

		if (!module.isLoaded()) {
			return null;
		}

		URL url = getResource(name);
		if (url != null && url.toString().endsWith("/")) {
			String file = url.getFile();
			if (file.indexOf('!') != -1) {
				file = file.substring(0, file.indexOf('!'));
				try {
					List<String> resources = new ArrayList<String>();
					JarFile jar = new JarFile(file.substring("file:/".length()));
					Enumeration<JarEntry> entries = jar.entries();
					while (entries.hasMoreElements()) {
						JarEntry entry = entries.nextElement();
						if (!entry.isDirectory() && entry.getName().startsWith(name) && entry.getName().indexOf('/', name.length()) == -1) {
							resources.add(entry.getName().substring(name.length()));
						}
					}
					jar.close();
					StringBuilder builder = new StringBuilder();
					for (String s : resources) {
						builder.append(s);
						builder.append("\n");
					}
					return new ByteArrayInputStream(builder.toString().getBytes("UTF-8"));
				} catch (Exception e) {
					return null;
				}
			} else {
				return getParent().getResourceAsStream(name);
			}
		} else {
			ZipEntry entry = null;
			try {
				entry = jar.getJarEntry(name);
				if (entry != null) {
					return jar.getInputStream(entry);
				}
				return null;
			} catch (IOException e) {
				return null;
			}
		}
	}

	private void inspectJarClasses(JarFile file, Enumeration<JarEntry> entries, AnnotationDB annotationDb) {

		while (entries.hasMoreElements()) {

			JarEntry entry = entries.nextElement();

			// If a class is discovered, inspects class annotations
			if (!entry.isDirectory() && entry.getName().endsWith(".class")) {
				try {
					InputStream is = file.getInputStream(entry);
					annotationDb.scanClass(is);
					is.close();
				} catch (IOException ex) {
					logger.log(Level.WARNING, "Error parsing class file \"" + entry.getName() + "\" from module " + module.getName(), ex);
				}
			}

		}

	}

	void close() {
		try {
			for (ModuleClassLoader mcl : tempClassLoaders) {
				mcl.close();
			}
			jar.close();
		} catch (IOException ex) {
			Logger.getLogger(ModuleClassLoader.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public Module getModule() {
		return module;
	}

	public File getModuleFile() {
		return moduleFile;
	}

	Set<String> getDependencies() {
		return dependencies;
	}

	ModuleAttributes getModuleAttributes() {
		return moduleAttributes;
	}

	public <T> ModuleCreationalContext<T> getCreationalContext(Class<T> cls) {
		if (cls.getClassLoader() != this) {
			throw new IllegalArgumentException("The requested creational context is for a class not owned by this " + ModuleClassLoader.class);
		}
		return new ModuleCreationalContext<T>();
	}

	public class ModuleCreationalContext<T> implements CreationalContext<T> {

		@SuppressWarnings("unused")
		private T incompleteInstance;

		@Override
		public void push(T incompleteInstance) {
			this.incompleteInstance = incompleteInstance;
		}

		@Override
		public void release() {
			incompleteInstance = null;
		}

	}
	
	void cleanGarbage() {
		annotationsIndex.clear();
	}

}
