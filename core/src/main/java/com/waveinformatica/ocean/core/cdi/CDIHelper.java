/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.cdi;

import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.http.HttpSession;

public class CDIHelper {
	
	private static RequestScopeContext requestScopeContext;
	private static ConversationScopeContext conversationScopeContext;
	private static SessionScopeContext sessionScopeContext;
	private static GlobalSessionScopeContext globalSessionScopeContext;
	
	public static Set<Type> getTypeClosure(Class<?> cls) {
		
		Set<Type> types = new HashSet<Type>();
		
		// Same object
		types.add(cls);
		
		// All implemented interfaces
		for (Class<?> i : cls.getInterfaces()) {
			types.addAll(getTypeClosure(i));
		}
		
		// Extended classes and implemented interfaces
		while (cls != Object.class) {
			cls = cls.getSuperclass();
			if (cls == null) {
				break;
			}
			types.addAll(getTypeClosure(cls));
		}
		
		return types;
	}
	
	public static CDISession startCdiContexts(CDISession session, HttpSession httpSession) {
		
		if (session == null) {
			session = new CDISession();
		}
		
		requestScopeContext.init();
		conversationScopeContext.init();
		sessionScopeContext.init(session);
		if (httpSession != null) {
			globalSessionScopeContext.init(httpSession);
		}
		else {
			globalSessionScopeContext.init(session);
		}
		
		return session;
		
	}
	
	public static CDISession startCdiContexts(CDISession session, CDISession httpSession) {
		
		if (session == null) {
			session = new CDISession();
		}
		
		requestScopeContext.init();
		conversationScopeContext.init();
		sessionScopeContext.init(session);
		globalSessionScopeContext.init(httpSession);
		
		return session;
		
	}
	
	public static CDISession startGlobalSessionContextOnly(HttpSession httpSession) {
		
		return globalSessionScopeContext.init(httpSession);
		
	}
	
	public static void stopCdiContexts() {

		conversationScopeContext.destroy();
		requestScopeContext.destroy();
		sessionScopeContext.close();
		
	}
	
	protected static void setRequestScopeContext(RequestScopeContext requestScopeContext) {
		CDIHelper.requestScopeContext = requestScopeContext;
	}
	
	protected static void setSessionScopeContext(SessionScopeContext sessionScopeContext) {
		CDIHelper.sessionScopeContext = sessionScopeContext;
	}
	
	public static void setGlobalSessionScopeContext(GlobalSessionScopeContext globalSessionScopeContext) {
		CDIHelper.globalSessionScopeContext = globalSessionScopeContext;
	}
	
	private CDIHelper() {
		
	}

	public static void setConversationScopeContext(ConversationScopeContext conversationScopeContext) {
		CDIHelper.conversationScopeContext = conversationScopeContext;
	}
	
}
