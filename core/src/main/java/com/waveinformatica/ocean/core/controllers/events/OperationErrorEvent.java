/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.waveinformatica.ocean.core.controllers.events;

/**
 * @author Marco Merli
 */
public class OperationErrorEvent extends Event {

	private final Throwable error;
	private boolean critical = false;

	public OperationErrorEvent(Object source, Throwable error) {

		super(source, CoreEventName.OPERATION_ERROR.name());
		this.error = error;
	}

	public OperationErrorEvent(Object source, Throwable error, boolean critical) {

		super(source, CoreEventName.OPERATION_ERROR.name());
		this.error = error;
		this.critical = critical;
		
	}

	public Throwable getError()
	{
		return error;
	}

	public boolean isCritical() {
		return critical;
	}

	public void setCritical(boolean critical) {
		this.critical = critical;
	}
	
}
