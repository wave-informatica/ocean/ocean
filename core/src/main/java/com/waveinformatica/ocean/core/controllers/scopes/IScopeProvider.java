/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.scopes;

import com.waveinformatica.ocean.core.annotations.ScopeProvider;

/**
 * This interface MUST be implemented by all the {@link ScopeProvider} annotated
 * components.
 * 
 * @author ivano
 * @since 2.2.0
 */
public interface IScopeProvider {
	
	/**
	 * This method is invoked by the {@link ScopesController} and classes implementing
	 * it should add relevant scopes to the provided {@link ScopeChain} if any.
	 * Then it is expected that this method returns <code>null</code> if no scopes have
	 * been added to the chain (no work done by the provider), <code>"$"</code> if
	 * the provider wants to stop the chain evaluation or the remaining part of the path
	 * after the path used to produce the added scopes.
	 * 
	 * @param chain the {@link ScopeChain} object
	 * @param path the path to be evaluated to produce scopes
	 * @return <code>null</code> to skip this provider, <code>"$"</code> if this
	 * provider is the last one, the remaining path to continue processing the chain
	 */
	public String produce(ScopeChain chain, String path);
	
}
