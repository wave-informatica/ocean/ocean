/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.util;

import com.waveinformatica.ocean.core.cdi.GlobalSessionScoped;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.inject.Named;

/**
 * @author Ivano Culmine
 * @author Marco Merli
 * @since 2.0
 */
@Named
@GlobalSessionScoped
public class OceanSession implements Serializable {

    private static final long serialVersionUID = - 5211053858495898784L;

    private Map<String, Serializable> session;

    public OceanSession() {

        session = new HashMap<String, Serializable>();
    }

    public void put(String key, Serializable value) {
        session.put(key, value);
    }

    public boolean containsKey(String key) {
        return session.containsKey(key);
    }

    public boolean containsValue(Serializable value) {
        return session.containsValue(value);
    }

    public Serializable get(String key) {
        return session.get(key);
    }

    @SuppressWarnings("unchecked")
    public <V extends Serializable> V get(String key, Class<V> type) {
        Object v = get(key);
        if (v == null || !type.isAssignableFrom(v.getClass())) {
            return null;
        }
        return (V) v;
    }

    public Serializable remove(String key) {
        return session.remove(key);
    }

    public void clear() {
        session.clear();
    }
    
    public Set<String> keySet() {
        return session.keySet();
    }
}
