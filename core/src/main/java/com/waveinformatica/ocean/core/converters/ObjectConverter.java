/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.converters;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.modules.ClassInfoCache;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import javax.inject.Inject;

/**
 *
 * @author Ivano
 */
public class ObjectConverter implements ITypeConverter {

	@Inject
	private ClassInfoCache classInfoCache;
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private CoreController controller;
	
	@Override
	public Object fromParametersMap(final String destName, final Class destClass, Type genericType, Map<String, String[]> source, Object[] ctxObjects) {
		
		boolean nullValue = true;
		for (String k : source.keySet()) {
			if (k.startsWith(destName + ".") || k.startsWith(destName + "[")) {
				nullValue = false;
				break;
			}
		}
		
		if (nullValue) {
			return null;
		}
		
		Object obj = factory.newInstance(destClass);
		
		ObjectFiller filler = (ObjectFiller) classInfoCache.get(destClass, "typeConverters.ObjectConverter[" + destName + "]{" + destClass.getName() + "}", new ClassInfoCache.Factory() {
			@Override
			public Object create() {
				ObjectFiller filler = factory.newInstance(ObjectFiller.class);
				filler.init(controller, factory, destName, destClass);
				return filler;
			}
		});
		
		filler.fill(obj, destName, destClass, genericType, source, ctxObjects);
		
		return obj;
		
	}

	@Override
	public Object fromScalar(String value, Class destClass, Map<String, String[]> source, Object[] ctxObjects) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	
	public static class ObjectFiller {
		
		private String baseName;
		private Map<String, ObjectProperty> properties = new HashMap<String, ObjectProperty>();
		
		public ObjectFiller() {
			
		}
		
		public void init (CoreController controller, ObjectFactory factory, String baseName, Class cls) {
			
			this.baseName = baseName;
			
			Method[] methods = cls.getMethods();
			
			for (Method m : methods) {
				if (!m.getName().startsWith("set") ||
					  m.getParameterTypes().length != 1) {
					continue;
				}
				ObjectProperty prop = new ObjectProperty();
				prop.setName(m.getName().substring(3, 4).toLowerCase() + m.getName().substring(4));
				prop.setType(m.getParameterTypes()[0]);
				prop.setGenericType(m.getGenericParameterTypes()[0]);
				prop.setMethod(m);
				Class<? extends ITypeConverter> converterCls = null;
				for (Annotation a : m.getParameterAnnotations()[0]) {
					if (a instanceof Param) {
						Param p = (Param) a;
						if (p.converter() != ITypeConverter.class) {
							converterCls = p.converter();
						}
						if (p.value().length() > 0) {
							prop.setName(p.value());
						}
					}
				}
				if (converterCls == null) {
					if (prop.getType().isEnum()) {
						converterCls = EnumConverter.class;
					}
					else {
						converterCls = controller.findConverter(prop.getType().getName());
					}
				}
				if (converterCls != null) {
					prop.setConverter(converterCls);
				}
				else if (prop.getType() != String.class) {
					prop.setConverter(ObjectConverter.class);
				}
				properties.put(prop.getName(), prop);
			}
			
		}
		
		public void fill(Object obj, String destName, Class destClass, Type genericType, Map<String, String[]> source, Object[] ctxObjects) {
			for (ObjectProperty prop : properties.values()) {
				String parName = destName + "." + prop.getName();
				String parNameAssoc = destName + "[" + prop.getName() + "]";
				
				boolean modeDot = true;
				for (String k : source.keySet()) {
					if (k.startsWith(parNameAssoc)) {
						modeDot = false;
						break;
					}
				}
				
				/*String[] value = source.get(parName);
				if (value == null) {
					continue;
				}*/
				Object convertedValue = null;
				if (prop.getConverter() != null) {
					ITypeConverter conv = Application.getInstance().getObjectFactory().newInstance(prop.getConverter());
					convertedValue = conv.fromParametersMap(modeDot ? parName : parNameAssoc, prop.getType(), prop.getGenericType(), source, ctxObjects);
				}
				else if (prop.getType() == String.class) {
					String[] val = source.get(parName);
					if (val == null) {
						val = source.get(parNameAssoc);
					}
					if (val != null) {
						convertedValue = source.get(parName)[0];
					}
					else {
						convertedValue = null;
					}
				}
				try {
					prop.getMethod().invoke(obj, convertedValue);
				} catch (IllegalAccessException ex) {
					LoggerUtils.getLogger(ObjectConverter.class).log(Level.SEVERE, "Unable to set property \"" + prop.getName() + "\" to object of type \"" + obj.getClass().getName() + "\"", ex);
				} catch (IllegalArgumentException ex) {
					LoggerUtils.getLogger(ObjectConverter.class).log(Level.SEVERE, "Unable to set property \"" + prop.getName() + "\" to object of type \"" + obj.getClass().getName() + "\"", ex);
				} catch (InvocationTargetException ex) {
					LoggerUtils.getLogger(ObjectConverter.class).log(Level.SEVERE, "Unable to set property \"" + prop.getName() + "\" to object of type \"" + obj.getClass().getName() + "\"", ex);
				}
			}
		}
		
	}
	
	public static class ObjectProperty {
		
		private String name;
		private Class type;
		private Type genericType;
		private Method method;
		private Class<? extends ITypeConverter> converter;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Class getType() {
			return type;
		}

		public void setType(Class type) {
			this.type = type;
		}

		public Type getGenericType() {
			return genericType;
		}

		public void setGenericType(Type genericType) {
			this.genericType = genericType;
		}

		public Method getMethod() {
			return method;
		}

		public void setMethod(Method method) {
			this.method = method;
		}

		public Class<? extends ITypeConverter> getConverter() {
			return converter;
		}

		public void setConverter(Class<? extends ITypeConverter> converter) {
			this.converter = converter;
		}
		
	}
	
}
