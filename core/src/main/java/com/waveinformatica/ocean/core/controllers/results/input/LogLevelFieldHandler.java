/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.results.input;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import java.util.logging.Level;

/**
 *
 * @author Ivano
 */
public class LogLevelFieldHandler extends ComboFieldHandler {

	@Override
	public void init(InputResult result, CoreController.OperationInfo opInfo, CoreController.ParameterInfo paramInfo, InputResult.FormField field) {
		addOption(Level.ALL.getName(), Level.ALL.getName());
		addOption(Level.FINEST.getName(), Level.FINEST.getName());
		addOption(Level.FINER.getName(), Level.FINER.getName());
		addOption(Level.FINE.getName(), Level.FINE.getName());
		addOption(Level.CONFIG.getName(), Level.CONFIG.getName());
		addOption(Level.INFO.getName(), Level.INFO.getName());
		addOption(Level.WARNING.getName(), Level.WARNING.getName());
		addOption(Level.SEVERE.getName(), Level.SEVERE.getName());
		addOption(Level.OFF.getName(), Level.OFF.getName());
	}
	
}
