/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.compile;

import com.waveinformatica.ocean.core.annotations.OceanComponent;
import java.util.Set;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;

/**
 *
 * @author ivano
 */
@SupportedAnnotationTypes("*")
@SupportedSourceVersion(SourceVersion.RELEASE_6)
public class OceanComponentVerifier extends AbstractProcessor {
	
	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		
		Messager messager = processingEnv.getMessager();
		
		if (annotations != null) {
			for (TypeElement te : annotations) {
				OceanComponent oc = te.getAnnotation(OceanComponent.class);
				if (oc != null) {
					TypeMirror mustExtend = null;
					try {
						oc.mustExtend();
					} catch (MirroredTypeException e) {
						mustExtend = e.getTypeMirror();
					}
					if (mustExtend != null) {
						for (Element ae : roundEnv.getElementsAnnotatedWith(te)) {
							if (ae.getKind() == ElementKind.CLASS) {
								if (!processingEnv.getTypeUtils().isAssignable(ae.asType(), mustExtend)) {
									if (TypeKind.DECLARED.equals( mustExtend.getKind() ) && ( (DeclaredType) mustExtend ).asElement().getKind().isInterface()) {
										messager.printMessage(Diagnostic.Kind.ERROR, "The class must implement the interface " + mustExtend.toString(), ae);
									}
									else {
										messager.printMessage(Diagnostic.Kind.ERROR, "The class must extend the class " + mustExtend.toString(), ae);
									}
								}
							}
						}
						
					}
				}
			}
		}
		
		return false;
		
	}
	
}
