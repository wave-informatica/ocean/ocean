package com.waveinformatica.ocean.core.controllers.decorators;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.ResultDecorator;
import com.waveinformatica.ocean.core.controllers.results.HttpErrorResult;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.scopes.AbstractScope;
import com.waveinformatica.ocean.core.controllers.webservice.WebServiceInvocationScope;
import com.waveinformatica.ocean.core.controllers.webservice.WebServiceScope;
import com.waveinformatica.ocean.core.interfaces.ScopeAware;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.XmlUtils;
import com.waveinformatica.ocean.schemas.soapenv.Body;
import com.waveinformatica.ocean.schemas.soapenv.Envelope;
import com.waveinformatica.ocean.schemas.soapenv.Fault;

@ResultDecorator(classes = HttpErrorResult.class, mimeTypes = MimeTypes.XML, inScope = WebServiceInvocationScope.class)
public class WebServiceErrorDecorator implements IDecorator<HttpErrorResult>, ScopeAware {
	
	@Inject
	private XmlUtils xmlUtils;
	
	private WebServiceScope scope;
	
	@Override
	public void decorate(HttpErrorResult result, OutputStream out, MimeTypes type) {
		
		String namespace = scope.getNamespace();
		
		Envelope response = new Envelope();
		
		Body body = new Body();
		response.setBody(body);
		
		Fault fault = new Fault();
		fault.setFaultcode(new QName(WebServiceResponseDecorator.NS_SOAP_ENVELOPE, result.getStatusCode() >= 500 ? "Server" : "Client"));
		
		Throwable t = result.getThrowable();
		if (t != null) {
			if (t instanceof InvocationTargetException) {
				t = t.getCause();
			}
			String message = t.getMessage();
			if (StringUtils.isNotBlank(message)) {
				message = t.getClass().getSimpleName() + ": ";
			}
			else {
				message = t.getClass().getSimpleName();
			}
			fault.setFaultstring(message);
		}
		
		body.getAny().add(fault);
		
		body.getAny().add(new JAXBElement<Fault>(new QName(WebServiceResponseDecorator.NS_SOAP_ENVELOPE, "Fault"), Fault.class, fault));
		
		try {
			
			Marshaller marshaller = xmlUtils.getContext().createMarshaller();
			
			marshaller.marshal(new JAXBElement<Envelope>(new QName(WebServiceResponseDecorator.NS_SOAP_ENVELOPE, "Envelope"), Envelope.class, response), out);
			
		} catch (JAXBException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
		}

	}

	@Override
	public void decorate(HttpErrorResult result, HttpServletRequest request, HttpServletResponse httpResponse,
			MimeTypes type) {
		
		httpResponse.setContentType("text/xop+xml");
		
		try {
			
			decorate(result, httpResponse.getOutputStream(), type);
			
		} catch (IOException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
		}
		
	}

	@Override
	public void setScope(AbstractScope scope) {
		AbstractScope tmp = scope;
		while (tmp != null) {
			if (tmp instanceof WebServiceScope) {
				this.scope = (WebServiceScope) tmp;
				break;
			}
			tmp = tmp.getParent();
		}
	}

}
