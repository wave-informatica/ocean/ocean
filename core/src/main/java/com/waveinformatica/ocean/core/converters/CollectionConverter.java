/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.converters;

import com.waveinformatica.ocean.core.annotations.TypeConverter;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;

/**
 *
 * @author Ivano
 */
@TypeConverter({"java.util.Collection",
	"java.util.List", "java.util.ArrayList", "java.util.LinkedList",
	"java.util.Set", "java.util.HashSet", "java.util.TreeSet"})
public class CollectionConverter implements ITypeConverter {
	
	private static final Logger logger = LoggerUtils.getCoreLogger();
	
	@Inject
	private CoreController coreController;
	
	@Inject
	private ObjectFactory objectFactory;
	
	@Override
	public Object fromParametersMap(String destName, Class destClass, Type genericType, Map<String, String[]> source, Object[] ctxObjects) {
		
		Integer maxIndex = null;
		Character mode = null;
		
		String[] parameters = source.get(destName);
		if (parameters == null) {
			for (String k : source.keySet()) {
				if (k.startsWith(destName) && k.charAt(destName.length()) == '$') {
					if (mode == null || mode.equals('$')) {
						mode = '$';
						String indexPart = k.substring(destName.length() + 1);
						int pos = indexPart.indexOf('.');
						if (pos != -1) {
							try {
								int index = Integer.parseInt(indexPart.substring(0, pos));
								if (maxIndex == null) {
									maxIndex = index;
								}
								else {
									maxIndex = Math.max(maxIndex, index);
								}
							} catch (NumberFormatException e) {
								// Ignore non numeric indexes
								continue;
							}
						}
					}
					else {
						LoggerUtils.getCoreLogger().log(Level.SEVERE, "Mixing $index and [index] modes in parameter " + k);
					}
				}
				else if (k.startsWith(destName) && k.charAt(destName.length()) == '[') {
					if (mode == null || mode.equals('[')) {
						String indexPart = k.substring(destName.length() + 1);
						int pos = indexPart.indexOf(']');
						if (pos == -1) {
							LoggerUtils.getCoreLogger().log(Level.SEVERE, "Invalid format for parameter name \"" + k + "\" at position " + (destName.length() + 1) + ", expected \"index]\" or \"]\"");
							continue;
						}
						else if (pos == 0) {
							maxIndex = maxIndex != null ? maxIndex + 1 : 0;
						}
						else {
							int index = Integer.parseInt(indexPart.substring(0, pos));
							if (maxIndex == null) {
								maxIndex = index;
							}
							else {
								maxIndex = Math.max(maxIndex, index);
							}
						}
						mode = '[';
					}
					else {
						LoggerUtils.getCoreLogger().log(Level.SEVERE, "Mixing $index and [index] modes in parameter " + k);
					}
				}
			}
			if (maxIndex == null) {
				return null;
			}
		}
		
		ParameterizedType type = (ParameterizedType) genericType;
		Class cls = (Class) type.getActualTypeArguments()[0];
		Class convCls = null;
		if (maxIndex != null && (mode.equals('$') || !Collection.class.isAssignableFrom(cls))) {
			convCls = ObjectConverter.class;
		}
		else if (cls.isEnum()) {
			convCls = EnumConverter.class;
		}
		else {
			convCls = coreController.findConverter(cls.getName());
		}
		ITypeConverter converter = null;
		if (cls != String.class && convCls == null) {
			logger.warning("Unable to build collection argument \"" + destName + "\" of type \"" + destClass.getName() + "<" + cls.getName() + ">\"");
			return null;
		}
		else if (convCls != null) {
			converter = (ITypeConverter) objectFactory.newInstance(convCls);
		}
		
		Collection result = null;
		if (destClass == Collection.class || destClass == List.class || destClass == ArrayList.class) {
			result = new ArrayList(maxIndex != null ? maxIndex + 1 : parameters.length);
		}
		else if (destClass == LinkedList.class) {
			result = new LinkedList();
		}
		else if (destClass == Set.class || destClass == HashSet.class) {
			result = new HashSet();
		}
		else if (destClass == TreeSet.class) {
			result = new TreeSet();
		}
		else {
			return null;
		}
		
		if (maxIndex != null) {
			for (int i = 0; i <= maxIndex; i++) {
				if (mode.equals('$')) {
					String partName = destName + "$" + i;
					for (String k : source.keySet()) {
						if (k.startsWith(partName + '.')) {
							result.add(converter.fromParametersMap(partName, cls, null, source, ctxObjects));
							break;
						}
					}
				}
				else if (mode.equals('[')) {
					String partName = destName + "[" + i + "]";
					String partName2 = destName + "[]";
					for (String k : source.keySet()) {
						if (k.startsWith(partName + '.') || k.startsWith(partName + '[')) {
							result.add(converter.fromParametersMap(partName, cls, null, source, ctxObjects));
							break;
						}
						else if (k.startsWith(partName2)) {
							for (String v : source.get(partName2)) {
								if (cls == String.class) {
									result.add(v);
								}
								else if (converter != null) {
									result.add(converter.fromScalar(v, cls, source, ctxObjects));
								}
								else {
									result.add(null);
								}
							}
						}
					}
				}
			}
		}
		else {
			for (String v : parameters) {
				if (cls == String.class) {
					result.add(v);
				}
				else if (converter != null) {
					result.add(converter.fromScalar(v, cls, source, ctxObjects));
				}
				else {
					result.add(null);
				}
			}
		}
		
		return result;
		
	}
	
	@Override
	public Object fromScalar(String value, Class destClass, Map<String, String[]> source, Object[] ctxObjects) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	
}
