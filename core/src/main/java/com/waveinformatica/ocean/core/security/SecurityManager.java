/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.security;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.controllers.events.AuthorizeOperationEvent;
import com.waveinformatica.ocean.core.controllers.events.SecurityConfigurationEvent;
import com.waveinformatica.ocean.core.controllers.scopes.ScopeChain;

/**
 * SecurityManager is the main class for managing security in Ocean Framework. Everytime the developer needs to
 * access security related information, he can inject this bean.
 *
 * @group security
 * @author Ivano
 */
@Named
@RequestScoped
public class SecurityManager {
	
	@Inject
	private GlobalSecurityManager parent;
	
	@Inject
	private UserSession userSession;
	
	@Inject
	private CoreController coreController;
	
	private boolean authorizeGuests = true;
	private OceanUser requestUser;
	
	/**
	 * Asks if the current user is authorized to execute the specified operation.
	 *
	 * @param operationInfo the operation to authorize
	 * @param chain the operation's scope chain
	 * @return <code>true</code> if the operation is authorized
	 */
	public boolean authorize(OperationInfo operationInfo, ScopeChain chain) {
		if (authorizeGuests) {
			return true;
		}
		else {
			AuthorizeOperationEvent event = new AuthorizeOperationEvent(this, operationInfo, chain);
			coreController.dispatchEvent(event);
			
			if (event.getAuthorized() == null) {
				return isLogged();
			}
			else {
				return event.getAuthorized();
			}
		}
	}
	
	/**
	 * Returns if the login is disabled and all guest users are authorized.
	 *
	 * @return {@code true} if guests are authorized, {@code false} otherwise.
	 */
	public boolean isAuthorizeGuests() {
		return authorizeGuests;
	}
	
	/**
	 * If a module needs to configure application's security, it can use this method
	 * to set whether guest users are authorized (no login is required).
	 *
	 * @param authorizeGuests <code>true</code> to disable login
	 */
	public void setAuthorizeGuests(boolean authorizeGuests) {
		this.authorizeGuests = authorizeGuests;
	}
	
	/**
	 * Returns the current user's session
	 *
	 * @return the current user's session
	 */
	public UserSession getUserSession() {
		return userSession;
	}
	
	/**
	 * This method is useful to authenticate single requests (e.g. authenticating API calls via tokens in {@link SecurityConfigurationEvent}s).
	 *
	 * @param user the user to authenticate the request with
	 */
	public void authenticateRequest(OceanUser user) {
		this.requestUser = user;
	}
	
	/**
	 * The currently logged in user (if any). It is preferrable to use this method instead of the {@link UserSession}'s one, because it is more
	 * granular and supports request authentication.
	 *
	 * @return the current user if any, or {@code null} if none
	 */
	public OceanUser getPrincipal() {
		return requestUser != null ? requestUser : userSession.getPrincipal();
	}
	
	/**
	 * Returns if the current request is authenticated. This method supports Request authentication and is preferred to {@link UserSession}'s one
	 * in most cases.
	 *
	 * @return <code>true</code> if the current request is authenticated
	 */
	public boolean isLogged() {
		return requestUser != null || userSession.isLogged();
	}
	
	/**
	 * Returns the User's name of the currently logged-in user if any.
	 *
	 * @return the user's name if any, <code>null</code> else
	 */
	public String getUserName() {
		return requestUser != null ? requestUser.toString() : userSession.getUserName();
	}
	
}
