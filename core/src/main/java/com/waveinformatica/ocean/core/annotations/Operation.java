/*
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.annotations;

import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>This annotation defines an operation. It is mandatory to define an operation name
 * as value of this annotation.</p>
 * 
 * <p>The default output type is HTML, but it can be redefined setting the <code>defaultOutputType</code> property.
 * In this case, if not otherwise requested, it will be the output type of the operation.</p>
 * 
 * @author Ivano
 * @see OperationProvider
 * @see MimeTypes
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Operation {
    
    /**
     * the operation's name
     * 
     * @return the operation's name
     */
    String value();
    
    /**
     * the default output type for this operation (defaults to text/html)
     * 
     * @return the default output type
     */
    MimeTypes defaultOutputType() default MimeTypes.HTML;
    
}
