/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.decorators;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.GsonBuilder;
import com.waveinformatica.ocean.core.annotations.ResultDecorator;
import com.waveinformatica.ocean.core.controllers.results.HttpErrorResult;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;

/**
 *
 * @author Ivano
 */
@ResultDecorator(
	classes = { Object.class },
	mimeTypes = {
		MimeTypes.JSON
	})
public class JsonDecorator implements IDecorator {

	@Override
	public void decorate(Object result, OutputStream out, MimeTypes type)
	{
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, Charset.forName("UTF-8")));
		try {
			
			decorate(result, writer);
			
		} finally {
			try {
				writer.close();
			} catch (IOException e) {
				LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unexpected error closing buffered writer", e);
			}
		}
	}

	@Override
	public void decorate(Object result, HttpServletRequest request, HttpServletResponse response, MimeTypes type)
	{
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		response.setHeader("Expires", "0"); // Proxies.
		
		if (result instanceof HttpErrorResult) {
			response.setStatus(((HttpErrorResult) result).getStatusCode());
		}

		try {
			
			Writer writer = response.getWriter();
			
			if (request.getParameter("callback") != null) {
				
				response.setContentType("text/javascript");
				
				writer.write(request.getParameter("callback"));
				writer.write("(");
			}
			
			decorate(result, response.getWriter());
			
			if (request.getParameter("callback") != null) {
				writer.write(");");
			}
                        
                        writer.flush();
                        //writer.close();
			
		}
		catch (IOException ex) {
			Logger.getLogger(JsonDecorator.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	protected void decorate(Object result, Writer writer)
	{
		
		GsonBuilder builder = JsonUtils.getBuilder();

		builder.create().toJson(result, writer);
		
		try {
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
