/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.converters;

import com.waveinformatica.ocean.core.annotations.TypeConverter;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
@TypeConverter({"java.lang.Boolean", "boolean"})
public class BooleanConverter implements ITypeConverter {
    
    private static final Logger logger = LoggerUtils.getCoreLogger();

    @Override
    public Object fromParametersMap(String destName, Class destClass, Type genericType, Map<String, String[]> source, Object[] ctxObjects) {
        
        Boolean result = null;
        
        String[] args = source.get(destName);
        if (args != null && args.length > 0 && StringUtils.isNotBlank(args[0])) {
            String val = args[0].trim();
            if (val.equalsIgnoreCase("true") || val.equalsIgnoreCase("on")) {
                result = true;
            }
            else if (val.equalsIgnoreCase("false") || val.equalsIgnoreCase("off")) {
                result = false;
            }
            else {
                try {
                    Double d = Double.parseDouble(val);
                    if (d.equals(0.0)) {
                        result = false;
                    }
                    else {
                        result = true;
                    }
                } catch (NumberFormatException e) {
                    logger.warning("Unable to parse request parameter \"" + destName + "\" as a double for boolean evaluation: \"" + val + "\"");
                }
            }
        }
        
        if (result == null) {
            if (destClass.isPrimitive()) {
                result = false;
            }
        }
        
        return result;
        
    }

    @Override
    public Object fromScalar(String value, Class destClass, Map<String, String[]> source, Object[] ctxObjects) {
        
        if (StringUtils.isNotBlank(value)) {
            if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("on")) {
                return true;
            }
            else if (value.equalsIgnoreCase("false") || value.equalsIgnoreCase("off")) {
                return false;
            }
            else {
                try {
                    Double d = Double.parseDouble(value);
                    if (d.equals(0.0)) {
                        return false;
                    }
                    else {
                        return true;
                    }
                } catch (NumberFormatException e) {
                    logger.warning("Unable to parse boolean value as a double for boolean evaluation: \"" + value + "\"");
                }
            }
        }
        
        return null;
        
    }
    
}
