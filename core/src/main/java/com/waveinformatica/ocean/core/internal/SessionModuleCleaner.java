/*
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.internal;

import com.waveinformatica.ocean.core.modules.Module;
import com.waveinformatica.ocean.core.security.UserSession;
import com.waveinformatica.ocean.core.util.OceanSession;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Ivano
 */
@Named
@Dependent
public class SessionModuleCleaner {
    
    @Inject
    private UserSession userSession;
    
    @Inject
    private OceanSession oceanSession;
    
    public void unloadSessionObjects(Module module) {
        
        if (userSession != null && userSession.isLogged() && userSession.getPrincipal().getClass().getClassLoader() == module.getModuleClassLoader()) {
            userSession.logout();
        }
        
        if (oceanSession != null) {
            Set<String> removeKeys = new HashSet<String>();

            for (String k : oceanSession.keySet()) {
                Object o = oceanSession.get(k);
                if (o == null) {
                    continue;
                }
                if (o.getClass().getClassLoader() == module.getModuleClassLoader()) {
                    removeKeys.add(k);
                }
            }

            for (String k : removeKeys) {
                oceanSession.remove(k);
            }
        }
        
    }
    
}
