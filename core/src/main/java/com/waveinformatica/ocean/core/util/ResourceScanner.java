/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;

import com.waveinformatica.ocean.core.controllers.dto.ResourceInfo;
import com.waveinformatica.ocean.core.modules.ModuleClassLoader;

/**
 * This class allows to scan a class loader resources set to find resources
 *
 * @author Ivano
 */
public class ResourceScanner {

	private static final Logger logger = LoggerUtils.getCoreLogger();

	/**
	 * Scans the classloader accessible resources to find resources in the
	 * given path
	 *
	 * @param classLoader ClassLoader to scan resources
	 * @param path the path to scan into
	 * @param openStream return {@link ResourceInfo}s with open
	 * {@link InputStream}
	 * @return the list of resources info found in the requested path
	 */
	public static List<ResourceInfo> findResources(ClassLoader classLoader, String path, boolean openStream) {

		List<ResourceInfo> resources = new ArrayList<ResourceInfo>();

		try {

			if (classLoader instanceof ModuleClassLoader) {

				Enumeration<URL> urls = classLoader.getResources(path);

				ModuleClassLoader mcl = (ModuleClassLoader) classLoader;
				String prefix = mcl.getModuleFile().getName() + "!/";

				while (urls.hasMoreElements()) {

					URL url = urls.nextElement();
					String strUrl = url.toExternalForm();

					String resName = strUrl.substring("ocean:".length());
					resName = resName.substring(resName.indexOf(':') + 1);

					ResourceInfo res = new ResourceInfo();
					res.setUrl(url);
					res.setLastModified(mcl.getCreationDate());
					res.setName(resName);
					if (openStream) {
						res.setInputStream(mcl.getResourceAsStream(resName));
					}

					resources.add(res);

				}

			} else {
				List<URL> urls = findResources(classLoader, path);
				for (URL url : urls) {

					String strUrl = url.toExternalForm();

					ResourceInfo res = new ResourceInfo();
					res.setUrl(url);
					URLConnection connection = url.openConnection();
					res.setLastModified(new Date(connection.getLastModified()));
					if (openStream) {
						res.setInputStream(connection.getInputStream());
					}

					resources.add(res);

				}
			}

		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error reading resources for path " + path + " in class-loader " + classLoader.getClass().getName(), e);
		}

		return resources;

	}

	/**
	 * <p>
	 * Scans the classloader accessible resources to find resources in the
	 * given path.</p>
	 * <p>
	 * This method is unstable in some cases reading resources from modules,
	 * and for this reason has been marked as <code>@Deprecated</code>. Prefer
	 * the <code>{@link List}&lt;{@link ResourceInfo}&gt;
	 * {@link #findResources(ClassLoader, String, boolean) findResources}({@link ClassLoader}
	 * classLoader, {@link String} path, boolean openStream)</code> version
	 * instead.</p>
	 *
	 * @param classLoader ClassLoader to scan resources
	 * @param path the path to scan into
	 * @return the list of resource names found in the requested path
	 */
	@Deprecated
	public static List<URL> findResources(ClassLoader classLoader, String path) {
		try {
			List<URL> resourceUrls = new ArrayList<URL>();
			Enumeration<URL> urls = classLoader.getResources(path);
			while (urls.hasMoreElements()) {
				URL url = urls.nextElement();
				List<String> resources = null;
				if (url.getProtocol().equalsIgnoreCase("jar")) {
					String resPath = url.getFile().substring("file:/".length());
					if (!resPath.startsWith("/")) {
						resPath = "/" + resPath;
					}
					String inPath = resPath.substring(resPath.indexOf('!') + 1);
					resPath = resPath.substring(0, resPath.indexOf('!'));
					resources = findResourcesInJar(resPath, inPath);
				} else if (url.getProtocol().equalsIgnoreCase("file")) {
					String pathIn18 = URLDecoder.decode(url.getFile(), "UTF-8");
					LoggerUtils.getCoreLogger().log(Level.INFO, "findResourcesInFileSystem: Looking for i18n resources in " + pathIn18);
					resources = findResourcesInFileSystem(pathIn18);
				} else if (url.getProtocol().equalsIgnoreCase("vfs")) {
					JarInputStream jis = (JarInputStream) url.openStream();
					JarEntry entry = null;
					resources = new ArrayList<String>();
					while ((entry = jis.getNextJarEntry()) != null) {
						resources.add(entry.getName());
					}
				} else {
					resources = IOUtils.readLines(classLoader.getResourceAsStream(path), Charsets.UTF_8);
				}

				if (resources != null) {
					String baseUrl = url.toString();
					if (!baseUrl.endsWith("/")) {
						baseUrl += "/";
					}
					for (String res : resources) {
						resourceUrls.add(new URL(baseUrl + res));
					}
				}
			}
			return resourceUrls;
		} catch (IOException ex) {
			logger.log(Level.SEVERE, null, ex);
		}
		return null;
	}

	private static List<String> findResourcesInJar(String jarFile, String path) {

		path = path.substring(1);

		List<String> resources = new ArrayList<String>();

		try {

			if (jarFile.indexOf('%') != -1) {
				jarFile = URLDecoder.decode(jarFile, "UTF-8");
			}

			JarFile jar = new JarFile(jarFile);

			Enumeration<JarEntry> entries = jar.entries();
			while (entries.hasMoreElements()) {
				JarEntry entry = entries.nextElement();
				if (entry.getName().startsWith(path)) {
					String resName = entry.getName().substring(path.length());
					if (resName.length() > 0 && resName.indexOf('/') == -1) {
						resources.add(resName);
					}
				}
			}

			jar.close();

		} catch (IOException ex) {
			Logger.getLogger(ResourceScanner.class.getName()).log(Level.SEVERE, null, ex);
		}

		return resources;
	}

	private static List<String> findResourcesInFileSystem(String path) {

		List<String> result = new ArrayList<String>();

		File file = new File(path);
		
		File[] files = file.listFiles();
		if (files != null) {
			for (File f : files) {
				if (f.isFile()) {
					result.add(f.getName());
				}
			}
		}

		return result;

	}

}
