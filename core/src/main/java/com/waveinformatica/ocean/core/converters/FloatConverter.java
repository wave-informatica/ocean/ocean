/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.converters;

import com.waveinformatica.ocean.core.annotations.TypeConverter;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
@TypeConverter({"java.lang.Float", "float"})
public class FloatConverter implements ITypeConverter {

    private static final Logger logger = LoggerUtils.getCoreLogger();
    
    @Override
    public Object fromParametersMap(String destName, Class destClass, Type genericType, Map<String, String[]> source, Object[] ctxObjects) {
        
        String[] args = source.get(destName);
        
        if (args != null && args.length == 1 && StringUtils.isNotBlank(args[0])) {
            try {
                return Float.parseFloat(args[0].replace(',', '.'));
            } catch (NumberFormatException e) {
                logger.warning("Unable to parse float request parameter \"" + destName + "\": \"" + args[0] + "\"");
            }
        }
        
        if (destClass.isPrimitive()) {
            return 0.0f;
        }
        
        return null;
        
    }

    @Override
    public Object fromScalar(String value, Class destClass, Map<String, String[]> source, Object[] ctxObjects) {
        
        if (StringUtils.isNotBlank(value)) {
            try {
                return Float.parseFloat(value.replace(',', '.'));
            } catch (NumberFormatException e) {
                logger.warning("Unable to parse float value \"" + value + "\"");
            }
        }
        
        return null;
        
    }
    
}
