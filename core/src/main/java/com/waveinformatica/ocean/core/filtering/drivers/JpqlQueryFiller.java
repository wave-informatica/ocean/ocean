package com.waveinformatica.ocean.core.filtering.drivers;

import java.util.Iterator;
import java.util.logging.Level;

import javax.persistence.TypedQuery;

import com.waveinformatica.ocean.core.util.LoggerUtils;

public class JpqlQueryFiller<T> implements QueryFiller<TypedQuery<T>> {
	
	private final ValuesManager values;
	
	public JpqlQueryFiller(ValuesManager values) {
		this.values = values;
	}
	
	@Override
	public TypedQuery<T> fillQueryParams(TypedQuery<T> query) {
		
		Iterator<DataValue> iter = values.iterator();
		while (iter.hasNext()) {
			DataValue dv = iter.next();
			try {
				query.setParameter(dv.getName(), dv.getValue());
			} catch (IllegalArgumentException e) {
				LoggerUtils.getCoreLogger().log(Level.WARNING, "Unable to set parameter " + dv.getName() + " in JPQL query: " + e.getClass().getName() + ": " + e.getMessage());
			}
		}
		
		return query;
	}

}
