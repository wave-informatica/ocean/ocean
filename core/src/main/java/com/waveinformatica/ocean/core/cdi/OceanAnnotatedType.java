/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.cdi;

import com.waveinformatica.ocean.core.modules.ModuleClassLoader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.spi.AnnotatedConstructor;
import javax.enterprise.inject.spi.AnnotatedField;
import javax.enterprise.inject.spi.AnnotatedMethod;
import javax.enterprise.inject.spi.AnnotatedType;

/**
 *
 * @author Ivano
 */
public class OceanAnnotatedType<T> implements AnnotatedType<T> {

	private final Class<T> cls;
	private final Set<AnnotatedConstructor<T>> constructors = new HashSet<AnnotatedConstructor<T>>();
	private final Set<AnnotatedMethod<? super T>> methods = new HashSet<AnnotatedMethod<? super T>>();
	private final Set<AnnotatedField<? super T>> fields = new HashSet<AnnotatedField<? super T>>();
	private final Set<Type> typeClosure;

	public OceanAnnotatedType(Class<T> cls) {
		this.cls = cls;

		// Constructors
		for (Constructor<?> c : cls.getConstructors()) {
			constructors.add(new OceanAnnotatedConstructor<T>(this, (Constructor<T>) c));
		}

		// Methods
		for (Method m : cls.getMethods()) {
			methods.add(new OceanAnnotatedMethod<T>(this, m));
		}

		// Fields
		while (cls != null && cls != Object.class) {
			Field[] fields = cls.getDeclaredFields();
			for (Field f : fields) {
				this.fields.add(new OceanAnnotatedField<T>(this, f));
			}
			cls = (Class<T>) cls.getSuperclass();
		}

		// Type Closure
		typeClosure = CDIHelper.getTypeClosure(this.cls);
	}

	@Override
	public Class<T> getJavaClass() {
		return cls;
	}

	@Override
	public Set<AnnotatedConstructor<T>> getConstructors() {
		return constructors;
	}

	@Override
	public Set<AnnotatedMethod<? super T>> getMethods() {
		return methods;
	}

	@Override
	public Set<AnnotatedField<? super T>> getFields() {
		return fields;
	}

	@Override
	public Type getBaseType() {
		return cls;
	}

	@Override
	public Set<Type> getTypeClosure() {
		return typeClosure;
	}

	@Override
	public <T extends Annotation> T getAnnotation(Class<T> annotationType) {
		return cls.getAnnotation(annotationType);
	}

	@Override
	public Set<Annotation> getAnnotations() {
		Set<Annotation> annotations = new HashSet<Annotation>();
		for (Annotation a : cls.getAnnotations()) {
			annotations.add(a);
		}
		if (cls.getClassLoader() instanceof ModuleClassLoader) {
			annotations.add(new Dependent() {
				@Override
				public Class<? extends Annotation> annotationType() {
					return Dependent.class;
				}
			});
		}
		return annotations;
	}

	@Override
	public boolean isAnnotationPresent(Class<? extends Annotation> annotationType) {
		return cls.isAnnotationPresent(annotationType);
	}

}
