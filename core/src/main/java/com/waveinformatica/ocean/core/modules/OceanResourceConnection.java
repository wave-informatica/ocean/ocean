package com.waveinformatica.ocean.core.modules;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class OceanResourceConnection extends URLConnection {
	
	private final JarFile jar;
	private final JarEntry entry;
	
	public OceanResourceConnection(URL url, JarFile jar, JarEntry entry) {
		super(url);
		this.jar = jar;
		this.entry = entry;
	}

	@Override
	public void connect() throws IOException {
		
	}
	
	@Override
	public InputStream getInputStream() throws IOException {
		return jar.getInputStream(entry);
	}

}
