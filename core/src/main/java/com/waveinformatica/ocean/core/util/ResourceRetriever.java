/*
 * Copyright 2017, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLStreamHandler;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.cdi.CDIHelper;
import com.waveinformatica.ocean.core.cdi.CDISession;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.InstanceListener;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.OperationContext;
import com.waveinformatica.ocean.core.controllers.dto.ResourceInfo;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.results.StreamResult;
import com.waveinformatica.ocean.core.exceptions.OperationNotFoundException;
import com.waveinformatica.ocean.core.internal.OceanUserAgentCallback;
import com.waveinformatica.ocean.core.network.NetworkManager;

/**
 *
 * @author Ivano
 */
public class ResourceRetriever {
	
	private static Pattern dataUriPattern = Pattern.compile("^data:(\\w+\\/[-+.\\w]+)?((;[^;,]+)*),(.+)");

	@Inject
	private ObjectFactory factory;
	
	@Inject
	private CoreController controller;

	@Inject
	private GlobalClassLoader globalClassLoader;

	@Inject
	private Configuration config;
	
	@Inject
	private NetworkManager network;

	public ResourceInfo getResource(String uri) {
		
		if (uri.startsWith("data:")) {
			try {
				Matcher matcher = dataUriPattern.matcher(uri);
				if (matcher.find()) {
					String mediaType = matcher.group(1);
					boolean base64 = false;
					String charset = "UTF-8";
					if (StringUtils.isNotBlank(matcher.group(2))) {
						for (String p : matcher.group(2).toLowerCase().split(";")) {
							if (StringUtils.isBlank(p)) {
								continue;
							}
							if ("base64".equals(p)) {
								base64 = true;
							}
							if (p.startsWith("charset=")) {
								charset = p.substring("charset=".length()).toUpperCase();
								while (charset.startsWith("\"") || charset.startsWith("'")) {
									charset = charset.substring(1);
								}
								while (charset.endsWith("\"") || charset.endsWith("'")) {
									charset = charset.substring(0, charset.length() - 1);
								}
							}
						}
					}
					String data = URLDecoder.decode(matcher.group(4), charset);
					ResourceInfo ri = new ResourceInfo();
					ri.setLastModified(new Date());
					ri.setName("unknown");
					for (MimeTypes mt : MimeTypes.values()) {
						if (mt.getMimeType().equals(mediaType)) {
							ri.setMimeType(mt);
							ri.setName(ri.getName() + "." + mt.getExtensions()[0]);
							break;
						}
					}
					if (base64) {
						byte[] bytes = Base64.decodeBase64(data);
						ri.setInputStream(new ByteArrayInputStream(bytes));
						ri.setLength((long) bytes.length);
					}
					else {
						byte[] bytes = data.getBytes(Charset.forName(charset));
						ri.setInputStream(new ByteArrayInputStream(bytes));
						ri.setLength((long) bytes.length);
					}
					return ri;
				}
			} catch (UnsupportedEncodingException e) {
				LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
			}
		}
		
		try {

			URL url = new URL(uri);
			URLConnection conn = network.getURLConnection(url);
			long lastModified = conn.getLastModified();
			String cntType = conn.getContentType();
			MimeTypes mt = null;
			if (StringUtils.isNotBlank(cntType)) {
				int pos = cntType.indexOf(';');
				if (pos >= 0) {
					cntType = cntType.substring(0, pos);
				}
				for (MimeTypes type : MimeTypes.values()) {
					if (type.getMimeType().equals(cntType)) {
						mt = type;
						break;
					}
				}
			}

			InputStream is = conn.getInputStream();

			ResourceInfo ri = new ResourceInfo();
			ri.setUrl(url);
			ri.setInputStream(is);
			ri.setLastModified(new Date(lastModified));
			ri.setMimeType(mt);
			ri.setName(url.getFile().substring(url.getFile().lastIndexOf('/') + 1));

			return ri;

		} catch (MalformedURLException e) {
			LoggerUtils.getLogger(OceanUserAgentCallback.class).log(Level.INFO, e.getMessage());
		} catch (IOException ex) {
			LoggerUtils.getLogger(OceanUserAgentCallback.class).log(Level.SEVERE, null, ex);
		}

		UrlBuilder url = new UrlBuilder(uri);

		String operation = url.getUrl(false);
		if (!operation.startsWith("/")) {
			operation = "/" + operation;
		}
		try {
			operation = FsUtils.normalizePath(operation);
			OperationContext opCtx = new OperationContext();
			Object result = null;
			if (Context.get() != null && Context.get().getRequest() != null) {
				result = controller.executeOperation(operation, url.getParameters().build(), opCtx, Context.get().getRequest());
			} else {
				result = controller.executeOperation(operation, url.getParameters().build(), opCtx);
			}
			if (result != null) {
				// Look for a decorator
				List<MimeTypes> types = new LinkedList<MimeTypes>();
				types.add(opCtx.getOperation().getOperation().defaultOutputType());
				if (!types.contains(MimeTypes.HTML)) {
					types.add(MimeTypes.HTML);
				}
				
				DecoratorHelper helper = factory.newInstance(DecoratorHelper.class);
				
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				helper.decorateResult(result, baos, opCtx, types.toArray(new MimeTypes[types.size()]));
				
				byte[] bytes = baos.toByteArray();
				LoggerUtils.getCoreLogger().log(Level.INFO, "Returning " + bytes.length + " bytes from a " + (result != null ? result.getClass().getName() : "[null result]") + " through a " + (opCtx.getDecoratorInfo() != null ? opCtx.getDecoratorInfo().getDecorator().getClass().getName() : "[null decorator]"));

				ResourceInfo ri = new ResourceInfo();
				ri.setInputStream(new ByteArrayInputStream(bytes));
				ri.setLastModified(new Date());
				if (result instanceof BaseResult) {
					ri.setName(((BaseResult) result).getName());
					if (StringUtils.isBlank(ri.getName())) {
						ri.setName(operation.substring(operation.lastIndexOf('/') + 1) + opCtx.getDecoratorInfo().getMimeType().getExtensions()[0]);
					} else if (ri.getName().indexOf('.') < 0) {
						ri.setName(ri.getName() + "." + opCtx.getDecoratorInfo().getMimeType().getExtensions()[0].toLowerCase());
					}
				}
				if (result instanceof StreamResult) {
					for (MimeTypes mt : MimeTypes.values()) {
						if (((StreamResult) result).getContentType().startsWith(mt.getMimeType())) {
							ri.setMimeType(mt);
							break;
						}
					}
				}
				if (ri.getMimeType() == null && opCtx.getDecoratorInfo() != null) {
					ri.setMimeType(opCtx.getDecoratorInfo().getMimeType());
				}
				return ri;
				
			}
		} catch (OperationNotFoundException ex) {

			uri = FsUtils.normalizePath(url.getUrl(false));

			if (uri.startsWith("ris/")) {

				String theme = config.getSiteProperty("theme", "");

				ResourceInfo res = null;
				if (StringUtils.isNotBlank(theme)) {
					res = globalClassLoader.getResourceInfo("themes/" + theme + uri, true);
				}
				if (res == null) {
					res = globalClassLoader.getResourceInfo(uri, true);
				}

				if (res != null) {
					MimeTypes mt = null;
					String ext = uri.substring(uri.lastIndexOf(".") + 1);
					external:
					for (MimeTypes type : MimeTypes.values()) {
						for (String extension : type.getExtensions()) {
							if (extension.equals(ext)) {
								mt = type;
								break external;
							}
						}
					}

					res.setMimeType(mt);
					res.setName(uri.substring(uri.lastIndexOf('/') + 1));
				}

				return res;

			}
		}

		return null;
	}
	
	public static class URLHandler extends URLStreamHandler {
		
		@Inject
		private ObjectFactory factory;
		
		@Inject
		private CoreController controller;
		
		private final String protocol;
		
		public URLHandler() {
			
			protocol = "ocean-" + UUID.randomUUID().toString();
			
		}
		
		public String getProtocol() {
			return protocol;
		}

		@Override
		protected URLConnection openConnection(URL u) throws IOException {
			
			if (!u.getProtocol().equals(protocol)) {
				return null;
			}
			
			LoggerUtils.getCoreLogger().log(Level.SEVERE, Thread.currentThread().getName() + ": Requested URL: " + u.toExternalForm());
			
			Context.setCoreController(controller);
			Context.init(Application.getInstance().getContext());
			CDISession cdiSession = new CDISession();
			CDIHelper.startCdiContexts(cdiSession, cdiSession);
			
			try {
				
				ResourceRetriever retriever = factory.newInstance(ResourceRetriever.class);
				
				String path = u.toExternalForm().substring(protocol.length() + ":".length());
				while (path.startsWith("/")) {
					path = path.substring(1);
				}
				
				ResourceInfo info = retriever.getResource(path);
				
				if (info == null) {
					throw new FileNotFoundException(u.toExternalForm());
				}
				
				return new OceanURLConnection(u, info);
				
			} finally {
				Context.clear();
				Context.setCoreController(null);
				CDIHelper.stopCdiContexts();
				InstanceListener.get().clear();
			}
		}
		
		public static class OceanURLConnection extends URLConnection {
			
			private ResourceInfo info;
			
			protected OceanURLConnection(URL url, ResourceInfo info) {
				super(url);
				this.info = info;
			}
			
			@Override
			public Map<String, List<String>> getHeaderFields() {
				Map<String, List<String>> map = new HashMap<String, List<String>>(super.getHeaderFields());
				map.put("access-control-allow-origin", Arrays.asList("*"));
				return map;
			}
			
			@Override
			public String getHeaderField(String name) {
				if ("access-control-allow-origin".equals(name)) {
					return "*";
				}
				else if ("content-type".equals(name)) {
					return info != null && info.getMimeType() != null ? info.getMimeType().getMimeType() : null;
				}
				else if ("content-length".equals(name)) {
					return info != null && info.getLength() != null ? info.getLength().toString() : null;
				}
				return super.getHeaderField(name);
			}
			
			@Override
			public void connect() throws IOException {
				if (info.getInputStream() == null) {
					info.setInputStream(info.getUrl().openStream());
				}
			}

			@Override
			public InputStream getInputStream() throws IOException {
				connect();
				return info.getInputStream();
			}
			
		}
		
	}

}
