/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.controllers.decorators;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.waveinformatica.ocean.core.annotations.ResultDecorator;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.decorators.ExcelTableDecorator.ExcelRowHandler;
import com.waveinformatica.ocean.core.controllers.dto.OperationContext;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.results.TabPanelResult;
import com.waveinformatica.ocean.core.controllers.results.TabPanelResult.Tab;
import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.core.exceptions.OperationNotFoundException;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.DecoratorHelper;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.ParametersBuilder;
import com.waveinformatica.ocean.core.util.UrlBuilder;

@ResultDecorator(classes = TabPanelResult.class, mimeTypes = {MimeTypes.EXCEL})
public class ExcelTabPanelDecorator implements IDecorator<TabPanelResult> {
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private CoreController controller;
	
	@Override
	public void decorate(TabPanelResult result, OutputStream out, MimeTypes type) {
		
		Workbook wb = new SXSSFWorkbook();
		
		try {
			
			int sheetIndex = 0;
			
			for (Tab tab : result.getPanel()) {
				try {
					
					ParametersBuilder builder = new ParametersBuilder(Context.get().getRequest());
					UrlBuilder urlBuilder = new UrlBuilder(tab.getOperation());
					Map<String, String[]> query = urlBuilder.getParameters().build();
					for (String p : query.keySet()) {
						builder.replace(p, query.get(p));
					}
					
					Object tabRes = controller.executeOperation(urlBuilder.getUrl(false), builder.build(), Context.get().getRequest());
					
					if (tabRes instanceof TableResult) {
						
						ExcelTableDecorator decorator = factory.newInstance(ExcelTableDecorator.class);
						
						TableResult tbl = (TableResult) tabRes;
						
						tbl.setRowHandler(factory.newInstance(ExcelRowHandler.class));
						
						tbl.analyzeData();
						
						try {
							
							Sheet sh = wb.createSheet(StringUtils.defaultIfBlank(tbl.getName(), "Sheet " + (++sheetIndex)));
							
							decorator.decorate(tbl, sh);
							
						} catch (Exception e) {
							LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
						}
						
					}
					else if (tabRes != null) {
						
						DecoratorHelper helper = factory.newInstance(DecoratorHelper.class);
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						OperationContext opCtx = new OperationContext();
						helper.decorateResult(tabRes, baos, opCtx, type);
						
						XSSFWorkbook wbl = new XSSFWorkbook(new ByteArrayInputStream(baos.toByteArray()));
						baos = null;
						for (int i = 0; i < wbl.getNumberOfSheets(); i++) {
							copySheet(wbl.getSheetAt(i), wb);
						}
						wbl.close();
						
					}
					
				} catch (OperationNotFoundException e) {
					LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
				}
			}
			
			wb.write(out);
			
		} catch (IOException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
		} finally {
			try {
				wb.close();
			} catch (IOException e) {
				LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
			}
		}
		
	}

	@Override
	public void decorate(TabPanelResult result, HttpServletRequest request, HttpServletResponse response, MimeTypes type) {
		
		if (result.getName() == null) {
			result.setName("export");
		}
		response.setContentType(type.getMimeType());
		response.setHeader("Content-disposition", "attachment; filename=\"" + result.getName() + ".xlsx\"");
		
		try {
			
			decorate(result, response.getOutputStream(), type);
			
		} catch (IOException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
		}
		
	}
	
	private void copySheet(Sheet sheet, Workbook destination) {
		
		Map<Integer, CellStyle> stylesMap = new HashMap<>();
		int maxColumnNum = 0;
		
		Integer index = null;
		Sheet dest = null;
		do {
			String name = sheet.getSheetName() + (index != null ? "_" + index.toString() : "");
			if (destination.getSheet(name) == null) {
				dest = destination.createSheet(name);
			}
			else {
				index = index != null ? index + 1 : 1;
			}
		} while (dest == null);
		
		for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
			Row row = sheet.getRow(i);
			Row drow = dest.createRow(i);
			if (row != null) {
				copyRow(dest, sheet, drow, row, stylesMap);
				if (row.getLastCellNum() > maxColumnNum) {
					maxColumnNum = row.getLastCellNum();
				}
			}
		}
		for (int i = 0; i <= maxColumnNum; i++) {
			dest.setColumnWidth(i, sheet.getColumnWidth(i));
		}
	}
	
	private void copyRow(Sheet dest, Sheet source, Row drow, Row srow, Map<Integer, CellStyle> stylesMap) {
		Set<CellRangeAddress> mergedRegions = new TreeSet<>();
		for (int j = srow.getFirstCellNum(); j <= srow.getLastCellNum(); j++) {
            Cell oldCell = srow.getCell(j);
            Cell newCell = drow.getCell(j);
            if (oldCell != null) {
                if (newCell == null) {
                    newCell = drow.createCell(j);
                }
                copyCell(oldCell, newCell, stylesMap);
                CellRangeAddress mergedRegion = getMergedRegion(source, srow.getRowNum(), (short) j);
                if (mergedRegion != null) {
                	CellRangeAddress newMergedRegion = mergedRegion.copy();
                	if (!mergedRegions.contains(newMergedRegion)) {
                        mergedRegions.add(newMergedRegion);
                        dest.addMergedRegion(newMergedRegion);
                    }
                }
            }
        }
		drow.setHeight(srow.getHeight());
	}
	
	private void copyCell(Cell oldCell, Cell newCell, Map<Integer, CellStyle> styleMap) {
        if(styleMap != null) {
            if(oldCell.getSheet().getWorkbook() == newCell.getSheet().getWorkbook()){
                newCell.setCellStyle(oldCell.getCellStyle());
            } else{
                int stHashCode = oldCell.getCellStyle().hashCode();
                CellStyle newCellStyle = styleMap.get(stHashCode);
                if(newCellStyle == null){
                    newCellStyle = newCell.getSheet().getWorkbook().createCellStyle();
                    newCellStyle.cloneStyleFrom(oldCell.getCellStyle());
                    styleMap.put(stHashCode, newCellStyle);
                }
                newCell.setCellStyle(newCellStyle);
            }
        }
        switch(oldCell.getCellTypeEnum()) {
            case STRING:
                newCell.setCellValue(oldCell.getStringCellValue());
                break;
            case NUMERIC:
                newCell.setCellValue(oldCell.getNumericCellValue());
                break;
            case BLANK:
                newCell.setCellType(CellType.BLANK);
                break;
            case BOOLEAN:
                newCell.setCellValue(oldCell.getBooleanCellValue());
                break;
            case ERROR:
                newCell.setCellErrorValue(oldCell.getErrorCellValue());
                break;
            case FORMULA:
                newCell.setCellFormula(oldCell.getCellFormula());
                break;
            default:
                break;
        }
         
    }
	
	private CellRangeAddress getMergedRegion(Sheet sheet, int rowNum, short cellNum) {
        for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
            CellRangeAddress merged = sheet.getMergedRegion(i);
            if (merged.containsRow(rowNum) && merged.containsColumn(cellNum)) {
                return merged;
            }
        }
        return null;
    }
	
}
