/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.templates.directives;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.annotations.FreemarkerDirective;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.Event;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.templates.BaseResultAdapter;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.Results;

import freemarker.core.Environment;
import freemarker.template.TemplateBooleanModel;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;

/**
 *
 * @author Ivano
 */
@FreemarkerDirective("head")
public class HtmlHeaderDirective implements TemplateDirectiveModel {

	private static final String CSS_ATTR = "css";
	private static final String SCRIPTS_ATTR = "scripts";
	
	private static long lastUpdate = System.currentTimeMillis();
	
	private Field rootDataModel = null;

	@Override
	public void execute(Environment e, Map map, TemplateModel[] tms, TemplateDirectiveBody tdb) throws TemplateException, IOException
	{
		BaseResult result = null;
		
		try {
			if (rootDataModel == null) {
				rootDataModel = e.getClass().getDeclaredField("rootDataModel");
				rootDataModel.setAccessible(true);
			}
			BaseResultAdapter adapter = (BaseResultAdapter) rootDataModel.get(e);
			result = adapter.getResult();
		} catch (NoSuchFieldException ex) {
			Logger.getLogger(HtmlHeaderDirective.class.getName()).log(Level.SEVERE, null, ex);
		} catch (SecurityException ex) {
			Logger.getLogger(HtmlHeaderDirective.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalArgumentException ex) {
			Logger.getLogger(HtmlHeaderDirective.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(HtmlHeaderDirective.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ClassCastException ex) {
			return;
		}
		
		if (result == null) {
			return;
		}
		
		if (map.size() > 2) {
			throw new TemplateModelException("Only \"" + CSS_ATTR + "\" and \"" + SCRIPTS_ATTR + "\" properties are supported");
		}

		TemplateModel css = (TemplateModel) map.get(CSS_ATTR);
		if (css == null) {
			throw new TemplateModelException(CSS_ATTR + " property is required");
		}
		TemplateModel scripts = (TemplateModel) map.get(SCRIPTS_ATTR);
		if (css == null) {
			throw new TemplateModelException(SCRIPTS_ATTR + " property is required");
		}
		
		Results.HeadWrappersContainer container = Results.getHeadContainer(result);
		for (Results.HeadPartWrapper p : container.getParts()) {
			if ((p.getType() == Results.HeadPartType.SCRIPT || p.getType() == Results.HeadPartType.SCRIPT_SOURCE) && getBoolean(scripts, true)) {
				if (p.getType() == Results.HeadPartType.SCRIPT_SOURCE) {
					e.getOut().write("<script src=\"" + p.getContent().trim() + "?dc.fw=" + lastUpdate + "\" type=\"text/javascript\"></script>");
				}
				else if (p.getType() == Results.HeadPartType.SCRIPT) {
					e.getOut().write("<script type=\"text/javascript\">" + p.getContent().trim() + "</script>");
				}
			}
			if ((p.getType() == Results.HeadPartType.CSS || p.getType() == Results.HeadPartType.CSS_SOURCE) && getBoolean(css, true)) {
				if (p.getType() == Results.HeadPartType.CSS_SOURCE) {
					e.getOut().write("<link rel=\"stylesheet\" href=\"" + p.getContent().trim() + "?dc.fw=" + lastUpdate + "\" type=\"text/css\">");
				}
				else if (p.getType() == Results.HeadPartType.CSS) {
					e.getOut().write("<style type=\"text/css\">" + p.getContent().trim() + "</style>");
				}
			}
		}
		
	}
	
	private boolean getBoolean(TemplateModel model, boolean defaultValue) {
		if (model == null) {
			return defaultValue;
		}
		else {
			try {
				return ((TemplateBooleanModel) model).getAsBoolean();
			} catch (TemplateModelException ex) {
				LoggerUtils.getLogger(HtmlHeaderDirective.class).log(Level.SEVERE, "Error retrieving boolean value from TemplateModel", ex);
				return defaultValue;
			}
		}
	}
	
	@CoreEventListener(eventNames = {CoreEventName.MODULE_LOADED, CoreEventName.MODULE_UNLOADED})
	public static class UpdateLastUpdatedTime implements IEventListener {

		@Override
		public void performAction(Event event) {
			HtmlHeaderDirective.lastUpdate = System.currentTimeMillis();
		}
		
	}
}
