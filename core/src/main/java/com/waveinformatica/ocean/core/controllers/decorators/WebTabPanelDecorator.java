/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.decorators;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.annotations.ResultDecorator;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.results.TabPanelResult;
import com.waveinformatica.ocean.core.controllers.results.WebPageResult;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.Results;

/**
 *
 * @author Ivano
 */
@ResultDecorator(
	classes = TabPanelResult.class,
	mimeTypes = {
		MimeTypes.HTML
	})
public class WebTabPanelDecorator extends AbstractTemplateDecorator<TabPanelResult> {

	@Inject
	private ObjectFactory factory;

	@Override
	public void decorate(TabPanelResult tabRes, HttpServletRequest request, HttpServletResponse response, MimeTypes type)
	{
		
		WebPageDecorator wpd = factory.newInstance(WebPageDecorator.class);
		wpd.setExtraObjects(getExtraObjects());
		
		WebPageResult wrapperResult = wrapResult(request, tabRes);

		wpd.decorate(wrapperResult, request, response, type); // To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void decorate(TabPanelResult tabRes, OutputStream out, MimeTypes type)
	{
		
		WebPageDecorator wpd = factory.newInstance(WebPageDecorator.class);
		wpd.setExtraObjects(getExtraObjects());
		
		WebPageResult wrapperResult = wrapResult(Context.get().getRequest(), tabRes);

		wpd.decorate(wrapperResult, out, type);
	}

	protected WebPageResult wrapResult(HttpServletRequest request, TabPanelResult tabRes)
	{
		StringBuilder pathBase = new StringBuilder(request.getRequestURI());
		Enumeration<String> names = request.getParameterNames();
		if (names.hasMoreElements()) {
			pathBase.append("?");
		}
		while (names.hasMoreElements()) {
			String n = names.nextElement();
			if (! n.equals("fw:tab")) {
				for (String value : request.getParameterValues(n)) {
					if (pathBase.charAt(pathBase.length() - 1) != '?') {
						pathBase.append('&');
					}
					try {
						pathBase.append(URLEncoder.encode(n, "UTF-8"));
						pathBase.append('=');
						pathBase.append(URLEncoder.encode(value, "UTF-8"));
					}
					catch (UnsupportedEncodingException ex) {
						Logger.getLogger(WebTabPanelDecorator.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}
		}

		if (request.getParameter("fw:tab") != null)
			tabRes.setActive(request.getParameter("fw:tab"));

		tabRes.setPathBase(pathBase.toString());
		WebPageResult wrapperResult = factory.newInstance(WebPageResult.class);

		wrapperResult.setData(tabRes);
		wrapperResult.setTemplate(tabRes.getTemplate() != null ? tabRes.getTemplate() : "/templates/core/tabPanelResult.tpl");
		wrapperResult.setParentOperation(tabRes.getParentOperation());
		wrapperResult.getExtras().putAll(tabRes.getExtras());
		wrapperResult.setFullResult(tabRes.isFullResult());
		Results.copyHeadResources(tabRes, wrapperResult);

		wrapperResult.setContextPath(Application.getInstance().getBaseUrl());

		return wrapperResult;
	}
}
