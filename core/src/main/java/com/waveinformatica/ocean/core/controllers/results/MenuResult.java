/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.results;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.RootOperation;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.scopes.AbstractScope;
import com.waveinformatica.ocean.core.controllers.scopes.RootScope;
import com.waveinformatica.ocean.core.controllers.scopes.ScopeChain;
import com.waveinformatica.ocean.core.controllers.scopes.ScopesController;

/**
 *
 * @author Ivano
 */
public class MenuResult extends WebPageResult {

	private final List<MenuSection> sections = new LinkedList<MenuSection>();
	private String subTitle;

	@Inject
	private com.waveinformatica.ocean.core.security.SecurityManager securityManager;

	@Inject
	private ScopesController scopesController;

	@Inject
	private CoreController controller;

	private boolean analyzed = false;

	public MenuResult () {
		super();
		setTemplate("/templates/core/simpleMenu.tpl");
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public MenuSection addSection(String title) {
		for (MenuSection ms : sections) {
			if (ms.getTitle().equals(title)) {
				return ms;
			}
		}
		MenuSection s = new MenuSection(title);
		sections.add(s);
		return s;
	}

	public void initFromScope(AbstractScope scope) {

		HashMap<String, MenuResult.MenuSection> sectionMap = new HashMap<String, MenuResult.MenuSection>();

		List<CoreController.OperationInfo> ops = controller.getRootOperations(scope.getClass());
		for (CoreController.OperationInfo op : ops) {
			Method m = op.getMethod();
			RootOperation ro = m.getAnnotation(RootOperation.class);
			Operation o = m.getAnnotation(Operation.class);
			OperationProvider opProv = m.getDeclaringClass().getAnnotation(OperationProvider.class);
			StringBuilder url = new StringBuilder();
			if (opProv.namespace().startsWith("/")) {
				url.append(opProv.namespace().substring(1));
			} else {
				url.append(opProv.namespace());
			}
			if (o.value().startsWith("/") && url.toString().endsWith("/")) {
				url.append(o.value().substring(1));
			} else if (o.value().startsWith("/") && !url.toString().endsWith("/")) {
				url.append(o.value());
			} else {
				if (url.toString().length() > 0 && !url.toString().endsWith("/")) {
					url.append('/');
				}
				url.append(o.value());
			}

			MenuResult.MenuSection section = sectionMap.get(ro.section());
			if (section == null) {
				section = addSection(ro.section());
				sectionMap.put(ro.section(), section);
			}

			section.add(ro.iconUrl(), ro.title(), url.toString());
		}

	}

	public List<MenuSection> getSections() {
		if (!analyzed && !securityManager.isAuthorizeGuests()) {
			Iterator<MenuSection> iter = sections.iterator();
			while (iter.hasNext()) {
				MenuSection sect = iter.next();
				Iterator<MenuItem> iiter = sect.getItems().iterator();
				while (iiter.hasNext()) {
					MenuItem mi = iiter.next();
					String op = mi.getOperation();
					if (StringUtils.isNotBlank(op) && op.charAt(0) != '/') {
						op = "/" + op;
					}

					ScopeChain chain = scopesController.build(op);

					if (!chain.isEmpty() && !chain.getCurrent().isAuthorized()) {
						iiter.remove();
					}
				}
				if (sect.getItems().isEmpty()) {
					iter.remove();
				}
			}
			analyzed = true;
		}
		return sections;
	}

	public static class MenuSection {

		private final String title;
		private final List<MenuItem> items = new ArrayList<MenuItem>();

		public MenuSection(String title) {
			this.title = title;
		}

		public String getTitle() {
			return title;
		}

		public List<MenuItem> getItems() {
			return items;
		}

		public MenuItem add(String title, String operation) {
			MenuItem item = new MenuItem(title, operation);
			items.add(item);
			return item;
		}

		public MenuItem add(String icon, String title, String operation) {
			MenuItem item = new MenuItem(icon, title, operation);
			items.add(item);
			return item;
		}

	}

	public static class MenuItem {

		private final String icon;
		private final String title;
		private final String operation;

		public MenuItem(String title, String operation) {
			this.title = title;
			this.operation = operation;
			this.icon = "eye";
		}

		public MenuItem(String icon, String title, String operation) {
			this.icon = icon;
			this.title = title;
			this.operation = operation;
		}

		public String getIcon() {
			return icon;
		}

		public String getTitle() {
			return title;
		}

		public String getOperation() {
			return operation;
		}

	}

}
