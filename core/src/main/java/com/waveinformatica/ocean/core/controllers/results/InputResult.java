/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.controllers.results;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.CoreController.ParameterInfo;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.ObjectProperty;
import com.waveinformatica.ocean.core.controllers.events.BeforeOperationEvent;
import com.waveinformatica.ocean.core.controllers.events.BeforeResultEvent;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.results.input.AutoFieldHandler;
import com.waveinformatica.ocean.core.controllers.scopes.OperationScope;
import com.waveinformatica.ocean.core.controllers.scopes.ScopeChain;
import com.waveinformatica.ocean.core.controllers.scopes.ScopesController;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.I18N;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.ObjectUpdater;
import com.waveinformatica.ocean.core.util.OceanConversation;
import com.waveinformatica.ocean.core.util.OceanSession;
import com.waveinformatica.ocean.core.util.RequestCache;
import com.waveinformatica.ocean.core.util.Results;
import com.waveinformatica.ocean.core.util.UrlBuilder;

/**
 * This result type produces a Form which will call a defined operation.
 *
 * @author Ivano
 */
public class InputResult extends WebPageResult {
	
	@Inject
	private CoreController coreController;
	
	@Inject
	private I18N i18n;
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private OceanSession session;
	
	@Inject
	private RequestCache requestCache;
	
	@Inject
	private ScopesController scopesController;
	
	@Inject
	private OceanConversation conversation;
	
	@Inject
	private CoreController controller;
	
	private String formClass = "form-horizontal";
	private String action;
	private String refAction;
	private String validateOperation;
	
	private List<FormField> result = null;
	private MessageResult message;
	
	/**
	 * Never instantiate a result using its constructor. Use a injected
	 * ObjectFactory instead!
	 */
	public InputResult() {
		setTemplate("/templates/core/inputResult.tpl");
		Results.addScriptSource(this, "ris/js/ocean-validator.js");
	}
	
	public String getFullAction(String action) {
		if (!conversation.isActive()) {
			return action;
		}
		return action + (action.contains("?") ? "&" : "?") + "cid=" + conversation.getId();
	}
	
	/**
	 * Returns the {@code <form>} class attribute value
	 * 
	 * @return the {@code <form>} class attribute value
	 */
	public String getFormClass() {
		return formClass;
	}
	
	/**
	 * Sets the {@code <form>} class attribute value
	 */
	public void setFormClass(String formClass) {
		this.formClass = formClass;
	}

	/**
	 * Returns the action to submit the form
	 *
	 * @return form action
	 */
	public String getAction() {
		return action;
	}
	
	/**
	 * Sets the action to submit the form.
	 *
	 * <strong>NOTE:</strong> this method will clear fields info if the action
	 * is changed.
	 *
	 * @param action form submit action
	 */
	public void setAction(String action) {
		if (this.action != null && !this.action.equals(action)) {
			result = null;
		}
		this.action = action;
	}
	
	/**
	 * Defines the action for fields calculation (Optional). If
	 * <code>null</code>, the "<code>action</code>" configuration will be used.
	 *
	 * @return the action for calculating form fields or <code>null</code> (the
	 * default) to use the "<code>action</code>" configuration.
	 */
	public String getRefAction() {
		return refAction;
	}
	
	/**
	 * Defines the action for fields calculation (Optional). If
	 * <code>null</code>, the "<code>action</code>" configuration will be used.
	 *
	 * @param refAction the action for calculating form fields or
	 * <code>null</code> (the default) to use the "<code>action</code>"
	 * configuration.
	 */
	public void setRefAction(String refAction) {
		this.refAction = refAction;
	}
	
	/**
	 * Returns the optional operation used for remotely validating the form
	 * 
	 * @return the operation used for validation
	 */
	public String getValidateOperation() {
		return validateOperation;
	}
	
	/**
	 * Defines the <strong>optional</strong> operation used for remotely validating
	 * the form's data. The operation should have the same parameters as the action
	 * operation, because it will receive all the form's data.
	 * 
	 * <p>The vaidation operation should return a {@link ValidationResult} result.</p>
	 * 
	 * @param validateOperation the operation to be used for validation
	 */
	public void setValidateOperation(String validateOperation) {
		this.validateOperation = validateOperation;
	}
	
	/**
	 * Sets the initial value for a field in the form. It's a commodity method to
	 * pre-populate a form.
	 *
	 * @param name the name of the field
	 * @param value the initial value
	 */
	public void setValue(String name, String value) {
		for (FormField ff : getFieldsFromOperation()) {
			if (ff.getName().equals(name)) {
				ff.setValue(value);
				break;
			}
		}
	}
	
	/**
	 * Sets the initial values for a field in the form. It's a commodity method to
	 * pre-populate a form.
	 *
	 * @param name the name of the field
	 * @param values the initial values
	 */
	public void setValues(String name, String[] values) {
		FormField fld = getField(name);
		fld.getValues().clear();
		if (values != null) {
			for (String s : values) {
				fld.addValue(s);
			}
		}
	}
	
	/**
	 * Returns the list of fields in the form.
	 * 
	 * @return the list of form fields
	 */
	public List<FormField> getFields() {
		return getFieldsFromOperation();
	}
	
	/**
	 * Get a named field if available. It triggers operation analysis if not
	 * already analyzed.
	 *
	 * @param name the name of the requested field
	 * @return the requested field if available or <code>null</code> if not
	 * found
	 */
	public FormField getField(String name) {
		List<FormField> fields = getFieldsFromOperation();
		
		for (FormField ff : fields) {
			if (ff.getName().equals(name)) {
				return ff;
			}
		}
		
		return null;
	}
	
	private List<FormField> getFieldsFromOperation() {
		
		if (result != null) {
			return result;
		}
		
		result = new LinkedList<FormField>();
		
		String actionName = this.refAction;
		if (actionName == null) {
			actionName = this.action;
		}
		
		if (actionName.charAt(0) != '/') {
			actionName = "/" + actionName;
		}
		
		String cacheName = DataPersistenceManager.getName(null, actionName);
		Map<String, String[]> persistedData = (Map<String, String[]>) requestCache.get(cacheName, null);
		if (persistedData == null) {
			persistedData = (Map<String, String[]>) session.get(cacheName);
		}
		if (persistedData == null) {
			persistedData = (Map<String, String[]>) conversation.get(cacheName);
		}
        
        UrlBuilder urlBuilder = new UrlBuilder(actionName);
		
		ScopeChain scopeChain = scopesController.build(urlBuilder.getUrl(false));
		if (!(scopeChain.getCurrent() instanceof OperationScope)) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unable to find operation with path \"" + actionName + "\"");
			return result;
		}
		final CoreController.OperationInfo info = ((OperationScope) scopeChain.getCurrent()).getOperationInfo();
		
		result = new LinkedList<FormField>();
		
		for (CoreController.ParameterInfo p : info.getParameters()) {
			if (p.getName() == null) {
				continue;
			}
			FormField field = new FormField();
			if (p.hasAnnotation(Field.class)) {
				Field f = p.getAnnotation(Field.class);
				if (f.ignore()) {
					continue;
				}
				if (f.handler() == FieldHandler.class) {
					throw new IllegalArgumentException("The field handler for parameter \"" + p.getName() + "\" in operation \"" + info.getFullName() + "\" must be a class implementing " + FieldHandler.class.getName() + " interface. Not the interface itself.");
				} else if (Modifier.isAbstract(f.handler().getModifiers())) {
					throw new IllegalArgumentException("The field handler for parameter \"" + p.getName() + "\" in operation \"" + info.getFullName() + "\" must NOT be abstract ");
				}
                                field.setTooltip(f.tooltip());
				field.setHandler(factory.newInstance(f.handler()));
				field.setRequired(f.required());
				field.setReadOnly(f.readOnly());
				if (f.attribute().equals("")) {
					field.setAttribute(p.getName());
				} else {
					field.setAttribute(f.attribute());
				}
				field.setValue("");
				field.setDefaultValue(p.getDefaultValue());
			} else {
				field.setHandler(factory.newInstance(AutoFieldHandler.class));
				field.setAttribute(p.getName());
				field.setDefaultValue(p.getDefaultValue());
			}
			field.setParamInfo(p);
			field.setLabel(i18n.translate(info.getCls().getName() + "." + info.getMethod().getName() + "." + p.getName()));
			field.setName(p.getName());
			field.setType(p.getParamClass());
			
			result.add(field);
		}
		
		if (persistedData != null) {
			for (FormField f : result) {
				String[] data = persistedData.get(f.getName());
				if (data != null && (Boolean.class.isAssignableFrom(f.getType()) || data.length == 1)) {
					f.setValue(data[0]);
				}
				else if (data != null && data.length > 1) {
					for (String s : data) {
						f.addValue(s);
					}
				}
			}
		}
		
		for (FormField f : result) {
			if (f.getHandler() != null) {
				f.getHandler().init(this, info, f.getParamInfo(), f);
			}
		}
		
		return result;
		
	}
	
	/**
	 * This method loads fields to the InputResult from an object's attributes. If the object is a class, this method loads fields from that class,
	 * else the method loads fields from the object's class and, then, sets the InputResult's data to the object.
	 * 
	 * @param object the object to load fields from
	 * @param prefix the prefix to use for field names
	 * @return the list of fields in the InputResult
	 */
	public List<FormField> getFieldsFromObject(Object object, String prefix) {
		
		if (object == null) {
			return null;
		}
		
		if (result == null) {
			result = new LinkedList<FormField>();
		}
		
		if (object instanceof Class) {
			
			ObjectUpdater updater = factory.newInstance(ObjectUpdater.class);
			
			Map<String, ObjectProperty> props = updater.getObjectProperties((Class<?>) object);
			
			int i = 0;
			for (ObjectProperty prop : props.values()) {
				InputResult.Field ann = prop.getField().getAnnotation(InputResult.Field.class);
				InputResult.FormField ff = new InputResult.FormField();
				if (ann != null) {
					if (ann.ignore()) {
						continue;
					}
					if (ann.handler() == FieldHandler.class) {
						throw new IllegalArgumentException("The field handler for parameter \"" + prop.getField().getName() + "\" in class \"" + ((Class<?>) object).getName() + "\" must be a class implementing " + FieldHandler.class.getName() + " interface. Not the interface itself.");
					} else if (Modifier.isAbstract(ann.handler().getModifiers())) {
						throw new IllegalArgumentException("The field handler for parameter \"" + prop.getField().getName() + "\" in class \"" + ((Class<?>) object).getName() + "\" must NOT be abstract ");
					}
	                ff.setTooltip(ann.tooltip());
					ff.setHandler(factory.newInstance(ann.handler()));
					ff.setRequired(ann.required());
					ff.setReadOnly(ann.readOnly());
					if (ann.attribute().equals("")) {
						ff.setAttribute(prop.getField().getName());
					} else {
						ff.setAttribute(ann.attribute());
					}
					ff.setValue("");
				}
				else {
					ff.setHandler(factory.newInstance(AutoFieldHandler.class));
					ff.setAttribute(prop.getField().getName());
				}
				Class converter = controller.findConverter(prop.getField().getType().getName());
				ff.setParamInfo(new ParameterInfo(i++, prop.getField().getName(), prop.getField().getType(), prop.getField().getGenericType(), converter, prop.getField().getAnnotations(), new String[0]));
				ff.setLabel(i18n.translate(((Class<?>) object).getName() + "." + prop.getField().getName()));
				ff.setName(StringUtils.defaultIfEmpty(prefix, "") + prop.getField().getName());
				ff.setType(prop.getField().getType());
				result.add(ff);
			}
		}
		else {
			getFieldsFromObject(object.getClass(), prefix);
			setData(object);
		}
		
		return result;
		
	}
	
	/**
	 * This method is a shortand for {@link #getFieldsFromObject(Object, String) getFieldsFromObject(Object object, String prefix)} with {@code null} prefix.
	 * 
	 * @param object the object to load fields from
	 * @return the list of fields in this InputResult
	 */
	public List<FormField> getFieldsFromObject(Object object) {
		return getFieldsFromObject(object, null);
	}
	
	/**
	 * Fires the re-initialization of the field handlers. It is required after
	 * a set of manual operations on one or more handlers.
	 */
	public void reInitializeHandlers() {
		
		String actionName = this.refAction;
		if (actionName == null) {
			actionName = this.action;
		}
		
		ScopeChain scopeChain = scopesController.build(actionName);
		
		CoreController.OperationInfo info = null;
		if (scopeChain.getCurrent() instanceof OperationScope) {
			info = coreController.getOperationInfo(scopeChain.getCurrent().getParent().getClass(), actionName);
		}
		
		for (FormField f : result) {
			if (f.getHandler() != null) {
				f.getHandler().init(this, info, f.getParamInfo(), f);
			}
		}
		
	}
	
	/**
	 * Sets the result data to the specified object and load field values with
	 * data from object's attributes with matching name.
	 *
	 * @param data source for filling {@link InputResult}'s fields
	 */
	@Override
	public void setData(Object data) {
		
		super.setData(data);
		
		List<FormField> fields = getFieldsFromOperation();
		for (FormField ff : fields) {
			if (data instanceof Map) {
				Map map = (Map) data;
				Object val = map.get(ff.getAttribute());
				if (val == null) {
					continue;
				}
				if (val instanceof String[] && ((String[]) val).length > 0) {
					val = ((String[]) val)[0];
				}
				String value = ff.getHandler().formatValue(this, ff, val);
				if (value != null) {
					ff.setValue(value);
				}
			}
			else {
				String value = null;
				Class cls = data.getClass();
				while (cls != Object.class && value == null) {
					try {
						java.lang.reflect.Field field = cls.getDeclaredField(ff.getAttribute());
						field.setAccessible(true);
						Object v = field.get(data);
						field.setAccessible(false);
						if (v != null) {
							if (v instanceof Collection) {
								value = null;
								for (Object o : (Collection) v) {
									if (o != null) {
										String formattedValue = ff.getHandler().formatValue(this, ff, o);
										if (formattedValue != null) {
											ff.addValue(formattedValue);
										}
									}
								}
							} else {
								value = ff.getHandler().formatValue(this, ff, v);
							}
						}
						break;
					} catch (NoSuchFieldException ex) {
						cls = cls.getSuperclass();
					} catch (SecurityException ex) {
						Logger.getLogger(InputResult.class.getName()).log(Level.SEVERE, null, ex);
					} catch (IllegalArgumentException ex) {
						Logger.getLogger(InputResult.class.getName()).log(Level.SEVERE, null, ex);
					} catch (IllegalAccessException ex) {
						Logger.getLogger(InputResult.class.getName()).log(Level.SEVERE, null, ex);
					}
				}

				if (value != null) {
					ff.setValue(value);
				}
			}
		}
		
	}
	
    //ELM 10/07/2018 Added function
    public void setFormFieldsList(List<FormField> result) {
        this.result = result;
    }
	
	public static class FormField {
		
		private CoreController.ParameterInfo paramInfo;
		private String name;
		private String label;
                private String tooltip;
		private Class type;
		private String value;
		private boolean editable = true;
		private FieldHandler handler;
		private String attribute;
		private boolean required;
		private boolean readOnly;
		private final List<String> values = new ArrayList<String>();
		private boolean defaultValue = false;
		private final Map<String, Object> attributes = new HashMap<String, Object>();
		
		public CoreController.ParameterInfo getParamInfo() {
			return paramInfo;
		}
		
		public FormField setParamInfo(CoreController.ParameterInfo paramInfo) {
			this.paramInfo = paramInfo;
			return this;
		}
		
		public String getName() {
			return name;
		}
		
		public FormField setName(String name) {
			this.name = name;
			return this;
		}
		
		public String getLabel() {
			return label;
		}
		
		public FormField setLabel(String label) {
			this.label = label;
			return this;
		}

        public String getTooltip() {
            return tooltip;
        }

        public void setTooltip(String tooltip) {
            this.tooltip = tooltip;
        }

		public Class getType() {
			return type;
		}
		
		public FormField setType(Class type) {
			this.type = type;
			return this;
		}
		
		public String getValue() {
			return value;
		}
		
		public List<String> getValues() {
			return values;
		}
		
		public FormField setValue(String value) {
			if (defaultValue) {
				defaultValue = false;
			}
			this.value = value;
			values.clear();
			values.add(value);
			
			return this;
		}
		
		public FormField addValue(String value) {
			if (defaultValue) {
				defaultValue = false;
				values.clear();
				this.value = null;
			}
			values.add(value);
			StringBuilder builder = new StringBuilder();
			for (String v : values) {
				builder.append(',');
				builder.append(v);
			}
			this.value = values.isEmpty() ? "" : builder.toString().substring(1);
			
			return this;
		}
		
		public FormField setDefaultValue(String[] values) {
			
			if (values == null || values.length == 0)
				return this;
			
			this.values.clear();
			for (String v : values) {
				addValue(v);
			}
			defaultValue = true;
			
			return this;
		}
		
		public boolean isEditable() {
			return editable;
		}
		
		public FormField setEditable(boolean editable) {
			this.editable = editable;
			return this;
		}
		
		public FieldHandler getHandler() {
			return handler;
		}
		
		public FormField setHandler(FieldHandler handler) {
			this.handler = handler;
			return this;
		}
		
		public String getAttribute() {
			return attribute;
		}
		
		public FormField setAttribute(String attribute) {
			this.attribute = attribute;
			return this;
		}
		
		public boolean isRequired() {
			return required;
		}
		
		public FormField setRequired(boolean required) {
			this.required = required;
			return this;
		}

		public boolean isReadOnly() {
			return readOnly;
		}

		public FormField setReadOnly(boolean readOnly) {
			this.readOnly = readOnly;
			return this;
		}

		public Map<String, Object> getAttributes() {
			return attributes;
		}
		
		public FormField setAttribute(String name, Object value) {
			attributes.put(name, value);
			return this;
		}
		
		public Object getAttribute(String name) {
			return attributes.get(name);
		}
		
	}
	
	/**
	 * Returns the {@link MessageResult} object set through the {@link #setMessage(com.waveinformatica.ocean.core.controllers.results.MessageResult) setMessage}
	 * method.
	 * 
	 * @return the {@link MessageResult} object if any or <code>null</code>
	 */
	public MessageResult getMessage() {
		return message;
	}
	
	/**
	 * Allows to set a message into the header of the produced form.
	 *
	 * @param message the message to show into the header
	 */
	public void setMessage(MessageResult message) {
		this.message = message;
	}
	
	/**
	 * This interface defines a field handler. It provides everything needed to
	 * render a form field: a HTML template and a way to initialize the field
	 * with required data.
	 */
	public static interface FieldHandler {
		
		/**
		 * This method must return the path of the template which will be used
		 * to render the form field.
		 *
		 * @return the template path to render the field
		 */
		public String getFieldTemplate();
		
		/**
		 * This method will be called to initialize data required by the field
		 * to be rendered (i.e. a combo will need a list of values).
		 *
		 * @param result the current {@link InputResult} instance
		 * @param opInfo information about the action's operation
		 * @param paramInfo information about the destination parameter of the
		 * field
		 * @param field the {@link FormField} instance
		 */
		public void init(InputResult result, CoreController.OperationInfo opInfo, CoreController.ParameterInfo paramInfo, FormField field);
		
		/**
		 * This method will provide a string formatted value from the
		 * <code>value</code> parameter. The provided value will be used as
		 * value for the field.
		 *
		 * @param result the current {@link InputResult} instance
		 * @param field the {@link FormField} instance
		 * @param value the object representing the source value to be formatted
		 * as string
		 * @return the string formatted value
		 */
		public String formatValue(InputResult result, FormField field, Object value);
		
	}
	
	/**
	 * Configures operation parameters as fields for InputResult
	 */
	@Target({ElementType.PARAMETER, ElementType.FIELD})
	@Retention(RetentionPolicy.RUNTIME)
	public static @interface Field {
		
		/**
		 * @return if <code>true</code> the current parameter will be ignored
		 * when the form will be rendered
		 */
		boolean ignore() default false;
		
		/**
		 *
		 * @return a specific or custom handler to render the field
		 */
		Class<? extends FieldHandler> handler() default AutoFieldHandler.class;
		
		/**
		 *
		 * @return a name of a different attribute to fill data from
		 */
		String attribute() default "";
		
		/**
		 *
		 * @return if <code>true</code> the rendered field should or will be
		 * marked as <strong>required</strong>
		 */
		boolean required() default false;
		
		/**
		 *
		 * @return if <code>true</code> the rendered field should or will be
		 * marked as <strong>read-only</strong>
		 */
		boolean readOnly() default false;
                
                /**
                 * Defines a tooltip for the field. It will be used as a i18n key
                 * and can contain HTML code, depending on the frontend implementation.
                 * 
                 * @return a i18n key or HTML code for the field's tooltip
                 */
                String tooltip() default "";
	}
	
	/**
	 * This event listener is for **INTERNAL USE ONLY** to ensure correct fields initialization
	 */
	@CoreEventListener(eventNames = CoreEventName.BEFORE_RESULT, flags = "com.waveinformatica.ocean.core.controllers.results.InputResult")
	public static class InputResultInitializationEnsurer implements IEventListener<BeforeResultEvent> {
		
		@Override
		public void performAction(BeforeResultEvent evt) {
			InputResult result = (InputResult) evt.getResult();
			result.getFields();
		}
		
	}
	
	/**
	 * This enum defines field value supported persistence types.
	 */
	public static enum PersistenceType {
		/**
		 * No persistence for the form (the default)
		 */
		NONE,
		/**
		 * Automatic persistence for request only
		 */
		REQUEST,
		/**
		 * Automatic persistence in session
		 */
		SESSION,
		
		/**
		 * Automatic persistence in conversation
		 */
		CONVERSATION
	}
	
	/**
	 * <p>Use this annotation to annotate methods referenced by {@link InputResult} results, to
	 * configure it's data automatic persistence mode.</p>
	 *
	 * <p><strong>WARNING:</strong> if the referenced operation is included in other operations,
	 * it is highly suggested to annotate the parent operation instead, to ensure right execution
	 * order.</p>
	 *
	 * <p>Field persistence will be automatically managed according to this configuration
	 * only on POST requests, after the form's submission</p>
	 */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public static @interface Persistence {
		
		/**
		 *
		 * @return the automatic fields persistence type
		 */
		PersistenceType value() default PersistenceType.NONE;
		
		/**
		 * Set this field if the annotated operation is an operation which
		 * includes the <code>refAction</code> operation of the {@link InputResult}.
		 * In this case set the same value as for <code>refAction</code> attribute.
		 * 
		 * @return 
		 */
		String refOperation() default "";
	}
	
	/**
	 * This event listener is for **INTERNAL USE ONLY** to manage fields persistence
	 */
	@CoreEventListener(eventNames = CoreEventName.BEFORE_OPERATION)
	public static class DataPersistenceManager implements IEventListener<BeforeOperationEvent> {
		
		@Inject
		private OceanSession session;
		
		@Inject
		private RequestCache requestCache;
		
		@Inject
		private OceanConversation conversation;
		
		@Override
		public void performAction(BeforeOperationEvent evt) {
			
			HttpServletRequest request = Context.get().getRequest();
			
			if (request == null) {
				return;
			}
			
			Persistence persistence = evt.getOperation().getMethod().getAnnotation(Persistence.class);
			if (persistence == null) {
				return;
			}
			
			if (request.getMethod().equals("POST")) {
				switch (persistence.value()) {
					case NONE:
						break;
					case REQUEST:
						requestCache.put(getName(persistence, evt.getOperation().getFullName()), evt.getArgs());
						break;
					case CONVERSATION:
						conversation.put(getName(persistence, evt.getOperation().getFullName()), new HashMap<String, String[]>(evt.getArgs()));
						break;
					case SESSION:
						session.put(getName(persistence, evt.getOperation().getFullName()), new HashMap<String, String[]>(evt.getArgs()));
						break;
				}
			}
			else {
				Map<String, String[]> params = (Map<String, String[]>) requestCache.get(getName(persistence, evt.getOperation().getFullName()), null);
				if (params == null) {
					params = (Map<String, String[]>) conversation.get(getName(persistence, evt.getOperation().getFullName()));
				}
				if (params == null) {
					params = (Map<String, String[]>) session.get(getName(persistence, evt.getOperation().getFullName()));
				}
				if (params != null) {
					Map<String, String[]> mergedParams = new HashMap<>(params);
					mergedParams.putAll(evt.getArgs());
					evt.getArgs().clear();
					evt.getArgs().putAll(mergedParams);
				}
			}
			
		}
		
		public static String getName(Persistence persistence, String operation) {
			
			if (persistence != null && ! persistence.refOperation().equals(""))
				operation = persistence.refOperation();
			
			if (! operation.startsWith("/"))
				operation = "/" + operation;
			
			return "InputResult.Persistence{" + operation + "}";
		}
		
	}
	
}
