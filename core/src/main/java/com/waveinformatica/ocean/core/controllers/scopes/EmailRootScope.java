package com.waveinformatica.ocean.core.controllers.scopes;

public class EmailRootScope extends RootScope {
	
	private String subject;
	
	public EmailRootScope() {
		super();
		
		setHeaderTemplate("/templates/mail/header.tpl");
		setFooterTemplate("/templates/mail/footer.tpl");
	}

	@Override
	public AbstractScope getParent() {
		return null;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

}
