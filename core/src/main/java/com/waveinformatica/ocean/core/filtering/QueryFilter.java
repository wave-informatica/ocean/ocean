/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.filtering;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.filtering.drivers.DataValue;
import com.waveinformatica.ocean.core.filtering.drivers.ValuesManager;

/**
 *
 * @author Ivano
 */
public class QueryFilter<Q> {
	
	@Deprecated
	private String condition;
	private Q whereClause;
	@Deprecated
	private final Map<String, Object> parameters = new HashMap<String, Object>();
	private final ValuesManager values;

	@Deprecated
	public QueryFilter() {
		values = new ValuesManager();
	}
	
	public QueryFilter(ValuesManager values) {
		this.values = values;
	}
	
	@Deprecated
	public String getCondition(String prefix) {
		return whereClause != null && whereClause instanceof String && StringUtils.isNotBlank(whereClause.toString()) ? prefix + whereClause.toString() : "";
	}

	@Deprecated
	public String getCondition() {
		return whereClause != null ? whereClause.toString() : "";
	}

	@Deprecated
	public void setCondition(String condition) {
		this.condition = condition;
	}

	public Map<String, Object> getParameters() {
		Map<String, Object> params = new HashMap<>();
		Iterator<DataValue> iter = values.iterator();
		while (iter.hasNext()) {
			DataValue dv = iter.next();
			params.put(dv.getName(), dv.getValue());
		}
		return params;
	}
	
	@Deprecated
	public void set(String name, Object value) {
		values.getParam(new DataValue(value));
	}
	
	@Deprecated
	public <T extends Query> T fillQueryParams(T query) {
		for (String k : parameters.keySet()) {
			query.setParameter(k, parameters.get(k));
		}
		return query;
	}
	
	public Q getWhereClause() {
		return whereClause;
	}
	
	public void setWhereClause(Q whereClause) {
		this.whereClause = whereClause;
	}
	
}
