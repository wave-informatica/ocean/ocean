/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.controllers.results;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.annotations.ExcludeSerialization;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.Event;
import com.waveinformatica.ocean.core.security.UserSession;

/**
 *
 * @author Ivano
 */
public class WebPageResult extends BaseResult {
	
	private static Long lastModify = System.currentTimeMillis();
	private int statusCode = 200;
	private String contextPath;
	private String template;
	private Object data;
	private Object provider;
	
	@Inject
	@ExcludeSerialization
	@Deprecated
	private UserSession userSession;
	
	@Inject
	@ExcludeSerialization
	@Deprecated
	private Configuration configuration;
	
	public String getTemplate() {
		return template;
	}
	
	public void setTemplate(String template) {
		this.template = template;
	}
	
	public Object getData() {
		return data;
	}
	
	public void setData(Object data) {
		this.data = data;
	}
	
	public Object getProvider() {
		return provider;
	}
	
	public void setProvider(Object provider) {
		this.provider = provider;
	}
	
	public UserSession getUserSession() {
		return userSession;
	}
	
	public String getContextPath() {
		return contextPath;
	}
	
	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}
	
	public int getStatusCode() {
		return statusCode;
	}
	
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	
	public Configuration getConfiguration() {
		return configuration;
	}
	
	public long getLastModify() {
		return lastModify;
	}
	
	public static void updateLastModify() {
		lastModify = System.currentTimeMillis();
	}
	
	@CoreEventListener(eventNames = CoreEventName.MODULE_LOADED)
	public static class LastModifyUpdater implements IEventListener {
		
		@Override
		public void performAction(Event event) {
			updateLastModify();
		}
		
	}
}
