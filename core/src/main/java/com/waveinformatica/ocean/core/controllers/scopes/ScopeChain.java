/*
 * Copyright 2017, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.scopes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * This class is one of the building blocks of the Ocean's scopes support. The developer
 * could see it like the FilterChain, largely used in J2EE applications. Each
 * {@link IScopeProvider} object, can add a {@link AbstractScope} to it.
 * 
 * @author ivano
 * @since 2.2.0
 */
public class ScopeChain implements Iterable<AbstractScope> {
	
	private final List<AbstractScope> scopes = new ArrayList<AbstractScope>();
	private final List<AbstractScope> stage = new ArrayList<AbstractScope>();
	
	/**
	 * This method returns the last current scope in the chain
	 * 
	 * @return the current scope in the chain
	 */
	public AbstractScope getCurrent() {
		return scopes.isEmpty() ? null : scopes.get(scopes.size() - 1);
	}
	
	/**
	 * This method returns the parent scope of the given one. It is a shortcut for
	 * {@link ScopeChain#getParent(java.lang.Class, com.waveinformatica.ocean.core.controllers.scopes.AbstractScope) getParent(Class parentClass, AbstractScope scope)}
	 * method, with the first parameter <code>null</code>.
	 * 
	 * @param scope the child scope
	 * @return the direct parent scope if any, or <code>null</code> if none found
	 */
	public AbstractScope getParent(AbstractScope scope) {
		return getParent(null, scope);
	}
	
	/**
	 * This method finds the ancestor of the given scope, given the <code>parentClass</code> class.
	 * This method follows the chain in reverse order to find the first ancestor of the requested
	 * type. If <code>parentClass</code> is null, the first parent is returned.
	 * 
	 * @param <T> the parent class type
	 * @param parentClass the parent class
	 * @param scope the starting context
	 * @return the parent context of the given type if found, or <code>null</code> if none is found
	 */
	public <T extends AbstractScope> T getParent(Class<T> parentClass, AbstractScope scope) {
		
		int index = -1;
		
		for (int i = scopes.size() - 1; i >= 0; i--) {
			if (scopes.get(i) == scope) {
				index = i;
			}
		}
		for (int i = index - 1; i >= 0; i--) {
			if (parentClass == null || parentClass.isAssignableFrom(scopes.get(i).getClass())) {
				return (T) scopes.get(i);
			}
		}
		
		return null;
		
	}
	
	/**
	 * Adds the given scope to the <strong>stage</strong> chain. The scopes are
	 * committed to the scope chain if and only if the scope provider does not
	 * return <code>null</code>.
	 * 
	 * @param scope the scope
	 */
	public void add(AbstractScope scope) {
		if (scope != null) {
			stage.add(scope);
		}
		else {
			throw new NullPointerException("The scope must not be null");
		}
	}

	@Override
	public Iterator<AbstractScope> iterator() {
		return Collections.unmodifiableList(scopes).iterator();
	}
	
	/**
	 * This method returns the chain's scopes as a list ordered
	 * from the root scope to the "current" one
	 * 
	 * @return the chain's scope as a list
	 */
	public List<AbstractScope> getScopes() {
		return Collections.unmodifiableList(scopes);
	}
	
	/**
	 * Checks if the chain is empty.
	 * 
	 * @return <code>true</code> if the chain is empty
	 */
	public boolean isEmpty() {
		return scopes.isEmpty();
	}
	
	void commit() {
		scopes.addAll(stage);
		stage.clear();
	}
	
	void rollback() {
		stage.clear();
	}
	
	void completeChain() {
		
		for (AbstractScope scope : scopes) {
			scope.complete(this);
		}
		
	}
	
}
