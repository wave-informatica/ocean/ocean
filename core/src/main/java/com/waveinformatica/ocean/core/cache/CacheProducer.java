/*
 * Copyright 2017, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.cache;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;
import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.ManagedCache;
import com.waveinformatica.ocean.core.cache.CacheManager.RegisteredCache;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.PersistedProperties;

/**
 *
 * @author ivano
 */
public class CacheProducer {
	
	@Inject
	private Configuration config;
	
	@Inject
	private CacheManager manager;
	
	@Produces
	@ManagedCache("")
	public OceanCache produce(InjectionPoint ip) {
		
		ManagedCache mc = ip.getAnnotated().getAnnotation(ManagedCache.class);
		
		if (mc.register()) {
			for (RegisteredCache rc : manager.getRegisteredCaches()) {
				if (rc.getName().equals(mc.value())) {
					return rc.getCache();
				}
			}
		}
		
		PersistedProperties props = config.getCachingProperties();

		String v = props.getProperty(mc.value());
		
		if (StringUtils.isNotBlank(v)) {
			Gson gson = JsonUtils.getBuilder().create();
			CacheElement ce = gson.fromJson(v, CacheElement.class);
			ICacheProvider provider = manager.getProvider(ce.getProviderName());
			Type gt = null;
			Type[] types = provider.getClass().getGenericInterfaces();
			for (Type t : types) {
				if (t instanceof ParameterizedType) {
					ParameterizedType pt = (ParameterizedType) t;
					if (pt.getRawType() == ICacheProvider.class) {
						gt = pt.getActualTypeArguments()[0];
						break;
					}
				}
			}
			if (gt != null) {
				Object conf = null;
				if (gt != Object.class) {
					conf = gson.fromJson(ce.getConfigJson(), gt);
				}
				OceanCache cache = new OceanCache(mc.value());
				cache.setMap(provider.getCache(conf));
				cache.setProviderName(mc.value());
				if (mc.register()) {
					manager.register(cache);
				}
				return cache;
			}
		}
		
		Map map = manager.getProvider("Default").getCache(null);
		
		OceanCache cache = new OceanCache(mc.value());
		cache.setMap(map);
		cache.setProviderName("Default");
		if (mc.register()) {
			manager.register(cache);
		}
		return cache;
		
	}
	
}
