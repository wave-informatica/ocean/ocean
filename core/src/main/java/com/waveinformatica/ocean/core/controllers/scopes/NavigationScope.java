/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.controllers.scopes;

import java.util.List;

import com.waveinformatica.ocean.core.controllers.dto.NavigationLink;

/**
 * This interface is intended to be implemented by scopes to provide
 * breadcrumb links
 * 
 * @author ivano
 *
 */
public interface NavigationScope {
	
	/**
	 * This method should return a list of breadcrumb links
	 * 
	 * @return breadcrumb links or empty list
	 */
	public List<NavigationLink> getNavigationLinks();
	
}
