/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers;

import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.controllers.dto.OperationContext;
import com.waveinformatica.ocean.core.controllers.events.AfterResultEvent;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.SecurityConfigurationEvent;
import com.waveinformatica.ocean.core.controllers.events.WebRequestEvent;
import com.waveinformatica.ocean.core.controllers.results.HttpErrorResult;
import com.waveinformatica.ocean.core.exceptions.OperationNotAuthorizedException;
import com.waveinformatica.ocean.core.exceptions.OperationNotFoundException;
import com.waveinformatica.ocean.core.util.DecoratorHelper;
import com.waveinformatica.ocean.core.util.LazyRequestParameterMap;
import com.waveinformatica.ocean.core.util.LoggerUtils;

/**
 *
 * @author Ivano
 */
@CoreEventListener(eventNames = CoreEventName.WEB_REQUEST, flags = {"GET", "POST", "HEAD"})
public class SimpleWebRequestController implements IEventListener<WebRequestEvent> {

	private static final Logger logger = LoggerUtils.getCoreLogger();

	@Inject
	private CoreController coreController;

	@Inject
	private com.waveinformatica.ocean.core.security.SecurityManager securityManager;

	@Inject
	private ObjectFactory factory;

	@Override
	public void performAction(WebRequestEvent e) {

		HttpServletRequest req = e.getRequest();
		String opName = req.getPathInfo();

		if (StringUtils.isBlank(opName)) {
			opName = "/";
		}
		else if (e.getRequestManagedBy() != null) {
			return;
		}

		SecurityConfigurationEvent secConfigEvent = new SecurityConfigurationEvent(this, securityManager);
		coreController.dispatchEvent(secConfigEvent);
		securityManager.setAuthorizeGuests(!secConfigEvent.isAuthenticationNeeded());

		OperationContext opCtx = new OperationContext();
		Object result = null;
		Throwable err = null;
		try {
			result = coreController.executeOperation(opName, new LazyRequestParameterMap(req),
					opCtx, req, e.getResponse());
		} catch (Throwable t) {
			while (t instanceof InvocationTargetException) {
				t = t.getCause();
			}
			err = t;
		}
		
		if (err != null) {
			
			if (err instanceof OperationNotFoundException) {
				logger.log(Level.SEVERE, "Operation not found: " + opName);
				result = factory.newInstance(HttpErrorResult.class);
				((HttpErrorResult) result).setErrorCode(404);
			}
			else if (err instanceof OperationNotAuthorizedException) {
				try {
					logger.log(Level.SEVERE, "Operation not authorized: " + opName);
					if (securityManager.getUserSession().isLogged()) {
						result = factory.newInstance(HttpErrorResult.class);
						((HttpErrorResult) result).setErrorCode(403);
					} else {
						if (secConfigEvent.getHandler() != null) {
							result = secConfigEvent.getHandler().getAuthenticationResult(e, secConfigEvent, opName);
						}
					}
				} catch (Throwable t) {
					result = factory.newInstance(HttpErrorResult.class);
					((HttpErrorResult) result).setData(t);
					((HttpErrorResult) result).setErrorCode(500);
				}
			}
			else {
				LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unexpected error executing operation " + opName, err);
				result = factory.newInstance(HttpErrorResult.class);
				((HttpErrorResult) result).setData(err);
				((HttpErrorResult) result).setErrorCode(500);
			}
			
		}

		e.setRequestManagedBy(this);

		try {

			if (result != null) {
				
				DecoratorHelper helper = factory.newInstance(DecoratorHelper.class);
				
				helper.decorateResult(result, e, opCtx);

			}

		} finally {
			AfterResultEvent are = new AfterResultEvent(result, req, e.getResponse(), result);
			coreController.dispatchEvent(are);
		}

	}

}
