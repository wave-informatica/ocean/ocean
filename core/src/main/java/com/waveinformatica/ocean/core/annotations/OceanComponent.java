/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.annotations;

import com.waveinformatica.ocean.core.modules.ModulesManager;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>This is a meta-annotation. It marks annotations to be used to define components
 * for the Ocean Framework. Each component must be annotated with a defined annotation
 * to be loaded by the core or by other components and those annotations must be
 * marked with the OceanComponent annotation.</p>
 *
 * <p>If a component annotation is not marked with the <code>OceanComponent</code>
 * annotation, it will be discarded by the {@link ModulesManager} and the marked won't be
 * loaded.</p>
 *
 * @author Ivano
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.ANNOTATION_TYPE)
public @interface OceanComponent {
	
	public Class<?> mustExtend() default Object.class;
	public Class<?> requiresBean() default Object.class;
	
}
