/*
* Copyright 2017, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */
package com.waveinformatica.ocean.core.html;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.controllers.dto.ResourceInfo;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.ResourceRetriever;

import se.fishtank.css.selectors.Selectors;
import se.fishtank.css.selectors.dom.W3CNode;

/**
 * @author Ivano
 */
public class PDFUtils {

	public static PrepareResult prepareDocument(Document doc) {

		PrepareResult result = new PrepareResult();

		CSSContext context = new CSSContext();
		context.setMedia("print");

		CSSStyleSheet styleSheet = CSSUtils.extractCss(doc, context, "", true);

		prepareCSS(styleSheet, context, result);

		try {
			importCssFonts(styleSheet);
		} catch (IOException ex) {
			LoggerUtils.getLogger(PDFUtils.class).log(Level.SEVERE, null, ex);
		}

		// Appending CSS to the document's head
		Selectors selectors = new Selectors(new W3CNode(doc));
		Node head = (Node) selectors.querySelector("head");
		Element style = doc.createElement("style");
		Text sContent = doc.createTextNode(styleSheet.toString());
		style.appendChild(sContent);
		head.appendChild(style);

		return result;

	}

	public static void prepareCSS(CSSStyleSheet styleSheet, CSSContext context, PrepareResult prepareResult) {

		if (context == null) {
			context = new CSSContext();
			context.setMedia("print");
		}
		if (prepareResult == null) {
			prepareResult = new PrepareResult();
		}

		// Cleaning rules by media queries
		Iterator<CSSRule> rIter = styleSheet.getRules().iterator();
		while (rIter.hasNext()) {
			if (!context.ruleApplies(rIter.next())) {
				rIter.remove();
			}
		}

		// Fonts management
		for (CSSRule r : styleSheet.getRules()) {
			if ("@page".equalsIgnoreCase(r.getSelector().get(0))) {
				for (CSSProperty p : r.getProperties()) {
					if ("size".equalsIgnoreCase(p.getName())) {
						CSSValue v = p.getParsedValues().get(0);
						if (v instanceof CSSValueName) {
							String n = ((CSSValueName) v).getName();
							try {
								SheetSize size = SheetSize.valueOf(n.toUpperCase());
								if (size != null) {
									prepareResult.setWidth(size.getWidth());
									prepareResult.setHeight(size.getHeight());
								}
							} catch (IllegalArgumentException e) {
								LoggerUtils.getLogger(PDFUtils.class).log(Level.WARNING, "Unknown @page size " + n);
							}
						} else if (v instanceof CSSValueNumber) {
							CSSValueNumber n = (CSSValueNumber) v;
							prepareResult.setWidth(new Measure(n.getValue(), n.getUnit()));
						} else if (v instanceof CSSValueComposite) {
							Measure width = null, height = null;
							CSSValueComposite c = (CSSValueComposite) v;
							if (c.getParts().size() > 1) {
								if (c.getParts().get(0) instanceof CSSValueNumber
									  && c.getParts().get(1) instanceof CSSValueNumber) {
									CSSValueNumber w = (CSSValueNumber) c.getParts().get(0);
									CSSValueNumber h = (CSSValueNumber) c.getParts().get(1);
									prepareResult.setWidth(new Measure(w.getValue(), w.getUnit()));
									prepareResult.setHeight(new Measure(h.getValue(), h.getUnit()));
								} else if (c.getParts().get(0) instanceof CSSValueName) {
									SheetSize size = SheetSize.valueOf(((CSSValueName) c.getParts().get(0)).getName());
									if (size != null) {
										width = size.getWidth();
										height = size.getHeight();
										if (c.getParts().get(1) instanceof CSSValueName) {
											if ("landscape".equalsIgnoreCase(((CSSValueName) c.getParts().get(1)).getName())) {
												Measure tmp = width;
												width = height;
												height = tmp;
											}
										}
										prepareResult.setWidth(width);
										prepareResult.setHeight(height);
									}
								}
							} else if (c.getParts().get(0) instanceof CSSValueName) {
								SheetSize size = SheetSize.valueOf(((CSSValueName) c.getParts().get(0)).getName());
								prepareResult.setWidth(size.getWidth());
								prepareResult.setHeight(size.getHeight());
							} else if (c.getParts().get(0) instanceof CSSValueNumber) {
								CSSValueNumber w = (CSSValueNumber) c.getParts().get(0);
								prepareResult.setWidth(new Measure(w.getValue(), w.getUnit()));
							}
						}
					}
				}
			}
			if ("@font-face".equalsIgnoreCase(r.getSelector().get(0))) {

				boolean embedPropFound = false;
				boolean encPropFound = false;
				CSSValueUrl ttfSrc = null;

				for (CSSProperty p : r.getProperties()) {
					if (ttfSrc == null && "src".equalsIgnoreCase(p.getName())) {
						for (CSSValue v : p.getParsedValues()) {
							if (v instanceof CSSValueUrl) {
								CSSValueUrl u = (CSSValueUrl) v;
								if (u.getValue().endsWith(".ttf")) {
									ttfSrc = u;
								}
							} else if (v instanceof CSSValueComposite) {
								for (CSSValue vc : ((CSSValueComposite) v).getParts()) {
									if (vc instanceof CSSValueUrl) {
										CSSValueUrl u = (CSSValueUrl) vc;
										if (u.getValue().endsWith(".ttf")) {
											ttfSrc = u;
										}
									}
								}
							}
						}
					}
					if (p.getName().equals("-fs-pdf-font-embed")) {
						p.getValues().clear();
						p.getValues().add("embed");
						embedPropFound = true;
					}
					if (p.getName().equals("-fs-pdf-font-encoding")) {
						p.getValues().clear();
						p.getValues().add("Identity-H");
						encPropFound = true;
					}
				}

				if (ttfSrc != null) {
					Iterator<CSSProperty> pIter = r.getProperties().iterator();
					while (pIter.hasNext()) {
						CSSProperty p = pIter.next();
						if ("src".equalsIgnoreCase(p.getName())) {
							pIter.remove();
						}
					}
					r.addProperty("src", "url('" + ttfSrc.getValue() + "')");
				}

				if (!embedPropFound) {
					r.addProperty("-fs-pdf-font-embed", "embed");
				}
				if (!encPropFound) {
					r.addProperty("-fs-pdf-font-encoding", "Identity-H");
				}

			}
		}

	}

	public static void importCssFonts(CSSStyleSheet styleSheet) throws IOException {

		GraphicsEnvironment g = GraphicsEnvironment.getLocalGraphicsEnvironment();
		String[] availableFonts = g.getAvailableFontFamilyNames();
		Set<String> fontNames = new HashSet<String>();
		fontNames.addAll(Arrays.asList(availableFonts));

		ResourceRetriever retriever = Application.getInstance().getObjectFactory().newInstance(ResourceRetriever.class);

		Map<String, String> fontFamilyRemap = new HashMap<String, String>();

		for (CSSRule r : styleSheet.getRules()) {
			if ("@font-face".equalsIgnoreCase(r.getSelector().get(0))) {
				String fontFamily = r.getPropertyValue("font-family");
				if (!fontNames.contains(fontFamily)) {
					CSSProperty p = r.getProperty("src");
					if (p != null) {
						List<CSSValue> values = p.getParsedValues();
						for (CSSValue v : values) {
							if (v instanceof CSSValueUrl) {
								try {
									Font registered = loadFont(retriever, fontFamily, ((CSSValueUrl) v).getValue());
									if (registered != null) {
										fontNames.add(registered.getFamily());
										fontFamilyRemap.put(fontFamily, registered.getFamily());
									}
								} catch (FontFormatException ex) {
									LoggerUtils.getLogger(PDFUtils.class).log(Level.SEVERE, null, ex);
								}
								break;
							} else if (v instanceof CSSValueComposite) {
								CSSValueComposite vc = (CSSValueComposite) v;
								for (CSSValue vp : vc.getParts()) {
									if (vp instanceof CSSValueUrl) {
										try {
											Font registered = loadFont(retriever, fontFamily, ((CSSValueUrl) vp).getValue());
											if (registered != null) {
												fontNames.add(registered.getFamily());
												fontFamilyRemap.put(fontFamily, registered.getFamily());
											}
										} catch (FontFormatException ex) {
											LoggerUtils.getLogger(PDFUtils.class).log(Level.SEVERE, null, ex);
										}
										break;
									}
								}
								break;
							}
						}
					}
				}
			}
		}

		// Change font names in all "font-family" properties
		for (CSSRule r : styleSheet.getRules()) {
			if ("@font-face".equals(r.getSelector().get(0))) {
				continue;
			}
			for (CSSProperty p : r.getProperties()) {
				if (p.getName().equalsIgnoreCase("font-family")) {
					int size = p.getParsedValues().size();
					for (int i = 0; i < size; i++) {
						CSSValue pv = p.getParsedValues().get(i);
						String n = null;
						if (pv instanceof CSSValueString) {
							n = ((CSSValueString) pv).getValue();
						} else if (pv instanceof CSSValueName) {
							n = ((CSSValueName) pv).getName();
						} else {
							continue;
						}
						if (fontFamilyRemap.containsKey(n)) {
							p.getValues().set(i, "'" + fontFamilyRemap.get(n) + "'");
						}
					}
				}
			}
		}

	}

	public static Font loadFont(ResourceRetriever retriever, String fontFamily, String url) throws FontFormatException, IOException {

		GraphicsEnvironment g = GraphicsEnvironment.getLocalGraphicsEnvironment();

		ResourceInfo ri = retriever.getResource(url);
		if (ri == null) {
			return null;
		}

		Font font = Font.createFont(Font.TRUETYPE_FONT, ri.getInputStream());

		g.registerFont(font);

		ri.getInputStream().close();

		return font;

	}
	
	public static void inlineImages(ResourceRetriever retriever, Document doc) {
		Selectors selectors = new Selectors(new W3CNode(doc));
		List<Node> nodes = selectors.querySelectorAll("img");
		for (Node n : nodes) {
			Node attr = n.getAttributes().getNamedItem("src");
			if (attr != null && StringUtils.isNotBlank(attr.getNodeValue()) && !attr.getNodeValue().startsWith("data:")) {
				ResourceInfo ri = retriever.getResource(attr.getNodeValue());
				if (ri != null) {
					StringBuilder builder = new StringBuilder();
					builder.append("data:");
					for (MimeTypes mt : MimeTypes.values()) {
						if (mt == ri.getMimeType()) {
							builder.append(mt.getMimeType());
							break;
						}
					}
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					byte[] buff = new byte[4096];
					int read;
					try {
						while ((read = ri.getInputStream().read(buff)) > 0) {
							baos.write(buff, 0, read);
						}
						ri.getInputStream().close();
					} catch (IOException e) {
						LoggerUtils.getCoreLogger().log(Level.WARNING, null, e);
					}
					builder.append(";base64,");
					builder.append(Base64.encodeBase64String(baos.toByteArray()));
					attr.setNodeValue(builder.toString());
				}
			}
		}
	}

	public static class PrepareResult {

		private Measure width;
		private Measure height;
		private boolean removeBorders = false;

		public Measure getWidth() {
			return width;
		}

		public void setWidth(Measure width) {
			this.width = width;
		}

		public Measure getHeight() {
			return height;
		}

		public void setHeight(Measure height) {
			this.height = height;
		}

		public boolean isRemoveBorders() {
			return removeBorders;
		}

		public void setRemoveBorders(boolean removeBorders) {
			this.removeBorders = removeBorders;
		}

	}

	public static enum SheetSize {
		A0("841mm", "1189mm"),
		A1("594mm", "841mm"),
		A2("420mm", "594mm"),
		A3("297mm", "420mm"),
		A4("210mm", "297mm"),
		A5("148mm", "210mm"),
		A6("105mm", "148mm"),
		A7("74mm", "105mm"),
		A8("52mm", "74mm"),
		A9("37mm", "52mm"),
		A10("26mm", "37mm"),
		B0("1000mm", "1414mm"),
		B1("707mm", "1000mm"),
		B2("500mm", "707mm"),
		B3("353mm", "500mm"),
		B4("250mm", "353mm"),
		B5("176mm", "250mm"),
		B6("125mm", "176mm"),
		B7("88mm", "125mm"),
		B8("62mm", "88mm"),
		B9("44mm", "62mm"),
		B10("31mm", "44mm"),
		C0("917mm", "1297mm"),
		C1("648mm", "917mm"),
		C2("458mm", "648mm"),
		C3("324mm", "458mm"),
		C4("229mm", "324mm"),
		C5("162mm", "229mm"),
		C6("114mm", "162mm"),
		C7("81mm", "114mm"),
		C8("57mm", "81mm"),
		C9("40mm", "57mm"),
		C10("28mm", "40mm"),
		DL("110mm", "220mm");

		private final Measure width;
		private final Measure height;

		private SheetSize(String width, String height) {
			this.width = new Measure(width);
			this.height = new Measure(height);
		}

		public Measure getWidth() {
			return width;
		}

		public Measure getHeight() {
			return height;
		}

	}

}
