package com.waveinformatica.ocean.core.filtering.drivers;

public class DataValue implements DataReference {
	
	private String name;
	private final Object value;

	public DataValue(Object value) {
		this.value = value;
	}
	
	public String getName() {
		return name;
	}

	void setName(String name) {
		this.name = name;
	}

	public Object getValue() {
		return value;
	}

}
