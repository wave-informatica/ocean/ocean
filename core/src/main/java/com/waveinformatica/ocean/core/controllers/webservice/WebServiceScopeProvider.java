package com.waveinformatica.ocean.core.controllers.webservice;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.ScopeProvider;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.controllers.scopes.IScopeProvider;
import com.waveinformatica.ocean.core.controllers.scopes.ScopeChain;
import com.waveinformatica.ocean.core.util.Context;

@ScopeProvider(inScope = WebServiceScope.class)
public class WebServiceScopeProvider implements IScopeProvider {
	
	@Inject
	private CoreController controller;
	
	@Override
	public String produce(ScopeChain chain, String path) {
		
		HttpServletRequest request = Context.get().getRequest();
		
		if (request == null) {
			return null;
		}
		
		if ("wsdl".equals(request.getQueryString())) {
			
			WebServiceInvocationScope scope = new WebServiceInvocationScope();
			chain.add(scope);
			
			return "/webservice/wsdl";
			
		}
		else {
			
			if (request.getContentLength() <= 0) {
				return null;
			}
			
			String contentType = request.getContentType();
			if (StringUtils.isBlank(contentType) || !contentType.startsWith("text/xml")) {
				return null;
			}
			
			String soapAction = StringUtils.strip(request.getHeader("SOAPAction"), "\"");
			if (StringUtils.isBlank(soapAction)) {
				return null;
			}
			
			OperationInfo opInfo = controller.getOperationInfo(chain.getCurrent().getClass(), soapAction);
			if (opInfo == null) {
				return null;
			}
			
			WebServiceInvocationScope scope = new WebServiceInvocationScope();
			scope.setOperationInfo(opInfo);
			chain.add(scope);
			
			return "/webservice/invoke";
		}
		
	}

}
