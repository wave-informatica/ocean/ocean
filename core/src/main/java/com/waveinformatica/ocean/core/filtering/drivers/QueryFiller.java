package com.waveinformatica.ocean.core.filtering.drivers;

public interface QueryFiller<T> {
	
	public T fillQueryParams(T query);
	
}
