/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.controllers.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.controllers.scopes.ScopeChain;

public class OperationContext {
	
	private OperationInfo operation;
	private ScopeChain chain;
	private DecoratorInfo decoratorInfo;
	private List<Object> extraObjects;
	
	public OperationInfo getOperation() {
		return operation;
	}
	
	public void setOperation(OperationInfo operation) {
		this.operation = operation;
	}
	
	public ScopeChain getChain() {
		return chain;
	}
	
	public void setChain(ScopeChain chain) {
		this.chain = chain;
	}
	
	public DecoratorInfo getDecoratorInfo() {
		return decoratorInfo;
	}

	public void setDecoratorInfo(DecoratorInfo decoratorInfo) {
		this.decoratorInfo = decoratorInfo;
	}

	public List<Object> getExtraObjects() {
		return extraObjects;
	}
	
	public void setExtraObjects(List<Object> extraObjects) {
		this.extraObjects = extraObjects;
	}

	public Map<String, Object> getAllObjects() {
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("operation", operation);
		map.put("chain", chain);
		map.put("decoratorInfo", decoratorInfo);
		map.put("extraObjects", extraObjects);
		
		return map;
		
	}
	
}
