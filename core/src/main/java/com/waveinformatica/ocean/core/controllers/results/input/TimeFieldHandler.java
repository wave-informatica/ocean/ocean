/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.results.input;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.util.Results;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Ivano
 */
public class TimeFieldHandler implements InputResult.FieldHandler {

    private final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

    @Override
    public String getFieldTemplate() {
	return "/templates/core/components/fieldTime.tpl";
    }

    @Override
    public void init(InputResult result, CoreController.OperationInfo opInfo, CoreController.ParameterInfo paramInfo, InputResult.FormField field) {
	    Results.addCssSource(result, "ris/css/bootstrap-timepicker.min.css");
	    Results.addScriptSource(result, "ris/js/bootstrap-timepicker.js");
    }

    @Override
    public String formatValue(InputResult result, InputResult.FormField field, Object value) {
	if (value instanceof Date) {
	    return timeFormat.format((Date) value);
	}
	return value.toString();
    }

}
