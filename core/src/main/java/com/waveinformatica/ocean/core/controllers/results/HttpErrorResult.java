/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.results;

import com.waveinformatica.ocean.core.util.Context;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.WordUtils;

import org.apache.commons.lang.exception.ExceptionUtils;

/**
 *
 * @author Ivano
 */
public class HttpErrorResult extends WebPageResult {
	
	private static final String[] MASK_FIELD_VALUES = {
			"password",
			"passwd",
			"pass",
			"pwd"
	};
	
	private Throwable throwable;

	public void setErrorCode(int error) {
		setTemplate("/templates/errors/" + error + "-error.tpl");
		setStatusCode(error);
	}

	public void setErrorCode(String error) {
		setTemplate("/templates/errors/" + error + "-error.tpl");
	}

	public Throwable getThrowable() {
		return throwable;
	}

	public void setThrowable(Throwable throwable) {
		this.throwable = throwable;
	}

	@Override
	public void setData(Object data) {
		super.setData(data);
		throwable = (Throwable) data;
	}

	public String getMessage()
	{
		return ExceptionUtils.getRootCauseMessage(throwable);
	}

	public String getFormattedException() {
		StringWriter strWr = new StringWriter();
		PrintWriter writer = new PrintWriter(strWr);
		throwable.printStackTrace(writer);
		return strWr.toString();
	}
	
	public String getMethod() {
		return Context.get() != null && Context.get().getRequest() != null ? Context.get().getRequest().getMethod() : "";
	}
	
	public String getRequestInput() {
		if (Context.get() != null && Context.get().getRequest() != null) {
			
			StringBuilder inputBuilder = new StringBuilder();
			
			HttpServletRequest req = Context.get().getRequest();
			
			inputBuilder.append(req.getMethod());
			inputBuilder.append(" ");
			inputBuilder.append(req.getRequestURL().toString());
			inputBuilder.append(" ");
			inputBuilder.append(req.getProtocol());
			inputBuilder.append("\r\n");
			
			Enumeration<String> headersEnum = req.getHeaderNames();
			while (headersEnum.hasMoreElements()) {
				
				String n = headersEnum.nextElement();
				
				Enumeration<String> valueEnum = req.getHeaders(n);
				while (valueEnum.hasMoreElements()) {

					inputBuilder.append(WordUtils.capitalizeFully(n, new char[]{'-','_',' '}));
					inputBuilder.append(": ");
					if (!Arrays.asList(MASK_FIELD_VALUES).contains(n.trim().toLowerCase())) {
						inputBuilder.append(valueEnum.nextElement());
					}
					else {
						inputBuilder.append("*****");
					}
					inputBuilder.append("\r\n");
					
				}
				
			}
			
			inputBuilder.append("\r\n");
			
			try {
				
				if ("GET".equals(req.getMethod()) || ("POST".equals(req.getMethod()) && req.getContentType().startsWith("application/x-www-form-urlencoded"))) {
					for (String k : req.getParameterMap().keySet()) {
						for (String v : req.getParameterValues(k)) {
							inputBuilder.append(k);
							inputBuilder.append(": \"");
							if (!Arrays.asList(MASK_FIELD_VALUES).contains(k.trim().toLowerCase())) {
								inputBuilder.append(v);
							}
							else {
								inputBuilder.append("*****");
							}
							inputBuilder.append("\"\r\n");
						}
					}
				}
				else if ("POST".equals(req.getMethod()) && req.getContentType().startsWith("multipart/form-data")) {
					Collection<Part> parts = req.getParts();
					if (parts != null) {
						for (Part p : parts) {
							
							inputBuilder.append("--------------------------------\r\n");
							for (String n : p.getHeaderNames()) {
								for (String v : p.getHeaders(n)) {
									inputBuilder.append(WordUtils.capitalizeFully(n, new char[]{'-','_',' '}));
									inputBuilder.append(": ");
									if (!Arrays.asList(MASK_FIELD_VALUES).contains(n.trim().toLowerCase())) {
										inputBuilder.append(v);
									}
									else {
										inputBuilder.append("*****");
									}
									inputBuilder.append("\r\n");
								}
							}
							inputBuilder.append("\r\n");
							byte[] buff = new byte[(int) Math.min(p.getSize(), 1024L)];
							p.getInputStream().read(buff);
							inputBuilder.append(Base64.encodeBase64String(buff));
							if (p.getSize() > 1024) {
								inputBuilder.append(" [[[ ... CONTENT TRUNCATED BECAUSE ");
								inputBuilder.append(p.getSize());
								inputBuilder.append(" > 1024 BYTES ... ]]]");
							}
							inputBuilder.append("\r\n");
							
						}
					}
				}
				else {
					inputBuilder.append("[[[ ... UNKNOWN REQUEST FORMAT AND THE REQUEST INPUT MAY BE INCOMPLETE ... ]]]\r\n");
				}
				
			} catch (Exception e) {
				
				inputBuilder.append("[[[ EXCEPTION WHILE BUILDING REQUEST INPUT ]]]\r\n\r\n");
				StringWriter strWr = new StringWriter();
				PrintWriter writer = new PrintWriter(strWr);
				e.printStackTrace(writer);
				
				inputBuilder.append(strWr.toString());
				
			}
			
			return inputBuilder.toString();
			
		}
		else {
			return "no request";
		}
	}
	
}
