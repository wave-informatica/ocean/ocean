/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.scopes;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.dto.NavigationLink;
import com.waveinformatica.ocean.core.security.SecurityManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 *
 * @author ivano
 */
public class OperationScope extends AbstractScope implements NavigationScope {
	
	@Inject
	private com.waveinformatica.ocean.core.security.SecurityManager securityManager;
	
	private CoreController.OperationInfo operationInfo;
	private final List<NavigationLink> navigationLinks = new ArrayList<NavigationLink>();

	public CoreController.OperationInfo getOperationInfo() {
		return operationInfo;
	}

	public void setOperationInfo(CoreController.OperationInfo operationInfo) {
		this.operationInfo = operationInfo;
	}

	public SecurityManager getSecurityManager() {
		return securityManager;
	}

	@Override
	public boolean isAuthorized() {
		return operationInfo.isSkipAuthorization() || (super.isAuthorized() && securityManager.authorize(operationInfo, getChain()));
	}

	@Override
	public List<NavigationLink> getNavigationLinks() {
		return navigationLinks;
	}
	
	public void addBreadcrumbLink(String url, String text) {
		navigationLinks.add(new NavigationLink(url, text));
	}
	
}
