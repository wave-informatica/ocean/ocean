/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.controllers.scopes;

/**
 * This interface is provided to support scopes defining header and/or footer templates.
 * Write a scope implementing this interface to surround the result template with the scope's
 * specific templates.
 * 
 * @author ivano
 *
 */
public interface TemplateScope {
	
	/**
	 * The template to render before the result
	 * 
	 * @return the header template
	 */
	public String getHeaderTemplate();
	
	/**
	 * The template to render after the result
	 * 
	 * @return the footer template
	 */
	public String getFooterTemplate();
	
}
