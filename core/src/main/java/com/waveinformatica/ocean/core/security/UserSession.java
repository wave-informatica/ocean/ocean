/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.security;

import com.waveinformatica.ocean.core.cdi.GlobalSessionScoped;
import com.waveinformatica.ocean.core.util.Context;
import java.io.Serializable;
import java.util.Locale;
import javax.inject.Named;

/**
 * This class represents the current user's session.
 *
 * @group security
 * @author Ivano
 */
@Named
@GlobalSessionScoped
public class UserSession implements Serializable {
	
	private OceanUser principal;
	private String userName;
	private Locale locale;
	
	/**
	 * Logs in a user
	 *
	 * @param user the object representing the logged in user
	 */
	public void login(OceanUser user) {
		this.principal = user;
		userName = user.toString();
	}
	
	/**
	 * Logs out the current user
	 */
	public void logout() {
		userName = null;
		principal = null;
	}
	
	/**
	 * Returns if the current user is logged in
	 *
	 * @return <code>true</code> if the current user is logged in
	 */
	public boolean isLogged() {
		return userName != null && userName.trim().length() > 0;
	}
	
	/**
	 * Retrieves the user name of the logged in user (as principal.toString())
	 *
	 * @return The user's user name as principal.toString()
	 */
	public String getUserName() {
		return userName;
	}
	
	/**
	 * Returns the object representing the current logged in user or null if no user is logged in
	 *
	 * @return the current user object or null if not logged in
	 */
	public OceanUser getPrincipal() {
		return principal;
	}
	
	/**
	 * Returns the currently selected locale. If it is not set, returns the first locale in context.
	 * 
	 * @return the selected locale
	 */
	public Locale getLocale() {
		if (locale == null && Context.get() != null &&
			  Context.get().getLanguages() != null &&
			  Context.get().getLanguages().length > 0) {
			return Context.get().getLanguages()[0];
		}
		else {
			return locale;
		}
	}
	
	/**
	 * Sets the current desired locale
	 * 
	 * @param locale the desired locale
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	
}
