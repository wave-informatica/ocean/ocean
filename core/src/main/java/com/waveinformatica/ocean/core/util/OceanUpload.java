/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.Part;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.controllers.results.MimeTypes;

public class OceanUpload {
	
	private Part part;
	private String fileName;
	private String contentType;
	private MimeTypes knownContentType;
	private Long size;

	public OceanUpload(Part part) {
		super();
		this.part = part;
	}

	public OceanUpload() {
		super();
	}
	
	public boolean isValid() {
		return part != null && StringUtils.isNotBlank(fileName) && StringUtils.isNotBlank(contentType);
	}

	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public MimeTypes getKnownContentType() {
		return knownContentType;
	}

	public void setKnownContentType(MimeTypes knownContentType) {
		this.knownContentType = knownContentType;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public InputStream getInputStream() throws IOException {
		return part.getInputStream();
	}
	
	public void save(File file) throws IOException {
		try {
			save(file, null);
		} catch (NoSuchAlgorithmException e) {
			// No hash -> no exception
		}
	}
	
	public MessageDigest save(File file, String hash) throws IOException, NoSuchAlgorithmException {
		
		MessageDigest md = null;
		
		FileOutputStream fos = new FileOutputStream(file);
		try {
			
			byte[] buff = new byte[4096];
			int read;
			InputStream is = part.getInputStream();
			while ((read = is.read(buff)) > 0) {
				fos.write(buff, 0, read);
				
				if (StringUtils.isNotBlank(hash)) {
					if (md == null) {
						md = MessageDigest.getInstance(hash);
					}
					md.update(buff, 0, read);
				}
			}
			
		} finally {
			fos.close();
		}
		
		return md;
	}
	
}
