package com.waveinformatica.ocean.core.interfaces;

import java.io.InputStream;

public interface InputStreamProvider {
	
	public InputStream getStream();
	
}
