/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.cdi;

import java.io.Serializable;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.AfterBeanDiscovery;
import javax.enterprise.inject.spi.Extension;

public class OceanScopesExtension implements Extension, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*public void addScope(@Observes final BeforeBeanDiscovery event) {
        event.addScope(RequestScoped.class, true, false);
        event.addScope(SessionScoped.class, true, false);
    }*/

	public void registerContext(@Observes final AfterBeanDiscovery event) {
		event.addContext(new RequestScopeContext());
		event.addContext(new ConversationScopeContext());
		event.addContext(new SessionScopeContext());
		event.addContext(new GlobalSessionScopeContext());
	}

}
