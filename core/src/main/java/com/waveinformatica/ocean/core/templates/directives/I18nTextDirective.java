/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.templates.directives;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.annotations.FreemarkerDirective;
import com.waveinformatica.ocean.core.util.I18N;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateSequenceModel;

/**
 *
 * @author Ivano
 */
@FreemarkerDirective("i18n")
public class I18nTextDirective implements TemplateDirectiveModel {

	private static final String KEY_ATTR = "key";
	private static final String PARAM_ATTR = "param";

	@Inject
	private I18N i18n;

	@Override
	public void execute(Environment e, Map map, TemplateModel[] tms, TemplateDirectiveBody tdb) throws TemplateException, IOException
	{

		if (map.size() > 2) {
			throw new TemplateModelException("Only " + KEY_ATTR + " and " + PARAM_ATTR + " properties are supported");
		}

		TemplateModel key = (TemplateModel) map.get(KEY_ATTR);
		if (key == null) {
			throw new TemplateModelException(KEY_ATTR + " property is required");
		}

		TemplateModel param = (TemplateModel) map.get(PARAM_ATTR);
		if (param instanceof TemplateSequenceModel) {
			String[] values = new String[((TemplateSequenceModel) param).size()];
			for (int i = 0; i < values.length; i++) {
				values[i] = ((TemplateSequenceModel) param).get(i).toString();
			}
			e.getOut().write(i18n.translate(key.toString(), values));
		}
		else if (param != null) {
			e.getOut().write(i18n.translate(key.toString(), param.toString()));
		}
		else {
			e.getOut().write(i18n.translate(key.toString()));
		}
	}
}
