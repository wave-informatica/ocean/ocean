/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.security;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;

import com.waveinformatica.ocean.core.controllers.events.SecurityConfigurationEvent;
import com.waveinformatica.ocean.core.controllers.events.WebRequestEvent;
import com.waveinformatica.ocean.core.controllers.results.RedirectResult;

/**
 * 
 * @group security
 * @author ivano
 *
 */
public class OceanAuthenticationHandler implements AuthenticationHandler {

	@Override
	public Object getAuthenticationResult(WebRequestEvent req, SecurityConfigurationEvent secConfigEvent, String opName) {
		
		if (secConfigEvent.getLoginOperationBase() == null) {
			return null;
		}
		
		StringBuilder builder = new StringBuilder();
		builder.append(secConfigEvent.getLoginOperationBase());
		if (secConfigEvent.getLoginOperationBase().indexOf('?') != -1) {
			builder.append('&');
		} else {
			builder.append('?');
		}
		builder.append("redirect=");
		if (Arrays.asList("GET", "HEAD").contains(req.getRequest().getMethod())) {
			StringBuilder subQueryBuilder = new StringBuilder();
			subQueryBuilder.append(opName);
			if (req.getRequest().getQueryString() != null) {
				if (!req.getRequest().getQueryString().startsWith("?"))
					subQueryBuilder.append('?');
				subQueryBuilder.append(req.getRequest().getQueryString());
			}
			try {
				builder.append(URLEncoder.encode(subQueryBuilder.toString(), "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				builder.append(subQueryBuilder.toString());
			}
		} else {
			builder.append("/");
		}
		
		return new RedirectResult(builder.toString(), 303);
		
	}

}
