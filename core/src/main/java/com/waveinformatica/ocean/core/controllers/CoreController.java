/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers;

import java.lang.annotation.Annotation;
import java.lang.management.ManagementFactory;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.Constants;
import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.annotations.EventListener;
import com.waveinformatica.ocean.core.annotations.OceanMBean;
import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.annotations.ResultDecorator;
import com.waveinformatica.ocean.core.annotations.RootOperation;
import com.waveinformatica.ocean.core.annotations.Service;
import com.waveinformatica.ocean.core.annotations.SkipAuthorization;
import com.waveinformatica.ocean.core.annotations.TypeConverter;
import com.waveinformatica.ocean.core.controllers.decorators.IDecorator;
import com.waveinformatica.ocean.core.controllers.dto.DecoratorInfo;
import com.waveinformatica.ocean.core.controllers.dto.OperationContext;
import com.waveinformatica.ocean.core.controllers.dto.ServiceStatus;
import com.waveinformatica.ocean.core.controllers.events.BeforeOperationEvent;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.Event;
import com.waveinformatica.ocean.core.controllers.events.OperationErrorEvent;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.scopes.AbstractScope;
import com.waveinformatica.ocean.core.controllers.scopes.OperationScope;
import com.waveinformatica.ocean.core.controllers.scopes.RootScope;
import com.waveinformatica.ocean.core.controllers.scopes.ScopeChain;
import com.waveinformatica.ocean.core.controllers.scopes.ScopesController;
import com.waveinformatica.ocean.core.converters.EnumConverter;
import com.waveinformatica.ocean.core.converters.ITypeConverter;
import com.waveinformatica.ocean.core.exceptions.ModuleConsistencyException;
import com.waveinformatica.ocean.core.exceptions.ModuleException;
import com.waveinformatica.ocean.core.exceptions.OperationNotAuthorizedException;
import com.waveinformatica.ocean.core.exceptions.OperationNotFoundException;
import com.waveinformatica.ocean.core.modules.ModuleClassLoader;
import com.waveinformatica.ocean.core.tasks.OceanCallable;
import com.waveinformatica.ocean.core.tasks.OceanExecutorService;
import com.waveinformatica.ocean.core.tasks.OceanRunnable;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.OperationParameters;
import com.waveinformatica.ocean.core.util.RequestCache;

/**
 * This class represents the core Ocean's controller. It provides the main
 * framework's features to have access to all the main components.
 *
 * @author Ivano
 */
@Named
@ApplicationScoped
public class CoreController {

	private static Logger logger;

	@Inject
	private ObjectFactory objectFactory;
	
	@Inject
	private ScopesController scopesController;
	
	@Inject
	private OceanExecutorService executor;

	private Map<String, List<EventListenerInfo>> eventHandlers = new HashMap<String, List<EventListenerInfo>>();
	private Map<Class<?>, Map<String, List<OperationInfo>>> operationHandlers = new HashMap<Class<?>, Map<String, List<OperationInfo>>>();
	@SuppressWarnings("rawtypes")
	private Map<MimeTypes, Map<Class, Class>> decorators = new HashMap<MimeTypes, Map<Class, Class>>();
	@SuppressWarnings("rawtypes")
	private Map<String, Class> typeConverters = new HashMap<String, Class>();
	@SuppressWarnings("rawtypes")
	private Map<String, List<Class>> componentsByModule = new HashMap<String, List<Class>>();
	private Map<String, IService> services = new HashMap<String, IService>();
	private Map<ObjectName, Object> mbeans = new HashMap<ObjectName, Object>();
	
	private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

	@PostConstruct
	public void init() {
		logger = LoggerUtils.getCoreLogger();
	}

	/**
	 * Fires the given event
	 *
	 * @param event the event to fire
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void dispatchEvent(Event event) {
		
		try {
			if (!lock.readLock().tryLock(30, TimeUnit.SECONDS)) {
				throw new RuntimeException("Unable to acquire read lock in 30 seconds for dispatching event");
			}
		} catch (InterruptedException e1) {
			throw new RuntimeException("Interrupted while acquiring read lock for dispatching event", e1);
		}
		
		try {
			
			logger.log(Level.INFO, "Dispatching event " + event.getClass().getName() + "(" + event.getName() + ") from a " + (event.getSource() != null ? event.getSource().getClass().getName() : "[unknown source]") + " object");

			List<EventListenerInfo> handlersInfo = eventHandlers.get(event.getName());
			if (handlersInfo == null || handlersInfo.size() == 0) {
				return;
			}
			for (EventListenerInfo eli : handlersInfo) {

				boolean ok = false;

				// Check flags
				if (event.getFlags().size() > 0) {
					if (eli.getConfig() != null) {
						for (String sf : eli.getConfig().flags()) {
							if (event.getFlags().contains(sf)) {
								ok = true;
								break;
							}
						}
					} else {
						for (String sf : eli.getConfigGeneric().flags()) {
							if (event.getFlags().contains(sf)) {
								ok = true;
								break;
							}
						}
					}
				} else if ((eli.getConfig() != null && eli.getConfig().flags().length == 0)
					  || (eli.getConfigGeneric() != null && eli.getConfigGeneric().flags().length == 0)) {
					ok = true;
				}

				// Check source constraint
				if (ok && ((eli.getConfig() != null && eli.getConfig().sourceNames().length > 0)
					  || (eli.getConfigGeneric() != null && eli.getConfigGeneric().sourceNames().length > 0))) {
					boolean found = false;
					String[] sourceNames = eli.getConfig() != null ? eli.getConfig().sourceNames() : eli.getConfigGeneric().sourceNames();
					for (String s : sourceNames) {
						if (s.equals(event.getSource().getClass().getName())) {
							found = true;
							break;
						}
					}
					ok = found;
				}

				// Dispatch event to handler
				if (ok) {

					IEventListener listener = (IEventListener) objectFactory.newInstance(eli.getCls());
					try {
						listener.performAction(event);
					} catch (Throwable e) {
						LoggerUtils.getLogger(listener.getClass()).log(Level.SEVERE, "Error invoking event listener \"" + listener.getClass() + "\" on event \"" + event.getName() + "\"", e);
						if (!event.getName().equals(CoreEventName.OPERATION_ERROR.name())) {
							dispatchEvent(new OperationErrorEvent(this, e));
						}
					}

				}

			}

		} finally {
			lock.readLock().unlock();
		}

	}
	
	/**
	 * Fires the given event asynchronously.
	 * 
	 * @param event the event to be fired
	 * @return the future with the provided event
	 */
	public <E extends Event> Future<E> dispatchEventAsync(final E event) {
		return executor.submit(OceanRunnable.create(new Runnable() {
			@Override
			public void run() {
				dispatchEvent(event);
			}
		}), event);
	}

	/**
	 * Returns info about the requested operation
	 *
	 * @param name the operation's name
	 * @return the operation info or null if no operation is found
	 */
	@SuppressWarnings("rawtypes")
	public OperationInfo getOperationInfo(Class<?> scope, String name) {
		
		List<Class> types = new ArrayList<Class>();
		while (scope != Object.class) {
			types.add(scope);
			types.addAll(Arrays.asList(scope.getInterfaces()));
			scope = scope.getSuperclass();
		}
		
		for (Class c : types) {
			Map<String, List<OperationInfo>> scopedHandlers = operationHandlers.get(c);
			if (scopedHandlers != null && !scopedHandlers.isEmpty()) {
				List<OperationInfo> handlers = scopedHandlers.get(name);
				if (handlers != null && !handlers.isEmpty()) {
					return handlers.get(0);
				}
			}
		}

		return null;
	}
	
	/**
	 * This method just invokes the {@link #executeOperation(AbstractScope, String, Map, Object...) executeOperation(AbstractScope, String, Map, Object...)}
	 * method with a {@link RootScope} instance.
	 * 
	 * @param name the operation's name
	 * @param args the map of arguments for the operation
	 * @param extraObjects execution context parameters
	 * @return the operation's result
	 * @throws OperationNotFoundException if no operation found with the given
	 */
	public Object executeOperation(String name, Map<String, String[]> args, Object... extraObjects)
			  throws OperationNotFoundException {
		
		return executeOperation(objectFactory.newInstance(RootScope.class), name, args, extraObjects);
		
	}
	
	/**
	 * This method just asynchronously invokes the {@link #executeOperation(AbstractScope, String, Map, Object...) executeOperation(AbstractScope, String, Map, Object...)}
	 * method with a {@link RootScope} instance.
	 * 
	 * @param name
	 * @param args
	 * @param extraObjects
	 * @return
	 */
	public Future<Object> executeOperationAsync(final String name, final Map<String, String[]> args, final Object... extraObjects) {
		return executor.submit(OceanCallable.create(new Callable<Object>() {
			@Override
			public Object call() throws Exception {
				return executeOperation(objectFactory.newInstance(RootScope.class), name, args, extraObjects);
			}
		}));
	}

	/**
	 * Executes the specified operation with the given arguments
	 *
	 * @param rootScope the instance of the rootScope for the operation
	 * @param name the operation's name
	 * @param args the map of arguments for the operation
	 * @param extraObjects execution context parameters
	 * @return the operation's result
	 * @throws OperationNotFoundException if no operation found with the given
	 * name
	 */
	@SuppressWarnings("unchecked")
	public Object executeOperation(AbstractScope rootScope, String name, Map<String, String[]> args, Object... extraObjects)
		  throws OperationNotFoundException {
		
		try {
			if (!lock.readLock().tryLock(30, TimeUnit.SECONDS)) {
				throw new RuntimeException("Unable to acquire read lock in 30 seconds for executing operation " + name);
			}
		} catch (InterruptedException e1) {
			throw new RuntimeException("Interrupted while acquiring read lock for executing operation " + name, e1);
		}
		
		try {
			
			Object res = null;
			Object provider = null;

			ScopeChain chain = scopesController.build(name, rootScope);
			if (chain.isEmpty() || !(chain.getCurrent() instanceof OperationScope)) {
				throw new OperationNotFoundException(name);
			}

			OperationScope opScope = (OperationScope) chain.getCurrent();

			final OperationInfo oi = opScope.getOperationInfo();

			if (oi != null) {

				if (!opScope.isAuthorized()) {
					throw new OperationNotAuthorizedException(name);
				}

				BeforeOperationEvent boe = new BeforeOperationEvent(this, oi, args, extraObjects);
				dispatchEvent(boe);

				boe.getExtraObjects().add(oi);
				boe.getExtraObjects().add(chain);
				boe.getExtraObjects().add(new OperationParameters(args));
				for (AbstractScope scope : chain) {
					boe.getExtraObjects().add(scope);
				}
				if (opScope.getSecurityManager().isLogged()) {
					boe.getExtraObjects().add(opScope.getSecurityManager().getPrincipal());
				}
				extraObjects = boe.getExtraObjects().toArray();
				
				OperationContext ctx = null;
				for (Object o : boe.getExtraObjects()) {
					if (o instanceof OperationContext) {
						ctx = (OperationContext) o;
						break;
					}
				}
				if (ctx == null) {
					ctx = new OperationContext();
				}
				ctx.setChain(chain);
				ctx.setOperation(oi);
				ctx.setExtraObjects(boe.getExtraObjects());

				try {
					List<Object> finalArgs = new ArrayList<Object>(oi.getParameters().size());
					for (ParameterInfo pi : oi.getParameters()) {
						if (pi.getName() != null && !args.containsKey(pi.getName()) && pi.getDefaultValue() != null) {
							args.put(pi.getName(), pi.getDefaultValue());
						}
						if (pi.getConverter() != null) {
							ITypeConverter converter = objectFactory.newInstance(pi.getConverter());
							finalArgs.add(converter.fromParametersMap(pi.getName(), pi.getParamClass(), pi.getGenericType(), args, extraObjects));
						} else if (pi.getName() == null) {
							boolean found = false;
							if (pi.getParamClass().isAssignableFrom(OperationContext.class)) {
								finalArgs.add(ctx);
								found = true;
							}
							else {
								for (Object eo : extraObjects) {
									if (eo != null && pi.getParamClass().isAssignableFrom(eo.getClass())) {
										finalArgs.add(eo);
										found = true;
										break;
									}
								}
							}
							if (!found &&
									// Exclude HttpServletRequest, HttpServletResponse, etc.
									!pi.getParamClass().getPackage().getName().equals(HttpServletRequest.class.getPackage().getName())) {
								Object eo = objectFactory.getBean(oi.getMethod(), pi.getPosition());
								finalArgs.add(eo);
								found = true;
							}
							if (!found) {
								finalArgs.add(null);
							}
						} else if (pi.getName() != null && pi.getParamClass() == String.class
							  && args.get(pi.getName()) != null && args.get(pi.getName()).length == 1) {
							finalArgs.add(args.get(pi.getName())[0]);
						} else {
							finalArgs.add(null);
						}
					}

					RequestCache requestCache = objectFactory.getBean(RequestCache.class);
					provider = requestCache.get(OperationProvider.class.getName() + "::" + oi.getCls().getName(), new RequestCache.Factory() {
						@Override
						public Object create() {
							return objectFactory.newInstance(oi.getCls());
						}
					});

					res = oi.getMethod().invoke(provider, finalArgs.toArray(new Object[oi.getParameters().size()]));

				} catch (IllegalAccessException ex) {
					Logger localLogger = CoreController.logger;
					if (provider != null) {
						localLogger = LoggerUtils.getLogger(provider.getClass());
					}
					localLogger.log(Level.SEVERE, "Error invoking operation \"" + name + "\"", ex);
					dispatchEvent(new OperationErrorEvent(this, ex));
				} catch (IllegalArgumentException ex) {
					Logger localLogger = CoreController.logger;
					if (provider != null) {
						localLogger = LoggerUtils.getLogger(provider.getClass());
					}
					localLogger.log(Level.SEVERE, "Error invoking operation \"" + name + "\"", ex);
					dispatchEvent(new OperationErrorEvent(this, ex));
				} catch (InvocationTargetException ex) {
					if (ex.getCause() instanceof OperationNotAuthorizedException) {
						throw new OperationNotAuthorizedException(ex.getCause());
					} else if (ex.getCause() instanceof OperationNotFoundException) {
						throw new OperationNotFoundException(ex.getCause());
					} else {
						dispatchEvent(new OperationErrorEvent(this, ex.getCause()));
						throw new RuntimeException("Unexpected error invoking operation \"" + name + "\"", ex.getCause());
					}
				}
			} else {
				throw new OperationNotFoundException(name);
			}
			return res;

		} finally {
			lock.readLock().unlock();
		}
	}
	
	/**
	 * This method just asynchronously invokes the {@link #executeOperation(AbstractScope, String, Map, Object...) executeOperation(AbstractScope, String, Map, Object...)}.
	 * 
	 * @param scope
	 * @param name
	 * @param args
	 * @param extraObjects
	 * @return
	 */
	public Future<Object> executeOperationAsync(final AbstractScope scope, final String name, final Map<String, String[]> args, final Object... extraObjects) {
		return executor.submit(OceanCallable.create(new Callable<Object>() {
			@Override
			public Object call() throws Exception {
				return executeOperation(scope, name, args, extraObjects);
			}
		}));
	}

	/**
	 * Finds the best decorator given the result class names and the list of
	 * accepted mime types
	 *
	 * @param cls the result class
	 * @param mimeTypesList the list of accepted mime types from the best
	 * accepted to the worst one
	 * @return info about the found decorator or null if no decorators found
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DecoratorInfo findDecorator(Class cls, List<MimeTypes> mimeTypesList, Object scope) {
		
		try {
			if (!lock.readLock().tryLock(30, TimeUnit.SECONDS)) {
				throw new RuntimeException("Unable to acquire read lock in 30 seconds for finding decorator");
			}
		} catch (InterruptedException e1) {
			throw new RuntimeException("Interrupted while acquiring read lock for finding decorator", e1);
		}
		
		try {
			
			List<Class> types = new ArrayList<Class>();

			Class c = cls;
			while (c != null) {
				types.add(c);
				types.addAll(Arrays.asList(c.getInterfaces()));
				c = c.getSuperclass();
			}

			for (MimeTypes mt : mimeTypesList) {

				Map<Class, Class> map = decorators.get(mt);
				if (map != null) {

					for (Class cn : types) {
						Class res = map.get(cn);
						if (res != null) {
							
							boolean found = (scope == null);
							
							if (!found) {
								ResultDecorator annotation = (ResultDecorator) res.getAnnotation(ResultDecorator.class);
								
								ext:
								for (Class<?> dc : annotation.inScope()) {
									AbstractScope as = (AbstractScope) scope;
									while (as != null) {
										if (dc.isAssignableFrom(as.getClass())) {
											found = true;
											break ext;
										}
										as = as.getParent();
									}
								}
								
								if (!found) {
									continue;
								}
							}

							IDecorator dec = (IDecorator) objectFactory.newInstance(res);
							return new DecoratorInfo(dec, mt);

						}
					}

				}

			}

			return null;

		} finally {
			lock.readLock().unlock();
		}
	}

	/**
	 * Finds the class of a type converter per type name
	 *
	 * @param className the type name
	 * @return the found converter or null if no converter found
	 */
	@SuppressWarnings("rawtypes")
	public Class findConverter(String className) {
		return typeConverters.get(className);
	}

	/**
	 * Returns the loaded services status
	 *
	 * @return statuses for services provided by all the loaded modules
	 */
	public List<ServiceStatus> getServices() {
		return getServices(null);
	}

	/**
	 * Returns the services status per module
	 *
	 * @param module the module's name
	 * @return statuses for services provided by the specified module
	 */
	public List<ServiceStatus> getServices(String module) {
		
		try {
			if (!lock.readLock().tryLock(30, TimeUnit.SECONDS)) {
				throw new RuntimeException("Unable to acquire read lock in 30 seconds for getting services");
			}
		} catch (InterruptedException e1) {
			throw new RuntimeException("Interrupted while acquiring read lock for getting services", e1);
		}

		try {
			
			List<ServiceStatus> statuses = new ArrayList<ServiceStatus>(services.size());

			for (IService s : services.values()) {
				if (module != null && module.trim().length() > 0) {
					ClassLoader srvCl = s.getClass().getClassLoader();
					if (srvCl instanceof ModuleClassLoader) {
						ModuleClassLoader mcl = (ModuleClassLoader) srvCl;
						if (!mcl.getModule().getName().equals(module)) {
							continue;
						}
					}
				}
				Service annotation = s.getClass().getAnnotation(Service.class);
				ServiceStatus ss = s.getServiceStatus();
				ss.setName(annotation.value());
				ss.setAnnotation(annotation);
				statuses.add(ss);
			}

			return statuses;

		} finally {
			lock.readLock().unlock();
		}

	}

	/**
	 * Finds a service by its name
	 *
	 * @param name the service's name
	 * @return the requested service or null if no service found
	 */
	public IService getService(String name) {
		return services.get(name);
	}

	/**
	 * This method is a utility method to retrieve the Service instance of the
	 * specified type. It is preferable to use this method to write better and
	 * linked code.
	 *
	 * @param <T> the service class type
	 * @param serviceClass the service's type
	 * @return the requested service if available
	 * @throws IllegalArgumentException if the argument class is not annotated
	 * with {@link Service} annotation.
	 */
	@SuppressWarnings("unchecked")
	public <T extends IService> T getService(Class<T> serviceClass) {

		Service annotation = serviceClass.getAnnotation(Service.class);
		if (annotation == null) {
			throw new IllegalArgumentException("The service class MUST be annotated with " + Service.class.getName());
		}

		return (T) getService(annotation.value());

	}

	/**
	 * FOR INTERNAL USE ONLY. Loads core components per module
	 *
	 * @param moduleName the module's name
	 * @param annotatedClasses the set of annotated classes by annotation
	 * class name
	 * @throws com.waveinformatica.ocean.core.exceptions.ModuleException if an error occurs or interrupted while acquiring write lock
	 */
	public void loadComponents(String moduleName, Map<Class<? extends Annotation>, Set<Class<?>>> annotatedClasses) throws ModuleException {
		
		try {
			
			lock.writeLock().lockInterruptibly();
			
			try {

				// MBeans
				loadMBeans(annotatedClasses.get(OceanMBean.class));

				// Event handlers
				loadEventHandlers(moduleName, annotatedClasses.get(CoreEventListener.class));
				loadEventHandlers(moduleName, annotatedClasses.get(EventListener.class));

				// Type converters
				loadTypeConverters(moduleName, annotatedClasses.get(TypeConverter.class));

				// Operation providers
				loadOperations(moduleName, annotatedClasses.get(OperationProvider.class));

				// Decorators
				loadDecorators(moduleName, annotatedClasses.get(ResultDecorator.class));

				// Services
				loadServices(moduleName, annotatedClasses.get(Service.class));

			} finally {
				lock.writeLock().unlock();
			}

		} catch (InterruptedException e) {
			throw new ModuleException("Interrupted while waiting for loading components for module " + moduleName, e);
		} catch (Throwable e) {
			throw new ModuleException("Unexpected error occurred while loading components for module " + moduleName, e);
		}

	}

	@SuppressWarnings("rawtypes")
	private void loadModuleClasses(String moduleName, Set<Class<?>> classes) {
		List<Class> storedCls = componentsByModule.get(moduleName);
		if (storedCls == null) {
			storedCls = new LinkedList<Class>();
			componentsByModule.put(moduleName, storedCls);
		}
		storedCls.addAll(classes);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void loadEventHandlers(String moduleName, Set<Class<?>> classes) {
		if (classes == null || classes.isEmpty()) {
			return;
		}

		loadModuleClasses(moduleName, classes);

		for (Class c : classes) {
			if (!IEventListener.class.isAssignableFrom(c)) {
				LoggerUtils.getLogger(c).warning("Event handler " + c.getName() + " does not implement EventListener interface");
				continue;
			}
			CoreEventListener cel = (CoreEventListener) c.getAnnotation(CoreEventListener.class);
			if (cel != null) {
				for (CoreEventName cen : cel.eventNames()) {
					if (eventHandlers.get(cen.name()) == null) {
						eventHandlers.put(cen.name(), new LinkedList<EventListenerInfo>());
					}
					eventHandlers.get(cen.name()).add(0, new EventListenerInfo(c, cel));
				}
			} else {
				EventListener el = (EventListener) c.getAnnotation(EventListener.class);
				for (String en : el.eventNames()) {
					if (eventHandlers.get(en) == null) {
						eventHandlers.put(en, new LinkedList<EventListenerInfo>());
					}
					eventHandlers.get(en).add(0, new EventListenerInfo(c, el));
				}
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void loadOperations(String moduleName, Set<Class<?>> classes) {
		if (classes == null || classes.isEmpty()) {
			return;
		}

		loadModuleClasses(moduleName, classes);

		for (Class providerClass : classes) {
			OperationProvider op = (OperationProvider) providerClass.getAnnotation(OperationProvider.class);
			Class c = providerClass;
			if (Modifier.isAbstract(c.getModifiers())
				  || Modifier.isInterface(c.getModifiers())
				  || Modifier.isNative(c.getModifiers())
				  || Modifier.isPrivate(c.getModifiers())
				  || Modifier.isProtected(c.getModifiers())) {
				throw new ModuleConsistencyException("Unsupported class type for @OperationProvider and class \"" + c.getName() + "\"");
			}
			while (c != Object.class) {
				Method[] methods = c.getDeclaredMethods();
				for (Method m : methods) {
					if (Modifier.isStatic(m.getModifiers())
						  || !Modifier.isPublic(m.getModifiers())) {
						continue;
					}
					Operation o = m.getAnnotation(Operation.class);
					if (o == null) {
						continue;
					}
					String opName = buildOperationName(op, o);
					SkipAuthorization sa = m.getAnnotation(SkipAuthorization.class);
					RootOperation ro = m.getAnnotation(RootOperation.class);
					for (Class<?> opScope : op.inScope()) {
						Map<String, List<OperationInfo>> scopedHandlers = operationHandlers.get(opScope);
						if (scopedHandlers == null) {
							scopedHandlers = new HashMap<String, List<OperationInfo>>();
							operationHandlers.put(opScope, scopedHandlers);
						}
						List<OperationInfo> ops = scopedHandlers.get(opName);
						if (ops == null) {
							ops = new LinkedList<OperationInfo>();
							scopedHandlers.put(opName, ops);
						} else if (!ops.isEmpty()) {
							OperationInfo tmpOi = ops.get(0);
							if (tmpOi.getCls() == c || c.isAssignableFrom(tmpOi.getCls())) {	// Avoid back operation substitution in parent classes
								continue;
							}
							for (OperationInfo oi : ops) {
								if (oi.getCls().getClassLoader() instanceof ModuleClassLoader) {
									ModuleClassLoader loader = (ModuleClassLoader) oi.getCls().getClassLoader();
									if (loader.getModule().getName().equals(moduleName)) {
										throw new ModuleConsistencyException("Duplicated operation \"" + opName + "\" in module \"" + moduleName + "\" loading @Operation \"" + m.getName() + "\" in @OperationProvider \"" + providerClass.getName() + "\" (Already loaded: method \"" + oi.getMethod().getName() + "\" in class \"" + oi.getCls().getName() + "\").");
									}
								}
							}
						}
						ops.add(0, new OperationInfo(providerClass, m, buildParamInfo(m), sa != null, ro != null));
					}
				}
				c = c.getSuperclass();
			}
		}

	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void loadMBeans(Set<Class<?>> classes) {
		
		if (classes == null || classes.isEmpty()) {
			return;
		}
		
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		
		for (Class c : classes) {
			
			OceanMBean annotation = (OceanMBean) c.getAnnotation(OceanMBean.class);
			
			String name = annotation.value();
			if (StringUtils.isBlank(name)) {
				name = c.getSimpleName();
			}
			
			try {
				
				ObjectName on = new ObjectName("OCEAN_" + Application.getInstance().getNamespace() + ":type=" + name);
				
				Object o = objectFactory.newInstance(c);
				
				mbs.registerMBean(o, on);
				
				mbeans.put(on, o);
				
			} catch (MalformedObjectNameException ex) {
				logger.log(Level.SEVERE, "Unable to instantiate MBean " + c.getName(), ex);
			} catch (InstanceAlreadyExistsException ex) {
				logger.log(Level.SEVERE, "Unable to instantiate MBean " + c.getName(), ex);
			} catch (MBeanRegistrationException ex) {
				logger.log(Level.SEVERE, "Unable to instantiate MBean " + c.getName(), ex);
			} catch (NotCompliantMBeanException ex) {
				logger.log(Level.SEVERE, "Unable to instantiate MBean " + c.getName(), ex);
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Unexpected error instantiating MBean " + c.getName());
			}
			
		}
		
	}

	private static String buildOperationName(OperationProvider op, Operation o) {
		String opName = o.value();
		if (opName.length() > 0 && opName.charAt(0) == '/') {
			opName = opName.substring(1);
		}
		if (!op.namespace().endsWith("/")) {
			opName = op.namespace() + "/" + opName;
		} else {
			opName = op.namespace() + opName;
		}
		if (opName.charAt(0) != '/') {
			opName = "/" + opName;
		}

		return opName;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private List<ParameterInfo> buildParamInfo(Method m) {
		List<ParameterInfo> params = new LinkedList<ParameterInfo>();

		Class[] types = m.getParameterTypes();
		Type[] genericTypes = m.getGenericParameterTypes();
		Annotation[][] annotations = m.getParameterAnnotations();
		for (int i = 0; i < types.length; i++) {
			String name = null;
			Class converter = null;
			if (types[i].isEnum()) {
				converter = EnumConverter.class;
			} else {
				converter = typeConverters.get(types[i].getName());
			}
			String[] defaultValue = null;
			for (Annotation a : annotations[i]) {
				if (a.annotationType() == Param.class) {
					Param pa = (Param) a;
					name = pa.value();
					if (pa.converter() != ITypeConverter.class) {
						converter = pa.converter();
					}
					if (pa.defaultValue().length > 0) {
						defaultValue = pa.defaultValue();
					}
				}
			}
			params.add(new ParameterInfo(i, name, types[i], genericTypes[i],
				  converter, m.getParameterAnnotations()[i], defaultValue));
		}

		return params;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void loadDecorators(String moduleName, Set<Class<?>> classes) {
		if (classes == null || classes.isEmpty()) {
			return;
		}

		loadModuleClasses(moduleName, classes);

		for (Class c : classes) {
			if (!IDecorator.class.isAssignableFrom(c)) {
				LoggerUtils.getLogger(c).warning("Decorator " + c.getName() + " does not implement " + IDecorator.class.getName() + " interface. Skipping.");
				continue;
			}

			ResultDecorator annotation = (ResultDecorator) c.getAnnotation(ResultDecorator.class);
			for (MimeTypes mt : annotation.mimeTypes()) {
				Map<Class, Class> map = decorators.get(mt);
				if (map == null) {
					map = new HashMap<Class, Class>();
					decorators.put(mt, map);
				}
				for (Class type : annotation.classes()) {
					map.put(type, c);
				}
			}
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void loadTypeConverters(String moduleName, Set<Class<?>> classes) {
		if (classes == null || classes.isEmpty()) {
			return;
		}

		loadModuleClasses(moduleName, classes);

		for (Class c : classes) {
			if (!ITypeConverter.class.isAssignableFrom(c)) {
				LoggerUtils.getLogger(c).warning("Type converter \"" + c.getName() + "\" from module \"" + moduleName + "\" must implement the \"" + ITypeConverter.class.getName() + "\" interface");
				continue;
			}
			TypeConverter annotation = (TypeConverter) c.getAnnotation(TypeConverter.class);
			for (String s : annotation.value()) {
				typeConverters.put(s, c);
			}
		}

	}

	@SuppressWarnings("rawtypes")
	private void loadServices(String moduleName, Set<Class<?>> classes) {
		if (classes == null || classes.isEmpty()) {
			return;
		}

		loadModuleClasses(moduleName, classes);

		for (Class c : classes) {
			if (!IService.class.isAssignableFrom(c)) {
				LoggerUtils.getLogger(c).warning("Service \"" + c.getName() + "\" from module \"" + moduleName + "\" must implement the \"" + IService.class.getName() + "\" interface");
				continue;
			}
		}

	}

	/**
	 * FOR INTERNAL USE ONLY. Starts services marked with "autoStart" after
	 * containing module is loaded
	 *
	 * @param module the name of the module
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void initializeServices(String module) {
		
		try {
			
			lock.writeLock().lockInterruptibly();
			
			try {

				List<Class> classes = componentsByModule.get(module);

				if (classes == null) {
					return;
				}

				for (Class c : classes) {
					if (!IService.class.isAssignableFrom(c)) {
						continue;
					}

					Service annotation = (Service) c.getAnnotation(Service.class);

					try {

						IService instance = objectFactory.newInstance((Class<? extends IService>) c);

						services.put(annotation.value(), instance);

					} catch (Exception e) {
						LoggerUtils.getLogger(c).log(Level.WARNING, "Error instantiating service \"" + annotation.value() + "\" from module \"" + module + "\"", e);
					}

				}

			} finally {
				lock.writeLock().unlock();
			}

		} catch (InterruptedException e) {
			throw new ModuleConsistencyException("Interrupted while starting autoStart services from module " + module, e);
		}

	}

	/**
	 * FOR INTERNAL USE ONLY. Unloads all the classes provided by the
	 * specified module, before module undeploy.
	 *
	 * @param moduleName the module's name
	 * @throws com.waveinformatica.ocean.core.exceptions.ModuleException if an error occurs
	 */
	@SuppressWarnings("rawtypes")
	public void unloadModuleClasses(String moduleName) throws ModuleException {
		
		try {
			
			lock.writeLock().lockInterruptibly();
			
			try {
				
				Collection<Class> classes = componentsByModule.get(moduleName);

				if (classes == null && !moduleName.equals(Constants.CORE_MODULE_NAME)) {
					return;
				}

				// Unloading event handlers
				Iterator<Map.Entry<String, List<EventListenerInfo>>> entries = eventHandlers.entrySet().iterator();
				while (entries.hasNext()) {
					Map.Entry<String, List<EventListenerInfo>> entry = entries.next();
					Iterator<EventListenerInfo> iter = entry.getValue().iterator();
					while (iter.hasNext()) {
						EventListenerInfo eli = iter.next();
						if (classes != null && classes.contains(eli.getCls())) {
							iter.remove();
						}
					}
					if (entry.getValue().isEmpty()) {
						entries.remove();
					}
				}

				// Unloading operations
				Iterator<Entry<Class<?>, Map<String, List<OperationInfo>>>> scopedOps = operationHandlers.entrySet().iterator();
				while (scopedOps.hasNext()) {
					Entry<Class<?>, Map<String, List<OperationInfo>>> scopedOp = scopedOps.next();
					if (scopedOp.getKey().getClassLoader() instanceof ModuleClassLoader && ((ModuleClassLoader)scopedOp.getKey().getClassLoader()).getModule().getName().equals(moduleName)) {
						scopedOps.remove();
					}
					else {
						Iterator<Map.Entry<String, List<OperationInfo>>> ops = scopedOp.getValue().entrySet().iterator();
						while (ops.hasNext()) {
							Map.Entry<String, List<OperationInfo>> entry = ops.next();
							Iterator<OperationInfo> oiIter = entry.getValue().iterator();
							while (oiIter.hasNext()) {
								OperationInfo oi = oiIter.next();
								if (classes != null && classes.contains(oi.getCls())) {
									oiIter.remove();
								}
							}
							if (entry.getValue().isEmpty()) {
								ops.remove();
							}
						}
						if (scopedOp.getValue().isEmpty()) {
							scopedOps.remove();
						}
					}
				}

				// Unloading decorators
				Iterator<Map.Entry<MimeTypes, Map<Class, Class>>> decs = decorators.entrySet().iterator();
				while (decs.hasNext()) {
					Map.Entry<MimeTypes, Map<Class, Class>> entry = decs.next();
					Iterator<Map.Entry<Class, Class>> values = entry.getValue().entrySet().iterator();
					while (values.hasNext()) {
						Map.Entry<Class, Class> clsEnt = values.next();
						if (classes != null && classes.contains(clsEnt.getValue())) {
							values.remove();
						}
					}
					if (entry.getValue().isEmpty()) {
						decs.remove();
					}
				}

				// Unloading type converters
				Iterator<Map.Entry<String, Class>> convs = typeConverters.entrySet().iterator();
				while (convs.hasNext()) {
					Map.Entry<String, Class> entry = convs.next();
					if (classes != null && classes.contains(entry.getValue())) {
						convs.remove();
					}
				}

				// Unloading services
				Iterator<Map.Entry<String, IService>> srvs = services.entrySet().iterator();
				while (srvs.hasNext()) {
					Map.Entry<String, IService> srv = srvs.next();
					if (classes != null && classes.contains(srv.getValue().getClass())) {
						srv.getValue().stop();
						srvs.remove();
					}
				}

				// Unloading module mbeans
				MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
				Iterator<Map.Entry<ObjectName, Object>> mbeans = this.mbeans.entrySet().iterator();
				while (mbeans.hasNext()) {
					Map.Entry<ObjectName, Object> e = mbeans.next();
					if (e.getValue().getClass().getClassLoader() instanceof ModuleClassLoader) {
						ModuleClassLoader mcl = (ModuleClassLoader) e.getValue().getClass().getClassLoader();
						if (mcl.getModule().getName().equals(moduleName)) {
							try {
								mbs.unregisterMBean(e.getKey());
							} catch (InstanceNotFoundException ex) {
								logger.log(Level.SEVERE, "Unable to unregister MBean " + e.getKey().toString() + " unloading module " + moduleName, ex);
							} catch (MBeanRegistrationException ex) {
								logger.log(Level.SEVERE, "Unable to unregister MBean " + e.getKey().toString() + " unloading module " + moduleName, ex);
							} catch (Exception ex) {
								logger.log(Level.SEVERE, "Unable to unregister MBean " + e.getKey().toString() + " unloading module " + moduleName, ex);
							}
							mbeans.remove();
						}
					}
					else if (moduleName.equals(Constants.CORE_MODULE_NAME) /*&& e.getValue().getClass().getClassLoader() == CoreController.class.getClassLoader()*/) {
						try {
							mbs.unregisterMBean(e.getKey());
						} catch (InstanceNotFoundException ex) {
							logger.log(Level.SEVERE, "Unable to unregister MBean " + e.getKey().toString() + " unloading module " + moduleName, ex);
						} catch (MBeanRegistrationException ex) {
							logger.log(Level.SEVERE, "Unable to unregister MBean " + e.getKey().toString() + " unloading module " + moduleName, ex);
						} catch (Exception ex) {
							logger.log(Level.SEVERE, "Unable to unregister MBean " + e.getKey().toString() + " unloading module " + moduleName, ex);
						}
						mbeans.remove();
					}
				}

				// Unloading class references
				componentsByModule.remove(moduleName);

			} finally {
				lock.writeLock().unlock();
			}

		} catch (InterruptedException e) {
			throw new ModuleException("Interrupted while unloading module components for module " + moduleName, e);
		} catch (Throwable t) {
			throw new ModuleException("Unexpected error occurred while unloading module " + moduleName, t);
		}
	}

	/**
	 * Returns the list of root operations
	 *
	 * @return the list of root operations
	 */
	public List<OperationInfo> getRootOperations(Class<?> scope) {
		
		try {
			if (!lock.readLock().tryLock(30, TimeUnit.SECONDS)) {
				throw new RuntimeException("Unable to acquire read lock in 30 seconds for getting root operations");
			}
		} catch (InterruptedException e1) {
			throw new RuntimeException("Interrupted while acquiring read lock for getting root operation", e1);
		}

		try {
			
			List<OperationInfo> ops = new LinkedList<OperationInfo>();
			
			for (Class<?> scopeCls : operationHandlers.keySet()) {
				if (scopeCls.isAssignableFrom(scope)) {
					Map<String, List<OperationInfo>> handlers = operationHandlers.get(scopeCls);
					if (handlers != null) {
						for (List<OperationInfo> oi : handlers.values()) {
							if (oi.get(0).isRoot()) {
								ops.add(oi.get(0));
							}
						}
					}
				}
			}

			return ops;

		} finally {
			lock.readLock().unlock();
		}

	}
	
	public void runLockedAsync(final Runnable runnable, final boolean write) {
		
		final Runnable finalRunnable;
		
		if (runnable instanceof OceanRunnable) {
			finalRunnable = runnable;
		}
		else {
			finalRunnable = OceanRunnable.create(runnable);
		}
		
		executor.submit(new Runnable() {
			@Override
			public void run() {
				try {
					runLocked(finalRunnable, write);
				} catch (InterruptedException e) {
					LoggerUtils.getLogger(runnable.getClass()).log(Level.SEVERE, "Interrupted while executing locked task " + runnable.getClass().getName());
				}
			}
		});
		
	}
	
	public void runLocked(Runnable runnable, boolean write) throws InterruptedException {
		
		if (write) {
			lock.writeLock().lockInterruptibly();
		}
		else {
			
			try {
				if (!lock.readLock().tryLock(30, TimeUnit.SECONDS)) {
					throw new RuntimeException("Unable to acquire read lock in 30 seconds for running locked task");
				}
			} catch (InterruptedException e1) {
				throw new RuntimeException("Interrupted while acquiring read lock for running locked task", e1);
			}
			
		}
		
		try {
			
			runnable.run();
			
		} finally {
			if (write) {
				lock.writeLock().unlock();
			}
			else {
				lock.readLock().unlock();
			}
		}
		
	}

	protected static class EventListenerInfo {

		@SuppressWarnings("rawtypes")
		private final Class cls;
		private final CoreEventListener config;
		private final EventListener configGeneric;

		@SuppressWarnings("rawtypes")
		public EventListenerInfo(Class cls, CoreEventListener config) {
			this.cls = cls;
			this.config = config;
			this.configGeneric = null;
		}

		@SuppressWarnings("rawtypes")
		public EventListenerInfo(Class cls, EventListener config) {
			this.cls = cls;
			this.config = null;
			this.configGeneric = config;
		}

		@SuppressWarnings("rawtypes")
		public Class getCls() {
			return cls;
		}

		public CoreEventListener getConfig() {
			return config;
		}

		public EventListener getConfigGeneric() {
			return configGeneric;
		}

	}

	public static class OperationInfo {

		@SuppressWarnings("rawtypes")
		private final Class cls;
		private final Method method;
		private final List<ParameterInfo> parameters;
		private final boolean skipAuthorization;
		private final boolean root;
		private final Operation operation;
		private final OperationProvider provider;
		private final Set<Annotation> extraAnnotations = new HashSet<Annotation>();

		@SuppressWarnings({ "rawtypes", "unchecked" })
		public OperationInfo(Class cls, Method method, List<ParameterInfo> parameters, boolean skipAuthorization,
			  boolean root) {
			this.cls = cls;
			this.method = method;
			this.parameters = parameters;
			this.skipAuthorization = skipAuthorization;
			this.root = root;
			this.operation = method.getAnnotation(Operation.class);
			this.provider = (OperationProvider) cls.getAnnotation(OperationProvider.class);

			for (Annotation a : method.getAnnotations()) {
				extraAnnotations.add(a);
			}
		}

		@SuppressWarnings("rawtypes")
		public Class getCls() {
			return cls;
		}

		public Method getMethod() {
			return method;
		}

		public List<ParameterInfo> getParameters() {
			return parameters;
		}

		public boolean isSkipAuthorization() {
			return skipAuthorization;
		}

		public boolean isRoot() {
			return root;
		}

		public Operation getOperation() {
			return operation;
		}

		public OperationProvider getProvider() {
			return provider;
		}

		public String getFullName() {
			return buildOperationName(provider, operation);
		}

		@SuppressWarnings("unchecked")
		public <T extends Annotation> T getExtraAnnotation(Class<T> annotation) {
			for (Annotation a : extraAnnotations) {
				if (a.annotationType() == annotation) {
					return (T) a;
				}
			}
			return null;
		}
	}

	public static class ParameterInfo {

		private final int position;
		private final String name;
		@SuppressWarnings("rawtypes")
		private final Class paramClass;
		private final Type genericType;
		private final Class<? extends ITypeConverter> converter;
		private final Set<Annotation> annotations = new HashSet<Annotation>();
		private final String[] defaultValue;

		@SuppressWarnings("rawtypes")
		public ParameterInfo(int position, String name, Class paramClass, Type genericType,
			  Class<? extends ITypeConverter> converter, Annotation[] annotations,
			  String[] defaultValue) {

			this.position = position;
			this.name = name;
			this.paramClass = paramClass;
			this.genericType = genericType;
			this.converter = converter;
			this.defaultValue = defaultValue;
			for (Annotation a : annotations) {
				this.annotations.add(a);
			}
		}

		public int getPosition() {
			return position;
		}

		public String getName() {
			return name;
		}

		@SuppressWarnings("rawtypes")
		public Class getParamClass() {
			return paramClass;
		}

		public Class<? extends ITypeConverter> getConverter() {
			return converter;
		}

		public Type getGenericType() {
			return genericType;
		}

		public Set<Annotation> getAnnotations() {
			return annotations;
		}

		public String[] getDefaultValue() {
			return defaultValue;
		}

		@SuppressWarnings("unchecked")
		public <T extends Annotation> T getAnnotation(Class<T> cls) {
			for (Annotation a : annotations) {
				if (cls.equals(a.annotationType())) {
					return (T) a;
				}
			}
			return null;
		}

		public <T extends Annotation> boolean hasAnnotation(Class<T> cls) {
			for (Annotation a : annotations) {
				if (cls.equals(a.annotationType())) {
					return true;
				}
			}
			return false;
		}
	}

	public CoreControllerInspector getInspector() {
		return new CoreControllerInspector(this);
	}

	static abstract class ControllerAccessor {

		Map<String, List<EventListenerInfo>> getEventHandlers(CoreController controller) {
			return Collections.unmodifiableMap(controller.eventHandlers);
		}

		Map<Class<?>, Map<String, List<OperationInfo>>> getOperationHandlers(CoreController controller) {
			return Collections.unmodifiableMap(controller.operationHandlers);
		}

		@SuppressWarnings("rawtypes")
		Map<MimeTypes, Map<Class, Class>> getDecorators(CoreController controller) {
			return Collections.unmodifiableMap(controller.decorators);
		}

		@SuppressWarnings("rawtypes")
		Map<String, Class> getTypeConverters(CoreController controller) {
			return Collections.unmodifiableMap(controller.typeConverters);
		}

	}

}
