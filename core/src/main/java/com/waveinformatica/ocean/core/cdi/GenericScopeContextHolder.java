/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.cdi;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;

@SuppressWarnings({"unchecked", "rawtypes"})
public class GenericScopeContextHolder implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Map<Class, GenericScopeInstance> beans;// we will have only one
													// instance of a type so the
													// key is a class

	public GenericScopeContextHolder() {
		beans = Collections.synchronizedMap(new HashMap<Class, GenericScopeInstance>());
	}

	public Map<Class, GenericScopeInstance> getBeans() {
		return beans;
	}

	public GenericScopeInstance getBean(Class type) {
		return getBeans().get(type);
	}

	public void putBean(GenericScopeInstance customInstance) {
		getBeans().put(customInstance.bean.getBeanClass(), customInstance);
	}

	void destroyBean(GenericScopeInstance customScopeInstance) {
		getBeans().remove(customScopeInstance.bean.getBeanClass());
		customScopeInstance.bean.destroy(customScopeInstance.instance,
				customScopeInstance.ctx);
	}

	/**
	 * wrap necessary properties so we can destroy the bean later
	 */
	public static class GenericScopeInstance<T> implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		Bean<T> bean;
		CreationalContext<T> ctx;
		T instance;
	}

}
