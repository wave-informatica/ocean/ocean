/*
* Copyright 2017 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.i18n;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;

/**
 * This class allows to parse Java SimpleDateFormat patterns, useful to build parsers and converters of
 * date formats, to support internationalization and interoperation between components.
 * 
 * @author Ivano
 * @since 2.1
 */
public class DateFormatConverter {
	
	private static final Pattern parsePattern = Pattern.compile("(\\s*'([^']|'')+'\\s*)+|y+|Y+|m+|M+|G+|w+|W+|D+|d+|F+|E+|u+|a+|H+|k+|K+|h+|s+|S+|z+|Z+|X+|.+?");
	
	public static void main(String[] args) {
		DateFormatConverter conv = new DateFormatConverter();
		String fmt = "dd/MM/yyyy HH:mm MMyyyy 'o''clock' MMMMMM";
		List<Token> tokens = conv.tokenize(fmt);
		for (Token t : tokens) {
			System.out.println("[" + t.getType() + "] \"" + t.getText() + "\"");
		}
		System.out.println(new SimpleDateFormat(fmt).format(new Date()));
	}
	
	/**
	 * Tokenizes a format string to a list of tokens.
	 * 
	 * @param format the format string to be parsed
	 * @return the list of format tokens
	 */
	public List<Token> tokenize(String format) {
		
		List<Token> tokens = new ArrayList<Token>();
		
		Matcher matcher = parsePattern.matcher(format);
		
		while (matcher.find()) {
			String text = matcher.group();
			Token current = new Token();
			while (text.length() > 0) {
				boolean found = false;
				for (TokenType tt : TokenType.values()) {
					if (text.startsWith(tt.getText())) {
						current.setType(tt);
						current.setText(text.substring(0, tt.getText().length()));
						text = text.substring(tt.getText().length());
						tokens.add(current);
						current = new Token();
						found = true;
					}
				}
				if (!found) {
					break;
				}
			}
			if (StringUtils.isBlank(current.getText()) && StringUtils.isNotEmpty(text)) {
				current.setText(text);
				current.setType(null);
				tokens.add(current);
			}
		}
		
		return tokens;
		
	}
	
	/**
	 * This class represents a date format token
	 * 
	 * @author Ivano
	 */
	public static class Token {
		
		private String text;
		private TokenType type;
		
		/**
		 * Returns the source text of the token
		 * 
		 * @return the source text of the token
		 */
		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}
		
		/**
		 * Returns the token's type or <code>null</code> for text-only tokens
		 * 
		 * @return the token's type
		 */
		public TokenType getType() {
			return type;
		}

		public void setType(TokenType type) {
			this.type = type;
		}
		
	}
	
	/**
	 * The token type
	 */
	public static enum TokenType {
		
		ERA("G"),
		YEAR_4_DIGITS("yyyy"),
		YEAR_2_DIGITS("yy"),
		MONTH_NAME_FULL("MMMM"),
		MONTH_NAME_3("MMM"),
		MONTH_2_DIGITS("MM"),
		MONTH_1_DIGIT("M"),
		DAY_IN_YEAR("D"),
		DAY_IN_MONTH_2("dd"),
		DAY_IN_MONTH_1("d"),
		DAY_WEEK_NAME_FULL("EEEE"),
		DAY_WEEK_NAME_3("EEE"),
		AM_PM_MARKER("a"),
		HOUR_IN_DAY_23_2("HH"),
		HOUR_IN_DAY_23_1("H"),
		HOUR_IN_DAY_24_2("kk"),
		HOUR_IN_DAY_24_1("k"),
		HOUR_HALF_11_2("KK"),
		HOUR_HALF_11_1("K"),
		HOUR_HALF_12_2("hh"),
		HOUR_HALF_12_1("h"),
		MINUTE_2("mm"),
		MINUTE_1("m"),
		SECOND_2("ss"),
		SECOND_1("s"),
		MILLISECOND_3("SSS"),
		MILLISECOND_1("S"),
		TIMEZONE_GENERAL("z"),
		TIMEZONE_RFC822("Z"),
		TIMEZONE_ISO8601("X")
		
		;
		
		private final String text;

		private TokenType(String text) {
			this.text = text;
		}
		
		/**
		 * Returns the text for the format
		 * 
		 * @return the text for the format
		 */
		public String getText() {
			return text;
		}
		
	}
	
}
