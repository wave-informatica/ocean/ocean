package com.waveinformatica.ocean.core.filtering.drivers;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ValuesManager implements Iterable<DataValue> {
	
	private static int counter = 0;
	private Map<Object, DataValue> paramsMap = new HashMap<>();
	
	public DataValue getParam(DataValue p) {
		DataValue mp = paramsMap.get(p.getValue());
		if (mp == null) {
			mp = p;
			counter = (counter + 1) % Integer.MAX_VALUE;
			p.setName("param" + (counter));
			paramsMap.put(p.getValue(), p);
		}
		return mp;
	}
	
	public DataValue getByName(String name) {
		for (DataValue dv : this) {
			if (dv.getName().equals(name)) {
				return dv;
			}
		}
		return null;
	}
	
	@Override
	public Iterator<DataValue> iterator() {
		return Collections.unmodifiableCollection(paramsMap.values()).iterator();
	}
	
}
