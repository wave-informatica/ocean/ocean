package com.waveinformatica.ocean.core.filtering.drivers;

public class DataPath implements DataReference {
	
	private final String path;
	
	public DataPath(String path) {
		super();
		this.path = path;
	}
	
	public String getPath() {
		return path;
	}
	
}
