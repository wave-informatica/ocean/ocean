/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.ObjectProperty;
import com.waveinformatica.ocean.core.converters.EnumConverter;
import com.waveinformatica.ocean.core.converters.ITypeConverter;
import com.waveinformatica.ocean.core.modules.ClassInfoCache;
import com.waveinformatica.ocean.core.modules.ClassInfoCache.Factory;

/**
 *
 * @author Ivano
 */
@Named
@ApplicationScoped
public class ObjectUpdater {

	@Inject
	private CoreController coreController;
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private ClassInfoCache cache;
	
	/**
	 * This method can be used to retrieve cached object properties. The cache used is {@link ClassInfoCache}.
	 * 
	 * @param objectClass the class to get properties from
	 * @return the map of found properties (never null)
	 */
	@SuppressWarnings("unchecked")
	public Map<String, ObjectProperty> getObjectProperties(final Class<?> objectClass) {
		
		if (objectClass == null) {
			throw new NullPointerException("The class must not be null");
		}
		else if (objectClass.isInterface()) {
			return Collections.EMPTY_MAP;
		}
		
		return (Map<String, ObjectProperty>) cache.get(objectClass, ObjectUpdater.class.getName() + "#properties", new Factory() {
			@Override
			public Object create() {
				
				if (objectClass == Object.class) {
					return new HashMap<String, ObjectProperty>();
				}
				
				Map<String, ObjectProperty> properties = new LinkedHashMap<>();
				
				properties.putAll(getObjectProperties(objectClass.getSuperclass()));
				
				Field[] fields = objectClass.getDeclaredFields();
				for (Field f : fields) {
					ObjectProperty prop = new ObjectProperty(f);
					if (prop.getGetter() != null) {
						properties.put(f.getName(), prop);
					}
				}
				
				Method[] methods = objectClass.getMethods();
				for (Method m : methods) {
					if (m.getDeclaringClass() == Object.class) {
						continue;
					}
					char n = '\0';
					int prefLen = 0;
					if (m.getName().startsWith("is")) {
						n = m.getName().charAt(2);
						prefLen = 2;
					}
					else if (m.getName().startsWith("get")) {
						n = m.getName().charAt(3);
						prefLen = 3;
					}
					if (n >= 'A' && n <= 'Z' && prefLen > 0 && m.getName().length() > prefLen) {
						String propName = m.getName().substring(prefLen, prefLen + 1).toLowerCase() + m.getName().substring(prefLen + 1);
						if (!properties.containsKey(propName)) {
							ObjectProperty prop = new ObjectProperty(objectClass, propName, m);
							properties.put(propName, prop);
						}
					}
				}
				
				return properties;
			}
		});
		
	}
	
	/**
	 * This method allows to read the specified property from an object
	 * 
	 * @param o the object to read the property value from
	 * @param name the name of the property to read
	 * @return the property's value or <code>null</code> if no property is found or the property is null
	 */
	public Object read(Object o, String name) {
		
		if (o == null) {
			throw new NullPointerException("Object must not be null");
		}
		
		Map<String, ObjectProperty> props = getObjectProperties(o.getClass());
		
		if (!props.containsKey(name)) {
			return null;
		}
		
		return props.get(name).get(o);
		
	}

	/**
	 * This method allows to update an object's property with the specified value
	 * 
	 * @param o the object to update
	 * @param name the name of the property
	 * @param value the value to be set
	 * @return {@code true} if the field has actually been updated
	 */
	public boolean update(Object o, String name, String value) {
		
		if (o == null) {
			throw new NullPointerException("Object must not be null");
		}
		
		Map<String, ObjectProperty> props = getObjectProperties(o.getClass());
		
		ObjectProperty prop = props.get(name);
		if (prop == null || prop.getSetter() == null) {
			return false;
		}
		
		Object oldValue = prop.get(o);
		Object newValue = value;
		boolean error = false;

		Class<?>[] types = prop.getSetter().getParameterTypes();
		if (types.length != 1) {
			throw new IllegalArgumentException("Invalid setter type \"" + prop.getSetter().getName() + "\" in \"" + prop.getSetter().getDeclaringClass().getName() + "\" for property \"" + name + "\"");
		}

		if (types[0] == String.class) {
			
			prop.set(o, value);
			
		} else {
			
			Class convCls = coreController.findConverter(types[0].getName());
			if (convCls == null && types[0].isEnum()) {
				convCls = EnumConverter.class;
			}
			if (convCls == null) {
				throw new IllegalArgumentException("Unable to find a suitable type converter from java.lang.String to " + types[0].getName() + " for property \"" + name + "\"");
			}

			ITypeConverter converter = (ITypeConverter) factory.newInstance(convCls);
			try {
				newValue = converter.fromScalar(value, types[0], null, new Object[]{oldValue});
				prop.set(o, newValue);
			} catch (Exception e) {
				LoggerUtils.getLogger(o.getClass()).log(Level.WARNING, "Unable to set value for property " + prop.getField().getDeclaringClass().getName() + "." + prop.getField().getName(), e);
				error = true;
			}
			
		}

		return !error && !((newValue == null && oldValue == null) || (newValue != null && newValue.equals(oldValue)));

	}

}
