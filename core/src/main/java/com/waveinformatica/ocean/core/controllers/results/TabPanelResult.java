/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.results;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.annotations.ExcludeSerialization;
import com.waveinformatica.ocean.core.util.OceanSession;
import com.waveinformatica.ocean.core.util.Results;

/**
 * @author Ivano Culmine
 * @author Marco Merli
 * @since 2.0
 */
public class TabPanelResult extends BaseResult {

	@Inject
	@ExcludeSerialization
	private OceanSession session;

	private final List<Tab> panel;
	private final List<String> shared;
	private String namespace;
	private String pathBase;
	private String template;

	public TabPanelResult() {

		panel = new ArrayList<TabPanelResult.Tab>();
		shared = new ArrayList<String>();
	}

	public void add(String label, String operation) {
		add(new Tab(label, operation));
	}

	public void add(Tab tab) {
		panel.add(tab);
	}
	
	public void addShared(String sharedOperation) {
		shared.add(sharedOperation);
	}

	public List<Tab> getPanel() {
		return panel;
	}
	
	public List<String> getShared() {
		return shared;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setActive(String operation) {
		if (namespace != null) {
			session.put(sessionKey(), operation);
		}
	}

	public String getActive() {
		String active = session.get(sessionKey(), String.class);
		if (active == null) {
			active = panel.get(0).getOperation();
		}

		return active;
	}

	public String getPathBase() {
		return pathBase;
	}

	public void setPathBase(String pathBase) {
		this.pathBase = pathBase;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public static class Tab {

		private final String label;
		private final String operation;

		public Tab(String label, String operation) {

			this.label = label;
			this.operation = operation;
		}

		public String getLabel() {
			return label;
		}

		public String getOperation() {
			return operation;
		}

		public String getKey() {
			return operation.replaceAll("[^0-9a-zA-Z]+", "-");
		}
	}

	private String sessionKey() {
		return String.format("tabpanel.%s.active", namespace);
	}
	
}
