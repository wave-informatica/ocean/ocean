/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */
package com.waveinformatica.ocean.core.converters;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.TypeConverter;

/**
 *
 * @author Ivano
 */
@TypeConverter({"java.util.Date"})
public class DateConverter implements ITypeConverter {

	private static DateFormat[] parsers = new DateFormat[]{
		new DateFormat(true, true, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", TimeZone.getTimeZone("GMT")),
		new DateFormat(true, true, "dd/MM/yyyy hh:mm"),
		new DateFormat(true, true, "dd-MM-yyyy hh:mm"),
		new DateFormat(true, true, "dd-MMM-yyyy hh:mm"),
		new DateFormat(true, true, "dd.MM.yyyy hh:mm"),
		new DateFormat(true, true, "dd MMM yyyy hh:mm"),
		new DateFormat(true, true, "dd/MM/yyyy hh:mm:ss"),
		new DateFormat(true, true, "dd-MM-yyyy hh:mm:ss"),
		new DateFormat(true, true, "dd-MMM-yyyy hh:mm:ss"),
		new DateFormat(true, true, "dd.MM.yyyy hh:mm:ss"),
		new DateFormat(true, true, "dd MMM yyyy hh:mm:ss"),
		new DateFormat(true, false, "dd/MM/yyyy"),
		new DateFormat(true, false, "dd-MM-yyyy"),
		new DateFormat(true, false, "dd-MMM-yyyy"),
		new DateFormat(true, false, "dd.MM.yyyy"),
		new DateFormat(true, false, "dd MMM yyyy"),
		new DateFormat(false, true, "hh:mm"),
		new DateFormat(false, true, "hh:mm:ss")
	};

	@Override
	public Object fromParametersMap(String destName, Class destClass, Type genericType, Map<String, String[]> source, Object[] ctxObjects) {

		String format = null;

		if (source.containsKey(destName + "$f")) {
			String[] fmt = source.get(destName + "$f");
			if (fmt != null && fmt.length > 0) {
				format = fmt[0];
			}
		}

		String[] values = source.get(destName);

		if (values != null && values.length == 1) {
			if (StringUtils.isBlank(values[0])) {
				return null;
			} else if (format != null) {
				try {
					return new SimpleDateFormat(format).parse(values[0]);
				} catch (ParseException ex) {
					// Invalid date...
				}
			} else {
				return fromScalar(values[0], destClass, source, ctxObjects);
			}
		}

		return null;
	}

	@Override
	public Object fromScalar(String value, Class destClass, Map<String, String[]> source, Object[] ctxObjects) {

		if (StringUtils.isBlank(value)) {
			return null;
		}

		Calendar result = null;

		Calendar originalDate = null;
		for (Object o : ctxObjects) {
			if (o == null) {
				continue;
			}
			if (o instanceof Date) {
				originalDate = Calendar.getInstance();
				originalDate.setTime((Date) o);
				break;
			} else if (o instanceof Calendar) {
				originalDate = (Calendar) o;
				break;
			}
		}

		for (DateFormat fmt : parsers) {
			try {
				result = fmt.parse(value);
				if (originalDate != null && fmt.isDate() && !fmt.isTime()) {
					result.set(Calendar.HOUR_OF_DAY, originalDate.get(Calendar.HOUR_OF_DAY));
					result.set(Calendar.MINUTE, originalDate.get(Calendar.MINUTE));
					result.set(Calendar.SECOND, originalDate.get(Calendar.SECOND));
					result.set(Calendar.MILLISECOND, originalDate.get(Calendar.MILLISECOND));
				} else if (originalDate != null && !fmt.isDate() && fmt.isTime()) {
					result.set(Calendar.DAY_OF_MONTH, originalDate.get(Calendar.DAY_OF_MONTH));
					result.set(Calendar.MONTH, originalDate.get(Calendar.MONTH));
					result.set(Calendar.YEAR, originalDate.get(Calendar.YEAR));
				}
				return result.getTime();
			} catch (ParseException ex) {
				// Invalid date...
			}
		}
		
		// Avoid too many exceptions...
		// throw new IllegalArgumentException("Error parsing date \"" + value + "\"");
		// ... just return null
		return null;
		
	}

	private static class DateFormat {

		private final boolean date;
		private final boolean time;
		private final String format;
		private final SimpleDateFormat simpleDateFormat;

		public DateFormat(boolean date, boolean time, String format) {
			this.date = date;
			this.time = time;
			this.format = format;
			simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
		}

		public DateFormat(boolean date, boolean time, String format, TimeZone timeZone) {
			this.date = date;
			this.time = time;
			this.format = format;
			simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
			simpleDateFormat.setTimeZone(timeZone);
		}

		public boolean isDate() {
			return date;
		}

		public boolean isTime() {
			return time;
		}

		public String getFormat() {
			return format;
		}

		public Calendar parse(String source) throws ParseException {
			Date res = simpleDateFormat.parse(source);
			Calendar cal = Calendar.getInstance();
			cal.setTime(res);
			return cal;
		}

	}

}
