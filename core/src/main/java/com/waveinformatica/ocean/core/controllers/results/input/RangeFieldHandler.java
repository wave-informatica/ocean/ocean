/*
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.controllers.results.input;

import java.lang.reflect.TypeVariable;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.controllers.results.InputResult.FormField;
import com.waveinformatica.ocean.core.util.OceanSession;
import com.waveinformatica.ocean.core.util.RequestCache;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;

/**
 * @author Marco Merli
 * @author Ivano Culmine
 */
public class RangeFieldHandler implements InputResult.FieldHandler {
	
	private InputResult.FieldHandler handlerType;
	private FieldWrapper fromWrapper;
	private FieldWrapper toWrapper;
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private OceanSession session;
	
	@Inject
	private RequestCache requestCache;
	
	@Override
	public String getFieldTemplate()
	{
		return "/templates/core/components/fieldRange.tpl";
	}

	@Override
	public void init(InputResult result, CoreController.OperationInfo opInfo,
		CoreController.ParameterInfo paramInfo, InputResult.FormField field)
	{
		Class type = (Class) ((ParameterizedType) paramInfo.getGenericType()).getActualTypeArguments()[0];
		
		if (type.isEnum()) {
			handlerType = factory.newInstance(EnumFieldHandler.class);
		} else if (DateFieldHandler.canHandle(type)) {
			handlerType = factory.newInstance(DateFieldHandler.class);
		} else {
			handlerType = factory.newInstance(TextFieldHandler.class);
		}
		
		handlerType.init(result, opInfo, paramInfo, field);
		
		fromWrapper = new FieldWrapper(field, "from", handlerType);
		toWrapper = new FieldWrapper(field, "to", handlerType);
		
		String cacheName = InputResult.DataPersistenceManager.getName(null, opInfo.getFullName());
		Map<String, String[]> persistedData = (Map<String, String[]>) requestCache.get(cacheName, null);
		if (persistedData == null) {
			persistedData = (Map<String, String[]>) session.get(cacheName);
		}
		
		if (persistedData != null) {
			for (FieldWrapper f : new FieldWrapper[]{ fromWrapper, toWrapper }) {
				String[] data = persistedData.get(f.getName());
				if (data != null && (Boolean.class.isAssignableFrom(type) || data.length == 1)) {
					f.setValue(data[0]);
				}
				// FIXME: da implementare?
				/*else if (data != null && data.length > 1) {
					for (String s : data) {
						f.addValue(s);
					}
				}*/
			}
		}
		
	}

	@Override
	public String formatValue(InputResult result, FormField field, Object value)
	{
		Range range = (Range) value;
		TypeVariable type = field.getType().getTypeParameters()[0];

		// FIXME: manage the conversion
		if (String.class.isAssignableFrom(type.getClass()))
			return String.format("%s-%s",
				range.getFrom(), range.getTo());

		return null;
	}

	public static boolean canHandle(Class<?> type)
	{
		return type == Range.class;
	}

	public FieldWrapper getFromWrapper() {
		return fromWrapper;
	}

	public FieldWrapper getToWrapper() {
		return toWrapper;
	}
	
	public static class FieldWrapper {
		
		private final FormField original;
		private final String suffix;
		private final InputResult.FieldHandler handler;
		private String value;
		
		public FieldWrapper(FormField original, String suffix, InputResult.FieldHandler handler) {
			this.original = original;
			this.suffix = suffix;
			this.handler = handler;
		}

		public FormField getOriginal() {
			return original;
		}

		public String getSuffix() {
			return suffix;
		}

		public InputResult.FieldHandler getHandler() {
			return handler;
		}
		
		public String getName() {
			return original.getName() + "." + suffix;
		}
		
		public boolean isRequired() {
			return original.isRequired();
		}
		
		public boolean isReadOnly() {
			return original.isRequired();
		}
		
		public String getLabel() {
			return "";
		}
		
		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
		
		public List<String> getValues() {
			return new ArrayList<String>();
		}
	}
}
