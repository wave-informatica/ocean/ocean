/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */
package com.waveinformatica.ocean.core.modules;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.scannotation.ClasspathUrlFinder;
import org.scannotation.WarUrlFinder;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.Constants;
import com.waveinformatica.ocean.core.FrameworkConfiguration;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.events.ModuleEvent;
import com.waveinformatica.ocean.core.exceptions.ModuleException;
import com.waveinformatica.ocean.core.internal.SessionModuleCleaner;
import com.waveinformatica.ocean.core.persistence.EntitiesStore;
import com.waveinformatica.ocean.core.urlhandlers.OceanUrlFactory;
import com.waveinformatica.ocean.core.util.FsUtils;
import com.waveinformatica.ocean.core.util.GlobalClassLoader;
import com.waveinformatica.ocean.core.util.I18N;
import com.waveinformatica.ocean.core.util.LoggerUtils;

/**
 *
 * @author Ivano
 */
@Named
@ApplicationScoped
public class ModulesManager {

	private static Logger logger;

	@Inject
	private Configuration configuration;

	@Inject
	private CoreController coreController;
	
	@Inject
	private ComponentsManager componentsManager;

	@Inject
	private GlobalClassLoader globalClassLoader;

	@Inject
	private FsUtils fsUtils;

	@Inject
	private ObjectFactory objectFactory;

	@Inject
	private I18N i18n;
	
	@Inject
	private EntitiesStore entitiesStore;

	private final Map<String, Module> modules = new HashMap<String, Module>();
	private OceanUrlFactory urlFactory;

	public ModulesManager() {
	}

	@PostConstruct
	public void init() {
		logger = LoggerUtils.getCoreLogger();
	}

	public OceanUrlFactory getUrlFactory() {
		return urlFactory;
	}

	public void setUrlFactory(OceanUrlFactory urlFactory) {
		this.urlFactory = urlFactory;
	}

	public synchronized void loadModules() {

		logger.info("Loading modules...");

		List<String> moduleNames = new ArrayList<String>();

		File modulesDir = new File(configuration.getConfigDir(), "modules");
		if (!modulesDir.exists()) {
			modulesDir.mkdirs();
		}
		File[] files = modulesDir.listFiles();
		if (files != null) {
			for (File f : files) {
				if (f.getName().endsWith(".jar")) {
					String modName = f.getName().substring(0, f.getName().indexOf(".jar"));
					moduleNames.add(modName);
				}
			}
		}
		
		Map<String, Set<String>> missingDependencies = new HashMap<>();
		
		int loadedModules = 0;
		boolean updates = false;
		do {
			updates = false;
			for (String name : moduleNames) {
				Module m = modules.get(name);
				if (m != null && m.isLoaded()) {
					continue;
				}
				try {
					m = new Module(this, new File(modulesDir, name + ".jar"));
				} catch (IOException e) {
					LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unable to instantiate module " + name, e);
					continue;
				}
				Module oldModule = this.modules.put(name, m);
				if (oldModule != null) {
					oldModule.unload(false);
				}
				Set<String> deps = new TreeSet<>();
				m.checkDependencies(this, deps);
				missingDependencies.put(m.getName(), deps);
				if (!deps.isEmpty()) {
					continue;
				}
				try {
					m.load();
					updates = true;
					loadedModules++;
					missingDependencies.remove(m.getName());
				} catch (ModuleException e) {
					m.getLogger().log(Level.SEVERE, null, e);
					try {
						unloadModule(m.getName(), false);
					} catch (ModuleException e1) {
						m.getLogger().log(Level.SEVERE, null, e1);
					}
				}
			}
		} while (updates);
		
		for (String n : missingDependencies.keySet()) {
			Set<String> deps = missingDependencies.get(n);
			if (deps == null || deps.isEmpty()) {
				continue;
			}
			for (String d : deps) {
				this.modules.get(n).getLogger().log(Level.SEVERE, "Missing dependency " + d);
			}
		}

		logger.info(loadedModules + " of " + modules.size() + " modules successfully loaded.");

	}

	public synchronized void reloadModule(String name) throws ModuleException, IOException {
		unloadModule(name, false);
		modules.remove(name);
		loadModule(name);
	}

	public synchronized void loadModule(String name) throws ModuleException, IOException {
		loadModule(new File(configuration.getConfigDir(), "modules/" + name + ".jar"));
	}

	public synchronized void loadModule(File jarFile) throws ModuleException, IOException {

		if (!jarFile.exists()) {
			logger.warning("Trying to load not existing module \"" + jarFile.getAbsolutePath() + "\". Ignoring operation.");
			return;
		}

		// Checking if the module is already present
		String modName = jarFile.getName().substring(0, jarFile.getName().indexOf(".jar"));
		Module module = modules.get(modName);
		if (module != null) {
			module.getLogger().info("Unloading older module \"" + modName + "\"...");
			unloadModule(modName, false);
		}

		File modulesDir = new File(configuration.getConfigDir(), "modules");
		if (!jarFile.getParentFile().equals(modulesDir)) {
			File tmpFile = jarFile;
			jarFile = fsUtils.copyFile(tmpFile, modulesDir, jarFile.getName());
			tmpFile.delete();
		}

		module = new Module(this, jarFile);
		modules.put(module.getName(), module);
		try {
			module.load();
		} catch (ModuleException e) {
			module.getLogger().log(Level.SEVERE, "Error loading module: " + e.getMessage(), e);
			unloadModule(modName, false);
		}
		
		if (module.isLoaded()) {
			loadModules();
		}
	}

	public synchronized Set<String> unloadModule(String name, boolean delete) throws ModuleException {

		Set<String> dependentModules = new TreeSet<String>();

		Module module = modules.get(name);

		try {
			ModuleEvent modEvent = new ModuleEvent(this, false, module);
			modEvent.setAnnotatedClasses(module.getMarkedComponents());
			coreController.dispatchEvent(modEvent);
		} catch (Exception e) {
			logger.log(Level.WARNING, "Error dispatching MODULE_UNLOADED event for module \"" + module.getName() + "\"", e);
		}

		// Unloading dependent modules
		module.getLogger().info("Unloading dependent modules for \"" + name + "\"...");
		List<Module> depModules = new ArrayList<Module>(modules.values());
		for (Module m : depModules) {
			if (m != null && m.isLoaded() && m.getDependencies() != null && m.getDependencies().contains(name)) {
				dependentModules.add(m.getName());
				Set<String> depDep = unloadModule(m.getName(), false); // Never delete dependent modules
				dependentModules.addAll(depDep);
			}
		}

		module.getLogger().info("Unloading module \"" + name + "\"...");

		try {
			SessionModuleCleaner sessionCleaner = objectFactory.newInstance(SessionModuleCleaner.class);
			sessionCleaner.unloadSessionObjects(module);
		} catch (Throwable t) {
			LoggerUtils.getCoreLogger().log(Level.WARNING, "Unable to unload session objects: " + t.getMessage());
		}

		i18n.unloadModule(modules.get(name));
		
		componentsManager.unloadComponents(module);

		coreController.unloadModuleClasses(name);
		
		globalClassLoader.removeClassLoader(module.getModuleClassLoader());

		module.unload(delete);
		
		entitiesStore.unloadModule(name);

		objectFactory.moduleUnloaded(name);

		if (delete) {
			modules.remove(name);
		}

		module.getLogger().info("Unloaded module \"" + name + "\".");

		return dependentModules;

	}

	public synchronized void unloadModules() {

		logger.info("Unloading all loaded modules...");

		List<String> keys = new ArrayList<String>(modules.keySet());

		for (String entry : keys) {
			try {
				unloadModule(entry, false);
			} catch (ModuleException e) {
				LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unexpected error unloading module " + entry, e);
			}
		}

	}

	public List<Module> getModules() {
		List<Module> modules = new LinkedList<Module>();

		for (Map.Entry<String, Module> m : this.modules.entrySet()) {
			modules.add(m.getValue());
		}

		return modules;
	}

	/**
	 * Retrieves the module descriptor with the given name
	 *
	 * @param name
	 * @return
	 */
	public Module getModule(String name) {
		return modules.get(name);
	}

	/**
	 * Loads core components
	 */
	public void loadCore(FrameworkConfiguration fwConfig) {

		logger.info("Loading core components...");

		Set<URL> urls = new LinkedHashSet<URL>();

		// Ocean Core JAR
		urls.add(ClasspathUrlFinder.findClassBase(this.getClass()));
		// Web App classes
		try {
			urls.add(ClasspathUrlFinder.findResourceBase(""));
		} catch (Exception e) {
			LoggerUtils.getCoreLogger().log(Level.WARNING, "Unable to find url for WebApp classes");
		}
		if (Application.getInstance().getContext() != null) {
			urls.add(WarUrlFinder.findWebInfClassesPath(Application.getInstance().getContext()));
		}
		
		for (Class<?> cl : fwConfig.getAppClassBases()) {
			try {
				String uStr = ClasspathUrlFinder.findResourceBase(cl.getName().replace(".", "/") + ".class", cl.getClassLoader()).toExternalForm();
				int pos = uStr.indexOf("!/");
				if (pos >= 0) {
					uStr = uStr.substring(0, pos + 2);
				}
				urls.add(new URL(uStr));
			} catch (Exception e) {
				LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unable to use configured classloader for loading components", e);
			}
		}

		try {

			for (URL url : urls) {
				
				if (url == null) {
					continue;
				}

				LoggerUtils.getCoreLogger().log(Level.INFO, "Loading core components from " + url.toString());

				OceanAnnotationDB annotationDb = new OceanAnnotationDB();

				annotationDb.scanArchives(url);

				try {
					
					ModuleTransaction transaction = objectFactory.newInstance(ModuleTransaction.class);
					transaction.loadComponentsCatalog(Constants.CORE_MODULE_NAME, this.getClass().getClassLoader(), annotationDb.getAnnotationIndex());
					transaction.checkPersistenceAvailability(annotationDb.getAnnotationIndex());
					transaction.loadComponents(this.getClass().getClassLoader(), annotationDb.getAnnotationIndex());
					transaction.loadPersistenceUnit(annotationDb.getAnnotationIndex());
					transaction.commit();
					
				} catch (ModuleException e) {
					LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unable to load components from " + url.toExternalForm(), e);
				}

			}

		} catch (IOException ex) {
			Logger.getLogger(ModulesManager.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

}
