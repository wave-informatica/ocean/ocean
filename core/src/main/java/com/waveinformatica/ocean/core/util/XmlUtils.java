/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.util;

import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlSchema;

import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.ModuleEvent;
import com.waveinformatica.ocean.core.modules.Module;
import com.waveinformatica.ocean.core.modules.ModuleClassLoader;
import com.waveinformatica.ocean.core.modules.ModulesManager;
import com.waveinformatica.ocean.schemas.soapenv.Envelope;
import com.waveinformatica.ocean.schemas.soapenv.Fault;

@ApplicationScoped
public class XmlUtils {
	
	@SuppressWarnings("rawtypes")
	private Set<Class> entities = new HashSet<Class>();
	
	private WeakReference<JAXBContext> context;
	
	public synchronized JAXBContext getContext() throws JAXBException {
		
		if (context == null || context.get() == null) {
			context = new WeakReference<JAXBContext>(create());
		}
		
		return context.get();
		
	}
	
	@SuppressWarnings("rawtypes")
	public synchronized void moduleLoaded(Module m) {
		
		if (m == null) {
			// Manually adding classes from core
			entities.add(Envelope.class);
			entities.add(Fault.class);
		}
		else {
			for (String s : m.getModuleClassLoader().getAnnotatedClassesMap().keySet()) {
				if (s.startsWith(XmlSchema.class.getPackage().getName() + ".")) {
					for (String cName : m.getModuleClassLoader().getAnnotatedClassesMap().get(s)) {
						try {
							Class c = m.getModuleClassLoader().loadClass(cName);
							if (!c.isAnnotation() && !c.isEnum() && !c.isInterface()) {
								entities.add(c);
							}
						} catch (Throwable e) {
							LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unable to load XML class " + cName + " from module " + m.getName(), e);
						}
					}
				}
			}
		}
		
		context = null;
		
	}
	
	@SuppressWarnings("rawtypes")
	public synchronized void moduleUnloaded(String m) {
		Iterator<Class> iter = entities.iterator();
		while (iter.hasNext()) {
			Class c = iter.next();
			if (c.getClassLoader() instanceof ModuleClassLoader && ((ModuleClassLoader) c.getClassLoader()).getModule().getName().equals(m)) {
				iter.remove();
			}
		}
		
		context = null;
	}
	
	@CoreEventListener(eventNames = { CoreEventName.MODULE_LOADED, CoreEventName.MODULE_UNLOADED })
	public static class ModuleLoaded implements IEventListener<ModuleEvent> {

		@Inject
		private XmlUtils xmlUtils;
		
		@Inject
		private ModulesManager mm;
		
		@Override
		public void performAction(ModuleEvent event) {
			if (event.isLoaded()) {
				xmlUtils.moduleLoaded(mm.getModule(event.getModuleName()));
			}
			else {
				xmlUtils.moduleUnloaded(event.getModuleName());
			}
		}
		
	}
	
	private JAXBContext create() throws JAXBException {
		return JAXBContext.newInstance(entities.toArray(new Class[entities.size()]));
	}
	
}
