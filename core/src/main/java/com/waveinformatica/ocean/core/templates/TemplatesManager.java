/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */
package com.waveinformatica.ocean.core.templates;

import com.waveinformatica.ocean.core.annotations.FreemarkerDirective;
import com.waveinformatica.ocean.core.annotations.Preferred;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.modules.ComponentsBin;
import com.waveinformatica.ocean.core.security.UserSession;
import com.waveinformatica.ocean.core.templates.directives.AuthorizeDirective;
import com.waveinformatica.ocean.core.templates.directives.HtmlHeaderDirective;
import com.waveinformatica.ocean.core.templates.directives.I18nTextDirective;
import com.waveinformatica.ocean.core.templates.directives.IncludeOperationDirective;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.LoggerUtils;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModelException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author Ivano
 */
@Named
@ApplicationScoped
public class TemplatesManager implements ComponentsBin.ComponentLoaded, ComponentsBin.ComponentUnloaded {

	private static final String DEFAULT_ENCODING = "UTF-8";

	private TemplatesConfiguration config;

	@Inject
	private TemplateLoader loader;

	@Inject
	private ObjectFactory factory;

	@Inject
	private com.waveinformatica.ocean.core.Configuration oceanConfig;

	@Inject
	@Preferred
	@FreemarkerDirective("")
	private ComponentsBin directives;

	private final Map<String, Set<String>> themesMap = new HashMap<String, Set<String>>();

	@PostConstruct
	public void init() {

		config = factory.newInstance(TemplatesConfiguration.class);

		config.setDefaultEncoding(DEFAULT_ENCODING);
		config.setURLEscapingCharset(DEFAULT_ENCODING);
		config.setLocalizedLookup(false);

		config.setObjectWrapper(new OceanObjectWrapper(config.getIncompatibleImprovements()));

		config.addAutoImport("input", "/macros/inputField.tpl");
		config.addAutoImport("results", "/macros/results.tpl");

		directives.register(this);

		config.setTemplateLoader(loader);

		// Ocean defined directives
		try {
			for (Class<?> c : directives.getComponents(FreemarkerDirective.class)) {
				componentLoaded(FreemarkerDirective.class, c);
			}
		} catch (Throwable e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, "Error configuring template macros", e);
		}

	}

	public Template getTemplate(MimeTypes expectedType, String name) {
		try {

			if (expectedType == null) {
				expectedType = MimeTypes.HTML;
			}

			Context.setExpectedType(expectedType); // For selecting best available template

			UserSession session = factory.getBean(UserSession.class);

			Template tpl = config.getTemplate(name, session.getLocale());
			if (tpl != null) {
				try {
					tpl.setSetting("locale", getFreemarkerLocale(session.getLocale()));
				} catch (TemplateException ex) {
					LoggerUtils.getLogger(TemplatesManager.class).log(Level.WARNING, "Unable to set locale for template: " + getFreemarkerLocale(session.getLocale()), ex);
				}
			}
			return tpl;

		} catch (IOException ex) {
			Logger.getLogger(TemplatesManager.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}
	}

	public Template getTemplate(String name) {
		return getTemplate(Context.getExpectedType(), name);
	}

	public Configuration getConfig() {
		return config;
	}

	@Override
	public void componentLoaded(Class<? extends Annotation> annotation, Class<?> componentClass) {
		if (annotation == FreemarkerDirective.class) {
			FreemarkerDirective directive = componentClass.getAnnotation(FreemarkerDirective.class);
			try {
				config.setSharedVariable(directive.value(), factory.newInstance(componentClass));
			} catch (TemplateModelException ex) {
				LoggerUtils.getCoreLogger().log(Level.SEVERE, "Error configuring template macro \"" + directive.value() + "\"", ex);
			}
		}
	}

	@Override
	public void componentUnloaded(Class<? extends Annotation> annotation, Class<?> componentClass) {
		if (annotation == FreemarkerDirective.class) {
			FreemarkerDirective directive = componentClass.getAnnotation(FreemarkerDirective.class);
			config.setSharedVariable(directive.value(), null);
		}
	}

	protected void loadThemes(String moduleName, Set<String> themes) {
		Set<String> lThemes = themesMap.get(moduleName);
		if (lThemes == null) {
			lThemes = new TreeSet<String>(themes);
			themesMap.put(moduleName, lThemes);
		} else {
			lThemes.addAll(themes);
		}
	}

	protected void unloadThemes(String moduleName) {
		themesMap.remove(moduleName);
	}

	public Set<String> getThemes() {
		Set<String> set = new TreeSet<String>();
		for (String k : themesMap.keySet()) {
			set.addAll(themesMap.get(k));
		}
		return set;
	}

	private String getFreemarkerLocale(Locale locale) {
		StringBuilder builder = new StringBuilder();
		builder.append(locale.getLanguage());
		if (locale.getCountry().length() > 0) {
			builder.append('_');
			builder.append(locale.getCountry());
		}
		return builder.toString();
	}
}
