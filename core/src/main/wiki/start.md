[TOC]

# Quick start

## Premessa

Ocean Framework è un framework per lo sviluppo di applicazioni web modulari seguendo il paradigma MVC (Model View Controller), ovvero ogni applicazione è costituita da moduli (o plugin se preferite) in grado di essere caricati o rimossi "a caldo" e che aggiungono, migliorano o sostituiscono funzionalità di altri moduli.

L'integrazione tra i moduli avviene in modo molto semplice: i moduli contengono componenti che non sono altro che classi annotate con opportune annotation. Il "core" di Ocean, quando carica un modulo, li riconosce e li carica in memoria. I principali di questi componenti sono gli **@OperationProvider** e i **@CoreEventListener**, di cui si parlerà successivamente. Di seguito, riporto un elenco dei tipi di componenti supportati direttamente da Ocean, anche per farvi un'idea di cosa può essere un componente:

- @OperationProvider
- @CoreEventListener
- @EventListener
- @Service
- @FreemarkerDirective
- @ResultDecorator
- @TypeConverter

Il principio implementato da Ocean è molto semplice: un'applicazione implementa delle operazioni e tali operazioni hanno dei parametri di input e un risultato. Con Ocean, ogni operazione può essere invocata in modi anche molto diversi, anche se quasi tutte sono pensate per essere invocate tramite richieste HTTP.

Di qui arriviamo al pattern MVC: nel caso di Ocean, l'operazione costitisce il "controller", il risultato costituisce il "model" e la "view" invece, è rappresentata dal risultato prodotto in output al client. Che differenza c'è tra risultato dell'operazione e risultato prodotto in output al client? Supponiamo di voler implementare un servizio che non faccia altro che ritornare dei dati sotto forma di tabella. In questo caso, i dati ritornati dall'operazione costituiscono il modello, mentre la tabella costituisce la vista per quei dati. E se volessimo ottenere quei dati anche sotto forma di JSON o di foglio Excel? Dovremmo scrivere tre operazioni uguali per produrre tre risultati diversi nella forma, ma identici nel contenuto? Ocean vi permette di scrivere un'unica operazione e, tramite i @ResultDecorator, genera l'output finale. In poche parole, dato il risultato di un'operazione e l'output desiderato, Ocean trova il decoratore più opportuno per produrre l'output richiesto a partire dai dati ottenuti dall'operazione.

Per semplificare il lavoro dello sviluppatore, Ocean fornisce già un set di risultati e decoratori per gli scopi più comuni, così dovrete solo (o poco più) preoccuparvi di implementare la logica dell'applicazione e Ocean farà quasi tutto il resto.

Infine, per assumere la piena padronanza di questo framework, si consiglia vivamente di avere una certa padronanza con:

- Apache Maven
- CDI (Context Dependency Injection)
- JPA (Java Persistence API)
- Apache Freemarker

Fatta questa introduzione su cosa è Ocean e come è strutturato, possiamo passare alla parte pratica, iniziando a costruire la nostra prima applicazione.

## 1. Deploy del core

Il core di Ocean è principalmente utilizzabile in due modi: come JAR all'interno di un'altra web application o come WAR direttamente deployato come applicazione in un application server. Per lo scopo di questo documento, seguiremo l'installazione come WAR.

**ATTENZIONE:** per usare il core di Ocean come JAR all'interno di un'applicazione Web esistente, fare attenzione a caricare tutte e solo le librerie necessarie, evitando di caricare librerie già fornite dall'application server utilizzato. Inoltre, sarà necessario utilizzare il modulo *classicWebApp*.

L'installazione del core in un application server è molto semplice, ma bisogna disporre del WAR corretto per l'application server che andremo ad utilizzare. A tale scopo, il core definisce dei profili per il build con Maven, specifici per ciascun application server:

- Apache Tomcat 7+: profilo no-cdi-jpa
- Wildfly 8-9: profilo wildfly-9
- Wildfly 10: profilo wildfly-10

Compilando il core con il profilo corretto otterremo un WAR direttamente installabile sull'application server designato.

Fatto ciò, andando all'indirizzo http://localhost:8080/core/ vedrete la semplice interfaccia di Ocean. Vedrete che l'unica voce di menu disponibile è la voce "Amministrazione". Da lì, potrete accedere al pannello di amministrazione dell'applicazione. Per il momento l'unico sotto menu che ci interessa è la voce "Moduli", che useremo quando installeremo il modulo che stiamo andando a costruire.

### 1.1. Easy startup

In Ocean Framework è stato creato anche un modulo "startup". Questo modulo permette di costruire ed avviare nel modo più semplice un WAR basato su Ocean.

Questo progetto offre diversi profili per soddisfare qualunque necessità, riassunti nella tabella seguente:

| Application server | Compilazione base | Supporto WebSocket   | Hibernate Search            | WebSocket e Hibernate Search          |
|--------------------|-------------------|----------------------|-----------------------------|---------------------------------------|
| **Tomcat**         | no-cdi-jpa        | no-cdi-jpa websocket | no-cdi-jpa hibernate-search | no-cdi-jpa hibernate-search websocket |
| **Wildfly 9**      | wildfly-9         | wildfly-9 websocket  | wildfly-9 hibernate-search  | wildfly-9 hibernate-search websocket  |
| **Wildfly 10**     | wildfly-10        | wildfly-10 websocket | wildfly-10 hibernate-search | wildfly-10 hibernate-search websocket |
| **Jetty**          | jetty             | jetty websocket      | jetty hibernate-search      | jetty hibernate-search websocket      |

Nel caso di compilazione con i profili relativi a jetty, avete anche la possibilità di avviare direttamente un ambiente di sviluppo completo e pre-configurato con Jetty e HSQLDB. Per far ciò, è sufficiente usare il comando

`mvn -P jetty jetty:run`

Ovviamente è possibile aggiungere i profili riportati sopra per le funzionalità aggiuntive richieste per il vostro progetto.

**ATTENZIONE:** il database HSQLDB è preconfigurato per gestire un database all'interno della cartella target del modulo "startup", quindi ad ogni "clean" del modulo, si ripartirà da una situazione completamente ripulita.

## 2. Creazione di un modulo base

Se avete compilato e fatto l'install dell'intero framework, avrete a disposizione due archetype maven, molto utili per iniziare a costruire una struttura di progetto per creare un'applicazione con Ocean. Questi archetype sono

- ocean-module-archetype
- ocean-war-archetype

Il primo dei due archetype vi consente di creare velocemente la struttura per un modulo da caricare su Ocean. Il secondo invece costruisce la struttura per un'applicazione web contenente Ocean come libreria.

... TBD ...

## 3. Installiamo il nostro primo modulo

Una volta generato e compilato il modulo e fatto il deploy del war del core di Ocean in un application server, potremo caricare il modulo appena generato dal pannello di amministrazione del framework. Per far ciò, una volta aperta la pagina principale dell'applicazione, cliccate su "Amministrazione", quindi su "Moduli", "Carica modulo", selezionate il file JAR del modulo generato e inviate la richiesta.

Il modulo così come è generato dall'archetype maven-module-archetype, contiene un'operazione di test ... **TBD** ...

## 4. Implementiamo le operazioni base per il CRUD

Qualsiasi applicazione necessita di gestire anagrafiche di oggetti. Con Ocean, sviluppare la gestione delle anagrafiche è estremamente semplice e tutto il necessario è già fornito dal framework. In questo capitolo sarà mostrato passo per passo come implementare una semplice gestione anagrafica.

Avremo bisogno di una serie di operazioni per mostrare l'elenco degli oggetti, inserire e modificare gli elementi, eliminarli, esportarli in formato Microsoft Excel ed eventualmente eliminarli. Per iniziare, avremo quindi bisogno di un "operation provider" che consiste in una classe contenente le operazioni.

```java
package com.waveinformatica.ocean.tutorial;

@OperationProvider(namespace = "tutorial")
public class TutorialCrud {
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	@OceanPersistenceContext
	private EntityManager em;
	
}
```

### 4.1. Visualizziamo i dati con una tabella

### 4.2. Costruiamo un form

### 4.3. Salviamo l'oggetto con JPA

### 4.5. Generiamo un foglio Excel

### 4.6. Aggiungiamo i filtri

Per aggiungere i filtri ad un TableResult, è possibile fare riferimento al tutorial specifico [[Ocean:Filtri sulle tabelle]].

## 5. Costruiamo la nostra prima pagina

### 5.1. L'operazione

### 5.2. Il template Freemarker

### 5.3. Generiamo un PDF

## 6. Implementiamo un servizio REST

## 7. Altri risultati disponibili

### 7.1. Operazioni in tab

### 7.2. Operazioni in pila

### 7.3. Stream

## Conclusione

Alla fine di questo tutorial, avrete visto quasi tutto ciò che c'è da sapere sul framework. Ho volutamente lasciato da parte però un tema fondamentale nell'ambito dello sviluppo di software: la profilazione degli utenti e delle operazioni. Questo tema è piuttosto ampio ed esula dallo scopo di questo Getting Started, quindi troverete una pagina interamente dedicata a questo tema.

Il suggerimento quindi è quello di proseguire sulla pagina [[Ocean:Tutorial vari]], dove troverete molti utili tutorial per imparare tutto ciò che c'è da sapere per implementare nel miglior modo possibile le vostre applicazioni.