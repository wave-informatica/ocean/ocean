# Tables filtering

Al TableResult è stato aggiunto il supporto per i filtri sulle colonne. L'utilizzo è molto semplice, ma al momento è supportato unicamente l'utilizzo con JPA.

Per abilitare i filtri sul TableResult, dovete annotare i campi dell'oggetto sui quali desiderate applicare i filtri con l'annotation

`@com.waveinformatica.ocean.core.annotations.Filterable`

Dovete quindi impostare l'attributo `filter` dell'annotation con il tipo di filtro che si desidera utilizzare. &Egrave; anche possibile impostare l'attributo `path` se desiderate utilizzare un nome diverso o un path particolare per identificare un campo all'interno di altri attributi. Sono attualmente definiti i seguenti tipi di filtri:

- core
  - `com.waveinformatica.ocean.core.filtering.NumericFilter`
  - `com.waveinformatica.ocean.core.filtering.StringFilter`
  - `com.waveinformatica.ocean.core.filtering.DayFilter`
- web-extras
  - `com.waveinformatica.ocean.web.extras.filters.MultiSelectFilter` (abstract)
  - `com.waveinformatica.ocean.web.extras.filters.DateRangeFilter`

Nello specifico, il `MultiSelectFilter` è una classe astratta che dovete estendere per definire filtri a selezione multipla con selectize.js (molto simile al `SuggestFieldHandler`).

Fatto ciò, nell'operazione che produce il `TableResult` potete usare il seguente codice:

```java
FilterBuilder filterBuilder = factory.newInstance(FilterBuilder.class);
filterBuilder.init(TableObject.class);
QueryFilter filter = filterBuilder.getJpqlFilter(result, "x");
```

**TableObject** è l'oggetto che userete come tipo per il TableResult, con i campi annotati. al metodo `getJpqlFilter` del **filterBuilder** passerete il TableResult (che implementa l'interfaccia FilterProvider) e l'alias della classe nella query: ad esempio il codice precedente può riferirsi ad una query del tipo

`select **x** from TableObject **x** ....`

Ottenuto quindi il **QueryFilter**, potete costruire la query nel modo seguente:

```java
TypedQuery<TableObject> query = entityManager.createQuery("select x from TableObject x" + (StringUtils.isNotBlank(filter.getCondition()) ? " where " + filter.getCondition() : "") + " order by x.qualcosa", TableObject.class);
filter.fillQueryParams(query); // Imposta i parametri del filtro nella query
List<TableObject> oggetti = query.getResultList();
```