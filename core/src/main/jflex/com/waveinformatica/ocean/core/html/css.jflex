package com.waveinformatica.ocean.core.html;

import java.util.Stack;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

%%

%class CSSLexer
%unicode
%line
%column
%int

LineTerminator = \r|\n|\r\n
WhiteSpace     = {LineTerminator} | [ \t\f]

Comment = {TraditionalComment} | {DocumentationComment}

TraditionalComment   = "/*" [^*] ~"*/" | "/*" "*"+ "/"
DocumentationComment = "/**" {CommentContent} "*"+ "/"
CommentContent       = ( [^*] | \*+ [^/*] )*

Identifier = "-"*[a-zA-Z-]+

Selector = [#\.a-zA-Z0-9_=\(\)\"'\[\]\~\+:\^\>\*\s-]+
KeyframesSelector = [,a-zA-Z0-9_%=\(\)\"'\[\]\~\+:\^\>\*\s\.-]+

/************
 ** STATES **
*************/

%state STRING
%state RULE_BLOCK
%state PROPERTY
%state PROPERTY_IGNORE

%state AT_CHARSET
%state AT_IMPORT
%state AT_NAMESPACE
%state AT_DOCUMENT
%state AT_DOCUMENT_BODY
%state AT_KEYFRAMES
%state AT_KEYFRAMES_BODY
%state AT_MEDIA
%state AT_MEDIA_BODY
%state AT_PAGE
%state AT_PAGE_BODY
%state AT_BLOCK_PROPERTY
%state AT_SUPPORTS
%state AT_SUPPORTS_BODY

%{

    private final Stack<Integer> statesStack = new Stack<Integer>();

    private StringBuilder propValueBuilder;
    private Stack<String> valueTokens = new Stack<String>();

    public void beginState(int state) {
        statesStack.push(yystate());
        yybegin(state);
    }

    public void closeState() {
        yybegin(statesStack.pop());
    }

    public int getLine() {
        return yyline;
    }

    public int getColumn() {
        return yycolumn;
    }

    private void appendValue(String val) {
        if (propValueBuilder == null) {
            propValueBuilder = new StringBuilder();
        }
        propValueBuilder.append(val);
        Pattern pattern = Pattern.compile("[\"\\(\\)]");
        Matcher m = pattern.matcher(val);
        while (m.find()) {
            if (m.group().equals("\"") || m.group().equals("'")) {
                if (!valueTokens.isEmpty() && valueTokens.peek().equals(m.group())) {
                    valueTokens.pop();
                }
                else {
                    valueTokens.push(m.group());
                }
            }
            else if (valueTokens.isEmpty() || (!valueTokens.peek().equals("\"") && !valueTokens.peek().equals("'"))) {
                if (m.group().equals("(")) {
                    valueTokens.push("(");
                }
                else if (m.group().equals(")")) {
                    valueTokens.pop();
                }
            }
        }
    }

    public String getBuiltValue() {
        String res = propValueBuilder.toString();
        propValueBuilder = null;
        closeState();
        return res.trim();
    }

%}

%%

<YYINITIAL> {

    "@charset"      { beginState(AT_CHARSET); return CSSSym.DIR_CHARSET.ordinal(); }

    "@import"       { beginState(AT_IMPORT); return CSSSym.DIR_IMPORT.ordinal(); }

    "@namespace"    { beginState(AT_NAMESPACE); return CSSSym.DIR_NAMESPACE.ordinal(); }

    "@document"     { beginState(AT_DOCUMENT); return CSSSym.DIR_DOCUMENT.ordinal(); }

    "@keyframes" |
    "@-webkit-keyframes" |
    "@-o-keyframes" |
    "@-moz-keyframes" |
    "@-ms-keyframes"
                    { beginState(AT_KEYFRAMES); return CSSSym.DIR_KEYFRAMES.ordinal(); }

    "@media"        { beginState(AT_MEDIA); return CSSSym.DIR_MEDIA.ordinal(); }

    "@page"         { beginState(AT_PAGE); return CSSSym.DIR_PAGE.ordinal(); }

    "@supports"     { beginState(AT_SUPPORTS); return CSSSym.DIR_SUPPORTS.ordinal(); }

    {Selector} |
    "@font-face" |
    "@viewport" |
    "@-ms-viewport"
                    { return CSSSym.SELECTOR.ordinal(); }

    "{"             { beginState(RULE_BLOCK); return CSSSym.BODY_BEGIN.ordinal(); }

    ","             { return CSSSym.SELECTOR_COMMA.ordinal(); }

}

<RULE_BLOCK> {

    "}"             { closeState(); return CSSSym.BODY_END.ordinal(); }

    {Identifier} |
    "*"{Identifier} { beginState(PROPERTY); return CSSSym.PROPERTY_NAME.ordinal(); }

}

<PROPERTY> {

    ";"             { if (valueTokens.isEmpty()) { return CSSSym.PROPERTY_VALUE.ordinal(); } else { appendValue(yytext()); } }

    "}"             { if (valueTokens.isEmpty()) { return CSSSym.BODY_END.ordinal(); } else { appendValue(yytext()); } }

    ":"             { if (valueTokens.isEmpty()) { return CSSSym.PROP_SPEC_COLON.ordinal(); } else { appendValue(yytext()); } }

    [^;:}]+         { appendValue(yytext()); }

}

<PROPERTY_IGNORE> {

    ";"             { if (valueTokens.isEmpty()) { closeState(); } else { appendValue(yytext()); } }

    "}"             { if (valueTokens.isEmpty()) { closeState(); } else { appendValue(yytext()); } }

    [^;}]+          { appendValue(yytext()); /* ignore */ }

}

/****************
    DIRECTIVES
*****************/

<AT_CHARSET> {

    ";"             { closeState(); return CSSSym.DIR_END.ordinal(); }

    [^;]+           { return CSSSym.DIR_VALUE.ordinal(); }

}

<AT_IMPORT> {

    ";"             { closeState(); return CSSSym.DIR_END.ordinal(); }

    [^;]+           { return CSSSym.DIR_VALUE.ordinal(); }

}

<AT_NAMESPACE> {

    ";"             { closeState(); return CSSSym.DIR_END.ordinal(); }

    [^;]+           { return CSSSym.DIR_VALUE.ordinal(); }

}

/***********************
    NESTED DIRECTIVES
************************/

<AT_DOCUMENT> {

    [^,{]*          { return CSSSym.DIR_ARG.ordinal(); }

    ","             { return CSSSym.SELECTOR_COMMA.ordinal(); }

    "{"             { beginState(AT_DOCUMENT_BODY); return CSSSym.BODY_BEGIN.ordinal(); }

}

<AT_DOCUMENT_BODY> {

    {Selector}      { return CSSSym.SELECTOR.ordinal(); }

    "{"             { beginState(RULE_BLOCK); return CSSSym.BODY_BEGIN.ordinal(); }

    ","             { return CSSSym.SELECTOR_COMMA.ordinal(); }

    "}"             { closeState(); return CSSSym.BODY_END.ordinal(); }

}

<AT_KEYFRAMES> {

    [^,{]*          { return CSSSym.DIR_ARG.ordinal(); }

    "{"             { beginState(AT_KEYFRAMES_BODY); return CSSSym.BODY_BEGIN.ordinal(); }

}

<AT_KEYFRAMES_BODY> {

    {KeyframesSelector}
                    { beginState(AT_BLOCK_PROPERTY); return CSSSym.SELECTOR.ordinal(); }

    "}"             { closeState(); return CSSSym.BODY_END.ordinal(); }

}

<AT_MEDIA> {

    [^,{]*          { return CSSSym.DIR_ARG.ordinal(); }

    ","             { return CSSSym.SELECTOR_COMMA.ordinal(); }

    "{"             { beginState(AT_MEDIA_BODY); return CSSSym.BODY_BEGIN.ordinal(); }

}

<AT_MEDIA_BODY> {

    "@keyframes" |
    "@-webkit-keyframes" |
    "@-o-keyframes" |
    "@-moz-keyframes" |
    "@-ms-keyframes"
                    { beginState(AT_KEYFRAMES); return CSSSym.DIR_KEYFRAMES.ordinal(); }

    {Selector} |
    "@font-face" |
    "@viewport" |
    "@-ms-viewport"
                    { return CSSSym.SELECTOR.ordinal(); }

    "{"             { beginState(RULE_BLOCK); return CSSSym.BODY_BEGIN.ordinal(); }

    ","             { return CSSSym.SELECTOR_COMMA.ordinal(); }

    "}"             { closeState(); return CSSSym.BODY_END.ordinal(); }

}

<AT_PAGE> {

    {Selector}      { return CSSSym.SELECTOR.ordinal(); }

    "{"             { beginState(AT_PAGE_BODY); return CSSSym.BODY_BEGIN.ordinal(); }

}

<AT_PAGE_BODY> {

    "@"{Identifier} { beginState(AT_BLOCK_PROPERTY); return CSSSym.SELECTOR.ordinal(); }

    {Identifier}    { beginState(PROPERTY); return CSSSym.PROPERTY_NAME.ordinal(); }

    "}"             { closeState(); return CSSSym.BODY_END.ordinal(); }

    ";"             { /* ignore */ }

}

<AT_BLOCK_PROPERTY> {

    "{"             { return CSSSym.BODY_BEGIN.ordinal(); }

    {Identifier}    { beginState(PROPERTY); return CSSSym.PROPERTY_NAME.ordinal(); }

    "}"             { return CSSSym.BODY_END.ordinal(); }

}

<AT_SUPPORTS> {

    [^{]+           { return CSSSym.DIR_ARG.ordinal(); }

    "{"             { beginState(AT_SUPPORTS_BODY); return CSSSym.BODY_BEGIN.ordinal(); }

}

<AT_SUPPORTS_BODY> {

    "}"             { closeState(); return CSSSym.BODY_BEGIN.ordinal(); }

}

/***********************
    GLOBAL AND ERRORS
************************/

{Comment}           { /* ignore */ }
{WhiteSpace}        { /* ignore */ }
[^]                 { throw new CSSParseException(this, "Illegal character <"+yytext()+"> from " + ((ParserHelperReader)zzReader).getReadData()); }