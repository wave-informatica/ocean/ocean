<@results.wrap>
	
	<#list data?keys as k>
		
		<h2>${k}</h2>
		
		<ul>
			<#list data[k].managedComponents as mc>
			<li>${mc}
				<#if data[k].getManagedComponents(mc.name)?has_content>
				<ul>
					<#list data[k].getManagedComponents(mc.name) as c>
					<li>${c.name}</li>
					</#list>
				</ul>
				</#if>
			</li>
			</#list>
		</ul>
		
	</#list>
	
</@results.wrap>