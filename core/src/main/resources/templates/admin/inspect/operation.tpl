<@results.wrap>
	
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th><@i18n key="operation" /></th>
				<th><@i18n key="scopes" /></th>
				<th><@i18n key="handlers" /></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>${extras["main"].op}</td>
				<td>${extras["main"].handlers?c}</td>
				<td>${extras["main"].scopes?c}</td>
			</tr>
		</tbody>
	</table>
	
	<#list data as scope>
		<h3>${scope.scopeCls}</h3>
		
		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th><@i18n key="module" /></th>
					<th><@i18n key="handler" /></th>
					<th><@i18n key="method" /></th>
				</tr>
			</thead>
			<tbody>
				<#list scope.handlersList as handler>
				<tr>
					<td>${handler.module}</td>
					<td>${handler.info.cls.name}</td>
					<td>${handler.info.method.name}</td>
				</tr>
				</#list>
			</tbody>
		</table>
		
	</#list>
	
	
</@results.wrap>