<p>This filter supports the following formats</p>

<ul>
    <li><code>=N1,N2,N3</code></li>
    <li><code>N..M</code></li>
</ul>

<p>where <code>N1</code>, <code>N2</code>, <code>N3</code>, <code>N</code> and <code>M</code>
    are positive or negative numbers.</p>

<p>Starting the pattern with a <code>=</code> sign, you can specify one or more comma separated values.
    The <code>N..M</code> pattern defines a range instead. When using the last pattern, defined bounds
    are considered inclusive.</p>

<p>You can combine more patterns together separating them with spaces. The filter will produce a union
    of the results of each pattern.</p>

<h2>Examples</h2>

<ul>
    <li><code>=-1</code> matches -1;</li>
    <li><code>=-1,5</code> matches -1 and 5;</li>
    <li><code>-6..0</code> matches values from -6 to 0;</li>
    <li><code>-1..</code> matches values greater or equal to -1;</li>
    <li><code>..10</code> matches values lower or equal to 10;</li>
    <li><code>-20..-10 =5 10..</code> matches all values between -20 and -10, equal to 5 or greater or equal to 10.</li>
</ul>