<p>This filter supports the following formats</p>

<ul>
    <li><code>word</code></li>
    <li><code>"some words"</code></li>
    <li><code>!word</code></li>
    <li><code>!"some words"</code></li>
</ul>

<p>The first pattern matches values containing the specified word, while the second pattern matches values
    containing the exact whole phrase. The "<code>!</code>" before the single pattern negates that specified pattern.</p>

<p>You can combine more patterns together separating them with spaces. The filter will produce the intersection (and)
    of the results for each pattern.</p>

<h2>Examples</h2>

<ul>
    <li><code>foo</code> matches values containing the word "foo";</li>
    <li><code>foo bar</code> matches values containing the word "foo" and the word "bar";</li>
    <li><code>"foo bar"</code> matches values containing the whole phrase "foo bar";</li>
    <li><code>pen "is on the table"</code> matches values containing the word "pen" and, somewhere, the phrase "is on the table".</li>
    <li><code>!pen "is on the table"</code> matches values <strong>not containing</strong> the word "pen", but <strong>containing</strong> the phrase "is on the table".</li>
</ul>