<div class="input-group">
    <input type="text" class="form-control" name="fw:filter:${head.name}" value="${(data.getFilterValue(head.sourceField)!)?html}">
    <#if head.filter.helpTemplate?has_content>
    <span class="input-group-btn">
        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#fw_filter_modal_${head.name}"><span class="glyphicon glyphicon-question-sign"></span></button>
    </span>
    </#if>
</div>

<#if head.filter.helpTemplate?has_content>
<div class="modal fade" tabindex="-1" role="dialog" id="fw_filter_modal_${head.name}">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><@i18n key="filters.help" /></h4>
      </div>
      <div class="modal-body">
        <#include head.filter.helpTemplate>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</#if>