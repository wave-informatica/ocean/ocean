<p>This filter supports the following formats</p>

<ul>
    <li><code>=N1,N2,N3</code></li>
    <li><code>N..M</code></li>
</ul>

<p>where <code>N1</code>, <code>N2</code>, <code>N3</code>, <code>N</code> and <code>M</code>
    are positive or negative numbers representing days from today: negative numbers represent
    days back, while positive numbers represent days in the future.</p>

<p>Starting the pattern with a <code>=</code> sign, you can specify one or more comma separated values.
    The <code>N..M</code> pattern defines a range instead. When using the last pattern, defined bounds
    are considered inclusive.</p>

<p>You can combine more patterns together separating them with spaces. The filter will produce a union
    of the results of each pattern.</p>

<h2>Examples</h2>

<ul>
    <li><code>=0</code> defines today;</li>
    <li><code>=-1</code> for yesterday;</li>
    <li><code>=-1,0</code> for today or yesterday;</li>
    <li><code>=1</code> for tomorrow;</li>
    <li><code>-6..0</code> for the last 7 days including today;</li>
    <li><code>-1..1</code> from yesterday to tomorrow;</li>
    <li><code>-1..</code> since yesterday;</li>
    <li><code>..1</code> until tomorrow included.</li>
</ul>