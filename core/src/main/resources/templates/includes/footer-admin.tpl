
                    </section>
                </div>
            </div>

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> ${_scope.configuration.packageVersion}
                </div>
                <#assign currentDate = .now>
                <strong>Copyright &copy; ${currentDate?date?string("Y")} Ocean&reg; Framework</strong> powered by <a href="http://www.waveinformatica.com" target="_blank">Wave Informatica S.r.l.</a>. All rights reserved.
            </footer>
        </div>
        
        <@resmerge>
        <!-- Ocean Base UI Framework -->
        <script src="ris/js/ocean.js"></script>
        <!-- Bootstrap -->
        <script src="ris/libs/bootstrap-3.3.6/js/bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="ris/libs/AdminLTE-2.3.0/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="ris/libs/AdminLTE-2.3.0/plugins/fastclick/fastclick.min.js"></script>
        <!-- AdminLTE App -->
        <script src="ris/libs/AdminLTE-2.3.0/js/app.min.js"></script>
        <!-- Head scripts -->
        <@head scripts=true css=false />
        </@resmerge>
    </body>
</html>