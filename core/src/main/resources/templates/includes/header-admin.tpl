<!DOCTYPE html>
<html>
    <head>
        <base href="${_scope.contextPath}/">
        <title>${_scope.configuration.siteProperties['site-title']!"Ocean"}<#if (extras["page-title"])??> - ${extras["page-title"]}</#if></title>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta http-equiv="Content-Type" content="charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- jQuery -->
        <script src="ris/js/jquery-1.11.3.min.js"></script>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="ris/libs/bootstrap-3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="ris/libs/font-awesome-4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="ris/libs/AdminLTE-2.3.0/css/AdminLTE.min.css">
        <link rel="stylesheet" href="ris/libs/AdminLTE-2.3.0/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="ris/css/ocean.css">
        <!-- Header CSS -->
        <@head scripts=false css=true />
        <script type="text/javascript" src="ris/libs/prefixfree-1.0.7/prefixfree.js"></script>
        <link rel="icon" href="ris/images/ocean.png" type="image/png" />
    </head>
    <body class="fixed skin-blue sidebar-mini">

        <div class="wrapper">
        	
            <header class="main-header">
            	<a href="" class="logo">
			      <!-- mini logo for sidebar mini 50x50 pixels -->
			      <span class="logo-mini"><b><#if _scope.configuration.siteProperties['site-title']?has_content>${(_scope.configuration.siteProperties['site-title']!"Ocean")?substring(0,1)}<#else>O</#if></b></span>
			      <!-- logo for regular state and mobile devices -->
			      <span class="logo-lg">${_scope.configuration.siteProperties['site-title']!"Ocean"}</span>
			    </a>
            	
                <!-- Header Navbar: style can be found in header.less -->
                <nav role="navigation" class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
				        <span class="sr-only">Toggle navigation</span>
				    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Messages: style can be found in dropdown.less-->

                            <!-- Notifications: style can be found in dropdown.less -->

                            <!-- Tasks: style can be found in dropdown.less -->

                            <#if _scope.userSession.isLogged()>
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                    <img alt="User Image" class="user-image" src="${_scope.userSession.principal.photo!"ris/images/icons/user.png"}">
                                    <span class="hidden-xs">${_scope.userSession.userName!}</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img alt="${_scope.userSession.userName!}" class="img-circle" src="${_scope.userSession.principal.photo!"ris/images/icons/user.png"}">
                                        <p>
                                            ${_scope.userSession.userName!}
                                            <!--small>Member since Nov. 2012</small-->
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <!--li class="user-body">
                                      <div class="col-xs-4 text-center">
                                        <a href="#">Followers</a>
                                      </div>
                                      <div class="col-xs-4 text-center">
                                        <a href="#">Sales</a>
                                      </div>
                                      <div class="col-xs-4 text-center">
                                        <a href="#">Friends</a>
                                      </div>
                                    </li-->
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <!--div class="pull-left">
                                          <a class="btn btn-default btn-flat" href="#">Profile</a>
                                        </div-->
                                        <div class="pull-right">
                                            <a class="btn btn-default btn-flat" href="auth/logout"><@i18n key="logout" /></a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            </#if>

                        </ul>
                    </div>
                </nav>
            </header>
            
            <aside class="main-sidebar">
			    <!-- sidebar: style can be found in sidebar.less -->
			    <section class="sidebar" style="height: auto;">
			      <!-- Sidebar user panel -->
			      <div class="user-panel">
			        <div class="pull-left image">
			          <img src="${(_scope.userSession.principal.photo)!"ris/images/icons/user.png"}" class="img-circle" alt="${(_scope.userSession.userName)!"Guest"}">
			        </div>
			        <div class="pull-left info">
			          <p>${(_scope.userSession.userName)!"Guest"}</p>
			        </div>
			      </div>
			      <!-- search form -->
			      <!--form action="#" method="get" class="sidebar-form">
			        <div class="input-group">
			          <input type="text" name="q" class="form-control" placeholder="Search...">
			          <span class="input-group-btn">
			                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
			                  <i class="fa fa-search"></i>
			                </button>
			              </span>
			        </div>
			      </form-->
			      <!-- /.search form -->
			      <!-- sidebar menu: : style can be found in sidebar.less -->
			      <ul class="sidebar-menu tree" data-widget="tree">
			        <li class="header">MAIN NAVIGATION</li>
			        <li class="treeview">
			          <a href="${_scope.basePath!}">
			            <i class="fa fa-dashboard"></i> <span><@i18n key="dashboard" /></span>
			          </a>
			        </li>
			        <#list _scope.sections as section>
			        <li class="treeview">
			          <a href="#">
			            <i class="fa fa-gear"></i> <span><@i18n key=(section.title?html)! /></span>
			          </a>
			          <ul class="treeview-menu">
			          	<#list section.items as item>
			            <li><a href="${item.operation!}"><i class="fa fa-${item.icon!}"></i> <@i18n key=(item.title?html)! /></a></li>
			            </#list>
			          </ul>
			        </li>
			        </#list>
			      </ul>
			    </section>
			    <!-- /.sidebar -->
			  </aside>

            <div class="content-wrapper">
                <div class="container-fluid">
                    <section class="content-header">
                        <h1>${extras["page-title"]!"&nbsp;"}<#if (extras["page-subtitle"])??> <small>${extras["page-subtitle"]}</small></#if></h1>
                        <ol class="breadcrumb">
                            <li><a href=""><i class="fa fa-home"></i> Home</a></li>
                            <@results.breadcrumb />
                        </ol>
                    </section>
                    <section class="content">
