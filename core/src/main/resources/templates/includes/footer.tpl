
                    </section>
                </div>
            </div>

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.0.0
                </div>
                <strong>Copyright &copy; 2016 <a href="http://www.waveinformatica.com" target="_blank">Wave Informatica S.r.l.</a>.</strong> All rights reserved.
            </footer>
        </div>
        
        <@resmerge>
        <!-- Ocean Base UI Framework -->
        <script src="ris/js/ocean.js"></script>
        <!-- Bootstrap -->
        <script src="ris/libs/bootstrap-3.3.6/js/bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="ris/libs/AdminLTE-2.3.0/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="ris/libs/AdminLTE-2.3.0/plugins/fastclick/fastclick.min.js"></script>
        <!-- AdminLTE App -->
        <script src="ris/libs/AdminLTE-2.3.0/js/app.min.js"></script>
        <!-- Head scripts -->
        <@head scripts=true css=false />
        </@resmerge>
    </body>
</html>