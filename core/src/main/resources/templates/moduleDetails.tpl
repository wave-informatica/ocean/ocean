<@results.wrap>

<#list data as log>

<#switch log.level>
    <#case "INFO">
    <div class="alert alert-info">
        <#break>
    <#case "WARNING">
    <div class="alert alert-warning">
        <#break>
    <#case "CRITICAL">
    <div class="alert alert-danger">
        <#break>
</#switch>
    
    <#if log.exception>
    <button class="close" type="button">
        <span class="glyphicon glyphicon-collapse-down"></span>
    </button>
    </#if>
    
    <p><strong>${log.formattedDate}</strong> ${(log.message!"null")?html}</p>
    
    <#if log.exception>
    <div class="exception hidden">
        <pre>${(log.formattedException!)?html}</pre>
    </div>
    </#if>
    
</div>

</#list>

<script type="text/javascript">
    $(document).ready(function () {
        $('.alert button.close').click(function () {
            var btn = $(this).children('span');
            btn.toggleClass('glyphicon-collapse-down');
            btn.toggleClass('glyphicon-collapse-up');
            $(this).closest('.alert').find('div.exception').toggleClass('hidden');
        });
    });
    
</script>

</@results.wrap>