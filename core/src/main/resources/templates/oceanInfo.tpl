<@results.wrap>

<dl class="dl-horizontal">
    <dt>Vendor</dt>
    <dd>${data.organization!"unkown"}</dd>
    <dt>Vendor URL</dt>
    <dd><a href="${data.organizationUrl!"#"}" target="_blank">${data.organizationUrl!"unkown"}</a></dd>
    <dt>Ocean version</dt>
    <dd>${data.version!"unkown"}</dd>
</dl>

<h2><@i18n key="license" /></h2>

<pre>${data.license}</pre>


</@results.wrap>