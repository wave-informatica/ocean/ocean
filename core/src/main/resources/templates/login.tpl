<@results.wrap>

<form action="auth/login" method="POST" class="form-horizontal">
    <fieldset>
        <div class="form-group">
            <label for="userName" class="col-lg-2 control-label">User name:</label>
            <div class="col-lg-10">
                <input type="text" name="userName" id="userName" class="form-control" placeholder="User name">
            </div>
        </div>
        
        <div class="form-group">
            <label for="password" class="col-lg-2 control-label">Password:</label>
            <div class="col-lg-10">
                <input type="password" name="password" id="password" class="form-control" placeholder="Password">
            </div>
        </div>

        <input type="hidden" name="redirect" value="${data.from}">

        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <input type="reset" value="Reset" class="btn btn-default">
                <input type="submit" value="Login" class="btn btn-success">
            </div>
        </div>

    </fieldset>
</form>

</@results.wrap>