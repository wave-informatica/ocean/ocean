<@results.wrap>
	
	<#if counters?has_content>
	<div class="row">
		<#list counters as counter>
		<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-${(counter.background)!"aqua"}"><i class="fa fa-${(counter.icon)!}"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><@i18n key=counter.title! /></span>
              <span class="info-box-number">${(counter.value?html)!}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
		</#list>
	</div>
	</#if>
	
	<#if tiles?has_content>
	<div class="row">
		<#list tiles as tile>
		<div class="tile col-md-4 col-sm-6">
			<div class="box box-info" id="${tile.clientUniqueId?html}" data-sid="${(tile.serverId?c)!}">
				
				<div class="tile-header box-header with-border">
					
					<div class="tile-title">
						
						<#if tile.icon?has_content>
						<span class="${tile.icon?html}"></span>
						</#if>
		
						${(tile.title!)?html}
						
					</div>
						
					<div class="pull-right tile-options box-tools">
						
						<#if tile.options?has_content>
						<div class="dropdown">
							<button class="btn btn-xs btn-link dropdown-toggle" title="<@i18n key="tile.options" />" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								<i class="fa fa-gear"></i>
							</button>
							<ul class="dropdown-menu dropdown-menu-right">
								<#list tile.options as option>
								<#if option.url == "-">
								<li role="separator" class="divider"></li>
								<#else>
								<li class="${(option.cls!)?html}">
									<a href="${(option.url!"#")?html}">
										<#if option.icon?has_content>
										<span class="option-icon ${option.icon?html}"></span>
										</#if>
										
										${(option.text!)?html}
									</a>
								</li>
								</#if>
								</#list>
							</ul>
						</div>
						</#if>
					
						<i class="fa fa-bars tile-handle"></i>
						
					</div>
					<div class="clearfix"></div>
				</div>
					
				<div class="tile-body box-body">
					<#include tile.template>
				</div>
					
				<div class="tile-footer box-footer clearfix">
					<div class="pull-right btn-group">
						<#if tile.buttons?has_content>
						<#list tile.buttons as btn>
						<a href="${(btn.url!"#")?html}" class="btn btn-link">
							<#if btn.icon?has_content>
							<span class="${btn.icon?html}"></span>
							</#if>
								
							${(btn.text!)?html}
						</a>
						</#list>
						<#else>
						<span style="display:block;height:29px">&nbsp;</span>
						</#if>
					</div>
				</div>
				
			</div>
		</div>
		</#list>
	</div>
	</#if>
	
	<script type="text/javascript">
		$(document).ready(function () {
			$('.tile .tile-options a[href="#"]').click(function (evt) {
				evt.preventDefault();
			});
			
			/*$( ".tiles" ).sortable({
				handle: '.tile-handle',
				stop: function (event, ui) {
					
					var ids = [];
					
					$('.tiles .tile[data-sid]').each(function () {
						ids.push($(this).data('sid'));
					});
					
					$.post('sortTiles', {
						ids: ids
					}, function (data) {
					}, 'json');
					
				}
			});
	    	$( ".tiles" ).disableSelection();*/
		});
		
	</script>
	
</@results.wrap>