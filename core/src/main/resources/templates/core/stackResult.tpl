<@results.wrap>

<#list operations as op>
<div>
    <@invoke operation=op />
</div>
</#list>

</@results.wrap>