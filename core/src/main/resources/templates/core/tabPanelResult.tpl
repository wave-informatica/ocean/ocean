<@results.wrap>

<div data-namespace="${data.namespace}" class="contenitore-tabs">

	<ul class="nav nav-tabs">
		<#list data.panel as tab>
	  		<li <#if tab.operation == data.active> class="active"</#if>
	  		><a href="#${tab.key}" data-href="${data.pathBase}<#if data.pathBase?contains("?")>&<#else>?</#if>fw:tab=${tab.operation?url}" 
	  		data-toggle="tab"><@i18n key=data.namespace + "." + tab.label /></a></li>
	  	</#list>
	</ul>
	
	<div class="tab-content">
		<#list data.panel as tab>
	    	<div class="tab-pane <#if tab.operation == data.active>active</#if>" id="${tab.key}">
	    		<@invoke operation=tab.operation />
	    	</div>
		</#list>
	</div>

	<#list data.shared as op>
		<@invoke operation=op />
	</#list>
</div>

<script type="text/javascript">
	$('div[data-namespace="${data.namespace}"] >ul>li> a[data-toggle="tab"]').click(
		function (event) {
			event.preventDefault();
			$.get($(this).data('href'), undefined, undefined, "html");
		} 
	);
</script>

</@results.wrap>