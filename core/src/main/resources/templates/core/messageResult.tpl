<@results.wrap>

<#switch data.type>
    <#case "INFO">
    <div class="alert alert-info <#if !fullResult>alert-dismissible</#if>">
        <#break>
    <#case "WARNING">
    <div class="alert alert-warning <#if !fullResult>alert-dismissible</#if>">
        <#break>
    <#case "CRITICAL">
    <div class="alert alert-danger <#if !fullResult>alert-dismissible</#if>">
        <#break>
    <#case "CONFIRM">
    <div class="alert alert-success <#if !fullResult>alert-dismissible</#if>">
        <#break>
</#switch>
    
    <#if !(fullResult||(data.cancelAction)?? || (data.confirmAction)??) >
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </#if>
    
    <p>${data.message}</p>
    
    <#if data.exception??>
    <p>
        <pre>${data.exception}</pre>
    </p>
    </#if>
    
</div>

<#if fullResult || (data.cancelAction)?? || (data.confirmAction)?? >
<div class="btn-toolbar">
    <div class="btn-group pull-right">
        <#if data.cancelAction??>
        <a class="btn btn-default btn-action-cancel" href="${data.getFullOperation(data.cancelAction)}">&laquo; CANCEL</a>
        </#if>
        <#if data.confirmAction??>
        <a class="btn btn-success btn-action-confirm" href="${data.getFullOperation(data.confirmAction)}">OK &raquo;</a>
        </#if>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$(document).on('keydown', function (evt) {
			if (evt.keyCode == 13) {
				$('.btn-action-confirm')[0].click();
			}
			else if (evt.keyCode == 27) {
				$('.btn-action-cancel')[0].click();
			}
		});
	});
</script>
</#if>

</@results.wrap>