<@results.wrap>

<#if subTitle?has_content>
<h2 class="af-centered-x af-subtitle">${(subTitle?html)!}</h2>
</#if>

<div>
    <#list sections as section>
    <div class="box box-default">
        <#if section.title??>
        <div class="box-header with-border">
            <h3 class="box-title"><@i18n key=section.title!"" /></h3>
        </div>
        </#if>
        <div class="box-body">
            <#list section.items as menu>
            <div class="menu-item" style="">
                <a href="${menu.operation}">
                    <i class="fa fa-${menu.icon} fa-4x"></i><br>
                    <span><@i18n key=menu.title /></span>
                </a>
            </div>
            </#list>
        </div>
    </div>
    </#list>
</div>

</@results.wrap>