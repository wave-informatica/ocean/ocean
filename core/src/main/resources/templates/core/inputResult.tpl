<@results.wrap>

<#if message??>
<#switch message.type>
    <#case "INFO">
    <div class="alert alert-info <#if !((message.cancelAction)?? || (message.confirmAction)??)>alert-dismissible</#if>">
        <#break>
    <#case "WARNING">
    <div class="alert alert-warning <#if !((message.cancelAction)?? || (message.confirmAction)??)>alert-dismissible</#if>">
        <#break>
    <#case "CRITICAL">
    <div class="alert alert-danger <#if !((message.cancelAction)?? || (message.confirmAction)??)>alert-dismissible</#if>">
        <#break>
    <#case "CONFIRM">
    <div class="alert alert-success <#if !((message.cancelAction)?? || (message.confirmAction)??)>alert-dismissible</#if>">
        <#break>
</#switch>

    <#if !((message.cancelAction)?? || (message.confirmAction)??)>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </#if>
    
    <p>${message.message}</p>
    
    <#if message.exception??>
    <p>
        <pre>${message.exception}</pre>
    </p>
    </#if>
    
</div>
 <#if (message.cancelAction)?? || (message.confirmAction)??>
  <div class="btn-toolbar">
    <div class="btn-group pull-right">
        <#if message.cancelAction??>
        <a class="btn btn-default" href="${message.cancelAction}">&laquo; CANCEL</a>
        </#if>
        <#if message.confirmAction??>
        <a class="btn btn-success" href="${message.confirmAction}">OK &raquo;</a>
        </#if>
    </div>
  </div>
 </#if> 
</#if>
<@input.form class="${formClass!}">

    <#list fields as field>
        <#if field.handler.fieldTemplate??>
            <@input.wrapField>
                <@input.field name=field.name />
            </@input.wrapField>
        </#if>
    </#list>

    <@input.submit />
</@input.form>
	
<script type="text/javascript">
	
	$(document).ready(function () {

		$("form").bind("reset", function(event) {
			event.preventDefault();
			$(this).find("input,textarea").each(function () {
                                if ($(this).attr('type') != 'submit' && $(this).attr('type') != 'reset') {
                                    $(this).val('');
                                }
                        });
		});
	});
</script>

</@results.wrap>
