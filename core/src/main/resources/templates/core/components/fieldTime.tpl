<#if !showLabel?? || showLabel>
<label for="fld_${field.name}" class="control-label">${field.label}</label>
<div class="form-element">
</#if>
    <div class="input-group bootstrap-timepicker timepicker">
        <input id="fld_${field.name}" type="text" class="form-control input-small" name="${field.name}" data-provide="timepicker" data-show-meridian="false" value="${field.value!""}">
        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
        <#if field.tooltip?has_content>
        <span class="input-group-btn">
            <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="<@i18n key=field.tooltip />" data-html="true">
                <i class="fa fa-info-circle"></i>  
            </button>
        </span>
        </#if>
    </div>
<#if !showLabel?? || showLabel>
</div>
</#if>

