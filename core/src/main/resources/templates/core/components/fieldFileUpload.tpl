<label for="fld_${field.name}" class="control-label">${field.label}</label>
<input type="file" name="${field.name}" id="fld_${field.name}" class="file">
<div class="form-element">
    <div class="input-group">
        <input type="text" class="form-control" disabled placeholder="<@i18n key="ocean.form.file.placeholder" />">
        <span class="input-group-btn">
            <button class="browse btn btn-primary" type="button"><i class="glyphicon glyphicon-search"></i> <@i18n key="ocean.form.file.browse" /></button>
        </span>
        <#if field.tooltip?has_content>
        <span class="input-group-btn">
            <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="<@i18n key=field.tooltip />" data-html="true">
                <i class="fa fa-info-circle"></i>  
            </button>
        </span>
        </#if>
    </div>
</div>
<script type="text/javascript">
    $('#fld_${field.name}').closest('.form-group').on('click', '.browse', function(){
        var file = $(this).closest('.form-group').find('input[type="file"]');
        file.trigger('click');
    });
    $('#fld_${field.name}').closest('.form-group').on('change', 'input[type="file"]', function(){
        var val = $(this).val();
        var pos = Math.max(val.lastIndexOf('/'), val.lastIndexOf('\\'));
        $(this).closest('.form-group').find('.form-control').val($(this).val().substring(pos + 1));
    });
</script>