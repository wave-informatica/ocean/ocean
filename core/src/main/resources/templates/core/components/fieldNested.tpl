<fieldset>
	
	<caption>${field.label?html}</caption>
	
	<#assign parent=field>
	
	<#list parent.handler.fields as field>
		
        <#if field.handler.fieldTemplate??>
            <@input.wrapField>
                <@input.field result=parent.handler name=field.name />
            </@input.wrapField>
        </#if>
		
	</#list>
	
</fieldset>