<label for="fld_${field.name}" class="control-label">${field.label}</label>
<div class="form-element">
    <#if field.tooltip?has_content>
    <div class="input-group">
    </#if>
        <input type="password" name="${field.name}" id="fld_${field.name}" class="form-control"<#if field.required> required</#if>>
    <#if field.tooltip?has_content>
        <span class="input-group-btn">
            <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="<@i18n key=field.tooltip />" data-html="true">
                <i class="fa fa-info-circle"></i>  
            </button>
        </span>
    </div>
    </#if>
</div>