<#if !showLabel?? || showLabel>
<label for="fld_${field.name}" class="control-label">${field.label?html}</label>
<div class="form-element">
</#if>
    <#if field.tooltip?has_content>
    <div class="input-group">
    </#if>
        <input type="text" name="${field.name}" id="fld_${field.name}" value="${(field.value!"")?html}" class="form-control"<#if field.required> required</#if><#if field.readOnly> readonly</#if>>
    <#if field.tooltip?has_content>
        <span class="input-group-btn">
            <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="<@i18n key=field.tooltip />" data-html="true">
                <i class="fa fa-info-circle"></i>  
            </button>
        </span>
    </div>
    </#if>
<#if !showLabel?? || showLabel>
</div>
</#if>