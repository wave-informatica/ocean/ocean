<#if !showLabel?? || showLabel>
<label for="fld_${field.name}" class="control-label">${field.label}</label>
<div class="form-element">
</#if>
    <#if field.tooltip?has_content>
    <div class="input-group">
    </#if>
        <select name="${field.name}" id="fld_${field.name}" class="form-control"<#if field.required> required</#if><#if field.readOnly> disabled</#if>>
            <option value=""></option>
            <#list field.handler.items as item>
                    <#if item.value??>
                            <option value="${item.value?html}" <#if field.value?? && field.value == item.value>selected="selected"</#if>>${item.label?html}</option>
                    </#if>
            </#list>
        </select>
        <#if field.readOnly>
        <input type="hidden" name="${field.name}" value="${(field.value?html)!}">
        </#if>
    <#if field.tooltip?has_content>
        <span class="input-group-btn">
            <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="<@i18n key=field.tooltip />" data-html="true">
                <i class="fa fa-info-circle"></i>  
            </button>
        </span>
    </div>
    </#if>
<#if !showLabel?? || showLabel>
</div>
</#if>