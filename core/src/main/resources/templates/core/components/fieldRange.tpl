<label for="fld_${field.name}.from" class="control-label">${field.label}</label>

<#assign showLabel=false>

<div class="form-element row range-field">
    
    <#assign rangeHandler=field.handler>
    
    <div class="col-md-3">
        <#list [ rangeHandler.fromWrapper ] as field>
        <#include field.handler.fieldTemplate>
        </#list>
    </div>
    
    <span class="range-separator">&ndash;</span>
    
    <div class="col-md-3">
        <#list [ rangeHandler.toWrapper ] as field>
        <#include field.handler.fieldTemplate>
        </#list>
    </div>
    
</div>

<#assign showLabel=true>