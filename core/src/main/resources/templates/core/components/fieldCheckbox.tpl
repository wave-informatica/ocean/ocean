<#if !showLabel?? || showLabel>
<label for="fld_${field.name}" class="control-label">${field.label}</label>
<div class="form-element">
    <div class="checkbox">
        <label for="fld_${field.name}">
            <input name="${field.name}" id="fld_${field.name}" type="checkbox" value="true" <#if field.value?? && field.value == "true">checked="checked"</#if><#if field.required> required</#if><#if field.readOnly> disabled</#if>> Seleziona
            <input name="${field.name}" id="fld_${field.name}_hidden" type="hidden" value="false">
        </label>
    </div>
</div>
<#else>
    <div class="checkbox">
        <label for="fld_${field.name}">
            <input name="${field.name}" id="fld_${field.name}" type="checkbox" value="true" <#if field.value?? && field.value == "true">checked="checked"</#if><#if field.required> required</#if><#if field.readOnly> disabled</#if>> ${field.label}
            <input name="${field.name}" id="fld_${field.name}_hidden" type="hidden" value="false">
        </label>
    </div>
</#if>