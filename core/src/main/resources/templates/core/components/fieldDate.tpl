<#if !showLabel?? || showLabel>
<label for="fld_${field.name}" class="control-label">${field.label}</label>
<div class="form-element">
</#if>
    <#if field.tooltip?has_content>
    <div class="input-group">
    </#if>
        <div class="input-group date ocean-date-component" data-ocean="bootstrapDatePicker.init">
          <input type="text" class="form-control" name="${field.name}" data-validator-events="dp.change" data-format="${field.handler.getClientFormat(field.attributes["format"]!"dd/MM/yyyy")}" id="fld_${field.name}" value="${field.value!""}"<#if field.required> required</#if><#if field.readOnly> readonly</#if>><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
        </div>
        <input type="hidden" name="${field.name}$f" value="${field.attributes["format"]!"dd/MM/yyyy"}">
    <#if field.tooltip?has_content>
        <span class="input-group-btn">
            <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="<@i18n key=field.tooltip />" data-html="true">
                <i class="fa fa-info-circle"></i>  
            </button>
        </span>
    </div>
    </#if>
<#if !showLabel?? || showLabel>
</div>
</#if>
