<div class="control-label">${field.label}</div>
<div class="form-element">
    <#list field.handler.items as item>
    <div class="checkbox">
        <label><input name="${field.name}" value="${item.value?html}" type="checkbox" <#if field.values?seq_contains(item.value)>checked="checked"</#if><#if field.required> required</#if>> ${item.label?html}</label>
    </div>
    </#list>
</div>