<@results.wrap>

<style>
.table > thead > tr > th {
    white-space: nowrap;
}

table.table th.with-menu {
    padding: 0;
}
table.table th.with-menu > div.dropdown > button {
    border: 0 none;
    border-radius: 0;
    text-align: left;
    width: 100%;
}
table.table th.with-menu > div.dropdown > button > span.caret {
    border: 0;
}
table.table th.with-menu > div.dropdown > button > span.caret.ASC {
    border-bottom: 4px dashed;
    border-top: 0;
    border-left: 4px solid transparent;
    border-right: 4px solid transparent;
    display: inline-block;
    height: 0;
    margin-left: 2px;
    vertical-align: middle;
    width: 0;
}
table.table th.with-menu > div.dropdown > button > span.caret.DESC {
    border-top: 4px dashed;
    border-bottom: 0;
    border-left: 4px solid transparent;
    border-right: 4px solid transparent;
    display: inline-block;
    height: 0;
    margin-left: 2px;
    vertical-align: middle;
    width: 0;
}
table.table th.with-menu > div.dropdown > button > span.caret {
    display: block;
    float: right;
    height: 100%;
    margin: 8px auto;
    text-align: right;
}
table.table th.with-menu > div.dropdown > button:hover, table.table th.with-menu > div.dropdown.open > button {
    border: 1px solid #ccc;
    border-radius: 4px;
}
</style>

<#if data.checkable || data.hasFilters>
<form method="post" action="#" id="TableResult_Form_${(name?html)!}">
    <!--input type="hidden" name="__tbl_action" value=""-->
</#if>

<div class="btn-toolbar">

    <#if data.tableOperations?size &gt; 0>
    <div class="btn-group pull-left">
        <#list data.tableOperations as op>
        <a href="${op.name}" class="btn btn-default table-operation"><@i18n key=op.title /></a>
        </#list>
    </div>
    </#if>

    <ul class="btn-group pagination pull-right">
    <#if data.rowsPerPage?? && data.rowsPerPage &gt; 0 && data.pagesCount &gt; 1>
        <#list 1..data.pagesCount as p>
            <#if (p != 1 && p != data.pagesCount) && (p < (data.page - (data.pagesPerBox / 2)) || p > (data.page + (data.pagesPerBox / 2))) && data.pagesCount &gt; data.pagesPerBox>
                    <#if p == 2 || p == (data.pagesCount - 1)>
                    <li class="disabled"><span>...</span></li>
                    </#if>
                <#else>
                <li<#if p == data.page> class="active"</#if>>
                    <a<#if p == data.page> class="active"</#if> href="${data.getFullOperation(data.pathBase)}<#if data.getFullOperation(data.pathBase)?contains("?")>&<#else>?</#if>fw:page=${p?c}<#if data.dataConfiguration?? && data.dataConfiguration.sorted>&fw:sort=${data.dataConfiguration.sorting.direction}-${data.dataConfiguration.sorting.columnName?url}</#if>">
                    ${p}
                    </a>
                </li>
                    </#if>
        </#list>
    </#if>
        <li class="disabled">
            <span><@i18n key="results" param=data.totalRowsCount?string.number /></span>
        </li>
    </ul>

</div>

<div class="table-responsive" style="width:100%;">
    <table class="table table-striped table-hover ">
        <thead>
            <tr>
                <#if data.alignRowOperations == "LEFT" && data.rowOperations?size &gt; 0>
                <th colspan="${data.rowOperations?size}"><@i18n key="actions" /></th>
                </#if>
                <#if data.checkable>
                <th><input type="checkbox" title="Seleziona/Deseleziona tutti" id="TableResult_selectAll"></th>
                </#if>
                <#list data.header.columns as head>
                <#if !head.hidden>
                <th <#if head.sortable && data.dataConfiguration??>class="with-menu"</#if>>
                    <#if head.sortable && data.dataConfiguration??>
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" type="button" id="dLabel">
                                <span class="<#if data.dataConfiguration.sorted>caret <#if data.dataConfiguration.sorting.columnName == head.name>${data.dataConfiguration.sorting.direction}</#if></#if>"></span>
                                ${head.label}
                            </button>
                            <ul aria-labelledby="dLabel" class="dropdown-menu">
                                <li><a href="${data.getFullOperation(data.pathBase)}<#if data.getFullOperation(data.pathBase)?contains("?")>&<#else>?</#if>fw:page=${data.page}&fw:sort=ASC-${head.name?url}">Ordine crescente</a></li>
                                <li><a href="${data.getFullOperation(data.pathBase)}<#if data.getFullOperation(data.pathBase)?contains("?")>&<#else>?</#if>fw:page=${data.page}&fw:sort=DESC-${head.name?url}">Ordine decrescente</a></li>
                            </ul>
                        </div>
                    <#else>
                        ${head.label}
                    </#if>
                </th>
                </#if>
                </#list>
                <#if data.alignRowOperations == "RIGHT" && data.rowOperations?size &gt; 0>
                <th colspan="${data.rowOperations?size}"><@i18n key="actions" /></th>
                </#if>
            </tr>
            <#if data.hasFilters>
            <tr>
                <#if data.alignRowOperations == "LEFT">
                <td<#if data.rowOperations?size &gt; 0> colspan="${data.rowOperations?size}"</#if>><button type="submit" class="btn btn-xs btn-default"><i class="fa fa-filter"></i> <@i18n key="filter" /></button></td>
                </#if>
                <#if data.checkable>
                <th>&nbsp;</th>
                </#if>
                <#list data.header.columns as head>
                <#if !head.hidden>
                <td>
                    <#if head.filter?? && head.sourceField??>
                        <#include head.filter.template>
                    <#else>
                        &nbsp;
                    </#if>
                </td>
                </#if>
                </#list>
                <#if data.alignRowOperations == "RIGHT">
                <td<#if data.rowOperations?size &gt; 0> colspan="${data.rowOperations?size}"</#if>><button type="submit" class="btn btn-xs btn-default"><i class="fa fa-filter"></i> <@i18n key="filter" /></button></td>
                </#if>
            </tr>
            </#if>
        </thead>
        <tbody>
            <#list data.pageRows as row>
            <tr<#if row.classes?has_content> class="<#list row.classes as cl>${cl}<#sep> </#sep></#list>"</#if>>
                <#if data.alignRowOperations == "LEFT">
                <#if data.rowOperations?size == 0 && data.hasFilters>
                <td>&nbsp;</td>
                <#elseif data.rowOperations?size &gt; 0>
                <#assign handledOperations = data.handledRowOperations>
                <#if data.maxOperations?? && handledOperations?size &gt; data.maxOperations>
                <#list handledOperations as op>
                <#if op?index &gt;= data.maxOperations - 1><#break></#if>
                <td>
                    <a class="btn btn-xs<#if !op.enabled> btn-default disabled<#else> btn-primary</#if> btn-block" href="${op.name}${op.getQuery(row)}"><@i18n key=op.title /></a>
                </td>
                </#list>
                <td colspan="${handledOperations?size - (data.maxOperations!0) + 1}">
                    <div class="dropdown">
                        <button class="btn btn-primary btn-xs btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true">
                            <@i18n key=data.maxOperationsLabel!"ocean.tableResult.otherActions" />&#8230;
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <#list handledOperations as op>
                            <#if op?index &gt;= data.maxOperations - 1>
                            <li<#if !op.enabled> class="disabled"</#if>><a<#if op.enabled> href="${op.name}${op.getQuery(row)}"</#if>><@i18n key=op.title /></a></li>
                            </#if>
                            </#list>
                        </ul>
                    </div>
                </td>
                <#else>
                <#list handledOperations as op>
                <td>
                    <a class="btn btn-xs<#if !op.enabled> btn-default disabled<#else> btn-primary</#if> btn-block" href="${op.name}${op.getQuery(row)}"><@i18n key=op.title /></a>
                </td>
                </#list>
                </#if>
                </#if>
                </#if>
                <#if data.checkable>
                <td style="text-align: center;"><input type="checkbox" class="TableResult_rowSelect" value="${data.getKey(row)}" name="fw:tab_sel" <#if data.selectedRows?seq_contains(data.getKey(row))>checked="checked"</#if>></td>
                </#if>
                <#list row.cells as cell>
                <#if !data.header.columns[cell_index].hidden>
                <td>
                    <#assign column = data.header.columns[cell_index]>
                    <#if column.template?has_content>
                        <#include column.template>
                    <#elseif column.linkType == "OPERATION">
                        <a href="${column.formatLinkOperation(cell.value!)}">${cell.value!"&nbsp;"}</a>
                    <#elseif column.linkType == "JAVASCRIPT">
                        <a onclick="${column.formatLinkOperation(cell.value!)}" href="javascript:void(0);">${cell.value!"&nbsp;"}</a>
                    <#elseif column.editType != "NONE">
                        <a href="#" class="TableResult_editable" data-pk="${data.getKey(row)}" data-type="${column.editType?lower_case}" data-url="${column.editOperation?html}" data-name="${column.name?html}" data-title="${column.label?html}" data-source="${column.editSource!?html}">${(cell.value)!"&nbsp;"}</a>
                    <#else>
                        <#if cell.value?has_content>${cell.value}<#else>&nbsp;</#if>
                    </#if>
                </td>
                </#if>
                </#list>
                <#if data.alignRowOperations == "RIGHT">
                <#if data.rowOperations?size == 0 && data.hasFilters>
                <td>&nbsp;</td>
                <#elseif data.rowOperations?size &gt; 0>
                <#assign handledOperations = data.handledRowOperations>
                <#if data.maxOperations?? && handledOperations?size &gt; data.maxOperations>
                <#list handledOperations as op>
                <#if op?index &gt;= data.maxOperations - 1><#break></#if>
                <td>
                    <a class="btn btn-xs<#if !op.enabled> btn-default disabled<#else> btn-primary</#if> btn-block" href="${op.name}${op.getQuery(row)}"><@i18n key=op.title /></a>
                </td>
                </#list>
                <td colspan="${handledOperations?size - (data.maxOperations!0) + 1}">
                    <div class="dropdown">
                        <button class="btn btn-primary btn-xs btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true">
                            <@i18n key=data.maxOperationsLabel!"ocean.tableResult.otherActions" />&#8230;
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <#list handledOperations as op>
                            <#if op?index &gt;= data.maxOperations - 1>
                            <li<#if !op.enabled> class="disabled"</#if>><a<#if op.enabled> href="${op.name}${op.getQuery(row)}"</#if>><@i18n key=op.title /></a></li>
                            </#if>
                            </#list>
                        </ul>
                    </div>
                </td>
                <#else>
                <#list handledOperations as op>
                <td>
                    <a class="btn btn-xs<#if !op.enabled> btn-default disabled<#else> btn-primary</#if> btn-block" href="${op.name}${op.getQuery(row)}"><@i18n key=op.title /></a>
                </td>
                </#list>
                </#if>
                </#if>
                </#if>
            </tr>
            </#list>
        </tbody>
    </table>
</div>

<div class="btn-toolbar">

    <#if data.tableOperations?size &gt; 0>
    <div class="btn-group pull-left">
        <#list data.tableOperations as op>
        <a href="${op.name}" class="btn btn-default table-operation"><@i18n key=op.title /></a>
        </#list>
    </div>
    </#if>

    <ul class="btn-group pagination pull-right">
    <#if data.rowsPerPage?? && data.rowsPerPage &gt; 0 && data.pagesCount &gt; 1>
        <#list 1..data.pagesCount as p>
            <#if (p != 1 && p != data.pagesCount) && (p < (data.page - (data.pagesPerBox / 2)) || p > (data.page + (data.pagesPerBox / 2))) && data.pagesCount &gt; data.pagesPerBox>
                    <#if p == 2 || p == (data.pagesCount - 1)>
                    <li class="disabled"><span>...</span></li>
                    </#if>
                <#else>
                <li<#if p == data.page> class="active"</#if>>
                    <a<#if p == data.page> class="active"</#if> href="${data.getFullOperation(data.pathBase)}<#if data.getFullOperation(data.pathBase)?contains("?")>&<#else>?</#if>fw:page=${p?c}<#if data.dataConfiguration?? && data.dataConfiguration.sorted>&fw:sort=${data.dataConfiguration.sorting.direction}-${data.dataConfiguration.sorting.columnName?url}</#if>">
                    ${p}
                    </a>
                </li>
                    </#if>
        </#list>
    </#if>
        <li class="disabled">
            <span><@i18n key="results" param=data.totalRowsCount?string.number /></span>
        </li>
    </ul>

</div>

<#if data.checkable || data.hasFilters>
</form>

<script type="text/javascript">
    $(document).ready(function () {
        $('#TableResult_selectAll').click(function () {
            $('.TableResult_rowSelect').prop('checked', $(this).prop('checked'));
        });
        if ($('.TableResult_rowSelect:checked').length === $('.TableResult_rowSelect').length) {
            $('#TableResult_selectAll').prop('checked', true);
        }
        $('.TableResult_rowSelect').click(function () {
            if (!$(this).prop('checked')) {
                $('#TableResult_selectAll').prop('checked', false);
            }
            else {
                var allChecked = true;
                $('.TableResult_rowSelect').each(function (){
                    allChecked = allChecked && $(this).prop('checked');
                });
                $('#TableResult_selectAll').prop('checked', allChecked);
            }
        });
        $('a.table-operation').click(function (evt) {
            evt.preventDefault();
            var oldAction = $('#TableResult_Form_${(name?html)!}').attr('action');
            //$('#TableResult_Form input[name="__tbl_action"]').val($(this).attr('href'));
            $('#TableResult_Form_${(name?html)!}')
                    .attr('action', $(this).attr('href'))
                    .submit();
            $('#TableResult_Form_${(name?html)!}').attr('action', oldAction);
        });
        <#if data.hasFilters>
        var action = location.href;
        if (action.indexOf('?') !== -1) {
            var query = action.substring(action.indexOf('?') + 1);
            var parts = query.split('&');
            for (var i = 0; i < parts.length; i++) {
                if (decodeURIComponent(parts[i]).substring(0, 3) === 'fw:') {
                    parts.splice(i, 1);
                    i--;
                }
            }
            query = parts.join('&');
            action = action.substring(0, action.indexOf('?'));
            if (query.length > 0) {
                action += '?' + query;
            }
        }
        $('#TableResult_Form_${(name?html)!}').attr('action', action);
        </#if>
    });
</script>
</#if>

<#if extras["table_inline_edit"]!false == true>
<script type="text/javascript">
    $(document).ready(function(){
        $('.TableResult_editable').editable({
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: "<@i18n key="empty"/>",
            success: function (response, newValue) {
                if (response.message && response.type && response.type == 'CRITICAL') {
                    return response.message;
                }
            }
        });
    });
</script>
</#if>

</@results.wrap>