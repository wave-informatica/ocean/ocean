<#macro row class="" bgcolor="">
	
	<tr>
        <td<#if class?has_content> class="${class}"</#if><#if bgcolor?has_content> bgcolor="#c7d8a7"</#if>>
        	
        	<#nested>
        	
        </td>
    </tr>
	
</#macro>

<#macro column width height="" first=false style="">
	
	<#if first>
	<table width="${width}" align="left" border="0" cellpadding="0" cellspacing="0">  
      <tr>
        <td<#if height?has_content> height="${height}"</#if><#if style?has_content> style="${style}"</#if>>
          <img src="images/article1.png" width="115" height="115" border="0" alt="" />
        </td>
      </tr>
    </table>
	<#else>
	<!--[if (gte mso 9)|(IE)]>
	<table width="${width}" align="left" cellpadding="0" cellspacing="0" border="0">
	    <tr>
	        <td>
	        <![endif]-->
	            <table class="col${width}" align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: ${width}px;">
	                <tr>
	                    <td<#if height?has_content> height="${height}"</#if><#if style?has_content> style="${style}"</#if>>
	                    	<#nested>
	                    </td>
	                </tr>
	            </table>
	        <!--[if (gte mso 9)|(IE)]>
	        </td>
	    </tr>
	</table>
	<![endif]-->
	</#if>
	
</#macro>

<#macro header title="" logo="" bgcolor="" height=70 subhead="">
	
	<@row class="header" bgcolor=bgcolor>
    	<@column width=height height=height style="padding: 0 20px 20px 0;">
		    <img src="${logo}" width="${height}" height="${height}" border="0" alt="" / >
		</@column>
		<@column width=425 height=height>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<#if subhead?has_content>
			    <tr>
			        <td class="subhead" style="padding: 0 0 0 3px;">
			            ${subhead?upper_case}
			        </td>
			    </tr>
			    </#if>
			    <tr>
			        <td class="h1" style="padding: 5px 0 0 0;">
			            ${title}
			        </td>
			    </tr>
			</table>
		</@column>
	</@row>
	
</#macro>

<#macro section title="">
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<#if title?has_content>
	    <tr>
	        <td class="h2">
	            ${title}
	        </td>
	    </tr>
	    </#if>
	    
	    <!-- Use @text or @button -->
	    <#nested>
	    
	</table>
	
</#macro>

<#macro text>
	
	<tr>
		<td class="bodycopy">
			<#nested>
		</td>
    </tr>
	
</#macro>

<#macro button bgcolor="">
	
	<tr>
		<td style="padding: 20px 0 0 0;">
			<table class="buttonwrapper"<#if bgcolor?has_content> bgcolor="${bgcolor}"</#if> border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="button" height="45">
						
						<!-- Use a link -->
						<#nested>
						
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
</#macro>

<#macro footer copyright="" unsubscribe="" class="footer" bgcolor="#44525f">
	
	<tr>
	    <td<#if class?has_content> class="${class}"</#if><#if bgcolor?has_content> bgcolor="${bgcolor}"</#if>>
	        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	            <tr>
	                <td align="center" class="footercopy">
	                	<#if copyright?has_content>
	                    &reg; ${copyright}
	                    </#if>
	                    <#if copyright?has_content && unsubscribe?has_content>
	                    <br/>
	                    </#if>
	                    <#if unsubscribe?has_content>
	                    <a href="${unsubscribe?html}"><font color="#ffffff">Unsubscribe</font></a> from this newsletter instantly
	                    </#if>
	                </td>
	            </tr>
	            
	            <!-- Use @socialRow ? -->
	            <#nested>
	            
	        </table>
	    </td>
	</tr>
	
</#macro>

<#macro socialRow>
	
	<tr>
        <td align="center" style="padding: 20px 0 0 0;">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                	
                	<!-- Use a @social -->
                	<#nested>
                	
                </tr>
            </table>
        </td>
    </tr>
	
</#macro>

<#macro social link icon alt="">
	
	<td width="37" style="text-align: center; padding: 0 10px 0 10px;">
        <a href="${link}">
            <img src="${icon}" width="37" height="37" alt="${alt}" border="0" />
        </a>
    </td>
	
</#macro>