<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<#import "/templates/mail/macros.tpl" as mail>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>${(_scope.subject)!}</title>
        <link rel="stylesheet" href="ris/css/mail-base.css">
    </head>
    <body yahoo bgcolor="#f6f8f1">
        <table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                	<!--[if (gte mso 9)|(IE)]>
					<table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
					    <tr>
					        <td>
					            <![endif]-->
			                    <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
			                        