<html>
	<head>
		
	</head>
	<body>
		<p>The error was generated at the following location</p>
		<pre><strong>${(method!)?html}</strong> ${(href!)?html}</pre>
		
		<#if loggedUser??>
		<p>The user who generated the error is:</p>
		<pre>${loggedUser.id?c} - ${loggedUser}</pre>
		</#if>
		
		<p>The exception's stack trace:</p>
		<pre>${(exception!)?html}</pre>
	</body>
</html>
