<#macro form class="form-horizontal" action=.data_model.action result=.data_model>
    
    <form action="${(getFullAction(action))!action}" method="post" enctype="multipart/form-data" class="${class}" data-messages="i18n/messages" <#if (result.validateOperation)?has_content>data-remote="${result.validateOperation}"</#if>>
        
        <fieldset>
            
            <#nested>

        </fieldset>
          
    </form>
    
</#macro>

<#macro submit>
    <div class="btn-toolbar">
        <div class="btn-group pull-right">
            <button type="submit" <#if name?has_content>name="form" value="${name!}"</#if> class="btn btn-success"><@i18n key="form.send" /></button>
        </div>
    </div>
</#macro>

<#macro wrapField>
    <div class="form-group">
        <#nested>
        <div class="help-block with-errors"></div>
        <div class="help-block with-warnings"></div>
    </div>
</#macro>

<#macro field name result=.data_model>
    
    <#local field=result.getField(name)>

    <#if field.handler.fieldTemplate?has_content>
        
        <#include field.handler.fieldTemplate>
        
    </#if>
    
</#macro>