<#macro wrap __scope=_chain.current>
	
	<#if (!fullResult?? || fullResult) && __scope.parent??>
		
		<@wrap __scope=__scope.parent>
			
			<#list [__scope] as _scope>
				<#if __scope.headerTemplate?has_content>
					<#include __scope.headerTemplate>
				</#if>
				
				<#nested>
				
				<#if __scope.footerTemplate?has_content>
					<#include __scope.footerTemplate>
				</#if>
			</#list>
			
		</@wrap>
		
	<#else>
		
		<#list [__scope] as _scope>
			<#if (!fullResult?? || fullResult) && __scope.headerTemplate?has_content>
				<#include __scope.headerTemplate>
			</#if>
			
			<#nested>
			
			<#if (!fullResult?? || fullResult) && __scope.footerTemplate?has_content>
				<#include __scope.footerTemplate>
			</#if>
		</#list>
		
	</#if>
	
</#macro>

<#macro breadcrumb __scope=_chain.current>
	
	<#if __scope.parent??>
		
		<@breadcrumb __scope=__scope.parent />
		
	</#if>
	
	<#if __scope.navigationLinks?has_content>
	<#list __scope.navigationLinks as link>
	<li><a href="${(link.url!)?html}" title="<@i18n key=link.text! />"><@i18n key=link.text! /></a></li>
	</#list>
	</#if>
	
</#macro>