/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
(function () {

	uiModule('bootstrapDatePicker', function () {
		
		return {
			init: init
		};
		
	});
	
	function fireEvent(element,event){
	    if (document.createEventObject){
	        // dispatch for IE
	        var evt = document.createEventObject();
	        return element.fireEvent('on'+event,evt)
	    }
	    else{
	        // dispatch for firefox + others
	        var evt = document.createEvent("HTMLEvents");
	        evt.initEvent(event, true, true ); // event type,bubbling,cancelable
	        return !element.dispatchEvent(evt);
	    }
	}
	
	function init () {
		
		// this is the .ocean-date-component
		var date = $(this);
		if (!date.is(':input')) {
			date = date.children(':input');
		}
		if (date.is('[type="date"]')) {
			date = date
				.attr('type', 'text')
				.attr('autocomplete', 'off');
		}
		
		if (!date.prop('readonly')) {
			
			/*var fn = 'datepicker';
			if (date.data('format') && date.data('format').indexOf('HH') >= 0) {
				fn = 'datetimepicker';
			}*/
			fn = 'datetimepicker';
			
			$(this)[fn]({
				format: date.data('format') || 'DD/MM/YYYY',
				/*weekStart: 1,*/
				/*todayBtn: "linked",*/
				locale: "it" /*,*/
				/*autoclose: true,*/
				/*todayHighlight: true*/
			});
			$(this).on('dp.change', function (evt) {
				if ($(this).is('input')) {
					$(this).trigger('input');
				}
				else {
					$(this).find('input[type="text"]').trigger('input');
				}
				if (!evt.date || !evt.oldDate || evt.date.toDate() != evt.oldDate.toDate()) {
					var $this = $(this);
					if (!$this.is('input')) {
						$this = $this.find('input');
					}
					fireEvent($this[0], 'valuechange');
				}
			});
			
		}
		
	}
	
	$(document).on('focus', '[data-ocean="bootstrapDatePicker.init"] > input', function () {
		if ($(this).data('datepicker') || $(this).prop('readonly')) {
			return;
		}
		else {
			init.call(this);
		}
	});
	$(document).on('click focus', '[data-ocean="bootstrapDatePicker.init"] .input-group-addon', function () {
		$(this).prev().focus();
	});
	
})();
