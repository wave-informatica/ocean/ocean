/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
uiModule('network', function () {
	
	function postData(url, data) {
		var form = $('<form>');
		form.attr('action', url)
			.attr('method', 'post')
			.attr('enctype', 'multipart/form-data')
			.hide();
		for (var i in data) {
			if (typeof data[i] === 'undefined' || data[i] === null) {
				continue;
			}
			if (Array.isArray(data[i])) {
				for (var j = 0; j < data[i].length; j++) {
					var f = $('<input>');
					f.attr('type', 'hidden')
					.attr('name', i)
					.attr('value', data[i][j]);
					form.append(f);
				}
			}
			else {
				var f = $('<input>');
				f.attr('type', 'hidden')
				.attr('name', i)
				.attr('value', data[i]);
				form.append(f);
			}
		}
		$(document.body).append(form);
		form[0].submit();
		setTimeout(function () {
			form.remove();
		}, 1000);
	}
	
	return {
		postData: postData
	};
	
});