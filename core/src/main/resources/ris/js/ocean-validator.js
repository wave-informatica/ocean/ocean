/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
var ValidatorService = new (function () {
    
    var LOG_LEVEL = {
        NONE: 5,
        ERROR: 4,
        WARNING: 3,
        INFO: 2,
        DEBUG: 1,
        ALL: 0
    };
    
    var ValidationResult = function () {
        this.errors = [];
        this.warnings = [];
        this.mappedErrors = {};
        this.mappedWarnings = {};
        this.error = function (msg) {
            this.errors.push(msg);
        };
        this.warning = function (msg) {
            this.warnings.push(msg);
        };
        this.mapErrors = function (errors) {
            this.mappedErrors = $.extend(this.mappedErrors, errors);
        };
        this.mapWarnings = function (warnings) {
            this.mappedWarnings = $.extend(this.mappedWarnings, warnings);
        };
    };
    
    function getMessageTemplate(config, icon, messages) {
        var list = $('<ul class="list-unstyled"></ul>');
        for (var i = 0; i < messages.length; i++) {
            var el = $('<li><span class="glyphicon glyphicon-' + icon + '"></span> ' + messages[i] + '</li>');
            list.append(el);
        }
        return list;
    }
    
    function showMessages(config, result) {
        
        var field = $(this);
        var formGroup = field.closest('.form-group');
        
        var errors = formGroup.find('.help-block.with-errors');
        var warnings = formGroup.find('.help-block.with-warnings');
        
        // Cleaning
        errors.html('');
        warnings.html('');
        formGroup.removeClass('has-error');
        formGroup.removeClass('has-warning');
        
        // Showing messages
        if (result.warnings.length > 0) {
            formGroup.addClass('has-warning');
            warnings.append(getMessageTemplate(config, 'warning-sign', result.warnings));
        }
        if (result.errors.length > 0) {
            formGroup.addClass('has-error');
            errors.append(getMessageTemplate(config, 'exclamation-sign', result.errors));
        }
        
    }
    
    function showMappedMessages(config, errors) {
        
    	for (var i in errors.mappedWarnings) {
            var err = new ValidationResult();
            for (var j = 0; j < errors.mappedWarnings[i].length; j++) {
                err.warning(errors.mappedWarnings[i][j]);
            }
            showMessages.call($(this).find('[name="' + i + '"]')[0], config, err);
    	}
        for (var i in errors.mappedErrors) {
            var err = new ValidationResult();
            for (var j = 0; j < errors.mappedErrors[i].length; j++) {
                err.error(errors.mappedErrors[i][j]);
            }
            showMessages.call($(this).find('[name="' + i + '"]')[0], config, err);
        }
        
    }
    
    function toggleSubmit(config, disable) {
        
        var form = $(this).is('form') ? $(this) : $(this).closest('form');
        
        var submits = form.find('[type="submit"]');
        
        if (disable) {
            submits.addClass('disabled');
            submits.prop('disabled', true);
        }
        else {
            submits.removeClass('disabled');
            submits.prop('disabled', false);
        }
        
    }
    
    /* Era solo un'idea... vediamo...
    function buildChain(scope, initFn, conditionFn, nextFn, execFn, afterEachFn, endFn, args) {
        
        function execute(scope, conditionFn, nextFn, execFn, afterEachFn, args) {
            if (conditionFn.apply(scope, args)) {
                var result = execFn.apply(scope, args);
                if (typeof result !== 'undefined' && typeof result.then === 'function') {
                    result.then(function () {
                        afterEachFn.apply(scope, args);
                        nextFn.apply(scope, args);
                        execute(scope, conditionFn, nextFn, execFn, afterEachFn, args);
                    }, function () {

                    });
                }
                else {
                    afterEachFn.apply(scope, args);
                    nextFn.apply(scope, args);
                    execute(scope, conditionFn, nextFn, execFn, afterEachFn, args);
                }
            }
            else {
                
            }
        }
        
        initFn.apply(scope, args);
        execute(scope, conditionFn, nextFn, execFn, afterEachFn, args).done(function (defer) {
            var finArgs = [];
            for (var i = 0; i < args.length; i++) {
                finArgs[i] = args[i];
            }
            finArgs.push(defer);
            endFn.apply(scope, finArgs);
        });
        
    }*/
    
    function validateField (config) {
        
        var errors = false;
        
        var result = new ValidationResult();
        
        /*
        var orderedValidators = [];
        for (var i in validators) {
            orderedValidators.push(validators[i]);
        }
        
        var i = 0;
        buildChain(this, function () {
            i = 0;
        }, function () {
            return i < orderedValidators.length;
        }, function () {
            i++;
        }, function (config, result) {
            return orderedValidators[i].call(this, config, result);
        }, function (config, result) {
            if (result.errors.length > 0) {
                errors = true;
            }
        }, [config, result]);
        */
        
        for (var i in validators) {
            validators[i].call(this, config, result);
        }
        if (result.errors.length > 0) {
            errors = true;
        }
        showMessages.call(this, config, result);
        
        return errors;
        
    }
    
    function showModal(id, content) {
        var tpl = $('#' + id).html();
        Mustache.parse(tpl);
        var rendered = $(Mustache.render(tpl, content));
        rendered.on('hidden.bs.modal', function() {
            $(this).remove();
        });
        rendered.modal('show');
    }
    
    function remoteValidate(config, errors) {
        
        var form = $(this);
        
        return $.Deferred(function (defer) {
            
            var prevRequest = form.data('ocean.validation-request');
            if (prevRequest) {
                prevRequest.abort();
            }
            
            prevRequest = $.ajax({
                data: form.serialize(),
                dataType: 'json',
                type: 'POST',
                url: config.remote
            }).done(function (data) {
                if (!data.success) {
                    if (typeof data.alerts !== 'undefined') {
                        for (var i = 0; i < data.alerts.length; i++) {
                            errors.error(data.alerts[i]);
                        }
                    }
                    errors.mapErrors(data.fieldErrors);
                    errors.mapWarnings(data.fieldWarnings);
                    defer.reject(errors);
                }
                else {
                	errors.mapWarnings(data.fieldWarnings);
                    defer.resolve(errors);
                }
            }).fail(function () {
                defer.reject();
            }).always(function () {
                form.data('ocean.validation-request', null);
            });
            
            form.data('ocean.validation-request', prevRequest);
            
        }).promise();
    }
    
    function validateForm (config, submitting, fastValidate) {
        
        var form = $(this);
        
        var errors = false;
        
        form.find(config.selector).each(function () {
            errors = validateField.call(this, config) || errors;
        });
        
        return $.Deferred(function (defer) {
            if (!errors && config.remote && fastValidate !== true) {
                var result = new ValidationResult();
                remoteValidate.call(form[0], config, result).then(function () {
                    toggleSubmit.call(form[0], config, errors);
                    if (typeof result.mappedWarnings === 'object') {
                        showMappedMessages.call(form[0], config, result);
                    }
                    defer.resolve();
                }, function (errors) {
                    if (typeof errors === 'object') {
                        showMappedMessages.call(form[0], config, result);
                        if (submitting && errors.errors.length > 0) {
                            var html = '<ul>';
                            for (var i = 0; i < errors.errors.length; i++) {
                                html += '<li>' + errors.errors[i] + '</li>';
                            }
                            html += '</ul>';
                            showModal(config.modal, html);
                        }
                    }
                    defer.reject();
                });
            }
            else {
                toggleSubmit.call(form[0], config, errors);
                if (errors) {
                    defer.reject();
                }
                else {
                    defer.resolve();
                }
            }
        }).promise();
        
    }
    
    function getMessage(name) {
        var msg = messages[name];
        var args = arguments;
        msg = msg.replace(/{([0-9]+)}/g, function (match, p1, offset, string) {
            var idx = parseInt(p1);
            if (idx < 0 || idx >= args.length) {
                return '';
            }
            else {
                return args[idx];
            }
        });
        return msg;
    }
    
    var validators = {
        
        required: function (config, result) {
            
            if (!$(this).prop('required')) {
                return;
            }
            
            if ($(this).is('input[type="checkbox"],input[type="radio"]')) {
                var form = $(this).closest('form');
                var checked = false;
                form.find('input[type="' + $(this).attr('type') + '"][name="' + $(this).attr('name') + '"]:enabled').each(function () {
                    checked = checked || $(this).prop('checked');
                });
                if (checked) {
                    return;
                }
            }
            else {
                var val = $(this).val();
                if (typeof val === 'string') {
                    if (val.trim() !== '') {
                        return;
                    }
                }
                else if (Array.isArray(val)) {
                    if (val.length > 0) {
                        return;
                    }
                }
                else if (typeof val !== 'undefined') {
                    if (val !== null) {
                        return;
                    }
                }
            }
            
            result.error(getMessage('validation.required'));
            
        },
        
        match: function (config, result) {
            var target = $(this).data('match');
            if (!target) {
                return;
            }
            if ($(this).val() && $(this).val() !== $(this).closest('form').find(target).val()) {
                result.error(getMessage('validation.match'));
            }
        },
        
        pattern: function (config, result) {
            if (!$(this).attr('pattern')) {
                return;
            }
            var pattern = new RegExp($(this).attr('pattern'));
            if ($(this).val() && !$(this).val().match(pattern)) {
                if ($(this).data('pattern')) {
                    result.error($(this).data('pattern'));
                }
                else {
                    result.error(getMessage('validation.pattern'));
                }
            }
        },
        
        range: function (config, result) {
            if (!$(this).val()) {
                return;
            }
            if ($(this).attr('min') && parseInt($(this).val()) < parseInt($(this).attr('min'))) {
                result.error(getMessage('validation.range.min', $(this).val(), $(this).attr('min')));
            }
            if ($(this).attr('max') && parseInt($(this).val()) > parseInt($(this).attr('max'))) {
                result.error(getMessage('validation.range.max', $(this).val(), $(this).attr('max')));
            }
        },
        
        html5inputs: function (config, result) {
            if (!$(this).val() || !$(this).attr('type')) {
                return;
            }
            switch ($(this).attr('type')) {
                case 'color':
                    if (!/^#[0-9a-f]{6}$/.test($(this).val())) {
                        result.error(getMessage('validation.color.format', $(this).val()));
                    }
                    break;
                case 'number':
                    if (!/^[+-]?[0-9]+(\.[0-9]+)?$/.test($(this).val())) {
                        result.error(getMessage('validation.number.format', $(this).val()));
                    }
                    break;
                case 'email':
                    if (!/^[^@\s]+@[^@\s]+$/.test($(this).val())) {
                        result.error(getMessage('validation.email.format', $(this).val()));
                    }
                    break;
                case 'tel':
                    if (!/^\+?\s*(\([0-9 ]+\))?[0-9 ]+$/.test($(this).val())) {
                        result.error(getMessage('validation.tel.format', $(this).val()));
                    }
                    break;
                case 'url':
                    if (!/^[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?$/i.test($(this).val())) {
                        result.error(getMessage('validation.url.format', $(this).val()));
                    }
                    break;
            }
        }
        
    };
    
    var customValidatorCount = 0;
    var messages = {
        "validation.required": 'This field is required',
        "validation.match": 'These fields do not match',
        "validation.pattern": 'The field does not match the expected pattern',
        "validation.range.max": 'The value is higher than {2}',
        "validation.range.min": 'The value is lower than {2}',
        "validation.number.format": 'The number is invalid',
        "validation.tel.format": 'The phone number is invalid',
        "validation.email.format": 'The e-mail address is invalid',
        "validation.color.format": 'The color value is invalid',
        "validation.url.format": 'The url value is invalid'
    };
    
    this.addValidator = function (validator) {
        validators['custom' + (customValidatorCount++)] = validator;
    };
    
    this.getMessage = function () {
        return getMessage.apply(this, arguments);
    };
    
    this.validate = function (form) {
        if (typeof form === 'undefined') {
            var me = this;
            $('form').each(function () {
                me.validate($(this));
            });
        }
        else {
            var config = form.data('ocean.validator.config');
            form.each(function () {
                validateForm.call(this, config);
            });
        }
    };
    
    $(document).ready(function () {
    	
    	var observer = new MutationObserver(
			function (mutations) {
				for (var i = 0; i < mutations.length; i++) {
					if (mutations[i].addedNodes) {
						for (var j = 0; j < mutations[i].addedNodes.length; j++) {
							var node = mutations[i].addedNodes[j];
							if (node.tagName == 'FORM') {
								attachForm.call(node);
							}
						}
					}
				}
			}
		);
    	observer.observe(document.body, {
    		attributes: false,
    		characterData: false,
    		childList: true,
    		subtree: true
    	});
        
        $('form').each(attachForm);
        
        function attachForm() {

            var form = $(this);

            var config = $.extend({
                selector: ':input:not([type="submit"], [type="reset"], button, .selectize-control input):enabled:visible,.form-group:visible select.selectized,.form-group:visible [type="radio"],.form-group:visible [type="checkbox"]',
                log: LOG_LEVEL.NONE,
                remote: '',
                modal: ''
            }, form.data());

            // Initializing form
            form.prop('novalidate', true);
            form.data('ocean.validator.config', config);
            
            var checkFn = function (evt) {
                if (!$(this).is('input[type="text"],input[type="email"],input[type="number"],textarea') && evt.type === 'keyup') {
                    return;
                }
                validateForm.call($(this).closest('form')[0], config, false, evt.type === 'keyup');
            };
            
            form.on('change keyup paste', config.selector, checkFn);
            form.find(config.selector).each(function () {
            	if ($(this).data('validator-events')) {
            		$(this).on($(this).data('validator-events'), checkFn);
            	}
            });

            form.find('[type="submit"]').click(function (evt) {
                evt.preventDefault();
                $(this).prop('disabled', true);
                validateForm.call($(this).closest('form')[0], config, true).done(function () {
                    setTimeout(function () {
                    	var event = $.Event('ocean.validator.submitting');
                        form.trigger(event);
                        if (!event.isDefaultPrevented()) {
                        	form.submit();
                        }
                    }, 0);
                });
            });
            
            if (form.data('messages')) {
                $.get(form.data('messages')).done(function (data) {
                    messages = $.extend(messages, data);
                    validateForm.call(form[0], config);
                });
            }
            else {
                validateForm.call(form[0], config);
            }

        }
        
    });
    
    function log(config, level, msg) {
        if (level < config.log)
            return;
        switch (level) {
            case LOG_LEVEL.DEBUG:
                console.log('DEBUG: ' + msg);
                break;
            case LOG_LEVEL.INFO:
                console.info(msg);
                break;
            case LOG_LEVEL.WARNING:
                console.warn(msg);
                break;
            case LOG_LEVEL.ERROR:
                console.error(msg);
                break;
        }
    }
    
})();