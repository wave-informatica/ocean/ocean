/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
(function () {
	
	////////////////////////
	// Modules management //
	////////////////////////
	
	var modules = [];
	var waitedModules = [];
	
	window.uiModule = function (dependencies, name, loadFunction) {
		
		if (arguments.length === 1) {
			if (typeof dependencies === 'function') {
				dependencies = dependencies();
			}
			if (typeof dependencies === 'string') {
				dependencies = [ dependencies ];
			}
			
			var result = {};
			for (var i = 0; i < dependencies.length; i++) {
				result[dependencies[i]] = modules[dependencies[i]] && modules[dependencies[i]].loaded ? modules[dependencies[i]].result : null;
			}
			return result;
		}
		else if (arguments.length === 2) {
			loadFunction = arguments[1];
			name = arguments[0];
			dependencies = [];
		}
		else if (arguments.length !== 3) {
			throw 'Invalid arguments count';
		}
		
		if (typeof dependencies === 'function') {
			dependencies = dependencies();
		}
		if (typeof dependencies === 'string') {
			dependencies = [ dependencies ];
		}
		if (!Array.isArray(dependencies)) {
			dependencies = [];
		}
		
		modules[name] = {
			deps: dependencies,
			name: name,
			load: loadFunction,
			loaded: false,
			result: null
		};
		
		verifyLoaded(modules[name]);
		
	};
	
	function verifyLoaded(module) {
		if (typeof module === 'string') {
			module = modules[module];
			if (typeof module !== 'object') {
				return false;
			}
		}
		var allLoaded = true;
		for (var i = 0; i < module.deps.length; i++) {
			if (!modules[module.deps[i]] || !modules[module.deps[i]].loaded) {
				allLoaded = false;
				break;
			}
		}
		if (allLoaded) {
			var args = [];
			var deps = uiModule(module.deps);
			for (var i = 0; i < module.deps.length; i++) {
				args.push(deps[module.deps[i]]);
			}
			module.result = module.load.apply(window, args);
			module.loaded = true;
			// Waited modules actions
			for (var i = 0; i < waitedModules.length; i++) {
				if (waitedModules[i].name === module.name) {
					if (typeof waitedModules[i].onLoad === 'function') {
						waitedModules[i].onLoad();
					}
					waitedModules.splice(i, 1);
					i--;
				}
			}
			// Load dependent modules
			for (var i in modules) {
				if (modules[i].loaded) {
					continue;
				}
				verifyLoaded(modules[i]);
			}
		}
		
	};
	
	//////////////////
	// DOM Observer //
	//////////////////
	
	function attachModule(node) {
		
		var module = node.dataset.ocean.split(',');
		for (var i = 0; i < module.length; i++) {
			if (typeof node.dataset.oceanModulesAttached !== 'undefined' && node.dataset.oceanModulesAttached[module[i]]) {
				return;
			}
			var index = module[i].indexOf('.');
			var fn = module[i].substr(index + 1);
			var moduleName = module[i].substr(0, index);
			var moduleCtrl = uiModule(moduleName);
			if (!moduleCtrl[moduleName] || typeof moduleCtrl[moduleName][fn] !== 'function') {
				waitedModules.push({
					name: moduleName,
					onLoad: function () {
						attachModule(node);
					}
				});
			}
			else {
				if (typeof node.dataset.oceanModulesAttached === 'undefined') {
					node.dataset.oceanModulesAttached = {};
				}
				node.dataset.oceanModulesAttached[module[i]] = true;
				moduleCtrl[moduleName][fn].call(node);
			}
		}
	}
	
	function initObserver() {
		
		var observer = new MutationObserver(
			function (mutations) {
				for (var i = 0; i < mutations.length; i++) {
					if (mutations[i].addedNodes) {
						for (var j = 0; j < mutations[i].addedNodes.length; j++) {
							var node = mutations[i].addedNodes[j];
							if (node.dataset && node.dataset.ocean) {
								attachModule(node);
							}
							if (node.querySelectorAll) {
								var children = node.querySelectorAll('[data-ocean]');
								for (var k = 0; k < children.length; k++) {
									attachModule(children[k]);
								}
							}
						}
					}
				}
			}
		);
		
		observer.observe(document.body, {
			attributes: false,
			characterData: false,
			childList: true,
			subtree: true
		});
		
		// Init already present components
		var components = document.querySelectorAll('[data-ocean]');
		for (var i = 0; i < components.length; i++) {
			attachModule(components[i]);
		}
		
	}
	
	setTimeout(initObserver, 0);
	
})();