/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
uiModule('layout', function () {
	
	function initRelayout(el, fn) {
		if (!el.layoutResizeListener) {
			$(window).resize(function () {
				fn.call(el);
			});
			el.layoutResizeListener = true;
		}
	}
	
	function initContainer(el) {
		var pos = $(el).css('position');
		var size = 'auto';
		if (pos !== 'relative' && pos !== 'absolute' && pos !== 'fixed') {
			pos = 'relative';
			size = '100%';
		}
		$(el).css({
			overflow: 'hidden',
			position: pos
		});
		if (size !== 'auto') {
			$(el).css({
				width: size,
				height: size
			});
		}
	}
	
	function fitLayout() {
		initContainer(this);
	}
	
	function boxLayout() {
		
		initContainer(this);
		
		var dir = 'vertical';
		if ($(this).data('dir') === 'horizontal') {
			dir = 'horizontal';
		}
		
		var $this = $(this);
		
		$(this).children().each(function () {
			// general css
			$(this).css({
				position: 'relative'
			});
			// dir css
			switch (dir) {
			case 'vertical':
				$(this).css({
					display: 'block',
					width: '100%',
					height: 100.0 / $this.children().length + '%'
				});
				break;
			case 'horizontal':
				$(this).css({
					display: 'inline-block',
					height: '100%',
					width: 100.0 / $this.children().length + '%'
				});
				break;
			}
		});
		
	}
	
	function borderLayout() {
		
		initContainer(this);
		
		var heightSum = 0, widthSum = 0;
		
		var children = {
			north: null,
			east: null,
			center: null,
			west: null,
			south: null
		};
		
		$(this).children().each(function () {
			var pos = $(this).data('pos');
			if (typeof children[pos] === 'undefined') {
				pos = 'center';
			}
			children[pos] = this;
			if (pos == 'north' || pos == 'south') {
				if (!$(this).data('size')) {
					$(this).css('overflow', 'hidden');
					$(this).css('height', 'auto');
				}
				heightSum += $(this).data('size') || $(this).height();
			}
			else if (pos == 'east' || pos == 'west') {
				if (!$(this).data('size')) {
					$(this).css('overflow', 'hidden');
					$(this).css('width', 'auto');
				}
				widthSum += $(this).data('size') || $(this).width();
			}
		});
		
		var northHeight = 0;
		var westWidth = 0;
		
		for (var i in children) {
			if (children[i]) {
				$(children[i]).css({
					position: 'absolute'
				});
			}
		}
		if (children.north) {
			northHeight = $(children.north).data('size') || $(children.north).height();
			$(children.north).css({
				top: '0px',
				left: '0px',
				right: '0px',
				height: northHeight + 'px'
			});
		}
		if (children.south) {
			$(children.south).css({
				bottom: '0px',
				left: '0px',
				right: '0px',
				height: ($(children.south).data('size') || $(children.south).height()) + 'px'
			});
		}
		if (children.west) {
			westWidth = $(children.west).data('size') || $(children.west).width();
			$(children.west).css({
				top: northHeight + 'px',
				left: '0px',
				bottom: (heightSum - northHeight) + 'px',
				width: westWidth + 'px'
			});
		}
		if (children.east) {
			$(children.east).css({
				top: northHeight + 'px',
				right: '0px',
				bottom: (heightSum - northHeight) + 'px',
				width: ($(children.east).data('size') || $(children.east).width()) + 'px'
			});
		}
		if (children.center) {
			$(children.center).css({
				top: northHeight + 'px',
				left: westWidth + 'px',
				right: (widthSum - westWidth) + 'px',
				bottom: (heightSum - northHeight) + 'px'
			});
		}
	}
	
	function accordionLayout() {

		initContainer(this);
		
		$ul = $(this).children('ul');
		
		$ul.children('li').addClass('layout-accordion-element')
		
	}
	
	function tabs() {
		
		initContainer(this);
		
		var ul = $(this).children('ul');
		var cards = $(this).children('div');
		
		ul.addClass('tabs');
		
		var marker = $('<!-- -->');
		marker.insertBefore(cards.first());
		
		var childrenCntr = $('<div>');
		childrenCntr.attr('data-ocean', 'layout.card');
		childrenCntr.append(cards);
		
		childrenCntr.insertAfter(marker);
		marker.remove();
		
		ul.css({
			position: 'relative'
		});
		
		ul.children('li').children('a').on('click', function (evt) {
			evt.preventDefault();
			childrenCntr.find('.o-layout-card-active').removeClass('o-layout-card-active');
			$($(this).attr('href')).addClass('o-layout-card-active');
			$($(this).attr('href')).each(function () {
				relayout(this);
			});
		});
		
		setTimeout(function () {
			childrenCntr.css({
				height: 'calc(100% - ' + ul.height() + 'px)'
			});
		}, 0);
		
	}
	
	function cardLayout() {
		
		initContainer(this);
		
		$(this).addClass('o-layout-card');
		
		$(this).children('div').each(function () {
			$(this).css({
				position: 'relative',
				overflow: 'hidden',
				width: '100%',
				height: '100%'
			});
		});
		
		$(this).children('div').first().addClass('o-layout-card-active');
		
	}
	
	function scrollable() {
		$(this).css({
			'overflow-x': 'hidden',
			'overflow-y': 'auto'
		});
	}
	
	function noContainer() {
		$(this).css('overflow', 'visible');
	}
	
	function relayout(el) {
		if (!$(el).data('ocean')) {
			return;
		}
		var ls = $(el).data('ocean').split(',');
		for (var i = 0; i < ls.length; i++) {
			var l = ls[i].trim();
			var d = l.indexOf('.');
			if (d >= 0) {
				var m = l.substr(0, d);
				var f = l.substr(d + 1);
				if (m === 'layout' && typeof obj[f] === 'function') {
					obj[f].call(el);
				}
			}
		}
	}
	
	var obj = {
		accordion: accordionLayout,
		box: boxLayout,
		border: borderLayout,
		card: cardLayout,
		fit: fitLayout,
		scrollable: scrollable,
		tabs: tabs,
		noContainer: noContainer,
		relayout: relayout
	};
	
	return obj;
	
});