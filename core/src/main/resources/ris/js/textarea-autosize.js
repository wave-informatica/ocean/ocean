/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
uiModule('textareaAutosize', function () {
	
	function resize() {
		
		var style = getComputedStyle(this);
		var taStyle = {};
		for (var i = 0; i < style.length; i++) {
			var name = style[i];
			var value = style[name];
			taStyle[name] = value;
		}
		
		var div = $('<div>');
		div.css($.extend(taStyle, {
			height: 'auto'
		}));
		div.width($(this).width());
		div.text('$' + $(this).val() + '$');
		div.hide();
		$(document.body).append(div);
		
		$(this).height(Math.max(50, Math.min(div.height(), 150)));
		
		div.remove();
		
	}
	
	$(document).on('input change paste focusin focusout focus blur', 'textarea', function (evt) {
		resize.call(this);
	});
	
	$('textarea').each(function () {
		resize.call(this);
	});
	
});