/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
uiModule('dragndrop', function () {
	
	var dragging = null;
	
	function getBeforeElement(container, x, y) {
		var children = container.children(':not(.o-dnd-dragging)');
		var rows = [];
		var lastRow = null;
		children.each(function () {
			var offset = $(this).offset();
			var square = {
				x1: offset.left,
				y1: offset.top,
				x2: offset.left + $(this).width() - 1,
				y2: offset.top + $(this).height() - 1,
				height: $(this).height(),
				width: $(this).width()
			};
			
			if (lastRow != null && square.y1 < lastRow.maxY && square.y2 > lastRow.minY) {
				lastRow.squares.push(square);
				lastRow.minY = Math.min(lastRow.minY, square.y1);
				lastRow.maxY = Math.max(lastRow.maxY, square.y2);
			}
			else {
				lastRow = {
					minY: square.y1,
					maxY: square.y2,
					squares: [square]
				};
				rows.push(lastRow);
			}
		});
		var index = 0;
		for (var i = 0; i < rows.length; i++) {
			if (y <= rows[i].maxY) {
				var rowHeight = rows[i].maxY - rows[i].minY;
				for (var j = 0; j < rows[i].squares.length; j++) {
					var h = rows[i].squares[j].height;
					var w = rows[i].squares[j].width;
					if (y - rows[i].squares[j].y1 < h - h/w*(x-rows[i].squares[j].x1)) {
						return $(children[index + j]);
					}
				}
				break;
			}
			else {
				index += rows[i].squares.length;
			}
		}
		
		return null;
	}
	
	function move(evt) {
		
		if (!dragging) {
			return;
		}
		
		dragging.css({
			top: evt.clientY - dragging.height() / 2 + 'px',
			left: evt.clientX - dragging.width() / 2 + 'px',
		});
		
		$('.o-dnd-hover').removeClass('o-dnd-hover');
		
		var hCntr = null;
		
		var hover = $(':hover');
		for (var i = hover.length - 1; i >= 0; i--) {
			if ($(hover[i]).data('o-dnd-target')) {
				$(hover[i]).addClass('o-dnd-hover');
				hCntr = $(hover[i]);
				break;
			}
		}
		
		if (hCntr != null) {
			var alreadyPresent = hCntr.children('.o-dnd-dragging');
			var ref = getBeforeElement(hCntr, evt.clientX, evt.clientY);
			var el = $(dragging[0].cloneNode(true));
			if (alreadyPresent.length == 0) {
				if (ref == null) {
					hCntr.append(el);
				}
				else {
					el.insertBefore(ref);
				}
			}
			else {
				if (ref == null) {
					if (!alreadyPresent.is(':last')) {
						alreadyPresent.remove();
						hCntr.append(el);
					}
				}
				else {
					var next = alreadyPresent.next();
					if (next.length != 1 || next[0] != ref[0]) {
						alreadyPresent.remove();
						el.insertBefore(ref);
					}
				}
			}
			
			el.attr('style', '');
			el.css({
				opacity: 0.75
			});
			
		}
		
	}
	
	function leave(evt) {
		
		if (dragging) {
			
			var hover = $(':hover');
			for (var i = hover.length - 1; i >= 0; i--) {
				if ($(hover[i]).data('o-dnd-target')) {
					var groups = $(hover[i]).data('o-dnd-target');
					var found = false;
					ext:
					for (var j = 0; j < groups.length; j++) {
						if (groups[j] === '*') {
							found = true;
							break;
						}
						for (var k = 0; k < dragging.data('drag-group').length; k++) {
							if (groups[j] == dragging.data('drag-group')[k]) {
								found = true;
								break ext;
							}
						}
					}
					if (found) {
						var before = getBeforeElement($(hover[i]), evt.clientX, evt.clientY);
						$(hover[i]).find('.o-dnd-dragging').remove();
						$(hover[i]).trigger({
							type: 'dnd:drop',
							dragged: dragging.data('source'),
							before: before != null && before.length > 0 ? before[0] : null
						});
					}
					break;
				}
			}

			dragging.remove();
			dragging = null;
			$('.o-dnd-hover').removeClass('o-dnd-hover');
			
		}
		
	}
	
	$(document).ready(function () {
		$(document).on('mousemove', move);
		$(document).on('mouseup', leave);
	});
	
	function drag(evt, dragGroup) {
		
		dragging = $(this.cloneNode(true));
		dragging.data('source', this);
		dragging.data('drag-group', dragGroup);
		dragging.data('sourceContainer', $(this).parent());
		dragging.addClass('o-dnd-dragging');
		
		$(document.body).append(dragging);
		
		dragging.css({
			position: 'absolute',
			top: evt.clientY - dragging.height() / 2 + 'px',
			left: evt.clientX - dragging.width() / 2 + 'px',
			'pointer-events': 'none',
			opacity: 0.75
		});
		
	}
	
	function draggableContainer() {
		
		var groups = ($(this).data('drag-group') || '*').split(/\s*,\s*/)
		
		$(this).on('mousedown', '> *', function (evt) {
			
			evt.preventDefault();
			
			drag.call(this, evt, groups);
			
		});
		
	}
	
	function draggable () {
		
		$(this).on('mousedown', function (evt) {
			
			evt.preventDefault();
			
			drag.call(this, evt, ($(this).data('drag-group') || '*').split(/\s*,\s*/));
			
		});
		
	}
	
	function droppable () {
		
		$(this).data('o-dnd-target', ($(this).data('drag-group') || '*').split(/\s*,\s*/));
		
		$(this).mouseleave(function (evt) {
			$(this).find('.o-dnd-dragging').remove();
		});
		
	}
	
	return {
		draggable: draggable,
		droppable: droppable,
		draggableContainer: draggableContainer
	}
	
});