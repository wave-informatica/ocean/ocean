/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
uiModule('mimeTypes', function () {
	
	var mimeTypesIcons = {
		'image': '<i class="fa fa-file-image-o"></i>',
		'video': '<i class="fa fa-file-video-o"></i>',
		'audio': '<i class="fa fa-file-audio-o"></i>',
		'text': '<i class="fa fa-file-text-o"></i>',
		'application': {
			/* PDF */
			'pdf': '<i class="fa fa-file-pdf-o"></i>',
			/* Word */
			'msword': '<i class="fa fa-file-word-o"></i>',
			'vnd.openxmlformats-officedocument.wordprocessingml.document': '<i class="fa fa-file-word-o"></i>',
			'vnd.openxmlformats-officedocument.wordprocessingml.template': '<i class="fa fa-file-word-o"></i>',
			'vnd.ms-word.document.macroEnabled.12': '<i class="fa fa-file-word-o"></i>',
			'vnd.ms-word.template.macroEnabled.12': '<i class="fa fa-file-word-o"></i>',
			/* Excel */
			'vnd.ms-excel': '<i class="fa fa-file-excel-o"></i>',
			'vnd.openxmlformats-officedocument.spreadsheetml.sheet': '<i class="fa fa-file-excel-o"></i>',
			'vnd.openxmlformats-officedocument.spreadsheetml.template': '<i class="fa fa-file-excel-o"></i>',
			'vnd.ms-excel.sheet.macroEnabled.12': '<i class="fa fa-file-excel-o"></i>',
			'vnd.ms-excel.template.macroEnabled.12': '<i class="fa fa-file-excel-o"></i>',
			'vnd.ms-excel.addin.macroEnabled.12': '<i class="fa fa-file-excel-o"></i>',
			'vnd.ms-excel.sheet.binary.macroEnabled.12': '<i class="fa fa-file-excel-o"></i>',
			/* Powerpoint */
			'vnd.ms-powerpoint': '<i class="fa fa-file-powerpoint-o"></i>',
			'vnd.openxmlformats-officedocument.presentationml.presentation': '<i class="fa fa-file-powerpoint-o"></i>',
			'vnd.openxmlformats-officedocument.presentationml.template': '<i class="fa fa-file-powerpoint-o"></i>',
			'vnd.openxmlformats-officedocument.presentationml.slideshow': '<i class="fa fa-file-powerpoint-o"></i>',
			'vnd.ms-powerpoint.addin.macroEnabled.12': '<i class="fa fa-file-powerpoint-o"></i>',
			'vnd.ms-powerpoint.presentation.macroEnabled.12': '<i class="fa fa-file-powerpoint-o"></i>',
			'vnd.ms-powerpoint.template.macroEnabled.12': '<i class="fa fa-file-powerpoint-o"></i>',
			'vnd.ms-powerpoint.slideshow.macroEnabled.12': '<i class="fa fa-file-powerpoint-o"></i>',
			/* Access */
			'vnd.ms-access': '<i class="fa fa-file-archive-o"></i>',
			/* Compressed archives */
			'x-rar-compressed': '<i class="fa fa-file-archive-o"></i>',
			'zip': '<i class="fa fa-file-archive-o"></i>',
			'x-zip-compressed': '<i class="fa fa-file-archive-o"></i>',
			'x-java-archive': '<i class="fa fa-file-archive-o"></i>',
			/* Unknown */
			'unknown': '<i class="fa fa-file-o"></i>'
		}
	};
	
	function getIcon(type) {
		type = type.split('/');
		var genType = mimeTypesIcons[type[0]];
		if (typeof genType === 'string') {
			return genType;
		}
		else if (typeof genType === 'object') {
			var spec = genType[type[1]];
			if (typeof spec === 'string') {
				return spec;
			}
			else {
				return '<i class="fa fa-file-o"></i>';
			}
		}
		else {
			return '<i class="fa fa-file-o"></i>';
		}
	}
	
	return {
		getIcon: getIcon
	};
	
});