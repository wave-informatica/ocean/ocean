/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.tests.css;

import com.waveinformatica.ocean.core.html.CSSRule;
import com.waveinformatica.ocean.core.html.CSSStyleSheet;
import com.waveinformatica.ocean.core.html.PDFUtils;
import com.waveinformatica.ocean.core.tests.base.AbstractTestBase;
import java.io.IOException;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Ivano
 */
public class FontsManagementTest extends AbstractTestBase {
	
	@Test
	public void test() throws IOException {
		
		String source = readResource("/html/CSSUtils/fonts.css");
		
		if (StringUtils.isBlank(source)) {
			throw new IOException("Unable to read fonts.css resource");
		}
		
		CSSStyleSheet styleSheet = new CSSStyleSheet();
		styleSheet.parse(source);
		
		PDFUtils.prepareCSS(styleSheet, null, null);
		
		Assert.assertTrue("@font-face not correctly adapted for PDF", styleSheet.toString().contains("src:url('monopol_thin-webfont.ttf');-fs-pdf-font-embed:embed;-fs-pdf-font-encoding:Identity-H}"));
		
	}
	
}
