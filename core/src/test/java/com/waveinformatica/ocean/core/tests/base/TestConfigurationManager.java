/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.tests.base;

import com.waveinformatica.ocean.core.DefaultConfigurationBean;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.PersistedProperties;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Specializes;
import org.junit.Assert;

/**
 *
 * @author Ivano
 */
@Default
@Specializes
public class TestConfigurationManager extends DefaultConfigurationBean {
	
	private static ThreadLocal<String> namespace = new ThreadLocal<String>();
	
	@Override
	public PersistedProperties getLoggingProperties() {
		if (loggingProperties == null) {
			loggingProperties = loadProperties("logging.properties");
		}
		return loggingProperties;
	}

	@Override
	public PersistedProperties getSmtpProperties() {
		if (smtpProperties == null) {
			smtpProperties = loadProperties("smtp.properties");
		}
		return smtpProperties;
	}

	@Override
	public PersistedProperties getCustomProperties(String name) {
		PersistedProperties props = customConfigurations.get(name);
		if (props == null) {
			props = loadProperties("custom_" + name.replaceAll("[^a-zA-Z0-9]+", "-") + ".properties");
			customConfigurations.put(name, props);
		}
		return props;
	}

	@Override
	public PersistedProperties getSiteProperties() {
		if (siteProperties == null) {
			siteProperties = loadProperties("site.properties");
		}
		return siteProperties;
	}

	@Override
	public PersistedProperties getPersistenceProperties() {
		if (persistenceProperties == null) {
			persistenceProperties = loadProperties("persistence.properties");
		}
		return persistenceProperties;
	}
	
	private PersistedProperties loadProperties(String resource) {
		String ns = namespace.get();
		if (ns == null) {
			ns = "";
		}
		if (!ns.endsWith("/")) {
			ns += "/";
		}
		if (resource.startsWith("/")) {
			resource = resource.substring(1);
		}
		if (!ns.startsWith("/")) {
			ns = "/" + ns;
		}
		MockPersistedProperties props = new MockPersistedProperties(ns + resource);
		props.load();
		return props;
	}
	
	public static void setNamespace(String value) {
		namespace.set(value);
	}
	
	public static class MockPersistedProperties extends PersistedProperties {

		private final String resource;
		
		public MockPersistedProperties(String resource) {
			super(null);
			this.resource = resource;
		}

		@Override
		public synchronized void load() {
			
			InputStream is = TestConfigurationManager.class.getClassLoader().getResourceAsStream(resource);
			if (is == null) {
				LoggerUtils.getLogger(TestConfigurationManager.class).log(Level.WARNING, "Unable to find persisted properties \"" + resource + "\"");
			}
			else {

				try {
					load(is);
				} catch (IOException ex) {
					throw new RuntimeException(ex);
				}
				
			}
			
		}
		
	}
	
}
