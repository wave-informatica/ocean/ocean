/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.tests.results;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;

import com.waveinformatica.ocean.core.controllers.dto.OperationContext;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.core.controllers.results.input.Range;
import com.waveinformatica.ocean.core.tests.base.AbstractTestCdiBase;
import com.waveinformatica.ocean.core.util.DecoratorHelper;

public class TableResultTest extends AbstractTestCdiBase {
	
	@Test
	public void paginatedDataSource() throws IOException {
		
		TableResult result = factory.newInstance(TableResult.class);
		
		final List<Model> models = new ArrayList<TableResultTest.Model>();
		for (int i = 0; i < 40; i++) {
			models.add(new Model(i));
		}
		
		result.setTableData(Model.class, new TableResult.PaginationCount() {
			@Override
			public Long getCount() {
				return (long) models.size();
			}
		}, new TableResult.PaginationData<Model>() {
			@Override
			public Collection<Model> getData(Range<Long> bound) {
				return new ArrayList<>(models.subList(bound.getFrom().intValue(), Math.min(models.size(), bound.getTo().intValue() + 1)));
			}
		}, null);
		
		result.setPage(1);
		
		assertTrue(result.getTotalRowsCount().equals((long) models.size()));
		
		int lastPage = 0;
		do {
			
			lastPage = result.getPage();
			
		} while (result.nextPage());
		
		assertTrue(result.getPagesCount().equals(lastPage));
		
		OperationContext opCtx = new OperationContext();
		
		File outputFile = new File("target/TableResultTest.paginatedDataSource.xlsx");
		if (!outputFile.getParentFile().exists()) {
			outputFile.getParentFile().mkdirs();
		}
		
		FileOutputStream out = new FileOutputStream(outputFile);
		
		try {
			
			DecoratorHelper helper = factory.newInstance(DecoratorHelper.class);
			helper.decorateResult(result, out, opCtx, MimeTypes.EXCEL);
			
		} finally {
			if (out != null) {
				out.close();
			}
		}
		
	}
	
	public static class Model {
		
		private Integer field;

		public Model(Integer field) {
			super();
			this.field = field;
		}

		public Integer getField() {
			return field;
		}

		public void setField(Integer field) {
			this.field = field;
		}
		
	}
	
}
