/*
* Copyright 2016 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.core.tests.base;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

/**
 *
 * @author Ivano
 */
public class WeldContext {
	
	public static final WeldContext INSTANCE = new WeldContext();
	
	private final Weld weld;
	private final WeldContainer container;
	
	private WeldContext() {
		this.weld = new Weld();
		this.container = weld.initialize();
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				try {
					weld.shutdown();
				} catch (IllegalStateException e) {
					
				}
			}
		});
	}
	
	public <T> T getBean(Class<T> type) {
		return container.instance().select(type).get();
	}
	
}
