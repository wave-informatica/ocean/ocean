/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.tests.json;

import com.waveinformatica.ocean.core.util.JsonUtils;
import java.util.Date;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Ivano
 */
public class JsonUtilsTest {
	
	@Test
	public void jsonWithNullDates() {
		
		// CHECKS: TICKET-5080 JsonDecorator fallisce con date nulle
		
		JsonTest test = new JsonTest();
		
		String json = JsonUtils.getBuilder().create().toJson(test);
		
		Assert.assertEquals("{}", json);
		
	}
	
	public static class JsonTest {
		
		private String str;
		private Date dt;

		public String getStr() {
			return str;
		}

		public void setStr(String str) {
			this.str = str;
		}

		public Date getDt() {
			return dt;
		}

		public void setDt(Date dt) {
			this.dt = dt;
		}
		
	}
	
}
