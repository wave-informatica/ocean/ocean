/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.tests.base;

import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.Constants;
import com.waveinformatica.ocean.core.FrameworkConfiguration;
import com.waveinformatica.ocean.core.cdi.CDIHelper;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.InstanceListener;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.exceptions.ModuleException;
import com.waveinformatica.ocean.core.modules.ModulesManager;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.PersistedProperties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

/**
 *
 * @author Ivano
 */
@RunWith(WeldJUnit4Runner.class)
public class AbstractTestCdiBase extends AbstractTestBase {
	
	@Inject
	protected ModulesManager modulesManager;
	
	@Inject
	protected CoreController coreController;
	
	@Inject
	protected Configuration configuration;
	
	@Inject
	protected Application application;
	
	@Inject
	protected ObjectFactory factory;
	
	@BeforeClass
	public static void initBase() {
		
		WeldContext.INSTANCE.getBean(Application.class).getNamespace();
		
		Application.getInstance().init(null, "test");
		
		FrameworkConfiguration fwConfig = new FrameworkConfiguration();
	
		Logger logger = LoggerUtils.getCoreLogger();
		
		logger.info("Ocean Framework initializing for testing...");
		
		Application.getInstance().getConfiguration().initPackageConfiguration(null);
		
		Context.init((ServletContext) null);
		Context.setCoreController(Application.getInstance().getCoreController());
		
		// Core logger configuration
		PersistedProperties loggingProps = Application.getInstance().getConfiguration().getLoggingProperties();
		String loggerLevel = loggingProps.getProperty("core.level");
		if (loggerLevel != null) {
			LoggerUtils.getCoreLogger().setLevel(Level.parse(loggerLevel));
		}
		else {
			LoggerUtils.getCoreLogger().setLevel(Level.INFO);
		}
		
		// Application loading
		try {
			
			Application.getInstance().getModulesManager().loadCore(fwConfig);
			
//			modulesManager.loadModules();
			
		} catch (Throwable e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, "Error initializing Ocean Framework", e);
		}
		
	}
	
	@AfterClass
	public static void finishAll() {
		
		Context.init((ServletContext) null);
		Context.setCoreController(Application.getInstance().getCoreController());
		
		try {
			
			Application.getInstance().getModulesManager().unloadModules();
			
			Application.getInstance().getCoreController().unloadModuleClasses(Constants.CORE_MODULE_NAME);
			
		} catch (ModuleException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, null, e);
		}
		
	}
	
	@Before
	public void init() {
		
		CDIHelper.startCdiContexts(null, (HttpSession) null);
		
	}
	
	@After
	public void finish() throws ModuleException {
		
		CDIHelper.stopCdiContexts();
		InstanceListener.get().clear();
		
	}
	
}
