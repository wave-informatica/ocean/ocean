/*
 * Copyright 2017, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.tests.pdf;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.swing.Java2DRenderer;
import org.xhtmlrenderer.util.FSImageWriter;

import com.itextpdf.text.DocumentException;
import com.waveinformatica.ocean.core.controllers.decorators.WebPageDecorator;
import com.waveinformatica.ocean.core.html.PDFUtils;
import com.waveinformatica.ocean.core.html.XHTMLUtils;
import com.waveinformatica.ocean.core.internal.OceanUserAgentCallback;
import com.waveinformatica.ocean.core.tests.base.AbstractTestCdiBase;
import com.waveinformatica.ocean.core.util.Context;

/**
 *
 * @author Ivano
 */
public class PdfBaseTest extends AbstractTestCdiBase {
	
	@Test
	public void html() throws IOException {
		
		Context.init();
		
		Document xmlDoc = buildDoc();
		
		writeOutput("html", XHTMLUtils.toHTML(xmlDoc).getBytes(Charset.forName("UTF-8")));
		
	}
	
	@Test
	public void pdf() throws IOException {
		
		Context.init();
		
		Document xmlDoc = buildDoc();
		
		ITextRenderer renderer = new ITextRenderer();
		
		// Setting the UserAgent for retrieving resources
		renderer.getSharedContext().setUserAgentCallback(new OceanUserAgentCallback(renderer));

		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		renderer.setDocument(xmlDoc, XHTMLUtils.getBase(xmlDoc, "pdf/"));
		renderer.layout();
		try {
			renderer.createPDF(bytes);
		}
		catch (DocumentException ex) {
			Logger.getLogger(WebPageDecorator.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		writeOutput("pdf", bytes.toByteArray());
		
	}
	
	@Test
	public void jpeg() throws IOException {
		
		System.setProperty("java.awt.headless", "true");
		
		Context.init();
		
		Document xmlDoc = buildDoc();
		
		int width = 1024;
		int height = 768;
		
		Java2DRenderer renderer = new Java2DRenderer(xmlDoc, width, height);
		
		// Setting the UserAgent for retrieving resources
		renderer.getSharedContext().setUserAgentCallback(new OceanUserAgentCallback(renderer));

		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		
		BufferedImage img = renderer.getImage();
		
		
		FSImageWriter imageWriter = FSImageWriter.newJpegWriter(0.8f);
		imageWriter.write(img, bytes);
		
		writeOutput("jpg", bytes.toByteArray());
		
	}
	
	@Test
	public void png() throws IOException {
		
		System.setProperty("java.awt.headless", "true");
		
		Context.init();
		
		Document xmlDoc = buildDoc();
		
		int width = 1024;
		int height = 768;
		
		Java2DRenderer renderer = new Java2DRenderer(xmlDoc, width, height);
		
		// Setting the UserAgent for retrieving resources
		renderer.getSharedContext().setUserAgentCallback(new OceanUserAgentCallback(renderer));

		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		
		BufferedImage img = renderer.getImage();
		
		FSImageWriter imageWriter = new FSImageWriter();
		imageWriter.write(img, bytes);
		
		writeOutput("png", bytes.toByteArray());
		
	}
	
	private Document buildDoc() throws IOException {
		
		String source = readResource("/pdf/test.tpl");
		
		if (StringUtils.isBlank(source)) {
			throw new IOException("Unable to read test.tpl resource");
		}
		
		Document xmlDoc = XHTMLUtils.toDocument(source);
		PDFUtils.prepareDocument(xmlDoc);
		
		return xmlDoc;
		
	}
	
	private void writeOutput(String ext, byte[] bytes) throws IOException {
		
		File file = new File("target/PdfBaseTest." + ext);
		if (!file.getParentFile().exists()) {
			file.getParentFile().mkdirs();
		}
		if (!file.exists()) {
			file.createNewFile();
		}
		FileOutputStream fos = new FileOutputStream(file);
		fos.write(bytes);
		fos.close();
		
	}
	
}
