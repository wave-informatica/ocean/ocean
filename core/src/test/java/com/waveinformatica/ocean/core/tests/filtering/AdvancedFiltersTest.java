package com.waveinformatica.ocean.core.tests.filtering;

import java.lang.reflect.Field;

import javax.inject.Inject;

import org.junit.Test;

import com.waveinformatica.ocean.core.annotations.Filterable;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.filtering.FilterBuilder;
import com.waveinformatica.ocean.core.filtering.FilterProvider;
import com.waveinformatica.ocean.core.filtering.QueryFilter;
import com.waveinformatica.ocean.core.filtering.StringFilter;
import com.waveinformatica.ocean.core.filtering.drivers.SQLBuilderDriver;
import com.waveinformatica.ocean.core.filtering.drivers.ValuesManager;
import com.waveinformatica.ocean.core.tests.base.AbstractTestCdiBase;

public class AdvancedFiltersTest extends AbstractTestCdiBase {
	
	@Inject
	private ObjectFactory factory;
	
	@Test
	public void genericTest() {
		
		ValuesManager values = new ValuesManager();
		SQLBuilderDriver driver = new SQLBuilderDriver(values);
		FilterBuilder builder = factory.newInstance(FilterBuilder.class);
		builder.init(TestEntity.class);
		QueryFilter<String> filter = builder.getFilter(new TestFilterProvider(), driver, values);
		String query = "select * from entity" + SQLBuilderDriver.getCondition(" where ", filter);
		
	}
	
	public static class TestEntity {
		
		@Filterable(filter = StringFilter.class)
		private String strValue;

		public String getStrValue() {
			return strValue;
		}

		public void setStrValue(String value) {
			this.strValue = value;
		}
		
	}
	
	public static class TestFilterProvider implements FilterProvider {

		@Override
		public String getFilterValue(Field field) {
			return "test query";
		}
		
	}

}
