/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.tests.html;

import com.waveinformatica.ocean.core.html.CSSStyleSheet;
import com.waveinformatica.ocean.core.html.CSSUtils;
import com.waveinformatica.ocean.core.tests.base.AbstractTestBase;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Ivano
 */
public class CSSUtilsTest extends AbstractTestBase {
	
	@Test
	public void removeComments() throws IOException {
		
		String source = readResource("/html/CSSUtils/removeComments.css");
		
		String dest = CSSUtils.removeComments(source);
		
		Assert.assertFalse(dest.contains("/*"));
		
	}
	
	@Test
	public void removeComments2() throws IOException {
		
		String source = readResource("/html/CSSUtils/removeComments.css");
		
		CSSStyleSheet styleSheet = CSSUtils.buildStyleSheet(source);
		
		String dest = styleSheet.toString();
		
		Assert.assertFalse(dest.contains("/*"));
		
	}
	
	@Test
	public void rescopeCss() throws IOException {
		
		String source = readResource("/html/CSSUtils/removeComments.css");
		
		CSSStyleSheet styleSheet = CSSUtils.buildStyleSheet(source);
		
		CSSUtils.rescopeSelectors(styleSheet, ".prefixed");
		
		String dest = styleSheet.toString();
		
		Assert.assertFalse(dest.contains("/*"));
		Assert.assertFalse(dest.contains("body"));
		Assert.assertTrue(dest.contains(".prefixed{"));
		Assert.assertTrue(dest.contains("}.prefixed p{"));
		
	}
	
}
