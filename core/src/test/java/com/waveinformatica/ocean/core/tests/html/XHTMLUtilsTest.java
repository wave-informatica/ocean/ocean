/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.tests.html;

import com.waveinformatica.ocean.core.html.XHTMLUtils;
import com.waveinformatica.ocean.core.tests.base.AbstractTestBase;
import java.io.IOException;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;

/**
 *
 * @author Ivano
 */
public class XHTMLUtilsTest extends AbstractTestBase {
	
	@Test
	public void test1() throws IOException {
		
		String html = readResource("/html/XHTMLUtils/test1.html");
		
		Document doc = XHTMLUtils.toDocument(html);
		
		String result = XHTMLUtils.toHTML(doc);
		
		Assert.assertFalse(result.contains("classe1 classe2"));
		Assert.assertTrue(result.contains("<span></span>"));
		
		
	}
	
}
