/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.tests.templates;

import com.waveinformatica.ocean.core.templates.TemplatesManager;
import com.waveinformatica.ocean.core.tests.base.AbstractTestCdiBase;
import freemarker.template.Configuration;
import javax.inject.Inject;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Ivano
 */
public class CustomDirectivesTest extends AbstractTestCdiBase {
	
	@Inject
	private TemplatesManager templatesManager;
	
	@Test
	public void head() {
		
		Configuration config = templatesManager.getConfig();
		
		Assert.assertTrue(config.getSharedVariableNames().contains("head"));
		Assert.assertTrue(config.getSharedVariableNames().contains("i18n"));
		Assert.assertTrue(config.getSharedVariableNames().contains("authorize"));
		Assert.assertTrue(config.getSharedVariableNames().contains("invoke"));
		
	}
	
}
