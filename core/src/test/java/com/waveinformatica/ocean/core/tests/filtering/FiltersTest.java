/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.tests.filtering;

import org.junit.Test;

import com.waveinformatica.ocean.core.filtering.NumericFilter;
import com.waveinformatica.ocean.core.tests.base.AbstractTestBase;

/**
 *
 * @author Ivano
 */
public class FiltersTest extends AbstractTestBase {
	
	@Test
	public void numericFilterOld() {
		
		NumericFilter filter = new NumericFilter();
		
		System.out.println(filter.getJpqlFilter("x.number", "10..20 =4,30 50..70 45.. ..60", Long.class).getCondition());
		
	}
	
}
