package com.waveinformatica.ocean.core.tests.webservice;

import javax.inject.Inject;

import org.junit.Test;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.decorators.WebServiceResponseDecorator;
import com.waveinformatica.ocean.core.controllers.results.WebServiceResult;
import com.waveinformatica.ocean.core.controllers.scopes.AdminScopeProvider.AdminRootScope;
import com.waveinformatica.ocean.core.controllers.webservice.WebServiceInvocationScope;
import com.waveinformatica.ocean.core.tests.base.AbstractTestCdiBase;

public class WebServiceTests extends AbstractTestCdiBase {

	@Inject
	private ObjectFactory factory;
	
	@Inject
	private CoreController controller;
	
	@Test
	public void testResponse() {
		
		WebServiceResult result = factory.newInstance(WebServiceResult.class);
		
		WebServiceInvocationScope scope = factory.newInstance(WebServiceInvocationScope.class);
		scope.setOperationInfo(controller.getOperationInfo(AdminRootScope.class, "/admin/variables"));
		
		result.setOperationInfo(scope.getOperationInfo());
		result.setScope(scope);
		
	}
	
	/*public static class MockWebServiceScope extends AbstractScope implements WebServiceScope {
		
	}*/
	
	public static class ResponseDecoratorMock extends WebServiceResponseDecorator {
		
		@Override
		protected String getNamespace(WebServiceResult result) {
			return "http://localhost:8080/admin/";
		}
		
	}

}
