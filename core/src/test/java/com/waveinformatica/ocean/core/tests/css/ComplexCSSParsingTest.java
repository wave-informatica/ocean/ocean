/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.tests.css;

import com.waveinformatica.ocean.core.html.CSSRule;
import com.waveinformatica.ocean.core.html.CSSStyleSheet;
import com.waveinformatica.ocean.core.tests.base.AbstractTestCdiBase;
import java.io.IOException;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

/**
 *
 * @author Ivano
 */
public class ComplexCSSParsingTest extends AbstractTestCdiBase {
	
	public static void main (String[] args) throws IOException {
		new ComplexCSSParsingTest().testBootstrap();
	}
	
	@Test
	public void test1() throws IOException {
		
		String source = readResource("/html/CSSUtils/css-test-1.css");
		
		if (StringUtils.isBlank(source)) {
			throw new IOException("Unable to read bootstrap.min.css resource");
		}
		
		CSSStyleSheet styleSheet = new CSSStyleSheet();
		styleSheet.parse("/html/CSSUtils/", source);
		
	}
	
	@Test
	public void test2() throws IOException {
		
		String source = readResource("/html/CSSUtils/css-test-2.css");
		
		if (StringUtils.isBlank(source)) {
			throw new IOException("Unable to read bootstrap.min.css resource");
		}
		
		CSSStyleSheet styleSheet = new CSSStyleSheet();
		styleSheet.parse("/html/CSSUtils/", source);
		
	}
	
	@Test
	public void testBootstrap() throws IOException {
		
		System.out.println("*** COMPLEX CSS PARSE TEST ***");
		
		String source = readResource("/html/CSSUtils/bootstrap.min.css");
		
		if (StringUtils.isBlank(source)) {
			throw new IOException("Unable to read bootstrap.min.css resource");
		}
		
		CSSStyleSheet styleSheet = new CSSStyleSheet();
		styleSheet.parse("/html/CSSUtils/", source);
		
		System.out.println(styleSheet.getRules().size() + " total rule blocks");
		
		int mediaQueries = 0;
		
		for (CSSRule r : styleSheet.getRules()) {
			if (r.getMediaQuery() != null && !r.getMediaQuery().isEmpty()) {
				mediaQueries++;
			}
		}
		
		System.out.println(mediaQueries + " rules under media query found");
		
	}
	
	@Test
	public void testImport() throws IOException {
		
		System.out.println("*** COMPLEX CSS IMPORT PARSE TEST ***");
		
		String source = readResource("/html/CSSUtils/import.css");
		
		if (StringUtils.isBlank(source)) {
			throw new IOException("Unable to read import.css resource");
		}
		
		CSSStyleSheet styleSheet = new CSSStyleSheet();
		styleSheet.parse(source);
		
		System.out.println(styleSheet.getRules().size() + " total rule blocks");
		
		int mediaQueries = 0;
		
		for (CSSRule r : styleSheet.getRules()) {
			if (r.getMediaQuery() != null && !r.getMediaQuery().isEmpty()) {
				mediaQueries++;
			}
		}
		
		System.out.println(mediaQueries + " rules under media query found");
		
	}
	
	@Test
	public void toggleSwitch() throws IOException {
		
		String source = readResource("/html/CSSUtils/toggle-switch.css");
		
		if (StringUtils.isBlank(source)) {
			throw new IOException("Unable to read bootstrap.min.css resource");
		}
		
		CSSStyleSheet styleSheet = new CSSStyleSheet();
		styleSheet.parse("/html/CSSUtils/", source);
		
	}
	
}
