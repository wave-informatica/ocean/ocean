package com.waveinformatica.ocean.core.tests.core;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.StringWriter;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.waveinformatica.ocean.core.templates.TemplatesManager;
import com.waveinformatica.ocean.core.tests.base.AbstractTestCdiBase;
import com.waveinformatica.ocean.core.util.I18N;

import freemarker.template.Template;
import freemarker.template.TemplateException;

public class I18NTest extends AbstractTestCdiBase {
	
	@Inject
	private I18N i18n;
	
	@Inject
	private TemplatesManager templates;
	
	@Test
	public void i18n() {
		
		String result = i18n.translate("name");
		assertTrue(StringUtils.isNotBlank(result));
		
	}
	
	@Test
	public void template() throws TemplateException, IOException {
		
		StringWriter result = new StringWriter();
		
		Template tpl = templates.getTemplate("/i18n/i18nDirective.tpl");
		tpl.process(new Object(), result);
		
		assertTrue(StringUtils.isNotBlank(result.toString()));
	}
	
}
