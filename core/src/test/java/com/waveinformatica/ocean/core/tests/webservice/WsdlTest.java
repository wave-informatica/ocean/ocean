package com.waveinformatica.ocean.core.tests.webservice;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.inject.Inject;
import javax.wsdl.Definition;
import javax.wsdl.Fault;
import javax.wsdl.Message;
import javax.wsdl.Operation;
import javax.wsdl.Part;
import javax.wsdl.PortType;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLWriter;
import javax.xml.namespace.QName;

import org.junit.Test;

import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.scopes.AdminScopeProvider.AdminRootScope;
import com.waveinformatica.ocean.core.controllers.webservice.WsdlBuilder;
import com.waveinformatica.ocean.core.tests.base.AbstractTestCdiBase;

public class WsdlTest extends AbstractTestCdiBase {

	@Inject
	private ObjectFactory factory;

	@Test
	public void testOnAdminRootScope() {
		
		try {
			WsdlBuilder builder = factory.newInstance(WsdlBuilder.class);
	
			AdminRootScope scope = new AdminRootScope();
	
			Definition def = builder.build(scope, "http://localhost:8080/admin/", "Admin");
			
			WSDLWriter writer = WSDLFactory.newInstance().newWSDLWriter();
			File wsdl = new File("target/admin.wsdl");
			if (!wsdl.getParentFile().exists()) {
				wsdl.getParentFile().mkdirs();
			}
			try (FileOutputStream out = new FileOutputStream(wsdl)) {
				writer.writeWSDL(def, out);
			}
			
			validate(def);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void validate(Definition def) {

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// VALIDATION: https://stackoverflow.com/questions/19899681/api-to-validate-wsdl-files-using-java //
		////////////////////////////////////////////////////////////////////////////////////////////////////

		// This is a Bag of all Messages in the WSDL that are used in some <wsdl:fault> of any Operation of any PortType
		Set faultMessages = new HashSet();
		Map allPortTypes = def.getPortTypes();
		Iterator portTypeIt = allPortTypes.entrySet().iterator();
		while (portTypeIt.hasNext()) {
			Map.Entry entry = (Entry) portTypeIt.next();
			PortType portType = (PortType) entry.getValue();
			List allOperations = portType.getOperations();
			Iterator listIt = allOperations.iterator();
			while (listIt.hasNext()) {
				Operation operation = (Operation)listIt.next();
				Iterator faultIt = operation.getFaults().values().iterator();
				while (faultIt.hasNext()) {
					Fault fault = (Fault) faultIt.next();    
					faultMessages.add(fault.getMessage());                    
				}
			}
		}

		Map allMessages = def.getMessages();
		Iterator messageIt = allMessages.entrySet().iterator();
		while (messageIt.hasNext()) {
			Map.Entry entry = (Entry) messageIt.next();
			QName messageNameQName = (QName) entry.getKey();
			String messageName = messageNameQName.getLocalPart();

			Message message = (Message) entry.getValue();
			Map parts = message.getParts();
			assertTrue("wsdl:message has more than one part: " + messageNameQName.toString(), parts.size() == 1);
			Part messagePart = (Part) parts.values().iterator().next();
			assertTrue("wsdl:part should not have a 'type' attribute: " + messagePart.getName(), messagePart.getTypeName() == null);

			// Only for Messages that are used in Fault:
			if (faultMessages.contains(message)) {
				assertTrue("Due to an Axis1 bug, please do NOT use the same name for <wsdl:message name=\"" + messageName + "\"> and <xsd:element name=\"" + messagePart.getElementName().getLocalPart()+"\">",
						!messagePart.getElementName().getLocalPart().equals(messageName));
			}
		}

	}

}
