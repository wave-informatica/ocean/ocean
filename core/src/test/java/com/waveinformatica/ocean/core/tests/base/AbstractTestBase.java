/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.core.tests.base;

import com.waveinformatica.ocean.core.tests.html.XHTMLUtilsTest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 *
 * @author Ivano
 */
public class AbstractTestBase {
	
	protected String readResource(String path) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream is = XHTMLUtilsTest.class.getResourceAsStream(path);
		byte[] buffer = new byte[4096];
		int read;
		while ((read = is.read(buffer)) > 0) {
			baos.write(buffer, 0, read);
		}
		is.close();
		
		return new String(baos.toByteArray(), Charset.forName("UTF-8"));
		
	}
	
}
