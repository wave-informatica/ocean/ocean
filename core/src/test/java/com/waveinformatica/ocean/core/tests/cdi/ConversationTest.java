/*******************************************************************************
 * Copyright 2015, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.core.tests.cdi;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.Test;

import com.waveinformatica.ocean.core.tests.base.AbstractTestCdiBase;
import com.waveinformatica.ocean.core.util.OceanConversation;

public class ConversationTest extends AbstractTestCdiBase {
	
	@Inject
	private OceanConversation conversation;
	
	@Test
	public void beanExists() {
		
		Assert.assertNotNull(conversation);
		
		conversation.put("test", "value");
		Assert.assertNotNull(conversation.getId());
		
	}
	
}
