package com.waveinformatica.ocean.core.tests.css;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;

import javax.xml.transform.TransformerException;

import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.waveinformatica.ocean.core.html.CSSUtils;
import com.waveinformatica.ocean.core.html.XHTMLUtils;
import com.waveinformatica.ocean.core.tests.base.AbstractTestCdiBase;

import se.fishtank.css.selectors.Selectors;
import se.fishtank.css.selectors.dom.W3CNode;


/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author Ivano
 */
public class CSSInliningTest extends AbstractTestCdiBase {

	@Test
	public void test() throws TransformerException, IOException {

		InputStream is = CSSInliningTest.class.getClassLoader().getResourceAsStream("mail-test.tpl");

		Document xmlDoc = XHTMLUtils.toDocument(new InputStreamReader(is, Charset.forName("UTF-8")));

		CSSUtils.inlineCss(xmlDoc);

		String result = XHTMLUtils.toHTML(xmlDoc);
		
		//System.err.println(result);

		Assert.assertFalse("The document still contains <style> elements", result.contains("<style"));
		Assert.assertTrue("Bad inlined CSS result", result.contains(" class=\"alert warning\" style=\"background-color:#808000;"));
		Assert.assertFalse("Bad inlined CSS merge", result.contains("background-color:#fcf8e3"));

	}

	@Test
	public void testMedia() throws TransformerException, IOException {

		String res = readResource("/html/CSSUtils/mail-test2.tpl");
		
		Document xmlDoc = XHTMLUtils.toDocument(res);

		CSSUtils.inlineCss(xmlDoc);
		
		//System.err.println(XHTMLUtils.toHTML(xmlDoc));

		Selectors selectors = new Selectors(new W3CNode(xmlDoc));

		List<Node> styles = selectors.querySelectorAll("style");

		Assert.assertTrue("The document should contain one <style> element, " + styles.size() + " found.", styles.size() == 1);
		if (styles.size() == 1) {
			Assert.assertTrue("The <style> element must be the first element in the <body> element.", ((Node) selectors.querySelector("body")).getFirstChild().getNodeName().equals("style"));
			Assert.assertTrue("Invalid media query style result", styles.get(0).getFirstChild().getNodeValue().equals("@media all and(min-width:800.0px){.alert{width:50%;margin:auto 0}}"));
		}

		/*
		String result = XHTMLUtils.toHTML(xmlDoc.getDocumentElement());

		System.out.println(result);
		 */
	}

}
