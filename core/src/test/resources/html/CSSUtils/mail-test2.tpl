<html>
    <head>
        <title></title>
        <style type="text/css" media="screen">
            .alert {
                background-color: #fcf8e3;
                color: #8a6d3b;
                border: 1px solid #faebcc;
                padding: 15px;
                margin: 20px;
                border-radius: 4px;
            }
            .warning {
                background-color: #808000;
            }
		@media all and (min-width: 800px) {
			.alert {
				width: 50%;
				margin: auto 0;
			}
		}
        </style>
        
    </head>
    <body>
        
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td><h1>AGT: Nuova richiesta da approvare</h1></td>
                <td style="text-align: center;"><img src="ris/images/fca.png" height="30"></td>
            </tr>
        </table>
        
        <p class="alert warning">
            L'approvatore della richiesta &egrave; stato cambiato ed è stata assegnata a lei. Potr&agrave; trovare maggiori dettagli
            cliccando su questo <a href="agt/request?id=${part.request.id?c}">link</a>.
        </p>
        
        <p>
            Buongiorno <strong>${destinatario.fullName}</strong>,<br>
            l'utente <strong>${mittente.fullName} (${mittente.userId?upper_case})</strong> ha inviato al sistema <strong>AGT</strong>
            in data ${part.request.creationDate?datetime?string("EEEE dd MMMM yyyy HH:mm")} una richiesta che le si chiede
            (in qualit&agrave; di approvatore) di valutare.
        </p>
        
        <p>
            Cliccando sul seguente <a href="agt/request?id=${part.request.id?c}">link</a>, potr&agrave; visualizzare la richiesta e
            decidere se approvarla o rigettarla.
        </p>
        
    </body>
</html>