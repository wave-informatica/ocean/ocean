[TOC]

# Quick start

In questa pagina vedremo come iniziare ad usare Ocean.

## Installazione

Innanzi tutto è bene, se si sta compilando dai sorgenti, fare il build dell'intero progetto, in modo da avere disponibili i binari di tutti i moduli, pronti da installare ed usare. Per fare questo, dalla directory principale di Ocean, lanciare il comando

`mvn clean install`

In questo modo sarà effettuata la compilazione del core di Ocean, di tutti i suoi moduli e componenti vari (archetypes, doclet, ecc.).