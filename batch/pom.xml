<!--
    Copyright 2015, 2018 Wave Informatica S.r.l..
   
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
   
         http://www.apache.org/licenses/LICENSE-2.0
     
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>com.waveinformatica</groupId>
		<artifactId>ocean</artifactId>
		<version>2.5.0-SNAPSHOT</version>
		<relativePath>../</relativePath>
	</parent>
	<groupId>com.waveinformatica.ocean</groupId>
	<artifactId>batch</artifactId>
	<packaging>jar</packaging>

	<name>${project.artifactId}-${project.version}</name>

	<organization>
		<name>Wave Informatica S.r.l.</name>
		<url>http://www.waveinformatica.com</url>
	</organization>

	<licenses>
		<license>
			<name>Apache License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
			<comments>A business-friendly OSS license</comments>
		</license>
	</licenses>

	<dependencies>
		<dependency>
			<groupId>com.waveinformatica.ocean</groupId>
			<artifactId>core</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>org.hibernate.javax.persistence</groupId>
			<artifactId>hibernate-jpa-2.1-api</artifactId>
			<version>1.0.0.Final</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>${project.groupId}</groupId>
			<artifactId>scheduler</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpclient</artifactId>
			<version>4.3.6</version>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-csv</artifactId>
			<version>1.4</version>
		</dependency>
		<dependency>
			<groupId>com.github.spullara.mustache.java</groupId>
			<artifactId>compiler</artifactId>
			<version>0.8.18</version>
		</dependency>
		<dependency>
			<groupId>javax</groupId>
			<artifactId>javaee-web-api</artifactId>
			<version>6.0</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>3.8.1</version>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>com.waveinformatica.ocean</groupId>
				<artifactId>ocean-maven-plugin</artifactId>
				<version>${project.version}</version>
				<configuration>
					<projectPackage>${project.groupId}.${project.artifactId}</projectPackage>
				</configuration>
				<executions>
					<execution>
						<id>ocean-module</id>
						<goals>
							<goal>ocean-module</goal>
						</goals>
						<phase>validate</phase>
					</execution>
					<execution>
						<id>ocean-r-generator</id>
						<goals>
							<goal>ocean-r-generator</goal>
						</goals>
						<phase>generate-sources</phase>
					</execution>
					<execution>
						<id>ocean-generator</id>
						<goals>
							<goal>ocean-generator</goal>
						</goals>
						<phase>process-classes</phase>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>2.3.2</version>
				<configuration>
					<source>1.6</source>
					<target>1.6</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-dependency-plugin</artifactId>
				<version>2.10</version>
				<executions>
					<execution>
						<id>copy</id>
						<phase>prepare-package</phase>
						<goals>
							<goal>unpack</goal>
						</goals>
						<configuration>
							<artifactItems>
								<artifactItem>
									<groupId>org.apache.httpcomponents</groupId>
									<artifactId>httpclient</artifactId>
									<version>4.3.6</version>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.build.directory}/classes</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>org.apache.httpcomponents</groupId>
									<artifactId>httpcore</artifactId>
									<version>4.3.3</version>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.build.directory}/classes</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>commons-logging</groupId>
									<artifactId>commons-logging</artifactId>
									<version>1.1.3</version>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.build.directory}/classes</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>org.apache.commons</groupId>
									<artifactId>commons-csv</artifactId>
									<version>1.4</version>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.build.directory}/classes</outputDirectory>
								</artifactItem>
								<artifactItem>
								    <groupId>com.google.guava</groupId>
								    <artifactId>guava</artifactId>
								    <version>16.0.1</version>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.build.directory}/classes</outputDirectory>
								</artifactItem>
								<artifactItem>
									<groupId>com.github.spullara.mustache.java</groupId>
									<artifactId>compiler</artifactId>
									<version>0.8.18</version>
									<type>jar</type>
									<overWrite>true</overWrite>
									<outputDirectory>${project.build.directory}/classes</outputDirectory>
								</artifactItem>
							</artifactItems>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>2.3.2</version>
				<configuration>
					<finalName>${project.groupId}.${project.artifactId}</finalName>
					<archive>
						<manifestEntries>
							<Maven-GroupId>${project.groupId}</Maven-GroupId>
							<Maven-ArtifactId>${project.artifactId}</Maven-ArtifactId>
							<Module-Version>${project.version}</Module-Version>
							<Module-Build>${maven.build.timestamp}</Module-Build>
							<Dependencies>com.waveinformatica.ocean.scheduler</Dependencies>
						</manifestEntries>
					</archive>
				</configuration>
			</plugin>
		</plugins>
	</build>
</project>