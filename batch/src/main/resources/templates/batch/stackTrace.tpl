<@results.wrap>

<pre>
    <#list data as el>
    ${el.className}.${el.methodName!}<#if el.nativeMethod>[native]</#if>(${el.fileName!"<unknown>"}:<#if el.lineNumber??>${el.lineNumber?c}<#else>??</#if>)<br>
    </#list>
</pre>

</@results.wrap>