<div class="form-group">
    <label for="fld_download_filename" class="col-lg-2 control-label">File Name</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="fileName" id="fld_download_filename" value="${fileName!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>