<div class="form-group">
    <label for="fld_stream_variable" class="col-lg-2 control-label">Stream variable</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="sourceVar" id="fld_stream_variable" value="${sourceVar!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>
            
<div class="form-group">
    <label for="fld_dest_path" class="col-lg-2 control-label">File path</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="filePath" id="fld_dest_path" value="${filePath!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
        <span class="help-block">Tip: Character "&#126;" will be replaced with workspace location if any, while "$" will be replaced with Ocean's configuration dir.</span>
    </div>
</div>