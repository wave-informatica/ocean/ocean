<div class="form-group">
    <label for="fld_unzip_variable" class="col-lg-2 control-label">Source variable</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="sourceVariable" id="fld_unzip_variable" value="${sourceVariable!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>
            
<div class="form-group">
    <label for="fld_unzip_extract_in_folder" class="col-lg-2 control-label">Extract in folder</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="extractInFolder" id="fld_unzip_extract_in_folder" value="${extractInFolder!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>