<div class="form-group">
    <label for="fld_stream_variable" class="col-lg-2 control-label">Stream variable</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="sourceVar" id="fld_stream_variable" value="${sourceVar!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>
            
<div class="form-group">
    <label for="fld_dest_path" class="col-lg-2 control-label">File path</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="filePath" id="fld_dest_path" value="${filePath!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
        <span class="help-block">Tip: Character "&#126;" will be replaced with workspace location if any, while "$" will be replaced with Ocean's configuration dir.</span>
    </div>
</div>
            
<div class="form-group">
    <label for="fld_stop_if" class="col-lg-2 control-label">Stop If</label>
    <div class="col-lg-10">
        <div class="input-group">
            <select name="stopIf" id="fld_stop_if" class="form-control">
                <#list stopIfNames as name>
                <option value="${name}" <#if stopIf?has_content && name == stopIf>selected</#if>><@i18n key="com.waveinformatica.ocean.batch.steps.StopIf."+name /></option>
                </#list>
            </select>
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>
            
<div class="form-group">
    <label for="fld_file_check_arg" class="col-lg-2 control-label">"Stop-If" argument</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="stopIfArgument" id="fld_file_check_arg" value="${stopIfArgument!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>