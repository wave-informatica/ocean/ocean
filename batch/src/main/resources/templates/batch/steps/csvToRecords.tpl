<div class="form-group">
    <label for="fld_csv_read_input_variable" class="col-lg-2 control-label">Input variable</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="inputVariable" id="fld_csv_read_input_variable" value="${(inputVariable?html)!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>
            
<div class="form-group">
    <label for="fld_csv_read_format" class="col-lg-2 control-label">Character encoding</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="format" id="fld_csv_read_format" value="${(format?html)!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>
            
<div class="form-group">
    <label for="fld_csv_read_delimiter" class="col-lg-2 control-label">Delimiter</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="delimiter" id="fld_csv_read_delimiter" value="${(delimiter?html)!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>
            
<div class="form-group">
    <label for="fld_csv_read_quote" class="col-lg-2 control-label">Quote</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="quote" id="fld_csv_read_quote" value="${(quote?html)!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>
            
<div class="form-group">
    <div class="col-lg-10 col-lg-offset-2">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="firstRowAsHeader" value="true" <#if firstRowAsHeader>checked</#if>>
                First record as header
            </label>
        </div>
    </div>
</div>
            
<div class="form-group">
    <label for="fld_csv_read_output_variable" class="col-lg-2 control-label">Output variable</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="outputVariable" id="fld_csv_read_output_variable" value="${(outputVariable?html)!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>
            
<div class="form-group">
    <label for="fld_csv_extra_header" class="col-lg-2 control-label">Extra header expression</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="extraHeader" id="fld_csv_extra_header" value="${(extraHeader?html)!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>
                  
<div class="form-group">
    <label for="fld_csv_extra_footer" class="col-lg-2 control-label">Extra footer expression</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="extraFooter" id="fld_csv_extra_footer" value="${(extraFooter?html)!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>
            
<div class="form-group">
    <div class="col-lg-10 col-lg-offset-2">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="forceExtraBounds" value="true" <#if forceExtraBounds>checked</#if>>
                Force extra header/footer
            </label>
        </div>
    </div>
</div>