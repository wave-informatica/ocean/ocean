<div class="form-group">
    <label for="fld_httprequest_url" class="col-lg-2 control-label">URL</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="url" id="fld_httprequest_url" value="${url!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>
            
<div class="form-group">
    <label for="fld_httprequest_auth_domain" class="col-lg-2 control-label">Domain</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="domain" id="fld_httprequest_auth_domain" value="${domain!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>
            
<div class="form-group">
    <label for="fld_httprequest_auth_user_name" class="col-lg-2 control-label">User Name</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="userName" id="fld_httprequest_auth_user_name" value="${userName!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="fld_httprequest_auth_password" class="col-lg-2 control-label">Password</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="password" id="fld_httprequest_auth_password" value="${password!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="fld_httprequest_output_variable" class="col-lg-2 control-label">Output Variable</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="outputVariable" id="fld_httprequest_output_variable" value="${outputVariable!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>
