<div class="form-group">
    <label for="fld_decorate_result_variable" class="col-lg-2 control-label">Result variable</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="sourceVar" id="fld_decorate_result_variable" value="${sourceVar!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>
            
<div class="form-group">
    <label for="fld_decorate_dest_variable" class="col-lg-2 control-label">Destination variable</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="destVar" id="fld_decorate_dest_variable" value="${destVar!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>
            
<div class="form-group">
    <label for="fld_decorate_output_type" class="col-lg-2 control-label">Output type</label>
    <div class="col-lg-10">
        <div class="input-group">
            <select name="typeName" id="fld_decorate_output_type" class="form-control">
                <option value=""></option>
                <#list mimeTypes as type>
                <option value="${type}" <#if type == typeName!>selected</#if>>${type}</option>
                </#list>
            </select>
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>