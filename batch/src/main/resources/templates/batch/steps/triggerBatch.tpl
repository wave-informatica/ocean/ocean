<div class="form-group">
    <label for="fld_batch" class="col-lg-2 control-label">Batch</label>
    <div class="col-lg-10">
        <select name="batchId" id="fld_batch" class="form-control">
            <option value=""></option>
            <#list availableBatches as batch>
            <option value="${batch.id}"<#if batchId?? && batch.id == batchId> selected="selected"</#if>>${(batch.title!)?html}</option>
            </#list>
        </select>
    </div>
</div>