<div class="form-group">
    <label for="fld_operation" class="col-lg-2 control-label">Operation</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="operation" id="fld_operation" value="${operation!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>
            
<div class="form-group">
    <label for="fld_operation" class="col-lg-2 control-label">Result variable</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input type="text" name="resultVar" id="fld_operation" value="${resultVar!}" class="form-control">
            <span class="input-group-addon"><a href="#"><span class="glyphicon glyphicon-question-sign"></span></a></span>
        </div>
    </div>
</div>