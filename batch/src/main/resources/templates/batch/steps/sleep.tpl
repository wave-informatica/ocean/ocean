<div class="form-group">
    <label class="col-lg-2 control-label">Millisecondi</label>
    <div class="col-lg-10">
	    <input type="text" name="milliseconds" value="<#if milliseconds??>${milliseconds?c}</#if>" class="form-control">
    </div>
</div>

<div class="form-group">
    <div class="col-lg-10 col-lg-offset-2">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="manual" value="true" <#if manual?? && manual>checked</#if>>
                Wait in manual runs too
            </label>
        </div>
    </div>
</div>