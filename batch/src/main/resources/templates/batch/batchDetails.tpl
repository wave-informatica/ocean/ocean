<@results.wrap>

<table class="table table-bordered table-striped table-condensed">
	<thead>
		<th><@i18n key="com.waveinformatica.ocean.batch.entities.BatchInstance.id" /></th>
		<th><@i18n key="com.waveinformatica.ocean.batch.entities.BatchInstance.startDate" /></th>
		<th><@i18n key="com.waveinformatica.ocean.batch.entities.BatchInstance.launchType" /></th>
		<th><@i18n key="com.waveinformatica.ocean.batch.entities.BatchInstance.launcherUserId" /></th>
		<th><@i18n key="com.waveinformatica.ocean.batch.entities.BatchInstance.exitCode" /></th>
		<th><@i18n key="com.waveinformatica.ocean.batch.entities.BatchInstance.endDate" /></th>
	</thead>
	<tbody>
		<td>${(extras["instance"].id?c)!}</td>
		<td>${(extras["instance"].startDate?datetime?string("dd/MM/yyyy HH:mm"))!}</td>
		<td>${(extras["instance"].launchType)!}</td>
		<td>${(extras["instance"].launcherUserId?c)!}</td>
		<td>${(extras["instance"].exitCode?c)!}</td>
		<td>${(extras["instance"].endDate?datetime?string("dd/MM/yyyy HH:mm"))!}</td>
	</tbody>
</table>

<div class="btn-toolbar">
	<#if extras["workspace"]?has_content>
	<div class="btn-group">
		<a class="btn btn-default" href="batch/downloadWorkspace?id=${extras["instance"].id?c}"><i class="fa fa-download"></i> Workspace</a>
	</div>
	</#if>
	<div class="btn-group">
		<a class="btn btn-default" href="batch/stackTrace?id=${extras["instance"].id?c}"><i class="fa fa-bolt"></i> Stacktrace</a>
		<a class="btn btn-danger" href="batch/kill?id=${extras["instance"].id?c}"><i class="fa fa-power-off"></i> Kill</a>
	</div>
</div>

<#list data as step>

<#if !step.exitCode??>
    <div class="alert alert-info">
<#elseif step.exitCode == 0>
    <div class="alert alert-success">
<#else>
    <div class="alert alert-danger">
</#if>
    
<#if step.exitCode??>
    <button class="close" type="button">
        <span class="glyphicon glyphicon-collapse-down"></span>
    </button>
</#if>
    
    <p style="margin-bottom: 5px;"><strong>${step.startDate?datetime?string("dd/MM/yyyy HH:mm:ss.SSS")}</strong> [${step.group}] ${step.name}</p>
    
    <#if !step.exitCode?? && extras["currentStepProcess"]??>
    <div class="progress">
	<div class="progress-bar" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: ${extras["currentStepProcess"].stepExecution.completionPercentage?string["0"]}%;">
	  ${extras["currentStepProcess"].stepExecution.completionPercentage?string["0"]}%
	</div>
    </div>
    <div class="exception">
        ${extras["currentStepProcess"].stepLogHandler.log!}
    </div>
    </#if>
    
    <#if step.exitCode??>
    <div class="exception hidden">
        ${step.log!}
    </div>
    </#if>
    
</div>

</#list>

<script type="text/javascript">
    $(document).ready(function () {
        $('.alert button.close').click(function () {
            var btn = $(this).children('span');
            btn.toggleClass('glyphicon-collapse-down');
            btn.toggleClass('glyphicon-collapse-up');
            $(this).closest('.alert').find('div.exception').toggleClass('hidden');
        });
    });
    
</script>

</@results.wrap>