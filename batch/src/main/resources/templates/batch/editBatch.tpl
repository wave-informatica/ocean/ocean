<@results.wrap>

<form action="batch/saveBatch" method="post" enctype="multipart/form-data" class="form-horizontal">
    <fieldset>
        <div class="form-group">
            <label for="title" class="col-lg-2 control-label">Nome del batch</label>
            <div class="col-lg-10">
                <input type="text" name="title" id="title" value="${data.title!}" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-lg-2 control-label">Descrizione</label>
            <div class="col-lg-10">
                <textarea name="title" id="description" rows="5" class="form-control">${data.description!?html}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="executor" class="col-lg-2 control-label">Executor</label>
            <div class="col-lg-10">
                <select name="executor" id="executor" class="form-control">
                    <option value=""></option>
                    <#list data.availableExecutors as executor>
                    <option value="${executor.name}"<#if executor.name == data.executorName!> selected</#if>>${executor.name}</option>
                    </#list>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="maxExecutions" class="col-lg-2 control-label">Esec. concorrenti</label>
            <div class="col-lg-10">
                <input type="number" name="maxExecutions" id="maxExecutions" class="form-control" value="<#if data.maxExecutions??>${data.maxExecutions?c}<#else>0</#if>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Mantieni X esecuzioni</label>
            <div class="col-md-4">
                <label for="maxLogs">Totale</label>
                <input type="number" placeholder="Totale" name="maxLogs" id="maxLogs" class="form-control" value="${data.maxLogs!}">
            </div>
            <div class="col-md-3">
                <label for="maxSuccessLogs">Max OK (exit code 0)</label>
                <input type="number" placeholder="Successo" name="maxSuccessLogs" id="maxSuccessLogs" class="form-control" value="${data.maxSuccessLogs!}">
            </div>
            <div class="col-md-3">
                <label for="maxFailedLogs">Max KO</label>
                <input type="number" placeholder="Falliti" name="maxFailedLogs" id="maxFailedLogs" class="form-control" value="${data.maxFailedLogs!}">
            </div>
        </div>
    </fieldset>
    <table class="table table-bordered table-condensed" id="parameters-table">
	    <caption>Parameters</caption>
	    <thead>
	        <tr>
	            <th>Name</th>
	            <th>Default value</th>
	            <th><button type="button" class="btn btn-primary btn-xs btn-block" title="Add" id="btn-add-parameter"><i class="fa fa-plus"></i></button></th>
	        </tr>
	    </thead>
	    <tbody>
	    	<#if data.batch??>
	    	<#list data.batch.parameters as param>
	    	<tr>
		        <td><input type="text" class="form-control" name="parameter-name" value="${(param.name!)?html}"></td>
		        <td><input type="text" class="form-control" name="parameter-default" value="${(param.defaultValue!)?html}"></td>
		        <td><button type="button" class="btn btn-danger btn-sm btn-block btn-delete-parameter" title="Remove"><i class="fa fa-trash-o"></i></button></td>
		    </tr>
		    </#list>
		    </#if>
	    </tbody>
	</table>
    <div id="steps-container">
        <#if (data.batch)??>
        <#list data.batch.steps as step>
        <#if !step.validUntil??>
        <fieldset>
            <div class="edit-step panel panel-default" data-steptype="${step.name}" data-id="${step.id?c}">
                <div class="panel-heading step-title">
                    <span class="glyphicon glyphicon-sort sort-handle"></span>
                    ${step.title}
                </div>
                <div class="panel-body step-config">
                    <#list data.stepTypes as type>
                    <#if type.type == step.name>
                    ${step.template!}
                    </#if>
                    </#list>
                </div>
                <div class="panel-footer step-footer">
                    <a href="#" class="btn btn-danger btn-sm btn-delete">Delete</a>
                </div>
            </div>
        </fieldset>
        </#if>
        </#list>
        </#if>
    </div>
    <fieldset>
        <div class="form-group">
            <div class="col-lg-12">
                <div class="dropup">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Add step <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <#list data.stepTypes as type>
                        <li><a href="#" class="step-btn" data-step-type="${type.type}"><@i18n key=type.title!type.type /></a></li>
                        </#list>
                    </ul>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <input type="reset" value="Reset" class="btn btn-default">
                <input type="submit" value="Invia" class="btn btn-success" id="save-batch">
            </div>
        </div>
    </fieldset>
</form>

<script id="step-tpl" type="x-tmpl-mustache">
    <fieldset>
        <div class="edit-step panel panel-default" data-steptype="{{stepType}}" data-id="{{id}}">
            <div class="panel-heading step-title">
                <span class="glyphicon glyphicon-sort sort-handle"></span>
                {{title}}
            </div>
            <div class="panel-body step-config">
            </div>
            <div class="panel-footer step-footer">
                <a href="#" class="btn btn-danger btn-sm btn-delete">Delete</a>
            </div>
        </div>
    </fieldset>
</script>

<script id="parameter-tpl" type="x-tmpl-mustache">
    <tr>
        <td><input type="text" class="form-control" name="parameter-name"></td>
        <td><input type="text" class="form-control" name="parameter-default"></td>
        <td><button type="button" class="btn btn-danger btn-sm btn-block btn-delete-parameter" title="Remove"><i class="fa fa-trash-o"></i></button></td>
    </tr>
</script>

<script type="text/javascript">
    $(document).ready(function () {
        stepTpl = $('#step-tpl').html();
        Mustache.parse(stepTpl);
        
        $(document.body).append($('<div class="ajax-spinner"><div class="background"></div><div class="whirly-loader">Loading...</div></div>'));
        $(document).bind("ajaxSend", function () {
            $(".ajax-spinner").show();
        }).bind("ajaxComplete", function () {
            $(".ajax-spinner").hide();
        });
        $('.ajax-spinner').hide();
        
        $('#steps-container').sortable({
            handle: '.sort-handle',
            cursor: 'move'
        });
        
        $(document).on('click', '.btn-delete-parameter', function () {
        	$(this).closest('tr').remove();
        });
        
        $('#btn-add-parameter').click(function () {
        	$(this).closest('table').find('tbody').append($($('#parameter-tpl').html()));
        });
        
        $('.btn-delete').click(deleteStepHandler);
        
        $('.step-btn').click(function (evt) {
            
            evt.preventDefault();
            
            var me = $(this);
            
            var params = {
                stepType: me.data('step-type')
            };
            
            $.get('batch/getStepEdit', params, function (data, textStatus, jqXHR){
                
                data.stepType = params.stepType;
                var rendered = $(Mustache.render(stepTpl, data));
                rendered.find('.step-config').html(data.template);
                rendered.find('.btn-delete').click(deleteStepHandler);
                $('#steps-container').append(rendered);
                
            }, 'json');
            
        });
        
        $('#save-batch').click(function (evt) {
            evt.preventDefault();
            
            var steps = [];
            var parameters = [];
            
            $('#parameters-table').find('tbody tr').each(function () {
            	var name = $(this).find('[name="parameter-name"]').val();
            	var defaultValue = $(this).find('[name="parameter-default"]').val();
            	parameters.push({
            		name: name,
            		defaultValue: defaultValue
            	});
            });
            
            $('#steps-container div.edit-step').each(function () {
                var id = parseInt($(this).data('id'));
                if (id === NaN) {
                    id = null;
                }
                var config = {
                    stepType: $(this).data('steptype'),
                    id: id,
                    fields: {}
                };
                var me = $(this);
                me.find('input, select, textarea').each(function() {
                    var field = $(this);
                    if (field.attr('name')) {
                        if (field.attr('type') === 'checkbox' || field.attr('type') === 'radio') {
                            config.fields[field.attr('name')] = field.prop('checked');
                        }
                        else {
                            config.fields[field.attr('name')] = field.val();
                        }
                    }
                });
                config.fields = JSON.stringify(config.fields);
                steps.push(config);
            });
            
            var params = {
                title: $('#title').val(),
                description: $('#description').val(),
                executor: $('#executor').val(),
                maxExecutions: $('[name="maxExecutions"]').val(),
                clearWorkspace: $('[name="clearWorkspace"]').prop('checked'),
                maxLogs: $('[name="maxLogs"]').val(),
                maxSuccessLogs: $('[name="maxSuccessLogs"]').val(),
                maxFailedLogs: $('[name="maxFailedLogs"]').val(),
                batchId: <#if data.id??>${data.id}<#else>null</#if>,
                parameters: parameters,
                steps: steps
            };
            
            params = {
                definition: JSON.stringify(params)
            };
            
            $.post('batch/saveBatch', params, function (data, textStatus, jqXHR) {
                if (data.success) {
                    location.href = $('base').attr('href') + 'batch/';
                }
            }, 'json');
        });
    });
    
    function deleteStepHandler(evt) {
        
        evt.preventDefault();
        
        $(this).closest('fieldset').remove();
        
    }
</script>

</@results.wrap>