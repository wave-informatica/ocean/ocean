/*
 * Copyright 2017, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.batch.entities;

import com.waveinformatica.ocean.core.controllers.results.TableResult;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @group entities
 * @author Ivano
 */
@Entity
@Table(name = "batch_tokens")
public class BatchToken implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_batch_tokens")
	@SequenceGenerator(name = "seq_batch_tokens", sequenceName = "seq_batch_tokens", allocationSize = 1)
	@TableResult.TableResultColumn(key = true, hidden = true)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "batch_id")
	private BatchDefinition batch;
	
	@Column
	private String description;
	
	@Column
	private String token;
	
	@Transient
	private String url;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BatchDefinition getBatch() {
		return batch;
	}

	public void setBatch(BatchDefinition batch) {
		this.batch = batch;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}
