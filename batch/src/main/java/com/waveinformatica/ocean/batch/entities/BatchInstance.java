/*
* Copyright 2015, 2018 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.batch.entities;

import com.waveinformatica.ocean.core.annotations.Filterable;
import com.waveinformatica.ocean.core.filtering.DayFilter;
import com.waveinformatica.ocean.core.filtering.NumericFilter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @group entities
 * @author Ivano
 */
@Entity
@Table(name = "batch_instance")
public class BatchInstance implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_batch_instance")
	@SequenceGenerator(name = "seq_batch_instance", sequenceName = "seq_batch_instance", allocationSize = 1)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "batch_id")
	private BatchDefinition batch;
	
	@Column(name = "start_date")
	@Temporal(TemporalType.TIMESTAMP)
	@Filterable(filter = DayFilter.class)
	private Date startDate;
	
	@Column(name = "launch_type")
	@Enumerated(EnumType.STRING)
	private LaunchType launchType;
	
	@Column(name = "launcher_user_id")
	private Long launcherUserId;
	
	@Column(name = "exit_code")
	@Filterable(filter = NumericFilter.class)
	private Integer exitCode;
	
	@Column(name = "end_date")
	@Temporal(TemporalType.TIMESTAMP)
	@Filterable(filter = DayFilter.class)
	private Date endDate;
	
	@OneToMany(mappedBy = "batchInstance", cascade = CascadeType.ALL)
	private List<BatchInstanceParam> parameters;
	
	@OneToMany(mappedBy = "batchInstance", cascade = CascadeType.REMOVE)
	private List<StepExecutionInstance> stepInstances;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public BatchDefinition getBatch() {
		return batch;
	}
	
	public void setBatch(BatchDefinition batch) {
		this.batch = batch;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public LaunchType getLaunchType() {
		return launchType;
	}
	
	public void setLaunchType(LaunchType launchType) {
		this.launchType = launchType;
	}
	
	public Long getLauncherUserId() {
		return launcherUserId;
	}
	
	public void setLauncherUserId(Long launcherUserId) {
		this.launcherUserId = launcherUserId;
	}
	
	public Integer getExitCode() {
		return exitCode;
	}
	
	public void setExitCode(Integer exitCode) {
		this.exitCode = exitCode;
	}
	
	public Date getEndDate() {
		return endDate;
	}
	
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public List<BatchInstanceParam> getParameters() {
		return parameters;
	}

	public void setParameters(List<BatchInstanceParam> parameters) {
		this.parameters = parameters;
	}

	public List<StepExecutionInstance> getStepInstances() {
		return stepInstances;
	}
	
	public void setStepInstances(List<StepExecutionInstance> stepInstances) {
		this.stepInstances = stepInstances;
	}
	
}
