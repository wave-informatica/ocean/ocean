/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.ocean.batch;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.waveinformatica.ocean.batch.annotations.BatchExecutor;
import com.waveinformatica.ocean.batch.annotations.BatchStep;
import com.waveinformatica.ocean.batch.dto.BatchExecution;
import com.waveinformatica.ocean.batch.dto.ExecutorInfo;
import com.waveinformatica.ocean.batch.entities.BatchDefinition;
import com.waveinformatica.ocean.batch.entities.BatchInstance;
import com.waveinformatica.ocean.batch.entities.BatchInstanceParam;
import com.waveinformatica.ocean.batch.entities.LaunchType;
import com.waveinformatica.ocean.batch.entities.ParameterDefinition;
import com.waveinformatica.ocean.batch.entities.StepDefinition;
import com.waveinformatica.ocean.batch.interfaces.IBatchExecutor;
import com.waveinformatica.ocean.batch.interfaces.IBatchStep;
import com.waveinformatica.ocean.core.annotations.OceanPersistenceContext;
import com.waveinformatica.ocean.core.annotations.Service;
import com.waveinformatica.ocean.core.controllers.IService;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.ServiceStatus;
import com.waveinformatica.ocean.core.security.UserSession;
import com.waveinformatica.ocean.core.util.LoggerUtils;

/**
 *
 * @author Ivano
 */
@Service("Batch")
public class BatchService implements IService {
	
	private static Logger logger = LoggerUtils.getLogger(BatchService.class);
	
	@Inject
	private ObjectFactory factory;
	
	private Map<String, Set<Class<? extends IBatchStep>>> steps = new HashMap<String, Set<Class<? extends IBatchStep>>>();
	private Map<String, Set<Class<? extends IBatchExecutor>>> executors = new HashMap<String, Set<Class<? extends IBatchExecutor>>>();
	
	private Map<String, IBatchExecutor> executorsMap = new HashMap<String, IBatchExecutor>();
	
	@Override
	public boolean start() {
		return true;
	}
	
	@Override
	public boolean stop() {
		return true;
	}
	
	@Override
	public ServiceStatus getServiceStatus() {
		
		ServiceStatus status = new ServiceStatus();
		
		status.setManageOperation("batch/");
		status.setRunning(true);
		
		return status;
		
	}
	
	public void loadClasses(String module, Map<Class<? extends Annotation>, Set<Class<?>>> annotatedClasses) {
		
		loadSteps(module, annotatedClasses);
		loadExecutors(module, annotatedClasses);
		
	}
	
	private void loadSteps(String module, Map<Class<? extends Annotation>, Set<Class<?>>> annotatedClasses) {
		
		Set<Class<?>> classes = annotatedClasses.get(BatchStep.class);
		
		if (classes == null || classes.isEmpty()) {
			return;
		}
		
		if (!steps.containsKey(module)) {
			steps.put(module, new HashSet<Class<? extends IBatchStep>>());
		}
		
		for (Class cls : classes) {
			
			steps.get(module).add(cls);
			
			logger.info("Loaded batch step \"" + cls.getName() + "\" from module \"" + module + "\"");
			
		}
		
	}
	
	private void loadExecutors(String module, Map<Class<? extends Annotation>, Set<Class<?>>> annotatedClasses) {
		
		Set<Class<?>> classes = annotatedClasses.get(BatchExecutor.class);
		
		if (classes == null || classes.isEmpty()) {
			return;
		}
		
		if (!executors.containsKey(module)) {
			executors.put(module, new HashSet<Class<? extends IBatchExecutor>>());
		}
		
		for (Class cls : classes) {
			
			executors.get(module).add(cls);
			
			logger.info("Loaded batch executor \"" + cls.getName() + "\" from module \"" + module + "\"");
			
		}
		
	}
	
	public void unloadClasses(String module) {
		
		steps.remove(module);
		
	}
	
	public List<Class<? extends IBatchStep>> getAvailableSteps() {
		
		List<Class<? extends IBatchStep>> result = new ArrayList<Class<? extends IBatchStep>>();
		
		for (String k : steps.keySet()) {
			result.addAll(steps.get(k));
		}
		
		return result;
		
	}
	
	public List<ExecutorInfo> getAvailableExecutors() {
		
		List<ExecutorInfo> result = new ArrayList<ExecutorInfo>();
		
		for (String k : executors.keySet()) {
			for (Class<? extends IBatchExecutor> c : executors.get(k)) {
				result.add(new ExecutorInfo(c));
			}
		}
		
		return result;
		
	}
	
	public Class<? extends IBatchStep> getStepByType(String type) {
		for (String k : steps.keySet()) {
			Set<Class<? extends IBatchStep>> types = steps.get(k);
			for (Class<? extends IBatchStep> t : types) {
				if (t.getName().equals(type)) {
					return t;
				}
			}
		}
		return null;
	}
	
	public Long startBatch(BatchDefinition batchDef, LaunchType type, Map<String, String> parameters) throws BatchException {
		
		if (parameters == null) {
			parameters = new HashMap<String, String>();
		}
		
		if (batchDef.getParameters() != null) {
			for (ParameterDefinition pd : batchDef.getParameters()) {
				if (!parameters.containsKey(pd.getName())) {
					parameters.put(pd.getName(), pd.getDefaultValue());
				}
			}
		}
		
		String name = batchDef.getExecutorName();
		
		if (name == null || name.trim().length() == 0) {
			throw new BatchException("Batch executor not defined for batch \"" + batchDef.getTitle() + "\" (" + batchDef.getId().toString() + ")");
		}
		
		Beans beans = factory.newInstance(Beans.class);
		
		if (batchDef.getMaxExecutions() != null && batchDef.getMaxExecutions() > 0) {
			int size = beans.getEntityManager().createQuery("select x from BatchInstance x where x.batch = :batch and x.endDate is null", BatchInstance.class)
				  .setParameter("batch", batchDef)
				  .getResultList()
				  .size();
			if (size >= batchDef.getMaxExecutions()) {
				throw new BatchException("Already " + size + " not yet terminated batch executions");
			}
		}
		
		IBatchExecutor instance = getExecutor(batchDef.getExecutorName());
		
		if (instance == null) {
			throw new BatchException("Batch executor \"" + batchDef.getExecutorName() + "\" not available for batch \"" + batchDef.getTitle() + "\" (" + batchDef.getId().toString() + ")");
		}
		
		BatchInstance batchInstance = new BatchInstance();
		batchInstance.setBatch(batchDef);
		batchInstance.setLaunchType(type);
		if (beans.getUserSession().isLogged()) {
			batchInstance.setLauncherUserId(beans.getUserSession().getPrincipal().getId());
		}
		batchInstance.setStartDate(new Date());
		if (batchDef.getParameters() != null) {
			batchInstance.setParameters(new ArrayList<BatchInstanceParam>());
			for (ParameterDefinition pd : batchDef.getParameters()) {
				BatchInstanceParam param = new BatchInstanceParam();
				param.setBatchInstance(batchInstance);
				param.setName(pd.getName());
				param.setValue(parameters.get(pd.getName()));
				batchInstance.getParameters().add(param);
			}
		}
		
		try {
			
			beans.getEntityManager().getTransaction().begin();
			
			beans.getEntityManager().persist(batchInstance);
			
			beans.getEntityManager().getTransaction().commit();
			
		} catch (Exception e) {
			
			if (beans.getEntityManager().getTransaction().isActive()) {
				beans.getEntityManager().getTransaction().rollback();
			}
			
			throw new BatchException("Error storing batch instance info", e);
			
		}
		
		batchDef.setSteps(new ArrayList<StepDefinition>(batchDef.getSteps()));
		return instance.startBatch(new BatchExecution(batchDef, batchInstance, parameters));
	}
	
	public IBatchExecutor getExecutor(String name) {
		IBatchExecutor instance = executorsMap.get(name);
		if (instance == null) {
			ExecutorInfo executor = null;
			for (ExecutorInfo ei : getAvailableExecutors()) {
				if (ei.getName().equals(name)) {
					executor = ei;
					break;
				}
			}
			if (executor != null) {
				instance = factory.newInstance(executor.getExecutorClass());
				executorsMap.put(executor.getName(), instance);
			}
		}
		return instance;
	}
	
	public static class Beans {
		
		@Inject
		@OceanPersistenceContext
		private EntityManager entityManager;
		
		@Inject
		private UserSession userSession;
		
		public EntityManager getEntityManager() {
			return entityManager;
		}
		
		public UserSession getUserSession() {
			return userSession;
		}
		
	}
	
}
