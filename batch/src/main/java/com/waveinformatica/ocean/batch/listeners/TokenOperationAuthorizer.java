/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.batch.listeners;

import com.waveinformatica.ocean.batch.Operations;
import com.waveinformatica.ocean.batch.entities.BatchToken;
import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.annotations.OceanPersistenceContext;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.events.AuthorizeOperationEvent;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.Event;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import java.util.logging.Level;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
@CoreEventListener(eventNames = CoreEventName.AUTHORIZE_OPERATION)
public class TokenOperationAuthorizer implements IEventListener {
	
	@Inject
	@OceanPersistenceContext
	private EntityManager em;
	
	@Override
	public void performAction(Event event) {
		
		AuthorizeOperationEvent evt = (AuthorizeOperationEvent) event;
		
		if (evt.getOperationInfo().getCls() == Operations.class &&
			  "start".equals(evt.getOperationInfo().getOperation().value()) &&
			  Context.get() != null &&
			  Context.get().getRequest() != null) {
			
			String token = Context.get().getRequest().getParameter("token");
			
			if (StringUtils.isNotBlank(token)) {
				
				try {
					
					BatchToken batchToken = em.createQuery("select x from BatchToken x where x.token = :token", BatchToken.class)
						  .setParameter("token", token)
						  .getSingleResult();
					
					String idStr = Context.get().getRequest().getParameter("id");
					if (StringUtils.isNotBlank(idStr)) {
						try {
							Long batchId = Long.parseLong(idStr);
							if (!batchToken.getBatch().getId().equals(batchId)) {
								LoggerUtils.getLogger(TokenOperationAuthorizer.class).log(Level.SEVERE, "Requested token \"" + token + "\" not applicable for batch ID \"" + idStr + "\"");
							}
							else {
								evt.setAuthorized(true);
							}
						} catch (NumberFormatException e) {
							LoggerUtils.getLogger(TokenOperationAuthorizer.class).log(Level.SEVERE, "Requested token \"" + token + "\" not applicable for batch ID \"" + idStr + "\"");
						}
					}
					
				} catch (NoResultException e) {
					LoggerUtils.getLogger(TokenOperationAuthorizer.class).log(Level.SEVERE, "Requested token \"" + token + "\" not found");
				} catch (NonUniqueResultException e) {
					LoggerUtils.getLogger(TokenOperationAuthorizer.class).log(Level.SEVERE, "Requested token \"" + token + "\" is known for multiple batches");
				}
				
			}
			
		}
		
	}
	
}
