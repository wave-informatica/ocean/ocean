/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.batch.exceptions;

import com.waveinformatica.ocean.batch.BatchException;

/**
 *
 * @author Ivano
 */
public class CancelException extends BatchException {

	public CancelException() {
	}

	public CancelException(String message) {
		super(message);
	}

	public CancelException(String message, Throwable cause) {
		super(message, cause);
	}

	public CancelException(Throwable cause) {
		super(cause);
	}

	public CancelException(int exitCode) {
		super(exitCode);
	}

	public CancelException(int exitCode, String message) {
		super(exitCode, message);
	}

	public CancelException(int exitCode, String message, Throwable cause) {
		super(exitCode, message, cause);
	}

	public CancelException(int exitCode, Throwable cause) {
		super(exitCode, cause);
	}
	
}
