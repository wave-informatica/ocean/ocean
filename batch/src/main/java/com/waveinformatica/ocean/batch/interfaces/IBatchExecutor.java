/*
 * Copyright 2015, 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.batch.interfaces;

import com.waveinformatica.ocean.batch.dto.BatchExecution;
import com.waveinformatica.ocean.batch.dto.StepProcess;
import com.waveinformatica.ocean.batch.entities.BatchInstance;

/**
 *
 * @author Ivano
 */
public interface IBatchExecutor {

	public Long startBatch(BatchExecution batchExec);

	public void startedCallback(BatchInstance instance);

	public void terminatedCallback(BatchInstance instance);

	public boolean kill(BatchInstance instance);

	public StackTraceElement[] getStackTrace(BatchInstance instance);

	public StepProcess getCurrentStepProcess(BatchInstance instance);

}
