/*
 * Copyright 2015, 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.batch.dto;

import com.waveinformatica.ocean.batch.StepLogHandler;
import com.waveinformatica.ocean.batch.entities.StepDefinition;
import com.waveinformatica.ocean.batch.interfaces.IBatchStep;

/**
 *
 * @author Ivano
 */
public class StepProcess {
    
    private final BatchProcess batchProcess;
    private final StepExecution stepExecution;
    private final StepDefinition stepDefinition;
    private final IBatchStep stepInstance;
    private final StepLogHandler stepLogHandler;

    public StepProcess(BatchProcess batchProcess, StepExecution stepExecution, StepDefinition stepDefinition, IBatchStep stepInstance, StepLogHandler stepLogHandler) {
	this.batchProcess = batchProcess;
	this.stepExecution = stepExecution;
	this.stepDefinition = stepDefinition;
	this.stepInstance = stepInstance;
	this.stepLogHandler = stepLogHandler;
    }

    public BatchProcess getBatchProcess() {
	return batchProcess;
    }

    public StepExecution getStepExecution() {
	return stepExecution;
    }

    public StepDefinition getStepDefinition() {
	return stepDefinition;
    }

    public IBatchStep getStepInstance() {
	return stepInstance;
    }

    public StepLogHandler getStepLogHandler() {
	return stepLogHandler;
    }
    
}
