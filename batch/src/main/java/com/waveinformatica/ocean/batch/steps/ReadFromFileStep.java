/*
* Copyright 2016, 2018 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.batch.steps;

import com.waveinformatica.ocean.batch.BatchException;
import com.waveinformatica.ocean.batch.annotations.BatchStep;
import com.waveinformatica.ocean.batch.dto.BatchExecution;
import com.waveinformatica.ocean.batch.dto.StepExecution;
import com.waveinformatica.ocean.batch.exceptions.CancelException;
import com.waveinformatica.ocean.batch.interfaces.IBatchStep;
import com.waveinformatica.ocean.core.Configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.inject.Inject;

/**
 *
 * @author Ivano
 */
@BatchStep(group = "ocean", name = "readFromFile", title = "Read stream from file", editTpl = "/templates/batch/steps/readFromFile.tpl")
public class ReadFromFileStep implements IBatchStep {
	
	@Inject
	private Configuration config;
	
	@Override
	public void execute(BatchExecution execution, StepExecution configuration) throws BatchException {
		
		ConfigurationObject conf = (ConfigurationObject) configuration.getConfiguration();
		
		try {
			
			File inputFile = null;
			String path = execution.process(conf.getFilePath());
			
			if (path.startsWith("$")) {
				path = path.substring(1);
				if (path.startsWith("/")) {
					path = path.substring(1);
				}
				inputFile = new File(config.getConfigDir(), path);
			}
			if (path.startsWith("~")) {
				path = path.substring(1);
				if (execution.getWorkspace() != null) {
					if (path.startsWith("/")) {
						path = path.substring(1);
					}
					inputFile = new File(execution.getWorkspace().getBase(), path);
				}
			}
			if (inputFile == null) {
				inputFile = new File(path);
			}
			
			if (!inputFile.exists()) {
				throw new BatchException("No file found \"" + inputFile.getAbsolutePath() + "\".");
			}
			else if (conf.getStopIf() != null && conf.getStopIf() != StopIf.NOCHECK) {
				switch (conf.getStopIf()) {
					case OLDER_THAN_X_HOURS:
						double hours = Double.parseDouble(conf.getStopIfArgument());
						if (inputFile.lastModified() < new Date().getTime() - hours * 3600000.0) {
							throw new CancelException("Input file \"" + inputFile.getAbsolutePath() + "\" is older than " + hours + " hours");
						}
						break;
				}
			}
			
			FileInputStream fos = new FileInputStream(inputFile);
			
			byte[] buffer = new byte[(int) inputFile.length()];
			
			int read = fos.read(buffer);
			
			configuration.getLogger().log(Level.INFO, "Read " + read + " bytes from \"" + path + "\"");
			
			fos.close();
			
			execution.getDataModel().put(conf.getSourceVar(), buffer);
			
		} catch (FileNotFoundException ex) {
			throw new BatchException(ex);
		} catch (IOException ex) {
			throw new BatchException(ex);
		}
		
	}
	
	@Override
	public Class<? extends Serializable> getConfigurationObject() {
		return ConfigurationObject.class;
	}
	
	public static class ConfigurationObject implements Serializable {
		
		private String sourceVar;
		private String filePath;
		private StopIf stopIf;
		private String stopIfArgument;
		
		public String getSourceVar() {
			return sourceVar;
		}
		
		public void setSourceVar(String sourceVar) {
			this.sourceVar = sourceVar;
		}
		
		public String getFilePath() {
			return filePath;
		}
		
		public void setFilePath(String filePath) {
			this.filePath = filePath;
		}

		public StopIf getStopIf() {
			return stopIf;
		}

		public void setStopIf(StopIf stopIf) {
			this.stopIf = stopIf;
		}

		public String getStopIfArgument() {
			return stopIfArgument;
		}

		public void setStopIfArgument(String stopIfArgument) {
			this.stopIfArgument = stopIfArgument;
		}
		
		public List<String> getStopIfNames() {
			List<String> names = new ArrayList<String>();
			for (StopIf si : StopIf.values()) {
				names.add(si.name());
			}
			return names;
		}
	}
	
	public static enum StopIf {
		NOCHECK,
		OLDER_THAN_X_HOURS
	}
	
}
