/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.ocean.batch.entities;

import com.waveinformatica.ocean.core.controllers.results.TableResult;
import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * @group entities
 * @author Ivano
 */
@Entity
@Table(name = "batch")
public class BatchDefinition implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_batch")
	@SequenceGenerator(name = "seq_batch", sequenceName = "seq_batch", allocationSize = 1)
	@Column
	@TableResult.TableResultColumn(hidden = true)
	private Long id;
	
	@Column
	@TableResult.TableResultColumn(sortable = true)
	private String title;
	
	@Column
	@TableResult.TableResultColumn(ignore = true)
	private String description;
	
	@Column(name = "executor_name")
	private String executorName;
	
	@Column(name = "max_executions")
	@TableResult.TableResultColumn(hidden = true)
	private Integer maxExecutions;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "batch")
	@OrderBy("order")
	private List<StepDefinition> steps;
	
	@Column(name = "max_logs")
	@TableResult.TableResultColumn(hidden = true)
	private Integer maxLogs;
	
	@Column(name = "max_success_logs")
	@TableResult.TableResultColumn(hidden = true)
	private Integer maxSuccessLogs;
	
	@Column(name = "max_failed_logs")
	@TableResult.TableResultColumn(hidden = true)
	private Integer maxFailedLogs;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "batch")
	private List<ParameterDefinition> parameters;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public List<StepDefinition> getSteps() {
		return steps;
	}
	
	public void setSteps(List<StepDefinition> steps) {
		this.steps = steps;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getExecutorName() {
		return executorName;
	}
	
	public void setExecutorName(String executorName) {
		this.executorName = executorName;
	}

	public Integer getMaxExecutions() {
		return maxExecutions;
	}

	public void setMaxExecutions(Integer maxExecutions) {
		this.maxExecutions = maxExecutions;
	}

	public Integer getMaxLogs() {
		return maxLogs;
	}

	public void setMaxLogs(Integer maxLogs) {
		this.maxLogs = maxLogs;
	}

	public Integer getMaxSuccessLogs() {
		return maxSuccessLogs;
	}

	public void setMaxSuccessLogs(Integer maxSuccessLogs) {
		this.maxSuccessLogs = maxSuccessLogs;
	}

	public Integer getMaxFailedLogs() {
		return maxFailedLogs;
	}

	public void setMaxFailedLogs(Integer maxFailedLogs) {
		this.maxFailedLogs = maxFailedLogs;
	}

	public List<ParameterDefinition> getParameters() {
		return parameters;
	}

	public void setParameters(List<ParameterDefinition> parameters) {
		this.parameters = parameters;
	}
	
}
