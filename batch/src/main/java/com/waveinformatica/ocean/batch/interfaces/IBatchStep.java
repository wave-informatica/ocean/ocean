/*******************************************************************************
 * Copyright 2015, 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.ocean.batch.interfaces;

import com.waveinformatica.ocean.batch.BatchException;
import com.waveinformatica.ocean.batch.dto.BatchExecution;
import com.waveinformatica.ocean.batch.dto.StepExecution;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import java.io.Serializable;

/**
 * 
 * @param <T> the data model interface type
 * @param <C> the configuration object type
 *
 * @author Ivano
 */
public interface IBatchStep<T, C extends Serializable> {
    
    /**
     * Executes the step into the batch.
     * 
     * @param execution the batch scope
     * @param configuration the step scope (with cofiguration, etc.)
     * @throws com.waveinformatica.ocean.batch.BatchException throw a {@link BatchException} exception if an error occurs during the step's execution
     */
    public void execute(BatchExecution<T> execution, StepExecution<C> configuration) throws BatchException;
    
    /**
     * <p>If the step uses a configuration, return it through this method. It will be made available to
     * configuration templates. This method may return null if no configuration is needed.</p>
     * 
     * <p>The configuration object's class may need a {@link ObjectFactory} instance: in this situation the class
     * can implement the {@link ObjectFactoryAware} interface to receive a {@link ObjectFactory} instance
     * after instantiation.</p>
     * 
     * @return the step's configuration object
     */
    public Class<C> getConfigurationObject();
    
}
