/*******************************************************************************
 * Copyright 2015, 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.ocean.batch.dto;

import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.google.gson.Gson;
import com.waveinformatica.ocean.batch.entities.BatchDefinition;
import com.waveinformatica.ocean.batch.entities.BatchInstance;
import com.waveinformatica.ocean.core.persistence.DataSourceProxy;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;

/**
 * @param <T> the batch data model interface
 *
 * @author Ivano
 */
public class BatchExecution<T> {
	
	private final BatchDefinition batch;
	private final BatchInstance batchInstance;
	private final List<Step> steps = new ArrayList<Step>();
	private final Map<String, Object> dataModel = new HashMap<String, Object>();
	private final MustacheFactory mf = new DefaultMustacheFactory();
	private Workspace workspace;
	
	public BatchExecution(BatchDefinition batch, BatchInstance batchInstance, Map<String, String> parameters) {
		this.batch = batch;
		this.batchInstance = batchInstance;
		this.dataModel.putAll(parameters);
	}
	
	public BatchDefinition getBatch() {
		return batch;
	}
	
	public List<Step> getSteps() {
		return steps;
	}
	
	public Map<String, Object> getDataModel() {
		return dataModel;
	}
	
	public BatchInstance getBatchInstance() {
		return batchInstance;
	}
	
	public Workspace getWorkspace() {
		return workspace;
	}
	
	public void setWorkspace(Workspace workspace) {
		this.workspace = workspace;
	}
	
	public T getDataModelInterface(Class<T> iface) {
		
		return DataModelProxy.wrap(iface, dataModel, this);
		
	}
	
	public String process(String value) {
		
		if (value == null) {
			return null;
		}
		
		StringWriter writer = new StringWriter();
		
		Mustache mustache = mf.compile(new StringReader(value), "process");
		mustache.execute(writer, dataModel);
		
		return writer.toString();
		
	}
	
	public static class DataModelProxy<T> implements InvocationHandler {
		
		private static <T> T wrap(Class<T> iface, Map<String, Object> dm, BatchExecution exec) {
			
			DataModelProxy<T> dmp = new DataModelProxy<T>(iface, dm, exec);
			
			try {
				
				return (T) Proxy.getProxyClass(iface.getClassLoader(), iface).
					  getConstructor(new Class[] { InvocationHandler.class }).
					  newInstance(new Object[] { dmp });
				
			} catch (Exception ex) {
				LoggerUtils.getLogger(DataSourceProxy.class).log(Level.WARNING, "Unable to proxy data source instance", ex);
				return null;
			}
			
		}
		
		private final Class<T> cls;
		private final Map<String, Object> dm;
		private final BatchExecution exec;
		
		private DataModelProxy(Class<T> t, Map<String, Object> dm, BatchExecution exec) {
			this.cls = t;
			this.dm = dm;
			this.exec = exec;
		}
		
		@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
			
			String key = null;
			boolean write = false;
			
			if (method.getName().startsWith("get") || method.getName().startsWith("set")) {
				key = method.getName().substring(3);
				write = method.getName().startsWith("set");
			}
			else if (method.getName().startsWith("is")) {
				key = method.getName().substring(2);
			}
			
			if (write) {
				ValueContainer container = null;
				if (args[0] == null || args[0] instanceof String || args[0].getClass().isPrimitive() || args[0] instanceof Number || args[0] instanceof Boolean) {
					container = new ValueContainer(args[0], false);
				}
				else {
					Gson gson = JsonUtils.getBuilder().create();
					container = new ValueContainer(gson.toJson(args[0]), true);
				}
				dm.put(key, container);
				return null;
			}
			else {
				Object o = dm.get(key);
				if (o == null) {
					return null;
				}
				else if (o instanceof ValueContainer) {
 					ValueContainer vc = (ValueContainer) o;
					if (vc.isEncoded()) {
						Gson gson = JsonUtils.getBuilder().create();
						return gson.fromJson(vc.getValue().toString(), method.getGenericReturnType());
					}
					else if (vc.getValue() instanceof String) {
						return exec.process((String) vc.getValue());
					}
					else {
						return vc.getValue();
					}
				}
				else {
					return o;
				}
			}
			
		}
		
		private static class ValueContainer {
			
			private Object value;
			private boolean encoded;

			public ValueContainer(Object value, boolean encoded) {
				this.value = value;
				this.encoded = encoded;
			}

			public Object getValue() {
				return value;
			}

			public void setValue(Object value) {
				this.value = value;
			}

			public boolean isEncoded() {
				return encoded;
			}

			public void setEncoded(boolean encoded) {
				this.encoded = encoded;
			}
			
		}
		
	}
	
}
