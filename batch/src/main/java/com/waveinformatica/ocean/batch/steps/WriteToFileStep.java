/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.batch.steps;

import com.waveinformatica.ocean.batch.BatchException;
import com.waveinformatica.ocean.batch.annotations.BatchStep;
import com.waveinformatica.ocean.batch.dto.BatchExecution;
import com.waveinformatica.ocean.batch.dto.StepExecution;
import com.waveinformatica.ocean.batch.interfaces.IBatchStep;
import com.waveinformatica.ocean.core.Configuration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
@BatchStep(group = "ocean", name = "writeToFile", title = "Write stream to file", editTpl = "/templates/batch/steps/writeToFile.tpl", workspace = true)
public class WriteToFileStep implements IBatchStep {
	
	@Inject
	private Configuration config;

	@Override
	public void execute(BatchExecution execution, StepExecution configuration) throws BatchException {

		ConfigurationObject conf = (ConfigurationObject) configuration.getConfiguration();
		
		String filePath = execution.process(conf.getFilePath());
		
		if (StringUtils.isBlank(filePath)) {
			try {

				conf.setFilePath(File.createTempFile("batch" + execution.getBatch().getId().toString() + "-" +
						execution.getBatchInstance().getId().toString() + "-step" + configuration.getStepInstance().getStep().getId().toString(), "").getAbsolutePath());

				execution.getDataModel().put("step-" + configuration.getStepInstance().getStep().getId().toString() + "-output", filePath);

				configuration.getLogger().log(Level.WARNING, "Output file path not set: temporary file name set into variable \"step-" + configuration.getStepInstance().getStep().getId().toString() + "-output\"");

			} catch (IOException ex) {
				throw new BatchException(ex);
			}
		}

		try {

			File inputFile = null;
			
			if (filePath.startsWith("$")) {
				filePath = filePath.substring(1);
				if (filePath.startsWith("/")) {
					filePath = filePath.substring(1);
				}
				inputFile = new File(config.getConfigDir(), filePath);
			}
			if (filePath.startsWith("~")) {
				filePath = filePath.substring(1);
				if (execution.getWorkspace() != null) {
					if (filePath.startsWith("/")) {
						filePath = filePath.substring(1);
					}
					inputFile = new File(execution.getWorkspace().getBase(), filePath);
				}
			}
			if (inputFile == null) {
				inputFile = new File(filePath);
			}

			FileOutputStream fos = new FileOutputStream(inputFile);

			byte[] buffer = (byte[]) execution.getDataModel().get(conf.getSourceVar());

			fos.write(buffer);

			configuration.getLogger().log(Level.INFO, "Written " + buffer.length + " bytes to \"" + filePath + "\"");

			fos.close();

		} catch (FileNotFoundException ex) {
			throw new BatchException(ex);
		} catch (IOException ex) {
			throw new BatchException(ex);
		}

	}

	@Override
	public Class<? extends Serializable> getConfigurationObject() {
		return ConfigurationObject.class;
	}

	public static class ConfigurationObject implements Serializable {

		private String sourceVar;
		private String filePath;

		public String getSourceVar() {
			return sourceVar;
		}

		public void setSourceVar(String sourceVar) {
			this.sourceVar = sourceVar;
		}

		public String getFilePath() {
			return filePath;
		}

		public void setFilePath(String filePath) {
			this.filePath = filePath;
		}

	}

}
