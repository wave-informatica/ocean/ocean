/*******************************************************************************
 * Copyright 2015, 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.ocean.batch.dto;

import com.waveinformatica.ocean.batch.entities.StepExecutionInstance;
import java.io.Serializable;
import java.util.logging.Logger;

/**
 * @param <C> the configuration object type
 *
 * @author Ivano
 */
public class StepExecution<C extends Serializable> {

	private C configuration;
	private Logger logger;
	private StepExecutionInstance stepInstance;
	private double completionPercentage;

	public C getConfiguration() {
		return configuration;
	}

	public void setConfiguration(C configuration) {
		this.configuration = configuration;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public StepExecutionInstance getStepInstance() {
		return stepInstance;
	}

	public void setStepInstance(StepExecutionInstance stepInstance) {
		this.stepInstance = stepInstance;
	}

	public double getCompletionPercentage() {
		return completionPercentage;
	}

	public void setCompletionPercentage(double completionPercentage) {
		this.completionPercentage = completionPercentage;
	}

}
