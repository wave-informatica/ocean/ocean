/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.batch.steps;

import com.waveinformatica.ocean.batch.BatchException;
import com.waveinformatica.ocean.batch.BatchService;
import com.waveinformatica.ocean.batch.annotations.BatchStep;
import com.waveinformatica.ocean.batch.dto.BatchExecution;
import com.waveinformatica.ocean.batch.dto.StepExecution;
import com.waveinformatica.ocean.batch.entities.BatchDefinition;
import com.waveinformatica.ocean.batch.entities.LaunchType;
import com.waveinformatica.ocean.batch.interfaces.IBatchStep;
import com.waveinformatica.ocean.batch.interfaces.ObjectFactoryAware;
import com.waveinformatica.ocean.core.annotations.ExcludeSerialization;
import com.waveinformatica.ocean.core.annotations.OceanPersistenceContext;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author Ivano
 */
@BatchStep(group = "ocean", name = "triggerBatch", editTpl = "/templates/batch/steps/triggerBatch.tpl", title = "com.waveinformatica.ocean.batch.steps.triggerBatch")
public class TriggerBatchStep implements IBatchStep {
	
	@Inject
	private CoreController controller;
	
	@Inject
	@OceanPersistenceContext
	private EntityManager em;
	
	@Override
	public void execute(BatchExecution execution, StepExecution configuration) throws BatchException {
		
		ConfigurationObject config = (ConfigurationObject) configuration.getConfiguration();
		
		BatchService service = controller.getService(BatchService.class);
		
		BatchDefinition batch = em.find(BatchDefinition.class, config.getBatchId());
		
		try {
			
			Long instance = service.startBatch(batch, LaunchType.TRIGGERED, null);
			
			configuration.getLogger().log(Level.INFO, "Batch lanciato con successo: <a href=\"batch/instance?id=" + instance.toString() + "\">Dettagli</a>");
			
		} catch (Exception e) {
			
			configuration.getLogger().log(Level.SEVERE, "Error starting batch with id " + batch.getId().toString(), e);
			
		}
		
	}

	@Override
	public Class<? extends Serializable> getConfigurationObject() {
		return ConfigurationObject.class;
	}
	
	public static class ConfigurationObject implements ObjectFactoryAware, Serializable {
		
		private Long batchId;
		
		@ExcludeSerialization
		private ObjectFactory factory;

		public Long getBatchId() {
			return batchId;
		}

		public void setBatchId(Long batchId) {
			this.batchId = batchId;
		}
		
		public List<BatchDefinition> getAvailableBatches() {
			
			Beans beans = factory.newInstance(Beans.class);
			EntityManager em = beans.getEm();
			
			return em.createQuery("select x from BatchDefinition x order by x.title", BatchDefinition.class).getResultList();
			
		}

		@Override
		public void setObjectFactory(ObjectFactory objectFactory) {
			this.factory = objectFactory;
		}
		
	}
	
	public static class Beans {
		
		@Inject
		@OceanPersistenceContext
		private EntityManager em;

		public EntityManager getEm() {
			return em;
		}

		public void setEm(EntityManager em) {
			this.em = em;
		}
		
	}
	
}
