/*
 * Copyright 2016, 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.batch.steps;

import com.waveinformatica.ocean.batch.BatchException;
import com.waveinformatica.ocean.batch.annotations.BatchStep;
import com.waveinformatica.ocean.batch.dto.BatchExecution;
import com.waveinformatica.ocean.batch.dto.StepExecution;
import com.waveinformatica.ocean.batch.interfaces.IBatchStep;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Ivano
 */
@BatchStep(group = "ocean", name = "unzip", title = "unzip", editTpl = "/templates/batch/steps/unzip.tpl", workspace = true)
public class UnzipStep implements IBatchStep {

	@Override
	public void execute(BatchExecution execution, StepExecution configuration) throws BatchException {
		
		ConfigurationObject cfg = (ConfigurationObject) configuration.getConfiguration();
		
		File basePath = execution.getWorkspace().getBase();
		if (StringUtils.isNotBlank(cfg.getExtractInFolder())) {
			basePath = new File(basePath, cfg.getExtractInFolder());
			if (!basePath.exists()) {
				basePath.mkdirs();
			}
		}
		
		Object obj = execution.getDataModel().get(execution.process(cfg.getSourceVariable()));
		ZipInputStream zip = null;
		if (obj instanceof String) {
			try {
				zip = new ZipInputStream(new FileInputStream(obj.toString()));
			} catch (FileNotFoundException e) {
				throw new BatchException(e);
			}
		}
		else {
			zip = new ZipInputStream(new ByteArrayInputStream((byte[]) obj));
		}
		ZipEntry entry;
		try {
			byte[] buffer = new byte[4096];
			int read = 0;
			while ((entry = zip.getNextEntry()) != null) {
				File file = new File(basePath, entry.getName());
				if (entry.isDirectory()) {
					configuration.getLogger().log(Level.INFO, "Creating directory " + entry.getName() + " ...");
					file.mkdirs();
				}
				else {
					configuration.getLogger().log(Level.INFO, "Extracting file " + entry.getName() + " ...");
					FileOutputStream fos = new FileOutputStream(file);
					try {
						while ((read = zip.read(buffer)) > 0) {
							fos.write(buffer, 0, read);
						}
					} finally {
						fos.close();
					}
					file.setLastModified(entry.getTime());
				}
			}
		} catch (IOException ex) {
			throw new BatchException(ex);
		} finally {
			try {
				zip.close();
			} catch (IOException ex) {
				throw new BatchException(ex);
			}
		}
		
	}

	@Override
	public Class<? extends Serializable> getConfigurationObject() {
		return ConfigurationObject.class;
	}
	
	public static class ConfigurationObject implements Serializable {
		
		private String sourceVariable;
		private String extractInFolder;

		public String getSourceVariable() {
			return sourceVariable;
		}

		public void setSourceVariable(String sourceVariable) {
			this.sourceVariable = sourceVariable;
		}

		public String getExtractInFolder() {
			return extractInFolder;
		}

		public void setExtractInFolder(String extractInFolder) {
			this.extractInFolder = extractInFolder;
		}
		
	}
	
}
