/*******************************************************************************
 * Copyright 2015, 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.ocean.batch.annotations;

import com.waveinformatica.ocean.batch.interfaces.IBatchStep;
import com.waveinformatica.ocean.core.annotations.OceanComponent;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is used to define a batch in a step. Batch steps MUST implement
 * the {@link IBatchStep} interface too.
 * 
 * @author Ivano
 */
@OceanComponent(mustExtend = IBatchStep.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface BatchStep {
    
    /**
     * A group name for grouping the step
     * 
     * @return the group name
     */
    String group();
    
    /**
     * The system name of the step
     * 
     * @return the system name
     */
    String name();
    
    /**
     * A short description for the step
     * 
     * @return a short description
     */
    String title() default "";
    
    /**
     * The template used to show a long description for the step
     * 
     * @return a template path
     */
    String help() default "";
    
    /**
     * Defines a custom template for editing the step's configuration
     * 
     * @return a custom template for editing the step's configuration
     */
    String editTpl() default "";
    
    /**
     * Says if the step requires the preparation of a workspace for its execution
     * 
     * @return if the step requires a workspace
     */
    boolean workspace() default false;
    
}
