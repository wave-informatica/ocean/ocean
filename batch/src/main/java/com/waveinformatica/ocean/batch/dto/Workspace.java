/*
 * Copyright 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.batch.dto;

import java.io.File;

/**
 *
 * @author Ivano
 */
public class Workspace {
	
	private File basePath;

	public File getBase() {
		return basePath;
	}

	public void setBase(File base) {
		this.basePath = base;
	}
	
	public void clear() {
		deleteDir(basePath);
	}
	
	private void deleteDir(File dir) {
		if (dir == null) {
			return;
		}
		File[] files = dir.listFiles();
		if (files != null) {
			for (File f : files) {
				if (f.isDirectory()) {
					deleteDir(f);
				}
				else {
					f.delete();
				}
			}
		}
		dir.delete();
	}
	
}
