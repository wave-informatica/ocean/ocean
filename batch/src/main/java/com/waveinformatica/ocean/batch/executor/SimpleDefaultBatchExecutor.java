/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */
package com.waveinformatica.ocean.batch.executor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

import javax.inject.Inject;

import com.waveinformatica.ocean.batch.annotations.BatchExecutor;
import com.waveinformatica.ocean.batch.dto.BatchExecution;
import com.waveinformatica.ocean.batch.dto.BatchProcess;
import com.waveinformatica.ocean.batch.dto.StepProcess;
import com.waveinformatica.ocean.batch.entities.BatchInstance;
import com.waveinformatica.ocean.batch.interfaces.IBatchExecutor;
import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.tasks.OceanExecutorService;
import com.waveinformatica.ocean.core.util.LoggerUtils;

/**
 * This batch executor will manage execution of batch processes in a local
 * thread pool and is the simplest and the default one.
 *
 * @author Ivano
 */
@BatchExecutor
public class SimpleDefaultBatchExecutor implements IBatchExecutor {

	@Inject
	private ObjectFactory factory;
	
	@Inject
	private OceanExecutorService executorService;

	private final Map<Long, Thread> threads = new HashMap<Long, Thread>();
	private final Map<Long, BatchProcess> processes = new ConcurrentHashMap<Long, BatchProcess>();

	@SuppressWarnings("rawtypes")
	@Override
	public Long startBatch(BatchExecution batchExec) {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		
		batchExec.getDataModel().put("currentDate", dateFormat.format(new Date()));
		batchExec.getDataModel().put("currentTime", dateTimeFormat.format(new Date()));
		
		BatchProcess process = factory.newInstance(BatchProcess.class);
		process.setBatchExecution(batchExec);
		process.setLogger(LoggerUtils.getLogger(SimpleDefaultBatchExecutor.class));
		process.setExecutor(this);

		processes.put(batchExec.getBatchInstance().getId(), process);

		executorService.submit(new ProcessWrapper(process));

		return batchExec.getBatchInstance().getId();

	}

	@Override
	public boolean kill(BatchInstance instance) {

		Thread t = threads.get(instance.getId());
		if (t != null) {
			t.interrupt();
			return t.isInterrupted();
		}

		return false;

	}

	@Override
	public void terminatedCallback(BatchInstance instance) {
		threads.remove(instance.getId());
		processes.remove(instance.getId());
	}

	@Override
	public void startedCallback(BatchInstance instance) {
		threads.put(instance.getId(), Thread.currentThread());
	}

	@Override
	public StackTraceElement[] getStackTrace(BatchInstance instance) {

		Thread t = threads.get(instance.getId());

		if (t != null) {
			return t.getStackTrace();
		} else {
			return new StackTraceElement[0];
		}

	}

	@Override
	public StepProcess getCurrentStepProcess(BatchInstance instance) {

		BatchProcess process = processes.get(instance.getId());

		return process != null ? process.getCurrentStepProcess() : null;

	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		threads.clear();
		processes.clear();
	}
	
	public static class ProcessWrapper implements Runnable {
		
		private final Runnable runnable;
		
		public ProcessWrapper (Runnable runnable) {
			this.runnable = runnable;
		}

		public Runnable getRunnable() {
			return runnable;
		}

		@Override
		public void run() {
			try {
				Application.getInstance().getCoreController().runLocked(runnable, false);
			} catch (InterruptedException ex) {
				LoggerUtils.getCoreLogger().log(Level.SEVERE, "Interrupted waiting to write lock", ex);
			}
		}
		
	}

}
