/*
 * Copyright 2016, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.batch;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 *
 * @author Ivano
 */
public class StepLogHandler extends Handler {

	private StringBuilder builder = new StringBuilder();

	private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");

	@Override
	public void publish(LogRecord record) {
		StringBuilder recordBuilder = new StringBuilder();

		String color = null;

		if (record.getLevel().intValue() == Level.ALL.intValue()) {
			color = "blue";
		}
		else if (record.getLevel().intValue() == Level.CONFIG.intValue()) {
			color = "blue";
		}
		else if (record.getLevel().intValue() == Level.FINE.intValue()) {
			color = "blue";
		}
		else if (record.getLevel().intValue() == Level.FINER.intValue()) {
			color = "blue";
		}
		else if (record.getLevel().intValue() == Level.FINEST.intValue()) {
			color = "blue";
		}
		else if (record.getLevel().intValue() == Level.INFO.intValue()) {
			color = "black";
		}
		else if (record.getLevel().intValue() == Level.OFF.intValue()) {
			color = "darkgray";
		}
		else if (record.getLevel().intValue() == Level.SEVERE.intValue()) {
			color = "darkred";
		}
		else if (record.getLevel().intValue() == Level.WARNING.intValue()) {
			color = "darkorange";
		}

		recordBuilder.append("<pre style=\"color: ");
		recordBuilder.append(color);
		recordBuilder.append(";\"><strong>");
		recordBuilder.append(format.format(new Date(record.getMillis())));
		recordBuilder.append(" [");
		recordBuilder.append(record.getLevel().getName());
		recordBuilder.append("] ");
		recordBuilder.append(record.getMessage());
		recordBuilder.append("</strong>");

		if (record.getThrown() != null) {
			recordBuilder.append("\n");
			StringWriter writer = new StringWriter();
			record.getThrown().printStackTrace(new PrintWriter(writer));
			recordBuilder.append(writer.toString());
		}

		recordBuilder.append("</pre>");

		builder.append(recordBuilder.toString());
	}

	@Override
	public void flush() {

	}

	@Override
	public void close() throws SecurityException {

	}

	public String getLog() {
		return builder.toString();
	}

}
