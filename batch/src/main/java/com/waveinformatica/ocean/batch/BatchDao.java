/*
 * Copyright 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.batch;

import com.waveinformatica.ocean.batch.entities.BatchDefinition;
import com.waveinformatica.ocean.batch.entities.BatchInstance;
import com.waveinformatica.ocean.batch.entities.StepDefinition;
import com.waveinformatica.ocean.batch.interfaces.IBatchStep;
import com.waveinformatica.ocean.core.annotations.OceanPersistenceContext;

import java.util.Collections;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ivano
 */
public class BatchDao {
	
	@Inject
	@OceanPersistenceContext
	private EntityManager em;
	
	public List<BatchDefinition> listBatches(String sortString) {
		
		if (StringUtils.isBlank(sortString)) {
			sortString = " order by title";
		}
		
		return em.createQuery("select x from BatchDefinition x" + sortString, BatchDefinition.class)
			  .getResultList();
		
	}
	
	public List<BatchDefinition> getBatchWithStepType(Class<? extends IBatchStep> stepType) {
		
		return em.createQuery("select distinct x.batch from StepDefinition x where x.name = :name", BatchDefinition.class)
			  .setParameter("name", stepType.getName())
			  .getResultList();
		
	}
	
	public List<BatchInstance> getBatchHistoryWithStepType(Class<? extends IBatchStep> stepType) {
		
		List<BatchDefinition> definitions = em.createQuery("select distinct x.batch from StepDefinition x where x.name = :name", BatchDefinition.class)
				  .setParameter("name", stepType.getName())
				  .getResultList();
		
		if (definitions.isEmpty()) {
			return Collections.emptyList();
		}
		
		List<BatchInstance> history = em.createQuery("select x from BatchInstance x where x.batch in :batches order by x.startDate desc", BatchInstance.class)
				.setParameter("batches", definitions)
				.getResultList();
		
		return history;
		
	}
	
}
