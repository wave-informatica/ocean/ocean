/*
* Copyright 2016, 2017 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */
package com.waveinformatica.ocean.batch.steps;

import com.waveinformatica.ocean.batch.BatchException;
import com.waveinformatica.ocean.batch.annotations.BatchStep;
import com.waveinformatica.ocean.batch.dto.BatchExecution;
import com.waveinformatica.ocean.batch.dto.StepExecution;
import com.waveinformatica.ocean.batch.interfaces.IBatchStep;
import java.io.Serializable;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509ExtendedTrustManager;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author Ivano
 */
@BatchStep(group = "http", name = "httpRequest", title = "Http Request", editTpl = "/templates/batch/steps/httpRequest.tpl")
public class HttpRequestStep implements IBatchStep {

	@Override
	public void execute(BatchExecution execution, StepExecution configuration) throws BatchException {

		boolean doRetry = false;
		int retry = 3;

		do {

			doRetry = false;

			try {

				ConfigurationObject stepConfig = (ConfigurationObject) configuration.getConfiguration();

				URL url = new URL(stepConfig.getUrl());

				SSLContext ctx = SSLContext.getInstance("TLS");
				ctx.init(new KeyManager[0], new TrustManager[]{new X509ExtendedTrustManager() {
					@Override
					public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
					}

					@Override
					public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
					}

					@Override
					public X509Certificate[] getAcceptedIssuers() {
						return null;
					}

					@Override
					public void checkClientTrusted(X509Certificate[] arg0, String arg1, Socket arg2) throws CertificateException {
					}

					@Override
					public void checkServerTrusted(X509Certificate[] arg0, String arg1, Socket arg2) throws CertificateException {
					}

					@Override
					public void checkClientTrusted(X509Certificate[] arg0, String arg1, SSLEngine arg2) throws CertificateException {
					}

					@Override
					public void checkServerTrusted(X509Certificate[] arg0, String arg1, SSLEngine arg2) throws CertificateException {
					}
				}}, new SecureRandom());
				//			SSLContext.setDefault(ctx);

				int timeout = 30000;

				RequestConfig reqConfig = RequestConfig.custom()
					  .setSocketTimeout(timeout)
					  .setConnectTimeout(timeout)
					  .setConnectionRequestTimeout(timeout)
					  .build();

				CloseableHttpClient httpclient = HttpClients.custom()
					  .setDefaultRequestConfig(reqConfig)
					  .setRetryHandler(new DefaultHttpRequestRetryHandler(0, false))
					  .setSslcontext(ctx)
					  .build();

				CredentialsProvider credsProvider = new BasicCredentialsProvider();
				credsProvider.setCredentials(AuthScope.ANY,
					  new NTCredentials(stepConfig.getUserName(), stepConfig.getPassword(), "", stepConfig.getDomain()));

				// You may get 401 if you go through a load-balancer.
				// To fix this, go directly to one of the sharepoint web server or
				// change the config. See this article :
				// http://blog.crsw.com/2008/10/14/unauthorized-401-1-exception-calling-web-services-in-sharepoint/
				HttpHost target = new HttpHost(url.getHost(), url.getPort(), url.getProtocol());
				HttpClientContext context = HttpClientContext.create();
				context.setCredentialsProvider(credsProvider);

				// The authentication is NTLM.
				// To trigger it, we send a minimal http request
				configuration.getLogger().log(Level.INFO, "Authenticating with user \"" + (StringUtils.isNotBlank(stepConfig.getDomain()) ? stepConfig.getDomain() + "\\" : "") + stepConfig.getUserName() + "\"...");
				HttpHead request1 = new HttpHead("/");
				CloseableHttpResponse response1 = null;
				try {
					response1 = httpclient.execute(target, request1, context);
					EntityUtils.consume(response1.getEntity());
				} finally {
					if (response1 != null) {
						response1.close();
					}
				}

				// The real request, reuse authentication
				configuration.getLogger().log(Level.INFO, "Authenticated. Requesting URL \"" + stepConfig.getUrl() + "\"...");
				HttpGet request2 = new HttpGet(getPathFromUrl(url));
				CloseableHttpResponse response2 = null;
				try {
					response2 = httpclient.execute(target, request2, context);
					HttpEntity entity = response2.getEntity();
					int rc = response2.getStatusLine().getStatusCode();
					String reason = response2.getStatusLine().getReasonPhrase();
					if (rc == HttpStatus.SC_OK) {
						byte[] data = EntityUtils.toByteArray(entity);
						execution.getDataModel().put(stepConfig.outputVariable.trim(), data);
						configuration.getLogger().log(Level.INFO, "Downloaded " + data.length + " bytes");
					} else {
						throw new Exception("Problem while receiving " + request2.getURI().toString() + "  reason : "
							  + reason + " httpcode : " + rc);
					}
				} finally {
					if (response2 != null) {
						response2.close();
					}
				}

			} catch (SocketTimeoutException e) {
				doRetry = true;
				retry--;
				if (retry == 0) {
					throw new BatchException("Request timeout after 3 retries. Stopping.", e);
				} else {
					configuration.getLogger().log(Level.SEVERE, "Request timeout. Waiting for 5 seconds and retrying...");
					try {
						Thread.sleep(5000);
					} catch (InterruptedException ex) {
						configuration.getLogger().log(Level.WARNING, "Interrupted.");
					}
				}
			} catch (Exception e) {
				throw new BatchException(e);
			}

		} while (doRetry && retry > 0);

	}

	protected String getPathFromUrl(URL url) {
		return url.getPath();
	}

	@Override
	public Class<? extends Serializable> getConfigurationObject() {
		return ConfigurationObject.class;
	}

	public static class ConfigurationObject implements Serializable {

		private String url;
		private String domain;
		private String userName;
		private String password;
		private String outputVariable;

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public String getDomain() {
			return domain;
		}

		public void setDomain(String domain) {
			this.domain = domain;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getOutputVariable() {
			return outputVariable;
		}

		public void setOutputVariable(String outputVariable) {
			this.outputVariable = outputVariable;
		}

	}

}
