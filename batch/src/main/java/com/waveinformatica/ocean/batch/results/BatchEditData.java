/*******************************************************************************
 * Copyright 2015, 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package com.waveinformatica.ocean.batch.results;

import com.waveinformatica.ocean.batch.dto.ExecutorInfo;
import com.waveinformatica.ocean.batch.entities.BatchDefinition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Ivano
 */
public class BatchEditData {

	private final List<StepType> stepTypes = new ArrayList<StepType>();
	private final List<ExecutorInfo> availableExecutors = new ArrayList<ExecutorInfo>();
	private BatchDefinition batch;
	private final Map<String, String> stepTemplates = new HashMap<String, String>();
	private boolean copy = false;

	public List<StepType> getStepTypes() {
		return stepTypes;
	}

	public List<ExecutorInfo> getAvailableExecutors() {
		return availableExecutors;
	}

	public BatchDefinition getBatch() {
		return batch;
	}

	public void setBatch(BatchDefinition batch) {
		this.batch = batch;
	}

	public String getTitle() {
		return batch != null ? batch.getTitle() : "";
	}

	public String getDescription() {
		return batch != null ? batch.getDescription() : "";
	}

	public String getExecutorName() {
		return batch != null ? batch.getExecutorName() : "";
	}

	public Integer getMaxExecutions() {
		return batch != null && batch.getMaxExecutions() != null ? batch.getMaxExecutions() : 0;
	}

	public Long getId() {
		return !copy && batch != null ? batch.getId() : null;
	}

	public Map<String, String> getStepTemplates() {
		return stepTemplates;
	}

	public Integer getMaxLogs() {
		return batch != null && batch.getMaxLogs() != null ? batch.getMaxLogs() : 0;
	}

	public Integer getMaxSuccessLogs() {
		return batch != null && batch.getMaxSuccessLogs() != null ? batch.getMaxSuccessLogs() : 0;
	}

	public Integer getMaxFailedLogs() {
		return batch != null && batch.getMaxFailedLogs() != null ? batch.getMaxFailedLogs() : 0;
	}

	public boolean isCopy() {
		return copy;
	}

	public void setCopy(boolean copy) {
		this.copy = copy;
	}

	public static class StepType {

		private final String title;
		private final String type;
		private String template;

		public StepType(String title, String type) {
			this.title = title;
			this.type = type;
		}

		public String getTitle() {
			return title;
		}

		public String getType() {
			return type;
		}

		public String getTemplate() {
			return template;
		}

		public void setTemplate(String template) {
			this.template = template;
		}

	}

}
