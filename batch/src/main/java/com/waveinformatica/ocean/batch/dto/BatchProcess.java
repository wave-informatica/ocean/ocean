/*
* Copyright 2015, 2019 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
 */
package com.waveinformatica.ocean.batch.dto;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.google.gson.Gson;
import com.waveinformatica.ocean.batch.BatchException;
import com.waveinformatica.ocean.batch.BatchService;
import com.waveinformatica.ocean.batch.StepLogHandler;
import com.waveinformatica.ocean.batch.annotations.BatchStep;
import com.waveinformatica.ocean.batch.entities.BatchDefinition;
import com.waveinformatica.ocean.batch.entities.BatchInstance;
import com.waveinformatica.ocean.batch.entities.LaunchType;
import com.waveinformatica.ocean.batch.entities.StepDefinition;
import com.waveinformatica.ocean.batch.entities.StepExecutionInstance;
import com.waveinformatica.ocean.batch.events.BatchErrorEvent;
import com.waveinformatica.ocean.batch.exceptions.CancelException;
import com.waveinformatica.ocean.batch.interfaces.IBatchExecutor;
import com.waveinformatica.ocean.batch.interfaces.IBatchStep;
import com.waveinformatica.ocean.batch.interfaces.ObjectFactoryAware;
import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.OceanPersistenceContext;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.events.OperationErrorEvent;
import com.waveinformatica.ocean.core.security.SecurityManager;
import com.waveinformatica.ocean.core.tasks.OceanExecutorService;
import com.waveinformatica.ocean.core.tasks.OceanRunnable;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;

/**
 *
 * @author Ivano
 */
public class BatchProcess extends OceanRunnable {

	private final List<StepProcess> steps = new ArrayList<StepProcess>();
	private BatchExecution batchExecution;
	private Logger logger;
	private IBatchExecutor executor;
	private StepProcess currentStepProcess;

	@Inject
	private ObjectFactory factory;

	@Inject
	private CoreController controller;

	@Inject
	private Configuration configuration;
	
	@Inject
	private OceanExecutorService executorService;
	
	@Inject
	private SecurityManager sm;

	public BatchExecution getBatchExecution() {
		return batchExecution;
	}

	public void setBatchExecution(BatchExecution batchExecution) {
		this.batchExecution = batchExecution;
	}

	public List<StepProcess> getSteps() {
		return steps;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	@Override
	public void execute() {

		BatchService service = controller.getService(BatchService.class);

		Gson gson = JsonUtils.getBuilder().create();
		
		if (!sm.isLogged() && batchExecution.getBatchInstance().getLaunchType() != LaunchType.MANUAL) {
			sm.authenticateRequest(new BatchUser());
		}

		Beans beans = factory.newInstance(Beans.class);

		// Batch initialization
		for (StepDefinition def : batchExecution.getBatch().getSteps()) {

			if (def.getValidUntil() != null) {
				continue;
			}

			Class<? extends IBatchStep> stepClass = service.getStepByType(def.getName());
			IBatchStep stepInstance = factory.newInstance(stepClass);
			Serializable config = (Serializable) gson.fromJson(def.getConfiguration(), stepInstance.getConfigurationObject());
			if (config instanceof ObjectFactoryAware) {
				((ObjectFactoryAware) config).setObjectFactory(factory);
			}

			StepExecution stepExec = new StepExecution();
			stepExec.setConfiguration(config);

			Logger stepLogger = Logger.getLogger("batch" + batchExecution.getBatch().getId().toString()
				  + "-" + batchExecution.getBatchInstance().getId().toString()
				  + ".step" + def.getId());

			StepLogHandler stepLogHandler = new StepLogHandler();

			stepLogger.addHandler(stepLogHandler);

			stepExec.setLogger(stepLogger);
			getSteps().add(new StepProcess(this, stepExec, def, stepInstance, stepLogHandler));
		}

		int error = 0;

		executor.startedCallback(batchExecution.getBatchInstance());

		// Esecuzione del batch
		for (StepProcess sp : steps) {

			StepExecutionInstance stepExecInst = new StepExecutionInstance();
			stepExecInst.setBatchInstance(sp.getBatchProcess().getBatchExecution().getBatchInstance());
			stepExecInst.setStep(sp.getStepDefinition());
			stepExecInst.setStartDate(new Date());
			stepExecInst.setGroup(sp.getStepDefinition().getGroup());
			stepExecInst.setName(sp.getStepDefinition().getName());

			try {

				beans.getEntityManager().getTransaction().begin();

				beans.getEntityManager().persist(stepExecInst);

				beans.getEntityManager().getTransaction().commit();

			} finally {
				if (beans.getEntityManager().getTransaction().isActive()) {
					beans.getEntityManager().getTransaction().rollback();
				}
			}

			LoggerUtils.forceLogger(sp.getStepExecution().getLogger());

			try {

				// Preparing workspace if this step needs one and no workspace was prepared before
				if (sp.getStepInstance().getClass().getAnnotation(BatchStep.class).workspace()
					  && batchExecution.getWorkspace() == null) {
					// Create workspace
					Workspace workspace = new Workspace();
					workspace.setBase(new File(configuration.getConfigDir(), "batch/workspaces/batch-" + batchExecution.getBatch().getId().toString() + "/inst-" + batchExecution.getBatchInstance().getId().toString()));
					if (!workspace.getBase().exists()) {
						workspace.getBase().mkdirs();
					}
					batchExecution.setWorkspace(workspace);
				}

				sp.getStepExecution().setStepInstance(stepExecInst);

				currentStepProcess = sp;

				sp.getStepInstance().execute(batchExecution, sp.getStepExecution());

				stepExecInst.setExitCode(0);

			} catch (CancelException ce) {
				sp.getStepExecution().getLogger().log(Level.SEVERE, "Batch execution cancelled", ce);
				stepExecInst.setExitCode(ce.getExitCode() != null ? ce.getExitCode() : -3);
				error = stepExecInst.getExitCode();
				BatchErrorEvent evt = new BatchErrorEvent(this, sp, ce);
				controller.dispatchEvent(evt);
				break;
			} catch (BatchException be) {
				sp.getStepExecution().getLogger().log(Level.SEVERE, "Error executing step", be);
				stepExecInst.setExitCode(be.getExitCode() != null ? be.getExitCode() : -2);
				error = stepExecInst.getExitCode();
				BatchErrorEvent evt = new BatchErrorEvent(this, sp, be);
				controller.dispatchEvent(evt);
				break;
			} catch (Throwable t) {
				sp.getStepExecution().getLogger().log(Level.SEVERE, "Error executing step", t);
				stepExecInst.setExitCode(-1);
				error = -1;
				BatchErrorEvent evt = new BatchErrorEvent(this, sp, t);
				controller.dispatchEvent(evt);
				break;
			} finally {

				LoggerUtils.forceLogger(null);

				currentStepProcess.getStepExecution().setCompletionPercentage(100.0);
				stepExecInst.setEndDate(new Date());
				stepExecInst.setLog(sp.getStepLogHandler().getLog());

				StepTerminator terminator = factory.newInstance(StepTerminator.class);
				terminator.setStepExecInst(stepExecInst);
				executorService.submit(terminator);

			}
		}

		executor.terminatedCallback(batchExecution.getBatchInstance());

		if (error != 0) {
			batchExecution.getBatchInstance().setExitCode(error);
		} else {
			batchExecution.getBatchInstance().setExitCode(0);
		}
		batchExecution.getBatchInstance().setEndDate(new Date());
		BatchTerminator terminator = factory.newInstance(BatchTerminator.class);
		terminator.setBatchInst(batchExecution.getBatchInstance());
		executorService.submit(terminator);

		// Batch logs cleaning
		BatchDefinition b = batchExecution.getBatchInstance().getBatch();
		if (b.getMaxLogs() != null && b.getMaxLogs() > 0) {
			try {
				beans.getEntityManager().getTransaction().begin();

				List<BatchInstance> batches = beans.getEntityManager().createQuery("select x from BatchInstance x where x.batch = :batch order by x.startDate desc", BatchInstance.class)
					  .setParameter("batch", b)
					  .setMaxResults(b.getMaxLogs())
					  .getResultList();
				if (b.getMaxLogs().intValue() == batches.size()) {
					List<Long> ids = new ArrayList<Long>();
					for (BatchInstance bi : batches) {
						ids.add(bi.getId());
					}
					List<Long> deleteInstances = beans.getEntityManager().createQuery("select x.id from BatchInstance x where x.batch = :batch and x.id not in (:ids)", Long.class)
						  .setParameter("batch", b)
						  .setParameter("ids", ids)
						  .getResultList();
					if (!deleteInstances.isEmpty()) {
						beans.getEntityManager().createQuery("delete from StepExecutionInstance x where x in (select y from StepExecutionInstance y where y.batchInstance.batch = :batch and y.batchInstance.id in (:ids))")
							  .setParameter("batch", b)
							  .setParameter("ids", deleteInstances)
							  .executeUpdate();
						beans.getEntityManager().createQuery("delete from BatchInstance x where x.batch = :batch and x.id in (:ids)")
							  .setParameter("batch", b)
							  .setParameter("ids", deleteInstances)
							  .executeUpdate();
						clearWorkspace(b.getId(), deleteInstances);
					}
				}

				beans.getEntityManager().getTransaction().commit();

			} catch (Throwable t) {
				OperationErrorEvent evt = new OperationErrorEvent(this, t);
				controller.dispatchEvent(evt);
			} finally {
				if (beans.getEntityManager().getTransaction().isActive()) {
					beans.getEntityManager().getTransaction().rollback();
				}
			}
		} else {

			try {

				beans.getEntityManager().getTransaction().begin();

				if (b.getMaxFailedLogs() != null && b.getMaxFailedLogs() > 0) {
					List<BatchInstance> batches = beans.getEntityManager().createQuery("select x from BatchInstance x where x.batch = :batch and x.exitCode != 0 order by x.startDate desc", BatchInstance.class)
						  .setParameter("batch", b)
						  .setMaxResults(b.getMaxFailedLogs())
						  .getResultList();
					if (b.getMaxFailedLogs().intValue() == batches.size()) {
						List<Long> ids = new ArrayList<Long>();
						for (BatchInstance bi : batches) {
							ids.add(bi.getId());
						}
						List<Long> deleteInstances = beans.getEntityManager().createQuery("select x.id from BatchInstance x where x.batch = :batch and x.exitCode != 0 and x.id not in (:ids)", Long.class)
							  .setParameter("batch", b)
							  .setParameter("ids", ids)
							  .getResultList();
						if (!deleteInstances.isEmpty()) {
							beans.getEntityManager().createQuery("delete from StepExecutionInstance x where x in (select y from StepExecutionInstance y where y.batchInstance.batch = :batch and y.batchInstance.id in (:ids))")
								  .setParameter("batch", b)
								  .setParameter("ids", deleteInstances)
								  .executeUpdate();
							beans.getEntityManager().createQuery("delete from BatchInstance x where x.batch = :batch and x.id in (:ids)")
								  .setParameter("batch", b)
								  .setParameter("ids", deleteInstances)
								  .executeUpdate();
							clearWorkspace(b.getId(), deleteInstances);
						}
					}
				}
				if (b.getMaxSuccessLogs() != null && b.getMaxSuccessLogs() > 0) {
					List<BatchInstance> batches = beans.getEntityManager().createQuery("select x from BatchInstance x where x.batch = :batch and x.exitCode = 0 order by x.startDate desc", BatchInstance.class)
						  .setParameter("batch", b)
						  .setMaxResults(b.getMaxSuccessLogs())
						  .getResultList();
					if (b.getMaxSuccessLogs().intValue() == batches.size()) {
						List<Long> ids = new ArrayList<Long>();
						for (BatchInstance bi : batches) {
							ids.add(bi.getId());
						}
						List<Long> deleteInstances = beans.getEntityManager().createQuery("select x.id from BatchInstance x where x.batch = :batch and x.exitCode = 0 and x.id not in (:ids)", Long.class)
							  .setParameter("batch", b)
							  .setParameter("ids", ids)
							  .getResultList();
						if (!deleteInstances.isEmpty()) {
							beans.getEntityManager().createQuery("delete from StepExecutionInstance x where x in (select y from StepExecutionInstance y where y.batchInstance.batch = :batch and y.batchInstance.id in (:ids))")
								  .setParameter("batch", b)
								  .setParameter("ids", deleteInstances)
								  .executeUpdate();
							beans.getEntityManager().createQuery("delete from BatchInstance x where x.batch = :batch and x.id in (:ids)")
								  .setParameter("batch", b)
								  .setParameter("ids", deleteInstances)
								  .executeUpdate();
							clearWorkspace(b.getId(), deleteInstances);
						}
					}
				}

				beans.getEntityManager().getTransaction().commit();

			} catch (Throwable t) {
				OperationErrorEvent evt = new OperationErrorEvent(this, t);
				controller.dispatchEvent(evt);
			} finally {
				if (beans.getEntityManager().getTransaction().isActive()) {
					beans.getEntityManager().getTransaction().rollback();
				}
			}

		}

	}

	private void clearWorkspace(Long batchId, List<Long> instanceIds) {
		for (Long instId : instanceIds) {
			File workspace = new File(configuration.getConfigDir(), "batch/workspaces/batch-" + batchId.toString() + "/inst-" + instId.toString());
			if (workspace.exists()) {
				clearDir(workspace);
			}
		}
	}
	
	private void clearDir(File dir) {
		if (dir == null) {
			return;
		}
		File[] files = dir.listFiles();
		if (files != null) {
			for (File f : files) {
				if (f.isFile()) {
					f.delete();
				}
				else {
					clearDir(f);
				}
			}
		}
		dir.delete();
	}

	public IBatchExecutor getExecutor() {
		return executor;
	}

	public void setExecutor(IBatchExecutor executor) {
		this.executor = executor;
	}

	public StepProcess getCurrentStepProcess() {
		return currentStepProcess;
	}

	public static class Beans {

		@Inject
		@OceanPersistenceContext
		private EntityManager entityManager;

		public EntityManager getEntityManager() {
			return entityManager;
		}

	}

	public static class StepTerminator extends OceanRunnable {

		@Inject
		private ObjectFactory factory;

		private StepExecutionInstance stepExecInst;

		@Override
		public void execute() {

			try {
				Thread.sleep(1000);
			} catch (InterruptedException ex) {
				LoggerUtils.getLogger(BatchProcess.class).log(Level.SEVERE, null, ex);
			}

			EntityManager em = factory.newInstance(Beans.class).getEntityManager();

			em.getTransaction().begin();

			try {

				em.merge(stepExecInst);

				em.getTransaction().commit();

			} catch (Exception e) {
				LoggerUtils.getLogger(StepTerminator.class).log(Level.SEVERE, "Unable to store step execution result", e);
			} finally {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			}

		}

		public StepExecutionInstance getStepExecInst() {
			return stepExecInst;
		}

		public void setStepExecInst(StepExecutionInstance stepExecInst) {
			this.stepExecInst = stepExecInst;
		}

	}

	public static class BatchTerminator extends OceanRunnable {

		@Inject
		private ObjectFactory factory;

		private BatchInstance batchInst;

		@Override
		public void execute() {

			try {
				Thread.sleep(1000);
			} catch (InterruptedException ex) {
				LoggerUtils.getLogger(BatchProcess.class).log(Level.SEVERE, null, ex);
			}

			EntityManager em = factory.newInstance(Beans.class).getEntityManager();

			em.getTransaction().begin();

			try {

				em.merge(batchInst);

				em.getTransaction().commit();

			} catch (Exception e) {
				LoggerUtils.getLogger(BatchTerminator.class).log(Level.SEVERE, "Unable to store batch execution result", e);
			} finally {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			}

		}

		public BatchInstance getBatchInst() {
			return batchInst;
		}

		public void setBatchInst(BatchInstance batchInst) {
			this.batchInst = batchInst;
		}

	}

}
