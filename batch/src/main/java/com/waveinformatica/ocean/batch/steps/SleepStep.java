/*
 * Copyright 2017, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.batch.steps;

import com.waveinformatica.ocean.batch.BatchException;
import com.waveinformatica.ocean.batch.annotations.BatchStep;
import com.waveinformatica.ocean.batch.dto.BatchExecution;
import com.waveinformatica.ocean.batch.dto.StepExecution;
import com.waveinformatica.ocean.batch.entities.LaunchType;
import com.waveinformatica.ocean.batch.interfaces.IBatchStep;
import java.io.Serializable;
import java.util.logging.Level;

/**
 *
 * @author ivano
 */
@BatchStep(group = "ocean", name = "sleep", title = "agt.batch.sleep", editTpl = "/templates/batch/steps/sleep.tpl")
public class SleepStep implements IBatchStep {

	@Override
	public void execute(BatchExecution execution, StepExecution configuration) throws BatchException {

		ConfigurationObject config = (ConfigurationObject) configuration.getConfiguration();
		
		if (execution.getBatchInstance().getLaunchType() == LaunchType.MANUAL && !Boolean.TRUE.equals(config.getManual())) {
			configuration.getLogger().log(Level.INFO, "Sleep skipped for manual launch");
			return;
		}
		
		try {

			configuration.getLogger().log(Level.INFO, "Waiting for " + config.getMilliseconds().toString() + " milliseconds...");

			Thread.sleep(config.getMilliseconds());

		} catch (InterruptedException ex) {
			throw new BatchException("Batch interrupted while waiting for " + config.getMilliseconds() + " milliseconds", ex);
		}

	}

	@Override
	public Class<? extends Serializable> getConfigurationObject() {
		return ConfigurationObject.class;
	}

	public static class ConfigurationObject implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private Long milliseconds;
		private Boolean manual;

		public Long getMilliseconds() {
			return milliseconds;
		}

		public void setMilliseconds(Long milliseconds) {
			this.milliseconds = milliseconds;
		}

		public Boolean getManual() {
			return manual;
		}

		public void setManual(Boolean manual) {
			this.manual = manual;
		}

	}

}
