/*******************************************************************************
 * Copyright 2015 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.ocean.batch.steps;

import com.waveinformatica.ocean.batch.annotations.BatchStep;
import com.waveinformatica.ocean.batch.dto.BatchExecution;
import com.waveinformatica.ocean.batch.dto.StepExecution;
import com.waveinformatica.ocean.batch.interfaces.IBatchStep;
import java.io.Serializable;
import java.util.logging.Level;

/**
 *
 * @author Ivano
 */
@BatchStep(group = "test", name = "test", title = "A test step", editTpl = "/templates/batch/steps/test.tpl")
public class TestStep implements IBatchStep {

    @Override
    public void execute(BatchExecution execution, StepExecution configuration) {
	
        configuration.getLogger().log(Level.INFO, "Test step in esecuzione...");
	configuration.getLogger().log(Level.INFO, "TestStep: " + ((ConfigurationObject)configuration.getConfiguration()).getMessaggio());
	configuration.getLogger().log(Level.INFO, "Test step terminato.");
    }

    @Override
    public Class<? extends Serializable> getConfigurationObject() {
        return ConfigurationObject.class;
    }
    
    public static class ConfigurationObject implements Serializable {
        
        private String messaggio;

        public String getMessaggio() {
            return messaggio;
        }

        public void setMessaggio(String messaggio) {
            this.messaggio = messaggio;
        }
        
    }
}
