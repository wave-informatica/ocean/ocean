/*
 * Copyright 2016, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.batch.steps;

import java.io.Serializable;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.batch.BatchException;
import com.waveinformatica.ocean.batch.annotations.BatchStep;
import com.waveinformatica.ocean.batch.dto.BatchExecution;
import com.waveinformatica.ocean.batch.dto.StepExecution;
import com.waveinformatica.ocean.batch.interfaces.IBatchStep;

/**
 *
 * @author Ivano
 */
@BatchStep(group = "ocean", name = "csvToRecords", title = "CSV to records", editTpl = "/templates/batch/steps/csvToRecords.tpl")
public class CsvToRecordsStep implements IBatchStep {

	@Override
	public void execute(BatchExecution execution, StepExecution configuration) throws BatchException {
		
		try {
			
			ConfigurationObject stepConfig = (ConfigurationObject) configuration.getConfiguration();

			Charset charset = Charset.forName(execution.process(StringUtils.isNotBlank(stepConfig.getFormat()) ? stepConfig.getFormat() : "UTF-8"));
			
			String data = new String((byte[]) execution.getDataModel().get(stepConfig.getInputVariable()), charset);
			
			if (StringUtils.isNotBlank(stepConfig.getExtraHeader())) {
				String patternSpec = stepConfig.getExtraHeader();
				if (!patternSpec.startsWith("^")) {
					patternSpec = "^" + patternSpec;
				}
				if (!patternSpec.endsWith("\r\n")) {
					patternSpec = patternSpec + "\r\n";
				}
				Pattern pattern = Pattern.compile(patternSpec);
				Matcher matcher = pattern.matcher(data);
				if (matcher.find()) {
					data = data.substring(matcher.group().length());
					execution.getDataModel().put("header.count", matcher.groupCount() + 1);
					for (int i = 0; i <= matcher.groupCount(); i++) {
						execution.getDataModel().put("header[" + i + "]", matcher.group(i));
					}
				}
				else if (stepConfig.isForceExtraBounds()) {
					throw new BatchException("Header row does not match expression");
				}
			}
			
			if (StringUtils.isNotBlank(stepConfig.getExtraFooter())) {
				String patternSpec = stepConfig.getExtraFooter();
				if (!patternSpec.startsWith("\r\n")) {
					patternSpec = "\r\n" + patternSpec;
				}
				if (!patternSpec.endsWith("(\r\n)?$")) {
					patternSpec = patternSpec + "(\r\n)?$";
				}
				Pattern pattern = Pattern.compile(patternSpec);
				Matcher matcher = pattern.matcher(data);
				if (matcher.find()) {
					data = data.substring(0, data.length() - matcher.group().length());
					execution.getDataModel().put("footer.count", matcher.groupCount() + 1);
					for (int i = 0; i <= matcher.groupCount(); i++) {
						execution.getDataModel().put("footer[" + i + "]", matcher.group(i));
					}
				}
				else if (stepConfig.isForceExtraBounds()) {
					throw new BatchException("Footer row does not match expression");
				}
			}

			if (StringUtils.isBlank(stepConfig.getDelimiter())) {
				stepConfig.setDelimiter(";");
			}
			if (StringUtils.isBlank(stepConfig.getQuote())) {
				stepConfig.setQuote("\"");
			}
			
			CSVFormat format = CSVFormat.DEFAULT
				  .withDelimiter(stepConfig.getDelimiter().charAt(0))
				  .withQuote(stepConfig.getQuote().charAt(0))
				  .withRecordSeparator("\r\n");
			
			if (stepConfig.isFirstRowAsHeader()) {
				format = format.withFirstRecordAsHeader();
			}
			
			CSVParser records = format.parse(new StringReader(data));
			
			execution.getDataModel().put(stepConfig.getOutputVariable(), records);
			
			configuration.getLogger().log(Level.INFO, "Read " + records.getRecordNumber() + " records from CSV input");

		} catch (Exception e) {
			throw new BatchException(e);
		}
		
	}

	@Override
	public Class<? extends Serializable> getConfigurationObject() {
		return ConfigurationObject.class;
	}
	
	public static class ConfigurationObject implements Serializable {
		
		private String inputVariable;
		private String format;
		private String delimiter;
		private String quote;
		private boolean firstRowAsHeader;
		private String outputVariable;
		private String extraHeader;
		private String extraFooter;
		private boolean forceExtraBounds;

		public String getInputVariable() {
			return inputVariable;
		}

		public void setInputVariable(String inputVariable) {
			this.inputVariable = inputVariable;
		}

		public String getFormat() {
			return format;
		}

		public void setFormat(String format) {
			this.format = format;
		}

		public String getDelimiter() {
			return delimiter;
		}

		public void setDelimiter(String delimiter) {
			this.delimiter = delimiter;
		}

		public String getQuote() {
			return quote;
		}

		public void setQuote(String quote) {
			this.quote = quote;
		}

		public boolean isFirstRowAsHeader() {
			return firstRowAsHeader;
		}

		public void setFirstRowAsHeader(boolean firstRowAsHeader) {
			this.firstRowAsHeader = firstRowAsHeader;
		}

		public String getOutputVariable() {
			return outputVariable;
		}

		public void setOutputVariable(String outputVariable) {
			this.outputVariable = outputVariable;
		}

		public String getExtraHeader() {
			return extraHeader;
		}

		public void setExtraHeader(String extraHeader) {
			this.extraHeader = extraHeader;
		}

		public String getExtraFooter() {
			return extraFooter;
		}

		public void setExtraFooter(String extraFooter) {
			this.extraFooter = extraFooter;
		}

		public boolean isForceExtraBounds() {
			return forceExtraBounds;
		}

		public void setForceExtraBounds(boolean forceExtraBounds) {
			this.forceExtraBounds = forceExtraBounds;
		}
		
	}
	
}
