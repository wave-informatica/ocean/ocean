/*******************************************************************************
 * Copyright 2015, 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.ocean.batch.steps;

import java.io.Serializable;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.batch.BatchException;
import com.waveinformatica.ocean.batch.annotations.BatchStep;
import com.waveinformatica.ocean.batch.dto.BatchExecution;
import com.waveinformatica.ocean.batch.dto.StepExecution;
import com.waveinformatica.ocean.batch.interfaces.IBatchStep;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.dto.OperationContext;
import com.waveinformatica.ocean.core.exceptions.OperationNotFoundException;
import com.waveinformatica.ocean.core.util.UrlBuilder;

/**
 *
 * @author Ivano
 */
@BatchStep(group = "ocean", name = "operation", title = "Call operation", editTpl = "/templates/batch/steps/operation.tpl")
public class OperationStep implements IBatchStep<Object, OperationStep.ConfigurationObject> {

	@Inject
	private CoreController coreController;

	@Override
	public void execute(BatchExecution<Object> execution, StepExecution<ConfigurationObject> configuration) throws BatchException {

		try {

			ConfigurationObject conf = (ConfigurationObject) configuration.getConfiguration();

			configuration.getLogger().info("Executing operation \"" + conf.getOperation() + "\"...");

			String operation = execution.process(configuration.getConfiguration().getOperation());

			UrlBuilder builder = new UrlBuilder(operation);
			
			OperationContext opCtx = new OperationContext();
			
			Object result = coreController.executeOperation(builder.getUrl(false),
					builder.getParameters().build(),
					this, execution, configuration, execution.getBatch(),
					execution.getBatchInstance(), opCtx);

			if (!StringUtils.isBlank(conf.getResultVar())) {
				String varName = execution.process(conf.getResultVar()).trim();
				execution.getDataModel().put(varName, result);
				execution.getDataModel().put(varName + "_opCtx", opCtx);
			}

		} catch (OperationNotFoundException e) {
			throw new BatchException(e);
		}

	}

	public static class ConfigurationObject implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private String operation;
		private String resultVar;

		public String getOperation() {
			return operation;
		}

		public void setOperation(String operation) {
			this.operation = operation;
		}

		public String getResultVar() {
			return resultVar;
		}

		public void setResultVar(String resultVar) {
			this.resultVar = resultVar;
		}

	}

	@Override
	public Class<ConfigurationObject> getConfigurationObject() {
		return ConfigurationObject.class;
	}

}
