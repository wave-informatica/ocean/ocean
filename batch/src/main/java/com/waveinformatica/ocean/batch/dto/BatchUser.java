package com.waveinformatica.ocean.batch.dto;

import com.waveinformatica.ocean.core.security.OceanUser;

public class BatchUser implements OceanUser {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Long getId() {
		return -1L;
	}
	
	@Override
	public String toString() {
		return "Batch User";
	}

}
