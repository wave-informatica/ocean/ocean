/*******************************************************************************
 * Copyright 2015, 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.ocean.batch.dto;

import java.util.List;

import com.waveinformatica.ocean.batch.entities.ParameterDefinition;

/**
 *
 * @author Ivano
 */
public class Batch {
	
	private Long batchId;
	private String title;
	private String description;
	private String executor;
	private Integer maxExecutions;
	private boolean clearWorkspace;
	private Integer maxLogs;
	private Integer maxSuccessLogs;
	private Integer maxFailedLogs;
	private List<ParameterDefinition> parameters;
	private List<Step> steps;
	
	public Long getBatchId() {
		return batchId;
	}
	
	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getExecutor() {
		return executor;
	}
	
	public void setExecutor(String executor) {
		this.executor = executor;
	}

	public Integer getMaxExecutions() {
		return maxExecutions;
	}

	public void setMaxExecutions(Integer maxExecutions) {
		this.maxExecutions = maxExecutions;
	}
	
	public boolean isClearWorkspace() {
		return clearWorkspace;
	}
	
	public void setClearWorkspace(boolean clearWorkspace) {
		this.clearWorkspace = clearWorkspace;
	}
	
	public Integer getMaxLogs() {
		return maxLogs;
	}
	
	public void setMaxLogs(Integer maxLogs) {
		this.maxLogs = maxLogs;
	}
	
	public Integer getMaxSuccessLogs() {
		return maxSuccessLogs;
	}
	
	public void setMaxSuccessLogs(Integer maxSuccessLogs) {
		this.maxSuccessLogs = maxSuccessLogs;
	}
	
	public Integer getMaxFailedLogs() {
		return maxFailedLogs;
	}
	
	public void setMaxFailedLogs(Integer maxFailedLogs) {
		this.maxFailedLogs = maxFailedLogs;
	}
	
	public List<ParameterDefinition> getParameters() {
		return parameters;
	}

	public void setParameters(List<ParameterDefinition> parameters) {
		this.parameters = parameters;
	}

	public List<Step> getSteps() {
		return steps;
	}
	
	public void setSteps(List<Step> steps) {
		this.steps = steps;
	}
	
}
