/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.batch.steps;

import java.io.File;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.Level;

import com.waveinformatica.ocean.batch.BatchException;
import com.waveinformatica.ocean.batch.annotations.BatchStep;
import com.waveinformatica.ocean.batch.dto.BatchExecution;
import com.waveinformatica.ocean.batch.dto.StepExecution;
import com.waveinformatica.ocean.batch.interfaces.IBatchStep;

@BatchStep(group = "ocean", name = "downloadWorkspaceFile", title = "downloadWorkspaceFile", workspace = true, editTpl = "/templates/batch/steps/downloadWorkspaceFile.tpl")
public class DownloadWorkspaceFile implements IBatchStep<Object, DownloadWorkspaceFile.ConfigurationObject> {

	@Override
	public void execute(BatchExecution<Object> execution, StepExecution<DownloadWorkspaceFile.ConfigurationObject> configuration) throws BatchException {
		
		String fileName = execution.process(configuration.getConfiguration().getFileName());
		
		File file = new File(execution.getWorkspace().getBase(), fileName);
		
		if (file.exists()) {
			try {
				configuration.getLogger().log(Level.INFO, "Download file <a href=\"batch/downloadWorkspaceFile?batch=" + execution.getBatch().getId().toString() + "&instance=" + execution.getBatchInstance().getId().toString() + "&file=" + URLEncoder.encode(fileName, "UTF-8") + "\">" + fileName + "</a>");
			} catch (UnsupportedEncodingException e) {
				configuration.getLogger().log(Level.SEVERE, null, e);
			}
		}
		else {
			throw new BatchException("File " + fileName + " not found in workspace");
		}
		
	}

	@Override
	public Class getConfigurationObject() {
		return ConfigurationObject.class;
	}
	
	public static class ConfigurationObject implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private String fileName;

		public String getFileName() {
			return fileName;
		}

		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
		
	}

}
