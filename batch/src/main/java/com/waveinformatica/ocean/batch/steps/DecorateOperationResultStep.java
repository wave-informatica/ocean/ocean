/*
 * Copyright 2016, 2017 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.batch.steps;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.waveinformatica.ocean.batch.annotations.BatchStep;
import com.waveinformatica.ocean.batch.dto.BatchExecution;
import com.waveinformatica.ocean.batch.dto.StepExecution;
import com.waveinformatica.ocean.batch.interfaces.IBatchStep;
import com.waveinformatica.ocean.core.Application;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.OperationContext;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.controllers.results.WebPageResult;
import com.waveinformatica.ocean.core.util.DecoratorHelper;

/**
 *
 * @author Ivano
 */
@BatchStep(group = "ocean", name = "decorateResult", title = "Decorate operation result", editTpl = "/templates/batch/steps/decorate.tpl")
public class DecorateOperationResultStep implements IBatchStep<Object, DecorateOperationResultStep.ConfigurationObject> {

	@Inject
	private ObjectFactory factory;

	@Override
	public void execute(BatchExecution execution, StepExecution configuration) {

		ConfigurationObject conf = (ConfigurationObject) configuration.getConfiguration();

		String resultVar = execution.process(conf.getSourceVar());
		
		Object result = execution.getDataModel().get(resultVar);
		// FIXME: usiamo gli eventi, magari...
		if (result instanceof WebPageResult) {
			((WebPageResult)result).setContextPath(Application.getInstance().getBaseUrl());
		}
		
		OperationContext opCtx = (OperationContext) execution.getDataModel().get(resultVar + "_opCtx");
		if (opCtx == null) {
			opCtx = new OperationContext();
		}
		
		DecoratorHelper helper = factory.newInstance(DecoratorHelper.class);
		
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		
		MimeTypes type = MimeTypes.valueOf(conf.getTypeName());
		
		configuration.getLogger().info("Rendering \"" + result.getClass().getName() + "\" result to \"" + conf.typeName + "\" (" + type.getMimeType() + ")...");
		
		helper.decorateResult(result, stream, opCtx, type);

		execution.getDataModel().put(conf.getDestVar(), stream.toByteArray());

		configuration.getLogger().info("Written " + stream.toByteArray().length + " bytes to variable named \"" + conf.getDestVar() + "\"");

	}

	@Override
	public Class<ConfigurationObject> getConfigurationObject() {
		return ConfigurationObject.class;
	}

	public static class ConfigurationObject implements Serializable {

		private String sourceVar;
		private String destVar;
		private String typeName;

		public String getSourceVar() {
			return sourceVar;
		}

		public void setSourceVar(String sourceVar) {
			this.sourceVar = sourceVar;
		}

		public String getDestVar() {
			return destVar;
		}

		public void setDestVar(String destVar) {
			this.destVar = destVar;
		}

		public String getTypeName() {
			return typeName;
		}

		public void setTypeName(String typeName) {
			this.typeName = typeName;
		}

		public List<String> getMimeTypes() {
			List<String> list = new ArrayList<String>();

			for (MimeTypes type : MimeTypes.values()) {
				list.add(type.name());
			}

			return list;
		}

	}

}
