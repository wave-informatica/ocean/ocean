/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.batch;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;
import com.waveinformatica.ocean.batch.annotations.BatchStep;
import com.waveinformatica.ocean.batch.dto.Batch;
import com.waveinformatica.ocean.batch.dto.Step;
import com.waveinformatica.ocean.batch.dto.Workspace;
import com.waveinformatica.ocean.batch.entities.BatchDefinition;
import com.waveinformatica.ocean.batch.entities.BatchInstance;
import com.waveinformatica.ocean.batch.entities.BatchToken;
import com.waveinformatica.ocean.batch.entities.LaunchType;
import com.waveinformatica.ocean.batch.entities.ParameterDefinition;
import com.waveinformatica.ocean.batch.entities.StepDefinition;
import com.waveinformatica.ocean.batch.entities.StepExecutionInstance;
import com.waveinformatica.ocean.batch.interfaces.IBatchExecutor;
import com.waveinformatica.ocean.batch.interfaces.IBatchStep;
import com.waveinformatica.ocean.batch.interfaces.ObjectFactoryAware;
import com.waveinformatica.ocean.batch.results.BatchEditData;
import com.waveinformatica.ocean.batch.results.TemplateResult;
import com.waveinformatica.ocean.core.Configuration;
import com.waveinformatica.ocean.core.annotations.OceanPersistenceContext;
import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.annotations.RootOperation;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.CoreController.OperationInfo;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.BaseResult;
import com.waveinformatica.ocean.core.controllers.results.InputResult;
import com.waveinformatica.ocean.core.controllers.results.MessageResult;
import com.waveinformatica.ocean.core.controllers.results.MessageResult.MessageType;
import com.waveinformatica.ocean.core.controllers.results.StackResult;
import com.waveinformatica.ocean.core.controllers.results.StreamResult;
import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.core.controllers.results.WebPageResult;
import com.waveinformatica.ocean.core.controllers.results.input.HiddenFieldHandler;
import com.waveinformatica.ocean.core.controllers.results.input.TextFieldHandler;
import com.waveinformatica.ocean.core.controllers.scopes.AdminScopeProvider.AdminRootScope;
import com.waveinformatica.ocean.core.controllers.scopes.OperationScope;
import com.waveinformatica.ocean.core.controllers.scopes.RootScope;
import com.waveinformatica.ocean.core.converters.JsonConverter;
import com.waveinformatica.ocean.core.exceptions.OperationNotFoundException;
import com.waveinformatica.ocean.core.filtering.FilterBuilder;
import com.waveinformatica.ocean.core.filtering.QueryFilter;
import com.waveinformatica.ocean.core.modules.ModuleClassLoader;
import com.waveinformatica.ocean.core.templates.TemplatesManager;
import com.waveinformatica.ocean.core.util.Context;
import com.waveinformatica.ocean.core.util.FsUtils;
import com.waveinformatica.ocean.core.util.I18N;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.core.util.OperationParameters;
import com.waveinformatica.ocean.core.util.ParametersBuilder;
import com.waveinformatica.ocean.core.util.Results;
import com.waveinformatica.ocean.scheduler.SchedulerService;
import com.waveinformatica.ocean.scheduler.entities.Schedule;

import freemarker.template.Template;
import freemarker.template.TemplateException;

@OperationProvider(namespace = "/batch", inScope = AdminRootScope.class)
public class Operations {

	@Inject
	private ObjectFactory factory;

	@Inject
	private CoreController coreController;

	@Inject
	private TemplatesManager templatesManager;

	@Inject
	private Configuration configuration;

	@Inject
	private I18N i18n;

	@Inject
	@OceanPersistenceContext
	private EntityManager em;

	@Operation("")
	@RootOperation(section = "batch", iconUrl = "hourglass-2", title = "batch")
	public StackResult main(OperationScope opScope) {

		StackResult result = factory.newInstance(StackResult.class);
		result.setParentOperation("admin/");

		result.addOperation("batch/runningBatches");
		result.addOperation("batch/batchList");
		
		opScope.addBreadcrumbLink("batch/", "Batch");
		
		return result;

	}

	@Operation("runningBatches")
	public TableResult runningBatches() {

		TableResult result = factory.newInstance(TableResult.class);
		result.setParentOperation("batch/");

		List<BatchInstance> instances = em.createQuery("select x from BatchInstance x where x.exitCode is null order by x.startDate desc", BatchInstance.class)
			  .getResultList();

		if (instances.isEmpty()) {
			return null;
		}

		List<BatchDefinition> batches = new ArrayList<BatchDefinition>();
		for (BatchInstance bi : instances) {
			BatchDefinition bd = new BatchDefinition();
			bd.setId(bi.getId());
			bd.setTitle(bi.getBatch().getTitle());
			bd.setExecutorName(bi.getBatch().getExecutorName());
			batches.add(bd);
		}
		
		result.setCheckable(true);
		result.setTableData(batches, BatchDefinition.class);
		result.addTableOperation("batch/kill", "kill");
		result.addRowOperation("batch/instance", "detail");
		result.addRowOperation("batch/stackTrace", "stackTrace");
		result.addRowOperation("batch/kill", "kill");

		result.setMaxOperations(2);

		return result;

	}

	@Operation("batchList")
	public TableResult batchList(TableResult.TableResultConfig tableConfig) {

		TableResult result = factory.newInstance(TableResult.class);
		result.setParentOperation("admin/");

		String sortString;

		if (tableConfig != null && tableConfig.isSorted()) {
			sortString = " order by " + tableConfig.getSorting().getColumnName() + " " + tableConfig.getSorting().getDirection().name();
		} else {
			sortString = " order by title";
		}

		List<BatchDefinition> batches = em.createQuery("select x from BatchDefinition x" + sortString, BatchDefinition.class).getResultList();
		result.setTableData(new ArrayList<BatchDefinition>(batches) {
		});

		result.addTableOperation("batch/item", "New batch");
		result.addRowOperation("batch/start", "Start");
		result.addRowOperation("batch/startWithParams", "Start parameterized");
		result.addRowOperation("batch/instances", "Storico");
		result.addRowOperation("batch/schedules", "schedules");
		result.addRowOperation("batch/tokens/", "Tokens");
		result.addRowOperation("batch/item", "Edit");
		result.addRowOperation("batch/item?copy=true", "Clone");
		result.addRowOperation("batch/remove", "Remove");

		result.setMaxOperations(4);
		result.setDataConfiguration(tableConfig);

		return result;

	}

	@Operation("item")
	public WebPageResult batchItem(@Param("id") Long id, @Param("copy") Boolean copy, OperationScope opScope) {

		WebPageResult result = factory.newInstance(WebPageResult.class);
		result.setParentOperation("batch/");
		result.setTemplate("/templates/batch/editBatch.tpl");

		BatchEditData data = new BatchEditData();
		if (copy != null) {
			data.setCopy(copy);
		}

		BatchService service = coreController.getService(BatchService.class);

		List<Class<? extends IBatchStep>> steps = service.getAvailableSteps();

		for (Class<? extends IBatchStep> c : steps) {
			BatchStep bs = c.getAnnotation(BatchStep.class);
			BatchEditData.StepType stepType = new BatchEditData.StepType(bs.title(), c.getName());
			data.getStepTypes().add(stepType);
		}

		data.getAvailableExecutors().addAll(service.getAvailableExecutors());

		opScope.addBreadcrumbLink("batch/", "Batch");
		
		if (id != null) {
			BatchDefinition batch = em.find(BatchDefinition.class, id);
			data.setBatch(batch);
			for (StepDefinition sd : batch.getSteps()) {
				if (sd.getValidUntil() != null) {
					continue;
				}
				for (BatchEditData.StepType t : data.getStepTypes()) {
					if (t.getType().equals(sd.getName())) {
						sd.setTemplate(getStepEditTemplate(t.getType(), sd.getConfiguration()).getTemplate());
						break;
					}
				}
			}
			
			opScope.addBreadcrumbLink("batch/item?id=" + id.toString(), batch.getTitle());
		}
		else {
			opScope.addBreadcrumbLink("batch/item", "new");
		}

		result.setData(data);

		Results.addCssSource(result, "ris/css/spinners.css");
		Results.addCssSource(result, "ris/css/batch.css");
		Results.addScriptSource(result, "ris/js/jquery-ui-sortable.min.js");
		Results.addScriptSource(result, "ris/js/mustache.min.js");

		return result;

	}

	@Operation("getStepEdit")
	public TemplateResult getStepEditTemplate(@Param("stepType") String stepType, String configuration) {

		BatchService service = coreController.getService(BatchService.class);

		List<Class<? extends IBatchStep>> steps = service.getAvailableSteps();

		Class<? extends IBatchStep> stepCls = null;
		for (Class<? extends IBatchStep> c : steps) {
			if (c.getName().equals(stepType)) {
				stepCls = c;
				break;
			}
		}

		if (stepCls == null) {
			return new TemplateResult();
		}

		BatchStep step = stepCls.getAnnotation(BatchStep.class);

		TemplateResult result = new TemplateResult();
		result.setTitle(i18n.translate(StringUtils.defaultIfBlank(step.title(), step.name())));

		if (step.editTpl().length() > 0) {

			StringWriter writer = new StringWriter();

			Template template = templatesManager.getTemplate(step.editTpl());

			try {

				Object root = null;
				try {
					Class<? extends Serializable> cfgRoot = stepCls.newInstance().getConfigurationObject();
					if (cfgRoot != null) {
						root = cfgRoot.newInstance();
					}
				} catch (InstantiationException ex) {
					LoggerUtils.getLogger(Operations.class).log(Level.SEVERE, null, ex);
				} catch (IllegalAccessException ex) {
					LoggerUtils.getLogger(Operations.class).log(Level.SEVERE, null, ex);
				}
				if (root == null) {
					root = new Object();
				}
				if (configuration != null) {
					try {
						IBatchStep stepInstance = stepCls.newInstance();
						Gson gson = JsonUtils.getBuilder().create();
						Class<? extends Serializable> objType = stepInstance.getConfigurationObject();
						if (objType != null) {
							root = gson.fromJson(new StringReader(configuration), objType);
						}
					} catch (InstantiationException ex) {
						Logger.getLogger(Operations.class.getName()).log(Level.SEVERE, null, ex);
					} catch (IllegalAccessException ex) {
						Logger.getLogger(Operations.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
				if (root instanceof ObjectFactoryAware) {
					((ObjectFactoryAware) root).setObjectFactory(factory);
				}

				template.process(root, writer);

				result.setTemplate(writer.toString());

			} catch (TemplateException ex) {
				LoggerUtils.getLogger(stepCls).log(Level.SEVERE, "Error evaluating edit template for step \"" + stepCls.getName() + "\"", ex);
			} catch (IOException ex) {
				LoggerUtils.getLogger(stepCls).log(Level.SEVERE, "Error evaluating edit template for step \"" + stepCls.getName() + "\"", ex);
			}

		}
		else {
			result.setTemplate("No configuration needed");
		}

		return result;

	}

	@Operation("steps")
	@RootOperation(section = "batch", iconUrl = "cubes", title = "Available Steps")
	public TableResult listAvailableSteps(OperationScope opScope) {

		TableResult result = factory.newInstance(TableResult.class);
		result.setParentOperation("admin/");

		BatchService service = coreController.getService(BatchService.class);

		List<Class<? extends IBatchStep>> steps = service.getAvailableSteps();

		List<StepInfo> tabElements = new ArrayList<StepInfo>() {
		};

		for (Class<? extends IBatchStep> c : steps) {

			tabElements.add(new StepInfo(c, i18n));

		}

		result.setTableData(tabElements);
		
		opScope.addBreadcrumbLink("batch/steps", "Available Steps");

		return result;

	}

	@Operation("saveBatch")
	public Map<String, Object> saveBatch(@Param(value = "definition", converter = JsonConverter.class) Batch definition) {

		BatchService service = (BatchService) coreController.getService("Batch");

		BatchDefinition batch = null;
		if (definition.getBatchId() == null) {
			batch = new BatchDefinition();
			batch.setSteps(new ArrayList<StepDefinition>());
			batch.setParameters(new ArrayList<ParameterDefinition>());
		} else {
			batch = em.find(BatchDefinition.class, definition.getBatchId());
		}

		batch.setTitle(definition.getTitle());
		batch.setDescription(definition.getDescription());
		batch.setExecutorName(definition.getExecutor());
		batch.setMaxExecutions(definition.getMaxExecutions());
		batch.setMaxLogs(definition.getMaxLogs());
		batch.setMaxSuccessLogs(definition.getMaxSuccessLogs());
		batch.setMaxFailedLogs(definition.getMaxFailedLogs());

		List<Class<? extends IBatchStep>> availableSteps = service.getAvailableSteps();

		int order = 0;
		
		// Rimozione parametri eliminati
		List<ParameterDefinition> removeParams = new ArrayList<ParameterDefinition>();
		for (ParameterDefinition pd : batch.getParameters()) {
			boolean trovato = false;
			if (definition.getParameters() != null) {
				for (ParameterDefinition pdf : definition.getParameters()) {
					if (pdf.getName().equals(pd.getName())) {
						trovato = true;
						break;
					}
				}
			}
			if (!trovato) {
				removeParams.add(pd);
			}
		}
		// Modifica e aggiunta di nuovi parametri
		if (definition.getParameters() != null) {
			for (ParameterDefinition pdf : definition.getParameters()) {
				ParameterDefinition trovato = null;
				for (ParameterDefinition pd : batch.getParameters()) {
					if (pd.getName().equals(pdf.getName())) {
						trovato = pd;
						break;
					}
				}
				if (trovato != null) {
					trovato.setDefaultValue(pdf.getDefaultValue());
				}
				else {
					pdf.setId(null);
					pdf.setBatch(batch);
					batch.getParameters().add(pdf);
				}
			}
		}

		// Rimozione step eliminati
		List<StepDefinition> removeSteps = new LinkedList<StepDefinition>();
		for (StepDefinition sd : batch.getSteps()) {
			boolean trovato = false;
			for (Step s : definition.getSteps()) {
				if (sd.getId().equals(s.getId())) {
					trovato = true;
					break;
				}
			}
			if (!trovato) {
				removeSteps.add(sd);
			}
		}
		// Modifica e aggiunta di nuovi step
		for (Step s : definition.getSteps()) {

			StepInfo info = new StepInfo(service.getStepByType(s.getStepType()), i18n);

			StepDefinition def = null;
			for (StepDefinition sd : batch.getSteps()) {
				if (sd.getId() != null && !sd.getId().equals(0L) && sd.getId().equals(s.getId())) {
					def = sd;
					break;
				}
			}
			if (def == null) {
				def = new StepDefinition();
				batch.getSteps().add(def);
				def.setBatch(batch);
			}
			def.setOrder(order++);
			def.setGroup(info.getGroup());
			def.setTitle(info.getTitle());
			def.setName(info.getClassName());
			def.setConfiguration(s.getFields());
		}

		em.getTransaction().begin();

		try {
			for (ParameterDefinition pd : removeParams) {
				batch.getParameters().remove(pd);
				em.remove(pd);
			}
			if (definition.getBatchId() == null) {
				em.persist(batch);
			} else {
				em.merge(batch);
			}
			for (StepDefinition sd : removeSteps) {
				batch.getSteps().remove(sd);
				sd.setValidUntil(new Date());
				em.merge(sd);
			}

			em.getTransaction().commit();

		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}

		Map<String, Object> result = new HashMap<String, Object>();
		result.put("success", true);

		return result;
	}

	@Operation("remove")
	public MessageResult deleteBatch(@Param("id") Long id) {

		MessageResult result = factory.newInstance(MessageResult.class);

		result.setParentOperation("batch/");
		result.setConfirmAction("batch/removeConfirm?id=" + id.toString());
		result.setMessage("Si &egrave; sicuri di voler eliminare il batch indicato? L'operazione non &egrave; reversibile.");
		result.setType(MessageResult.MessageType.WARNING);

		return result;

	}

	@Operation("removeConfirm")
	public MessageResult deleteBatchConfirm(@Param("id") Long id) {

		MessageResult result = factory.newInstance(MessageResult.class);
		result.setParentOperation("batch/");

		em.getTransaction().begin();

		try {

			BatchDefinition batch = em.find(BatchDefinition.class, id);
			em.remove(batch);

			em.getTransaction().commit();

			result.setMessage("Eliminazione avvenuta con successo");
			result.setConfirmAction("batch/");
			result.setType(MessageResult.MessageType.CONFIRM);

		} catch (Exception e) {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			LoggerUtils.getLogger(Operations.class).log(Level.SEVERE, "Impossibile eliminare il batch", e);
			result.setMessage("Impossibile eliminare il batch");
			result.setException(e);
			result.setType(MessageResult.MessageType.CRITICAL);
		}

		return result;

	}
	
	@Operation("startWithParams")
	public InputResult startBatchWithParameters(@Param("id") Long id, OperationScope opScope) {
		
		InputResult result = factory.newInstance(InputResult.class);
		
		result.setAction("batch/start");
		
		result.setValue("id", id.toString());
		result.setValue("type", LaunchType.MANUAL.name());

		BatchDefinition def = em.find(BatchDefinition.class, id);
		
		OperationInfo oi = coreController.getOperationInfo(AdminRootScope.class, "/batch/start");
		
		for (ParameterDefinition pd : def.getParameters()) {
			InputResult.FormField ff = new InputResult.FormField();
			ff.setAttribute(pd.getName());
			ff.setHandler(factory.newInstance(TextFieldHandler.class));
			ff.setName(pd.getName());
			ff.setParamInfo(oi.getParameters().get(0));
			ff.setLabel(pd.getName());
			result.getFields().add(ff);
		}
		
		opScope.addBreadcrumbLink("batch/", "Batch");
		opScope.addBreadcrumbLink("batch/instances?id=" + id.toString(), def.getTitle());
		opScope.addBreadcrumbLink("batch/startWithParams?id=" + id.toString(), "Start with parameters");
		
		return result;
		
	}

	@Operation("start")
	public MessageResult startBatch(
			@Param("id") @InputResult.Field(handler = HiddenFieldHandler.class) Long id,
			@Param("type") @InputResult.Field(handler = HiddenFieldHandler.class) LaunchType type,
			OperationParameters request,
			OperationScope opScope
	) {

		MessageResult result = factory.newInstance(MessageResult.class);

		BatchService service = (BatchService) coreController.getService("Batch");

		BatchDefinition def = em.find(BatchDefinition.class, id);
		
		Map<String, String> parameters = new HashMap<String, String>();
		for (ParameterDefinition pd : def.getParameters()) {
			String val = request.getParameter(pd.getName());
			if (StringUtils.isNotBlank(val)) {
				parameters.put(pd.getName(), val);
			}
		}

		try {

			Long instance = service.startBatch(def, type != null ? type : LaunchType.MANUAL, parameters);

			result.setMessage("Batch lanciato con successo: <a href=\"batch/instance?id=" + instance.toString() + "\">Dettagli</a>");
			result.setConfirmAction("batch/");
			result.setType(MessageResult.MessageType.INFO);

		} catch (Exception e) {

			result.setMessage("Si &egrave; verificato un errore durante l'avvio del batch");
			result.setCancelAction("batch/");
			result.setType(MessageResult.MessageType.CRITICAL);
			result.setException(e);

			LoggerUtils.getLogger(Operations.class).log(Level.SEVERE, "Error starting batch with id " + id.toString(), e);

		}
		
		opScope.addBreadcrumbLink("batch/", "Batch");
		opScope.addBreadcrumbLink("batch/instances?id=" + id.toString(), def.getTitle());

		return result;

	}

	@Operation("instances")
	public TableResult storicoEsecuzioni(@Param("id") Long id, OperationScope opScope) {

		TableResult result = factory.newInstance(TableResult.class);
		result.setParentOperation("batch/");

		BatchDefinition batch = em.find(BatchDefinition.class, id);

		result.putExtra("page-title", batch.getTitle());

		FilterBuilder fb = factory.newInstance(FilterBuilder.class);
		fb.init(BatchInstance.class);
		QueryFilter qf = fb.getJpqlFilter(result, "x");

		TypedQuery<BatchInstance> query = em.createQuery("select x from BatchInstance x where x.batch = :batch" + (StringUtils.isNotBlank(qf.getCondition()) ? " and " + qf.getCondition() : "") + " order by x.startDate desc", BatchInstance.class);
		qf.fillQueryParams(query);

		List<BatchInstance> instances = query
			  .setParameter("batch", batch)
			  .getResultList();

		result.setTableData(instances, BatchInstance.class);
		result.addTableOperation("batch/start?id=" + id.toString(), "start");
		result.addRowOperation("batch/instance", "detail");
		result.addRowOperation("batch/stackTrace", "stackTrace");
		result.addRowOperation("batch/kill", "kill");

		result.setMaxOperations(2);
		
		opScope.addBreadcrumbLink("batch/", "Batch");
		opScope.addBreadcrumbLink("batch/instances?id=" + id.toString(), batch.getTitle());

		return result;

	}

	@Operation("instance")
	public WebPageResult logIstanzaBatch(@Param("id") Long id, OperationScope opScope) {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");

		BatchInstance instance = em.find(BatchInstance.class, id);

		WebPageResult result = factory.newInstance(WebPageResult.class);
		result.setParentOperation("batch/instances?id=" + instance.getBatch().getId().toString());
		result.setTemplate("/templates/batch/batchDetails.tpl");

		result.putExtra("page-title", instance.getBatch().getTitle());
		result.putExtra("page-subtitle", "Started at " + sdf.format(instance.getStartDate()));
		
		em.getTransaction().begin();

		List<StepExecutionInstance> steps = em.createQuery("select x from StepExecutionInstance x where x.batchInstance = :instance order by x.startDate", StepExecutionInstance.class)
			  .setParameter("instance", instance)
			  .getResultList();

		em.getTransaction().commit();

		result.setData(steps);
		
		Workspace workspace = new Workspace();
		workspace.setBase(new File(configuration.getConfigDir(), "batch/workspaces/batch-" + instance.getBatch().getId().toString() + "/inst-" + instance.getId().toString()));
		if (workspace.getBase().exists()) {
			result.putExtra("workspace", workspace);
		}
		
		result.putExtra("instance", instance);

		BatchService service = coreController.getService(BatchService.class);
		IBatchExecutor executor = service.getExecutor(instance.getBatch().getExecutorName());
		result.putExtra("currentStepProcess", executor.getCurrentStepProcess(instance));
		
		opScope.addBreadcrumbLink("batch/", "Batch");
		opScope.addBreadcrumbLink("batch/instances?id=" + instance.getBatch().getId().toString(), instance.getBatch().getTitle());
		opScope.addBreadcrumbLink("batch/instance", "detail");
		
		return result;

	}

	@Operation("kill")
	public MessageResult killInstance(@Param("id") Long id, @Param("fw:tab_sel") List<Long> ids, OperationScope opScope) {

		MessageResult result = factory.newInstance(MessageResult.class);
		
		if (ids == null) {
			ids = new ArrayList<Long>(1);
			ids.add(id);
		}
		
		int notFound = 0,
				alreadyTerminated = 0,
				deads = 0,
				runningTerminated = 0,
				executorsNotFound = 0;

		opScope.addBreadcrumbLink("batch/", "Batch");
		
		for (Long bid : ids) {
			
			BatchInstance bi = em.find(BatchInstance.class, bid);
			
			if (ids.size() == 1 && bi != null) {
				opScope.addBreadcrumbLink("batch/instances?id=" + bi.getBatch().getId().toString(), bi.getBatch().getTitle());
			}
	
			if (bi == null) {
				notFound++;
			} else if (bi.getEndDate() != null) {
				alreadyTerminated++;
			} else {
	
				BatchService service = coreController.getService(BatchService.class);
	
				IBatchExecutor executor = service.getExecutor(bi.getBatch().getExecutorName());
	
				if (executor == null) {
					executorsNotFound++;
				} else if (executor.kill(bi)) {
					runningTerminated++;
				} else {
					
					try {
						
						em.getTransaction().begin();
						
						bi.setEndDate(new Date());
						bi.setExitCode(-1);
						em.merge(bi);
						
						for (StepExecutionInstance is : bi.getStepInstances()) {
							if (is.getEndDate() == null || is.getExitCode() == null) {
								
								StepLogHandler handler = new StepLogHandler();
								handler.publish(new LogRecord(Level.SEVERE, "Dead batch step manually killed"));
								
								if (StringUtils.isBlank(is.getLog())) {
									is.setLog("");
								}
								is.setLog(is.getLog() + handler.getLog());
								is.setEndDate(new Date());
								is.setExitCode(-1);
								em.merge(is);
								
							}
						}
						
						em.getTransaction().commit();
						
					} finally {
						if (em.getTransaction().isActive()) {
							em.getTransaction().rollback();
						}
					}
					
					deads++;
				}
	
			}
		}
		
		StringBuilder msg = new StringBuilder();
		if (runningTerminated > 0) {
			msg.append(runningTerminated);
			msg.append(" terminated instances");
		}
		if (deads > 0) {
			if (msg.length() > 0) {
				msg.append(", ");
			}
			msg.append(deads);
			msg.append(" dead instances terminated");
		}
		if (alreadyTerminated > 0) {
			if (msg.length() > 0) {
				msg.append(", ");
			}
			msg.append(alreadyTerminated);
			msg.append(" already terminated instances");
		}
		if (notFound > 0) {
			if (msg.length() > 0) {
				msg.append(", ");
			}
			msg.append(notFound);
			msg.append(" instances not found");
		}
		if (executorsNotFound > 0) {
			if (msg.length() > 0) {
				msg.append(", ");
			}
			msg.append(executorsNotFound);
			msg.append(" batches with executors not found");
		}
		
		result.setMessage(msg.toString());
		if ((runningTerminated > 0 || deads > 0 || alreadyTerminated > 0) && notFound == 0 && executorsNotFound == 0) {
			result.setType(MessageType.CONFIRM);
		}
		else if ((notFound == 0 || executorsNotFound == 0) && runningTerminated == 0 && deads == 0 && alreadyTerminated == 0) {
			result.setType(MessageType.CRITICAL);
		}
		else {
			result.setType(MessageType.WARNING);
		}
		if (result.getType() == MessageType.CRITICAL) {
			result.setCancelAction("batch/");
		}
		else {
			result.setConfirmAction("batch/");
		}
		
		return result;

	}

	@Operation("stackTrace")
	public BaseResult stackTrace(@Param("id") Long id, OperationScope opScope) {

		MessageResult result = factory.newInstance(MessageResult.class);

		BatchInstance bi = em.find(BatchInstance.class, id);
		
		if (bi != null) {
			opScope.addBreadcrumbLink("batch/", "Batch");
			opScope.addBreadcrumbLink("batch/instances?id=" + bi.getBatch().getId().toString(), bi.getBatch().getTitle());
		}

		if (bi == null) {
			result.setMessage("Batch instance not found");
			result.setType(MessageResult.MessageType.CRITICAL);
			result.setCancelAction("batch/");
		} else if (bi.getEndDate() != null) {
			result.setMessage("Batch instance already terminated");
			result.setType(MessageResult.MessageType.CRITICAL);
			result.setCancelAction("batch/instances?id=" + bi.getBatch().getId().toString());
		} else {

			BatchService service = coreController.getService(BatchService.class);

			IBatchExecutor executor = service.getExecutor(bi.getBatch().getExecutorName());

			if (executor == null) {
				result.setMessage("Batch executor not found");
				result.setType(MessageResult.MessageType.CRITICAL);
				result.setCancelAction("batch/instances?id=" + bi.getBatch().getId().toString());
			} else {

				StackTraceElement[] stack = executor.getStackTrace(bi);

				WebPageResult wr = factory.newInstance(WebPageResult.class);
				wr.setParentOperation("batch/instances?id=" + bi.getBatch().getId().toString());

				wr.setTemplate("/templates/batch/stackTrace.tpl");
				wr.setData(Arrays.asList(stack));
				
				opScope.addBreadcrumbLink("batch/stackTrace", "stackTrace");

				return wr;

			}

		}

		return result;

	}

	@Operation("schedules")
	public TableResult schedules(@Param("id") Long id, OperationScope opScope) {

		TableResult result = factory.newInstance(TableResult.class);
		result.setParentOperation("batch/");

		BatchDefinition batch = em.find(BatchDefinition.class, id);

		result.putExtra("page-title", batch.getTitle());

		List<ScheduleInfo> scheduleInfos = new ArrayList<ScheduleInfo>();

		SchedulerService scheduler = (SchedulerService) coreController.getService("SchedulerService");
		List<Schedule> schedules = scheduler.getSchedules("Batch:" + id.toString());
		for (Schedule s : schedules) {
			scheduleInfos.add(new ScheduleInfo(s));
		}
		result.setTableData(scheduleInfos, ScheduleInfo.class);

		result.addTableOperation("batch/editSchedule?batch=" + id.toString(), "add");
		result.addRowOperation("batch/toggleSchedule?batch=" + id.toString(), "toggle");
		result.addRowOperation("batch/editSchedule?batch=" + id.toString(), "edit");
		result.addRowOperation("batch/deleteSchedule?batch=" + id.toString(), "delete");
		
		opScope.addBreadcrumbLink("batch/", "Batch");
		opScope.addBreadcrumbLink("batch/instances?id=" + id.toString(), batch.getTitle());
		opScope.addBreadcrumbLink("batch/schedules?id=" + id.toString(), "schedules");

		return result;

	}

	@Operation("editSchedule")
	public InputResult editSchedule(@Param("batch") Long batchId, @Param("id") Long id, OperationScope opScope) {

		InputResult result = factory.newInstance(InputResult.class);
		result.setParentOperation("batch/schedules?id=" + batchId.toString());

		result.setAction("batch/saveSchedule");

		if (id != null) {
			SchedulerService scheduler = (SchedulerService) coreController.getService("SchedulerService");
			List<Schedule> schedules = scheduler.getSchedules("Batch:" + batchId.toString());
			for (Schedule s : schedules) {
				if (s.getId().equals(id)) {
					result.setValue("id", s.getId().toString());
					result.setValue("cron", s.getCron());
					break;
				}
			}
		}
		result.setValue("batch", batchId.toString());
		
		BatchDefinition def = em.find(BatchDefinition.class, batchId);
		
		OperationInfo oi = coreController.getOperationInfo(AdminRootScope.class, "/batch/start");
		
		for (ParameterDefinition pd : def.getParameters()) {
			InputResult.FormField ff = new InputResult.FormField();
			ff.setAttribute(pd.getName());
			ff.setHandler(factory.newInstance(TextFieldHandler.class));
			ff.setName(pd.getName());
			ff.setParamInfo(oi.getParameters().get(0));
			ff.setLabel(pd.getName());
			result.getFields().add(ff);
		}
		
		opScope.addBreadcrumbLink("batch/", "Batch");
		opScope.addBreadcrumbLink("batch/instances?id=" + batchId.toString(), def.getTitle());
		opScope.addBreadcrumbLink("batch/schedules?id=" + batchId.toString(), "schedules");

		return result;

	}

	@Operation("saveSchedule")
	public MessageResult saveSchedule(@InputResult.Field(handler = HiddenFieldHandler.class) @Param("batch") Long batch,
		  @InputResult.Field(handler = HiddenFieldHandler.class) @Param("id") Long id,
		  @Param("cron") String cron,
		  HttpServletRequest request) {

		MessageResult result = factory.newInstance(MessageResult.class);
		result.setParentOperation("batch/schedules?id=" + batch.toString());

		Schedule schedule = null;

		SchedulerService scheduler = (SchedulerService) coreController.getService("SchedulerService");
		List<Schedule> schedules = scheduler.getSchedules("Batch:" + batch.toString());
		if (id != null) {
			for (Schedule s : schedules) {
				if (s.getId().equals(id)) {
					schedule = s;
					break;
				}
			}
		}

		if (id != null && schedule == null) {
			result.setMessage("Impossibile trovare la schedulazione richiesta");
			result.setType(MessageResult.MessageType.CRITICAL);
			result.setCancelAction("batch/schedules?id=" + batch.toString());
		} else {
			
			BatchDefinition def = em.find(BatchDefinition.class, batch);
			
			ParametersBuilder builder = new ParametersBuilder();
			builder.add("id", batch.toString());
			builder.add("type", "SCHEDULED");
			
			for (ParameterDefinition pd : def.getParameters()) {
				builder.add(pd.getName(), request.getParameter(pd.getName()));
			}

			scheduler.schedule(id, "Batch:" + batch.toString(), cron, "/batch/start", builder.build());

			result.setMessage("Batch schedulato con successo");
			result.setType(MessageResult.MessageType.CONFIRM);
			result.setConfirmAction("batch/schedules?id=" + batch.toString());

		}

		return result;
	}

	@Operation("toggleSchedule")
	public MessageResult toggleSchedule(@Param("batch") Long batch, @Param("id") Long id) {

		MessageResult result = factory.newInstance(MessageResult.class);

		Schedule schedule = null;

		SchedulerService scheduler = (SchedulerService) coreController.getService("SchedulerService");
		List<Schedule> schedules = scheduler.getSchedules("Batch:" + batch.toString());
		if (id != null) {
			for (Schedule s : schedules) {
				if (s.getId().equals(id)) {
					schedule = s;
					break;
				}
			}
		}

		if (schedule == null) {
			result.setMessage("Impossibile trovare la schedulazione richiesta");
			result.setType(MessageResult.MessageType.CRITICAL);
			result.setCancelAction("batch/schedules?id=" + batch.toString());
		} else {
			boolean nowActive = scheduler.toggle(schedule.getId());

			if (nowActive) {
				result.setMessage("Schedulazione attivata correttamente");
			} else {
				result.setMessage("Schedulazione disattivata correttamente");
			}

			result.setType(MessageResult.MessageType.CONFIRM);
			result.setConfirmAction("batch/schedules?id=" + batch.toString());
		}

		return result;

	}

	@Operation("deleteSchedule")
	public MessageResult descheduleBatch(@Param("batch") Long batch, @Param("id") Long id) {

		MessageResult result = factory.newInstance(MessageResult.class);

		Schedule schedule = null;

		SchedulerService scheduler = (SchedulerService) coreController.getService("SchedulerService");
		List<Schedule> schedules = scheduler.getSchedules("Batch:" + batch.toString());
		if (id != null) {
			for (Schedule s : schedules) {
				if (s.getId().equals(id)) {
					schedule = s;
					break;
				}
			}
		}

		if (schedule == null) {
			result.setMessage("Impossibile trovare la schedulazione richiesta");
			result.setType(MessageResult.MessageType.CRITICAL);
			result.setCancelAction("batch/schedules?id=" + batch.toString());
		} else {

			BatchDefinition def = em.find(BatchDefinition.class, batch);

			result.setMessage("Sicuri di voler rimuovere la schedulazione indicata per il batch \"" + def.getTitle() + "\"?");
			result.setType(MessageResult.MessageType.INFO);
			result.setCancelAction("batch/schedules?id=" + batch.toString());
			result.setConfirmAction("batch/confirmDeschedule?batch=" + batch.toString() + "&id=" + id.toString());

		}

		return result;

	}

	@Operation("confirmDeschedule")
	public MessageResult confirmDeleteSchedule(@Param("batch") Long batch, @Param("id") Long id) {

		MessageResult result = factory.newInstance(MessageResult.class);

		SchedulerService scheduler = (SchedulerService) coreController.getService("SchedulerService");
		scheduler.deschedule(id);

		result.setMessage("Schedulazione del batch rimossa con successo");
		result.setType(MessageResult.MessageType.INFO);
		result.setConfirmAction("batch/schedules?id=" + batch.toString());

		return result;

	}

	@Operation("tokens/")
	public TableResult tokens(@Param("id") Long id, OperationScope opScope) {

		TableResult result = factory.newInstance(TableResult.class);
		result.setParentOperation("batch/");
		
		BatchDefinition batch = em.find(BatchDefinition.class, id);
		
		List<BatchToken> tokens = em.createQuery("select x from BatchToken x where x.batch = :batch", BatchToken.class)
			  .setParameter("batch", batch)
			  .getResultList();

		StringBuilder builder = new StringBuilder();
		builder.append(configuration.getSiteProperty("site.url", "http://localhost:8080/"));
		String ctxPath = Context.get().getServletContext().getContextPath();
		if (StringUtils.isNotBlank(ctxPath) && !builder.toString().endsWith(builder.toString().endsWith("/") ? ctxPath + "/" : ctxPath)) {
			builder.append(ctxPath);
			if (!builder.toString().endsWith("/")) {
				builder.append("/");
			}
		}
		builder.append("batch/start?id=" + id.toString() + "&type=TRIGGERED&token=");

		for (BatchToken t : tokens) {
			try {
				t.setUrl(builder.toString() + URLEncoder.encode(t.getToken(), "UTF-8"));
			} catch (UnsupportedEncodingException ex) {
				LoggerUtils.getLogger(Operations.class).log(Level.SEVERE, null, ex);
			}
		}

		result.setTableData(tokens, BatchToken.class);

		result.addTableOperation("batch/tokens/edit?batch=" + id.toString(), "new");
		result.addRowOperation("batch/tokens/edit?batch=" + id.toString(), "edit");
		result.addRowOperation("batch/tokens/delete?batch=" + id.toString(), "delete");
		
		opScope.addBreadcrumbLink("batch/", "Batch");
		opScope.addBreadcrumbLink("batch/instances?id=" + id.toString(), batch.getTitle());
		opScope.addBreadcrumbLink("batch/tokens?id=" + id.toString(), "tokens");

		return result;

	}

	@Operation("tokens/edit")
	public InputResult tokensEdit(@Param("id") Long id, @Param("batch") Long batchId, OperationScope opScope) {

		InputResult result = factory.newInstance(InputResult.class);
		result.setParentOperation("batch/tokens/?id=" + batchId.toString());
		result.setAction("batch/tokens/save");
		
		BatchDefinition batch = em.find(BatchDefinition.class, batchId);

		if (id != null) {
			BatchToken token = em.find(BatchToken.class, id);
			if (token != null) {
				result.setData(token);
			}
		}
		result.setValue("batch", batchId.toString());
		
		opScope.addBreadcrumbLink("batch/", "Batch");
		opScope.addBreadcrumbLink("batch/instances?id=" + batchId.toString(), batch.getTitle());
		opScope.addBreadcrumbLink("batch/tokens?id=" + batchId.toString(), "tokens");

		return result;

	}

	@Operation("tokens/save")
	public MessageResult tokensSave(
		  @Param("id") @InputResult.Field(handler = HiddenFieldHandler.class) Long id,
		  @Param("batch") @InputResult.Field(handler = HiddenFieldHandler.class) Long batchId,
		  @Param("description") @InputResult.Field(required = true) String description
	) {

		MessageResult result = factory.newInstance(MessageResult.class);

		try {

			em.getTransaction().begin();

			BatchToken token = null;
			if (id != null) {
				token = em.find(BatchToken.class, id);
			}
			if (token == null) {
				id = null;
				token = new BatchToken();

				SecureRandom rand = new SecureRandom();
				byte[] buff = new byte[12];
				rand.nextBytes(buff);
				token.setToken(Base64.encodeBase64String(buff));

				BatchDefinition batch = em.find(BatchDefinition.class, batchId);
				token.setBatch(batch);
			}

			token.setDescription(description);

			if (id != null) {
				em.merge(token);
			} else {
				em.persist(token);
			}

			em.getTransaction().commit();

			result.setMessage("Token successfully saved");
			result.setType(MessageResult.MessageType.CONFIRM);
			result.setConfirmAction("batch/tokens/?id=" + token.getBatch().getId().toString());

		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}

		return result;

	}

	@Operation("tokens/delete")
	public MessageResult tokensDelete(@Param("id") Long id) {

		MessageResult result = factory.newInstance(MessageResult.class);

		BatchToken token = em.find(BatchToken.class, id);

		result.setParentOperation("batch/tokens/?id=" + token.getBatch().getId().toString());

		result.setMessage("Are you sure to delete the token \"" + token.getDescription() + "\" from batch \"" + token.getBatch().getTitle() + "\"?");
		result.setType(MessageResult.MessageType.INFO);
		result.setConfirmAction("batch/tokens/deleteConfirm?id=" + token.getId().toString());
		result.setCancelAction(result.getParentOperation());

		return result;

	}

	@Operation("tokens/deleteConfirm")
	public MessageResult tokensDeleteConfirm(@Param("id") Long id) {

		MessageResult result = factory.newInstance(MessageResult.class);

		try {

			em.getTransaction().begin();

			BatchToken token = em.find(BatchToken.class, id);

			result.setParentOperation("batch/tokens/?id=" + token.getBatch().getId().toString());

			em.remove(token);

			em.getTransaction().commit();

			result.setMessage("Token successfully removed");
			result.setType(MessageResult.MessageType.INFO);
			result.setConfirmAction(result.getParentOperation());

		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}

		return result;

	}

    @Operation(value="downloadWorkspaceFile")
    public StreamResult downloadWorkspaceFile(@Param(value="batch") Long batchId, @Param(value="instance") Long instanceId, @Param(value="file") String fileName) throws FileNotFoundException {
        StreamResult result = (StreamResult)this.factory.newInstance(StreamResult.class);
        File file = new File(this.configuration.getConfigDir(), "batch/workspaces/batch-" + batchId.toString() + "/inst-" + instanceId.toString() + "/" + FsUtils.normalizePath((String)fileName));
        if (!file.exists()) {
            throw new FileNotFoundException(fileName);
        }
        result.setAttachment(true);
        result.setContentLength(Long.valueOf(file.length()));
        result.setContentType("unknown/octet-stream");
        result.setName(file.getName());
        result.setStream((InputStream)new FileInputStream(file));
        return result;
    }
	
	@Operation("downloadWorkspace")
	public StreamResult downloadWorkspace(@Param("id") Long id) throws IOException, OperationNotFoundException {
		
		BatchInstance instance = em.find(BatchInstance.class, id);
		
		Workspace workspace = new Workspace();
		workspace.setBase(new File(configuration.getConfigDir(), "batch/workspaces/batch-" + instance.getBatch().getId().toString() + "/inst-" + instance.getId().toString()));
		if (workspace.getBase().exists()) {
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ZipOutputStream zos = new ZipOutputStream(baos);
			
			try {
				
				zipDir(workspace.getBase(), zos, "");
				
			} finally {
				zos.close();
			}
			
			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			StreamResult result = factory.newInstance(StreamResult.class);
			result.setStream(bais);
			result.setAttachment(true);
			result.setContentLength((long) baos.toByteArray().length);
			result.setContentType("application/zip");
			result.setName("workspace-" + instance.getBatch().getId().toString() + "-" + instance.getId().toString() + ".zip");
			return result;
		}
		
		throw new OperationNotFoundException("The requested workspace does not exist");
		
	}
	
	private void zipDir(File dir, ZipOutputStream zos, String path) throws IOException {
		
		if (dir == null) {
			return;
		}
		
		File[] files = dir.listFiles();
		if (files != null) {
			for (File f : files) {
				if (f.isDirectory()) {
					zipDir(f, zos, (StringUtils.isNotBlank(path) ? path + "/" : "") + f.getName());
				}
				else {
					ZipEntry entry = new ZipEntry(f.getName());
					entry.setTime(f.lastModified());
					zos.putNextEntry(entry);
					InputStream is = new FileInputStream(f);
					try {
						int read;
						byte[] buff = new byte[4096];
						while ((read = is.read(buff)) > 0) {
							zos.write(buff, 0, read);
						}
					} finally {
						is.close();
					}
				}
			}
		}
		
	}

	public static class StepInfo {

		private final String module;
		private final String className;
		private final String group;
		private final String systemName;
		private final String title;
		private final boolean workspace;

		public StepInfo(Class<? extends IBatchStep> stepClass, I18N i18n) {
			this.module = ((ModuleClassLoader) stepClass.getClassLoader()).getModule().getName();
			this.className = stepClass.getName();

			BatchStep info = stepClass.getAnnotation(BatchStep.class);
			group = info.group();
			systemName = info.name();
			title = i18n.translate(StringUtils.defaultIfBlank(info.title(), info.name()));
			workspace = info.workspace();
		}

		public String getModule() {
			return module;
		}

		public String getClassName() {
			return className;
		}

		public String getGroup() {
			return group;
		}

		public String getSystemName() {
			return systemName;
		}

		public String getTitle() {
			return title;
		}

		public boolean isWorkspace() {
			return workspace;
		}

	}

	public static class ScheduleInfo {

		@TableResult.TableResultColumn(hidden = true, key = true)
		private final Long id;
		private final String cron;
		private final Boolean active;

		public ScheduleInfo(Schedule schedule) {
			this.id = schedule.getId();
			this.cron = schedule.getCron();
			this.active = schedule.getActive();
		}

		public Long getId() {
			return id;
		}

		public String getCron() {
			return cron;
		}

		public Boolean getActive() {
			return active;
		}

	}

}
