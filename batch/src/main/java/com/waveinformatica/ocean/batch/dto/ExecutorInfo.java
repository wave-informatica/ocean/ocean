/*
 * Copyright 2015, 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.batch.dto;

import com.waveinformatica.ocean.batch.interfaces.IBatchExecutor;
import com.waveinformatica.ocean.batch.annotations.BatchExecutor;

/**
 *
 * @author Ivano
 */
public class ExecutorInfo {
    
    private final String name;
    private final Class<? extends IBatchExecutor> executorClass;

    public ExecutorInfo(Class<? extends IBatchExecutor> executorClass) {
	this.executorClass = executorClass;
	
	BatchExecutor annotation = this.executorClass.getAnnotation(BatchExecutor.class);
	if (annotation != null && annotation.value().trim().length() > 0) {
	    name = annotation.value().trim();
	}
	else {
	    name = executorClass.getSimpleName();
	}
	
    }

    public String getName() {
	return name;
    }

    public Class<? extends IBatchExecutor> getExecutorClass() {
	return executorClass;
    }
    
}
