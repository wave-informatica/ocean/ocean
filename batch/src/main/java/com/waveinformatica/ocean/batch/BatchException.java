/*
* Copyright 2015, 2017 Wave Informatica S.r.l..
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.waveinformatica.ocean.batch;

/**
 *
 * @author Ivano
 */
public class BatchException extends Exception {
	
	private final Integer exitCode;
	
	public BatchException() {
		this.exitCode = null;
	}
	
	public BatchException(String message) {
		super(message);
		this.exitCode = null;
	}
	
	public BatchException(String message, Throwable cause) {
		super(message, cause);
		this.exitCode = null;
	}
	
	public BatchException(Throwable cause) {
		super(cause);
		this.exitCode = null;
	}
	
	public BatchException(int exitCode) {
		this.exitCode = exitCode;
	}
	
	public BatchException(int exitCode, String message) {
		super(message);
		this.exitCode = exitCode;
	}
	
	public BatchException(int exitCode, String message, Throwable cause) {
		super(message, cause);
		this.exitCode = exitCode;
	}
	
	public BatchException(int exitCode, Throwable cause) {
		super(cause);
		this.exitCode = exitCode;
	}

	public Integer getExitCode() {
		return exitCode;
	}
	
}
