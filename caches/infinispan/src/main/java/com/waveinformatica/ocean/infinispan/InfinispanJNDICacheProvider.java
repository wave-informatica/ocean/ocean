/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.infinispan;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.lang.StringUtils;
import org.infinispan.Cache;
import org.infinispan.manager.CacheContainer;

import com.waveinformatica.ocean.core.annotations.CacheProvider;
import com.waveinformatica.ocean.core.cache.ICacheProvider;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.infinispan.InfinispanJNDICacheProvider.Config;

@CacheProvider("Infinispan JNDI")
public class InfinispanJNDICacheProvider implements ICacheProvider<Config> {
	
	@Inject
	private ObjectFactory factory;
	
	@Override
	public String getOperation() {
		return "$infinispan/config";
	}

	@Override
	public Map getCache(Config config) {
		
		try {
			
			InitialContext ic = new InitialContext();
			
			if (StringUtils.isNotBlank(config.getContainerJndi())) {
				
				CacheContainer cache = (CacheContainer) ic.lookup(config.getContainerJndi());
				
				return cache.getCache(config.getCacheName());
				
			}
			else if (StringUtils.isNotBlank(config.getCacheName())) {
				
				Cache cache = (Cache) ic.lookup(config.getCacheName());
				
				return cache;
				
			}
			else {
				return new ConcurrentHashMap<>();
			}
			
		} catch (NamingException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unable to find infinispan cache " + config.getContainerJndi() + " " + config.getCacheName(), e);
			return new ConcurrentHashMap<>();
		}
	}
	
	public static class Config {
		
		private String containerJndi;
		private String cacheName;

		public String getContainerJndi() {
			return containerJndi;
		}

		public void setContainerJndi(String containerJndi) {
			this.containerJndi = containerJndi;
		}

		public String getCacheName() {
			return cacheName;
		}

		public void setCacheName(String cacheName) {
			this.cacheName = cacheName;
		}
		
	}

	@Override
	public boolean checkConfiguration(Config config) {
		
		try {
			
			InitialContext ic = new InitialContext();
			
			if (StringUtils.isNotBlank(config.getContainerJndi())) {
				
				CacheContainer cache = (CacheContainer) ic.lookup(config.getContainerJndi());
				
				if (cache == null) {
					return false;
				}
				
				return cache.getCache(config.getCacheName()) != null;
				
			}
			else if (StringUtils.isNotBlank(config.getCacheName())) {
				
				Cache cache = (Cache) ic.lookup(config.getCacheName());
				
				return cache != null;
				
			}
			else {
				return false;
			}
			
		} catch (NamingException e) {
			LoggerUtils.getCoreLogger().log(Level.SEVERE, "Unable to find infinispan cache " + config.getContainerJndi() + " " + config.getCacheName(), e);
			return false;
		}
	}
	
}
