package com.waveinformatica.ocean.redis;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.waveinformatica.ocean.core.annotations.CacheProvider;
import com.waveinformatica.ocean.core.cache.ICacheProvider;

import redis.clients.jedis.JedisPool;

@CacheProvider("Redis")
public class RedisProvider implements ICacheProvider<RedisProvider.Config> {
	
	private static JedisPool jedisPool = null;
	
	public static class Config {
		
		private String host;

		public String getHost() {
			return host;
		}

		public void setHost(String host) {
			this.host = host;
		}
		
	}

	@Override
	public boolean checkConfiguration(Config config) {
		
		if (config == null || StringUtils.isBlank(config.getHost())) {
			return false;
		}
		
		return true;
	}

	@Override
	public String getOperation() {
		return "$redis/config";
	}

	@Override
	public Map getCache(Config config) {
		
		if (!checkConfiguration(config)) {
			return new HashMap<>();
		}
		
		if (jedisPool == null) {
			jedisPool = new JedisPool(config.getHost());
		}
		
		return new RedisCache(jedisPool);
		
	}
	
}
