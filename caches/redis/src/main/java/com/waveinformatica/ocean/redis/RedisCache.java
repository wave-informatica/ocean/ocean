package com.waveinformatica.ocean.redis;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import com.waveinformatica.ocean.core.util.LoggerUtils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class RedisCache implements Map {
	
	private final JedisPool pool;

	public RedisCache(JedisPool pool) {
		super();
		this.pool = pool;
	}

	@Override
	public int size() {
		try (Jedis jedis = pool.getResource()) {
			return jedis.dbSize().intValue();
		}
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	@Override
	public boolean containsKey(Object key) {
		if (key == null) {
			return false;
		}
		try (Jedis jedis = pool.getResource()) {
			return jedis.get(key.toString()) != null;
		}
	}

	@Override
	public boolean containsValue(Object value) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object get(Object key) {
		if (key == null) {
			return null;
		}
		try (Jedis jedis = pool.getResource()) {
			byte[] value = jedis.get(key.toString().getBytes());
			if (value != null) {
				return SerializationUtils.deserialize(value);
			}
			
		} catch (ClassNotFoundException e) {
			LoggerUtils.getLogger(RedisCache.class).log(Level.SEVERE, "Unable to get object with key " + key.toString(), e);
		} catch (IOException e) {
			LoggerUtils.getLogger(RedisCache.class).log(Level.SEVERE, "Unable to get object with key " + key.toString(), e);
		}
		return null;
	}

	@Override
	public Object put(Object key, Object value) {
		if (key == null || value == null) {
			return null;
		}
		try (Jedis jedis = pool.getResource()) {
			byte[] old = jedis.getSet(key.toString().getBytes(), SerializationUtils.serialize(value));
			if (old != null) {
				return SerializationUtils.deserialize(old);
			}
			
		} catch (ClassNotFoundException e) {
			LoggerUtils.getLogger(RedisCache.class).log(Level.SEVERE, "Unable to get/set object with key " + key.toString(), e);
		} catch (IOException e) {
			LoggerUtils.getLogger(RedisCache.class).log(Level.SEVERE, "Unable to get/set object with key " + key.toString(), e);
		}
		return null;
	}

	@Override
	public Object remove(Object key) {
		if (key == null) {
			return null;
		}
		try (Jedis jedis = pool.getResource()) {
			byte[] keyBytes = key.toString().getBytes();
			byte[] old = jedis.get(keyBytes);
			jedis.del(keyBytes);
			if (old != null) {
				return SerializationUtils.deserialize(old);
			}
		} catch (ClassNotFoundException e) {
			LoggerUtils.getLogger(RedisCache.class).log(Level.SEVERE, "Unable to get/set object with key " + key.toString(), e);
		} catch (IOException e) {
			LoggerUtils.getLogger(RedisCache.class).log(Level.SEVERE, "Unable to get/set object with key " + key.toString(), e);
		}
		return null;
	}

	@Override
	public void putAll(Map m) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clear() {
		try (Jedis jedis = pool.getResource()) {
			jedis.flushDB();
		}
	}

	@Override
	public Set keySet() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection values() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set entrySet() {
		// TODO Auto-generated method stub
		return null;
	}

}
