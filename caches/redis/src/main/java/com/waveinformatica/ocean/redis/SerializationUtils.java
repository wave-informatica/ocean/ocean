package com.waveinformatica.ocean.redis;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerializationUtils {
	
	public static byte[] serialize(Object obj) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try (ObjectOutputStream out = new ObjectOutputStream(baos)) { 
	        // Method for serialization of object 
	        out.writeObject(obj); 
		}
		
		return baos.toByteArray();
		
	}
	
	public static Object deserialize(byte[] serObject) throws ClassNotFoundException, IOException {
		
		ByteArrayInputStream bais = new ByteArrayInputStream(serObject);
		
		try (ObjectInputStream ois = new ObjectInputStream(bais)) {
			return ois.readObject();
		}
		
	}
	
}
