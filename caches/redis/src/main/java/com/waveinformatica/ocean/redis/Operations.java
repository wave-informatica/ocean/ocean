package com.waveinformatica.ocean.redis;

import javax.inject.Inject;

import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.annotations.Param;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;

@OperationProvider(namespace = "$redis")
public class Operations {
	
	@Inject
	private ObjectFactory factory;
	
	@Operation("config")
	public RedisProvider.Config config(
			@Param("host") String host
	) {
		
		RedisProvider.Config config = new RedisProvider.Config();
		
		config.setHost(host);
		
		return config;
		
	}
	
}
