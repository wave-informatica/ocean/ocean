package com.waveinformatica.ocean.gpalgorithms.objects;

import com.waveinformatica.ocean.gpalgorithms.math.Arithmetic;

public class Dimension<D extends Number> {
	
	private D width;
	private D height;
	
	public Dimension() {
		
	}
	
	public Dimension(D width, D height) {
		this.width = width;
		this.height = height;
	}

	public D getWidth() {
		return width;
	}

	public void setWidth(D width) {
		this.width = width;
	}

	public D getHeight() {
		return height;
	}

	public void setHeight(D height) {
		this.height = height;
	}
	
	public D getRatio() {
		return Arithmetic.div(width, height);
	}

}
