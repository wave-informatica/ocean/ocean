package com.waveinformatica.ocean.gpalgorithms.objects;

public class Rectangle<D extends Number> {
	
	private Dimension<D> size;
	
	public Rectangle() {
		
	}
	
	public Rectangle(Dimension<D> size) {
		this.size = size;
	}
	
	public Rectangle(D width, D height) {
		this.size = new Dimension<D>(width, height);
	}

	public Dimension<D> getSize() {
		return size;
	}

	public void setSize(Dimension<D> size) {
		this.size = size;
	}

	public D getWidth() {
		if (size == null) {
			return null;
		}
		return size.getWidth();
	}

	public void setWidth(D width) {
		if (size == null) {
			size = new Dimension<D>();
		}
		size.setWidth(width);
	}

	public D getHeight() {
		if (size == null) {
			return null;
		}
		return size.getHeight();
	}

	public void setHeight(D height) {
		if (size == null) {
			size = new Dimension<D>();
		}
		size.setHeight(height);
	}

}
