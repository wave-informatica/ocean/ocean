package com.waveinformatica.ocean.gpalgorithms.objects;

public class Point2D<D extends Number> {
	
	private D x;
	private D y;
	
	public Point2D() {
		
	}
	
	public Point2D(D x, D y) {
		this.x = x;
		this.y = y;
	}

	public D getX() {
		return x;
	}

	public void setX(D x) {
		this.x = x;
	}

	public D getY() {
		return y;
	}

	public void setY(D y) {
		this.y = y;
	}

}
