package com.waveinformatica.ocean.gpalgorithms.layouts;

import java.util.ArrayList;
import java.util.List;

import com.waveinformatica.ocean.gpalgorithms.objects.PositionedRectangle;
import com.waveinformatica.ocean.gpalgorithms.objects.Rectangle;

public abstract class AbstractLayout<D extends Number> {
	
	private Rectangle<D> container;
	private final List<PositionedRectangle<D>> children = new ArrayList<PositionedRectangle<D>>();
	
	public AbstractLayout() {
		
	}
	
	public AbstractLayout(Rectangle<D> container) {
		this.container = container;
	}
	
	public abstract void calculateLayout();

	public Rectangle<D> getContainer() {
		return container;
	}

	public void setContainer(Rectangle<D> container) {
		this.container = container;
	}

	public List<PositionedRectangle<D>> getChildren() {
		return children;
	}
	
	public void addChild(PositionedRectangle<D> child) {
		children.add(child);
	}
}
