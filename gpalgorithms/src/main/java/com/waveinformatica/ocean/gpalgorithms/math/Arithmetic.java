package com.waveinformatica.ocean.gpalgorithms.math;

public final class Arithmetic {
	
	public static <T extends Number> T add(T a, T b) {
		if (a instanceof Double || b instanceof Double || a instanceof Float || b instanceof Float) {
			return (T) ((Double) (a.doubleValue() + b.doubleValue()));
		}
		else {
			return (T) ((Long) (a.longValue() + b.longValue()));
		}
	}
	
	public static <T extends Number> T sub(T a, T b) {
		if (a instanceof Double || b instanceof Double || a instanceof Float || b instanceof Float) {
			return (T) ((Double) (a.doubleValue() - b.doubleValue()));
		}
		else {
			return (T) ((Long) (a.longValue() - b.longValue()));
		}
	}
	
	public static <T extends Number> T mul(T a, T b) {
		if (a instanceof Double || b instanceof Double || a instanceof Float || b instanceof Float) {
			return (T) ((Double) (a.doubleValue() * b.doubleValue()));
		}
		else {
			return (T) ((Long) (a.longValue() * b.longValue()));
		}
	}
	
	public static <T extends Number> T div(T a, T b) {
		if (a instanceof Double || b instanceof Double || a instanceof Float || b instanceof Float) {
			return (T) ((Double) (a.doubleValue() / b.doubleValue()));
		}
		else {
			return (T) ((Long) (a.longValue() / b.longValue()));
		}
	}
	
}
