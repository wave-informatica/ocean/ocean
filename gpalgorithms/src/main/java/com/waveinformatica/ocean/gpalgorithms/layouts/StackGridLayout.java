package com.waveinformatica.ocean.gpalgorithms.layouts;

import java.util.ArrayList;
import java.util.List;

import com.waveinformatica.ocean.gpalgorithms.math.Arithmetic;
import com.waveinformatica.ocean.gpalgorithms.objects.PositionedRectangle;
import com.waveinformatica.ocean.gpalgorithms.objects.Rectangle;

public class StackGridLayout<D extends Number> extends AbstractLayout<D> {

	public StackGridLayout() {
		
	}

	public StackGridLayout(Rectangle<D> container) {
		super(container);
	}

	@Override
	public void calculateLayout() {
		
		if (getChildren().isEmpty()) {
			return;
		}
		
		D h1 = null;
		D w1 = null;
		
		for (PositionedRectangle<D> c : getChildren()) {
			if (h1 == null && w1 == null) {
				h1 = c.getHeight();
				w1 = c.getWidth();
			}
			else {
				D w2 = Arithmetic.div(Arithmetic.mul(c.getWidth(), h1), c.getHeight());
				c.setHeight(h1);
				c.setWidth(w2);
				w1 = Arithmetic.add(w1, w2);
			}
		}
		
		double containerRatio = getContainer().getSize().getRatio().doubleValue();
		double stripeRatio = Arithmetic.div(w1, h1).doubleValue();
		
		int rows = 0;
		
		if (containerRatio >= 1) {
			rows = (int) Math.floor(Math.sqrt(stripeRatio / containerRatio));
		}
		else {
			rows = (int) Math.ceil(Math.sqrt(stripeRatio / containerRatio));
		}
		
		double cut = w1.doubleValue() / ((Integer) rows).doubleValue();
		D w = null;
		D h = null;
		List<List<PositionedRectangle<D>>> rowsList = new ArrayList<List<PositionedRectangle<D>>>();
		List<PositionedRectangle<D>> row = new ArrayList<PositionedRectangle<D>>();
		for (PositionedRectangle<D> c : getChildren()) {
			if (w == null || h == null) {
				w = c.getWidth();
				h = c.getHeight();
				row.add(c);
			}
			else {
				D nw = Arithmetic.add(w, c.getWidth());
				if (nw.doubleValue() > cut) {
					if (cut - w.doubleValue() > nw.doubleValue() - cut) {
						w = nw; // Aggiungo e allargo
						row.add(c);
					}
					else {
						// Passo alla riga successiva
						w = c.getWidth();
						rowsList.add(row);
						row = new ArrayList<PositionedRectangle<D>>();
						row.add(c);
					}
				}
				else {
					w = nw;
					row.add(c);
				}
			}
		}
		if (!row.isEmpty()) {
			rowsList.add(row);
		}
		
		double maxWidth = 0.0;
		List<D> widths = new ArrayList<D>(rowsList.size());
		for (List<PositionedRectangle<D>> r : rowsList) {
			D rw = calculateRowWidth(r);
			widths.add(rw);
			if (rw.doubleValue() > maxWidth) {
				maxWidth = rw.doubleValue();
			}
		}
		double maxWidthRatio = getContainer().getWidth().doubleValue() / maxWidth;
		
		List<Double> rowFactors = new ArrayList<Double>(rowsList.size());
		double totalHeight = 0.0;
		for (int i = 0; i < rowsList.size(); i++) {
			List<PositionedRectangle<D>> r = rowsList.get(i);
			double rowFactor = Arithmetic.div((Double) maxWidth, widths.get(i)).doubleValue() * maxWidthRatio;
			D rh = Arithmetic.mul(h, (D) ((Double) rowFactor));
			totalHeight += rh.doubleValue();
			rowFactors.add(rowFactor);
		}
		
		double finalFactor = 1.0;
		if (maxWidth / totalHeight < containerRatio) {
			finalFactor = getContainer().getHeight().doubleValue() / totalHeight;
		}
		D y = null;
		for (int i = 0; i < rowsList.size(); i++) {
			List<PositionedRectangle<D>> r = rowsList.get(i);
			D x = null;
			for (PositionedRectangle<D> c : r) {
				if (y == null) {
					y = Arithmetic.sub(c.getWidth(), c.getWidth());
				}
				if (x == null) {
					x = Arithmetic.sub(c.getWidth(), c.getWidth());
				}
				c.setX(x);
				c.setY(y);
				c.setWidth((D) Arithmetic.mul(c.getWidth(), Arithmetic.mul(rowFactors.get(i), (Double) finalFactor)));
				c.setHeight((D) Arithmetic.mul(c.getHeight(), Arithmetic.mul(rowFactors.get(i), (Double) finalFactor)));
				x = Arithmetic.add(x, c.getWidth());
			}
			y = (D) Arithmetic.add(y, Arithmetic.mul(h, Arithmetic.mul(rowFactors.get(i), (Double) finalFactor)));
		}
		
	}
	
	private D calculateRowWidth(List<PositionedRectangle<D>> row) {
		D w = null;
		for (PositionedRectangle<D> c : row) {
			if (w == null) {
				w = c.getWidth();
			}
			else {
				w = Arithmetic.add(w, c.getWidth());
			}
		}
		return w;
	}

}
