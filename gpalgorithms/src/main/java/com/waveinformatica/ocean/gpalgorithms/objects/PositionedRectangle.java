package com.waveinformatica.ocean.gpalgorithms.objects;

public class PositionedRectangle<D extends Number> extends Rectangle<D> {
	
	private Point2D<D> position;
	
	public PositionedRectangle() {
		
	}

	public PositionedRectangle(Point2D<D> position, Dimension<D> size) {
		super(size);
		this.position = position;
	}

	public PositionedRectangle(D x, D y, D width, D height) {
		super(width, height);
		this.position = new Point2D<D>(x, y);
	}

	public PositionedRectangle(Point2D<D> position, D width, D height) {
		super(width, height);
		this.position = position;
	}

	public PositionedRectangle(D x, D y, Dimension<D> size) {
		super(size);
		this.position = new Point2D<D>(x, y);
	}

	public Point2D<D> getPosition() {
		return position;
	}

	public void setPosition(Point2D<D> position) {
		this.position = position;
	}

	public D getX() {
		if (position == null) {
			return null;
		}
		return position.getX();
	}

	public void setX(D x) {
		if (position == null) {
			position = new Point2D<D>();
		}
		position.setX(x);
	}

	public D getY() {
		if (position == null) {
			return null;
		}
		return position.getY();
	}

	public void setY(D y) {
		if (position == null) {
			position = new Point2D<D>();
		}
		position.setY(y);
	}

}
