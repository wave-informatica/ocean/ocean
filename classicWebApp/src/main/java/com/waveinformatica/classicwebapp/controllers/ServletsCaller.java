/*******************************************************************************
 * Copyright 2015, 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waveinformatica.classicwebapp.controllers;

import com.waveinformatica.ocean.core.annotations.CoreEventListener;
import com.waveinformatica.ocean.core.controllers.IEventListener;
import com.waveinformatica.ocean.core.controllers.WebInterface;
import com.waveinformatica.ocean.core.controllers.events.CoreEventName;
import com.waveinformatica.ocean.core.controllers.events.Event;
import com.waveinformatica.ocean.core.controllers.events.WebRequestEvent;
import com.waveinformatica.ocean.core.controllers.results.MimeTypes;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.http.HttpServlet;

/**
 *
 * @author Ivano
 */
@CoreEventListener(eventNames = CoreEventName.WEB_REQUEST, flags = {"GET", "POST", "HEAD"})
public class ServletsCaller implements IEventListener {
    
    private static final Logger logger = LoggerUtils.getCoreLogger();
    
    private static final Date lastModified = new Date();
    
    private static final Map<String, HttpServlet> servletsByName = new HashMap<String, HttpServlet>();
    
    @Override
    public void performAction(Event event) {
        
        WebRequestEvent e = (WebRequestEvent) event;
        
        if (e.getRequestManagedBy() != null)
            return;
        
        Map<String, ? extends ServletRegistration> registrations = e.getRequest().getServletContext().getServletRegistrations();
        
        for (Map.Entry<String, ? extends ServletRegistration> entry : registrations.entrySet()) {
            
            // Avoid call of the Ocean Framework main Servlet
            if (entry.getValue().getClassName().equals(WebInterface.class.getName())) {
                continue;
            }
            
            Collection<String> mappings = entry.getValue().getMappings();
            for (String s : mappings) {
                if (s.endsWith("*") && e.getRequest().getPathInfo().startsWith(s)) {
                    callServletByRegistration(entry.getValue(), s, e);
                    e.setRequestManagedBy(this);
                    return;
                }
                else if (!s.endsWith("*") && s.substring(0, s.length() - 1).equals(e.getRequest().getPathInfo())) {
                    callServletByRegistration(entry.getValue(), s, e);
                    e.setRequestManagedBy(this);
                    return;
                }
                else if (s.startsWith("*") && e.getRequest().getPathInfo() != null && e.getRequest().getPathInfo().endsWith(s.substring(1))) {
                    callServletByRegistration(entry.getValue(), s, e);
                    e.setRequestManagedBy(this);
                    return;
                }
            }
        }
        
        // No suitable servlet found... looking for matching static web resource...
        if (e.getRequest().getPathInfo() == null || (!e.getRequest().getPathInfo().startsWith("WEB-INF/") && !e.getRequest().getPathInfo().startsWith("META-INF/"))) {
            
            InputStream is = e.getRequest().getServletContext().getResourceAsStream(e.getRequest().getPathInfo());
            if (is != null) {

                MimeTypes mt = null;
                String ext = e.getRequest().getPathInfo().substring(e.getRequest().getPathInfo().lastIndexOf(".") + 1);
                external:
                for (MimeTypes type : MimeTypes.values()) {
                    for (String extension : type.getExtensions()) {
                        if (extension.equals(ext)) {
                            mt = type;
                            break external;
                        }
                    }
                }

                e.getResponse().setHeader("Cache-Control", "PUBLIC");
                e.getResponse().setDateHeader("Expires", new Date(new Date().getTime() + 7*24*3600*1000).getTime());
                e.getResponse().setDateHeader("Last-Modified", lastModified.getTime());
                e.getResponse().setContentType(mt != null ? mt.getMimeType() : "application/unknown");
                try {
                    int read = 0;
                    byte[] buffer = new byte[4096];
                    while ((read = is.read(buffer)) > 0) {
                        e.getResponse().getOutputStream().write(buffer, 0, read);
                    }
                } catch (IOException ex) {
                    logger.log(Level.SEVERE, "Error sending static resource \"" + e.getRequest().getPathInfo() + "\"", ex);
                } finally {
                    try {
                        is.close();
                    } catch (IOException ex) {
                        logger.log(Level.SEVERE, null, ex);
                    }
                    e.setRequestManagedBy(this);
                }
                
            }
            
        }
        
    }
    
    private void callServletByRegistration(final ServletRegistration reg, final String mapping, final WebRequestEvent event) {
        
        HttpServlet servlet = servletsByName.get(reg.getName());
        
        if (servlet == null) {
            try {
                Class<?> cls = Class.forName(reg.getClassName());
                servlet = (HttpServlet) cls.newInstance();
                ServletConfig config = new OceanServletConfig(reg, event.getRequest().getServletContext());
                servlet.init(config);
                servletsByName.put(reg.getName(), servlet);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ServletsCaller.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(ServletsCaller.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(ServletsCaller.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ServletException ex) {
                Logger.getLogger(ServletsCaller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        try {
            
            servlet.service(event.getRequest(), event.getResponse());
            
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
        
    }
    
    public static class OceanServletConfig implements ServletConfig {
        
        private ServletRegistration registration;
        private ServletContext context;

        public OceanServletConfig(ServletRegistration registration, ServletContext context) {
            this.registration = registration;
            this.context = context;
        }
        
        @Override
        public String getServletName() {
            return registration.getName();
        }

        @Override
        public ServletContext getServletContext() {
            return context;
        }

        @Override
        public String getInitParameter(String string) {
            return registration.getInitParameter(string);
        }

        @Override
        public Enumeration<String> getInitParameterNames() {
            return Collections.enumeration(registration.getInitParameters().keySet());
        }
        
    }
    
}
