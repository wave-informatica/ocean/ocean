/*******************************************************************************
 * Copyright 2015, 2018 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.waveinformatica.ocean.scheduler;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.waveinformatica.ocean.core.annotations.OceanPersistenceContext;
import com.waveinformatica.ocean.core.annotations.Operation;
import com.waveinformatica.ocean.core.annotations.OperationProvider;
import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.results.TableResult;
import com.waveinformatica.ocean.core.controllers.scopes.AdminScopeProvider.AdminRootScope;
import com.waveinformatica.ocean.scheduler.entities.Schedule;

@OperationProvider(namespace = "scheduler", inScope = AdminRootScope.class)
public class Operations {
	
	@Inject
	private ObjectFactory factory;
	
	@Inject
	private CoreController controller;
	
	@Inject
	@OceanPersistenceContext
	private EntityManager em;
	
	@Operation("")
	public TableResult schedules(TableResult.TableResultConfig tableConfig) {
		
		TableResult result = factory.newInstance(TableResult.class);
		result.setParentOperation("admin/services");
		
		StringBuilder builder = new StringBuilder();
		builder.append("select x from Schedule x order by ");
		if (!tableConfig.isSorted()) {
			builder.append("x.group");
		}
		else {
			builder.append("x.");
			builder.append(tableConfig.getSorting().getColumnName());
			builder.append(' ');
			builder.append(tableConfig.getSorting().getDirection().name());
		}
		
		List<Schedule> schedules = em.createQuery(builder.toString(), Schedule.class)
			  .getResultList();
		
		SchedulerService service = controller.getService(SchedulerService.class);
		
		for (Schedule s : schedules) {
			s.setScheduled(service.isScheduled(s.getId()));
		}
		
		result.setTableData(schedules, Schedule.class);
		result.setDataConfiguration(tableConfig);
		result.getColumn("id").setSortable(true);
		result.getColumn("group").setSortable(true);
		if (!tableConfig.isSorted()) {
			tableConfig.setSorting(new TableResult.TableResultSorting());
			tableConfig.getSorting().setColumnName("group");
			tableConfig.getSorting().setDirection(TableResult.SortDirection.ASC);
		}
		result.getColumn("cron").setSortable(true);
		result.getColumn("operation").setSortable(true);
		result.getColumn("active").setSortable(true);
		
		return result;
		
	}
	
}