/*
 * Copyright 2016, 2019 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.waveinformatica.ocean.scheduler;

import com.waveinformatica.ocean.core.controllers.CoreController;
import com.waveinformatica.ocean.core.tasks.OceanRunnable;
import com.waveinformatica.ocean.core.util.LoggerUtils;

import java.util.Map;
import java.util.logging.Level;
import javax.inject.Inject;

/**
 *
 * @author Ivano
 */
public class ScheduledThread extends OceanRunnable {

    private String operation;
    private Map<String, String[]> parameters;
    
    @Inject
    private CoreController controller;
    
    @Override
    public void execute() {
	
	try {
	    
	    controller.executeOperation(operation, parameters);
	    
	} catch (Throwable ex) {
	    LoggerUtils.getLogger(ScheduledThread.class).log(Level.SEVERE, "Error starting scheduled operation \"" + operation + "\"", ex);
	}
	
    }

    public String getOperation() {
	return operation;
    }

    public void setOperation(String operation) {
	this.operation = operation;
    }

    public Map<String, String[]> getParameters() {
	return parameters;
    }

    public void setParameters(Map<String, String[]> parameters) {
	this.parameters = parameters;
    }
    
}
