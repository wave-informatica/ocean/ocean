/*******************************************************************************
 * Copyright 2015, 2016 Wave Informatica S.r.l..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.waveinformatica.ocean.scheduler;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.waveinformatica.ocean.core.annotations.OceanPersistenceContext;
import com.waveinformatica.ocean.core.annotations.Service;
import com.waveinformatica.ocean.core.controllers.IService;
import com.waveinformatica.ocean.core.controllers.ObjectFactory;
import com.waveinformatica.ocean.core.controllers.dto.ServiceStatus;
import com.waveinformatica.ocean.core.util.JsonUtils;
import com.waveinformatica.ocean.core.util.LoggerUtils;
import com.waveinformatica.ocean.scheduler.entities.Schedule;
import it.sauronsoftware.cron4j.Scheduler;
import it.sauronsoftware.cron4j.Task;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author Ivano
 */
@Service("SchedulerService")
public class SchedulerService implements IService {
	
	private Scheduler scheduler;
	private final Map<Long, String> schedules = new HashMap<Long, String>();
	
	@Inject
	private ObjectFactory factory;
	
	/**
	 * Allows to schedule or reschedule an operation invocation.
	 *
	 * @param id the ID of a previous schedule (to be rescheduled) or <code>null</code> if it is a new schedule
	 * @param group a value to group schedules (i.e. id of a referred entity)
	 * @param cron a UNIX cron formatted string (see <a href="http://www.sauronsoftware.it/projects/cron4j/manual.php" target="_blank">Cron4J Manual</a> for details)
	 * @param operation the full name of the operation to be invoked
	 * @param params the map of parameters for the operation
	 * @return the scheduled task's ID
	 */
	public Long schedule(Long id, String group, String cron, String operation, Map<String, String[]> params) {
		
		EntityManager em = factory.newInstance(Beans.class).getEntityManager();
		
		Gson gson = JsonUtils.getBuilder().create();
		
		Schedule schedule = null;
		
		if (id == null) {
			schedule = new Schedule();
			schedule.setActive(true);
		}
		else {
			schedule = em.find(Schedule.class, id);
		}
		
		schedule.setCron(cron);
		schedule.setGroup(group);
		schedule.setOperation(operation);
		schedule.setParameters(gson.toJson(params));
		
		em.getTransaction().begin();
		
		try {
			
			if (id == null) {
				em.persist(schedule);
			}
			else {
				em.merge(schedule);
			}
			
			em.getTransaction().commit();
			
		} catch (Exception e) {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			LoggerUtils.getLogger(SchedulerService.class).log(Level.SEVERE, "Error saving schedule", e);
		}
		
		if (scheduler != null && scheduler.isStarted()) {
			
			if (id != null && schedules.containsKey(id)) {
				scheduler.deschedule(schedules.get(id));
				schedules.remove(id);
			}
			
			if (schedule.getActive() != null && schedule.getActive()) {
				schedules.put(schedule.getId(), scheduler.schedule(cron, buildTask(schedule)));
			}
		}
		
		return schedule.getId();
		
	}
	
	/**
	 * Deschedules a scheduled task
	 *
	 * @param id the previous schedule ID
	 */
	public void deschedule(Long id) {
		
		if (id == null) {
			return;
		}
		
		EntityManager em = factory.newInstance(Beans.class).getEntityManager();
		
		Schedule schedule = em.find(Schedule.class, id);
		
		if (schedule == null) {
			return;
		}
		
		em.getTransaction().begin();
		
		try {
			
			em.remove(schedule);
			
			if (scheduler != null && scheduler.isStarted()) {
				scheduler.deschedule(schedules.get(id));
			}
			
			schedules.remove(id);
			
			em.getTransaction().commit();
			
		} catch (Exception e) {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			LoggerUtils.getLogger(SchedulerService.class).log(Level.SEVERE, "Error removing schedule", e);
		}
		
	}
	
	/**
	 * Retrieves the scheduled tasks as stored into the database
	 *
	 * @param group the group to filter results or <code>null</code> for all scheduled tasks
	 * @return list of filtered scheduled tasks
	 */
	public List<Schedule> getSchedules(String group) {
		
		EntityManager em = factory.newInstance(Beans.class).getEntityManager();
		
		StringBuilder builder = new StringBuilder();
		builder.append("select x from Schedule x");
		if (group != null) {
			builder.append(" where x.group = :group");
		}
		builder.append(" order by x.group, x.id");
		
		Query query = em.createQuery(builder.toString(), Schedule.class);
		if (group != null) {
			query.setParameter("group", group);
		}
		
		return query.getResultList();
		
	}
	
	/**
	 * Toggles schedule activation and returns the new state. If the schedule was active before toggle,
	 * it will be stopped, else it will be started.
	 *
	 * @param id the schedule's ID
	 * @return the new activation value
	 */
	public boolean toggle(Long id) {
		
		EntityManager em = factory.newInstance(Beans.class).getEntityManager();
		
		Schedule s = em.find(Schedule.class, id);
		
		if (s.getActive() != null && s.getActive()) {
			if (scheduler != null && scheduler.isStarted() && schedules.containsKey(id)) {
				scheduler.deschedule(schedules.get(id));
				schedules.remove(id);
			}
		}
		
		boolean oldActive = s.getActive() != null ? s.getActive() : false;
		
		s.setActive(s.getActive() != null ? !s.getActive() : true);
		
		em.getTransaction().begin();
		
		try {
			
			em.merge(s);
			
			em.getTransaction().commit();
			
		} catch (Exception e) {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			LoggerUtils.getLogger(SchedulerService.class).log(Level.SEVERE, "Unexpected error toggling schedule activation to " + s.getActive().toString() + " for schedule with id " + s.getId().toString(), e);
			return oldActive;
		}
		
		if (s.getActive()) {
			if (scheduler != null && scheduler.isStarted()) {
				schedules.put(s.getId(), scheduler.schedule(s.getCron(), buildTask(s)));
			}
		}
		
		return s.getActive();
		
	}
	
	public boolean isScheduled(Long id) {
		
		String name = schedules.get(id);
		if (name == null) {
			return false;
		}
		
		Task task = scheduler.getTask(name);
		if (task == null) {
			return false;
		}
		
		return true;
		
	}
	
	@Override
	public boolean start() {
		
		schedules.clear();

		if (scheduler != null && scheduler.isStarted()) {
			scheduler.stop();
		}
		scheduler = new Scheduler();
		
		List<Schedule> storedSchedules = getSchedules(null);
		for (Schedule s : storedSchedules) {
			schedules.put(s.getId(), scheduler.schedule(s.getCron(), buildTask(s)));
		}
		
		scheduler.start();
		
		return scheduler.isStarted();
	}
	
	private ScheduledThread buildTask(Schedule schedule) {
		
		ScheduledThread th = factory.newInstance(ScheduledThread.class);
		
		Gson gson = JsonUtils.getBuilder().create();
		Map<String, String[]> params = gson.fromJson(schedule.getParameters(), new TypeToken<Map<String, String[]>>(){}.getType());
		
		th.setOperation(schedule.getOperation());
		th.setParameters(params);
		
		return th;
		
	}
	
	@Override
	public boolean stop() {
		if (scheduler != null && scheduler.isStarted()) {
			scheduler.stop();
		}
		scheduler = null;
		schedules.clear();
		return true;
	}
	
	@Override
	public ServiceStatus getServiceStatus() {
		ServiceStatus status = new ServiceStatus();
		status.setRunning(scheduler != null && scheduler.isStarted());
		status.setManageOperation("scheduler/");
		return status;
	}
	
	public static class Beans {
		
		@Inject
		@OceanPersistenceContext
		private EntityManager entityManager;
		
		public EntityManager getEntityManager() {
			return entityManager;
		}
		
	}
	
}
