$(document).ready(function () {

	var templates = {};

	// Loading menu markup
	$.get('ris/tpl/mm-menu.html', {}, function (data) {

		$(document.body).append($(data));

		// Initialization
		mmButtons.newStage.call($('[data-mm-action="newStage"]')[0]);

	}, 'html');

	// Loading or creating project/document
	/*var mmDocument = -1;
	var mmProject = localStorage.mmProject;
	if (!mmProject) {
		mmProject = {
			documents: []
		};
		localStorage.setItem('mmProject', mmProject);
	}
	for (var i = 0; i < mmProject.documents.length; i++) {
		if (mmProject.documents[i].url == location.href) {
			mmDocument = i;
		}
	}*/

	var mmDocument = {
			url: location.pathname,
			stages: []
	};

	var mmStage = null;

	var mmButtons = {

			newStage: function () {
				var nsId = 0;
				for (var i = 0; i < mmDocument.stages.length; i++) {
					if (mmDocument.stages[i].id > nsId) {
						nsId = mmDocument.stages[i].id;
					}
				}
				nsId++;
				var newStage = {
						id: nsId,
						bubbles: [],
						button: null
				};
				mmDocument.stages.push(newStage);
				var ns = $('<button type="button" class="mm-button" data-mm-action="changeStage" title="Show stage ' + nsId + '" data-id="' + nsId + '"><i class="fa fa-television"></i></button>');
				$(this).closest('.mm-stages').prepend(ns);
				newStage.button = ns;
				ns.click();
			},

			changeStage: function () {
				if (mmStage) {
					mmStage.button.removeClass('active');
					for (var i = 0; i < mmStage.bubbles.length; i++) {
						$(mmStage.bubbles[i]).hide();
					}
				}
				for (var j = 0; j < mmDocument.stages.length; j++) {
					if (mmDocument.stages[j].id == $(this).data('id')) {
						mmStage = mmDocument.stages[j];
						mmStage.button.addClass('active');
						for (var i = 0; i < mmStage.bubbles.length; i++) {
							$(mmStage.bubbles[i]).show();
						}
						break;
					}
				}
			},

			newBubble: function () {
				selectElement(function (evt) {
					getTemplate('mm-bubble', this, function (tpl) {
						var bubble = $(tpl);
						$(document.body).append(bubble);
						var pos = $(evt.target).offset();
						bubble.addClass('left');	// Arrow position
						bubble.css({
							top: pos.top + $(evt.target).outerHeight(false) / 2 - 15 + 'px',
							left: pos.left + $(evt.target).outerWidth(false) + 10 + 'px'
						});
						var editor = new MediumEditor(bubble.find('> .mm-content')[0]);
						bubble.data('editor', editor);
						bubble.data('element', evt.target);
						mmStage.bubbles.push(bubble[0]);
					});
				});
			},

			bubbleChangePos: function () {
				var b = $(this).closest('.mm-bubble');
				b.removeClass('left');
				b.removeClass('right');
				b.removeClass('top');
				b.removeClass('bottom');
				b.addClass($(this).data('pos'));
				var $el = $(b.data('element'));
				var pos = $el.offset();
				switch ($(this).data('pos')) {
				case 'left':
					b.css({
						top: pos.top + $el.outerHeight(false) / 2 - 15 + 'px',
						left: pos.left + $el.outerWidth(false) + 10 + 'px'
					});
					break;
				case 'right':
					b.css({
						top: pos.top + $el.outerHeight(false) / 2 - 15 + 'px',
						left: pos.left - b.outerWidth(false) - 10 + 'px'
					});
					break;
				case 'bottom':
					b.css({
						top: pos.top - b.outerHeight(false) - 10 + 'px',
						left: pos.left + $el.outerWidth(false) / 2 - b.outerWidth(false) / 2 + 'px'
					});
					break;
				case 'top':
					b.css({
						top: pos.top + $el.outerHeight(false) + 10 + 'px',
						left: pos.left + $el.outerWidth(false) / 2 - b.outerWidth(false) / 2 + 'px'
					});
					break;
				}
			},

			bubbleDelete: function () {
				for (var i = 0; i < mmStage.bubbles.length; i++) {
					if (mmStage.bubbles[i] == $(this).closest('.mm-bubble')[0]) {
						mmStage.bubbles.splice(i, 1);
						break;
					}
				}
				$(this).closest('.mm-bubble').remove();
			},

			screenshot: function () {

				var $menu = $(this).closest('.mm-menu');
				$(document.body).addClass('mm-screenshot');

				var minX = 999999999,
					minY = 999999999,
					maxX = 0,
					maxY = 0;

				for (var i = 0; i < mmStage.bubbles.length; i++) {
					var $b = $(mmStage.bubbles[i]);
					var off = $b.offset();
					// $b
					if (off.left < minX) minX = off.left;
					if (off.top < minY) minY = off.top;
					if (off.left + $b.outerWidth() > maxX) maxX = off.left + $b.outerWidth();
					if (off.top + $b.outerHeight() > maxY) maxY = off.top + $b.outerHeight();
					// $b.element
					$el = $($b.data('element'));
					off = $el.offset();
					if (off.left < minX) minX = off.left;
					if (off.top < minY) minY = off.top;
					if (off.left + $el.outerWidth() > maxX) maxX = off.left + $el.outerWidth();
					if (off.top + $el.outerHeight() > maxY) maxY = off.top + $el.outerHeight();
				}

				minX = Math.max(0, minX - 50);
				minY = Math.max(0, minY - 50);
				maxX = Math.min($(window).width(), maxX + 50);
				maxY = Math.min($(window).height() + $(window).scrollTop(), maxY + 50);

				var node = document.body;

				domtoimage.toBlob(node)
				.then(function (blob) {
					renderImage(blob, minX, minY, maxX, maxY, function (dataUrl) {
						var link = document.createElement('a');
						link.download = 'my-image-name.png';
						link.href = dataUrl;
						link.click();
						$(document.body).removeClass('mm-screenshot');
					});
				}).catch(function (e) {
					console.log(e);
					$(document.body).removeClass('mm-screenshot');
				});
				
				/*domtoimage.toPng(node)
				.then(function (dataUrl) {
					var link = document.createElement('a');
					link.download = 'my-image-name.jpeg';
					link.href = dataUrl;
					link.click();
					$(document.body).removeClass('mm-screenshot');
				});*/
			}

	};

	var undoAction = null;
	var clickAction = null;

	$(document).on('click', '[data-mm-action]', function (evt) {
		mmButtons[$(this).data('mm-action')].call(this, evt);
	});

	$(document).on('keydown', function (evt) {
		if (evt.keyCode == 27) {	// ESC key
			if (undoAction) {
				undoAction.call(this);
				undoAction = null;
			}
		}
	});

	// Element selection
	$(document).on('mouseenter', '.mm-selection *', function (evt) {
		if (evt.target == this) {
			$(this).addClass('mm-highlighted');
		}
		else {
			$(this).removeClass('mm-highlighted');
		}
	});
	$(document).on('mouseleave', '.mm-selection *', function (evt) {
		$(this).removeClass('mm-highlighted');
	});
	$(document).on('click', '.mm-selection *', function (evt) {
		if (typeof clickAction === 'function') {
			var tmpAction = clickAction;
			clickAction = null;
			tmpAction.apply(this, arguments);
		}
	});

	function selectElement(callback) {

		$(document.body).addClass('mm-selection');

		undoAction = function () {
			$('.mm-highlighted').removeClass('mm-highlighted');
			$(document.body).removeClass('mm-selection');
		}

		clickAction = function (evt) {
			evt.preventDefault();
			evt.stopPropagation();
			if (evt.stopImmediatePropagation) {
				evt.stopImmediatePropagation();
			}
			evt.cancelBubble = true;
			undoAction();
			callback.apply(this, arguments);
		};

	};

	function getTemplate(tpl, scope, fn) {
		if (typeof templates[tpl] === 'undefined') {
			$.get('ris/tpl/' + tpl + '.html', {}, function (data) {
				templates[tpl] = data;
				fn.apply(scope, argsToArray(arguments, 3, [ templates[tpl] ]));
			}, 'html');
		}
		else {
			fn.apply(scope, argsToArray(arguments, 3, [ templates[tpl] ]));
		}
	}

	function argsToArray(args, offset, arr) {
		if (typeof offset === 'undefined') {
			offset = 0;
		}
		var res = typeof arr === 'undefined' ? [] : arr;
		for (var i = offset; i < args.length; i++) {
			res.push(args[i]);
		}
		return res;
	}

	function renderImage(blob, x1, y1, x2, y2, callback){
		
		var img = new Image();

		img.onload = function(){
			
			var canvas = $('<canvas>');
			$(document.body).append(canvas);
			canvas.attr('width', x2 - x1 + 1);
			canvas.attr('height', y2 - y1 + 1);
			
			var ctx2 = canvas[0].getContext('2d');
			ctx2.drawImage(img, x1, y1, x2 - x1 + 1, y2 - y1 + 1, 0, 0, x2 - x1 + 1, y2 - y1 + 1);
			var dataUrl = canvas[0].toDataURL('image/png');
			
			canvas.remove();
			
			callback(dataUrl);
			
		}

		img.src = URL.createObjectURL(blob);
		
	};

});